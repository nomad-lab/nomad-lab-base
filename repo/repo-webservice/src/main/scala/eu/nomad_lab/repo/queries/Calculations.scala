/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.queries

import com.sksamuel.elastic4s.IndexAndType
import com.sksamuel.elastic4s.get.GetDefinition
import com.sksamuel.elastic4s.http.ElasticDsl.{ termsAgg, topHitsAggregation, _ }
import com.sksamuel.elastic4s.http.get.MultiGetResponse
import com.sksamuel.elastic4s.json4s.ElasticJson4s.Implicits.Json4sHitReader
import com.sksamuel.elastic4s.searches.SearchDefinition
import com.sksamuel.elastic4s.searches.aggs.TermsOrder
import com.sksamuel.elastic4s.searches.sort.FieldSortDefinition
import eu.nomad_lab.elasticsearch.{ ApiCallException, ESManager }
import eu.nomad_lab.repo.RequestTransformer.PagingFilter
import eu.nomad_lab.repo.objects.JsonFormats.formats
import eu.nomad_lab.repo.objects._
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import eu.nomad_lab.{ Base32, JsonUtils }
import org.elasticsearch.search.sort.SortOrder
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.JValue
import org.json4s.native.Serialization
import org.{ json4s => jn }

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal

class Calculations(private val es: ESManager)(implicit private val exec: ExecutionContext) {

  def getGroups(representation: SearchDefinition, nGroups: Int,
    pagingMode: PagingFilter): Future[ResponseData[CalculationGroup]] = {
    import es.connector.metaNameToField

    val orderDef = TermsOrder("_term", asc = pagingMode.before.isEmpty)
    val query = representation.aggregations(
      termsAgg("calc_groups", metaNameToField("repository_grouping_checksum")).
        size(nGroups).order(orderDef).subaggs(
          topHitsAggregation("ref_docs").size(1).sortBy(
            FieldSortDefinition(metaNameToField("repository_calc_id"))
          )
        )
    )
    val results = es.client.execute(query)
    results.map { response =>
      val aggData = JsonUtils.parseStr(response.aggregationsAsString)
      val buckets = (aggData \ "calc_groups" \ "buckets").extract[Seq[JValue]]
      val groups = buckets.map { bucket =>
        val count = (bucket \ "doc_count").extract[Int]
        val sample = (bucket \ "ref_docs" \ "hits" \ "hits")(0) \ "_source"
        CalculationGroup(sample.extract[CalculationWrapper], count)
      }
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(response.totalHits),
        "is_timed_out" -> decompose(response.isTimedOut),
        "is_terminated_early" -> decompose(response.isTerminatedEarly),
        "doc_count_error_upper_bound" -> aggData \ "calc_groups" \ "doc_count_error_upper_bound",
        "sum_other_doc_count" -> aggData \ "calc_groups" \ "sum_other_doc_count"
      )
      ResponseData(groups, meta0)
    }
  }

  def getResults(representation: SearchDefinition, numResults: Int,
    pagingMode: PagingFilter): Future[ResponseData[Calculation]] = {
    import es.connector.metaNameToField

    val sortDef = FieldSortDefinition(
      metaNameToField("repository_calc_id"),
      order = if (pagingMode.before.isDefined) SortOrder.DESC else SortOrder.ASC
    )
    val query = representation.limit(numResults).sortBy(sortDef)
    val results = es.client.execute(query)
    results.map { searchRes =>
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(searchRes.totalHits),
        "is_timed_out" -> decompose(searchRes.isTimedOut),
        "is_terminated_early" -> decompose(searchRes.isTerminatedEarly)
      )
      val jV = Json4sHitReader[CalculationWrapper](Serialization, formats,
        manifest[CalculationWrapper])
      val entries = searchRes.to[CalculationWrapper](jV).map(_.section_repository_info)
      //TODO: include next link in the response (how to account for nginx redirects?)
      ResponseData(entries, meta0)
    }
  }

  def getDocuments(
    calcIds: Option[String],
    pIds: Option[String]
  ): Future[ResponseData[Calculation]] = {
    if (calcIds.isEmpty && pIds.isEmpty) {
      return Future.failed(ApiCallException("neither calcId nor pId given"))
    }
    val gets = try {
      val index = IndexAndType(es.settings.indexNameData, es.settings.typeNameData)
      val getCalcIds: Seq[GetDefinition] = calcIds match {
        case Some(list) => list.split(",").map(x => GetDefinition(index, x))
        case None => Seq()
      }
      val getPIds: Seq[GetDefinition] = pIds match {
        case Some(list) => list.split(",").map(x =>
          GetDefinition(index, Base32.b32DecodeLongRepository(x).toString))
        case None => Seq()
      }
      multiget(getCalcIds ++ getPIds)
    } catch {
      case NonFatal(e) => return Future.failed(e)
    }
    val esQueryResult = es.client.execute(gets)
    esQueryResult.map { getRes: MultiGetResponse =>
      val jV = Json4sHitReader[CalculationWrapper](Serialization, formats,
        manifest[CalculationWrapper])
      val docs = getRes.docs.filter(_.found).map(_.to[CalculationWrapper](jV))
      val meta0 = Map[String, jn.JValue](
        "total_docs" -> decompose(getRes.docs.length),
        "docs_found" -> decompose(getRes.docs.count(_.found)),
        "docs_missing" -> decompose(getRes.docs.count(!_.found))
      )
      ResponseData(docs.map(_.section_repository_info), meta0)
    }
  }

}
