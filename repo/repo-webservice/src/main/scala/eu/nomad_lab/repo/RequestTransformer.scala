/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import eu.nomad_lab.elasticsearch.{ ApiCallException, ESManager }
import eu.nomad_lab.repo.queries.TopicsData

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.matching.Regex

object RequestTransformer {

  val allDataRe: Regex = """^all$""".r
  val sourceRe: Regex = """^(uploader|coauthor|shared|staging),([0-9]+)$""".r
  val trueRe: Regex = """^(true|false|yes|no)$""".r

  case class PagingFilter(fieldName: String, after: Option[Any], before: Option[Any])

}

class RequestTransformer(
    private val es: ESManager,
    private val topicsData: TopicsData
)(implicit private val exec: ExecutionContext) {
  import RequestTransformer._

  def getPidMapping(query: String): String = {
    s"alltarget repository_archive_gid = $query"
  }

  def transformTIdRequest(tIdsString: Option[String], onlySelectedAtoms: Boolean,
    dataSourceString: Option[String], comment: Option[String], groupId: Option[String],
    dataSets: Option[String], uploadIds: Seq[Int],
    paging: Option[PagingFilter]): Future[Option[String]] = {
    val topics = topicsData.retrieveTopicsInformation()
    topics.map { topicList =>
      val dataSource = dataSourceString match {
        case None => Seq("""any repository_access_now = "Open","Restricted"""")
        case Some(allDataRe()) => Seq("""any repository_access_now = "Open","Restricted"""")
        case Some(sourceRe(source, userID)) => source match {
          case "uploader" => Seq(
            s"alltarget uploader_repo_id = $userID",
            """any repository_access_now = "Open","Restricted""""
          )
          case "coauthor" => Seq(
            s"alltarget author_repo_id = $userID",
            """any repository_access_now = "Open","Restricted""""
          )
          case "shared" => Seq(
            s"alltarget shared_with_repo_id = $userID",
            """any repository_access_now = "Open","Restricted""""
          )
          case "staging" => Seq(
            s"alltarget uploader_repo_id = $userID",
            """any repository_access_now = "Staging""""
          )
        }
        case Some(x) => throw new UnsupportedOperationException(s"invalid data source: $x")
      }
      val authorCategories = Topic.keyToCids("section_author_info")
      val elementCategories = Topic.keyToCids("repository_atomic_elements")
      val filterTopics: Seq[Topic] = tIdsString match {
        case None => Seq()
        case Some(x) => x.split(",").map(_.toInt).map { tId =>
          topicList.find(x => x.repository_topic_id == tId).get
        }
      }
      val (elementTopics, otherTopics) = filterTopics.partition {
        x => elementCategories.contains(x.repository_topic_category_id)
      }
      val elements = if (onlySelectedAtoms && elementTopics.nonEmpty) {
        Some(s"all repository_atomic_elements = " +
          elementTopics.map(_.value).mkString("\"", "\",\"", "\""))
      } else if (elementTopics.nonEmpty) {
        Some(s"alltarget repository_atomic_elements = " +
          elementTopics.map(_.value).mkString("\"", "\",\"", "\""))
      } else {
        None
      }
      val topics = otherTopics.map { myTopic =>
        if (authorCategories.contains(myTopic.repository_topic_category_id)) {
          "alltarget author_repo_id = \"" + myTopic.userId + "\""
        } else {
          "alltarget " + Topic.cidToKey(myTopic.repository_topic_category_id) + " = \"" +
            myTopic.value + "\""
        }
      }
      val uploadSearch = if (uploadIds.nonEmpty)
        Some(s"""any upload_id = ${uploadIds.mkString("\"", "\",\"", "\"")}""")
      else
        None
      val commentSearch = comment match {
        case Some(text) => Some(s"""any repository_comment ~ "$text"""")
        case None => None
      }
      val groupSearch = groupId match {
        case Some(id) => Some(s"""alltarget repository_grouping_checksum = "$id"""")
        case None => None
      }
      val dataSetsSearch = dataSets match {
        case Some(ids) => Some(s"""alltarget dataset_calc_id = "$ids"""")
        case None => None
      }
      val offsetFilter = getPagingFilter(paging)
      val conditions = dataSource ++ topics ++ elements ++ commentSearch ++
        groupSearch ++ dataSetsSearch ++ offsetFilter ++ uploadSearch
      conditions.size match {
        case 0 => None
        case 1 => Some(conditions.head)
        case _ => Some(conditions.tail.fold(conditions.head)((x, y) => x + " AND " + y))
      }
    }
  }

  def getPagingFilter(paging: Option[PagingFilter]): Option[String] = {
    paging match {
      case None => None
      case Some(params) => (params.before, params.after) match {
        case (Some(_), Some(_)) => throw ApiCallException("only 'before' or 'after' allowed")
        case (Some(before), None) => Some(s"""any ${params.fieldName} < "$before"""")
        case (None, Some(after)) => Some(s"""any ${params.fieldName} > "$after"""")
        case (None, None) => None
      }
    }
  }
}
