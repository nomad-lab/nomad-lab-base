package eu.nomad_lab.repo.queries

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.searches.SearchDefinition
import com.sksamuel.elastic4s.searches.sort.FieldSortDefinition
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.objects._
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.{ JArray, JValue }
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.{ json4s => jn }

import scala.concurrent.{ ExecutionContext, Future }

class Uploads(private val es: ESManager)(implicit private val exec: ExecutionContext) {

  def getUploads(representation: SearchDefinition, nGroups: Int): Future[ResponseData[UploadResult]] = {
    import es.connector.metaNameToField

    val query = representation.aggregations(
      termsAgg("uploads", metaNameToField("upload_id")).size(nGroups.min(1000)).subaggs(
        topHitsAggregation("ref_docs").size(1).sortBy(
          FieldSortDefinition(metaNameToField("repository_calc_id"))
        ),
        termsAgg("access_levels", metaNameToField("repository_access_now")),
        minAgg("upload_date", metaNameToField("upload_date"))
      )
    )
    es.client.execute(query).map { response =>
      val aggData = JsonUtils.parseStr(response.aggregationsAsString)
      val buckets = (aggData \ "uploads" \ "buckets").extract[Seq[JValue]]
      val uploads = buckets.map { upload =>
        val id = (upload \ "key").extract[Int]
        val count = (upload \ "doc_count").extract[Int]
        val sampleSource = (upload \ "ref_docs" \ "hits" \ "hits")(0) \ "_source"
        val access = (upload \ "access_levels" \ "buckets").extract[JArray].arr.map { level =>
          AccessLevel.withName((level \ "key").extract[String]) -> (level \ "doc_count").extract[Int]
        }.toMap
        val sample = sampleSource.extract[CalculationWrapper].section_repository_info
        val myUpload = Upload(id, sample.section_uploader_info, sample.upload_date.getOrElse(-1), None)
        UploadResult(myUpload, access, count)
      }
      val agg = response.termsAgg("uploads")
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(response.totalHits),
        "is_timed_out" -> decompose(response.isTimedOut),
        "is_terminated_early" -> decompose(response.isTerminatedEarly),
        "doc_count_error_upper_bound" -> decompose(agg.docCountErrorUpperBound),
        "sum_other_doc_count" -> decompose(agg.otherDocCount)
      )
      ResponseData(uploads, meta0)
    }
  }

}
