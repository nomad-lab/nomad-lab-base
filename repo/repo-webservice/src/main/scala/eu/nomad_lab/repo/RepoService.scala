/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.{ HttpEntity, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }
import eu.nomad_lab.webservice_base.{ ElasticSearchWebservice, ResponseFormatter }
import io.swagger.models.Scheme

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

object RepoService extends App with ElasticSearchWebservice {

  implicit val system: ActorSystem = ActorSystem("repoAkkaHttpServer", config)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher
  lazy val config: Config = LocalEnv.defaultConfig
  lazy val metaData: MetaInfoEnv = KnownMetaInfoEnvs.repo

  override val apiDocumentationClasses: Set[Class[_]] = Set(
    classOf[OldApi], classOf[NqlSearchApi], classOf[UtilityApi], classOf[UpdateApi]
  )
  override val skipDefinitions = Seq("Function1", "Function1RequestContextFutureRouteResult")
  override val apiDescription = "Explore the data in the NOMAD Repository."
  override val apiVersion = "0.2"
  override val apiTitle = "NOMAD Repository API"

  private val oldApi = OldApi(es)
  private val utilityApi = UtilityApi(es)
  private val updateApi = UpdateApi(es)
  private val nqlSearchApi = NqlSearchApi(es)

  override def serviceRoute = concat(oldApi.routes, utilityApi.routes, updateApi.routes,
    nqlSearchApi.routes)

  val interface = config.getString("nomad_lab.repo.webservice.interface")
  val port = config.getInt("nomad_lab.repo.webservice.port")
  val rootPathString = config.getString("nomad_lab.repo.webservice.rootPath")
  val hostName = config.getString("nomad_lab.repo.webservice.hostName")
  val protocol = config.getString("nomad_lab.repo.webservice.protocol") match {
    case "" => if (hostName.startsWith("localhost:")) Scheme.HTTP else Scheme.HTTPS
    case "http://" | "http" => Scheme.HTTP
    case "https://" | "https" => Scheme.HTTPS
    case x => throw new UnsupportedOperationException(s"protocol $x is not supported!")
  }
  val fullRoute = fullRoutes(
    serviceBaseName = "repo",
    rootPath = rootPathString.split("/").filter(_ != ""),
    hostName = hostName,
    includeSwagger = true,
    protocol = protocol
  )
  val serverBindingFuture: Future[ServerBinding] = Http().bindAndHandle(
    fullRoute, interface = interface, port = port
  )

  println(s"Server online at http://$interface:$port/\n")

  def resolveQueryResult(result: Future[String]): Route = {
    onComplete(result) {
      case Success(body) =>
        logRequest("served", Logging.InfoLevel) {
          val entity = HttpEntity(ResponseFormatter.jsonApi, body)
          complete(HttpResponse(status = StatusCodes.OK, entity = entity))
        }
      case Failure(ex) => throw ex
    }
  }
}