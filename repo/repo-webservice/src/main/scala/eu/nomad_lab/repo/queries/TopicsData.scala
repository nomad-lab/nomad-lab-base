/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.queries

import com.sksamuel.elastic4s.IndexAndType
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.search.Bucket
import com.sksamuel.elastic4s.json4s.ElasticJson4s.Implicits.Json4sHitReader
import com.sksamuel.elastic4s.searches.SearchDefinition
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.Topic
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.json4s.native.Serialization
import org.{ json4s => jn }

import scala.concurrent.{ ExecutionContext, Future, Promise }

object TopicsData {

  case class TopicMatch(
      id: Int,
      categoryId: Int,
      topic: String,
      categoryName: String,
      count: Int
  ) extends BaseValue {
    override def typeStr = "Topic_information"
    override def idStr: String = id.toString

    override def attributes: Map[String, jn.JValue] = {
      Map(
        "repository_topic_category_id" -> decompose(categoryId),
        "repository_topic_name" -> decompose(topic),
        "repository_topic_category" -> decompose(categoryName),
        "matches" -> decompose(count)
      )
    }
  }
}

class TopicsData(private val es: ESManager)(implicit private val exec: ExecutionContext) {
  import TopicsData._

  def retrieveTopicsInformation(): Future[Seq[Topic]] = {
    val baseQuery = search(IndexAndType(es.settings.indexNameTopics, es.settings.typeNameTopics)).
      query(matchAllQuery()).size(500).sortByFieldAsc("repository_topic_id")
    val jV = Json4sHitReader[Topic](Serialization, formats, manifest[Topic])
    val promise = Promise[Seq[Topic]]()

    def iterateOverTopics(data: Seq[Topic], offset: Int): Unit = {
      val newQuery = baseQuery.searchAfter(Seq(offset))
      val results = es.client.execute(newQuery)
      results.onComplete {
        case util.Success(response) =>
          val newData = response.to[Topic](jV)
          if (newData.isEmpty) {
            promise.success(data)
          } else {
            val last_topic_index = response.hits.hits.map {
              x => x.sourceAsMap("repository_topic_id").asInstanceOf[Int]
            }.fold(offset)(math.max)
            iterateOverTopics(data ++ newData, last_topic_index)
          }
        case util.Failure(error) => promise.failure(error)
      }
    }
    iterateOverTopics(Seq(), -1)
    promise.future
  }

  def getFacets(representation: SearchDefinition): Future[ResponseData[TopicMatch]] = {
    val topics = retrieveTopicsInformation()
    val staticFacets = getStaticFacets(topics, representation)
    val authorFacets = getAuthorFacets(topics, representation)

    staticFacets.zip(authorFacets).map { tuple =>
      val allTopics = tuple._1 ++ tuple._2
      val meta = Map("total_topics" -> decompose(allTopics.size)) ++
        Topic.keyToCids.map { entry =>
          "topics_" + entry._1 ->
            decompose(allTopics.count(x => entry._2.contains(x.categoryId)))
        }
      ResponseData(allTopics, meta)
    }
  }

  @deprecated("only for backwards compatibility", "ApiVersion 0.1")
  def getFacetsSimple(representation: SearchDefinition): Future[String] = {
    val topics = retrieveTopicsInformation()
    val staticFacets = getStaticFacets(topics, representation)
    val authorFacets = getAuthorFacets(topics, representation)

    staticFacets.zip(authorFacets).map { tuple =>
      val allTopics = tuple._1 ++ tuple._2
      val mapping = List(allTopics.map(entry => (entry.id.toString, jn.JInt(entry.count)))).flatten
      JsonUtils.prettyStr(jn.JObject(mapping))
    }
  }

  private def getStaticFacets(topics: Future[Seq[Topic]], representation: SearchDefinition): Future[Seq[TopicMatch]] = {
    import es.connector.metaNameToField

    val static = {
      val staticDataQuery = representation.aggs(
        for (field <- Topic.tagFields.filter(_ != "section_author_info.author_repo_id")) yield {
          termsAgg(field, metaNameToField(field)).size(100)
        }
      )
      es.client.execute(staticDataQuery)
    }
    topics.zip(static).map { tuple =>
      Topic.tagFields.filter(_ != "section_author_info.author_repo_id").flatMap {
        topicName =>
          tuple._2.termsAgg(topicName).buckets.flatMap { bucket =>
            tuple._1.find(x => x.value == bucket.key) match {
              case None => None
              case Some(topic) =>
                Some(TopicMatch(topic.repository_topic_id, topic.repository_topic_category_id,
                  topic.repository_topic_name, Topic.cidToKey(topic.repository_topic_category_id),
                  bucket.docCount))
            }
          }
      }
    }
  }

  private def getAuthorFacets(topics: Future[Seq[Topic]], representation: SearchDefinition): Future[Seq[TopicMatch]] = {
    import es.connector.metaNameToField

    val authorPromise = Promise[Seq[Bucket]]()
    def iterateOverAuthors(data: Seq[Bucket], excludes: Seq[Long]): Unit = {
      val newQuery = representation.aggs(
        termsAgg("authors", metaNameToField("author_repo_id")).size(400).
          includeExcludeLongs(Seq(), excludes)
      )
      val results = es.client.execute(newQuery)
      results.onComplete {
        case util.Success(response) =>
          val newData = response.termsAgg("authors").buckets
          if (newData.isEmpty) {
            authorPromise.success(data)
          } else {
            val newExcludes = response.termsAgg("authors").buckets.map(_.key.toLong)
            iterateOverAuthors(data ++ newData, excludes ++ newExcludes)
          }
        case util.Failure(error) => authorPromise.failure(error)
      }
    }
    iterateOverAuthors(Seq(), Seq(-1))
    val authors = authorPromise.future
    // FIXME: we have to work with options here because the database contains a flawed entry (user ID 325 is a dummy account which uploaded data, but has no topic)
    topics.zip(authors).map { tuple =>
      val categories = Topic.keyToCids("section_author_info")
      val authorTopics = tuple._1.filter(x => categories.contains(x.repository_topic_category_id))
      tuple._2.map { bucket =>
        authorTopics.find(x => x.userId == bucket.key.toInt).map { topic =>
          TopicMatch(topic.repository_topic_id, topic.repository_topic_category_id,
            topic.repository_topic_name, Topic.cidToKey(topic.repository_topic_category_id),
            bucket.docCount)
        }
      }
    }.map(x => x.filter(_.isDefined).map(_.get))
  }

}
