/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import javax.ws.rs.{ GET, Path, QueryParam }

import akka.http.scaladsl.server.{ Directives, Route }
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.queries.{ PidInfo, TopicsData }
import io.swagger.annotations._

import scala.concurrent.{ ExecutionContext, Future }

object UtilityApi {
  def apply(es: ESManager)(implicit exec: ExecutionContext): UtilityApi = {
    val topicsData = new TopicsData(es)
    val transform = new RequestTransformer(es, topicsData)
    val pidInfo = new PidInfo(es)
    apply(es, transform, pidInfo)
  }

  protected def apply(es: ESManager, transform: RequestTransformer,
    pidInfo: PidInfo)(implicit exec: ExecutionContext): UtilityApi = {
    new UtilityApi(es, transform, pidInfo)
  }
}

@Api(
  value = "Utility API, provides information for other services",
  produces = "vnd.api+json"
)
@Path("/utility")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class UtilityApi private (
  private val es: ESManager,
  private val transform: RequestTransformer,
  private val pidInfo: PidInfo
)(implicit private val exec: ExecutionContext)
    extends Directives {
  import es.connector
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult

  def routes: Route = pathPrefix("utility") {
    path("pids") {
      get {
        parameters('gid) { (archiveIds) => findCalculationsInArchive(archiveIds) }
      }
    }
  }

  @GET @Path("/pids")
  @ApiOperation(value = "list calculations belonging to the rawdata archive with the specified ID")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findCalculationsInArchive(
    @ApiParam(value = "checksum of the archive to query for")@QueryParam("gid") archiveId: String
  ): Route = onComplete {
    val query = Some(transform.getPidMapping(archiveId))
    val representation = Future { connector.transformToQuery(query) }
    val result = representation.map(pidInfo.getPIDsForArchiveID(_, archiveId))
    result.flatMap(identity)
  }(resolveQueryResult(_, loneValue = true))
}
