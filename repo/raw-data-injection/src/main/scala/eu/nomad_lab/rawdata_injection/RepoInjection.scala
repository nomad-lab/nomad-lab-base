/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection;

import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.rawdata
import eu.nomad_lab.repo
import eu.nomad_lab.Base64
import eu.nomad_lab.CompactSha
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.JsonSupport
import scala.collection.mutable
import scala.collection.breakOut
import org.{ json4s => jsn }
import org.jooq.impl.DSL
import repo.db.{ Tables => REPO }
import rawdata.db.{ Tables => RAWDATA }
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.Path
import scala.io.Source
import scala.io.Codec
import scala.collection.JavaConversions.asScalaSet
import scala.collection.JavaConversions.asScalaBuffer

object RepoInjection extends StrictLogging {
  /**
   *  guarantees that the path starts and ends with slashes
   */
  def guaranteeSlashes(path: String): String = {
    val preSlash = if (path.startsWith("/"))
      path
    else
      "/" + path
    if (preSlash.endsWith("/"))
      preSlash
    else
      preSlash + "/"
  }

  /**
   * error in repo injection
   */
  class RepoInjectionError(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * the upload is invalid
   */
  class UploadAccessAnalysisException(
    msg: String, what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * Unknown user id in repository requested
   */
  class InvalidRepoUserId(
    userId: Integer
  ) extends Exception(s"Requested user id $userId, which does not exists in the repository")

  class SplitError(
    msg: String,
    what: Throwable = null
  ) extends Exception(s"Error evaluating split: $msg", what)

  /**
   * kind of access
   */
  object Access extends Enumeration {
    type Access = Value
    val Open, Restricted, Mixed, Unknown = Value

    def composeAccess(a1: Access, a2: Access): Access = {
      a1 match {
        case Open =>
          a2 match {
            case Open => Open
            case Restricted => Mixed
            case Mixed => Mixed
            case Unknown => Open
          }
        case Restricted =>
          a2 match {
            case Open => Mixed
            case Restricted => Restricted
            case Mixed => Mixed
            case Unknown => Restricted
          }
        case Mixed =>
          a2 match {
            case Open => Mixed
            case Restricted => Mixed
            case Mixed => Mixed
            case Unknown => Mixed
          }
        case Unknown =>
          a2 match {
            case Open => Open
            case Restricted => Restricted
            case Mixed => Mixed
            case Unknown => Unknown
          }
      }
    }
  }

  /**
   * tree to store the repository calculation identifiers and access
   */
  sealed abstract class CalculationsTree {
    def access: Access.Value

    def clearComputedAccess(): Unit = ()
  }

  case class CalculationFile(
    val checksum: String,
    val access: Access.Value
  ) extends CalculationsTree

  class CalculationsDirectory(
      contents0: Map[String, CalculationsTree] = Map(),
      var _access: Option[Access.Value] = None
  ) extends CalculationsTree {
    val contents = mutable.Map[String, CalculationsTree]()
    contents ++= contents0

    override def access: Access.Value = {
      _access match {
        case Some(a) => a
        case None =>
          val newAccess = contents.foldLeft(Access.Unknown) {
            case (a1, (_, v)) =>
              Access.composeAccess(a1, v.access)
          }
          _access = Some(newAccess)
          newAccess
      }
    }

    override def clearComputedAccess(): Unit = {
      _access = None
      contents.foreach { case (_, v) => v.clearComputedAccess }
    }

    def insertCalculationsTree(cTree: CalculationsTree, path: String): Unit = {
      val sPath = path.dropWhile(_ == '/').split("/")
      if (sPath.isEmpty)
        throw new UploadAccessAnalysisException(s"Empty path '$path' in insertCalculationsTree")
      var contentsAtt: mutable.Map[String, CalculationsTree] = contents
      for (i <- 0.until(sPath.length - 1)) {
        val pathSegment = sPath(i)
        contentsAtt.get(pathSegment) match {
          case Some(CalculationsDirectory(newContents)) =>
            contentsAtt = newContents
          case None =>
            val newDir = CalculationsDirectory()
            contentsAtt += (pathSegment -> newDir)
            contentsAtt = newDir.contents
          case Some(CalculationFile(_, _)) =>
            throw new UploadAccessAnalysisException(s"Found file '$pathSegment' when expecting directory while inserting $path")
        }
      }
      val fileName = sPath(sPath.length - 1)
      contentsAtt.get(fileName) match {
        case Some(t) =>
          throw new UploadAccessAnalysisException(s"Found value $t when inserting $cTree at $path")
        case None =>
          contentsAtt += (fileName -> cTree)
      }
    }
  }

  object CalculationsDirectory {
    def apply(
      contents: Map[String, CalculationsTree] = Map(),
      access: Option[Access.Value] = None
    ): CalculationsDirectory = {
      new CalculationsDirectory(contents, access)
    }

    def unapply(d: CalculationsDirectory): Option[mutable.Map[String, CalculationsTree]] = {
      Some(d.contents)
    }
  }

  /**
   * describes an upload (what is open access and the corresponding calculations
   */
  case class Upload(
    val name: String,
    val openDirectories: Seq[String],
    val openCalculationsTree: Map[String, RepoInjection.CalculationsTree]
  )

  /**
   * Describes a chunk of upload that can be archived if open access
   */
  case class UploadChunk(
    val uploadName: String,
    val uploadPath: Path,
    val relativePath: String,
    val contents: Seq[String],
    val iChunk: Integer,
    val nChunks: Integer
  )

  def apply(): RepoInjection = {
    new RepoInjection(
      repoDb = repo.RepoDb(),
      rawDataDb = rawdata.RawDataDb(),
      splitDir = "/raw-data/splits",
      toSplitDir = "/raw-data/to_split"
    )
  }

  /**
   * represent a split in a file
   */
  class SplitElement(
    val relativePath: String,
    val contents: Seq[String]
  )

  object SplitElement {
    def apply(relativePath: String, contents: Seq[String]): SplitElement = {
      new SplitElement(
        relativePath = guaranteeSlashes(relativePath),
        contents = contents
      )
    }
  }

  case class SplitFile(
      uploadName: String,
      uploadPath: Path,
      splits: Seq[SplitElement],
      complete: Boolean
  ) {
    def verify(ri: RepoInjection): Unit = {
      val uId = ri.rawDataUploadIdWithNameFetchOrInsert(uploadName, Some(uploadPath))
      if (!Files.isDirectory(uploadPath))
        throw new SplitFile.InvalidSplitException(s"$uploadPath is not a directory in split for $uploadName")
      for ((chunk, ichunk) <- splits.zipWithIndex) {
        val baseDirectory = uploadPath.resolve(chunk.relativePath.dropWhile(_ == '/'))
        if (!Files.exists(baseDirectory))
          throw new SplitFile.InvalidSplitException(s"relative path ${chunk.relativePath} expands to $baseDirectory in chunk $ichunk of upload $uploadName")
        for (fileName <- chunk.contents) {
          if (fileName.contains("/"))
            throw new SplitFile.InvalidSplitException(s"contents are expected to be just names, not paths like $fileName in in chunk $ichunk of upload $uploadName")
          if (!baseDirectory.resolve(fileName).toFile().exists())
            throw new SplitFile.InvalidSplitException(s"could not find $fileName in $baseDirectory split $ichunk for $uploadName")
        }
      }
    }

    def toUploadChunks: Seq[UploadChunk] = {
      val nTot = if (complete)
        splits.length
      else
        0
      for ((catt, i) <- splits.zipWithIndex) yield {
        UploadChunk(
          uploadName,
          uploadPath,
          catt.relativePath,
          catt.contents,
          i,
          nTot
        )
      }
    }
  }

  case class DuLine(
    path: Path,
    sizeInGb: Double
  )

  /**
   * converts a line of du -h to a DuLine
   */
  def parseDuLine(line: String): Option[DuLine] = {
    val lineRe = """\s*([0-9.]+)([kKMGT]?)\s*(.+)$""".r
    line match {
      case lineRe(size, sizeUnits, path) =>
        val sizeUnitsValue = sizeUnits match {
          case "" => 1.0 / (1024.0 * 1024.0 * 1024.0)
          case "K" | "k" => 1.0 / (1024.0 * 1024.0)
          case "M" => 1.0 / 1024.0
          case "G" => 1.0
          case "T" => 1024.0
        }
        Some(DuLine(Paths.get(path), size.toDouble * sizeUnitsValue))
      case _ =>
        None
    }
  }

  object SplitFile {
    class InvalidSplitException(
      msg: String
    ) extends Exception(msg)

    /**
     * Reads a SplitElement out a line iterator
     */
    def readSplitElement(lines: Iterator[String]): Option[SplitElement] = {
      if (lines.hasNext) {
        val baseDir = lines.next
        val contents = mutable.Buffer[String]()
        while (lines.hasNext) {
          val contentLine = lines.next
          if (!contentLine.isEmpty)
            contents += contentLine
          else
            return Some(SplitElement(
              baseDir, contents
            ))
        }
        return Some(SplitElement(
          baseDir, contents
        ))
      } else {
        return None
      }
    }

    /**
     * Loads a Split file, i.e. a file of the form uploadName.partialsplit or
     * uploadName.split . It should should have to following format
     *
     * #uploadPath
     *
     * baseDirectory1
     * fileOrDirFromBaseDir1
     * fileOrDirFromBaseDir2
     *
     * baseDirectory2
     * ...
     */
    def apply(inF: java.io.File): SplitFile = {
      val inFName = inF.getName()
      val complete = if (inFName.endsWith(".split"))
        true
      else
        false
      val uploadName = if (inFName.endsWith(".split"))
        inFName.dropRight(".split".length)
      else if (inFName.endsWith(".partialsplit"))
        inFName.dropRight(".partialsplit".length)
      else
        throw new RepoInjection.SplitError(s"file ${inF.getPath()} is expected to have the .split or .partialsplit extension")

      val lines = Source.fromFile(inF)(Codec.UTF8).getLines()

      val uploadPath = if (lines.hasNext) {
        val l = lines.next()
        if (l.startsWith("# "))
          Paths.get(l.drop(2)).toAbsolutePath()
        else
          throw new SplitError(s"first line of a split file should start with '# ' and contain the upload path")
      } else {
        throw new SplitError(s"empty split file")
      }
      val spaceLine = lines.next()
      val content = mutable.Buffer[SplitElement]()
      if ("^\\s*$".r.findFirstIn(spaceLine).isEmpty)
        throw new SplitError(s"the second line should be empty, not $spaceLine")
      while (readSplitElement(lines) match {
        case Some(x) =>
          content += x
          true
        case None =>
          false
      }) ()

      SplitFile(
        uploadName,
        uploadPath,
        content,
        complete
      )
    }
  }

}

/**
 * class containing methods to synchronize the repository with the raw data
 */
class RepoInjection(
    val repoDb: repo.RepoDb,
    val rawDataDb: rawdata.RawDataDb,
    val splitDir: String,
    val toSplitDir: String
) extends StrictLogging {
  import RepoInjection.Access

  /**
   * return the raw data upload_id for the upload with the given name,
   * creating it from the repository data if necessary
   */
  def rawDataUploadIdWithNameFetch(uploadName: String): Option[Int] = {
    rawDataDb.dbContext.select(
      RAWDATA.UPLOAD.UPLOAD_ID
    ).from(
      RAWDATA.UPLOAD
    ).where(
      RAWDATA.UPLOAD.UPLOAD_NAME.equal(DSL.inline(uploadName))
    ).fetchOne(RAWDATA.UPLOAD.UPLOAD_ID) match {
        case null => None
        case newId => Some(newId)
      }
  }

  /**
   * return the raw data upload_id for the upload with the given name,
   * creating it from the repository data if necessary
   */
  def rawDataUploadIdWithNameFetchOrInsert(uploadName: String, uploadPath: Option[Path] = None): Int = {
    rawDataUploadIdWithNameFetch(uploadName) match {
      case None =>
        val uploadData = repoDb.dbContext.select(
          REPO.UPLOADS.CREATED,
          REPO.UPLOADS.TARGET_PATH,
          REPO.UPLOADS.USER_ID
        ).from(
          REPO.UPLOADS
        ).where(
          REPO.UPLOADS.UPLOAD_NAME.equal(DSL.inline(uploadName))
        ).fetchOne()
        if (uploadData == null) {
          val uploadData2 = repoDb.dbContext.select(
            REPO.UPLOADS.CREATED,
            REPO.UPLOADS.TARGET_PATH,
            REPO.UPLOADS.USER_ID,
            REPO.UPLOADS.IS_ALL_UPLOADED
          ).from(
            REPO.UPLOADS
          ).where(
            REPO.UPLOADS.TARGET_PATH.equal(uploadName)
          ).orderBy(
              REPO.UPLOADS.UPLOAD_ID.desc()
            ).limit(1).fetchOne()
          if (uploadData2 != null) {
            if (uploadData2.getValue(REPO.UPLOADS.IS_ALL_UPLOADED) != true)
              throw new RepoInjection.RepoInjectionError(s"Refusing to create raw data of partially uploaded upload $uploadName")
            val newId = rawDataDb.dbContext.insertInto(
              RAWDATA.UPLOAD,
              RAWDATA.UPLOAD.UPLOAD_NAME, RAWDATA.UPLOAD.UPLOAD_DATE, RAWDATA.UPLOAD.UPLOAD_PATH, RAWDATA.UPLOAD.AUTHOR_ID
            ).values(
              uploadName, uploadData2.value1(), uploadPath match {
              case None => uploadData2.value2()
              case Some(path) => path.toString
            }, rawDataAuthorIdAndUsernameWithRepoUserIdFetchOrInsert(uploadData2.value3())._1
            ).returning(
              RAWDATA.UPLOAD.UPLOAD_ID
            ).fetchOne().value1()
            if (newId == null)
              throw new RepoInjection.RepoInjectionError(s"could not create upload $uploadName")
            else
              newId
          } else {
            throw new RepoInjection.RepoInjectionError(s"asking for upload named '$uploadName' which does not exist in the repository DB")
            val date = new java.util.Date
            rawDataDb.dbContext.insertInto(
              RAWDATA.UPLOAD,
              RAWDATA.UPLOAD.UPLOAD_NAME, RAWDATA.UPLOAD.UPLOAD_DATE, RAWDATA.UPLOAD.UPLOAD_PATH
            ).values(
              uploadName, new java.sql.Timestamp(date.getTime()),
              uploadPath.getOrElse(Paths.get(repoDb.extractedDir, uploadName)).toString
            ).returning(
              RAWDATA.UPLOAD.UPLOAD_ID
            ).fetchOne().value1()
          }
        } else {
          val newId = rawDataDb.dbContext.insertInto(
            RAWDATA.UPLOAD,
            RAWDATA.UPLOAD.UPLOAD_NAME, RAWDATA.UPLOAD.UPLOAD_DATE, RAWDATA.UPLOAD.UPLOAD_PATH, RAWDATA.UPLOAD.AUTHOR_ID
          ).values(
            uploadName, uploadData.value1(), uploadPath match {
            case None => uploadData.value2()
            case Some(path) => path.toString
          }, rawDataAuthorIdAndUsernameWithRepoUserIdFetchOrInsert(uploadData.value3())._1
          ).returning(
            RAWDATA.UPLOAD.UPLOAD_ID
          ).fetchOne().value1()
          if (newId == null)
            throw new RepoInjection.RepoInjectionError(s"could not create upload $uploadName")
          else
            newId
        }
      case Some(newId) => newId
    }
  }

  def rawDataAuthorIdAndUsernameWithRepoUserIdFetch(repoUserId: Int): Option[(Int, String)] = {
    rawDataDb.dbContext.select(
      RAWDATA.AUTHOR.AUTHOR_ID,
      RAWDATA.AUTHOR.USERNAME
    ).from(
      RAWDATA.AUTHOR
    ).where(
      RAWDATA.AUTHOR.REPO_USER_ID.equal(repoUserId)
    ).fetchOne() match {
        case null => None
        case authorRecord =>
          Some((authorRecord.value1(), authorRecord.value2()))
      }
  }

  def rawDataAuthorIdAndUsernameWithRepoUserIdFetchOrInsert(repoUserId: Int): (Int, String) = {
    rawDataAuthorIdAndUsernameWithRepoUserIdFetch(repoUserId) match {
      case Some(rdIdAndUsername) =>
        rdIdAndUsername
      case None =>
        val authorData = repoDb.dbContext.select(
          REPO.USERS.FIRSTNAME,
          REPO.USERS.LASTNAME,
          REPO.AFFILIATIONS.NAME,
          REPO.USERS.USERNAME
        ).from(
          REPO.USERS.
            join(REPO.AFFILIATIONS).on(REPO.AFFILIATIONS.A_ID.equal(REPO.USERS.AFFILIATION))
        ).where(
            REPO.USERS.USER_ID.equal(repoUserId)
          ).fetchOne()
        if (authorData == null)
          throw new RepoInjection.InvalidRepoUserId(repoUserId)
        val firstName = authorData.value1()
        val lastName = authorData.value2()
        val affiliation = authorData.value3()
        val repoUserName = if (authorData.value4() == null)
          None
        else
          Some(authorData.value4())
        val nameGenerator = rawdata.Author.nameGenerator(
          firstName = firstName,
          lastName = lastName,
          affiliation = affiliation,
          repoUserName = repoUserName,
          repoUserId = Some(repoUserId)
        )
        def isUsed(name: String): Boolean = {
          rawDataDb.dbContext.selectOne().from(
            RAWDATA.AUTHOR
          ).where(
            RAWDATA.AUTHOR.USERNAME.equal(DSL.inline(name))
          ).fetchOne() != null
        }
        var i: Int = 0
        while (isUsed(nameGenerator(i)))
          i += 1
        val name: String = nameGenerator(i)
        val now = new java.util.Date
        val authorId = rawDataDb.dbContext.insertInto(
          RAWDATA.AUTHOR
        ).set(RAWDATA.AUTHOR.REPO_USER_ID, new Integer(repoUserId)).
          set(RAWDATA.AUTHOR.FIRST_NAME, firstName).
          set(RAWDATA.AUTHOR.LAST_NAME, lastName).
          set(RAWDATA.AUTHOR.AFFILIATION, affiliation).
          set(RAWDATA.AUTHOR.USERNAME, name).
          set(RAWDATA.AUTHOR.REPO_USERNAME, authorData.value4()).
          set(RAWDATA.AUTHOR.LAST_USER_CHANGE, new java.sql.Timestamp(now.getTime())).
          returning(
            RAWDATA.AUTHOR.AUTHOR_ID
          ).fetchOne().value1()
        (authorId, name)
    }
  }

  /**
   * analyzes the upload, and comes up with a list of open access subpaths
   */
  def analyzeUploadAccess(uploadName: String, forceOpen: Boolean = false): RepoInjection.Upload = {
    if (uploadName.isEmpty())
      throw new RepoInjection.UploadAccessAnalysisException("Invalid (empty) uploadName")
    val rootDir = RepoInjection.CalculationsDirectory()

    // get calculations in upload
    val calculationsForFirstQuery = repoDb.calculationsWithPrefixQuery(uploadName)
    val calcs = calculationsForFirstQuery.fetchLazy()
    while (calcs.hasNext()) {
      val calc = calcs.fetchOne()
      val calcAccess: RepoInjection.Access.Value = if (calc.value3() == 1)
        RepoInjection.Access.Open
      else
        RepoInjection.Access.Restricted
      val checksum: String = calc.value2()
      val cFile = RepoInjection.CalculationFile(checksum = checksum, access = calcAccess)
      val mainFile = repoDb.mainFileFromFilenames(calc.value4()) match {
        case Some(f) => f
        case None =>
          throw new RepoInjection.UploadAccessAnalysisException("Internal error, calculation has no main file")
      }
      rootDir.insertCalculationsTree(cFile, mainFile.relativePath)
    }

    // get previously open directories
    val openDirsRecord = rawDataDb.dbContext.select(
      RAWDATA.UPLOAD.UPLOAD_ID, RAWDATA.UPLOAD.OPEN_DIRECTORIES
    ).from(
      RAWDATA.UPLOAD
    ).where(RAWDATA.UPLOAD.UPLOAD_NAME.equal(DSL.inline(uploadName))).fetchOne()
    val previousOpenDirs: Set[String] = if (forceOpen)
      Set("/")
    else if (openDirsRecord != null)
      openDirsRecord.value2().split("\n").toSet
    else
      Set()

    val openDirs = mutable.ListBuffer[(String, RepoInjection.CalculationsDirectory)]()
    val toDo = mutable.ListBuffer[(String, RepoInjection.CalculationsDirectory)]("/" -> rootDir)
    while (!toDo.isEmpty) {
      val (path, dir) = toDo.last
      toDo.trimEnd(1)
      if (previousOpenDirs(path)) {
        openDirs.append(path -> dir)
        dir.access match {
          case Access.Open => ()
          case a =>
            logger.warn(s"Upload $uploadName path $path detected as $a, but kept open as it was open already")
        }
      } else {
        dir.access match {
          case Access.Open =>
            openDirs.append(path -> dir)
          case Access.Restricted =>
            ()
          case Access.Mixed =>
            for ((k, v) <- dir.contents) {
              v match {
                case subDir: RepoInjection.CalculationsDirectory =>
                  toDo.append((path + k + "/") -> subDir)
                case _ => ()
              }
            }
          case Access.Unknown =>
            ()
        }
      }
    }

    val newOpenDirs: Set[String] = openDirs.map(_._1)(breakOut)
    val upload = RepoInjection.Upload(
      name = uploadName,
      openDirectories = newOpenDirs.toSeq.sorted,
      openCalculationsTree = openDirs.toMap
    )

    if (newOpenDirs != previousOpenDirs) {
      val removed = previousOpenDirs -- newOpenDirs
      val added = newOpenDirs -- previousOpenDirs
      logger.warn(s"Open directories changed, removed ${removed.mkString("[", ",", "]")}, added ${added.mkString("[", ",", "]")}")
      val uploadId: Int = if (openDirsRecord == null)
        rawDataUploadIdWithNameFetchOrInsert(uploadName)
      else
        openDirsRecord.getValue(RAWDATA.UPLOAD.UPLOAD_ID)
      rawDataDb.dbContext.update(RAWDATA.UPLOAD).
        set(RAWDATA.UPLOAD.OPEN_DIRECTORIES, upload.openDirectories.map(_ + "\n").mkString("")).
        set(RAWDATA.UPLOAD.OPEN_DIRECTORIES_CHANGE, new java.sql.Timestamp((new java.util.Date).getTime)).
        where(RAWDATA.UPLOAD.UPLOAD_ID.equal(uploadId)).execute()
    }
    upload
  }

  def transferUsers(): Unit = {
    val repoUsers = repoDb.dbContext.select(
      REPO.USERS.USER_ID
    ).from(
      REPO.USERS
    ).orderBy(REPO.USERS.USER_ID).fetchLazy()
    while (repoUsers.hasNext()) {
      val repoUserId = repoUsers.fetchOne().value1()
      rawDataAuthorIdAndUsernameWithRepoUserIdFetchOrInsert(repoUserId)
    }
  }

  /**
   * checks the given upload splits, creating it if needed.
   * throws if the upload is incompatible with what was stored in the DB
   */
  def checkSplitFile(splitFile: RepoInjection.SplitFile, forceOpen: Boolean = false,
    fromChunk: Int = 0, untilChunk: Int = Int.MaxValue): Unit = {
    logger.info(s"checking upload ${JsonSupport.writeStr(splitFile)}")
    // check access
    val upload = analyzeUploadAccess(splitFile.uploadName, forceOpen)
    // check known split

    val knownChunks = rawDataDb.dbContext.select(
      RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID,
      RAWDATA.UPLOAD_CHUNK.RELATIVE_PATH
    ).from(
      RAWDATA.UPLOAD_CHUNK.
        join(RAWDATA.UPLOAD).on(RAWDATA.UPLOAD.UPLOAD_ID.equal(RAWDATA.UPLOAD_CHUNK.UPLOAD_ID))
    ).where(
        RAWDATA.UPLOAD.UPLOAD_NAME.equal(DSL.inline(splitFile.uploadName))
      ).orderBy(
          RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID
        ).fetch()

    val rawDataUploadId = rawDataUploadIdWithNameFetchOrInsert(splitFile.uploadName)
    if (knownChunks != null && knownChunks.size > splitFile.splits.size) {
      throw new RepoInjection.SplitError(s"Upload ${splitFile.uploadName} has a non trivial split change: had ${knownChunks.size} chunks, now only ${splitFile.splits.size}.")
    }
    val dbIdToChunkIndex = mutable.Map[Int, Int]()
    for ((chunk, ichunk) <- splitFile.splits.zipWithIndex) {
      if (knownChunks == null || knownChunks.size <= ichunk) {
        // missing, add it
        logger.info(s"${splitFile.uploadName} $ichunk was missing, adding")
        rawDataDb.dbContext.insertInto(RAWDATA.UPLOAD_CHUNK).
          set(RAWDATA.UPLOAD_CHUNK.UPLOAD_ID, new Integer(rawDataUploadId)).
          set(RAWDATA.UPLOAD_CHUNK.RELATIVE_PATH, chunk.relativePath).
          returning(RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID).fetchOne().value1() match {
            case null => throw new RepoInjection.SplitError(s"failed to insert chunk ${ichunk + 1} in upload ${splitFile.uploadName}")
            case ucId =>
              dbIdToChunkIndex += ((ucId: Int) -> ichunk)
              var chunkPieces = rawDataDb.dbContext.insertInto(
                RAWDATA.CHUNK_PIECE,
                RAWDATA.CHUNK_PIECE.NAME,
                RAWDATA.CHUNK_PIECE.UPLOAD_CHUNK_ID
              )
              for (c <- chunk.contents)
                chunkPieces = chunkPieces.values(c, ucId)
              chunkPieces.execute()
          }
      } else {
        val oldRelativePath = knownChunks.getValue(ichunk, RAWDATA.UPLOAD_CHUNK.RELATIVE_PATH)
        if (oldRelativePath != chunk.relativePath) {
          throw new RepoInjection.SplitError(s"Upload ${splitFile.uploadName} has a non trivial split change: chunk ${ichunk + 1} relativePath changes from $oldRelativePath to ${chunk.relativePath}.")
        }
        val oldContents = rawDataDb.dbContext.select(
          RAWDATA.CHUNK_PIECE.NAME
        ).from(
          RAWDATA.CHUNK_PIECE
        ).where(
          RAWDATA.CHUNK_PIECE.UPLOAD_CHUNK_ID.equal(knownChunks.getValue(ichunk, RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID))
        ).orderBy(
            RAWDATA.CHUNK_PIECE.CHUNK_PIECE_ID
          ).fetch(RAWDATA.CHUNK_PIECE.NAME) match {
            case null =>
              Seq()
            case cnts => asScalaBuffer(cnts)
          }
        if (oldContents != chunk.contents) // use sets and allow permutations?
          throw new RepoInjection.SplitError(s"Upload ${splitFile.uploadName} has a non trivial split change: chunk ${ichunk + 1} contents changed from $oldContents to ${chunk.contents}")
        dbIdToChunkIndex += ((knownChunks.getValue(ichunk, RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID): Int) -> ichunk)
        logger.info(s"${splitFile.uploadName} $ichunk already in DB")
      }
    }
    // check chunks not yet packed
    val openDirs = upload.openDirectories.toSet
    if (!openDirs.isEmpty) {
      val toPack = rawDataDb.dbContext.select(
        RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID
      ).from(
        RAWDATA.UPLOAD_CHUNK.
          leftOuterJoin(RAWDATA.RAW_DATA).on(
            RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID.equal(RAWDATA.RAW_DATA.UPLOAD_CHUNK_ID)
          )
      ).where(
          RAWDATA.UPLOAD_CHUNK.UPLOAD_ID.equal(rawDataUploadId).and(
            RAWDATA.RAW_DATA.RAW_DATA_ID.isNull()
          )
        ).fetch(RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID)
      val uploadInfo = rawDataDb.dbContext.select(
        RAWDATA.UPLOAD.UPLOAD_DATE,
        RAWDATA.UPLOAD.AUTHOR_ID
      ).from(
        RAWDATA.UPLOAD
      ).where(
        RAWDATA.UPLOAD.UPLOAD_NAME.equal(DSL.inline(splitFile.uploadName))
      ).fetchOne()
      if (uploadInfo == null)
        throw new RepoInjection.SplitError(s"Missing upload info for upload ${splitFile.uploadName}")
      val it = toPack.iterator()
      val owner = rawDataDb.authorWithAuthorId(uploadInfo.value2()).get
      val nBags = if (splitFile.complete)
        splitFile.splits.size
      else
        0
      while (it.hasNext()) {
        val uploadChunkId = it.next()
        val ichunk = dbIdToChunkIndex(uploadChunkId: Int)
        if (ichunk >= fromChunk && ichunk < untilChunk) {
          val chunk = splitFile.splits(ichunk)
          val path = RepoInjection.guaranteeSlashes(chunk.relativePath.toString())
          var isOpen = false
          var iChar = 0
          while (!isOpen && iChar != -1) {
            iChar = path.indexOf('/', iChar)
            if (openDirs.contains(path.take(iChar + 1)))
              isOpen = true
          }
          if (!isOpen && openDirs.contains(path)) // should never happen as path should end with /
            isOpen = true
          if (!isOpen && !chunk.contents.isEmpty) {
            isOpen = true
            for (el <- chunk.contents) {
              if (!openDirs.contains(chunk.relativePath + el + "/"))
                isOpen = false
            }
          }

          if (isOpen) {
            logger.info(s"${splitFile.uploadName} $ichunk has not been handled yet and is open, generating raw data")
            val packRes = BagitManager.packUpload(
              BagitManager.PackUploadMessage(
                uploadPath = RepoInjection.guaranteeSlashes(splitFile.uploadPath.toString()),
                uploadName = splitFile.uploadName,
                uploadDate = uploadInfo.getValue(RAWDATA.UPLOAD.UPLOAD_DATE),
                uploader = owner,
                pathInUpload = path,
                inPathEls = chunk.contents,
                targetDir = rawDataDb.rawDataDir.toString(),
                iBag = ichunk + 1,
                nBags = nBags
              ),
              overwrite = false
            )
            if (packRes.created) {
              logger.info(s"creating raw data for $path in upload ${splitFile.uploadName}")
              rawDataDb.dbContext.insertInto(RAWDATA.RAW_DATA).
                set(RAWDATA.RAW_DATA.UPLOAD_CHUNK_ID, uploadChunkId).
                set(RAWDATA.RAW_DATA.OWNER, owner.userName).
                set(RAWDATA.RAW_DATA.BASE_PATH, packRes.archivePath).
                set(RAWDATA.RAW_DATA.RAW_DATA_GID, packRes.archiveGid).
                set(RAWDATA.RAW_DATA.ASSIMILATION_DATE, new java.sql.Timestamp((new java.util.Date).getTime())).execute()
            } else if (packRes.didExist) {
              logger.info(s"adding raw data for existing $path in upload ${splitFile.uploadName}")
              rawDataDb.dbContext.insertInto(RAWDATA.RAW_DATA).
                set(RAWDATA.RAW_DATA.UPLOAD_CHUNK_ID, uploadChunkId).
                set(RAWDATA.RAW_DATA.OWNER, owner.userName).
                set(RAWDATA.RAW_DATA.BASE_PATH, packRes.archivePath).
                set(RAWDATA.RAW_DATA.RAW_DATA_GID, packRes.archiveGid).
                set(RAWDATA.RAW_DATA.ASSIMILATION_DATE, new java.sql.Timestamp((new java.util.Date).getTime())) execute ()
            }
          }
        } else {
          logger.info(s"${splitFile.uploadName} $ichunk has not been handled yet, but is closed, ignoring")
        }
      }
    } else {
      logger.info(s"${splitFile.uploadName} is fully closed, ignoring")
    }
  }

  /**
   * handle small uploads
   */
  def readSmallUploads(inF: java.io.File, forceOpen: Boolean = false, basePath: Option[Path],
    verify: Boolean = true): Unit = {
    val lines = scala.io.Source.fromFile(inF)(scala.io.Codec.UTF8).getLines()
    for ((line, iline) <- lines.zipWithIndex) {
      RepoInjection.parseDuLine(line) match {
        case Some(RepoInjection.DuLine(path, size)) =>
          val absPath = basePath match {
            case Some(b) => b.resolve(path).normalize()
            case None => path
          }
          val splitFile = RepoInjection.SplitFile(
            uploadPath = absPath,
            uploadName = absPath.getFileName().toString(),
            splits = Seq(RepoInjection.SplitElement(
              relativePath = "/",
              contents = Seq()
            )),
            complete = true
          )
          if (verify)
            splitFile.verify(this)
          checkSplitFile(
            splitFile,
            forceOpen = forceOpen
          )
        case None =>
          if ("^\\s*$".r.findFirstIn(line).isEmpty) {
            logger.warn(s"$inF skipping line ${iline + 1}: '$line'")
          }
      }
    }
  }

  /**
   * handle split uploads
   */
  def readSplit(inF: java.io.File, forceOpen: Boolean = false, defaultRepoUid: Option[Int] = None,
    verify: Boolean = true,
    fromChunk: Int = 0, untilChunk: Int = Int.MaxValue): Unit = {
    val splitFile = RepoInjection.SplitFile(inF)
    if (verify)
      splitFile.verify(this)
    checkSplitFile(
      splitFile,
      forceOpen = forceOpen,
      fromChunk = fromChunk,
      untilChunk = untilChunk
    )
  }
}
