/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection

import java.io.{ BufferedInputStream, File, FileInputStream, InputStream }

import gov.loc.repository.bagit.BagFile
import org.apache.commons.compress.compressors.bzip2

/**
 * BagFile from a compressed Bz2 file; Uses commonscompress to uncompress
 *
 * @param filepath: Path of the file
 * @param file: The compressed file
 */
class Bz2BagFile(val filepath: String, val file: File) extends BagFile {
  private var size: Long = -1
  def newInputStream: InputStream = {
    try {

      val bZip2CompressorInputStream = new bzip2.BZip2CompressorInputStream(new FileInputStream(this.file), false)
      return new BufferedInputStream(bZip2CompressorInputStream)
    } catch {
      case ex: Exception => {
        throw new RuntimeException(ex)
      }
    }
  }

  def getFilepath: String = {
    bzip2.BZip2Utils.getUncompressedFilename(this.filepath)
  }

  def exists: Boolean = {
    if (this.file != null && this.file.exists) {
      return true
    }
    return false
  }

  def getSize: Long = {
    if (this.exists) {
      if (size == -1) {
        //        println("Calculating size now!")
        val tempfis: InputStream = newInputStream
        size = BagitManager.sizeOfStream(tempfis)
        //        println(s"Raw Size: $size")
      }
      return size
    }
    return 0L
  }
}
