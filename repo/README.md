Interface between the [NOMAD repository](http://nomad-repository.eu/) and the [NOMAD laboratory](http://nomad-lab.eu/).

This builds the raw data with reasonably sized archives identified with a unique id, and keeps the connection with the repository files.
