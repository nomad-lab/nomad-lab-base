/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import org.{ json4s => jn }

object CalculationUserData {

  val sectionName = "section_repository_userdata"

  /**
   * definition of the user editable fields contained in a calculation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.dateField("repository_open_date").format("epoch_millis"),
        ES.keywordField("repository_access_now"),
        ES.textField("repository_comment"),
        Citation.fieldDefinition,
        Author.fieldDefinition,
        SharedWith.fieldDefinition,
        DataSet.fieldDefinition
      )
    )
  }
}

/**
 * Part of the information on a calculation that can be modified by the user
 */
case class CalculationUserData(
    repository_open_date: Option[Long], //milliseconds since Epoch, not yet truly supported in DB
    repository_access_now: AccessLevel.Value,
    section_citation: Seq[Citation],
    section_author_info: Seq[Author],
    section_shared_with: Seq[SharedWith],
    repository_comment: Option[String],
    section_repository_dataset: Seq[DataSet]
) extends MappableBaseValue with Section {

  override def typeStr: String = CalculationUserData.sectionName

  //FIXME: choose an ID
  override def idStr: String = ???

  override def toMap: Map[String, Any] = {
    Map(
      "repository_open_date" -> repository_open_date,
      "repository_access_now" -> repository_access_now,
      "repository_comment" -> repository_comment,
      Citation.sectionName -> section_citation.map(_.filteredMap),
      Author.sectionName -> section_author_info.map(_.filteredMap),
      SharedWith.sectionName -> section_shared_with.map(_.filteredMap),
      DataSet.sectionName -> section_repository_dataset.map(_.filteredMap)
    )
  }

  override def valuesMap = Map(
    "repository_open_date" -> Seq(repository_open_date),
    "repository_access_now" -> Seq(repository_access_now),
    "repository_comment" -> Seq(repository_comment)
  )

  override def sectionMap = Map(
    Citation.sectionName -> section_citation,
    Author.sectionName -> section_author_info,
    SharedWith.sectionName -> section_shared_with,
    DataSet.sectionName -> section_repository_dataset
  )
}
