/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.objects.AccessLevel.AccessLevel
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

object Citation {

  val sectionName: String = "section_citation"

  /**
   * definition of the field types used in the ElasticSearch representation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.intField("citation_repo_id"),
        ES.keywordField("citation_value")
      )
    )
  }
}

/**
 * A reference to an article or other external resource
 */
case class Citation(
    citation_repo_id: Int,
    citation_value: String
) extends MappableBaseValue with Section {
  override def typeStr: String = Citation.sectionName

  override def idStr: String = citation_repo_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "citation_repo_id" -> citation_repo_id,
      "citation_value" -> citation_value
    )
  }

  override def valuesMap = Map(
    "citation_repo_id" -> Seq(citation_repo_id),
    "citation_value" -> Seq(citation_value)
  )

  override def sectionMap = Map()
}

/**
 * This class represents a citation as a search response, while
 * [[eu.nomad_lab.repo.objects.Citation]] is the representation of a citation as part of the
 * calculation data structure.
 * @param citation the actual citation
 * @param documentAccessLevels maps access levels to the number of contained calculations
 * @param numDocuments number of calculations using this citation
 */
case class CitationResult(citation: Citation, documentAccessLevels: Map[AccessLevel, Int],
    numDocuments: Int) extends BaseValue {

  override def typeStr: String = "CitationResult"

  override def idStr: String = citation.citation_repo_id.toString

  override def attributes: Map[String, jn.JValue] = Map(
    "num_calculations" -> decompose(numDocuments),
    "access_levels" -> decompose(documentAccessLevels.map(x => x._1.toString -> x._2)),
    "section_citation" -> decompose(citation.filteredMap)
  )
}