/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.repo.db.enums.CitationKindEnum
import eu.nomad_lab.repo.db.{ Tables => REPO }
import eu.nomad_lab.repo.objects._
import org.jooq.impl.DSL._

import scala.collection.JavaConverters._

class UserMetaDataResolver(repoDb: RepoDb) extends StrictLogging {
  val unknownUploader = Uploader(-1, "Unknown", "Unknown", None)

  var cachedCitations: Map[Int, Citation] = Map()
  var cachedUploaders: Map[Int, Uploader] = Map()
  var cachedAuthor: Map[Int, Author] = Map()
  var cachedSharedWith: Map[Int, SharedWith] = Map()
  var cachedDataSets: Map[Long, DataSet] = Map()

  def citation(citation_id: Int): Citation = {
    cachedCitations.get(citation_id) match {
      case Some(c) => c
      case None =>
        val cRecord = repoDb.dbContext.select(
          REPO.CITATIONS.VALUE,
          REPO.CITATIONS.KIND
        ).from(
          REPO.CITATIONS
        ).where(
          REPO.CITATIONS.CITATION_ID.equal(citation_id)
        ).fetchOne
        if (cRecord.getValue(REPO.CITATIONS.KIND) == "INTERNAL")
          logger.error(s"requested citation ID $citation_id as external reference, but is internal!")
        val newCitation = Citation(
          citation_repo_id = citation_id,
          citation_value = cRecord.getValue(REPO.CITATIONS.VALUE)
        )
        cachedCitations += (citation_id -> newCitation)
        newCitation
    }
  }

  def uploader(uploader_id: Int): Uploader = {
    if (uploader_id < 0) {
      unknownUploader
    } else {
      cachedUploaders.get(uploader_id) match {
        case Some(u) => u
        case None =>
          val r = repoDb.dbContext.select(
            REPO.USERS.FIRSTNAME, REPO.USERS.LASTNAME, REPO.USERS.USERNAME
          ).from(
            REPO.USERS
          ).where(
            REPO.USERS.USER_ID.equal(uploader_id)
          ).fetchOne()
          if (r == null) {
            unknownUploader
          } else {
            val newUploader = Uploader(uploader_id, r.getValue(REPO.USERS.FIRSTNAME),
              r.getValue(REPO.USERS.LASTNAME), Option(r.getValue(REPO.USERS.USERNAME)))
            cachedUploaders += uploader_id -> newUploader
            newUploader
          }
      }
    }
  }

  def author(author_id: Int): Author = {
    cachedAuthor.get(author_id) match {
      case Some(a) => a
      case None =>
        val u = uploader(author_id)
        val newA = Author(author_id, u.uploader_first_name, u.uploader_last_name,
          s"${u.uploader_first_name} ${u.uploader_last_name}")
        cachedAuthor += author_id -> newA
        newA
    }
  }

  def sharedWith(shared_with_id: Int): SharedWith = {
    cachedSharedWith.get(shared_with_id) match {
      case Some(a) => a
      case None =>
        val u = uploader(shared_with_id)
        val newA = SharedWith(shared_with_id, u.uploader_first_name, u.uploader_last_name,
          u.uploader_username.getOrElse(""))
        cachedSharedWith += shared_with_id -> newA
        newA
    }
  }

  def dataSets(calc_id: Long, dataSetIds: Seq[Long]): Seq[DataSet] = {
    var toFetch: Set[Long] = dataSetIds.filter { x: Long =>
      cachedDataSets.get(x) match {
        case Some(_) => false
        case None => true
      }
    }.toSet
    if (toFetch.nonEmpty) {
      var doiMapping: Map[Long, Seq[(Long, String)]] = Map()
      val dois = repoDb.dbContext.select(
        REPO.METADATA_CITATIONS.CALC_ID,
        REPO.CITATIONS.CITATION_ID,
        REPO.CITATIONS.VALUE
      ).from(
        REPO.METADATA_CITATIONS
      ).join(REPO.CITATIONS).on(
        REPO.CITATIONS.CITATION_ID.eq(REPO.METADATA_CITATIONS.CITATION_ID)
      ).where(
          REPO.CITATIONS.KIND.equal(CitationKindEnum.INTERNAL),
          REPO.METADATA_CITATIONS.CALC_ID.in(toFetch.asJava)
        ).fetchLazy()
      try {
        for (r <- dois.asScala) {
          val dataSetId = r.getValue(REPO.METADATA_CITATIONS.CALC_ID).toLong
          val doi = r.getValue(REPO.CITATIONS.VALUE)
          val internalId = r.getValue(REPO.CITATIONS.CITATION_ID).toLong
          val previous = doiMapping.getOrElse(dataSetId, Seq[(Long, String)]())
          doiMapping += dataSetId -> (previous :+ (internalId, doi))
        }
      } finally {
        dois.close()
      }

      var childToParents: Map[Long, Seq[Long]] = Map()
      val cs = REPO.CALCSETS.as("cs")
      val datasetPairIds = repoDb.dbContext.withRecursive("q").as(
        repoDb.dbContext.select(
          cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
        ).from(cs).where(
          cs.CHILDREN_CALC_ID.equal(new Integer(calc_id.toInt))
        ).unionAll(
            repoDb.dbContext.select(
              cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
            ).from(
              table(name("q")).
                join(cs).on(cs.CHILDREN_CALC_ID.equal(field[Integer](name("q", "parent_calc_id"), classOf[Integer])))
            )
          )
      ).select(
          field[Integer](name("q", "parent_calc_id"), classOf[Integer]),
          field[Integer](name("q", "children_calc_id"), classOf[Integer])
        ).from(
            table(name("q"))
          ).fetchLazy()
      try {
        for (r <- datasetPairIds.asScala) {
          val pNow = r.getValue(cs.PARENT_CALC_ID).toLong
          if (!cachedDataSets.contains(pNow))
            toFetch += pNow
          val child = r.getValue(cs.CHILDREN_CALC_ID).toLong
          val parent = r.getValue(cs.PARENT_CALC_ID).toLong
          childToParents += (child -> (childToParents.getOrElse(child, Seq()) :+ parent))
        }
        val datasets = repoDb.dbContext.select(
          REPO.CALCULATIONS.CALC_ID,
          REPO.CALCULATIONS.CHECKSUM,
          REPO.METADATA.CHEMICAL_FORMULA
        ).from(
          REPO.CALCULATIONS.
            join(REPO.METADATA).on(REPO.CALCULATIONS.CALC_ID.equal(REPO.METADATA.CALC_ID))
        ).where(
            REPO.CALCULATIONS.CALC_ID.in(toFetch.asJava)
          ).fetchLazy()
        try {
          for (d <- datasets.asScala) {
            val calcId = d.getValue(REPO.CALCULATIONS.CALC_ID).toLong
            val parentCalcIds = childToParents.getOrElse(calcId, Seq())
            val dois = doiMapping.getOrElse(calcId, Seq()).map(x => DataSetDoi(x._2, x._1))
            val newDataset = objects.DataSet(
              dataset_checksum = d.getValue(REPO.CALCULATIONS.CHECKSUM),
              dataset_calc_id = calcId,
              dataset_name = Some(d.getValue(REPO.METADATA.CHEMICAL_FORMULA)),
              dataset_pid = DataSet.dataset_pid(calcId),
              dataset_parent_calc_id = parentCalcIds,
              dataset_parent_pid = parentCalcIds.map(DataSet.dataset_pid),
              section_dataset_doi = dois
            )
            cachedDataSets += (calcId -> newDataset)
          }
        } finally {
          datasets.close()
        }
      } finally {
        datasetPairIds.close()
      }
    }
    dataSetIds.map { x: Long => cachedDataSets(x) }
  }

}