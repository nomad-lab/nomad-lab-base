/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.{ ElasticDsl => dsl }
import com.sksamuel.elastic4s.http.index.CreateIndexResponse
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.db.{ Tables => REPO }
import org.elasticsearch.action.support.WriteRequest

import collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future, TimeoutException }

object TopicElasticInserter {
  def apply(repoDb: RepoDb, es: ESManager, indexName: Option[String]): TopicElasticInserter = {
    new TopicElasticInserter(
      repoDb = repoDb,
      client = es.client,
      indexName = indexName.getOrElse(es.settings.indexNameTopics),
      nCachedTopics = es.settings.nCachedCalculations,
      verbose = es.settings.verbose,
      waitTime = es.settings.waitTime
    )
  }
}

/**
 * Inserts topics into the elastic search auxiliary index
 */
class TopicElasticInserter(
    val repoDb: RepoDb,
    val client: HttpClient,
    val indexName: String,
    val nCachedTopics: Int,
    var verbose: Boolean = false,
    val waitTime: Duration = Duration(10, SECONDS)
) extends StrictLogging {

  def indexTopics(minIndex: Int): Unit = {
    val auxData = new UserMetaDataResolver(repoDb)
    val tps = repoDb.dbContext.select(
      REPO.TOPICS.TOPIC, REPO.TOPICS.TID, REPO.TOPICS.CID
    ).from(
      REPO.TOPICS
    ).where(
      REPO.TOPICS.TID.ge(new Integer(minIndex))
    ).fetch()
    val orders = tps.asScala.map { r =>
      val tmp = Topic(
        r.getValue(REPO.TOPICS.TOPIC),
        r.getValue(REPO.TOPICS.CID).toInt,
        r.getValue(REPO.TOPICS.TID).toInt
      )
      val topic = if (Topic.keyToCids("section_author_info").contains(tmp.repository_topic_category_id)) {
        val author = auxData.author(tmp.userId)
        tmp.copy(repository_topic_name = s"${author.author_name}" +
          s"<sup class=hdn>${author.author_repo_id}</sup>")
      } else
        tmp
      indexInto(indexName, Topic.sectionName).fields(topic.toMap).id(topic.repository_topic_id)
    }
    if (orders.isEmpty) {
      return
    }
    val insertInfo = client.execute(bulk(orders).refresh(WriteRequest.RefreshPolicy.WAIT_UNTIL))
    insertInfo.onComplete {
      case util.Failure(e) => logger.warn(s"Elastic search insertion FAILURE: $e")
      case util.Success(s) =>
        if (s.errors)
          logger.warn(s"Had errors in batch: ${s.items.mkString("\n  ")}")
    }
    while (!insertInfo.isCompleted) {
      try {
        Await.ready(insertInfo, waitTime)
      } catch {
        case e: TimeoutException =>
          logger.warn(s"pending indexing op not finished in $waitTime, waiting more...")
      }
    }
  }

  /**
   * creates an index for the topics data
   */
  def createIndex(index: String): Future[CreateIndexResponse] = {
    client.execute(dsl.createIndex(index).mappings(Topic.mapping).shards(1))
  }
}
