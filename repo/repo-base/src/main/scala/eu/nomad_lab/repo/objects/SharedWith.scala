/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import org.{ json4s => jn }

object SharedWith {

  val sectionName: String = "section_shared_with"

  /**
   * definition of the field types used in the ElasticSearch representation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.keywordField("shared_with_repo_id"),
        ES.keywordField("shared_with_first_name"),
        ES.keywordField("shared_with_last_name"),
        ES.keywordField("shared_with_username"),
        ES.textField("shared_with_name")
      )
    )
  }
}

case class SharedWith(
    shared_with_repo_id: Int,
    shared_with_first_name: String,
    shared_with_last_name: String,
    shared_with_name: String,
    shared_with_username: Option[String] = None
) extends MappableBaseValue with Section {
  override def typeStr: String = SharedWith.sectionName

  override def idStr: String = shared_with_repo_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "shared_with_repo_id" -> shared_with_repo_id,
      "shared_with_first_name" -> shared_with_first_name,
      "shared_with_last_name" -> shared_with_last_name,
      "shared_with_username" -> shared_with_username,
      "shared_with_name" -> shared_with_name
    )
  }

  override def valuesMap = Map(
    "shared_with_repo_id" -> Seq(shared_with_repo_id),
    "shared_with_first_name" -> Seq(shared_with_first_name),
    "shared_with_last_name" -> Seq(shared_with_last_name),
    "shared_with_username" -> Seq(shared_with_username),
    "shared_with_name" -> Seq(shared_with_name)
  )

  override def sectionMap = Map()
}
