/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.jsonapi.{ BaseValue, Relationship, ToMany }
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

@deprecated("superseded by DataSet", " API version 0.1")
object DataSetLegacy {

  def apply(sample: DataSetResult): DataSetLegacy = {
    DataSetLegacy(
      dataSetId = sample.dataSet.dataset_calc_id,
      numDocuments = Some(sample.numDocuments),
      dataSetName = sample.dataSet.dataset_name,
      parentDataSetIds = sample.dataSet.dataset_parent_calc_id,
      containedDocuments = Seq()
    )
  }
}

@deprecated("superseded by DataSet", " API version 0.1")
case class DataSetLegacy(
    dataSetId: Long,
    numDocuments: Option[Long] = None,
    dataSetName: Option[String] = None,
    parentDataSetIds: Seq[Long] = Seq(),
    containedDocuments: Seq[CalculationLegacy] = Seq()
) extends BaseValue {
  override def typeStr: String = "DataSet"

  override def idStr: String = {
    dataSetId.toString
  }

  override def attributes: Map[String, jn.JValue] = {
    val m = Map(
      "dataSetId" -> decompose(dataSetId),
      "numDocuments" -> decompose(numDocuments),
      "dataSetName" -> decompose(dataSetName)
    )
    if (parentDataSetIds.nonEmpty) {
      m + ("parentDataSetIds" -> decompose(parentDataSetIds))
    } else m
  }

  override def includedRelationships: Map[String, Relationship[BaseValue]] = {
    Map(
      "containedDocuments" -> ToMany(containedDocuments)
    )
  }
}