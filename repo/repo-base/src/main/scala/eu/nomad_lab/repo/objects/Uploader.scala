/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import org.{ json4s => jn }

object Uploader {

  val sectionName: String = "section_uploader_info"

  /**
   * definition of the field types used in the ElasticSearch representation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.keywordField("uploader_repo_id"),
        ES.keywordField("uploader_first_name"),
        ES.keywordField("uploader_last_name"),
        ES.keywordField("uploader_username"),
        ES.textField("uploader_name")
      )
    )
  }
}

case class Uploader(
    uploader_repo_id: Int,
    uploader_first_name: String,
    uploader_last_name: String,
    uploader_username: Option[String] = None,
    uploader_name: Option[String] = None
) extends MappableBaseValue with Section {
  override def typeStr: String = Uploader.sectionName

  override def idStr: String = uploader_repo_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "uploader_repo_id" -> uploader_repo_id,
      "uploader_first_name" -> uploader_first_name,
      "uploader_last_name" -> uploader_last_name,
      "uploader_username" -> uploader_username,
      "uploader_name" -> uploader_name
    )
  }

  override def valuesMap = Map(
    "uploader_repo_id" -> Seq(uploader_repo_id),
    "uploader_first_name" -> Seq(uploader_first_name),
    "uploader_last_name" -> Seq(uploader_last_name),
    "uploader_username" -> Seq(uploader_username),
    "uploader_name" -> Seq(uploader_name)
  )

  override def sectionMap = Map()
}
