/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import org.{ json4s => jn }

object CalculationParserData {

  val sectionName = "section_repository_parserdata"

  /**
   * definition of the fields extracted by the parsers
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.keywordField("repository_checksum"),
        ES.keywordField("repository_chemical_formula"),
        ES.keywordField("repository_parser_id"),
        ES.keywordField("repository_atomic_elements").stored(true),
        ES.intField("repository_atomic_elements_count").stored(true),
        ES.keywordField("repository_basis_set_type").stored(true),
        ES.keywordField("repository_code_version").stored(true),
        ES.keywordField("repository_crystal_system").stored(true),
        ES.keywordField("repository_program_name").stored(true),
        ES.keywordField("repository_spacegroup_nr").stored(true),
        ES.keywordField("repository_system_type").stored(true),
        ES.keywordField("repository_xc_treatment").stored(true)
      )
    )
  }
}

/**
 * Information on calculations extracted by the parsers
 * This information is mostly stored in the tags assigned to the calculation.
 */
case class CalculationParserData(
    repository_atomic_elements: Seq[String],
    repository_basis_set_type: Seq[String],
    repository_checksum: String,
    repository_chemical_formula: Seq[String],
    repository_code_version: Seq[String],
    repository_crystal_system: Seq[String],
    repository_parser_id: String,
    repository_program_name: Seq[String],
    repository_spacegroup_nr: Seq[Int],
    repository_system_type: Seq[String],
    repository_xc_treatment: Seq[String]
) extends MappableBaseValue with Section {

  override def typeStr: String = CalculationParserData.sectionName

  override def idStr: String = repository_checksum

  override def toMap: Map[String, Any] = {
    Map(
      "repository_checksum" -> repository_checksum,
      "repository_chemical_formula" -> repository_chemical_formula,
      "repository_parser_id" -> repository_parser_id,
      "repository_atomic_elements" -> repository_atomic_elements,
      "repository_atomic_elements_count" -> repository_atomic_elements.size,
      "repository_basis_set_type" -> repository_basis_set_type,
      "repository_code_version" -> repository_code_version,
      "repository_crystal_system" -> repository_crystal_system,
      "repository_program_name" -> repository_program_name,
      "repository_spacegroup_nr" -> repository_spacegroup_nr,
      "repository_system_type" -> repository_system_type,
      "repository_xc_treatment" -> repository_xc_treatment
    )
  }

  override def valuesMap = Map(
    "repository_checksum" -> Seq(repository_checksum),
    "repository_chemical_formula" -> repository_chemical_formula,
    "repository_parser_id" -> Seq(repository_parser_id),
    "repository_atomic_elements" -> repository_atomic_elements,
    "repository_atomic_elements_count" -> Seq(repository_atomic_elements.size),
    "repository_basis_set_type" -> repository_basis_set_type,
    "repository_code_version" -> repository_code_version,
    "repository_crystal_system" -> repository_crystal_system,
    "repository_program_name" -> repository_program_name,
    "repository_spacegroup_nr" -> repository_spacegroup_nr,
    "repository_system_type" -> repository_system_type,
    "repository_xc_treatment" -> repository_xc_treatment
  )

  override def sectionMap = Map()
}
