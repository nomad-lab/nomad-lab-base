/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.repo.db.{ Tables => REPO }
import eu.nomad_lab.repo.objects.Helpers.{ CalculationId, MainFileUri }
import eu.nomad_lab.repo.objects._
import org.jooq._
import org.jooq.impl.DSL._
import scala.collection.JavaConverters._

class CalculationUserDataLoader(private val repoDb: RepoDb) extends StrictLogging {

  type record = Record9[Array[Integer], Array[Integer], Array[Integer], Array[Integer], Array[Integer], Integer, Integer, Integer, String]

  /**
   * Fetch user-editable data for calculations that match the given condition lazily from the
   * Postgres DB. This function returns an iterator which you can either use directly or convert
   * into a collection depending on your needs.
   * @param condition the condition used to restrict the query, an additional condition to filter
   *                  datasets entries from the response is added implicitly
   * @return an iterator over the user-data for the calculations
   */
  def fetchCalculationsWithCondition(
    condition: Condition,
    batchSize: Int
  ): Iterator[Map[CalculationId, CalculationUserData]] = {
    require(batchSize > 0, "batch size must be positive")

    val fullCondition = condition.and(REPO.CALCULATIONS.CHECKSUM.notLikeRegex("[0-9A-Z]{45}MS"))

    val citation_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.METADATA_CITATIONS.CITATION_ID)
    ).from(
        REPO.METADATA_CITATIONS
      ).where(
        REPO.METADATA_CITATIONS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("citation_ids")

    val owner_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.OWNERSHIPS.USER_ID)
    ).from(
        REPO.OWNERSHIPS
      ).where(
        REPO.OWNERSHIPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("owner_ids")

    val coauthor_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.COAUTHORSHIPS.USER_ID)
    ).from(
        REPO.COAUTHORSHIPS
      ).where(
        REPO.COAUTHORSHIPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("coauthor_ids")

    val shared_with_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.SHARESHIPS.USER_ID)
    ).from(
        REPO.SHARESHIPS
      ).where(
        REPO.SHARESHIPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("shared_with_ids")

    val cs = REPO.CALCSETS.as("cs")
    val dataset_ids: Field[Array[Integer]] = repoDb.dbContext.withRecursive("q").as(repoDb.dbContext.select(
      cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
    ).from(cs).where(
      cs.CHILDREN_CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
    ).unionAll(
        repoDb.dbContext.select(
          cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
        ).from(
          table(name("q")).
            join(cs).on(cs.CHILDREN_CALC_ID.equal(field[Integer](name("q", "parent_calc_id"), classOf[Integer])))
        )
      )).select(
      arrayAgg(field[Integer](name("q", "parent_calc_id"), classOf[Integer]))
    ).from(
        table(name("q"))
      ).asField("datasets")

    val q = repoDb.dbContext.select(
      // combined fields
      citation_ids,
      coauthor_ids,
      shared_with_ids,
      dataset_ids,
      owner_ids,
      // calc
      //FIXME: replace with main file URI as soon as its part of the DB
      REPO.CALCULATIONS.CALC_ID, REPO.UPLOADS.USER_ID,
      // editableData
      REPO.USER_METADATA.PERMISSION, REPO.USER_METADATA.LABEL
    ).from(
      REPO.CALCULATIONS.
        leftOuterJoin(REPO.UPLOADS).on(REPO.CALCULATIONS.ORIGIN_ID.equal(REPO.UPLOADS.UPLOAD_ID)).
        join(REPO.METADATA).on(REPO.METADATA.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)).
        join(REPO.USER_METADATA).on(REPO.METADATA.CALC_ID.equal(REPO.USER_METADATA.CALC_ID))
    ).where(fullCondition).orderBy(REPO.CALCULATIONS.CALC_ID)

    new UserDataIterator(new UserMetaDataResolver(repoDb), q.fetchSize(batchSize).fetchLazy(),
      batchSize)
  }

  /**
   * An iterator over the user-modifiable data records in the database. Note that the next() call
   * can trigger additional database calls to fetch auxiliary data like authors and citations.
   * @param auxData cached query tool for resolving additional meta-info like citations
   * @param cursor the cursor storing the results initial query which fetches the core data
   */
  private class UserDataIterator(auxData: UserMetaDataResolver, cursor: Cursor[record],
      batchSize: Int) extends Iterator[Map[CalculationId, CalculationUserData]] {

    override def hasNext: Boolean = cursor.hasNext

    override def next(): Map[CalculationId, CalculationUserData] = {
      val results = cursor.fetch(batchSize)

      results.iterator().asScala.map { rawData =>
        val calcId = rawData.getValue(REPO.CALCULATIONS.CALC_ID).toLong
        val citationIds = Option(rawData.value1()).toSeq.flatten.map(_.toInt)
        val datasetsIds = Option(rawData.value4()).toSeq.flatten.map(_.toLong)

        val uploadOwnerId = Option(rawData.getValue(REPO.UPLOADS.USER_ID)).map(_.toInt)
        val calculationOwnerIds = Option(rawData.value5()).toSeq.flatten.map(_.toInt)
        val coAuthorIds = Option(rawData.value2()).toSeq.flatten.map(_.toInt)

        val authorIds = (uploadOwnerId ++: calculationOwnerIds ++: coAuthorIds).distinct
        val sharedWithIds = Option(rawData.value3()).toSeq.flatten.map(_.toInt)

        val accessLevel = rawData.getValue(REPO.USER_METADATA.PERMISSION) match {
          case x: Integer if x == 0 => AccessLevel.Restricted
          case x: Integer if x == 1 => AccessLevel.Open
          case x => throw new Exception(s"unexpected access level $x")
        }

        val data = CalculationUserData(
          repository_open_date = None, //FIXME: extract proper value from DB once supported
          repository_access_now = accessLevel,
          section_citation = citationIds.map(auxData.citation),
          section_author_info = authorIds.map(auxData.author),
          section_shared_with = sharedWithIds.map(auxData.sharedWith),
          repository_comment = Option(rawData.getValue(REPO.USER_METADATA.LABEL)),
          section_repository_dataset = auxData.dataSets(calcId, datasetsIds.distinct)
        )
        CalculationId(calcId) -> data
      }.toMap

    }
  }

}
