/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.jsonapi.{ BaseValue, Relationship, ToMany }
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

@deprecated("superseded by CalculationGroup", "API version 0.2")
object CalculationGroupLegacy {
  def apply(sample: CalculationGroup): CalculationGroupLegacy = {
    val refDoc = sample.sample.section_repository_info
    CalculationGroupLegacy(
      groupChecksum = refDoc.repository_grouping_checksum,
      numDocuments = Some(sample.numDocuments),
      chemicalFormula = refDoc.section_repository_parserdata.repository_chemical_formula,
      spaceGroup = refDoc.section_repository_parserdata.repository_spacegroup_nr,
      basisSet = refDoc.section_repository_parserdata.repository_basis_set_type,
      xcTreatment = refDoc.section_repository_parserdata.repository_xc_treatment,
      codeVersion = refDoc.section_repository_parserdata.repository_code_version,
      dataAccess = Some(refDoc.section_repository_userdata.repository_access_now.toString),
      references = refDoc.section_repository_userdata.section_citation.map(x => x.citation_value),
      comment = refDoc.section_repository_userdata.repository_comment,
      authors = refDoc.section_repository_userdata.section_author_info.
        filter(x => x.author_first_name.nonEmpty && x.author_last_name.nonEmpty).
        map(x => x.author_last_name + ", " + x.author_first_name),
      containedDocuments = Seq(refDoc)
    )
  }
}

/**
 * Representation of a calculation group query result, which groups all calculations together
 * that are equivalent on the subset of information shown in the repository webinterface
 *
 * @see Calculation.computeCalcGroupChecksum
 * @param groupChecksum checksum of this group
 * @param numDocuments number of documents that are considered equal
 * @param containedDocuments a list with documents from this group to extract more data
 */
@deprecated("superseded by CalculationGroup", "API version 0.2")
case class CalculationGroupLegacy(
    groupChecksum: String,
    numDocuments: Option[Long] = None,
    chemicalFormula: Seq[String] = Seq(),
    spaceGroup: Seq[Int] = Seq(),
    basisSet: Seq[String] = Seq(),
    xcTreatment: Seq[String] = Seq(),
    codeVersion: Seq[String] = Seq(),
    dataAccess: Option[String] = None,
    references: Seq[String] = Seq(),
    comment: Option[String] = None,
    authors: Seq[String] = Seq(),
    containedDocuments: Seq[Calculation] = Seq()
) extends BaseValue {
  override def typeStr: String = "calculationGroup"

  override def idStr: String = {
    groupChecksum
  }

  override def attributes: Map[String, jn.JValue] = {
    Map(
      "groupChecksum" -> decompose(groupChecksum),
      "numDocuments" -> decompose(numDocuments),
      "chemicalFormula" -> decompose(chemicalFormula),
      "spaceGroup" -> decompose(spaceGroup),
      "basisSet" -> decompose(basisSet),
      "xcTreatment" -> decompose(xcTreatment),
      "codeVersion" -> decompose(codeVersion),
      "dataAccess" -> decompose(dataAccess),
      "references" -> decompose(references),
      "comment" -> decompose(comment),
      "authors" -> decompose(authors)
    )
  }

  override def includedRelationships: Map[String, Relationship[BaseValue]] = {
    Map(
      "containedDocuments" -> ToMany(containedDocuments)
    )
  }
}