/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import eu.nomad_lab.Base32
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.objects.AccessLevel.AccessLevel
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

object DataSet {
  val datasetRe = """\A[0-9A-Z]{45}MS\Z""".r

  val sectionName: String = "section_repository_dataset"

  def isDataset(checksum: String): Boolean = datasetRe.findFirstIn(checksum).isDefined

  def dataset_pid(id: Long): String = {
    Base32.b32NrRepository(id)
  }

  /**
   * definition of the field types used in the ElasticSearch representation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.keywordField("dataset_checksum"),
        ES.keywordField("dataset_pid"),
        ES.keywordField("dataset_name"),
        ES.keywordField("dataset_parent_pid"),
        ES.longField("dataset_calc_id"),
        ES.longField("dataset_parent_calc_id"),
        DataSetDoi.fieldDefinition
      )
    )
  }
}

case class DataSet(
    dataset_checksum: String,
    dataset_calc_id: Long,
    dataset_pid: String,
    dataset_parent_calc_id: Seq[Long] = Seq(),
    dataset_parent_pid: Seq[String] = Seq(),
    dataset_name: Option[String] = None,
    section_dataset_doi: Seq[DataSetDoi] = Seq()
) extends MappableBaseValue with Section {
  override def typeStr: String = DataSet.sectionName

  override def idStr: String = dataset_calc_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "dataset_checksum" -> dataset_checksum,
      "dataset_calc_id" -> dataset_calc_id,
      "dataset_pid" -> dataset_pid,
      "dataset_name" -> dataset_name,
      DataSetDoi.sectionName -> section_dataset_doi.map(_.toMap),
      "dataset_parent_pid" -> (if (dataset_parent_pid.nonEmpty) dataset_parent_pid else
        dataset_parent_calc_id.map { Base32.b32NrRepository }),
      "dataset_parent_calc_id" -> dataset_parent_calc_id
    )
  }

  override def valuesMap = Map(
    "dataset_checksum" -> Seq(dataset_checksum),
    "dataset_calc_id" -> Seq(dataset_calc_id),
    "dataset_pid" -> Seq(dataset_pid),
    "dataset_name" -> Seq(dataset_name),
    "dataset_parent_pid" -> (if (dataset_parent_pid.nonEmpty) dataset_parent_pid else
      dataset_parent_calc_id.map { Base32.b32NrRepository }),
    "dataset_parent_calc_id" -> dataset_parent_calc_id
  )

  override def sectionMap = Map(
    DataSetDoi.sectionName -> section_dataset_doi
  )
}

/**
 * This class represents a DataSet as a search response, while
 * [[eu.nomad_lab.repo.objects.DataSet]] is the representation of a data set as part of the
 * calculation data structure.
 * @param dataSet the actual data set
 * @param documentAccessLevels maps access levels to the number of contained calculations
 * @param numDocuments number of calculations belonging to this data set
 */
case class DataSetResult(dataSet: DataSet, documentAccessLevels: Map[AccessLevel, Int],
    numDocuments: Int) extends BaseValue {

  override def typeStr: String = "DataSetResult"

  override def idStr: String = dataSet.dataset_pid

  override def attributes: Map[String, jn.JValue] = Map(
    "num_calculations" -> decompose(numDocuments),
    "access_levels" -> decompose(documentAccessLevels.map(x => x._1.toString -> x._2)),
    "section_repository_dataset" -> decompose(dataSet.filteredMap)
  )
}