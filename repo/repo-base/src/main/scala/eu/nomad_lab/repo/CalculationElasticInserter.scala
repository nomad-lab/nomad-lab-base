/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.IndexAndType
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.{ ElasticDsl => dsl }
import com.sksamuel.elastic4s.indexes.IndexDefinition
import com.sksamuel.elastic4s.http.bulk.BulkResponse
import com.sksamuel.elastic4s.http.index.CreateIndexResponse
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.elasticsearch.{ ConnectorElasticSearch, ESManager }
import eu.nomad_lab.repo.objects.{ AccessLevel, Calculation }
import org.elasticsearch.action.support.WriteRequest
import org.{ json4s => jn }

import scala.concurrent
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.duration.SECONDS
import scala.concurrent.{ Await, Future }

object CalculationElasticInserter {
  def apply(es: ESManager, indexName: Option[String]): CalculationElasticInserter = {
    new CalculationElasticInserter(
      client = es.client,
      indexName = indexName.getOrElse(es.settings.indexNameData),
      nCachedCalculations = es.settings.nCachedCalculations,
      verbose = es.settings.verbose,
      waitTime = es.settings.waitTime
    )
  }
}

/**
 * Inserts calculations into the elastic search index
 *
 * Tries to insert calculations in bulk (batches), rememer to call
 * indexCached to flush the last ones...
 */
class CalculationElasticInserter(
    val client: HttpClient,
    val indexName: String,
    val nCachedCalculations: Int,
    var verbose: Boolean = false,
    val waitTime: Duration = Duration(10, SECONDS)
) extends StrictLogging {
  var cachedCalculations: Seq[Calculation] = Seq()
  var nInserted: Long = 0
  var pendingOps: Seq[concurrent.Future[Any]] = Seq()

  def finishPending(): Unit = {
    synchronized {
      for (op <- pendingOps) {
        while (!op.isCompleted) {
          try {
            Await.ready(op, waitTime)
          } catch {
            case e: concurrent.TimeoutException =>
              logger.warn(s"pending indexing op not finished in $waitTime, waiting more...")
          }
        }
      }
      pendingOps = Seq()
    }
  }

  def indexCached(): Unit = {
    if (verbose)
      logger.info(s"will flush cached calculations to index")
    if (!cachedCalculations.isEmpty) {
      val op = bulk(
        cachedCalculations.flatMap { c: Calculation =>
          Seq(indexInto(indexName, Calculation.typeName).fields(
            Map(
              Calculation.sectionName -> c.filteredMap,
              ConnectorElasticSearch.queryAuxiliaryDataName -> Map(
                ConnectorElasticSearch.countSection -> c.toCountMap
              )
            )
          ).id(c.repository_calc_id))
        }
      ).refresh(WriteRequest.RefreshPolicy.WAIT_UNTIL)
      finishPending()
      val insertInfo = client.execute(
        op
      )
      insertInfo.onComplete {
        case util.Failure(e) => logger.warn(s"Elastic serach insertion FAILURE: $e")
        case util.Success(s) =>
          if (s.errors)
            logger.warn(s"Had errors in batch: ${s.items.mkString("\n  ")}")
      }
      synchronized {
        pendingOps = pendingOps :+ insertInfo
      }
      nInserted += cachedCalculations.size.toLong
    }
    cachedCalculations = Seq()
    if (verbose)
      logger.info(s"did flush cached calculations to index")
  }

  def addCalculation(calc: Calculation): Unit = {
    if (verbose)
      logger.debug(s"adding calculation ${calc.repository_calc_id} (${calc.toMap})")
    cachedCalculations = cachedCalculations :+ calc
    if (cachedCalculations.size >= nCachedCalculations)
      indexCached()
  }

  /**
   * creates an index for the calculation data
   */
  def createIndex(index: String): Future[CreateIndexResponse] = {
    client.execute(dsl.createIndex(index).mappings(Calculation.mapping).analysis(Calculation.analysis))
  }
}
