/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.repo

import com.sksamuel.elastic4s.IndexAndType
import com.sksamuel.elastic4s.http.ElasticDsl._
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.db.{ Tables => REPO }
import eu.nomad_lab.repo.objects.Calculation
import eu.nomad_lab.repo.objects.Helpers.CalculationId
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.JValue
import org.{ json4s => jn }

import scala.collection.JavaConverters._
import scala.concurrent.{ ExecutionContext, Future }

class ElasticSearchIngestion(
    private val repoDb: RepoDb,
    private val es: ESManager,
    private val indexName: String
)(implicit private val exec: ExecutionContext) {

  private val index = IndexAndType(indexName, es.settings.typeNameData)

  def updateCalculationUserDataByCalcId(calcIds: Seq[CalculationId]): Future[(UpsertResponse, Map[String, jn.JValue])] = {
    val condition = REPO.CALCULATIONS.CALC_ID.in(calcIds.map(_.id).asJava)
    val loader = new CalculationUserDataLoader(repoDb)
    val elasticLoader = new ElasticSearchCalculationFetcher(es)
    val dataIterator = loader.fetchCalculationsWithCondition(
      condition,
      es.settings.nCachedCalculations
    )
    val start = Future.successful((Seq[String](), Seq[String](), 0l))

    val batchResponse = dataIterator.foldLeft(start) { (previous, batch) =>
      previous.flatMap { tuple =>
        val (success, failed, time) = tuple

        val entries = elasticLoader.getCalculations(batch.keys.map { _.id.toString }.toSeq)

        val requests = entries.map { response =>
          val documents = response._1
          documents.flatMap { calculation =>
            val key = calculation.section_repository_info.repository_calc_id
            val parserData = calculation.section_repository_info.section_repository_parserdata
            batch.get(CalculationId(key)) match {
              case None => None
              case Some(userData) =>
                val newChecksum = Calculation.groupingChecksum(parserData, userData)
                val newCalculation = calculation.section_repository_info.copy(
                  section_repository_userdata = userData,
                  repository_grouping_checksum = newChecksum
                )
                val fields = Map(Calculation.sectionName -> newCalculation.filteredMap)
                //FIXME: should use the MainFileUri as key in the future
                Some(indexInto(index).fields(fields).id(key))
            }
          }
        }
        requests.flatMap { commands =>
          if (commands.nonEmpty) {
            es.client.execute(bulk(commands).refresh(RefreshPolicy.WAIT_UNTIL))
          }.map { response =>
            (success ++ response.successes.map(_.id), failed ++ response.failures.map(_.id),
              time + response.took)
          }
          else {
            Future(success, failed, time)
          }
        }
      }
    }
    batchResponse.map { x =>
      val notFound = calcIds.filter { calcId =>
        !x._1.contains(calcId.id.toString) && !x._2.contains(calcId.id.toString)
      }.map(_.id.toString)
      val upsert = UpsertResponse(parserData = false, updated = x._1, failed = x._2,
        not_found = notFound)
      val meta = Map(
        "took" -> decompose(x._3),
        "has_failures" -> decompose(upsert.failed.nonEmpty)
      )
      (upsert, meta)
    }
  }
}

case class UpsertResponse(parserData: Boolean, updated: Seq[String], failed: Seq[String],
    not_found: Seq[String]) extends BaseValue {

  override def typeStr = "UpsertResponse"

  override def idStr = s"${if (parserData) "create" else "update"} " +
    s"${updated.length + failed.length} requests"

  override def attributes: Map[String, JValue] = Map(
    "updated" -> decompose(updated),
    "failed" -> decompose(failed),
    "not_found" -> decompose(not_found)
  )
}