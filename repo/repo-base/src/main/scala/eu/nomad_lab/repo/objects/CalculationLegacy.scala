/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import eu.nomad_lab.CompactSha
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.jsonapi.{ BaseValue, Relationship, ToMany }
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

@deprecated("superseded by Calculation class", "API version 0.2")
object CalculationLegacy {

  def apply(calc: Calculation): CalculationLegacy = {
    val basePath = calc.repository_filepaths.headOption.map { path =>
      if (path.startsWith("$EXTRACTED")) {
        path.drop("$EXTRACTED".length)
      } else {
        path.indexOfSlice("/extracted/") match {
          case -1 => path
          case n => path.drop(n + "/extracted".length)
        }
      }
    }

    CalculationLegacy(
      repository_uri = basePath.map("nmd-arc:/" + _),
      repository_pid = Some(calc.repository_calc_pid),
      repository_calc_id = Some(calc.repository_calc_id),
      upload_repository_id = Some(calc.upload_id),
      repository_nomad_uris = calc.main_file_uri ++: calc.secondary_file_uris,
      repository_archive_gid = calc.repository_archive_gid,
      upload_date = calc.upload_date,
      section_uploader_info = Seq(calc.section_uploader_info),
      repository_grouping_checksum = Some(calc.repository_grouping_checksum),
      repository_location = basePath.map { x => x.drop(1).drop(x.substring(1).indexOf("/") + 1) },
      repository_filepaths = calc.repository_filepaths,
      repository_checksum = Some(calc.section_repository_parserdata.repository_checksum),
      repository_chemical_formula = calc.section_repository_parserdata.repository_chemical_formula,
      repository_parser_id = Some(calc.section_repository_parserdata.repository_parser_id),
      repository_atomic_elements = calc.section_repository_parserdata.repository_atomic_elements,
      repository_basis_set_type = calc.section_repository_parserdata.repository_basis_set_type,
      repository_code_version = calc.section_repository_parserdata.repository_code_version,
      repository_crystal_system = calc.section_repository_parserdata.repository_crystal_system,
      repository_program_name = calc.section_repository_parserdata.repository_program_name,
      repository_spacegroup_nr = calc.section_repository_parserdata.repository_spacegroup_nr,
      repository_system_type = calc.section_repository_parserdata.repository_system_type,
      repository_xc_treatment = calc.section_repository_parserdata.repository_xc_treatment,
      repository_open_date = calc.section_repository_userdata.repository_open_date,
      repository_access_now = Some(calc.section_repository_userdata.repository_access_now.toString),
      repository_comment = calc.section_repository_userdata.repository_comment,
      section_citation = calc.section_repository_userdata.section_citation,
      section_author_info = calc.section_repository_userdata.section_author_info,
      section_shared_with = calc.section_repository_userdata.section_shared_with,
      section_repository_dataset = calc.section_repository_userdata.section_repository_dataset
    )
  }
}

@deprecated("superseded by Calculation class", "API version 0.2")
case class CalculationLegacy(
    repository_uri: Option[String] = None,
    repository_pid: Option[String] = None,
    repository_calc_id: Option[Long] = None,
    upload_repository_id: Option[Long] = None,
    repository_nomad_uris: Seq[String] = Seq(),
    repository_archive_gid: Seq[String] = Seq(),
    upload_date: Option[Long] = None,
    section_uploader_info: Seq[Uploader] = Seq(),
    repository_grouping_checksum: Option[String] = None,
    repository_location: Option[String] = None,
    repository_filepaths: Seq[String] = Seq(),
    repository_checksum: Option[String] = None,
    repository_chemical_formula: Seq[String] = Seq(),
    repository_parser_id: Option[String] = None,
    repository_atomic_elements: Seq[String] = Seq(),
    repository_basis_set_type: Seq[String] = Seq(),
    repository_code_version: Seq[String] = Seq(),
    repository_crystal_system: Seq[String] = Seq(),
    repository_program_name: Seq[String] = Seq(),
    repository_spacegroup_nr: Seq[Int] = Seq(),
    repository_system_type: Seq[String] = Seq(),
    repository_xc_treatment: Seq[String] = Seq(),
    repository_open_date: Option[Long] = None,
    repository_access_now: Option[String] = None,
    repository_comment: Option[String] = None,
    section_citation: Seq[Citation] = Seq(),
    section_author_info: Seq[Author] = Seq(),
    section_shared_with: Seq[SharedWith] = Seq(),
    section_repository_dataset: Seq[DataSet] = Seq()
) extends BaseValue {
  override def typeStr: String = "section_repository_info"

  override def idStr: String = {
    repository_uri match {
      case Some(x) =>
        val s = CompactSha(); s.updateStr(x); s.gidStr("r")
      case None => repository_calc_id.getOrElse(-1).toString
    }
  }

  override def attributes: Map[String, jn.JValue] = {
    Map(
      "repository_uri" -> decompose(repository_uri),
      "repository_pid" -> decompose(repository_pid),
      "repository_calc_id" -> decompose(repository_calc_id),
      "upload_repository_id" -> decompose(upload_repository_id),
      "repository_nomad_uris" -> decompose(repository_nomad_uris),
      "repository_archive_gid" -> decompose(repository_archive_gid),
      "upload_date" -> decompose(upload_date),
      "repository_location" -> decompose(repository_location),
      "repository_filepaths" -> decompose(repository_filepaths),
      "repository_grouping_checksum" -> decompose(repository_grouping_checksum),
      "repository_checksum" -> decompose(repository_checksum),
      "repository_chemical_formula" -> decompose(repository_chemical_formula),
      "repository_parser_id" -> decompose(repository_parser_id),
      "repository_atomic_elements" -> decompose(repository_atomic_elements),
      "repository_basis_set_type" -> decompose(repository_basis_set_type),
      "repository_code_version" -> decompose(repository_code_version),
      "repository_crystal_system" -> decompose(repository_crystal_system),
      "repository_program_name" -> decompose(repository_program_name),
      "repository_spacegroup_nr" -> decompose(repository_spacegroup_nr),
      "repository_system_type" -> decompose(repository_system_type),
      "repository_xc_treatment" -> decompose(repository_xc_treatment),
      "repository_open_date" -> decompose(repository_open_date),
      "repository_access_now" -> decompose(repository_access_now),
      "repository_comment" -> decompose(repository_comment),
      "references" -> decompose(section_citation.map(x => x.citation_value)),
      "authors" -> decompose(section_author_info.
        filter(x => x.author_first_name.nonEmpty && x.author_last_name.nonEmpty).
        map(x => x.author_last_name + ", " + x.author_first_name))
    )
  }

  override def includedRelationships: Map[String, Relationship[BaseValue]] = {
    Map(
      "section_uploader_info" -> ToMany(section_uploader_info),
      "section_citation" -> ToMany(section_citation),
      "section_author_info" -> ToMany(section_author_info),
      "section_shared_with" -> ToMany(section_shared_with),
      "section_repository_dataset" -> ToMany(section_repository_dataset)
    )
  }
}

