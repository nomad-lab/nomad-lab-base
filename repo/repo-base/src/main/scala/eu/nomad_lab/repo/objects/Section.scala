package eu.nomad_lab.repo.objects

trait Section {

  def typeStr: String

  def valuesMap: Map[String, Seq[Any]]

  def sectionMap: Map[String, Seq[Section]]

  def toCountMap: Map[String, Any] = {
    def uniqueValueMap(current: Section, basePath: Seq[String]): Map[Seq[String], Set[Any]] = {
      val directValues = current.valuesMap.map { x =>
        (basePath :+ x._1) -> x._2.filter(_ != None).toSet
      }
      val subValues = current.sectionMap.map { entry =>
        val rawResults = entry._2.map(uniqueValueMap(_, basePath :+ entry._1))
        val allKeys = rawResults.flatMap { _.keySet }
        allKeys.map { key =>
          val allValues = rawResults.foldLeft(Set[Any]())((result, values) =>
            result ++ values.getOrElse(key, Set()))
          key -> allValues
        }.toMap
      }
      subValues.foldLeft(directValues)((result, values) => result ++ values)
    }
    def nestFlatMap(flatMap: Map[Seq[String], Int]): Map[String, Any] = {
      val partition = flatMap.groupBy(x => x._1.head)
      partition.map { entry =>
        val (prefix, subValues) = entry
        val (direct, children) = subValues.span(x => x._1.size == 1)
        direct.size match {
          case 1 => prefix -> direct.head._2
          case _ => prefix -> nestFlatMap(children.map(x => x._1.tail -> x._2))
        }
      }
    }

    val uniques = uniqueValueMap(this, Seq(typeStr))
    val counts = uniques.map { x => x._1 -> x._2.size }
    nestFlatMap(counts)
  }

}