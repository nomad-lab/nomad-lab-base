/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import com.sksamuel.elastic4s.mappings.{ FieldDefinition, MappingDefinition }
import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }

/**
 * Topics are grouped using the cid.
 * The cid in the databases are the following:
 *
 *  cid  | count
 * ------+-------
 *   100 |     4 material class, discarded
 *   220 |    24  program_name
 *   703 |     7 ion, discarded
 *    10 |    93 atoms
 *   701 |    26 ammino, discarded
 *    75 |    12 xc
 *   702 |     2 capping, discarded
 *    90 |     7 crystal system
 *    80 |     9 basis_type
 *  1996 |   303 author
 *    50 |     5 ndim
 *  1994 |     2 access (duplicate)
 *
 * The ones used are stored in keyToCid and cidToKey
 *
 * Values (topic) can also be renamed in elastic search wrt. the database.
 * This mapping is stored in tagRenaming (DB -> ES) and tagRestore (ES -> DB)
 */
object Topic {

  val sectionName = "section_repository_topic"

  /**
   * definition of the field contained in a topic definition
   */
  def fieldDefinitions: Seq[FieldDefinition] = {
    Seq(
      ES.longField("repository_topic_id").stored(true),
      ES.longField("repository_topic_category_id").stored(true),
      ES.keywordField("repository_topic_category").stored(true),
      ES.keywordField("repository_topic_name").stored(true)
    )
  }

  /**
   * Mapping (index definition) of a topic
   */
  def mapping: MappingDefinition = {
    ES.mapping(sectionName).fields(fieldDefinitions)
  }

  val tagRenaming: Map[String, String] = Map(
    "1D" -> "1D",
    "0D" -> "Molecule / Cluster",
    "atom" -> "Atom",
    "2D" -> "Surface / Adsorption",
    "3D" -> "Bulk",
    "open access" -> "Open",
    "restricted" -> "Restricted"
  )

  val tagRestore: Map[String, String] = tagRenaming.map { case (k, v) => v -> k }

  val keyToCids: Map[String, Seq[Int]] = Map(
    "repository_atomic_elements" -> Seq(10),
    "repository_program_name" -> Seq(220),
    "repository_xc_treatment" -> Seq(75),
    "repository_crystal_system" -> Seq(90),
    "repository_basis_set_type" -> Seq(80),
    "section_author_info" -> Seq(1996),
    "repository_system_type" -> Seq(50, 100, 701, 702, 703),
    "repository_access_now" -> Seq(1994)
  )

  val cidToKey: Map[Int, String] = for ((key, values) <- keyToCids; cid <- values) yield cid -> key

  val tagFields = Topic.keyToCids.keys.toSeq.filter(_ != "section_author_info") :+ "section_author_info.author_repo_id"
}

/**
 * Represents a tag assigned to a calculation
 * (topic conntect via the tag table in the db)
 */
case class Topic(
    repository_topic_name: String,
    repository_topic_category_id: Int,
    repository_topic_id: Int
) {

  /**
   * returns the key (metainfo) used to store this tag in elastic search
   * (if stored)
   */
  def maybeKey: Option[String] = {
    Topic.cidToKey.get(repository_topic_category_id)
  }

  /**
   * returns the value that can be used to store this tag in elastic search
   */
  def value: String = {
    Topic.tagRenaming.getOrElse(repository_topic_name, repository_topic_name)
  }

  /**
   * If this tag represents an author returns its user_id
   */
  def userId: Int = {
    if (repository_topic_category_id != 1996)
      throw new Exception(s"No userId for tag $this")
    val userIdRe = """.*<sup class=hdn>([0-9]+)</sup>""".r
    repository_topic_name match {
      case userIdRe(uidStr) =>
        uidStr.toInt
      case _ =>
        throw new Exception(s"Could not extract userId from '$repository_topic_name' for tag $this")
    }
  }

  def toMap: Map[String, Any] = {
    Map(
      "repository_topic_id" -> repository_topic_id,
      "repository_topic_category_id" -> repository_topic_category_id,
      "repository_topic_category" -> Topic.cidToKey(repository_topic_category_id),
      "repository_topic_name" -> repository_topic_name
    )
  }
}
