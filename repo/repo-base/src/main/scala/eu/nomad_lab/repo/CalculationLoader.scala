/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import java.nio.charset.StandardCharsets

import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.rawdata.RawDataMapper
import eu.nomad_lab.ref.ObjectKind
import eu.nomad_lab.repo.db.enums.CitationKindEnum
import eu.nomad_lab.repo.db.{ Tables => REPO }
import eu.nomad_lab.repo.objects._
import eu.nomad_lab.{ Base32, Composition, JsonUtils }
import org.jooq.{ Condition, Field }
import org.jooq.impl.DSL._
import org.{ json4s => jn }

import scala.collection.JavaConverters._
import scala.collection.breakOut
import scala.util.control.NonFatal

/**
 * Object that loads Calculations from the repository database
 */
class CalculationLoader(
    val repoDb: RepoDb,
    val rawDataMapper: RawDataMapper
) extends StrictLogging {
  val tagMap = new TopicMapping(repoDb)
  var cachedCitations: Map[Int, Citation] = Map()
  def citation(citation_id: Int): Citation = {
    cachedCitations.get(citation_id) match {
      case Some(c) => c
      case None =>
        val cRecord = repoDb.dbContext.select(
          REPO.CITATIONS.VALUE,
          REPO.CITATIONS.KIND
        ).from(
          REPO.CITATIONS
        ).where(
          REPO.CITATIONS.CITATION_ID.equal(citation_id)
        ).fetchOne
        if (cRecord.getValue(REPO.CITATIONS.KIND) == "INTERNAL")
          logger.error(s"requested citation ID $citation_id as external reference, but is internal!")
        val newCitation = Citation(
          citation_repo_id = citation_id,
          citation_value = cRecord.getValue(REPO.CITATIONS.VALUE)
        )
        cachedCitations += (citation_id -> newCitation)
        newCitation
    }
  }

  val unknonwUploader = Uploader(-1, "Unknown", "Unknown", None)
  var cachedUploaders: Map[Int, Uploader] = Map()

  def uploader(uploader_id: Int): Uploader = {
    if (uploader_id < 0) {
      unknonwUploader
    } else {
      cachedUploaders.get(uploader_id) match {
        case Some(u) => u
        case None =>
          val r = repoDb.dbContext.select(
            REPO.USERS.FIRSTNAME, REPO.USERS.LASTNAME, REPO.USERS.USERNAME
          ).from(
            REPO.USERS
          ).where(
            REPO.USERS.USER_ID.equal(uploader_id)
          ).fetchOne()
          if (r == null) {
            unknonwUploader
          } else {
            val newUploader = Uploader(uploader_id, r.getValue(REPO.USERS.FIRSTNAME),
              r.getValue(REPO.USERS.LASTNAME), Option(r.getValue(REPO.USERS.USERNAME)))
            cachedUploaders += uploader_id -> newUploader
            newUploader
          }
      }
    }
  }

  var cachedAuthor: Map[Int, Author] = Map()

  def author(author_id: Int): Author = {
    cachedAuthor.get(author_id) match {
      case Some(a) => a
      case None =>
        val u = uploader(author_id)
        val newA = Author(author_id, u.uploader_first_name, u.uploader_last_name,
          s"${u.uploader_first_name} ${u.uploader_last_name}")
        cachedAuthor += author_id -> newA
        newA
    }
  }

  var cachedSharedWith: Map[Int, SharedWith] = Map()

  def sharedWith(shared_with_id: Int): SharedWith = {
    cachedSharedWith.get(shared_with_id) match {
      case Some(a) => a
      case None =>
        val u = uploader(shared_with_id)
        val newA = SharedWith(shared_with_id, u.uploader_first_name, u.uploader_last_name,
          u.uploader_username.getOrElse(""))
        cachedSharedWith += shared_with_id -> newA
        newA
    }
  }

  var cachedDatasets: Map[Long, DataSet] = Map()
  def datasets(calc_id: Long, datasetIds: Seq[Long]): Seq[DataSet] = {
    var toFetch: Set[Long] = datasetIds.filter { x: Long =>
      cachedDatasets.get(x) match {
        case Some(dd) => false
        case None => true
      }
    }.toSet
    if (toFetch.nonEmpty) {
      var doiMapping: Map[Long, Seq[(Long, String)]] = Map()
      val dois = repoDb.dbContext.select(
        REPO.METADATA_CITATIONS.CALC_ID,
        REPO.CITATIONS.CITATION_ID,
        REPO.CITATIONS.VALUE
      ).from(
        REPO.METADATA_CITATIONS
      ).join(REPO.CITATIONS).on(
        REPO.CITATIONS.CITATION_ID.eq(REPO.METADATA_CITATIONS.CITATION_ID)
      ).where(
          REPO.CITATIONS.KIND.equal(CitationKindEnum.INTERNAL),
          REPO.METADATA_CITATIONS.CALC_ID.in(toFetch.asJava)
        ).fetchLazy()
      try {
        for (r <- dois.asScala) {
          val dataSetId = r.getValue(REPO.METADATA_CITATIONS.CALC_ID).toLong
          val doi = r.getValue(REPO.CITATIONS.VALUE)
          val internalId = r.getValue(REPO.CITATIONS.CITATION_ID).toLong
          val previous = doiMapping.getOrElse(dataSetId, Seq[(Long, String)]())
          doiMapping += dataSetId -> (previous :+ (internalId, doi))
        }
      } finally {
        dois.close()
      }

      var childToParents: Map[Long, Seq[Long]] = Map()
      val cs = REPO.CALCSETS.as("cs")
      val datasetPairIds = repoDb.dbContext.withRecursive("q").as(
        repoDb.dbContext.select(
          cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
        ).from(cs).where(
          cs.CHILDREN_CALC_ID.equal(new Integer(calc_id.toInt))
        ).unionAll(
            repoDb.dbContext.select(
              cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
            ).from(
              table(name("q")).
                join(cs).on(cs.CHILDREN_CALC_ID.equal(field[Integer](name("q", "parent_calc_id"), classOf[Integer])))
            )
          )
      ).select(
          field[Integer](name("q", "parent_calc_id"), classOf[Integer]),
          field[Integer](name("q", "children_calc_id"), classOf[Integer])
        ).from(
            table(name("q"))
          ).fetchLazy()
      try {
        for (r <- datasetPairIds.asScala) {
          val pNow = r.getValue(cs.PARENT_CALC_ID).toLong
          if (!cachedDatasets.contains(pNow))
            toFetch += pNow
          val child = r.getValue(cs.CHILDREN_CALC_ID).toLong
          val parent = r.getValue(cs.PARENT_CALC_ID).toLong
          childToParents += (child -> (childToParents.getOrElse(child, Seq()) :+ parent))
        }
        val datasets = repoDb.dbContext.select(
          REPO.CALCULATIONS.CALC_ID,
          REPO.CALCULATIONS.CHECKSUM,
          REPO.METADATA.CHEMICAL_FORMULA
        ).from(
          REPO.CALCULATIONS.
            join(REPO.METADATA).on(REPO.CALCULATIONS.CALC_ID.equal(REPO.METADATA.CALC_ID))
        ).where(
            REPO.CALCULATIONS.CALC_ID.in(toFetch.asJava)
          ).fetchLazy()
        try {
          for (d <- datasets.asScala) {
            val calcId = d.getValue(REPO.CALCULATIONS.CALC_ID).toLong
            val parentCalcIds = childToParents.getOrElse(calcId, Seq())
            val dois = doiMapping.getOrElse(calcId, Seq()).map(x => DataSetDoi(x._2, x._1))
            val newDataset = objects.DataSet(
              dataset_checksum = d.getValue(REPO.CALCULATIONS.CHECKSUM),
              dataset_calc_id = calcId,
              dataset_name = Some(d.getValue(REPO.METADATA.CHEMICAL_FORMULA)),
              dataset_pid = DataSet.dataset_pid(calcId),
              dataset_parent_calc_id = parentCalcIds,
              dataset_parent_pid = parentCalcIds.map(DataSet.dataset_pid),
              section_dataset_doi = dois
            )
            cachedDatasets += (calcId -> newDataset)
          }
        } finally {
          datasets.close()
        }
      } finally {
        datasetPairIds.close()
      }
    }
    datasetIds.map { x: Long => cachedDatasets(x) }
  }

  /**
   * Loops on the calculation objects constructed querying the db
   * you can give bounds, os selected calculationIds.
   * By default loops on *all* database
   */
  def loopOnCalcs(
    calcIds: Option[Seq[Long]] = None,
    calcIdMin: Option[Long] = None,
    calcIdMax: Option[Long] = None
  )(
    calcOp: Calculation => Unit
  ): Unit = {
    // select the calculations to loop on (where part)
    var selectCalc: Condition = trueCondition()
    calcIds match {
      case Some(ids) =>
        selectCalc = selectCalc.and(REPO.CALCULATIONS.CALC_ID.in(ids.asJava))
      case None => ()
    }
    calcIdMin match {
      case Some(m) =>
        selectCalc = selectCalc.and(REPO.CALCULATIONS.CALC_ID.ge(new Integer(m.toInt)))
      case None => ()
    }
    calcIdMax match {
      case Some(m) =>
        selectCalc = selectCalc.and(REPO.CALCULATIONS.CALC_ID.lt(new Integer(m.toInt)))
      case None => ()
    }
    loopOnCalcsWithCondition(selectCalc)(calcOp)
  }

  /**
   * Loops on the calculation objects constructed querying the db that satisfy the given condition
   */
  def loopOnCalcsWithCondition(condition: Condition)(calcOp: Calculation => Unit): Unit = {
    val calc_tags: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.TAGS.TID)
    ).from(
        REPO.TAGS
      ).where(
        REPO.TAGS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("calc_tags")

    val citation_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.METADATA_CITATIONS.CITATION_ID)
    ).from(
        REPO.METADATA_CITATIONS
      ).where(
        REPO.METADATA_CITATIONS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("citation_ids")

    val owner_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.OWNERSHIPS.USER_ID)
    ).from(
        REPO.OWNERSHIPS
      ).where(
        REPO.OWNERSHIPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("owner_ids")

    val coauthor_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.COAUTHORSHIPS.USER_ID)
    ).from(
        REPO.COAUTHORSHIPS
      ).where(
        REPO.COAUTHORSHIPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("coauthor_ids")

    val shared_with_ids: Field[Array[Integer]] = repoDb.dbContext.select(
      arrayAgg(REPO.SHARESHIPS.USER_ID)
    ).from(
        REPO.SHARESHIPS
      ).where(
        REPO.SHARESHIPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
      ).asField("shared_with_ids")

    val cs = REPO.CALCSETS.as("cs")
    val dataset_ids: Field[Array[Integer]] = repoDb.dbContext.withRecursive("q").as(repoDb.dbContext.select(
      cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
    ).from(cs).where(
      cs.CHILDREN_CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)
    ).unionAll(
        repoDb.dbContext.select(
          cs.PARENT_CALC_ID, cs.CHILDREN_CALC_ID
        ).from(
          table(name("q")).
            join(cs).on(cs.CHILDREN_CALC_ID.equal(field[Integer](name("q", "parent_calc_id"), classOf[Integer])))
        )
      )).select(
      arrayAgg(field[Integer](name("q", "parent_calc_id"), classOf[Integer]))
    ).from(
        table(name("q"))
      ).asField("datasets")
    //with recursive q as (select cs.* from calcsets cs where cs.children_calc_id = 55280 union all select cs.* from q join calcsets cs on cs.children_calc_id = q.parent_calc_id ) select q.parent_calc_id from q left outer join calcsets cs2 on cs2.children_calc_id = q.parent_calc_id where cs2.parent_calc_id is null

    val q = repoDb.dbContext.select(
      // combined fields
      calc_tags,
      citation_ids,
      coauthor_ids,
      shared_with_ids,
      dataset_ids,
      owner_ids,
      // calc
      REPO.CALCULATIONS.CALC_ID, REPO.UPLOADS.UPLOAD_ID, REPO.UPLOADS.USER_ID, REPO.UPLOADS.CREATED, REPO.METADATA.ADDED, REPO.METADATA.FILENAMES,
      // parserData
      REPO.CALCULATIONS.CHECKSUM, REPO.METADATA.CHEMICAL_FORMULA, REPO.METADATA.LOCATION,
      REPO.STRUCT_RATIOS.FORMULA_UNITS, REPO.STRUCT_RATIOS.CHEMICAL_FORMULA,
      REPO.CODEVERSIONS.CONTENT, REPO.SPACEGROUPS.N,
      // editableData
      REPO.USER_METADATA.PERMISSION, REPO.USER_METADATA.LABEL
    ).from(
      REPO.CALCULATIONS.
        leftOuterJoin(REPO.UPLOADS).on(REPO.CALCULATIONS.ORIGIN_ID.equal(REPO.UPLOADS.UPLOAD_ID)).
        join(REPO.METADATA).on(REPO.METADATA.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)).
        leftOuterJoin(REPO.STRUCT_RATIOS).on(REPO.STRUCT_RATIOS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)).
        leftOuterJoin(REPO.CODEVERSIONS).on(REPO.CODEVERSIONS.VERSION_ID.equal(REPO.METADATA.VERSION_ID)).
        leftOuterJoin(REPO.SPACEGROUPS).on(REPO.SPACEGROUPS.CALC_ID.equal(REPO.CALCULATIONS.CALC_ID)).
        join(REPO.USER_METADATA).on(REPO.METADATA.CALC_ID.equal(REPO.USER_METADATA.CALC_ID))
    ).where(condition).orderBy(REPO.CALCULATIONS.CALC_ID)

    rawDataMapper.updateRawData()

    for (r <- q.fetchLazy().asScala) {
      try {
        val checksum = r.getValue(REPO.CALCULATIONS.CHECKSUM)
        if (checksum != null && !DataSet.isDataset(checksum)) {
          val calcId = r.getValue(REPO.CALCULATIONS.CALC_ID).toLong
          val t = tagMap.classifyTags(r.value1().map(_.toInt))
          val formulaNRep = r.getValue(REPO.STRUCT_RATIOS.FORMULA_UNITS) match {
            case null => 1
            case nRep => nRep.toInt
          }
          val formulas = (r.getValue(REPO.STRUCT_RATIOS.CHEMICAL_FORMULA) match {
            case null =>
              r.getValue(REPO.METADATA.CHEMICAL_FORMULA) match {
                case null => Seq()
                case f => Seq(f)
              }
            case f => Seq(f)
          }).map { f: String =>
            try {
              Composition.fromHtml(f, Some(formulaNRep)).toLiftedGcd.toString
            } catch {
              case NonFatal(e) =>
                if (!f.endsWith(" slab"))
                  logger.warn(s"Special formula $f not normalized for calc_id $calcId")
                f
            }
          }

          val pData = CalculationParserData(
            repository_atomic_elements = t.getOrElse("repository_atomic_elements", Seq()).map(_.value),
            repository_basis_set_type = t.getOrElse("repository_basis_set_type", Seq()).map(_.value),
            repository_checksum = r.getValue(REPO.CALCULATIONS.CHECKSUM),
            repository_chemical_formula = formulas,
            repository_code_version = r.getValue(REPO.CODEVERSIONS.CONTENT) match {
              case null => Seq()
              case v => Seq(v)
            },
            repository_crystal_system = t.getOrElse("repository_crystal_system", Seq()).map(_.value),
            repository_parser_id = "RepoBaseParser",
            repository_program_name = t.getOrElse("repository_program_name", Seq()).map(_.value),
            repository_spacegroup_nr = r.getValue(REPO.SPACEGROUPS.N) match {
              case null => Seq()
              case n => Seq(n.toInt)
            },
            repository_system_type = t.getOrElse("repository_system_type", Seq()).map(_.value),
            repository_xc_treatment = t.getOrElse("repository_xc_treatment", Seq()).map(_.value)
          )
          val files: String = new String(r.getValue(REPO.METADATA.FILENAMES), StandardCharsets.UTF_8)
          val filePaths: Seq[String] = {
            val raw = if (files.length > 0 && (files(0) == '[' || files(0) == '"')) {
              JsonUtils.parseStr(files) match {
                case jn.JArray(x) =>
                  x.map(y => y match {
                    case jn.JString(x) => x
                    case _ => throw new Exception(s"Unexpected value in metadata.filenames '${files}'")
                  })
                case jn.JString(s) =>
                  Seq(s)
                case _ =>
                  Seq("")
              }
            } else {
              Seq(files)
            }
            raw.map(_.replaceAll("\\/", "/"))
          }
          val truncatedPaths = filePaths.map { path =>
            if (path.startsWith("$EXTRACTED")) {
              path.drop("$EXTRACTED".length)
            } else {
              path.indexOfSlice("/extracted/") match {
                case -1 => path
                case n => path.drop(n + "/extracted".length)
              }
            }
          }
          val calcUris = truncatedPaths.flatMap { rawDataMapper.nmdUriForRepoPath(_).headOption }
          val archiveGids = rawDataMapper.nmdUriForRepoPath(truncatedPaths.head).map(_.archiveGid)

          val citations: Seq[Citation] = r.value2() match {
            case null => Seq()
            case cs => cs.map { x: Integer =>
              citation(x.toInt)
            }(breakOut)
          }

          val ownerIds: Seq[Int] = r.value6() match {
            case null =>
              r.getValue(REPO.UPLOADS.USER_ID) match {
                case null => Seq()
                case n => Seq(n.toInt)
              }
            case a =>
              val ids: Seq[Int] = a.map { x: Integer => x.toInt }(breakOut)
              r.getValue(REPO.UPLOADS.USER_ID) match {
                case null => ids
                case n => if (!ids.contains(n.toInt))
                  ids :+ n.toInt
                else
                  ids
              }
          }

          val uploaders: Seq[Uploader] = ownerIds.map(uploader)

          val authorIds: Seq[Int] = r.value3() match {
            case null => Seq()
            case a =>
              a.map { x: Integer => x.toInt }(breakOut)
          }

          val authors: Seq[Author] = (authorIds ++ ownerIds.filter(!authorIds.contains(_))).map(author)

          val sharedWiths: Seq[SharedWith] = r.value4() match {
            case null => Seq()
            case ss => ss.map { x: Integer =>
              sharedWith(x.toInt)
            }(breakOut)
          }

          val datasetsIdsSeq: Seq[Long] = r.value5() match {
            case null => Seq()
            case ds => ds.map { x: Integer =>
              x.toLong
            }(breakOut)
          }

          val mData = CalculationUserData(
            repository_open_date = None, //FIXME: extract proper value from DB once supported
            repository_access_now = r.getValue(REPO.USER_METADATA.PERMISSION).toInt match {
              case 0 => AccessLevel.Restricted
              case 1 => AccessLevel.Open
              case 2 => AccessLevel.Staging
              case x => throw new Exception(s"unexpected access level $x")
            },
            section_citation = citations,
            section_author_info = authors,
            section_shared_with = sharedWiths,
            repository_comment = Option(r.getValue(REPO.USER_METADATA.LABEL)),
            section_repository_dataset = datasets(calcId, datasetsIdsSeq.distinct)
          )

          val calc = Calculation(
            main_file_uri = calcUris.headOption.map(_.toUriStr(ObjectKind.NormalizedData)),
            secondary_file_uris = if (calcUris.isEmpty) Seq() else calcUris.tail.map(_.toUriStr(ObjectKind.NormalizedData)),
            repository_archive_gid = archiveGids,
            repository_filepaths = filePaths,
            repository_calc_id = r.getValue(REPO.CALCULATIONS.CALC_ID).toLong,
            repository_calc_pid = Base32.b32NrRepository(r.getValue(REPO.CALCULATIONS.CALC_ID).toLong),
            section_uploader_info = uploaders.head,
            upload_id = r.getValue(REPO.UPLOADS.UPLOAD_ID) match {
              case null => -1
              case n => n.toInt
            },
            upload_date = Option(r.getValue(REPO.UPLOADS.CREATED) match {
              case null =>
                r.getValue(REPO.METADATA.ADDED)
              case v => v
            }).map(_.getTime),
            section_repository_parserdata = pData,
            section_repository_userdata = mData,
            repository_grouping_checksum = Calculation.groupingChecksum(pData, mData)
          )
          try {
            calcOp(calc)
          } catch {
            case NonFatal(e) =>
              logger.error(s"Failure of calcOp in loopOnCalcs for $calc", e)
          }
        }
      } catch {
        case NonFatal(e) =>
          logger.error(s"Failure constructing calculation from $r", e)
      }
    }
  }

}
