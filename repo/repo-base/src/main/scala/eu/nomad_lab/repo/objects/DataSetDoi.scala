/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.objects.AccessLevel.AccessLevel
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

object DataSetDoi {
  val sectionName: String = "section_dataset_doi"

  /**
   * definition of the field types used in the ElasticSearch representation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.keywordField("dataset_doi_name"),
        ES.longField("dataset_doi_id")
      )
    )
  }
}

case class DataSetDoi(
    dataset_doi_name: String,
    dataset_doi_id: Long
) extends MappableBaseValue with Section {
  override def typeStr: String = DataSetDoi.sectionName

  override def idStr: String = dataset_doi_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "dataset_doi_name" -> dataset_doi_name,
      "dataset_doi_id" -> dataset_doi_id
    )
  }

  override def valuesMap = Map(
    "dataset_doi_name" -> Seq(dataset_doi_name),
    "dataset_doi_id" -> Seq(dataset_doi_id)
  )

  override def sectionMap = Map()
}

/**
 * This class represents a DOI assigned to a NOMAD DataSet as a search response, while
 * [[eu.nomad_lab.repo.objects.DataSetDoi]] is the representation of a DOI as part of the
 * calculation data structure.
 * @param doi the actual DOI record
 * @param dataSetWithDoi the data set record with the DOI
 * @param documentAccessLevels maps access levels to the number of contained calculations
 * @param numDocuments number of calculations belonging to this DOI
 */
case class DataSetDoiResult(doi: DataSetDoi, dataSetWithDoi: DataSet,
    documentAccessLevels: Map[AccessLevel, Int], numDocuments: Int) extends BaseValue {

  override def typeStr: String = "DataSetDoiResult"

  override def idStr: String = doi.dataset_doi_id.toString

  override def attributes: Map[String, jn.JValue] = Map(
    "num_calculations" -> decompose(numDocuments),
    "access_levels" -> decompose(documentAccessLevels.map(x => x._1.toString -> x._2)),
    "section_repository_dataset" -> decompose(dataSetWithDoi.filteredMap)
  )
}