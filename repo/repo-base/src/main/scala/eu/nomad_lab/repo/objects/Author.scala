/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.FieldDefinition
import org.{ json4s => jn }

object Author {

  val sectionName: String = "section_author_info"

  /**
   * definition of the field types used in the ElasticSearch representation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.intField("author_repo_id").index(true),
        ES.keywordField("author_first_name"),
        ES.keywordField("author_last_name"),
        ES.textField("author_name")
      )
    )
  }
}

case class Author(
    author_repo_id: Int,
    author_first_name: String,
    author_last_name: String,
    author_name: String
) extends MappableBaseValue with Section {
  override def typeStr: String = Author.sectionName

  override def idStr: String = author_repo_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "author_repo_id" -> author_repo_id,
      "author_first_name" -> author_first_name,
      "author_last_name" -> author_last_name,
      "author_name" -> author_name
    )
  }

  override def valuesMap = Map(
    "author_repo_id" -> Seq(author_repo_id),
    "author_first_name" -> Seq(author_first_name),
    "author_last_name" -> Seq(author_last_name),
    "author_name" -> Seq(author_name)
  )

  override def sectionMap = Map()
}
