/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import java.time.{ LocalDate, ZonedDateTime }

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.meta.KnownMetaInfoEnvs
import eu.nomad_lab.rawdata.{ RawDataDb, RawDataMapper }
import eu.nomad_lab.repo.objects.Calculation

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.control.NonFatal
import scala.util.matching.Regex

object RepoTool extends StrictLogging {
  val indexReData: Regex = """--indexName=([\w_-]+)""".r
  val indexReTopics: Regex = """--indexNameTopics=([\w_-]+)""".r
  val indexDateRe: Regex = """(?:index|topics)(\d{4}-\d{2}-\d{2})""".r

  lazy val repoDb = RepoDb()
  lazy val rawDataDb = RawDataDb()
  lazy val rawDataMapper = new RawDataMapper(rawDataDb)
  lazy val loader = new CalculationLoader(repoDb, rawDataMapper)

  val usage = """
Usage:
  repoTool [--help] <command>

        Available commands:
    newIndex       create and fill a new index
    updateIndex    update an existing index with new calculations
    partialIndex   partial update of an index or Nomad Archive index

    Run repoTool <command> [--help] to view information about the
    options for each command.
  """
  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    if (list.isEmpty) {
      println(usage)
      return
    }
    val cmd = list.head
    list = list.tail
    cmd match {
      case "--help" | "-h" =>
        println(usage)
      case "newIndex" => buildNewIndex(list)
      case "updateIndex" => fetchNewCalculations(list)
      case "partialIndex" => partialIndex(list)
      case command =>
        println(s"invalid command $command")
        println(usage)
        return
    }
  }

  def buildNewIndex(args: List[String]): Unit = {
    val usage =
      """Usage:
  repoTool newIndex
      [--verbose]
      [--noDeletion]
      [--noAliasRemap]
      [--indexName=<name of new index>]
      [--indexNameTopics=<name of new index>]

      Build a new index by querying all data from the connected
      PostgreSQL database. If no index name is given, it defaults
      to a name with the current date "indexYYYY_MM_DD".
      Unless specified otherwise, indices matching the pattern
      "indexYYYY_MM_DD" will be deleted if they're older than
      three days.
      Unless --noAliasRemap is specified, the alias specified in
      the config parameter nomad_lab.elastic.indexNameData
      will be remapped to the newly created index after completion.
  """
    var list: List[String] = args
    var doAliasRemap = true
    var deleteOldIndices = true
    var verbose = false
    val today = ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameData = "index" + today
    var indexNameTopic = "topics" + today
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--verbose" => verbose = true
        case "--noDeletion" => deleteOldIndices = false
        case "--noAliasRemap" => doAliasRemap = false
        case indexReData(name) => indexNameData = name
        case indexReTopics(name) => indexNameTopic = name
        case _ =>
          println(s"Error: unexpected argument $arg. $usage")
          return
      }
    }

    val es = try {
      ESManager(KnownMetaInfoEnvs.repo)
    } catch {
      case NonFatal(e) => {
        logger.error(s"Elastic search connection failed! " + e)
        return
      }
    }
    try {
      rebuildIndex(es, indexNameData, indexNameTopic, doAliasRemap, verbose, deleteOldIndices, today)
    } finally {
      es.stop()
    }
  }

  def rebuildIndex(es: ESManager, indexNameData: String, indexNameTopic: String,
    doAliasRemap: Boolean, verbose: Boolean, deleteOldIndices: Boolean, today: LocalDate): Unit = {
    val inserter = CalculationElasticInserter(es, indexName = Some(indexNameData))
    val topicInserter = TopicElasticInserter(repoDb, es, indexName = Some(indexNameTopic))
    inserter.verbose = verbose
    topicInserter.verbose = verbose

    val indexCreation = inserter.createIndex(indexNameData).await
    if (!indexCreation.acknowledged) {
      logger.error(s"Elastic search could not create the new index! " + indexCreation)
      return
    }
    val topicCreation = topicInserter.createIndex(indexNameTopic).await
    if (!topicCreation.acknowledged) {
      logger.error(s"Elastic search could not create the new topic index! " + topicCreation)
      return
    }
    topicInserter.indexTopics(0)
    logger.info("Finished topics index insertion")

    var ii: Long = 0
    var lastNInserted: Long = -1
    while (inserter.nInserted != lastNInserted) {
      lastNInserted = inserter.nInserted
      loader.loopOnCalcs(
        calcIdMin = Some(ii),
        calcIdMax = Some(ii + 1000)
      )(inserter.addCalculation)
      inserter.indexCached()
      ii += 1000
      if (ii % 50000 == 0) logger.info(s"Indexing progress: calculation IDs up to $ii finished")
    }
    inserter.finishPending()
    logger.info("Finished index insertion")

    if (doAliasRemap) {
      val aliasResult = es.client.execute(catAliases()).await()
      val liveIndexData = ESManager.defaultSettings(KnownMetaInfoEnvs.repo).indexNameData
      val liveIndexTopics = ESManager.defaultSettings(KnownMetaInfoEnvs.repo).indexNameTopics
      logger.info(s"Data Alias to be used: $liveIndexData")
      logger.info(s"Topics Alias to be used: $liveIndexTopics")
      val rawIndexNameData = aliasResult.find(x => x.alias == liveIndexData) match {
        case None => None
        case Some(x) => Some(x.index)
      }
      val rawIndexNameTopics = aliasResult.find(x => x.alias == liveIndexTopics) match {
        case None => None
        case Some(x) => Some(x.index)
      }
      if (rawIndexNameData.nonEmpty) {
        es.client.execute(refreshIndex(indexNameData, rawIndexNameData.get)).await
        val oldCount = es.client.execute(catCount(rawIndexNameData.get)).await.count
        val newCount = es.client.execute(catCount(indexNameData)).await.count
        logger.info(s"Old Data index: ${rawIndexNameData.get} with $oldCount entries")
        logger.info(s"New Data index: $indexNameData with $newCount entries")
        if (oldCount > newCount) {
          logger.error(s"New data index contains less entries than the current one!")
          return
        }
      }
      var aliasRemap = rawIndexNameData match {
        case None => Seq(addAlias("repo").on(indexNameData))
        case Some(x) => Seq(removeAlias("repo").on(x), addAlias("repo").on(indexNameData))
      }
      if (rawIndexNameTopics.nonEmpty) {
        es.client.execute(refreshIndex(indexNameTopic, rawIndexNameTopics.get)).await
        val oldCount = es.client.execute(catCount(rawIndexNameTopics.get)).await.count
        val newCount = es.client.execute(catCount(indexNameTopic)).await.count
        logger.info(s"Old Topics index: ${rawIndexNameTopics.get} with $oldCount entries")
        logger.info(s"New Topics index: $indexNameTopic with $newCount entries")
        if (oldCount > newCount) {
          logger.error(s"New topics index contains less entries than the current one!")
          return
        }
      }
      aliasRemap = aliasRemap ++ (rawIndexNameTopics match {
        case None => Seq(addAlias("topics").on(indexNameTopic))
        case Some(x) => Seq(removeAlias("topics").on(x), addAlias("topics").on(indexNameTopic))
      })

      val remapResponse = es.client.execute(aliases(aliasRemap)).await
      if (!remapResponse.success) {
        logger.error("Alias remapping failed!", remapResponse.toString)
        return
      } else {
        logger.info(s"Data Alias remapped, alias $liveIndexData now points to index $indexNameData")
        logger.info(s"Topics Alias remapped, alias $liveIndexTopics now points to index $indexNameTopic")
      }
    }
    if (deleteOldIndices) {
      logger.info("Beginning cleanup of old indices")
      es.client.execute(catIndices()).await.filter(x => x.index != indexNameData &&
        x.index != indexNameTopic).foreach(index => {
        index.index match {
          case indexDateRe(date) if LocalDate.parse(date).plusDays(3).isBefore(today) => {
            val deleteTask = es.client.execute(deleteIndex(index.index))
            if (deleteTask.await.acknowledged) {
              logger.info("  Deleting old index: " + index.index)
            } else {
              logger.error("  Failed to delete old index: " + index.index)
            }
          }
          case indexDateRe(_) => logger.info("  Keeping previous index: " + index.index)
          case _ => logger.info("  ignoring index not matching pattern: " + index.index)
        }
      })
    }
  }

  def fetchNewCalculations(args: List[String]): Unit = {
    val usage = """Usage:
  repoTool updateIndex
      [--verbose]
      [--indexName=<name of new index>]
      [--indexNameTopics=<name of new index>]

      Update the given index by adding all calculations from the
      PostgreSQL database which have a larger calculation ID than
      those stored in ElasticSearch. If no index name is given, it
      defaults to a name with the current date "indexYYYY_MM_DD".
  """
    var list: List[String] = args
    var verbose = false
    var indexNameData = "index" + ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameTopics = "topics" + ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--verbose" => verbose = true
        case indexReData(name) => indexNameData = name
        case indexReTopics(name) => indexNameTopics = name
        case _ =>
          println(s"Error: unexpected argument $arg. $usage")
          return
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.repo)
    } catch {
      case NonFatal(e) =>
        logger.error(s"Elastic search connection failed! " + e)
        return
    }
    try {
      insertNewCalculations(es, indexNameData, indexNameTopics, verbose)
    } finally {
      es.stop()
    }
  }

  def insertNewCalculations(es: ESManager, indexNameData: String, indexNameTopics: String,
    verbose: Boolean): Unit = {
    val inserter = CalculationElasticInserter(es, indexName = Some(indexNameData))
    val topicInserter = TopicElasticInserter(repoDb, es, indexName = Some(indexNameTopics))
    inserter.verbose = verbose
    topicInserter.verbose = verbose

    val lastCalcIdInIndex = Await.result(es.findLastId(indexNameData, es.settings.typeNameData,
      es.connector.metaNameToField("repository_calc_id")), 2.seconds)
    val lastTopicIdInIndex = Await.result(es.findLastId(indexNameTopics, es.settings.typeNameTopics,
      "repository_topic_id"), 2.seconds)
    Thread.sleep(15000)
    var iCalc = Await.result(es.findLastId(indexNameData, es.settings.typeNameData,
      es.connector.metaNameToField("repository_calc_id")), 2.seconds)
    var iTopic = Await.result(es.findLastId(indexNameTopics, es.settings.typeNameTopics,
      "repository_topic_id"), 2.seconds)
    val oldCountCalcs = es.client.execute(catCount(indexNameData)).await.count
    val oldCountTopics = es.client.execute(catCount(indexNameTopics)).await.count
    if (lastCalcIdInIndex != iCalc) {
      logger.error(s"ERROR: something else is appending to index (last calc_id in ES index " +
        s"changed from $lastCalcIdInIndex to $iCalc), stopping")
      return
    } else if (lastTopicIdInIndex != iTopic) {
      logger.error(s"ERROR: something else is appending to index (last topic_id in ES index " +
        s"changed from $lastTopicIdInIndex to $iTopic), stopping")
      return
    } else {
      logger.info(s"Index stats: $indexNameData has $oldCountCalcs entries, highest id $iCalc")
      logger.info(s"Index stats: $indexNameTopics has $oldCountTopics entries, highest id $iTopic")
      logger.info("Now checking for new calculations in db")
      iCalc += 1
      iTopic += 1
    }

    topicInserter.indexTopics(iTopic.toInt)

    var lastNInserted: Long = -1
    while (inserter.nInserted != lastNInserted) {
      lastNInserted = inserter.nInserted
      loader.loopOnCalcs(
        calcIdMin = Some(iCalc),
        calcIdMax = Some(iCalc + 1000)
      )(inserter.addCalculation)
      inserter.indexCached()
      iCalc += 1000
    }
    inserter.finishPending()
    es.client.execute(refreshIndex(indexNameData)).await
    val newCountCalcs = es.client.execute(catCount(indexNameData)).await.count
    val newCountTopics = es.client.execute(catCount(indexNameTopics)).await.count
    iCalc = Await.result(es.findLastId(indexNameData, es.settings.typeNameData,
      es.connector.metaNameToField("repository_calc_id")), 2.seconds)
    iTopic = Await.result(es.findLastId(indexNameTopics, es.settings.typeNameTopics,
      "repository_topic_id"), 2.seconds)
    logger.info(s"Finished topics index update, added ${newCountTopics - oldCountTopics} entries")
    logger.info(s"Index stats: $indexNameTopics has $newCountTopics entries, highest id $iTopic")
    logger.info(s"Finished index update, added ${newCountCalcs - oldCountCalcs} new entries")
    logger.info(s"Index stats: $indexNameData has $newCountCalcs entries, highest id $iCalc")
  }

  /**
   * Index updates for the Nomad Archive, or in general does a partial index
   */
  def partialIndex(args: List[String]): Unit = {
    val usage = """Usage:
  repoTool partialIndex
       [--help]
       [--verbose]
       [--fetch-new]
       [--min-calc-id <minCalcId>]
       [--max-calc-id <minCalcId>]
       [--calc-ids <calcId1>[,<calcId2>[,...]]]
       [--archive]
       [--indexName=<name of new index>]

       Does a partial or selective update of elastic search index or of the Nomad Archive index.
       Useful also for debugging.
  """
    var minCalcId: Option[Long] = None
    var maxCalcId: Option[Long] = None
    var calcIds: Seq[Long] = Seq()
    var verbose: Boolean = false
    var fetchNew: Boolean = false
    var list: List[String] = args
    var indexNameData: Option[String] = None
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case indexReData(name) =>
          indexNameData = Some(name)
        case "--verbose" =>
          verbose = true
        case "--fetch-new" =>
          fetchNew = true
        case "--min-calc-id" =>
          if (list.isEmpty) {
            println(s"Error: missing calc_id number after --min-calc-id in repo-index command. $usage")
            return
          }
          minCalcId = Some(list.head.toLong)
          list = list.tail
        case "--max-calc-id" =>
          if (list.isEmpty) {
            println(s"Error: missing calc_id number after --max-calc-id in repo-index command. $usage")
            return
          }
          maxCalcId = Some(list.head.toLong)
          list = list.tail
        case "--calc-ids" =>
          if (list.isEmpty) {
            println(s"Error: missing comma separated calc_ids number after --min-calc-id in repo-index command. $usage")
            return
          }
          calcIds ++= list.head.split(",").map(_.toLong)
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg in query command. $usage")
          return
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.repo)
    } catch {
      case NonFatal(e) => {
        logger.error(s"Elastic search connection failed! " + e)
        return
      }
    }
    try {
      partialIndexUpdate(es, verbose, fetchNew, minCalcId, maxCalcId, calcIds, indexNameData)
    } finally {
      es.stop()
    }
  }

  def partialIndexUpdate(es: ESManager, verbose: Boolean, fetchNew: Boolean, minCalcId: Option[Long],
    maxCalcId: Option[Long], calcIds: Seq[Long], indexNameData: Option[String] = None): Unit = {
    val repoDb = RepoDb()
    val rawDataDb = RawDataDb()
    val rawDataMapper = new RawDataMapper(rawDataDb)
    val inserter = CalculationElasticInserter(es, indexName = indexNameData)
    inserter.verbose = verbose
    val indexName = indexNameData.getOrElse(es.settings.indexNameData)
    val indexE = es.client.execute(indexExists(indexName)).await
    if (!indexE.exists) {
      val indexCreation = inserter.createIndex(indexName).await
      if (!indexCreation.acknowledged) {
        logger.error(s"Elastic search could not create the new index! " + indexCreation)
        return
      }
    }
    val loader = new CalculationLoader(repoDb, rawDataMapper)
    if (fetchNew && (!minCalcId.isEmpty || !calcIds.isEmpty)) {
      println(s"ERROR: --fetch-new cannot be specified together with --min-calc-id or --calc-ids\n$usage")
      return
    }
    if (maxCalcId.isEmpty && calcIds.isEmpty) {
      var ii: Long = minCalcId.getOrElse(0);
      if (fetchNew) {
        val lastCalcIdInIndex = Await.result(es.findLastId(
          indexName,
          Calculation.sectionName, "repository_calc_id"
        ), 2.seconds)
        Thread.sleep(10)
        ii = Await.result(es.findLastId(indexName, Calculation.sectionName,
          "repository_calc_id"), 2.seconds)
        if (lastCalcIdInIndex != ii) {
          println(s"ERROR: something else is appendig to index (last calc_id in ES index changed from $lastCalcIdInIndex to $ii), stopping")
          return
        } else {
          ii += 1
          println(s"Last calc_id in index is $lastCalcIdInIndex, checking for new calculations in db")
        }
      }
      var lastNInserted: Long = -1
      while (inserter.nInserted != lastNInserted) {
        lastNInserted = inserter.nInserted
        loader.loopOnCalcs(
          calcIdMin = Some(ii),
          calcIdMax = Some(ii + 1000)
        )(inserter.addCalculation)
        inserter.indexCached()
        ii += 1000
      }
    } else {
      loader.loopOnCalcs(
        calcIdMin = minCalcId,
        calcIdMax = maxCalcId,
        calcIds = if (calcIds.isEmpty)
        None
      else
        Some(calcIds)
      )(inserter.addCalculation)
      inserter.indexCached()
    }
    inserter.finishPending()
    logger.info("Finished index insertion")
  }

}
