/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection

import java.io.{ BufferedInputStream, File, FileInputStream, InputStream }
import java.math.RoundingMode
import java.text.DecimalFormat

import org.apache.commons.compress.compressors.bzip2

object FileSizeSupport {
  val df: DecimalFormat = new DecimalFormat("#.###")
  df.setRoundingMode(RoundingMode.CEILING)

  var sizesToWrite: scala.collection.mutable.MutableList[String] = collection.mutable.MutableList()
  def writeSizesToDisk(fileToWrite: Option[File]): Unit = {
    if (sizesToWrite.nonEmpty) {
      fileToWrite match {
        case Some(wFile) =>
          val p = new java.io.FileWriter(wFile, true)
          sizesToWrite.foreach(str => p.append(str + "\n"))
          p.close()
        case None =>
          sizesToWrite.foreach(str => println(str))
      }
    }
    sizesToWrite = collection.mutable.MutableList()
  }
  def writeSizeToBuffer(string: String, fileToWrite: Option[File]): Unit = {
    sizesToWrite += string
    if (sizesToWrite.size > 10) {
      writeSizesToDisk(fileToWrite)
    }
  }

  var existingFileSizeEntries: Map[String, String] = Map()
  def entriesInFile(file: File): Unit = {
    if (existingFileSizeEntries.isEmpty && file.exists() && file.isFile) {
      import scala.io.Source
      for (line <- Source.fromFile(file).getLines) {
        val sp = line.split("\t")
        if (sp.length == 2)
          existingFileSizeEntries += sp(1) -> sp(0) //fileName -> Size with symbol
      }
    }
    ()
  }
  def getAppropriateSizeAndSymbol(size: Long): (String, String) = {
    val kb = 1024.0
    val (sym, sizeToPrint) = if (size > kb * kb * kb) ("G", size / (kb * kb * kb)) else if (size > kb * kb) ("M", size / (kb * kb)) else ("K", size / kb)
    (sym, df.format(sizeToPrint))
  }

  def sizeOfExistingEntries(): Long = { //Directories show the size of the all the children file; so finding it is tough; Not used at the moment
    val kb: Double = 1024.0
    var size: Long = 0
    for ((k, v) <- existingFileSizeEntries) {
      if (v.endsWith("K")) size += (v.dropRight(1).toDouble * (kb)).toLong
      else if (v.endsWith("M")) size += (v.dropRight(1).toDouble * (kb * kb)).toLong
      else if (v.endsWith("G")) size += (v.dropRight(1).toDouble * (kb * kb * kb)).toLong
    }
    size
  }

  def getDuSize(files: List[String], cDepth: Int, maxDepth: Int, fileToWrite: Option[File]): Long = {
    var size: Long = 0
    for (fEle <- files) {
      if (existingFileSizeEntries.isEmpty || !existingFileSizeEntries.contains(fEle)) {
        val file = new File(fEle)
        val cFiles = file.listFiles
        if (file.isDirectory && !cFiles.isEmpty) {
          val dSize = getDuSize(cFiles.map(cf => cf.getAbsolutePath).toList, cDepth + 1, maxDepth, fileToWrite)
          size += dSize
          val (symb, dv) = getAppropriateSizeAndSymbol(dSize)
          val duString = s"${dv}$symb\t${file.getPath}"
          if (cDepth <= maxDepth)
            writeSizeToBuffer(duString, fileToWrite)
        } else if (file.isFile) {
          if (bzip2.BZip2Utils.isCompressedFilename(file.getName)) {
            var fSize: Long = -1
            try {
              val bZip2CompressorInputStream = new bzip2.BZip2CompressorInputStream(new FileInputStream(file), false)
              val inpStream = new BufferedInputStream(bZip2CompressorInputStream)
              fSize = BagitManager.sizeOfStream(inpStream)
              inpStream.close()
            } catch {
              case e: Exception =>
                val duString = (s"${file.getPath}: Exception while calculating the size")
                writeSizeToBuffer(duString, fileToWrite)
                println(duString)
            }
            val (symb, dv) = getAppropriateSizeAndSymbol(fSize)
            val duString = s"${dv}$symb\t${file.getPath}"
            if (cDepth <= maxDepth)
              writeSizeToBuffer(duString, fileToWrite)
            size += fSize
          } else {
            //          val attr = java.nio.file.Files.readAttributes(java.nio.file.Paths.get(file.getPath),classOf[java.nio.file.attribute.BasicFileAttributes])
            val fSize: Long = file.length()
            val (symb, dv) = getAppropriateSizeAndSymbol(fSize)
            val duString = s"${dv}$symb\t${file.getPath}"
            if (cDepth <= maxDepth)
              writeSizeToBuffer(duString, fileToWrite)
            size += fSize
          }
        }
      }
    }
    writeSizesToDisk(fileToWrite)
    size
  }
}
