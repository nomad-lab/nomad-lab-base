/*
  Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed,
                      Markus Schneider
                      Fritz-Haber-Institut der Max-Planck-Gesellschaft

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package eu.nomad_lab.repo.data_manager

import eu.nomad_lab.repo.RepoDb
import eu.nomad_lab.repo.db.{ Tables => REPO }
import org.jooq.{ Condition, DatePart, Record, TableOnConditionStep }
import org.jooq.impl.DSL

/*
 * Class for everything concerning "old restricted calculations"
 */
class OldRestictedCalcsHandler(val repoDb: RepoDb) {

  /*
   * List "old restricted calculations"
   */
  def listOldRestrictedCalcs(): Unit = {

    // selection for query
    val selection =
      repoDb.dbContext.select(
        REPO.USER_METADATA.CALC_ID, REPO.USER_METADATA.PERMISSION,
        REPO.METADATA.ADDED, REPO.METADATA.OADATE, REPO.CALCULATIONS.CHECKSUM
      )

    // table for query
    val table = joinTablesOnCalcId()

    // condition for query
    val condition = conditionOldRestrictedCalcs()

    // get query
    val query =
      selection.from(table).where(condition).
        orderBy(REPO.USER_METADATA.CALC_ID.asc()).fetch()

    // print query
    println(query.toString)

  }

  /*
   * Join tables on condition of identical calc_id's
   */
  private def joinTablesOnCalcId(): TableOnConditionStep[Record] = {

    REPO.USER_METADATA.
      innerJoin(REPO.METADATA).on(REPO.USER_METADATA.CALC_ID.eq(REPO.METADATA.CALC_ID)).
      innerJoin(REPO.CALCULATIONS).on(REPO.USER_METADATA.CALC_ID.eq(REPO.CALCULATIONS.CALC_ID))

  }

  /*
   * Join tables condition of identical calc_id's
   */
  private def joinConditionOnCalcId(): Condition = {

    REPO.USER_METADATA.CALC_ID.eq(REPO.METADATA.CALC_ID).
      and(REPO.USER_METADATA.CALC_ID.eq(REPO.CALCULATIONS.CALC_ID))

  }

  /*
   * Condition for "old restricted calculations"
   * => - the calculation is restricted (permission = 0)
   *    - the calculation is not a dataset (checksum mustn't end in "MS")
   *    - the calculation has either passed its open access date
   *      OR the upload date is older than 36 months (3 years)
   */
  private def conditionOldRestrictedCalcs(restrictionMonths: Int = 36): Condition = {

    REPO.USER_METADATA.PERMISSION.eq(0).
      and(DSL.currentTimestamp().gt(REPO.METADATA.OADATE).
        or(DSL.currentTimestamp().gt(DSL.timestampAdd(REPO.METADATA.ADDED, restrictionMonths, DatePart.MONTH)))).
      and(REPO.CALCULATIONS.CHECKSUM.notLike("%MS"))

  }

  /*
   * Make "old restricted calculations" open access
   */
  def setOldRestrictedCalcsOpenAccess(): Unit = {

    // join tables condition
    val joinCondition = joinConditionOnCalcId()

    // condition for update
    val condition = conditionOldRestrictedCalcs()

    // permission = 0 --> restricted
    // permission = 1 --> open access
    // update set permission = 1, i.e. make open access
    val iCalcsUpdated = repoDb.dbContext.update(REPO.USER_METADATA).
      set(REPO.USER_METADATA.PERMISSION, new Integer(1)).
      from(REPO.METADATA, REPO.CALCULATIONS).
      where(joinCondition.and(condition)).execute()

    println(s"$iCalcsUpdated entries have been made open access.")

  }

}
