/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab;
import java.nio.file.Paths

import com.typesafe.config.{ Config, ConfigFactory }

import scala.collection.mutable.ListBuffer
import scala.collection.breakOut
import scala.collection.mutable
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, CalculationParserResult, TreeParserRequest, NormalizerRequest }
import eu.nomad_lab.parsing_queue.{ CalculationParser, TreeParser }
import eu.nomad_lab.parsing_queue.TreeParser.TreeParserException
import org.json4s.JsonAST.JValue
import eu.{ nomad_lab => lab }
import eu.nomad_lab.parsing_stats.HighLevelStats
import eu.nomad_lab.parsing_stats.ParsingStatsDb
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.ref.NomadUri
import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.resolve.FailedResolution
import eu.nomad_lab.resolve.Resolver
import eu.nomad_lab.resolve.Archive
//import eu.nomad_lab.query.flink.FlinkScanner
import com.rabbitmq.client._
import eu.nomad_lab.h5.ArchiveH5
import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.h5.FileH5
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.parsers.BackendH5
import eu.nomad_lab.parsers.ParserBackendBase
import eu.nomad_lab.normalizers.AllNormalizers
//import eu.nomad_lab.parquet.ParquetArchiveWriter
import scala.util.control.NonFatal
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
//import org.apache.flink.api.scala._
import java.io._
//
import com.rabbitmq.client._

object CLITool extends StrictLogging {
  /**
   * Backend types recognized by this tool
   */
  object BackendType extends Enumeration {
    type Enum = Value
    val JsonWriter, JsonEventEmitter, Hdf5, AvroEvents = Value
  }

  /**
   * Exception in the cli tool evaluation
   */
  class CLIException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  lazy val statsDb: Option[ParsingStatsDb] = try {
    Some(ParsingStatsDb())
  } catch {
    case NonFatal(e) =>
      logger.error("Could not connect to the parsing stats db", e)
      None
  }

  val usage = """
Usage:
  nomadTool [--help]
        /* meta info related options: */
    meta
      [--main-meta-file <path to nomadmetainfo.json to load>]
      [--meta-dot-file <where to output dot file>]
      [--meta-remove-unintresting]
      [--meta-remove-meta]
      [--meta-section-only]
      [--meta-abstract-only]
      [--meta-base-url <base url>]
        /* parsing related options: */
    parse
      [--verbose]
      [--main-file-path <path of the main file or archive to parse>]
      [--main-file-uri <uri of the main file to parse>]
      [--path-in-archive <path within the archive identified by the main file>]
      [--test-pipeline]
      [--parser <parser to use>]
      [--json]
      [--avro-events | --avro]
      [--h5 | --hdf5]
      [--json-events]
      [--parsing-out-file <outFilePath>]
        /* converts to other formats */
    convert
      [--archive-uris <comma separated archive uris>]
      [--archive-paths <comma separated archive paths>]
      [--output-dir <output directory>]
      [--parquet]
      [--json]
      [--verbose]
        /* normalization */
    normalize
      [--archive-uri <archive_uri>]
      [--archive-path <archive_path>]
      [--normalizers <comma_separated_list_of_normalizers>]
      [--rabbitMQ <queue name>]
        /* h5 merging */
    merge
      [--append-to-merge]
      [--merge-h5 <h5FileOut> [<fileIn1> <fileIn2> ...]]
        /* query testing (without flink, on a single archive) */
    query
      [--context <context>]
      [--query-string <query string>]
      [--archive-uri <archive_uri>]
      [--archive-path <archive_path>]
      [--user <user>]
      [--flink]
        /* info */
    info
      [--verbose]
      [--list-parsers]
      [--list-normalizers]
      [--parsing-stats <baseName>]

Generates metainfo related outputs (.dot graph,...),
or runs the main parsing step
  """
  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    if (list.isEmpty) {
      println(usage)
      return
    }
    val cmd = list.head
    list = list.tail
    cmd match {
      case "--help" | "-h" =>
        println(usage)
        return
      case "parse" =>
        parseCmd(list)
      case "merge" =>
        mergeCmd(list)
      case "meta" =>
        metaCmd(list)
      case "info" =>
        infoCmd(list)
      case "normalize" =>
        normalizeCmd(list)
      case "query" =>
        queryCmd(list)
      case "convert" =>
        convertCmd(list)
      case c =>
        println("invalid command, expected one of 'parse', 'merge', 'meta', 'info' or 'normalize'")
        println(usage)
        return ()
    }
  }

  /**
   * Implements the query command of the command line
   */
  def queryCmd(args: List[String]): Unit = {
    var queryString: Option[String] = None
    var archiveUri: Option[String] = None
    var archivePath: Option[String] = None
    var context: Option[String] = None
    var list: List[String] = args
    var verbose: Boolean = false
    var flinkQuery: Boolean = false
    var user: String = "fawzi2"
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--verbose" =>
          verbose = true
        case "--flink" =>
          flinkQuery = true
        case "--archive-uri" =>
          if (list.isEmpty) {
            println(s"Error: missing archive uri after --archive-uri in query command. $usage")
            return
          }
          archiveUri = Some(list.head)
          list = list.tail
        case "--user" =>
          if (list.isEmpty) {
            println(s"Error: missing user after --user in query command. $usage")
            return
          }
          user = list.head
          list = list.tail
        case "--archive-path" =>
          if (list.isEmpty) {
            println(s"Error: missing archive path after --archive-path. $usage")
            return
          }
          archivePath = Some(list.head)
          list = list.tail
        case "--query-string" =>
          if (list.isEmpty) {
            println(s"Error: missing query string after --query-string in query command. $usage")
            return
          }
          queryString = Some(list.head)
          list = list.tail
        case "--context" =>
          if (list.isEmpty) {
            println(s"Error: missing context after --context in query command. $usage")
            return
          }
          context = Some(list.head)
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg in query command. $usage")
          return
      }
    }
    val queryExpression = queryString match {
      case Some(str) => query.QueryExpression(str)
      case None =>
        println(s"Error a query string is required in query command.")
        return
    }
    val filter = query.CompiledQuery(queryExpression, meta.KnownMetaInfoEnvs.all)
    val contextStr = context.getOrElse("section_single_configuration_calculation")
    if (flinkQuery) {
      logger.error(s"cli flink query disabled for now, parquet and flink conflict on hadoop dependency breaking assembly, we should probably just use flink to enable this")
      /*val scanner = new FlinkScanner(
        filter = filter,
        context = contextStr,
        fields = Seq(),
        user = user,
        archiveSet = ArchiveSetH5.normalizedSet
      )
      val env = ExecutionEnvironment.createRemoteEnvironment(
        "nomad-flink-01.esc.rzg.mpg.de", 6123, "/nomad/nomadlab/flink/jars/query.jar"
      )
      val csvFile = scanner.run(env)
      scanner.cleanup()*/
    } else {
      archiveUri match {
        case None =>
          println(s"archive uri or ---flink is required for command query. $usage")
          return ()
        case Some(uri) =>
          val nUri = NomadUri(uri)
          val aGid = nUri.toRef match {
            case ref.ArchiveRef(aGid) =>
              aGid
            case r =>
              println(s"Error: expected an archive reference, but got $r from $uri")
              return
          }
          var resolvedRef: Option[ResolvedRef] = None
          val archive = archivePath match {
            case None =>
              val rr = Resolver.resolve(uri)
              resolvedRef = Some(rr)
              rr match {
                case a: resolve.Archive => a
                case v =>
                  println(s"Error: expected an archive reference, but got $v when resolving $uri")
                  return
              }
            case Some(path) =>
              val f = h5.FileH5.open(Paths.get(path))
              val arch = f.openArchive(aGid)
              f.release()
              resolve.Archive(ArchiveSetH5.normalizedSet, arch)
          }
          try {
            val scanOp = new query.CollectAndWriteScanOp(
              context = contextStr,
              filter = filter
            )
            val evaluator = new query.DirectArchiveScanner(
              archiveSet = archive.archiveSet,
              archive = archive.archive,
              scanOp = scanOp
            )
            evaluator.run()
            evaluator.cleanup()
          } finally {
            resolvedRef match {
              case Some(r) =>
                r.giveBack()
              case None =>
                archive.archive.release()
            }
          }
      }
    }
  }

  /**
   * Implements the conversion to other formats on the command line
   */
  def convertCmd(args: List[String]): Unit = {
    var archiveUris: Seq[String] = Seq()
    var archivePaths: Seq[String] = Seq()
    var verbose: Boolean = false
    var parquetOut: Boolean = false
    var jsonOut: Boolean = false
    var outDir: Option[String] = None
    var list: List[String] = args
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--verbose" =>
          verbose = true
        case "--parquet" =>
          parquetOut = true
        case "--json" =>
          jsonOut = true
        case "--output-dir" =>
          if (list.isEmpty) {
            println(s"Error: missing output directory after --output-dir in convert command. $usage")
            return
          }
          outDir = Some(list.head)
          list = list.tail
        case "--archive-uris" =>
          if (list.isEmpty) {
            println(s"Error: missing comma separated archive uris after --archive-uris in convert command. $usage")
            return
          }
          archiveUris ++= list.head.split(",")
          list = list.tail
        case "--archive-paths" =>
          if (list.isEmpty) {
            println(s"Error: missing comma separated archive paths after --archive-paths. $usage")
            return
          }
          archivePaths ++= list.head.split(",")
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg in query command. $usage")
          return
      }
    }

    val archivesDone = mutable.Set[String]()

    def dumpArchive(archive: Archive): Unit = {
      if (archivesDone.contains(archive.archive.archiveGid.drop(1))) return ()
      archivesDone += archive.archive.archiveGid.drop(1)
      val baseDir = Paths.get(outDir.getOrElse("dump"))
      if (jsonOut)
        EmitJsonVisitor.dumpToJson(archive, targetDir = baseDir)
      if (parquetOut) {
        //ParquetArchiveWriter.dumpToParquet(archive, targetPath = baseDir.resolve(archive.archive.archiveGid + ".parquet").toString)
      }
    }

    // could be smarter in using both path and uri together

    for (p <- archivePaths) {
      val f = FileH5.open(Paths.get(p))
      for (a <- f.archives())
        dumpArchive(Archive(ArchiveSetH5.normalizedSet, a))
      f.release()
    }

    for (uri <- archiveUris) {
      val nUri = NomadUri(uri)
      if (!archivesDone.contains(nUri.archiveGid.drop(1))) {
        Resolver.resolve(uri) match {
          case a: Archive =>
            dumpArchive(a)
            a.giveBack()
          case FailedResolution(u, msg) =>
            logger.error(s"could not resolve $u: $msg")
          case v =>
            logger.error(s"expected an archive uri, but got ${v.uriString}")
            v.giveBack()
        }
      }
    }
  }

  /**
   * Implements the normalize command of the command line
   */
  def normalizeCmd(args: List[String]): Unit = {
    var archiveUri: Option[String] = None
    var archivePath: Option[String] = None
    var normalizerList: Seq[String] = Seq()
    var list: List[String] = args
    var verbose: Boolean = false

    var readFromQueue = false

    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--verbose" =>
          verbose = true
          normalize.Normalizer.trace = true
          ParserBackendBase.trace = true
        case "--archive-uri" =>
          if (list.isEmpty) {
            println(s"Error: missing archive uri after --archive-uri. $usage")
            return
          }
          archiveUri = Some(list.head)
          list = list.tail
        case "--archive-path" =>
          if (list.isEmpty) {
            println(s"Error: missing archive path after --archive-path. $usage")
            return
          }
          archivePath = Some(list.head)
          list = list.tail
        case "--normalizers" =>
          if (list.isEmpty) {
            println(s"Error: missing normalizers list after --normalizers. $usage")
            return
          }
          normalizerList ++= list.head.split(",")
          list = list.tail
        case "--rabbitMQ" =>
          //          println(s"Reading files to Normalize from the ToBeNormalizedInitializationQueue")
          //         throw new Exception("disabled as it doesn't compile")
          var repl = LocalEnv.defaultSettings.replacements
          val settings = new Settings(LocalEnv.defaultConfig)

          val rabbitMQConnectionFactory: ConnectionFactory = new ConnectionFactory
          rabbitMQConnectionFactory.setHost(settings.rabbitMQHost)
          val rabbitMQConnection: Connection = rabbitMQConnectionFactory.newConnection
          readFromQueue = true
          readFromRMQueue(settings, rabbitMQConnection)

        case _ =>
          println(s"Error: unexpected argument $arg in normalize command. $usage")
          return
      }
    }

    if (readFromQueue) {
      var repl = LocalEnv.defaultSettings.replacements
      val settings = new Settings(LocalEnv.defaultConfig)

      val rabbitMQConnectionFactory: ConnectionFactory = new ConnectionFactory
      rabbitMQConnectionFactory.setHost(settings.rabbitMQHost)
      val rabbitMQConnection: Connection = rabbitMQConnectionFactory.newConnection
      readFromRMQueue(settings, rabbitMQConnection)

    }

    class Settings(config: Config) {
      // validate vs. reference.conf
      config.checkValid(ConfigFactory.defaultReference(), "simple-lib")
      //    var repl_aux: Map[String, String] = replacements
      //    repl_aux += ("whichWorker" -> whichWorker)
      //    replacements = settings.repl_aux
      val (readExchange, readQueue, writeQueue, writeToExchange) = (
        config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedExchange"),
        config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedQueue"),
        config.getString("nomad_lab.parser_worker_rabbitmq.normalizerCompletedQueue"),
        config.getString("nomad_lab.parser_worker_rabbitmq.normalizerCompletedExchange")
      )

      val rabbitMQHost = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQHost")

      def toJValue: JValue = {
        import org.json4s.JsonDSL._;
        (("rabbitMQHost" -> rabbitMQHost) ~
          ("readQueue" -> readQueue) ~
          ("writeToExchange" -> writeToExchange) ~
          ("writeQueue" -> writeQueue))
      }
    }

    def readFromRMQueue(settings: Settings, rabbitMQConnection: Connection): Unit = {

      val channel: Channel = rabbitMQConnection.createChannel
      channel.exchangeDeclare(settings.readExchange, "fanout")
      //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
      channel.queueDeclare(settings.readQueue, true, false, false, null)
      channel.queueBind(settings.readQueue, settings.readExchange, "")
      logger.info(s"Reading from Queue: ${settings.readQueue}")

      // Fair dispatch: don't dispatch a new message to a worker until it has processed and acknowledged the previous one
      val prefetchCount = 1
      channel.basicQos(prefetchCount) //Sets prefetch for each consumer. For channel based prefetch use  channel.basicQos(prefetchCount, global = true)

      val consumer: Consumer = new DefaultConsumer((channel)) {
        @throws(classOf[IOException])
        override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]) {
          try {
            val message: NormalizerRequest = eu.nomad_lab.JsonSupport.readUtf8[NormalizerRequest](body)
            println(" [x] Received '" + message + "'")
            normalizerWorker(Some("nmd://S" + (message.treeUri).split(".zip")(0).split("/")(6).drop(1)))
            writeToRMQueue(message, settings, rabbitMQConnection)
          } finally {
            println(" [x] Done")
            channel.basicAck(envelope.getDeliveryTag(), false)
          }
        }
      }

      channel.basicConsume(settings.readQueue, false, consumer)
      ()
    }
    def writeToRMQueue(incomingMessage: NormalizerRequest, settings: Settings, rabbitMQConnection: Connection): Unit = {
      var archivePath: Option[String] = None
      var verbose: Boolean = false

      val prodchannel: Channel = rabbitMQConnection.createChannel
      prodchannel.exchangeDeclare(settings.writeToExchange, "fanout")
      prodchannel.queueDeclare(settings.writeQueue, true, false, false, null)
      prodchannel.queueBind(settings.writeQueue, settings.writeToExchange, "")
      def publish(incomingMessage: NormalizerRequest) = {
        logger.warn(s"DSB out ${incomingMessage}")
        val msgBytes = JsonSupport.writeUtf8(incomingMessage)
        logger.info(s"Message: $incomingMessage, bytes Array size: ${msgBytes.length}")
        try {
          prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
        } catch {
          case e: Exception =>
            logger.error(s"Error while publishing to the exchange. Trying again in sometime. Message: ${e.getMessage}")
            try {
              Thread.sleep(1000)
              prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
            } catch {
              case e: Exception =>
                logger.error(s"Retry failed. Exiting now. Message: ${e.getMessage}")
                System.exit(1)
            }
        }

      }
      publish(incomingMessage)
    }

    def normalizerWorker(archiveUri: Option[String]): String = {
      archiveUri match {
        case None =>
          println(s"archive uri is required for command normalize. $usage")
          return ("None")
        case Some(uri) =>
          println("DSBaAAA " + uri)
          val nUri = NomadUri(uri)
          val aGid = nUri.toRef match {
            case ref.ArchiveRef(aGid) =>
              aGid
            case r =>
              println(s"Error: expected an archive reference, but got $r from $uri")
              return ("None")
          }
          var resolvedRef: Option[ResolvedRef] = None
          val archiveSet = ArchiveSetH5.toNormalizeSet
          val archive = archivePath match {
            case None =>
              val rr = Resolver.resolveInArchiveSet(archiveSet, nUri.toRef)
              resolvedRef = Some(rr)
              rr match {
                case resolve.Archive(_, arch) => arch
                case v =>
                  println(s"Error: expected an archive reference, but got $v when resolving $uri")
                  return ("None")
              }
            case Some(path) =>
              val f = h5.FileH5.open(Paths.get(path))
              val arch = f.openArchive(aGid)
              f.release()
              arch
          }
          try {
            val knownNormalizers: Map[String, normalize.NormalizerGenerator] = AllNormalizers.knownNormalizers
            val unknownNormalizers: Seq[String] = normalizerList.filter { x: String => !knownNormalizers.contains(x) }
            if (!unknownNormalizers.isEmpty) {
              println(s"Error: unknown normalizers ${unknownNormalizers.mkString("[", ",", "]")}, known normalizers: ${knownNormalizers.keys.mkString("[", ",", "]")}")
            } else {
              for (normalizerName <- normalizerList) {
                val normalizer = knownNormalizers(normalizerName)
                val evaluator = new query.DirectArchiveScanner(
                  archiveSet = archiveSet,
                  archive = archive,
                  scanOp = new normalize.DirectNormalizer(
                  generator = normalizer,
                  backend = new BackendH5(archiveSet)
                )
                )
                evaluator.run()
                evaluator.cleanup()
              }
            }
          } catch {
            case NonFatal(e) => logger.error(s"error normalizing $archive with ${normalizerList.mkString("[", ",", "]")}", e)
          } finally {
            resolvedRef match {
              case Some(r) =>
                r.giveBack()
              case None =>
                archive.release()
            }
          }
      }
      " Done"
    }

    var message: String = if (!readFromQueue) normalizerWorker(archiveUri) else "None"

    def normalizerRabbitMQ(): Unit = {
      throw new Exception("disabled as it doesn't compile")
      /*class Settings(config: Config) {
        // validate vs. reference.conf
        config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

        val readExchange = config.getString("nomad_lab.parser_worker_rabbitmq.treeParserExchange")
        val readQueue = config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedQueue")
        val writeQueue = config.getString("nomad_lab.parser_worker_rabbitmq.normalizerFinishedQueue")
        val rabbitMQHost = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQHost")
        val writeToExchange = config.getString("nomad_lab.parser_worker_rabbitmq..normalizerFinishedExchange")
        def toJValue: JValue = {
          import org.json4s.JsonDSL._;
          (("rabbitMQHost" -> rabbitMQHost) ~
            ("readQueue" -> readQueue) ~
            ("writeToExchange" -> writeToExchange) ~
            ("writeQueue" -> writeQueue))
        }
      }
      var repl = LocalEnv.defaultSettings.replacements
      val settings = new Settings(LocalEnv.defaultConfig)
      val rabbitMQConnectionFactory: ConnectionFactory = new ConnectionFactory
      rabbitMQConnectionFactory.setHost(settings.rabbitMQHost)
      val rabbitMQConnection: Connection = rabbitMQConnectionFactory.newConnection
      readFromToBeNormalizedInitializationQueue()
      /**
       *  Read messages from the ToBeNormalizedInitializationQueue
       */
      def readFromToBeNormalizedInitializationQueue(): Unit = {
        val channel: Channel = rabbitMQConnection.createChannel
        channel.exchangeDeclare(settings.readExchange, "fanout")
        //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object>      arguments)
        channel.queueDeclare(settings.readQueue, true, false, false, null)
        channel.queueBind(settings.readQueue, settings.readExchange, "")
        logger.info(s"Reading from Queue: ${settings.readQueue}")

        // Fair dispatch: don't dispatch a new message to a worker until it has processed and acknowledged the previous ne
        val prefetchCount = 1
        channel.basicQos(prefetchCount) //Sets prefetch for each consumer. For channel based prefetch use  channel.basicQ os(prefetchCount, global = true)

        val consumer: Consumer = new DefaultConsumer((channel)) {
          @throws(classOf[IOException])
          override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]) {
            val message: NormalizationRequest = eu.nomad_lab.JsonSupport.readUtf8[NormalizerRequest](body)
            println(" [x] Received '" + message + "'")
            //Send acknowledgement to the broker once the message has been consumed.
            try {
              normalizeAndWriteToDoneQueue(message)
            } finally {
              println(" [x] Done")
              channel.basicAck(envelope.getDeliveryTag(), false)
            }
          }
        }
        //basicConsume(String queue, boolean autoAck, Consumer callback)
        channel.basicConsume(settings.readQueue, false, consumer)
        ()
      }
      def normalizeAndWriteToDoneQueue(incomingMessage: NormalizerRequest): Unit = {
        val prodchannel: Channel = rabbitMQConnection.createChannel
        prodchannel.exchangeDeclare(settings.writeToExchange, "fanout")
        //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
        prodchannel.queueDeclare(settings.writeQueue, true, false, false, null)
        prodchannel.queueBind(settings.writeQueue, settings.writeToExchange, "")
        def publish() = {
          val message: NormalizationDoneRequest = normalizerWorker(incomingMessage)
          val msgBytes = JsonSupport.writeUtf8(message)
          logger.info(s"Message: $message, bytes Array size: ${msgBytes.length}")
          try {
            prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
          } catch {
            case e: Exception =>
              logger.error(s"Error while publishing to the exchange. Trying again in sometime. Message: ${e.getMessage}")
              try {
                Thread.sleep(1000)
                prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
              } catch {
                case e: Exception =>
                  logger.error(s"Retry failed. Exiting now. Message: ${e.getMessage}")
                  System.exit(1)
              }
          }

          statsDb match {
            case Some(db) =>
              db.addCalculationAssignement(parserAssociation)
            case None => ()
          }
        }
        val archiveGid: Option[String] = incomingMessage.treeUri match {
          case archiveRe(gid) => Some(gid)
          case _ => None
        }
        archiveGid match {
          case Some(aGid) =>
            statsDb match {
              case Some(db) =>
                db.registerTreeScanStart(
                  rootNamespace = lab.LocalEnv.defaultSettings.rootNamespace,
                  archiveGid = aGid
                )
              case None => ()
            }
          case None => ()
        }
        try {
          logger.info(s"Writing to Exchange: ${settings.writeToExchange}")
          treeParser.findParserAndPublish(publish, incomingMessage)
        } finally {
          archiveGid match {
            case Some(aGid) =>
              statsDb match {
                case Some(db) =>
                  db.registerTreeScanEnd(
                    rootNamespace = lab.LocalEnv.defaultSettings.rootNamespace,
                    archiveGid = aGid
                  )
                case None => ()
              }
            case None => ()
          }
        }
        prodchannel.close
      }
       */
    }

  }

  /**
   * Implements the info command of the command line
   */
  def infoCmd(args: List[String]): Unit = {
    var parsingStatsFile: Option[String] = None
    var listParsers: Boolean = false
    var listNormalizers: Boolean = false
    var list: List[String] = args
    var verbose: Boolean = false
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--verbose" =>
          verbose = true
        case "--list-parsers" =>
          listParsers = true
        case "--list-normalizers" =>
          listNormalizers = true
        case "--parsing-stats" =>
          if (list.isEmpty) {
            println(s"Error: missing out file name after --parsing-stats. $usage")
            return
          }
          parsingStatsFile = Some(list.head)
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg in info command. $usage")
          return
      }
    }
    if (listParsers) {
      println("== Parsers ==")
      for ((name, parser) <- parsers.AllParsers.knownParsers) {
        val dot = if (parsers.AllParsers.defaultParserCollection.parsers.contains(name))
          "+"
        else
          "-"
        println(s"$dot $name")
        if (verbose) {
          print("  ")
          println(JsonUtils.prettyStr(parser.parserInfo, extraIndent = 2))
        }
      }
    }
    if (listNormalizers) {
      println("== Normalizers ==")
      for ((name, normalizer) <- AllNormalizers.knownNormalizers) {
        val dot = if (AllNormalizers.activeNormalizers.contains(name))
          "+"
        else
          "-"
        println(s"$dot $name")
        if (verbose) {
          print("  ")
          println(JsonUtils.prettyStr(normalizer.info, extraIndent = 2))
        }
      }
    }
    parsingStatsFile match {
      case Some(outFName) =>
        statsDb match {
          case Some(db) =>
            val stats = new HighLevelStats(db, HighLevelStats.CollectionParam())
            stats.collectAll()
            val w = new java.io.FileWriter(outFName + ".json")
            JsonUtils.prettyWriter(stats.toJValue, w)
            w.close()
            val w2 = new java.io.FileWriter(outFName + ".html")
            stats.writeHtml(w2)
            w2.close()
          case None =>
            print("Error: cannot connect to the db, and thus cannot gather stats")
        }
      case None => ()
    }
  }

  /**
   * Implements the parse command of the command line
   */
  def parseCmd(args: List[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    var mainFilePath: Option[String] = None
    var mainFileUri: Option[String] = None
    var parsersToUse: ListBuffer[String] = ListBuffer()
    var testPipeline: Boolean = false
    var verbose: Boolean = false
    var backendType: BackendType.Enum = BackendType.JsonEventEmitter
    var pathInArchive: Option[String] = None
    var parsingOutFile: Option[String] = None
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--test-pipeline" =>
          testPipeline = true
        case "--main-file-path" =>
          if (list.isEmpty) {
            println(s"Error: missing main meta file. $usage")
            return
          }
          mainFilePath = Some(list.head)
          list = list.tail
        case "--main-file-uri" =>
          if (list.isEmpty) {
            println(s"Error: missing main meta file. $usage")
            return
          }
          mainFileUri = Some(list.head)
          list = list.tail
        case "--parser" =>
          if (list.isEmpty) {
            println(s"Error: missing parser name after --parser. $usage")
            return
          }
          parsersToUse.append(list.head)
          list = list.tail
        case "--verbose" =>
          verbose = true
        case "--json" =>
          backendType = BackendType.JsonWriter
        case "--h5" | "--hdf5" =>
          backendType = BackendType.Hdf5
        case "--avro" | "--avro-events" =>
          backendType = BackendType.AvroEvents
        case "--json-events" =>
          backendType = BackendType.JsonEventEmitter
        case "--parsing-out-file" =>
          if (list.isEmpty) {
            println(s"Error: missing out file path after --parsing-out-file. $usage")
            return
          }
          parsingOutFile = Some(list.head)
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg in parse command. $usage")
          return
      }
    }
    if (verbose) {
      ParserBackendBase.trace = true
    }
    val parserCollection = if (parsersToUse.isEmpty) {
      parsers.AllParsers.defaultParserCollection
    } else {
      new parsers.ParserCollection(parsersToUse.map { (parserName: String) =>
        parserName -> parsers.AllParsers.knownParsers(parserName)
      }(breakOut): Map[String, parsers.ParserGenerator])
    }
    val rawDataUriRe = "nmd://(R[-_a-zA-Z0-9]{28})(/.*)?$".r
    val parsedFileGidRe = "nmd://(P[-_a-zA-Z0-9]{28})/?$".r
    val calculationGidRe = "nmd://(C[-_a-zA-Z0-9]{28})/?$".r
    /* This option also you to test the setup exactly the way it is run through the pipeline; If the main-file-path is not passed then it is generated with the default path*/
    if (testPipeline) {
      /**
       * The settings required to get the parsed and uncompress Root
       */
      class Settings(config: Config) {
        // validate vs. reference.conf
        config.checkValid(ConfigFactory.defaultReference(), "simple-lib")
        val uncompressRoot = config.getString("nomad_lab.parser_worker_rabbitmq.uncompressRoot")
        val parsedJsonPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedJsonPath")
        val parsedCalculationH5Path = config.getString("nomad_lab.parser_worker_rabbitmq.parsedCalculationH5Path")
        def toJValue: JValue = {
          import org.json4s.JsonDSL._
          (("uncompressRoot" -> uncompressRoot) ~
            ("parsedJsonPath" -> parsedJsonPath))
        }
      }
      val settings = new Settings(LocalEnv.defaultConfig)
      mainFileUri match {
        case Some(uri) =>
          mainFilePath match {
            case None =>
              val fileUriRe = "file://(.+)".r

              def handleRawDataUri(archiveGid: String, path: String): Unit = {
                val repl = lab.LocalEnv.defaultSettings.replacements ++ Map(
                  ("archiveGid" -> archiveGid),
                  ("archiveGidPrefix" -> archiveGid.slice(0, 3))
                )
                mainFilePath = Some(lab.LocalEnv.makeReplacements(repl, lab.LocalEnv.defaultSettings.rawDataArchives))
                pathInArchive = Some(pathInArchive.getOrElse(Paths.get(archiveGid, if (path == null) "/" else path).toString()))
              }

              def handleFileUri(path: String): Unit = {
                mainFileUri = Some(Paths.get(path).toAbsolutePath.toString)
              }

              def handleCalculationGid(calculationGid: String): Unit = {
                statsDb match {
                  case Some(db) =>
                    db.calculationInfoFetch(calculationGid) match {
                      case Some(calc) =>
                        logger.info(s"Resolving $uri to ${calc.calculationUri}")
                        mainFileUri = Some(calc.calculationUri)
                        calc.calculationUri match {
                          case rawDataUriRe(archiveGid, path) =>
                            handleRawDataUri(archiveGid, path)
                          case fileUriRe(path) =>
                            handleFileUri(path)
                          case _ =>
                            throw new CLIException(s"Cannot resolve uri ${calc.calculationUri} expanded from $uri")
                        }
                      case None =>
                        throw new CLIException(s"Could not resolve calculationGid $calculationGid, as it is not in the parsing stats db (never parsed?)")
                    }
                  case None =>
                    throw new CLIException(s"Cannot resolve calculationGid $calculationGid without a connection to the db.")
                }
              }

              uri match {
                case fileUriRe(path) =>
                  mainFilePath = Some(Paths.get(path).toAbsolutePath.toString)
                case rawDataUriRe(archiveGid, path) =>
                  handleRawDataUri(archiveGid, path)
                case parsedFileGidRe(parsedFileGid) =>
                  handleCalculationGid("C" + parsedFileGid.drop(1))
                case calculationGidRe(calculationGid) =>
                  handleCalculationGid(calculationGid)
                case _ =>
                  throw new CLIException(s"Unknown URI type $uri, can't generate path for it")
              }
            case Some(path) => ()
          }
        case None =>
          mainFilePath match {
            case Some(filePath) =>
              val path = Paths.get(filePath)
              val filename = path.getFileName.toString
              val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
              filename match {
                case archiveRe(gid) =>
                  mainFileUri = Some(s"nmd://$gid/data")
                  pathInArchive = Some(pathInArchive.getOrElse(s"$gid/data"))
                case _ =>
                  mainFileUri = Some("file://" + path.toAbsolutePath().toString())
              }
            case None =>
              throw new CLIException("Both main-file-path and main-file-uri can't be empty. Please specify either uri or path to test the queue")
          }
      }
      val message = TreeParserRequest(
        treeUri = mainFileUri.getOrElse(""),
        treeFilePath = mainFilePath.getOrElse(""),
        treeType = if (mainFilePath.getOrElse("").endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
        relativeTreeFilePath = pathInArchive
      )
      val treeParser = new TreeParser(
        parserCollection = parserCollection
      )
      val calculationParser = new CalculationParser(
        ucRoot = settings.uncompressRoot,
        backendGenerator = if (backendType == BackendType.Hdf5)
        CalculationParser.jsonH5Generator
      else
        CalculationParser.jsonGenerator,
        parserCollection = parsers.AllParsers.defaultParserCollection,
        replacements = LocalEnv.defaultSettings.replacements
      )

      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        val msg: CalculationParserRequest = TreeParser.createCalculationParserRequest(parserAssociation)
        val res: CalculationParserResult = calculationParser.handleParseRequest(msg)
        logger.info(s"Done parsing.. ${lab.JsonSupport.writePrettyStr(res)}")
      }
      treeParser.findParserAndPublish(publish, message)
    } else // Call parser directly without creating pipeline messages
      mainFilePath match {
        case Some(path) =>
          val uri = mainFileUri match {
            case Some(mainUri) => mainUri
            case None => ("file://" + path)
          }
          logger.info(s"will parse $path with uri $uri")
          val cachedOptimizedParsers: mutable.Map[String, parsers.OptimizedParser] = mutable.Map()
          val fIn = new java.io.FileInputStream(path)
          val buf = Array.fill[Byte](8 * 1024)(0)
          val nRead = parserCollection.tryRead(fIn, buf, 0)
          val minBuf = buf.dropRight(buf.size - nRead)
          var possibleParsers = parserCollection.scanFile(path, minBuf).sorted
          if (possibleParsers.isEmpty && parserCollection.parsers.size == 1) {
            val (pName, pAtt) = parserCollection.parsers.head
            logger.warn(s"file $path did not match parser $pName, but as it is the sole parser available, forcing its use.")
            possibleParsers = Seq(parsers.CandidateParser(parsers.ParserMatch(0, false), pName, pAtt))
          }
          if (!possibleParsers.isEmpty) {
            val parsers.CandidateParser(parserMatch, parserName, parser) = possibleParsers.head
            val optimizedParser: parsers.OptimizedParser = {
              cachedOptimizedParsers.get(parserName) match {
                case Some(oParser) =>
                  oParser
                case None =>
                  val oParser = parser.optimizedParser(Seq())
                  cachedOptimizedParsers += (parserName -> oParser)
                  oParser
              }
            }
            val intBackend: parsers.ParserBackendInternal = backendType match {
              case BackendType.JsonWriter => null
              case BackendType.Hdf5 =>
                val calcSha = CompactSha()
                calcSha.updateStr(uri)
                val parsedFileId = calcSha.gidStr("c")
                val groupPath = uri match {
                  case rawDataUriRe(archiveGid, path) =>
                    s"/$archiveGid/$parsedFileId"
                  case _ =>
                    s"/unknown/$parsedFileId"
                }
                val filePath = parsingOutFile match {
                  case Some(path) => Paths.get(path)
                  case None =>
                    java.nio.file.Files.createTempFile("parsing", ".h5")
                }
                println(s"Writing HDF5 file to $filePath")
                val h5File = parsers.H5Backend.H5File.create(filePath, Paths.get(groupPath))
                parsers.H5Backend(optimizedParser.parseableMetaInfo, h5File)
              case BackendType.JsonEventEmitter => null
              case BackendType.AvroEvents => null
            }
            val extBackend: parsers.ParserBackendExternal = backendType match {
              case BackendType.JsonWriter =>
                val outF = parsingOutFile match {
                  case Some(p) =>
                    new java.io.FileWriter(p)
                  case None =>
                    new java.io.OutputStreamWriter(System.out)
                }
                new parsers.JsonWriterBackend(optimizedParser.parseableMetaInfo, outF)
              case BackendType.Hdf5 => null
              case BackendType.JsonEventEmitter =>
                val outF = parsingOutFile match {
                  case Some(p) =>
                    new java.io.FileWriter(p)
                  case None =>
                    new java.io.OutputStreamWriter(System.out)
                }
                new parsers.JsonParseEventsWriterBackend(optimizedParser.parseableMetaInfo, outF)
              case BackendType.AvroEvents =>
                val outF = parsingOutFile match {
                  case Some(p) =>
                    new java.io.File(p)
                  case None =>
                    new java.io.File("out.avro")
                }
                new parsers.AvroParseEventsWriterBackend(optimizedParser.parseableMetaInfo, outF)
            }
            val parsingStatus = if (intBackend != null)
              optimizedParser.parseInternal(uri, Paths.get(path), intBackend, parserName)
            else if (extBackend != null)
              optimizedParser.parseExternal(uri, Paths.get(path), extBackend, parserName)
            else
              throw new Exception("no backend")
            parsingStatus match {
              case _ => ()
            }
          }
        case None => ()
      }
  }

  /**
   * Implements the merge command of the command line
   */
  def mergeCmd(args: List[String]): Unit = {
    var mergeTarget: Option[String] = None
    var mergeFiles: List[String] = Nil
    var appendToMerge: Boolean = false
    var list = args
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--merge-h5" =>
          if (list.isEmpty) {
            println(s"Error: missing target h5 file after --merge-h5. $usage")
            return
          }
          mergeTarget = Some(list.head)
          mergeFiles = list.tail
          list = Nil
        case "--append-to-merge" =>
          appendToMerge = true
        case _ =>
          println(s"Error: unexpected argument $arg in merge command. $usage")
          return
      }
    }
    mergeTarget match {
      case Some(target) =>
        logger.info(s"will merge $mergeFiles -> $target")
        val targetPath = Paths.get(target)
        val targetF = if (appendToMerge && Files.exists(targetPath))
          h5.FileH5.open(targetPath, write = true)
        else
          h5.FileH5.create(targetPath)
        try {
          for (inF <- mergeFiles) {
            val sourceF = h5.FileH5.open(Paths.get(inF))
            try {
              lab.h5.Merge.fileInFile(sourceF, targetF)
            } finally {
              sourceF.release()
            }
          }
        } finally {
          targetF.release()
        }
      case None => ()
    }
  }

  /**
   * Implements the meta command of the command line
   */
  def metaCmd(args: List[String]): Unit = {
    var list = args
    var metaInfoDotFile: Option[String] = None
    val classLoader: ClassLoader = getClass().getClassLoader();
    var metaInfoPath = classLoader.getResource("nomad_meta_info/main.nomadmetainfo.json").getFile()
    var removeUnintresting: Boolean = false
    var removeMeta: Boolean = false
    var removeDimensions: Boolean = false
    var sectionParents: Boolean = true
    var abstractParents: Boolean = true
    var metaUrlBase: String = "http://localhost:8081/"
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--main-meta-file" =>
          if (list.isEmpty) {
            println(s"Error: missing main meta file after --main-meta-file. $usage")
            return
          }
          metaInfoPath = list.head
          list = list.tail
        case "--meta-dot-file" =>
          if (list.isEmpty) {
            println(s"Error: missing place to put the dot file for the meta information after --meta-dot-file. $usage")
            return
          }
          metaInfoDotFile = Some(list.head)
          list = list.tail
        case "--meta-remove-unintresting" =>
          removeUnintresting = true
        case "--meta-remove-meta" =>
          removeMeta = true
        case "--meta-section-only" =>
          removeDimensions = true
          sectionParents = true
          abstractParents = false
        case "--meta-abstract-only" =>
          removeDimensions = true
          sectionParents = false
          abstractParents = true
        case "--meta-base-url" =>
          if (list.isEmpty) {
            println(s"Error: missing meta url after --meta-base-url. $usage")
            return
          }
          metaUrlBase = list.head
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg in command meta. $usage")
          return
      }
    }
    metaInfoDotFile match {
      case Some(outPath) =>
        val resolver = new meta.RelativeDependencyResolver
        val mainEnv = meta.SimpleMetaInfoEnv.fromFilePath(metaInfoPath, resolver)
        val w = new java.io.FileWriter(outPath)
        mainEnv.writeDot(w, meta.MetaInfoEnv.DotParams(
          removeUnintresting = removeUnintresting,
          removeMeta = removeMeta,
          sectionParents = sectionParents,
          abstractParents = abstractParents,
          abstractOnly = abstractParents && !sectionParents,
          removeDimensions = removeDimensions,
          urlBase = metaUrlBase
        ))
        w.close()
      case None => ()
    }
  }
}
