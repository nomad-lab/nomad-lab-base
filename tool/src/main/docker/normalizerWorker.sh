#!/bin/bash

cmd="java -Djava.library.path=/lib -jar /app/tool.jar"
baseName=toNormalize-
globPattern="??"
normalizers=SectionSystemNormalizer,MetaIndexRebuildNormalizer,SymmetryNormalizer,MetaIndexRebuildNormalizer,CalculationInfoNormalizer,UploadInfoNormalizer,FhiDosNormalizer,FhiAimsBasisNormalizer,SpringerNormalizer,SystemTypeNormalizer,PrototypesNormalizer,MetaIndexRebuildNormalizer
cmdArgs="normalize --archive-uri nmd://\$n --normalizers \$normalizers"

if [ -e /config.sh ] ; then
   source /config.sh
fi
cd /work
for f in /work/$baseName$globPattern ; do
  (
  if [[ -e "$f" ]] ; then
  for n in $( cat $f ); do
    eval "$cmd $cmdArgs &> out-\${n}.txt"
  done
  fi
) &
done
wait
