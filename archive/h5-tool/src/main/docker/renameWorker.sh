#!/bin/bash

cmd="java -Djava.library.path=/lib -jar /app/h5Tool.jar"
baseName=${baseName:-toRename-}
targetDir=${targetDir:---target-dir=/normalized/productionH5-patched}
globPattern=${globPattern:-??}
renames=${renames:---renames=/app/meta-values-replacements.txt}
cmdArgs=${cmdArgs:-rename  --fixes $renames $targetDir --source-paths-file=\$f}

if [ -e /config.sh ] ; then
   source /config.sh
fi
cd /work
for f in /work/$baseName$globPattern ; do
  (
  if [[ -e "$f" ]] ; then
      echo $cmd $cmdArgs  "out-$(basename $f).txt"
      eval $cmd $cmdArgs >& "out-$(basename $f).txt"
  fi
) &
done
wait
