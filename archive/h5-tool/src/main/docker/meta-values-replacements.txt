# section_run/program_basis_set_type
 MM -> none
 Gaussians -> GTOs
 gaussians -> GTOs
 "Real-space grid" -> "real-space grid"
# section_calculation_info/calculation_uploader_name
 "danilo brambila" -> "Danilo Brambila"
 "-" -> none
 "–" -> none
# section_run/section_method/section_XC_functionals/XC_functional_name
 x_qe_VDW_DF1    -> vDW_DF
 x_qe_VDW_DF2    -> vDW_DF2
 x_qe_GGA_X_GAUP    ->     HYB_GGA_XC_GAU_PBE
