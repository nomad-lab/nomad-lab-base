/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.RouteConcatenation.concat
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }
import eu.nomad_lab.webservice_base.ElasticSearchWebservice
import io.swagger.models.Scheme

import scala.concurrent.{ ExecutionContext, Future }

private object ArchiveService extends App with ElasticSearchWebservice {

  implicit val system: ActorSystem = ActorSystem("archiveAkkaHttpServer", config)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher
  lazy val config: Config = LocalEnv.defaultConfig
  lazy val metaData: MetaInfoEnv = KnownMetaInfoEnvs.archive

  override val apiDocumentationClasses: Set[Class[_]] = Set(classOf[OldApi], classOf[NqlApi])
  override val skipDefinitions = Seq()
  override val apiDescription = "Explore and visualize the available data in the NOMAD Archive."
  override val apiVersion = "0.1"
  override val apiTitle = "NOMAD Archive API"

  private val oldApi = OldApi(es)
  private val nqlApi = NqlApi(es)

  override def serviceRoute = concat(nqlApi.routes, oldApi.routes)

  val interface = config.getString("nomad_lab.archive.webservice.interface")
  val port = config.getInt("nomad_lab.archive.webservice.port")
  val rootPathstring = config.getString("nomad_lab.archive.webservice.rootPath")
  val hostName = config.getString("nomad_lab.archive.webservice.hostName")
  val protocol = config.getString("nomad_lab.archive.webservice.protocol") match {
    case "" => if (hostName.startsWith("localhost:")) Scheme.HTTP else Scheme.HTTPS
    case "http://" | "http" => Scheme.HTTP
    case "https://" | "https" => Scheme.HTTPS
    case x => throw new UnsupportedOperationException(s"protocol $x is not supported!")
  }
  val fullRoute = fullRoutes(
    serviceBaseName = "archive",
    rootPath = rootPathstring.split("/").filter(_ != ""),
    hostName = hostName,
    includeSwagger = true,
    protocol = protocol
  )
  val serverBindingFuture: Future[ServerBinding] = Http().bindAndHandle(
    fullRoute, interface = interface, port = port
  )

  println(s"Server online at http://$interface:$port/\n")
}
