/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive.queries

import java.util.NoSuchElementException

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.searches.SearchDefinition
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.elasticsearch.{ ApiCallException, ConnectorElasticSearch, ESManager }
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.webservice_base.PagingFilter
import eu.nomad_lab.webservice_base.ResponseFormatter.{ ResponseData, flattenMapKeys }
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.{ JArray, JValue }
import org.{ json4s => jn }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object StandardSearch {

  object Formats extends Enumeration {
    val nested = Value
    val flat = Value

    def getFormat(value: String): Value = {
      try {
        Formats.withName(value)
      } catch {
        case _: NoSuchElementException => throw new ApiCallException(
          s"invalid format specifier, allowed values: ${this.values.mkString(",")}"
        )
      }
    }
  }

  case class CalculationResult(idStr: String, data: Map[String, Any]) extends BaseValue {
    override def typeStr = "Calculation"
    override def attributes: Map[String, jn.JValue] = Map("metadata" -> decompose(data))
  }

  case class GroupedCalculationResult(idStr: String, groupTerm: String,
      data: Map[String, Any]) extends BaseValue {
    override def typeStr = "GroupedCalculation"
    override def attributes: Map[String, jn.JValue] = {
      Map(
        "metadata" -> decompose(data),
        "group" -> jn.JString(groupTerm)
      )
    }
  }
}

class StandardSearch(private val es: ESManager) {
  import StandardSearch._

  /**
   * Return a list of calculations that match the specified search criteria.
   * @param representation the search query for ElasticSearch
   * @param paging sort and paging information
   * @param count number of results in the returned list
   * @param sourceFields fields to include in the returned sources, if none are specified all
   *                     non-hidden fields will be included in the response
   * @param format meta-data output formats
   * @return list of results and query meta data
   */
  def findMatchingCalculations(
    representation: SearchDefinition,
    paging: PagingFilter, count: Int, sourceFields: Seq[String],
    format: Formats.Value
  ): Future[ResponseData[CalculationResult]] = {
    import es.connector.metaNameToField

    es.connector.validateFields(sourceFields, ConnectorElasticSearch.FieldUsage.SourceFilter)

    val query = representation.size(count).sourceFiltering(
      includes = sourceFields.map(metaNameToField),
      excludes = Seq("query_auxiliary_data")
    ).sortBy(paging.getSortDefinition(es.connector)).
      aggs(
        sumAgg("total_single_configs", metaNameToField("number_of_single_configurations")),
        cardinalityAgg("total_unique_geometries", metaNameToField("configuration_raw_gid"))
      )
    es.client.execute(query).map { results =>
      val calculations = results.hits.hits.map { x =>
        val data = format match {
          case Formats.flat => flattenMapKeys("", x.sourceAsMap)
          case Formats.nested => x.sourceAsMap
        }
        CalculationResult(x.id, data)
      }
      val meta0 = Map[String, jn.JValue](
        "total_calculations" -> decompose(results.totalHits),
        "total_single_config_calculations" ->
          decompose(results.sumAgg("total_single_configs").value.toLong),
        "total_unique_geometries" -> decompose(
          results.aggregationsAsMap("total_unique_geometries").asInstanceOf[Map[String, Any]].
            getOrElse("value", 0L)
        ),
        "is_timed_out" -> decompose(results.isTimedOut),
        "is_terminated_early" -> decompose(results.isTerminatedEarly)
      )
      ResponseData(calculations, meta0)
    }
  }

  /**
   * *Return a list of calculations that match the specified search criteria. To diversify the
   * set of results, the results are aggregated by the given field and a batch of results is then
   * taken from each bucket of the aggregation.
   * @param representation the search query for ElasticSearch
   * @param groupBy the meta-data field by which the results are grouped
   * @param numGroups number of groups to be returned
   * @param count number of results for each group
   * @param sourceFields fields to include in the returned sources, if none are specified all
   *                     non-hidden fields will be included in the response
   * @param format meta-data output format
   * @return list of results and query meta data
   */
  def findMatchingGroupedCalculations(representation: SearchDefinition, groupBy: String,
    numGroups: Int, count: Int, sourceFields: Seq[String],
    format: Formats.Value): Future[ResponseData[GroupedCalculationResult]] = {
    import es.connector.metaNameToField

    es.connector.validateFields(sourceFields, ConnectorElasticSearch.FieldUsage.SourceFilter)
    es.connector.validateFields(Seq(groupBy), ConnectorElasticSearch.FieldUsage.Aggregate)

    val query = representation.size(0).aggs(
      termsAgg("groups", metaNameToField(groupBy)).subAggregations(
        topHitsAggregation("samples").size(count).fetchSource(
          sourceFields.map(metaNameToField).toArray,
          Array("query_auxiliary_data")
        )
      ),
      sumAgg("total_single_configs", metaNameToField("number_of_single_configurations")),
      cardinalityAgg("total_unique_geometries", metaNameToField("configuration_raw_gid"))
    )
    es.client.execute(query).map { results =>
      val aggData = JsonUtils.parseStr(results.aggregationsAsString)
      val buckets = (aggData \ "groups" \ "buckets").extract[Seq[JValue]]
      val calculations = buckets.flatMap { bucket =>
        val documents = (bucket \ "samples" \ "hits" \ "hits").asInstanceOf[JArray]
        documents.arr.map { entry =>
          val rawData = (entry \ "_source").extract[Map[String, Any]]
          val data = format match {
            case Formats.flat => flattenMapKeys("", rawData)
            case Formats.nested => rawData
          }
          GroupedCalculationResult(
            (entry \ "_id").extract[String],
            (bucket \ "key").extract[String], data
          )
        }
      }
      val meta0 = Map[String, jn.JValue](
        "total_calculations" -> decompose(results.totalHits),
        "total_single_config_calculations" ->
          decompose(results.sumAgg("total_single_configs").value.toLong),
        "total_unique_geometries" -> decompose(
          results.aggregationsAsMap("total_unique_geometries").asInstanceOf[Map[String, Any]].
            getOrElse("value", 0L)
        ),
        "is_timed_out" -> decompose(results.isTimedOut),
        "is_terminated_early" -> decompose(results.isTerminatedEarly)
      )
      ResponseData(calculations, meta0)
    }
  }
}
