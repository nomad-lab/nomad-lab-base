/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive.queries

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.searches.SearchDefinition
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.json4s.JValue
import org.{ json4s => jn }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object SummaryStatistics {

  case class StatsSummary(
      queryString: String,
      numUniqueGeometries: Long,
      numSinglePointConfigs: Long,
      numCalculations: Long,
      systemTypeStats: Map[String, Map[String, Long]]
  ) extends BaseValue {
    override def typeStr = "StatsSummary"

    override def idStr: String = queryString

    override def attributes: Map[String, jn.JValue] = {
      Map(
        "total_unique_geometries" -> jn.JLong(numUniqueGeometries),
        "total_single_configurations" -> jn.JLong(numSinglePointConfigs),
        "total_hits" -> jn.JLong(numCalculations),
        "systemtype_statistics" -> decompose(systemTypeStats)
      )
    }
  }

}

class SummaryStatistics(private val es: ESManager) {
  import SummaryStatistics._

  /**
   * Get simple statistics about the calculations matching the given filter. This function returns
   * the number of unique geometries, single-point calculations, program_runs and statistics about
   * the system types.
   *
   * @param representation the base query to operate on (for ElasticSearch)
   * @param queryString only used for tagging the returned object with a unique ID
   * @return simple statistics for the matched calculations
   */
  def getSimpleStatistics(
    representation: SearchDefinition,
    queryString: String
  ): Future[ResponseData[StatsSummary]] = {
    import es.connector.metaNameToField

    val query = representation.size(0).aggregations(
      cardinalityAgg("geometries", metaNameToField("configuration_raw_gid")),
      sumAgg("configs", metaNameToField("number_of_single_configurations")),
      termsAgg("systemtype_statistics", metaNameToField("system_type")).size(10).subAggregations(
        cardinalityAgg("geometries", metaNameToField("configuration_raw_gid")),
        sumAgg("configs", metaNameToField("number_of_single_configurations"))
      )
    )
    def extractSystemData(rawData: JValue): Map[String, Map[String, Long]] = {
      val rawMap = rawData.children.map { bucket =>
        (bucket \ "key").extract[String] -> Map(
          "total_unique_geometries" -> (bucket \ "geometries" \ "value").extract[Long],
          "total_single_point_configs" -> (bucket \ "configs" \ "value").extract[Long],
          "number_calculations" -> (bucket \ "doc_count").extract[Long]
        )
      }.toMap
      var results: Map[String, Map[String, Long]] = Map()
      rawMap.foreach { x =>
        x._2.foreach { y =>
          val subMap = results.getOrElse(y._1, Map()) + (x._1 -> y._2)
          results = results + (y._1 -> subMap)
        }
      }
      results
    }
    es.client.execute(query).map { results =>
      val aggData = JsonUtils.parseStr(results.aggregationsAsString)
      val data = StatsSummary(
        queryString,
        numUniqueGeometries = (aggData \ "geometries" \ "value").extract[Long],
        numSinglePointConfigs = (aggData \ "configs" \ "value").extract[Long],
        numCalculations = results.totalHits,
        systemTypeStats = extractSystemData(aggData \ "systemtype_statistics" \ "buckets")
      )
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(results.totalHits),
        "is_timed_out" -> decompose(results.isTimedOut),
        "is_terminated_early" -> decompose(results.isTerminatedEarly)
      )
      ResponseData(Seq(data), meta0)
    }
  }
}

