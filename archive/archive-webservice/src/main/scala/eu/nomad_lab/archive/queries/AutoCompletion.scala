/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive.queries

import cats.data
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.searches.SearchDefinition
import eu.nomad_lab.{ JsonSupport, JsonUtils }
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.elasticsearch.{ ConnectorElasticSearch, ESManager }
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.{ JNothing, JValue }
import org.{ json4s => jn }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object AutoCompletion {
  case class UniqueValues(idStr: String, data: Map[String, Int]) extends BaseValue {
    override def typeStr = "UniqueValues"
    override def attributes: Map[String, jn.JValue] = Map("top_values" -> decompose(data))
  }
}

class AutoCompletion(private val es: ESManager) {
  import AutoCompletion._

  /**
   * Obtain a list of autocomplete-suggestions for the specified fields. The indexed non-numeric
   * meta-data fields in the ElasticSearch are usually keyword fields with a limited cardinality,
   * thus this simple auto-completion strategy ignoring the already typed user-input is sufficient
   * for most cases. If a field has more unique values than the requested result count, the most
   * frequently used terms will be returned.
   * @param representation the search query for ElasticSearch
   * @param fields the fields for which the suggestions should be returned
   * @param nBuckets number of results to be returned for each field
   * @return list of results and query meta data
   */
  def findTopValues(representation: SearchDefinition, fields: Seq[String],
    nBuckets: Int): Future[ResponseData[UniqueValues]] = {
    import es.connector.metaNameToField

    es.connector.validateFields(fields, ConnectorElasticSearch.FieldUsage.Aggregate)

    val query = representation.size(0).aggs {
      fields.map(field => termsAgg(s"statistics_$field", metaNameToField(field)).size(nBuckets))
    }
    es.client.execute(query).map { results =>
      val topValues = fields.map { field =>
        val agg = JsonUtils.parseStr(results.aggregationsAsString) \ s"statistics_$field" \ "buckets"
        val data = agg.children.map { entry =>
          val key = entry \ "key_as_string" match {
            case JNothing => entry \ "key"
            case x => x
          }
          key.extract[String] -> (entry \ "doc_count").extract[Int]
        }
        UniqueValues(field, data.toMap)
      }
      val meta0 = Map[String, jn.JValue](
        "total_calculations" -> decompose(results.totalHits),
        "is_timed_out" -> decompose(results.isTimedOut),
        "is_terminated_early" -> decompose(results.isTerminatedEarly)
      )
      ResponseData(topValues, meta0)
    }
  }

}
