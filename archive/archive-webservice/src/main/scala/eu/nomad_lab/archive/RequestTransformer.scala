/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive

import eu.nomad_lab.Composition
import eu.nomad_lab.query.{ AndConjunction, AtomicTerm, QueryExpression }
import org.json4s
import org.json4s.JString

object RequestTransformer {

  private val customFieldAnalysis = Map[String, (Seq[json4s.JValue]) => Seq[json4s.JValue]](
    "system_composition" -> { values =>
      import Composition._
      values.map { x =>
        val composition = formulaStrToDict(x.asInstanceOf[JString].values)
        JString(dictToFormulaStr(composition))
      }
    },
    "system_reweighted_composition" -> { values =>
      import Composition._
      values.map { x =>
        val composition = formulaStrToDict(x.asInstanceOf[JString].values)
        JString(dictToFormulaStr(formulaDictReweighted(composition)))
      }
    }
  )

  /**
   * applies custom field analysis to some fields of the search, e.g. reweighed system compositions
   * are normalized to ensure that the user will find all appropriate matches regardless of the
   * factors specified in the composition.
   * @param query the original NQL query expression
   * @return the NQL query expression with analysed values
   */
  def analyseNQLRequest(query: QueryExpression): QueryExpression = {
    def applyAnalysis(term: AtomicTerm): AtomicTerm = {
      val func = customFieldAnalysis.get(term.metaName)
      if (func.nonEmpty)
        term.copy(values = func.get(term.values))
      else
        term
    }

    query match {
      case x: AtomicTerm => applyAnalysis(x)
      case AndConjunction(terms) => AndConjunction(terms.map(applyAnalysis))
    }
  }

  /**
   * Transform the simple query language used by the Nomad search Beaker GUI into a NQL statement.
   * format.
   * @param simpleString the simple query language string
   * @return
   */
  def transformSimpleRequest(simpleString: Option[String]): Option[String] = {
    val queryFilters = simpleString match {
      case None => Seq()
      case Some(x) =>
        x.split("AND").map { condition =>
          val parts = condition.split(":").map(_.trim)
          val values = parts(1).split(",").map(x => s"""\"$x\"""").reduce((x, y) => x + "," + y)
          parts(0) match {
            // TODO: remap to NQL existence query
            case "stats_meta_present" => s"alltarget stats_meta_present = $values"
            case "atom_species" => s"alltarget atom_species = $values"
            case "atom_symbols" => s"alltarget atom_symbols = $values"
            case "program_name" => s"alltarget program_name = $values"
            case "system_composition" => s"alltarget system_composition = $values"
            case "system_reweighted_composition" => s"alltarget system_reweighted_composition = $values"
            case "uploader" => s"alltarget uploader = $values"
            case "section_repository_info.repository_basis_set_type" => s"alltarget program_basis_set_type = $values"
            case "section_repository_info.repository_crystal_system" => s"alltarget crystal_system = $values"
            case "section_repository_info.repository_spacegroup_nr" => s"alltarget space_group_number = $values"
            case "section_repository_info.repository_system_type" => s"alltarget system_type = $values"
            case "section_repository_info.repository_xc_treatment" => s"alltarget electronic_structure_method = $values"
            case y => throw new UnsupportedOperationException(s"unknown metadata query: $y")
          }
        }.toSeq
    }
    queryFilters.length match {
      case 0 => None
      case 1 => Some(queryFilters.head)
      case _ => Some(queryFilters.tail.fold(queryFilters.head)((x, y) => x + " AND " + y))
    }
  }
}
