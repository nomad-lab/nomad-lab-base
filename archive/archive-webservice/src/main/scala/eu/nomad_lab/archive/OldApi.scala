/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive

import javax.ws.rs.{ GET, Path }

import akka.http.scaladsl.server.{ Directives, Route }
import eu.nomad_lab.archive.queries.StandardSearch.Formats
import eu.nomad_lab.archive.queries._
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.query.QueryExpression
import eu.nomad_lab.webservice_base.PagingFilter
import io.swagger.annotations._

import scala.concurrent.{ ExecutionContext, Future }

object OldApi {
  def apply(es: ESManager)(implicit exec: ExecutionContext): OldApi = {
    val autoComplete = new AutoCompletion(es)
    val crossFilter = new CrossFilterStatistics(es)
    val histograms = new Histograms(es)
    val standardSearch = new StandardSearch(es)
    val stats = new SummaryStatistics(es)
    apply(es, autoComplete, crossFilter, histograms, standardSearch, stats)
  }

  protected def apply(es: ESManager, autoComplete: AutoCompletion, crossFilter: CrossFilterStatistics,
    histograms: Histograms, standardSearch: StandardSearch,
    stats: SummaryStatistics)(implicit exec: ExecutionContext): OldApi = {
    new OldApi(es, autoComplete, crossFilter, histograms, standardSearch, stats)
  }
}

@Api(value = "NOMAD Archive ElasticSearch with depreciated simple query language", produces = "vnd.api+json")
@Path("/old-api")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class OldApi private (
    private val es: ESManager,
    private val autoComplete: AutoCompletion,
    private val crossFilter: CrossFilterStatistics,
    private val histograms: Histograms,
    private val standardSearch: StandardSearch,
    private val stats: SummaryStatistics
)(implicit exec: ExecutionContext) extends Directives {
  import es.connector
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult

  implicit val queryTransform: Option[QueryExpression => QueryExpression] = Some(RequestTransformer.analyseNQLRequest)

  val routes: Route = pathPrefix("old-api") {
    concat(
      calculationQuery,
      calculationQueryGrouped,
      uniqueValues,
      queryStatistics,
      crossFilteringStatistics
    )
  }

  @Deprecated @GET @Path("/query")
  @ApiOperation(
    value = "Search individual calculations in the Nomad Archive",
    nickname = "old/query"
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "query", paramType = "query", dataType = "string",
      value = "query expression for filtering calculations as simple query language"),
    new ApiImplicitParam(name = "num_results", paramType = "query", dataType = "integer",
      value = "number of calculations to return", defaultValue = "100"),
    new ApiImplicitParam(name = "offset", paramType = "query", dataType = "integer",
      defaultValue = "0", value = "skip the first N results (NO LONGER SUPPORTED, WILL BE IGNORED)"),
    new ApiImplicitParam(name = "source_fields", paramType = "query", dataType = "string",
      value = "optional comma-separated list to filter meta-data fields included in the response"),
    new ApiImplicitParam(name = "format", paramType = "query", dataType = "string",
      allowableValues = "flat, nested", defaultValue = "nested",
      value = "output format of the meta-data for each returned calculation")
  ))
  def calculationQuery: Route = path("query") {
    get {
      parameters('query.?, 'num_results.as[Int] ? 100, 'offset.as[Int] ? 0,
        'source_fields ? "", 'format ? "nested") {
          (simpleQuery, nResults, offset, sourceFields, formatString) =>
            onComplete {
              val rawQuery = Future(RequestTransformer.transformSimpleRequest(simpleQuery))
              //Note: Offset isn't supported here anymore
              val pagingFilter = PagingFilter("calculation_gid", None, None)
              val query = rawQuery.map { raw =>
                val tmp = Seq(raw, pagingFilter.getPagingFilter).flatten.mkString(" AND ")
                connector.transformToQuery(if (tmp.nonEmpty) Some(tmp) else None)
              }
              val fields = sourceFields.split(",").toSeq.filter(_.length > 0)
              val format = Formats.getFormat(formatString)
              val result = query.map(standardSearch.findMatchingCalculations(
                _, pagingFilter, nResults, fields, format
              ))
              result.flatMap(identity)
            }(resolveQueryResult(_))
        }
    }
  }

  @Deprecated @GET @Path("/grouped_search")
  @ApiOperation(
    value = "Search individual calculations in the Nomad Archive and return a diversified sample",
    nickname = "old/grouped_search"
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "groupBy", paramType = "query", required = true, dataType = "string",
      value = "meta-data field used to group the matching calculations"),
    new ApiImplicitParam(name = "query", paramType = "query", dataType = "string",
      value = "query expression for filtering calculations as simple query language"),
    new ApiImplicitParam(name = "num_results", paramType = "query", dataType = "integer",
      defaultValue = "10", value = "number of calculations to return for each group"),
    new ApiImplicitParam(name = "num_groups", paramType = "query", dataType = "integer",
      defaultValue = "10", value = "number of distinct calculation groups to return"),
    new ApiImplicitParam(name = "source_fields", paramType = "query", dataType = "integer",
      value = "optional comma-separated list to filter meta-data fields included in the response"),
    new ApiImplicitParam(name = "format", paramType = "query", dataType = "string",
      allowableValues = "flat, nested", defaultValue = "nested",
      value = "output format of the meta-data for each returned calculation")
  ))
  def calculationQueryGrouped: Route = path("grouped_search") {
    get {
      parameters('groupBy, 'query.?, 'num_results.as[Int] ? 10, 'num_groups.as[Int] ? 10,
        'source_fields ? "", 'format ? "nested") {
          (groupByMetaName, simpleQuery, nResults, nGroups, sourceFields, formatString) =>

            onComplete {
              val query = Future(RequestTransformer.transformSimpleRequest(simpleQuery))
              val search = query.map(connector.transformToQuery(_))
              val fields = sourceFields.split(",").toSeq.filter(_.length > 0)
              val format = Formats.getFormat(formatString)
              val result = search.map(standardSearch.findMatchingGroupedCalculations(
                _, groupByMetaName, nGroups, nResults, fields, format
              ))
              result.flatMap(identity)
            }(resolveQueryResult(_))
        }
    }
  }

  @Deprecated @GET @Path("/autocompletion")
  @ApiOperation(
    value = "Return a collection of unique values for selected meta-data fields",
    notes = "Can be used for autocompletion on fields with limited cardinality.",
    nickname = "old/autocompletion"
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "fields", paramType = "query", required = true, dataType = "string",
      value = "meta-data fields for which unique values (after filtering) are requested"),
    new ApiImplicitParam(name = "query", paramType = "query", dataType = "string",
      value = "query expression for filtering calculations as simple query language"),
    new ApiImplicitParam(name = "num_groups", paramType = "query", dataType = "integer",
      defaultValue = "10", value = "number of unique values to return for each field")
  ))
  def uniqueValues: Route = path("autocompletion") {
    get {
      parameters('fields, 'query.?, 'num_groups.as[Int] ? 10) {
        (fields, simpleQuery, nGroups) =>
          onComplete {
            val query = Future(RequestTransformer.transformSimpleRequest(simpleQuery))
            val search = query.map(connector.transformToQuery(_))
            val aggFields = fields.split(",").toSeq.filter(_.length > 0)
            val result = search.map(autoComplete.findTopValues(_, aggFields, nGroups))
            result.flatMap(identity)
          }(resolveQueryResult(_))
      }
    }
  }

  @Deprecated @GET @Path("/statistics")
  @ApiOperation(
    value = "Provide statistics about the calculations matching the given query",
    nickname = "old/statistics"
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "query", paramType = "query", dataType = "string",
      value = "query expression for filtering calculations as simple query language"),
    new ApiImplicitParam(name = "num_buckets", paramType = "query", dataType = "integer",
      value = "number of values each field, uses field-specific defaults if not set")
  ))
  def queryStatistics: Route = path("statistics") {
    get {
      parameters('query.?, 'num_buckets.as[Int].?) {
        (simpleQuery, nBuckets) =>
          onComplete {
            val rawQuery = Future(RequestTransformer.transformSimpleRequest(simpleQuery))
            val query = rawQuery.map(connector.transformToQuery(_))
            val result = query.map(histograms.getStatistics(_, nBuckets = nBuckets))
            result.flatMap(identity)
          }(resolveQueryResult(_))
      }
    }
  }

  @Deprecated @GET @Path("/interactive_stats")
  @ApiOperation(
    value = "Provide pre-aggregated statistics for crossfiltering visualization",
    notes = "A calculation may be mapped to the multiple values for one field, this "
    + "pre-aggregation thus leads to potential double-counting.",
    nickname = "old/interactive_stats"
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "query", paramType = "query", dataType = "string",
      value = "query expression for filtering calculations as simple query language"),
    new ApiImplicitParam(name = "num_buckets", paramType = "query", dataType = "integer",
      value = "number of values each field, uses field-specific defaults if not set")
  ))
  def crossFilteringStatistics: Route = path("interactive_stats") {
    get {
      parameters('query.?, 'num_buckets.as[Int].?) {
        (simpleQuery, nBuckets) =>
          onComplete {
            val rawQuery = Future(RequestTransformer.transformSimpleRequest(simpleQuery))
            val query = rawQuery.map(connector.transformToQuery(_))
            val result = query.map(crossFilter.getStatistics(_, nBuckets = nBuckets))
            result.flatMap(identity)
          }(resolveQueryResult(_))
      }
    }
  }
}
