/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.mappings.dynamictemplate.DynamicMapping
import com.sksamuel.elastic4s.mappings.{ BasicFieldDefinition, FieldDefinition, MappingDefinition }
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.h5.CalculationH5
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }
import eu.nomad_lab.elasticsearch.ConnectorElasticSearch.metaNameToPathSegments
import org.joda.time.format.ISODateTimeFormat

/**
 * A manifest of the fields contained in the archive ElasticSearch index. For each field present
 * here we store the set of unique values in the Archive ES.
 * Besides listing the meta-data names, the manifest also contains additional information for
 * defining the ElasticSearch index mapping with proper settings.
 */
object IndexManifest {

  /**
   * ElasticSearch Index entry declaration
   * @param metaName name of the meta-info
   * @param fieldDataType customized ES-field type, defaults are derived from meta-info datatype
   * @param extractor optional custom data extraction from HDF5-entry
   * @param transformer optional post-processing transformation for extracted values
   */
  case class IndexEntry(
    metaName: String,
    fieldDataType: Option[(String) => BasicFieldDefinition] = None,
    extractor: Option[(CalculationH5) => Seq[Any]] = None,
    transformer: Option[(Seq[Any]) => Seq[Any]] = None
  )

  case class HierarchyIndexEntry(
    name: String,
    elements: Seq[IndexEntry],
    subNodes: Seq[HierarchyIndexEntry]
  )

  /**
   * Meta-data environment to use for Meta-Data lookups
   */
  val metaData: MetaInfoEnv = KnownMetaInfoEnvs.archive

  /**
   * Suffix for all fields not representing actual meta-data but their count-information to support
   * "all" quantifier queries
   */
  val countFieldsSuffix = "_count"

  val fields = Seq(
    //calculation identifiers (these are treated separately in the importer)
    IndexEntry("calculation_gid"),
    IndexEntry("archive_gid"),
    //calculation data
    IndexEntry("main_file_uri"),
    IndexEntry("calculation_uploader_name"),
    IndexEntry(
      "calculation_upload_date",
      fieldDataType = Some(x => dateField(x).format("date_optional_time")),
      transformer = Some(values => values.map { case x: Long => ISODateTimeFormat.dateTime().print(x) })
    ),
    IndexEntry("calculation_pid"),
    IndexEntry("program_name"),
    IndexEntry("stats_meta_present"),
    //system related data
    IndexEntry("atom_species"),
    IndexEntry("system_composition"),
    IndexEntry("system_reweighted_composition"),
    IndexEntry("system_type"),
    IndexEntry("crystal_system"),
    IndexEntry("space_group_number"),
    IndexEntry("springer_id"),
    IndexEntry("springer_classification"),
    IndexEntry("configuration_raw_gid"),
    //computational setup
    IndexEntry("electronic_structure_method"),
    IndexEntry("xc_functional_name"),
    IndexEntry("program_basis_set_type"),
    //data related to other projects ingested into Nomad(e.g. AFlowLib)
    IndexEntry("prototype_aflow_id"),
    IndexEntry("prototype_aflow_url"),
    //auxiliary data of the query
    IndexEntry("number_of_single_configurations", Some(intField), Some { calc =>
      Seq(calc.sectionTable(Seq("section_run", "section_system")).lengthL)
    })
  )

  def getHierarchy(implicit metaData: MetaInfoEnv): Seq[HierarchyIndexEntry] = {
    val paths = fields.map { x => metaNameToPathSegments(x.metaName) -> x }.toMap
    getSubHierarchy("", paths).subNodes
  }

  private def getSubHierarchy(name: String, input: Map[Seq[String], IndexEntry]): HierarchyIndexEntry = {
    val (sections, directChildren) = input.partition(x => x._1.length > 1)
    val subSections: Map[String, Map[Seq[String], IndexEntry]] = sections.groupBy(x => x._1.head)
    val subNodes = subSections.map(x => getSubHierarchy(x._1, x._2.map(y => y._1.tail -> y._2)))
    HierarchyIndexEntry(name, directChildren.values.toSeq, subNodes.toSeq)
  }

  def getESMapping(es: ESManager)(implicit metaData: MetaInfoEnv): MappingDefinition = {
    import eu.nomad_lab.elasticsearch.ConnectorElasticSearch.{ queryAuxiliaryDataName, countSection }
    val paths = fields.map { x => metaNameToPathSegments(x.metaName) -> x }.toMap
    val data = getSubESMapping("", paths).fields
    val counts = getSubESCountMapping(countSection, paths)
    val countRoot = objectField(queryAuxiliaryDataName).fields(counts)
    mapping(es.settings.typeNameData).fields(data :+ countRoot).dynamic(DynamicMapping.Strict)
  }

  /**
   * Generate the primary subtree of the ES-index which contains the unique values for each field.
   * @param metaName
   * @param input
   * @param metaData
   * @return
   */
  private def getSubESMapping(
    metaName: String,
    input: Map[Seq[String], IndexEntry]
  )(implicit metaData: MetaInfoEnv): FieldDefinition = {
    val (sections, directChildren) = input.partition(x => x._1.length > 1)
    val subSections: Map[String, Map[Seq[String], IndexEntry]] = sections.groupBy(x => x._1.head)
    val subNodes = subSections.map(x => getSubESMapping(x._1, x._2.map(y => y._1.tail -> y._2)))
    val directFields: Seq[FieldDefinition] = directChildren.map { x =>
      if (x._2.fieldDataType.nonEmpty) {
        x._2.fieldDataType.get(x._1.head)
      } else {
        metaData.metaInfoRecordForName(x._2.metaName).get.dtypeStr match {
          case Some("i") => longField(x._2.metaName)
          case Some("f") => doubleField(x._2.metaName)
          case Some("f32") => floatField(x._2.metaName)
          case Some("i32") => intField(x._2.metaName)
          case Some("f64") => doubleField(x._2.metaName)
          case Some("i64") => longField(x._2.metaName)
          case Some("b") => throw new UnsupportedOperationException("byte values not supported")
          case Some("B") => throw new UnsupportedOperationException("cannot index binary data fields")
          case Some("C") => keywordField(x._2.metaName)
          case Some("D") => throw new UnsupportedOperationException("indexing dicts is not supported")
          case Some("r") => throw new UnsupportedOperationException("indexing refs is not supported")
          case Some(y) => throw new UnsupportedOperationException(s"unknown data type '$y'")
          case None => throw new UnsupportedOperationException("no data type defined")
        }
      }
    }.toSeq
    objectField(metaName).fields(directFields ++ subNodes)
  }

  /**
   * Generate a sub-tree of the ES-index which only stores the unique values count for each field
   * in the main index.
   * @param metaName
   * @param input
   * @param metaData
   * @return
   */
  private def getSubESCountMapping(
    metaName: String,
    input: Map[Seq[String], IndexEntry]
  )(implicit metaData: MetaInfoEnv): FieldDefinition = {
    val (sections, directChildren) = input.partition(x => x._1.length > 1)
    val subSections: Map[String, Map[Seq[String], IndexEntry]] = sections.groupBy(x => x._1.head)
    val subNodes = subSections.map(x => getSubESCountMapping(x._1, x._2.map(y => y._1.tail -> y._2)))
    val countFields: Seq[FieldDefinition] = directChildren.map { x => intField(x._2.metaName) }.toSeq
    objectField(metaName).fields(countFields ++ subNodes)
  }
}
