/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.bulk.BulkResponse
import com.sksamuel.elastic4s.indexes.IndexDefinition
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.H5Lib
import eu.nomad_lab.archive.IndexManifest.HierarchyIndexEntry
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.h5._
import eu.nomad_lab.meta.MetaInfoEnv
import ucar.ma2._

import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }

object ElasticSearchData extends StrictLogging {

  def extractSearchParameters(hierarchy: HierarchyIndexEntry, calculation: CalculationH5)(implicit metaData: MetaInfoEnv): Map[String, Any] = {
    val values = hierarchy.elements.map { x =>
      val extracted = x.extractor match {
        case Some(f) => f(calculation)
        case None => extractUniqueValues(calculation, x.metaName, metaData)
      }
      x.transformer match {
        case Some(f) => x.metaName -> f(extracted)
        case None => x.metaName -> extracted
      }
    }.filter(_._2.nonEmpty).toMap
    val subNodes = hierarchy.subNodes.map { x =>
      x.name -> extractSearchParameters(x, calculation)
    }.filter(x => x._2.nonEmpty).toMap
    values ++ subNodes
  }

  def extractUniqueValues(calculation: CalculationH5, metaName: String,
    metaData: MetaInfoEnv): Seq[Any] = {
    val metaInfo = metaData.metaInfoRecordForName(metaName).get
    val dataType = metaInfo.dtypeStr
    if (metaInfo.shape.nonEmpty && metaInfo.shape.get.nonEmpty) {
      throw new UnsupportedOperationException("Nomad Query Language doesn't support " +
        s"operations on array elements. Array data from meta-data '$metaName' should be queried " +
        "directly on the HDF5 level.")
    }
    metaName match {
      case "calculation_gid" => Seq(calculation.calculationGid)
      case "archive_gid" => Seq(calculation.archive.archiveGid)
      case _ =>
        val path = metaData.pathViaSections(metaName).drop(1).split("/").toSeq
        val metaVTable = calculation.valueTable(path)
        val (_, metaDataSet) = metaVTable.openValueDataset(Seq(1), create = false)
        try {
          if (metaDataSet >= 0) {
            dataType match {
              case Some("i") | Some("i32") =>
                val array = new ArrayInt.D1(metaVTable.length.toInt)
                val rawData = metaVTable.valueGetLocalRange(metaDataSet, 0, metaVTable.length.toInt, value = Some(array))
                (for (i <- 0 until rawData.getSize.toInt) yield array.get(i)).distinct
              case Some("f") | Some("f32") =>
                val array = new ArrayFloat.D1(metaVTable.length.toInt)
                val rawData = metaVTable.valueGetLocalRange(metaDataSet, 0, metaVTable.length.toInt, value = Some(array))
                (for (i <- 0 until rawData.getSize.toInt) yield array.get(i)).distinct
              case Some("f64") =>
                val array = new ArrayDouble.D1(metaVTable.length.toInt)
                val rawData = metaVTable.valueGetLocalRange(metaDataSet, 0, metaVTable.length.toInt, value = Some(array))
                (for (i <- 0 until rawData.getSize.toInt) yield array.get(i)).distinct
              case Some("i64") =>
                val array = new ArrayLong.D1(metaVTable.length.toInt)
                val rawData = metaVTable.valueGetLocalRange(metaDataSet, 0, metaVTable.length.toInt, value = Some(array))
                (for (i <- 0 until rawData.getSize.toInt) yield array.get(i)).distinct
              case Some("b") => throw new UnsupportedOperationException("byte values not supported")
              case Some("B") => throw new UnsupportedOperationException("cannot index binary data")
              case Some("C") =>
                val array = new ArrayString.D1(metaVTable.length.toInt)
                val rawData = metaVTable.valueGetLocalRange(metaDataSet, 0, metaVTable.length.toInt, value = Some(array))
                (for (i <- 0 until rawData.getSize.toInt) yield array.get(i)).distinct
              case Some("D") => throw new UnsupportedOperationException("dicts are not supported")
              case Some("r") => throw new UnsupportedOperationException("references are not supported")
              case Some(x) => throw new UnsupportedOperationException(s"unknown data type '$x'")
              case None => throw new UnsupportedOperationException("no data type defined")
            }
          } else {
            Seq()
          }
        } finally {
          H5Lib.datasetClose(metaDataSet)
        }
    }
  }

  def generateCountMap(extracted: Map[String, Any]): Map[String, Any] = {
    extracted.map { entry =>
      entry._2 match {
        case subMap: Map[_, _] => entry._1 -> generateCountMap(subMap.asInstanceOf[Map[String, Any]])
        case value: Seq[_] => entry._1 -> value.length
        case _ => entry._1 -> 1
      }
    }
  }

  def loopOverArchive(es: ESManager, archive: ArchiveH5,
    indexName: String)(implicit metaInfo: MetaInfoEnv): Unit = {
    import eu.nomad_lab.elasticsearch.ConnectorElasticSearch.{ queryAuxiliaryDataName, countSection }
    val rootSections = IndexManifest.getHierarchy
    val maxPending = 100
    var indexTasks: List[IndexDefinition] = Nil
    var indexJob: Option[Future[BulkResponse]] = None
    var added = 0

    def indexBatch(
      batch: List[IndexDefinition],
      previousBatch: Option[Future[BulkResponse]]
    ): Option[Future[BulkResponse]] = {
      if (previousBatch.nonEmpty) {
        val response = Await.result(previousBatch.get, 100.seconds)
        if (response.hasFailures) {
          response.failures.foreach(x => logger.warn("    failed insertion: " + x))
        }
        added += response.successes.length
      }
      if (batch.nonEmpty) Some(es.client.execute(bulk(batch))) else None
    }

    archive.calculations().foreach { calculation =>
      val entries = rootSections.map { x =>
        x.name -> ElasticSearchData.extractSearchParameters(x, calculation)
      }.filter(x => x._2.nonEmpty).toMap
      val countInfo = Map(queryAuxiliaryDataName -> Map(countSection -> generateCountMap(entries)))
      val id = calculation.calculationGid
      indexTasks = indexInto(indexName, es.settings.typeNameData).fields(entries ++ countInfo).id(id) :: indexTasks
      if (indexTasks.size >= maxPending) {
        indexJob = indexBatch(indexTasks, indexJob)
        indexTasks = Nil
      }
    }
    indexJob = indexBatch(indexTasks, indexJob)
    indexJob = indexBatch(Nil, indexJob)
    logger.info(s"finished archive ${archive.archiveGid}: $added / ${archive.calculations().size}")
  }
}
