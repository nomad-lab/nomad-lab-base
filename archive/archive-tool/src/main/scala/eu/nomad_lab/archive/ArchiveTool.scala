/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive

import java.io.File
import java.time.ZonedDateTime

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }

import scala.util.control.NonFatal
import scala.util.matching.Regex

object ArchiveTool extends StrictLogging {

  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    if (list.isEmpty) {
      println(usage)
      return
    }
    val cmd = list.head
    list = list.tail
    cmd match {
      case "--help" | "-h" =>
        println(usage)
      case "completeIndex" => buildCompleteIndex(list)
      case "createIndex" => createEmptyIndex(list)
      case "indexArchives" => indexArchives(list)
      case "indexFolders" => indexFolders(list)
      case "aliasRemap" => aliasRemap(list)
      case command =>
        println(s"invalid command $command")
        println(usage)
    }
  }

  private val usage = """|Usage:
      |archiveTool [--help] <command>
      |
      |  Available commands:
      |
      |  completeIndex  create a new index and parse all available archives
      |  createIndex    create a new index but parse no archives
      |  indexArchives  parse a set of archives and add their data to an existing index
      |  indexFolders   parse a set of folders recursively and add their data to an existing index
      |  aliasRemap     remaps the index to the current one
      |
      |  Run archiveTool <command> [--help] to view information about the
      |  options for each command.""".stripMargin

  private val indexReData: Regex = """--indexName=([\w_-]+)""".r
  private val usage_complete = """archiveTool completeIndex
      |  [--noAliasRemap]
      |  [--indexName=<name of new index>]
      |
      |  Build a new index for the Nomad Archive and add the data from
      |  all available normalized archives. If no index name is given,
      |  it defaults to a name with the current date "archiveYYYY_MM_DD".
      |  Unless --noAliasRemap is specified, the alias specified in
      |  the config parameter nomad_lab.elastic.indexNameData
      |  will be remapped to the newly created index after completion.""".stripMargin
  private val usage_create = """archiveTool createIndex
      |  [--indexName=<name of new index>]
      |
      |  Build a new index for the Nomad Archive without adding any data
      |  to it. If no index name is given, it defaults to a name with
      |  the current date "archiveYYYY_MM_DD".""".stripMargin
  private val usage_index = """archiveTool indexArchives
      |  [--indexName=<name of new index>]
      |  [--archive-gids-file=<file with archive gids>]
      |  archiveGid1 [archiveGid2 [...]]
      |
      |  Parse the given normalized archives and add their data to an
      |  existing Nomad Archive index. If no index name is given,
      |  it defaults to a name with the current date "archiveYYYY_MM_DD".""".stripMargin
  private val usage_indexFolders = """archiveTool indexFolders
      |  [--indexName=<name of new index>]
      |  folder1 [folder2 [...]]
      |
      |  Parse the given folders recursively and add the data from archives in
      |  them to an existing Nomad Archive index. If no index name is given,
      |  it defaults to a name with the current date "archiveYYYY_MM_DD".""".stripMargin
  private val usage_aliasRemap = """archiveTool aliasRemap
      |  [--force]
      |  [--indexName=<name of new index>]
      |
      |  Remaps the alias specified in the config parameter
      |  nomad_lab.elastic.indexNameData to the given index
      |  (it defaults to a name with the current date "archiveYYYY_MM_DD).""".stripMargin

  def buildCompleteIndex(args: List[String]): Unit = {
    var list: List[String] = args
    var doAliasRemap = true
    val today = ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameData = "archive" + today
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_complete)
          return
        case "--noAliasRemap" => doAliasRemap = false
        case indexReData(name) => indexNameData = name
        case _ =>
          println(s"Error: unexpected argument $arg. $usage_complete")
          return
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.archive)
    } catch {
      case NonFatal(e) =>
        logger.error(s"Elastic search connection failed! " + e)
        return
    }
    try {
      createNewIndex(es, indexNameData)(IndexManifest.metaData)
      val archiveSet: ArchiveSetH5 = ArchiveSetH5.normalizedSet
      processFolder(es, archiveSet, archiveSet.basePath.toFile, indexNameData)(IndexManifest.metaData)
      if (doAliasRemap) remapAliases(es, indexNameData)
    } finally {
      es.stop()
    }
  }

  def createEmptyIndex(args: List[String]): Unit = {
    var list: List[String] = args
    val today = ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameData = "archive" + today
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_create)
          return
        case indexReData(name) => indexNameData = name
        case _ =>
          println(s"Error: unexpected argument $arg. $usage_create")
          return
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.archive)
    } catch {
      case NonFatal(e) =>
        logger.error(s"Elastic search connection failed! " + e)
        return
    }
    try {
      createNewIndex(es, indexNameData)(IndexManifest.metaData)
    } finally {
      es.stop()
    }
  }

  def indexArchives(args: List[String]): Unit = {
    var list: List[String] = args
    val today = ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameData = "archive" + today
    var archives = Seq[String]()
    val archiveGidsFileRe = "^--archive-gids-file=(.+)$".r
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_index)
          return
        case indexReData(name) => indexNameData = name
        case archiveGidsFileRe(file) =>
          val s = scala.io.Source.fromFile(file)
          archives ++= s.getLines()
        case _ => archives = arg :: list; list = List()
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.archive)
    } catch {
      case NonFatal(e) =>
        logger.error(s"Elastic search connection failed! " + e)
        return
    }
    try {
      val archiveSet: ArchiveSetH5 = ArchiveSetH5.normalizedSet
      processArchives(es, archiveSet, archives, indexNameData)(IndexManifest.metaData)
    } finally {
      es.stop()
    }
  }

  def indexFolders(args: List[String]): Unit = {
    var list: List[String] = args
    val today = ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameData = "archive" + today
    var folders = Seq[String]()
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_indexFolders)
          return
        case indexReData(name) => indexNameData = name
        case _ => folders = arg :: list; list = List()
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.archive)
    } catch {
      case NonFatal(e) =>
        logger.error(s"Elastic search connection failed! " + e)
        return
    }
    try {
      val archiveSet: ArchiveSetH5 = ArchiveSetH5.normalizedSet
      folders.foreach { x =>
        processFolder(es, archiveSet, new File(x), indexNameData)(IndexManifest.metaData)
      }
    } finally {
      es.stop()
    }
  }

  def aliasRemap(args: List[String]): Unit = {
    var list: List[String] = args
    var force: Boolean = false
    val today = ZonedDateTime.now(java.time.ZoneId.systemDefault()).toLocalDate
    var indexNameData = "archive" + today
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_aliasRemap)
          return
        case "--force" => force = true
        case indexReData(name) => indexNameData = name
        case _ =>
          println(s"Error: unexpected argument $arg. $usage_complete")
          return
      }
    }
    val es = try {
      ESManager(KnownMetaInfoEnvs.archive)
    } catch {
      case NonFatal(e) =>
        logger.error(s"Elastic search connection failed! " + e)
        return
    }
    try {
      remapAliases(es, indexNameData, force = force)
    } finally {
      es.stop()
    }
  }

  def createNewIndex(es: ESManager, indexName: String)(implicit metaInfo: MetaInfoEnv): Unit = {
    val mapping = IndexManifest.getESMapping(es)
    val creation = es.client.execute(createIndex(indexName).mappings(mapping))
    val response = creation.await
    if (!response.acknowledged) {
      throw new UnsupportedOperationException(s"Index creation failed: $response")
    }
    logger.info(s"created new Nomad archive index '$indexName'")
  }

  def processArchives(es: ESManager, archiveSet: ArchiveSetH5,
    archiveGids: Seq[String], indexName: String)(implicit metaInfo: MetaInfoEnv): Unit = {
    archiveGids.foreach { gid =>
      val archive = archiveSet.acquireArchive("R" + gid.drop(1))
      ElasticSearchData.loopOverArchive(es, archive, indexName)
    }
  }

  def processFolder(es: ESManager, archiveSet: ArchiveSetH5,
    location: File, indexName: String)(implicit metaInfo: MetaInfoEnv): Unit = {
    val (dirs, files) = location.listFiles().partition(_.isDirectory)
    val archiveNames = files.filter(_.getName.endsWith(".h5")).map(_.getName).toSeq
    val gids = archiveNames.map(x => x.substring(0, x.lastIndexOf(".")))
    processArchives(es, archiveSet, gids, indexName)
    dirs.foreach(processFolder(es, archiveSet, _, indexName))
  }

  def remapAliases(es: ESManager, indexNameData: String, force: Boolean = false): Unit = {
    val aliasResult = es.client.execute(catAliases()).await()
    val liveIndexData = ESManager.defaultSettings(KnownMetaInfoEnvs.archive).indexNameData
    logger.info(s"Data Alias to be used: $liveIndexData")
    val rawIndexNameData = aliasResult.find(x => x.alias == liveIndexData) match {
      case None => None
      case Some(x) => Some(x.index)
    }
    if (rawIndexNameData.nonEmpty) {
      es.client.execute(refreshIndex(indexNameData, rawIndexNameData.get)).await
      val oldCount = es.client.execute(catCount(rawIndexNameData.get)).await.count
      val newCount = es.client.execute(catCount(indexNameData)).await.count
      logger.info(s"Old Data index: ${rawIndexNameData.get} with $oldCount entries")
      logger.info(s"New Data index: $indexNameData with $newCount entries")
      if (oldCount > newCount && !force) {
        logger.error(s"New data index contains less entries than the current one!")
        return
      }
    }
    val aliasRemap = rawIndexNameData match {
      case None => Seq(addAlias(liveIndexData).on(indexNameData))
      case Some(x) => Seq(
        removeAlias(liveIndexData).on(x),
        addAlias(liveIndexData).on(indexNameData)
      )
    }
    val remapResponse = es.client.execute(aliases(aliasRemap)).await
    if (!remapResponse.success) {
      logger.error("Alias remapping failed!", remapResponse.toString)
    } else {
      logger.info(s"Data Alias remapped, alias $liveIndexData now points to index $indexNameData")
    }
  }
}
