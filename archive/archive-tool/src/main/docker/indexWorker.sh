#!/bin/bash

cmd="java -Djava.library.path=/lib -jar /app/archiveTool.jar"
baseName=${baseName:-toIndex-}
globPattern=${globPattern:-??}
if [ -n "$indexName" ] ; then
    indexFlags="--indexName=$indexName"
fi
if [ -e /config.sh ] ; then
   source /config.sh
fi
cmdArgs=${cmdArgs:-indexArchives $indexFlags --archive-gids-file=\${f}}

cd /work
for f in /work/$baseName$globPattern ; do
  (
  if [[ -e "$f" ]] ; then
      command=$(eval echo -e $cmd $cmdArgs)
      $command >& "out-$(basename $f).txt"
  fi
) &
done
wait
