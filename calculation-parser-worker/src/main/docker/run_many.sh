#!/bin/bash

SBT_OPTS="-Xmx1024M -Xss3M -XX:+UseConcMarkSweepGC"
CMD="java $SBT_OPTS -Dnomad_lab.configurationToUse=labdev \
-Dlabdev.nomad_lab.parser_worker_rabbitmq.numberOfWorkers=1 \
-Dlabdev.nomad_lab.parsing_stats.jdbcUrl=\"\" \
-Dlabdev.nomad_lab.baseTmpDir=/tmp/app \
-Dlabdev.nomad_lab.parser_worker_rabbitmq.uncompressRoot=/tmp/uncompress \
  -jar /app/nomadCalculationParserWorker-assembly-*.jar"

for i in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16; do
  $CMD &> out-${i}.txt &
done
wait
