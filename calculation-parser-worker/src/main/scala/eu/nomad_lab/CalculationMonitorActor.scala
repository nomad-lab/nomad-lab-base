/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import java.io.IOException
import java.util.Date

import akka.actor.{ Actor, ActorRef, Props }
import com.rabbitmq.client.{ AMQP, _ }
import com.thenewmotion.akka.rabbitmq.{ ChannelActor, ChannelMessage, ConnectionActor, CreateChannel }
import com.typesafe.config.{ Config, ConfigFactory }
import eu.{ nomad_lab => lab }
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, CalculationParserResult }
import eu.nomad_lab.parsing_queue.CalculationParser
import org.json4s.JsonAST.{ JString, JValue }
import spray.http._
import HttpMethods.GET
import MediaTypes.`text/html`
import MediaTypes.`application/json`
import com.typesafe.scalalogging.StrictLogging
import org.json4s._
import org.json4s.native.Serialization
import spray.can.Http
import eu.nomad_lab.parsing_stats.ParsingStatsDb
import scala.util.control.NonFatal

class CalculationMonitorActor extends Actor with StrictLogging {
  import CalculationMonitorActor._
  /**
   * The settings required to get the read, write queue, parsed and uncompressed Root
   */
  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val readQueue = config.getString("nomad_lab.parser_worker_rabbitmq.singleParserQueue")
    //    val writeQueue = config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedQueue")
    val readExchange = config.getString("nomad_lab.parser_worker_rabbitmq.singleParserExchange")
    val writeExchange = config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedExchange")
    val uncompressRoot = config.getString("nomad_lab.parser_worker_rabbitmq.uncompressRoot")
    val parsedJsonPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedJsonPath")
    val parsedCalculationH5Path = config.getString("nomad_lab.parser_worker_rabbitmq.parsedCalculationH5Path")
    val parsedArchiveH5Path = config.getString("nomad_lab.parser_worker_rabbitmq.parsedArchiveH5Path")
    val rabbitMQHost = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQHost")

    def toJValue: JValue = {
      import org.json4s.JsonDSL._
      (("rabbitMQHost" -> rabbitMQHost) ~
        ("readQueue" -> readQueue) ~
        ("readExchange" -> readExchange) ~
        ("writeToExchange" -> writeExchange) ~
        //        ("writeQueue" -> writeQueue) ~
        ("uncompressRoot" -> uncompressRoot) ~
        ("parsedJsonPath" -> parsedJsonPath))
    }
  }

  var repl = LocalEnv.defaultSettings.replacements
  val settings = new Settings(LocalEnv.defaultConfig)

  //RabbitMQ setup
  val factory = new ConnectionFactory()
  val rabbitMQHost = LocalEnv.makeReplacements(repl, settings.rabbitMQHost)
  factory.setHost(rabbitMQHost)
  logger.info(s"Trying to connect to RabbitMQHost at ${rabbitMQHost}")
  val connection = context.actorOf(ConnectionActor.props(factory), "rabbitmqConnection")
  logger.info(s"Conneted to RabbitMQHost at ${rabbitMQHost}")

  // requestInfo, internal stats
  var state = State.WaitingForMessage
  var acknowledgment: Option[(Boolean) => Unit] = None
  var currentRequest: Option[CalculationParserRequest] = None
  var lastRequestReceivedTime: Option[java.util.Date] = None
  var lastRequestFinishedTime: Option[java.util.Date] = None

  val statsDb: Option[ParsingStatsDb] = {
    try {
      val db = ParsingStatsDb()
      Some(db)
    } catch {
      case NonFatal(e) =>
        logger.warn("Failed to initialize parsing stats db", e)
        None
    }
  }

  val monitorActor = context
  val calculationParser = new CalculationParser(
    ucRoot = settings.uncompressRoot,
    backendGenerator = CalculationParser.jsonH5Generator, //DSB  single h5 gen
    parserCollection = parsers.AllParsers.defaultParserCollection,
    replacements = LocalEnv.defaultSettings.replacements /*,
    statsFinishedCallback = statsDb match {
      case None => None
      case Some(db) =>
        Some(db.updateParsingStats)
    }*/
  )

  def setupPublisher(channel: Channel, self: ActorRef): Unit = {
    channel.exchangeDeclare(settings.writeExchange, "fanout")
    //    channel.queueDeclare(settings.writeQueue, true, false, false, null)
    //    channel.queueBind(settings.writeQueue, settings.readExchange, "")
  }
  //connection ! CreateChannel(ChannelActor.props(setupPublisher), Some("publisher"))
  val publisher = monitorActor.actorSelection(connection.path + "/publisher")
  def setupSubscriber(channel: Channel, self: ActorRef): Unit = {
    channel.exchangeDeclare(settings.readExchange, "fanout")
    //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
    channel.queueDeclare(settings.readQueue, true, false, false, null)
    channel.queueBind(settings.readQueue, settings.readExchange, "")

    channel.basicQos(1, false) //Sets prefetch for each consumer. For channel based prefetch use  channel.basicQos(prefetchCount, global = true)
    val consumer: Consumer = new DefaultConsumer((channel)) {
      @throws(classOf[IOException])
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        val message: CalculationParserRequest = eu.nomad_lab.JsonSupport.readUtf8[CalculationParserRequest](body)
        def ack(success: Boolean): Unit = { channel.basicAck(envelope.getDeliveryTag(), !success) }
        println("received: " + message)
        monitorActor.self ! ParserRequest(message, ack)
      }
    }
    channel.basicConsume(settings.readQueue, false, consumer)
  }

  connection ! CreateChannel(ChannelActor.props(setupSubscriber), Some("subscriber"))

  /**
   * Command to be executed, in case an out of order request is received.
   * @param msg Message to be printed before dieing
   */
  def die(msg: String): Unit = {
    println(s"die $msg")

  }
  implicit val formats = JsonSupport.formats + new StateSerializer
  /**
   * Current Status of the calculation parser worker with the last request and last request time
   *
   */
  def statusJSON() = HttpResponse(
    entity = HttpEntity(
      `application/json`,
      Serialization.writePretty(Status(
        state,
        currentRequest,
        lastRequestReceivedTime,
        lastRequestFinishedTime
      ))
    )
  )

  /**
   * Current Status of the calculation parser worker with the last request and last request time
   *
   */
  def statusHTML() = HttpResponse(
    entity = HttpEntity(
      `text/html`,
      <html>
        <body>
          <h1>Calculation Parser Worker Status!</h1>
          <ul>
            <li>State: { state }</li>
            <li>Current Request: { currentRequest.getOrElse("No current request") }</li>
            <li>Request arrival time: { lastRequestReceivedTime.getOrElse("No current request") }</li>
            <li>Request finish time: { lastRequestFinishedTime.getOrElse("No current request") }</li>
          </ul>
        </body>
      </html>.toString()
    )
  )

  /**
   *  Handled for messages received by the calculation monitor
   */
  def receive = {
    // when a new connection comes in we register ourselves as the connection handler
    case _: Http.Connected => sender ! Http.Register(self)

    case HttpRequest(GET, Uri.Path("/status.json"), _, _, _) =>
      sender ! statusJSON

    case HttpRequest(GET, Uri.Path("/status.html"), _, _, _) =>
      sender ! statusHTML

    case HttpRequest(GET, Uri.Path("/ping"), _, _, _) =>
      sender ! HttpResponse(entity = "PONG!")

    case _: HttpRequest => sender ! HttpResponse(status = 404, entity = "Unknown resource!")

    case ParserRequest(req, ack) =>
      state match {
        case State.WaitingForMessage =>
          state = State.MessageReceived
          currentRequest = Some(req)
          acknowledgment = Some(ack)
          lastRequestReceivedTime = Some(new java.util.Date)
          //          context.system.scheduler.scheduleOnce(1000.milliseconds, context.self, TaskFinished("Done Processing"))
          def parse(): Unit = {
            val resultMessage = calculationParser.handleParseRequest(req)
            monitorActor.self ! TaskFinished(resultMessage)
          }
          val workerActor = context.actorOf(Props[ParserActor], "Parser-Worker-Actor")
          workerActor ! ParserActor.ParseMessage(parse)
          workerActor ! "done"
        case State.MessageReceived =>
          die("Message received before acknowledgement")
      }
    case TaskFinished(res) =>
      state match {
        case State.MessageReceived =>
          acknowledgment match {
            case Some(f) =>
              state = State.WaitingForMessage
              lastRequestFinishedTime = Some(new java.util.Date)
              //Initialize Next exchange with the incoming message
              def publish(channel: Channel): Unit = {
                val msgBytes = JsonSupport.writeUtf8(res)
                try {
                  channel.basicPublish(settings.writeExchange, "", null, msgBytes)
                } catch {
                  case e: Exception =>
                    logger.error(s"Error while publishing to the exchange. Trying again in sometime. Message: ${e.getMessage}")
                    try {
                      Thread.sleep(1000)
                      channel.basicPublish(settings.writeExchange, "", null, msgBytes)
                    } catch {
                      case e: Exception =>
                        logger.error(s"Retry failed. Exiting now. Message: ${e.getMessage}")
                        System.exit(1)
                    }
                }
              }
              statsDb match {
                case Some(db) => db.setParsingResult(res)
                case None => ()
              }
              //publisher ! ChannelMessage(publish, dropIfNoChannel = false)

              f(true) //Send back acknowledgement for the last received message
              logger.info(s"parsed ${lab.JsonSupport.writePrettyStr(res)}")
            case None => die("missing acknowledgment")
          }
        case State.WaitingForMessage =>
          die("no task when received TaskFinished")
      }
    case TaskStatus =>
      sender() ! (state, currentRequest, lastRequestReceivedTime)
    //    case x      => print( s"Unknown Request: $x. Current Subscriber State is $state")
  }

  def fromBytes(x: Array[Byte]) = new String(x, "UTF-8")
  def toBytes(x: Long) = x.toString.getBytes("UTF-8")
}

object CalculationMonitorActor {
  object State extends Enumeration {
    val WaitingForMessage, MessageReceived = Value
  }

  class InvalidStateException(
    msg: String, what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * Json serialization and deserialization support for CalculationMonitorActor State
   */
  class StateSerializer extends CustomSerializer[State.Value](format => (
    {
      case JString(str) =>
        State.withName(str)
      case v =>
        throw new InvalidStateException(s"invalid State ${JsonUtils.normalizedStr(v)}")
    },
    {
      case x: TreeType.Value => JString(x.toString)
    }
  ))

  sealed trait TaskManagerMessage
  case class ParserRequest(val message: CalculationParserRequest, val ack: (Boolean) => Unit) extends TaskManagerMessage
  object TaskStatus extends TaskManagerMessage
  case class TaskFinished(result: CalculationParserResult) extends TaskManagerMessage
  object StopPolling extends TaskManagerMessage
  case class Status(
    state: State.Value,
    request: Option[CalculationParserRequest],
    lastRequestReceivedTime: Option[java.util.Date],
    lastRequestFinishedTime: Option[java.util.Date]
  )
}
