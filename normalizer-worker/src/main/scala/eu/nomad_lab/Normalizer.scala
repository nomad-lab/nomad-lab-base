/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab
import org.apache.flink.api.scala._
//import org.apache.flink.streaming.api.scala._
//import org.apache.flink.streaming.connectors.rabbitmq.RMQSource
//import org.apache.flink.streaming.util.serialization.SimpleStringSchema
import java.nio.file.Paths
import org.apache.flink.api.java.utils.ParameterTool
import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.h5.CalculationH5
import eu.nomad_lab.h5.SectionTableH5
import eu.nomad_lab.h5.SectionH5
import eu.nomad_lab.h5.ValueH5
import eu.nomad_lab.ref._
import scala.collection.mutable
import scala.collection.SortedMap
import org.{ json4s => jn }

object FlinkNormalizer {

  def main(args: Array[String]) {

    val params: ParameterTool = ParameterTool.fromArgs(args)
    val baseDir = if (params.has("normalizedArchivePath"))
      params.get("normalizedArchivePath")
    else
      "/normalized/productionH5"

    // set up the execution environment
    val env: ExecutionEnvironment = if (params.has("remoteHost"))
      ExecutionEnvironment.getExecutionEnvironment
    else {
      val port = if (params.has("remotePort"))
        params.get("remotePort").toInt
      else
        6123
      val jars = if (params.has("remoteJars"))
        params.get("remoteJars")
      else
        "/nomad/nomadlab/flink/jars/query.jar"
      ExecutionEnvironment.createRemoteEnvironment(
        params.get("remoteHost"), port, jars
      )
    }

    // val env = StreamExecutionEnvironment.getExecutionEnvironment
    val index = env.readTextFile(Paths.get(baseDir + "/index.txt").toString(), "UTF-8")
    index.partitionByHash().flatMap {
      gid: String =>
        val arch = ArchiveSetH5.normalizedSet.acquireArchive(gid)
        val res = for (calc <- arch.calculations) yield {
          calc.toRef
        }
        ArchiveSetH5.parsedSet.giveBackArchive(arch)
        res
    }.partitionByHash("calculationGid").flatMap {
      calcRef: CalculationRef =>
        if (Set('1', '2', '3') contains (calcRef.archiveGid(1))) {
          val calc = ArchiveSetH5.normalizedSet.acquireCalculationFromRef(calcRef)
          val sysSections = calc.sectionTable(Seq("section_run", "section_single_configuration_calculation"))
          val res = for (sect <- sysSections) yield {
            sect.toRef
          }
          ArchiveSetH5.normalizedSet.giveBackCalculation(calc)
          res
        } else {
          Seq()
        }
    }.map {
      sectionRef: RowRef =>
        val singleConfSection = ArchiveSetH5.normalizedSet.acquireSectionFromRef(sectionRef)
        val calc: CalculationH5 = singleConfSection.table.calculation
        val methodTable: SectionTableH5 = calc.sectionTable(Seq("section_run", "section_method"))
        val method: Option[SectionH5] = singleConfSection.maybeValue("single_configuration_to_calculation_method_ref") map {
          mGIndex: ValueH5 => methodTable(mGIndex.longValue)
        }
        val electronic_structure_method: String = method match {
          case Some(m) =>
            m.maybeValue("electronic_structure_method") match {
              case Some(v) => v.jValue match {
                case jn.JString(v) => v
                case _ => "undefined"
              }
              case None => "none"
            }
          case None => "undefined"
        }
        val xc_functional = method match {
          case Some(m) =>
            m.maybeValue("xc_functional") match {
              case Some(v) => v.jValue match {
                case jn.JString(v) => v
                case _ => "undefined"
              }
              case None => "none"
            }
          case None => "undefined"
        }
        val sysTable = calc.sectionTable(Seq("section_run", "section_system"))
        val sys = singleConfSection.maybeValue("single_configuration_to_system_ref") map { mGIndex: ValueH5 => sysTable(mGIndex.longValue)
        }
        val formulaDict: SortedMap[Int, Int] = sys match {
          case Some(s) => s.maybeValue("atom_labels") match {
            case Some(v) =>
              val arr = v.arrayValue()
              val labels = for (i <- 0.until(arr.getSize().toInt)) yield {
                arr.getObject(i).toString
              }
              val atomNrs = AtomData.atomNrsFromLabels(labels)
              var fDict = SortedMap[Int, Int]()
              for (atNr <- atomNrs)
                fDict += (atNr -> (fDict.getOrElse(atNr, 0) + 1))
              fDict
            case None => SortedMap()
          }
          case None => SortedMap()
        }
        val formulaStr: String = formulaDict.map {
          case (atNr, n) =>
            AtomData.labelForAtomNr(atNr) + (if (n != 1)
              n.toString
            else
              "")
        }.mkString("")
        ArchiveSetH5.normalizedSet.giveBackSection(singleConfSection)
        ((sectionRef.archiveGid, electronic_structure_method, xc_functional, formulaStr), 1)
    } /*.map{ (x: ((String, String, String, String), Int)) =>
      (x._1._1, x._1._2, x._1._3, x._1._4, x._2)
        }*/ .writeAsCsv("file:///nomad/nomadlab/flink/users-transfer/tstOut.csv")
    /*val formulaDict = mutable.Order
    for (i <- 0
    //    val streamForNomalizer = env.addSource(new RMQSource[String]("localhost", "hello", new SimpleStringSchema))
    //      .print

      query(methodFilter, systemFilter, singleCalcFilter)
      query(requiredMeta, requiredAtoms, excludeOthers, normalizedFormula, electronicStructureMethod, xcFunctional)
    }*/

    // execute program
    //env.execute("Test collect")
  }
}
