/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

/*import org.apache.flink.annotation.PublicEvolving
import org.apache.flink.api.common.typeinfo.{ BasicTypeInfo, TypeInformation }
import org.apache.flink.streaming.api.datastream.DataStreamSource
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.rabbitmq.RMQSource
import org.apache.flink.streaming.util.serialization.{ DeserializationSchema, SerializationSchema, SimpleStringSchema }
import org.apache.flink.streaming.api.scala._
//import org.apache.flink.util.Collector

object RabbitMqConnector {
  def main(args: Array[String]) {
    val rabbitSource = new RMQSource[String]("127.0.0.1", 5672, "hello", true, new SimpleStringSchema)
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val streamWithoutCorrelationIds = env.addSource(rabbitSource)
    streamWithoutCorrelationIds.print()
    val counts = streamWithoutCorrelationIds.flatMap { _.toLowerCase.split("\\W+") filter { _.nonEmpty } }
      .map { (_, 1) }
      .keyBy(0)
      .sum(1)

    print("Connection works!")
    env.execute("Scala SocketTextStreamWordCount Example")
  }
}
 */
