/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.webservice_base

import akka.event.{ Logging, LoggingAdapter }
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import eu.nomad_lab.elasticsearch.ApiCallException
import eu.nomad_lab.jsonapi
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.query.InvalidTerm
import org.{ json4s => jn }

import scala.collection.immutable
import scala.util.{ Failure, Success, Try }
import scala.util.control.NonFatal

object ResponseFormatter {

  val jsonApi = MediaType.applicationWithFixedCharset("vnd.api+json", HttpCharsets.`UTF-8`)

  def resolveQueryResult[T <: BaseValue](response: Try[ResponseData[T]], loneValue: Boolean = false): Route = {
    response match {
      case Success(x) =>
        logRequest("served", Logging.InfoLevel) {
          val body = responseToJsonApi(x.data, Seq(), loneValue, meta0 = x.meta)
          val entity = HttpEntity(ResponseFormatter.jsonApi, body)
          complete(HttpResponse(status = StatusCodes.OK, entity = entity))
        }
      case Failure(ex: ApiCallException) => reject(InvalidRequestRejection(ex.getMessage))
      case Failure(ex: InvalidTerm) => reject(InvalidRequestRejection(ex.getMessage))
      case Failure(ex) => throw ex
    }
  }

  def resolveQueryResult(httpResponse: Try[HttpResponse]): Route = {
    httpResponse match {
      case Success(response) => logRequest("served", Logging.InfoLevel) { complete(response) }
      case Failure(ex: ApiCallException) => reject(InvalidRequestRejection(ex.getMessage))
      case Failure(ex: InvalidTerm) => reject(InvalidRequestRejection(ex.getMessage))
      case Failure(ex) => throw ex
    }
  }

  /**
   * converts a search response to the jsonapi format
   */
  def responseToJsonApi(entries: Seq[BaseValue], errors: Seq[jsonapi.Error],
    loneValue: Boolean,
    meta: Map[String, jn.JValue] = Map(),
    meta0: Map[String, jn.JValue] = Map(),
    links: Map[String, jsonapi.Link] = Map()): String = {
    val w = new java.io.StringWriter
    val rw = new jsonapi.ResultWriter(w, meta = meta, links = links, loneValue = loneValue)
    rw.start()
    var myErrors = errors
    if (entries.size > 1 && loneValue) {
      myErrors = myErrors :+ jsonapi.Error(
        title = "too many elements in response",
        detail = s"Response was supposed to contain a single element but contained ${entries.size}"
      )
    } else {
      try {
        rw.addValues(entries.iterator)
      } catch {
        case NonFatal(e) =>
          myErrors = myErrors :+ jsonapi.Error(title = e.getClass.getName, detail = e.getMessage)
      }
    }
    rw.finish(errors0 = myErrors.toIterator, meta0 = meta0.toIterator)
    w.flush()
    w.close()
    w.toString
  }

  def flattenMapKeys(prefix: String, subData: Map[_, _]): Map[String, Any] = {
    subData.flatMap { x =>
      val subPrefix = if (prefix.isEmpty) x._1.toString else prefix + "." + x._1.toString
      x._2 match {
        case y: Map[_, _] => flattenMapKeys(subPrefix, y)
        case y => Map(subPrefix -> y)
      }
    }
  }

  case class ResponseData[T <: BaseValue](data: Seq[T], meta: Map[String, jn.JValue])

  case class InvalidRequestRejection(reason: String) extends Rejection

  object RejectionFormatter extends RejectionHandler {
    override def apply(v1: immutable.Seq[Rejection]): Option[Route] = {
      import StatusCodes._

      import scala.language.implicitConversions

      implicit def codeToString(code: StatusCode): String = code.intValue.toString

      val errors = v1.map {
        case x: MissingQueryParamRejection =>
          jsonapi.Error("mandatory parameter missing", x.parameterName, status = BadRequest)
        case x: MalformedQueryParamRejection =>
          jsonapi.Error(s"invalid argument for parameter ${x.parameterName}", x.errorMsg,
            status = BadRequest)
        case x: InvalidRequestRejection =>
          jsonapi.Error(s"invalid request parameters", x.reason, status = BadRequest)
        case x: MethodRejection =>
          jsonapi.Error(s"invalid request type", s"supported type: ${x.supported.value}",
            status = MethodNotAllowed)
        case x =>
          jsonapi.Error(s"invalid request", x.toString, status = BadRequest)
      }
      if (errors.nonEmpty) {
        val body = ResponseFormatter.responseToJsonApi(Seq(), errors, loneValue = false)
        val statusSet = errors.map(_.status).distinct
        val status = if (statusSet.size == 1) StatusCode.int2StatusCode(statusSet.head.toInt) else BadRequest
        val response = HttpEntity(ResponseFormatter.jsonApi, body)
        Some(logRequestResult("rejected", Logging.WarningLevel) {
          complete(HttpResponse(status = status, entity = response))
        })
      } else {
        Some(extractUri { path =>
          val notFound = jsonapi.Error(s"path not found", path.toString(), status = NotFound)
          val body = ResponseFormatter.responseToJsonApi(Seq(), Seq(notFound), loneValue = false)
          val response = HttpEntity(ResponseFormatter.jsonApi, body)
          logRequestResult("rejected", Logging.WarningLevel) {
            complete(HttpResponse(status = NotFound, entity = response))
          }
        })
      }
    }
  }

  class ErrorFormatter(log: LoggingAdapter) extends PartialFunction[Throwable, Route] {
    override def isDefinedAt(x: Throwable): Boolean = true

    //TODO: add more fine-grained error analysis, e.g. for database down?
    override def apply(ex: Throwable): Route = {
      import StatusCodes._

      import scala.language.implicitConversions

      implicit def codeToString(code: StatusCode): String = code.intValue.toString

      val id = Integer.toHexString(ex.hashCode())
      val error = jsonapi.Error(
        s"An internal server error occured",
        "If this problem persists, please report it together with the given ID.",
        status = InternalServerError, idStr = id
      )
      log.error(ex, s"error checksum: $id")
      val body = ResponseFormatter.responseToJsonApi(Seq(), Seq(error), loneValue = false)
      val response = HttpEntity(ResponseFormatter.jsonApi, body)
      logRequestResult("failed", Logging.ErrorLevel) {
        complete(HttpResponse(status = InternalServerError, entity = response))
      }
    }
  }
}
