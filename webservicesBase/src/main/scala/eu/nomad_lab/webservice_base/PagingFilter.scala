package eu.nomad_lab.webservice_base

import com.sksamuel.elastic4s.searches.sort.FieldSortDefinition
import eu.nomad_lab.elasticsearch.{ ApiCallException, ConnectorElasticSearch }
import org.elasticsearch.search.sort.{ SortMode, SortOrder }

case class PagingFilter(fieldName: String, after: Option[Any], before: Option[Any],
    arraySorting: SortMode = SortMode.MIN) {

  def getPagingFilter: Option[String] = {
    (before, after) match {
      case (Some(_), Some(_)) => throw ApiCallException("only 'before' or 'after' allowed")
      case (Some(before), None) => Some(s"""any $fieldName < "$before"""")
      case (None, Some(after)) => Some(s"""any $fieldName > "$after"""")
      case (None, None) => None
    }
  }

  def getSortDefinition(connector: ConnectorElasticSearch): FieldSortDefinition = {
    FieldSortDefinition(
      connector.metaNameToField(fieldName),
      order = if (before.isDefined) SortOrder.DESC else SortOrder.ASC,
      sortMode = Some(arraySorting)
    )
  }
}