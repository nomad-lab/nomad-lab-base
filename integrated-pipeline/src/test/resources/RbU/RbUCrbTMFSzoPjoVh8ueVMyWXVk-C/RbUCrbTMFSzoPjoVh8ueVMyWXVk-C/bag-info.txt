Source-Organization: NOMAD Repository, http://nomad-repository.eu/
Contact-Email: webmaster@nomad-repository.eu
External-Description: Part of the data uploaded by Marc Jaeger of Aalto
   University (M.Jaeger__W) on 2018-01-15 14:23:23.704743 and identified with
   the upload name umWYL0EIZqL4GH1UvJayGKHIhx4C4zvSMIA0M9wFI
Bag-Group-Identifier: nmd-arc://umWYL0EIZqL4GH1UvJayGKHIhx4C4zvSMIA0M9wFI
External-Identifier: nmd://RbUCrbTMFSzoPjoVh8ueVMyWXVk-C
Internal-Sender-Identifier: nmd-arc://umWYL0EIZqL4GH1UvJayGKHIhx4C4zvSMIA0M9wFI
Bag-Count: 1 of 1
Payload-Oxum: 2251579.13
Bagging-Date: 2018-02-20
Bag-Size: 2.2 MB
