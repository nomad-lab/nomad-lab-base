#$ -S /bin/bash
#$ -j n
#$ -cwd
#$ -m n
#$ -N BaSiO3(2)
#$ -pe impi_hydra 96
#$ -l h_rt=86400
###$ -l h_vmem=22G

module purge
module load intel/16.0 mkl/11.3 impi/5.1.3
#module load intel/15.0 impi/5.1.1 mkl/11.2
module list

export TMPDIR=/tmp
aims_x=/u/xylin/fhi-aims.160328_2/bin/aims.160328_1.scalapack.mpi.x

mpiexec -ppn 32  $aims_x &> outfile
