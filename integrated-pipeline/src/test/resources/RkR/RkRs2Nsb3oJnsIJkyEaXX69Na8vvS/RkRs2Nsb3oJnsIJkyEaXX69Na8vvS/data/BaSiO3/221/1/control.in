xc      pbe
RI_method LVL_fast
#many_body_dispersion
#mbd_eigensolver lapack
spin none #collinear
#default_initial_moment hund

charge_mix_param 0.08
mixer pulay
n_max_pulay 6
relativistic atomic_zora scalar

  KS_method          scalapack
  basis_threshold    1.e-5
  empty_states 8
  occupation_type gaussian 0.01

#partition_type rho_r2
relax_geometry bfgs_textbook 1d-2
#relax_unit_cell full
  sc_accuracy_forces 1E-4
  sc_iter_limit      400
  sc_accuracy_rho    1E-6
  sc_accuracy_eev    1E-4
  sc_accuracy_etot   1E-6

  k_grid 3 3 4
################################################################################
#
#  FHI-aims code project
# Volker Blum, Fritz Haber Institute Berlin, 2009
#
#  Suggested "tight" defaults for O atom (to be pasted into control.in file)
#
################################################################################
  species        O
#     global species definitions
    nucleus             8
    mass                15.9994
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         36 7.0
    radial_multiplier   2
     angular_grids specified
      division   0.1817   50
      division   0.3417  110
      division   0.4949  194
      division   0.6251  302
      division   0.8014  434
#      division   0.8507  590
#      division   0.8762  770
#      division   0.9023  974
#      division   1.2339 1202
#      outer_grid 974
      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   4.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   3.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.0 A, 1.208 A, 1.5 A, 2.0 A, 3.0 A
#
################################################################################
#  "First tier" - improvements: -699.05 meV to -159.38 meV
     hydro 2 p 1.8
     hydro 3 d 7.6
     hydro 3 s 6.4
#  "Second tier" - improvements: -49.91 meV to -5.39 meV
     hydro 4 f 11.6
     hydro 3 p 6.2
     hydro 3 d 5.6
     hydro 5 g 17.6
     hydro 1 s 0.75
#  "Third tier" - improvements: -2.83 meV to -0.50 meV
#     ionic 2 p auto
#     hydro 4 f 10.8
#     hydro 4 d 4.7
#     hydro 2 s 6.8
#  "Fourth tier" - improvements: -0.40 meV to -0.12 meV
#     hydro 3 p 5
#     hydro 3 s 3.3
#     hydro 5 g 15.6
#     hydro 4 f 17.6
#     hydro 4 d 14
# Further basis functions - -0.08 meV and below
#     hydro 3 s 2.1
#     hydro 4 d 11.6
#     hydro 3 p 16
#     hydro 2 s 17.2
################################################################################
#
# For methods that use the localized form of the "resolution of identity" for
# the two-electron Coulomb operator (RI_method LVL), particularly Hartree-Fock and
# hybrid density functional calculations, the highest accuracy can be obtained by
# uncommenting the line beginning with "for_aux"  below, thus adding an extra g radial
# function to the construction of the product basis set for the expansion.
# See Ref. New J. Phys. 17, 093020 (2015) for more information, particularly Figs. 1 and 6.
#
################################################################################
#
# for_aux hydro 5 g 6.0
################################################################################
#
#  FHI-aims code project
#  Volker Blum, Fritz Haber Institute Berlin, 2009
#
#  Suggested "tight" defaults for Si atom (to be pasted into control.in file)
#
#  Revised Jan 04, 2011, following tests (SiC) done by Lydia Nemec: 
#     d and g functions of tier 2 now enabled by default.
#
################################################################################
  species        Si
#     global species definitions
    nucleus             14
    mass                28.0855
#
    l_hartree           6
#
    cut_pot             4.0          2.0  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         42 7.0
    radial_multiplier   2
    angular_grids       specified
      division   0.4121   50
      division   0.7665  110
      division   1.0603  194
      division   1.2846  302
      division   1.4125  434
#      division   1.4810  590
#      division   1.5529  770
#      division   1.6284  974
#      division   2.6016 1202
#      outer_grid   974
      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      3  s   2.
    valence      3  p   2.
#     ion occupancy
    ion_occ      3  s   1.
    ion_occ      3  p   1.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.75 A, 2.0 A, 2.25 A, 2.75 A, 3.75 A
#
################################################################################
#  "First tier" - improvements: -571.96 meV to -37.03 meV
     hydro 3 d 4.2
     hydro 2 p 1.4
     hydro 4 f 6.2
     ionic 3 s auto
#  "Second tier" - improvements: -16.76 meV to -3.03 meV
     hydro 3 d 9
     hydro 5 g 9.4
#     hydro 4 p 4
#     hydro 1 s 0.65
#  "Third tier" - improvements: -3.89 meV to -0.60 meV
#     ionic 3 d auto
#     hydro 3 s 2.6
#     hydro 4 f 8.4
#     hydro 3 d 3.4
#     hydro 3 p 7.8
#  "Fourth tier" - improvements: -0.33 meV to -0.11 meV
#     hydro 2 p 1.6
#     hydro 5 g 10.8
#     hydro 5 f 11.2
#     hydro 3 d 1
#     hydro 4 s 4.5
#  Further basis functions that fell out of the optimization - noise
#  level... < -0.08 meV
#     hydro 4 d 6.6
#     hydro 5 g 16.4
#     hydro 4 d 9
################################################################################
#
# For methods that use the localized form of the "resolution of identity" for
# the two-electron Coulomb operator (RI_method LVL), particularly Hartree-Fock and
# hybrid density functional calculations, the highest accuracy can be obtained by
# uncommenting the line beginning with "for_aux"  below, thus adding an extra g radial
# function to the construction of the product basis set for the expansion.
# See Ref. New J. Phys. 17, 093020 (2015) for more information, particularly Figs. 1 and 6.
#
################################################################################
#
# for_aux hydro 5 g 6.0
################################################################################
#
#  FHI-aims code project
# Volker Blum, Fritz Haber Institute Berlin, 2009
#
#  Suggested "tight" defaults for Ba atom (to be pasted into control.in file)
#
#  The onset of the cutoff pot'l WAS set to 8 A by default, because the neutral
#  Ba atom is a large atom. However, this is very expensive. The radius should be
#  much smaller in real-world situations, where Ba will be ionic. Please check 
#  and reduce the cutoff radius explicitly.
#
#  2015/11/12 : f and g functions from tier2 added to default basis set choice.
#               These functions make a difference in the "delta test". 
#               Reduced the default cutoff radius to 6AA. The free-atom 6s function
#               requires more. However, 6AA is already VERY expensive for
#               any production calculations. In fact, revisit this choice for
#               ionic systems to see if a smaller cutoff radius will do.
#
################################################################################
  species          Ba
#     global species definitions
    nucleus        56
    mass           137.327
#
    l_hartree      6
#
    cut_pot        6.0  2.0  1.0
    basis_dep_cutoff    1e-4
#
    radial_base    65  7.0
    radial_multiplier  2
    angular_grids specified
      division   0.6752  110
      division   0.9746  194
      division   1.2241  302
      division   1.3850  434
#      division   1.4734  590
#      division   1.6010  770
#      division   4.8366  974
#      outer_grid  974
      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      6  s   2.
    valence      5  p   6.
    valence      4  d  10.
#     ion occupancy
    ion_occ      6  s   1.
    ion_occ      5  p   6.
    ion_occ      4  d  10.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 2.65, 3.00, 3.50, 4.40, 5.50 Ang
#
################################################################################
#  "First tier" - improvements: -1277.43 meV to -9.16 meV
     ionic 5 d auto
     ionic 4 f auto
     hydro 3 p 2.7
     hydro 4 s 3.3
#  "Second tier" - improvements: -64.04 (!) meV to -0.25 meV
     hydro 4 f 5.8  
     hydro 5 g 7.4  
#     hydro 4 d 4.5  
#     hydro 6 h 11.2 
#     hydro 5 p 6.6  
#     hydro 2 s 3.2  
#  "Third tier" - max. impr. -1.16 meV, min. impr. -0.08 meV
#     hydro 5 f 7.4
#     hydro 5 g 10.8
#     hydro 4 d 2.3
#     hydro 4 p 3.7  
#     hydro 5 s 4.0  
#  Further functions - impr. -0.35 meV and below
#     hydro 5 d 3.5  
#     hydro 6 d 0.4  
#     hydro 2 p 2.5  
#     hydro 5 f 12   
#     hydro 6 d 8.8  
