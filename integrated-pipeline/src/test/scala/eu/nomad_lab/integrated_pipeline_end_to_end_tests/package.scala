package eu.nomad_lab

import java.nio.file.Path

import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.h5.CalculationH5
import eu.nomad_lab.integrated_pipeline.{ FileTree, OutputType }
import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline_tests._
import eu.nomad_lab.meta.MetaInfoEnv
import org.scalatest.Assertions.succeed
import org.scalatest.{ Assertion, Informer }

import scala.collection.mutable
import scala.io.Source

package object integrated_pipeline_end_to_end_tests extends TestDataBuilders {

  trait Fixture {
    val numWorkers: Int
    val outputType: OutputType
    val treeType: TreeType
    val sample: TestTreeData
    lazy val treeTask: FileTree = createFileTreeScanRequest(sample, treeType)
    lazy val dataRoot: String = treeTask.treeType match {
      case TreeType.Directory => s"src/test/resources/${sample.baseName.substring(0, 3)}/${sample.baseName}"
      case TreeType.Zip => s"src/test/resources/${sample.baseName.substring(0, 3)}/${sample.baseName}.zip"
    }
    lazy val tmpResultsFolder: Path = {
      val prefix = treeTask.treeType match {
        case TreeType.Directory => "directory"
        case TreeType.Zip => "zip-archive"
      }
      val tempDir = generateTempTestDirectory(s"$prefix-to-${outputType.toString.toLowerCase}")
      tempDir.toFile.deleteOnExit()
      tempDir
    }

    def generateConsoleArgumentList(info: Option[Informer]): Array[String] = {
      val command = treeTask.treeType match {
        case TreeType.Directory => "parseDirectory"
        case TreeType.Zip => "parseRawDataArchive"
      }
      val output = "--output=" + (outputType match {
        case OutputType.Json => "Json"
        case OutputType.HDF5 => "HDF5"
        case OutputType.HDF5merged => "HDF5merged"
      })
      val directory = s"""--outputdir="$tmpResultsFolder""""
      val workers = s"--numWorkers=$numWorkers"
      val suppressConfigDump = "--noConfigDump"
      val params = Array(command, output, workers, directory, suppressConfigDump, dataRoot)
      info.foreach(x => x(s"command line arguments: ${params.mkString("'", " ", "'")}"))
      params
    }
  }

  class SingleWorkerFixture(val treeType: TreeType, val outputType: OutputType) extends Fixture {
    override val numWorkers: Int = 1
    override val sample: TestTreeData = testData4
  }

  class MultiWorkerFixture(val treeType: TreeType, val outputType: OutputType,
      val numWorkers: Int) extends Fixture {
    require(numWorkers > 1, "need multiple workers")
    override val sample: TestTreeData = testData3
  }

  class NonExistingTreeFixture(val treeType: TreeType, val outputType: OutputType) extends Fixture {
    override val numWorkers: Int = 1
    override val sample: TestTreeData = TestTreeData("purely-imaginary", Map())
  }

  def assertValidityOfGeneratedJsonFiles(sample: TestTreeData, treeTask: FileTree,
    tmpResultsFolder: Path, metaInfo: MetaInfoEnv): Assertion = {
    sample.candidateCalculationsWithParsers.foreach { entry =>
      val task = aCandidateFound().withFileTree(treeTask).withRelativePath(entry._1).build()
      val fileName = s"${task.calculationGid}.json"
      val targetFolder = tmpResultsFolder.resolve(treeTask.prefixFolder).resolve(treeTask.archiveId)
      val location = targetFolder.resolve(fileName)
      assert(
        location.toFile.exists(),
        s"json file '$location' with parsing results does not exist"
      )
      val jsonData = JsonUtils.parseReader(Source.fromFile(location.toFile).bufferedReader())
      validateJsonMetaData(jsonData \ "sections", metaInfo)
      assert(
        (jsonData \ "parserInfo" \ "name").extract[String] == entry._2,
        "unexpected parser name in output file"
      )
    }
    succeed
  }

  def assertValidityOfGeneratedHDF5Files(sample: TestTreeData, treeTask: FileTree,
    tmpResultsFolder: Path, metaInfo: MetaInfoEnv): Assertion = {
    sample.candidateCalculationsWithParsers.foreach { entry =>
      val task = aCandidateFound().withFileTree(treeTask).withRelativePath(entry._1).build()
      val id = task.calculationGid
      val archiveId = treeTask.archiveId
      val fileName = s"$id.h5"
      val targetFolder = tmpResultsFolder.resolve(treeTask.prefixFolder).resolve(treeTask.archiveId)
      val location = targetFolder.resolve(fileName)
      assert(
        location.toFile.exists(),
        s"HDF5 file '$location' with parsing results does not exist"
      )
      validateHDF5(targetFolder, archiveId, metaInfo, checkSingleCalculationHDFContent(sample),
        Some(fileName))
    }
    succeed
  }

  def assertValidityOfGeneratedMergedHDF5File(sample: TestTreeData, treeTask: FileTree,
    tmpResultsFolder: Path, metaInfo: MetaInfoEnv): Assertion = {
    val id = treeTask.archiveId
    val fileName = treeTask.fileName
    val targetFolder = tmpResultsFolder.resolve(treeTask.prefixFolder)
    val location = targetFolder.resolve(fileName)
    assert(location.toFile.exists(), s"parsing results HDF5 file '$location' does not exist")
    validateHDF5(targetFolder, id, metaInfo,
      checkMergedCalculationsHDFContent(sample, mutable.Set()), Some(fileName.toString))
    succeed
  }

  private def checkSingleCalculationHDFContent(testData: TestTreeData)(calc: CalculationH5, mainFileUri: String): Unit = {
    val key = testData.candidateCalculationsWithParsers.keySet.find(mainFileUri.endsWith)
    assert(key.nonEmpty, "could not map calculation's main file URI to reference data")
    val expectedParser = testData.candidateCalculationsWithParsers(key.get)
    val attributeId = H5Lib.attributeOpen(calc.calculationGroup, "parserInfo", null)
    val parserInfoStrings = H5Lib.attributeReadStr(attributeId)
    assert(parserInfoStrings.length == 1, "expected exactly one parser info block")
    val parserInfo = JsonUtils.parseStr(parserInfoStrings.head)
    H5Lib.attributeClose(attributeId)
    assert((parserInfo \ "name").extract[String] == expectedParser, "not the expected parser")
  }

  private def checkMergedCalculationsHDFContent(testData: TestTreeData, finished: mutable.Set[String])(calc: CalculationH5, mainFileUri: String): Unit = {
    val key = testData.candidateCalculationsWithParsers.keySet.find(mainFileUri.endsWith)
    assert(key.nonEmpty, "could not map calculation's main file URI to reference data")
    assert(finished.add(key.get), s"calculation '${key.get}' was already processed before")
    val expectedParser = testData.candidateCalculationsWithParsers(key.get)
    val attributeId = H5Lib.attributeOpen(calc.calculationGroup, "parserInfo", null)
    val parserInfoStrings = H5Lib.attributeReadStr(attributeId)
    assert(parserInfoStrings.length == 1, "expected exactly one parser info block")
    val parserInfo = JsonUtils.parseStr(parserInfoStrings.head)
    H5Lib.attributeClose(attributeId)
    assert((parserInfo \ "name").extract[String] == expectedParser, "not the expected parser")
  }

}
