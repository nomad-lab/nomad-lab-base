package eu.nomad_lab.integrated_pipeline_end_to_end_tests

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.{ Main, OutputType }
import eu.nomad_lab.integrated_pipeline_tests.helpers.CustomMatchers
import eu.nomad_lab.integrated_pipeline_tests.{ EndToEnd, PendingEndToEnd }

/**
 * This Test Suite contains the End-to-End tests for the Integrated Pipeline that are not yet
 * fully implemented
 *
 * These tests validate some basic assumptions on the generated output, but the queries made are
 * simple enough to assume that the tests are independent of the parser versions.
 */
class UnderDevelopment extends LoggingTestAsyncWrapper with CustomMatchers {

  "The Integrated Pipeline" when {

  }
}
