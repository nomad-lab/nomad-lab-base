package eu.nomad_lab.integrated_pipeline_end_to_end_tests

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.{ Main, OutputType }
import eu.nomad_lab.integrated_pipeline_tests.EndToEnd
import eu.nomad_lab.integrated_pipeline_tests.helpers.CustomMatchers
import org.scalatest.AsyncWordSpec

/**
 * This Test Suite contains the End-to-End tests for the Integrated Pipeline that are fully
 * implemented and must not be regressed when changing the implementation.
 *
 * These tests validate some basic assumptions on the generated output, but the queries made are
 * simple enough to assume that the tests are independent of the parser versions.
 */
class WorkingFeatures extends LoggingTestAsyncWrapper with CustomMatchers {

  val metaInfo = Main.metaInfo

  "The Integrated Pipeline" when {
    "invoked in console mode with a single worker" should {
      "process the given directory file tree and generate a json parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new SingleWorkerFixture(TreeType.Directory, OutputType.Json)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedJsonFiles(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given directory file tree and generate a HDF5 parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new SingleWorkerFixture(TreeType.Directory, OutputType.HDF5)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedHDF5Files(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given directory file tree and generate one merged HDF5 parse report for all calculations" taggedAs EndToEnd in { () =>
        val f = new SingleWorkerFixture(TreeType.Directory, OutputType.HDF5merged)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedMergedHDF5File(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given rawdata zip archive and generate a json parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new SingleWorkerFixture(TreeType.Zip, OutputType.Json)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedJsonFiles(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given rawdata zip archive and generate a HDF5 parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new SingleWorkerFixture(TreeType.Zip, OutputType.HDF5)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedHDF5Files(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given rawdata zip archive and generate one merged HDF5 parse report for all calculations" taggedAs EndToEnd in { () =>
        val f = new SingleWorkerFixture(TreeType.Zip, OutputType.HDF5merged)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedMergedHDF5File(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }
    }

    "invoked in console mode with multiple workers" should {
      "process the given directory file tree and generate a json parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new MultiWorkerFixture(TreeType.Directory, OutputType.Json, 4)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedJsonFiles(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given directory file tree and generate a HDF5 parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new MultiWorkerFixture(TreeType.Directory, OutputType.HDF5, 5)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedHDF5Files(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given directory file tree and generate one merged HDF5 parse report for all calculations" taggedAs EndToEnd in { () =>
        val f = new MultiWorkerFixture(TreeType.Directory, OutputType.HDF5merged, 6)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedMergedHDF5File(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given rawdata zip archive and generate a json parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new MultiWorkerFixture(TreeType.Zip, OutputType.Json, 4)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedJsonFiles(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given rawdata zip archive and generate a HDF5 parse report for each calculation" taggedAs EndToEnd in { () =>
        val f = new MultiWorkerFixture(TreeType.Zip, OutputType.HDF5, 5)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedHDF5Files(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }

      "process the given rawdata zip archive and generate one merged HDF5 parse report for all calculations" taggedAs EndToEnd in { () =>
        val f = new MultiWorkerFixture(TreeType.Zip, OutputType.HDF5merged, 6)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          assertValidityOfGeneratedMergedHDF5File(f.sample, f.treeTask, f.tmpResultsFolder, metaInfo)
        }
      }
    }

    "invoked in console mode" should {
      "skip parse requests for non-existent file tree and log an error" taggedAs EndToEnd in { log =>
        val f = new NonExistingTreeFixture(TreeType.Directory, OutputType.Json)
        val finished = Main.mainWithFuture(f.generateConsoleArgumentList(Some(info)))
        finished.map { _ =>
          atLeast(1, log.messages()) should (startWith("ERROR") and
            includeAll("purely-imaginary", "not exist", "encountered failure", "ignoring task"))
        }
      }
    }
  }

}
