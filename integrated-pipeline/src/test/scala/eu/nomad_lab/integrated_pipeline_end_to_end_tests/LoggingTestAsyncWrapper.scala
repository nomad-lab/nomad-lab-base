package eu.nomad_lab.integrated_pipeline_end_to_end_tests

import eu.nomad_lab.integrated_pipeline_tests.helpers.LoggingTestBase
import org.scalatest.{ FutureOutcome, fixture }

trait LoggingTestAsyncWrapper extends fixture.AsyncWordSpec with LoggingTestBase {

  override protected[this] val appenderName: String = "ParsingModule-EndToEndTests"

  def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val logMessages = addAppender()
    complete {
      withFixture(test.toNoArgAsyncTest(logMessages))
    } lastly {
      purgeConfiguration()
    }
  }

}
