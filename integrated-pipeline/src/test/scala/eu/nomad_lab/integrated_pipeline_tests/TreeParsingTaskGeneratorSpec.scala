package eu.nomad_lab.integrated_pipeline_tests

import eu.nomad_lab.TreeType
import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.integrated_pipeline.{ FileTree, ParsingTaskGenerator }
import eu.nomad_lab.integrated_pipeline.io_integrations.{ DirectoryTreeParsingTaskGenerator, ZipTreeParsingTaskGenerator }
import eu.nomad_lab.parsers.AllParsers
import org.scalatest.{ Matchers, WordSpec }

class TreeParsingTaskGeneratorSpec extends WordSpec with Matchers {

  private val parsers = AllParsers.defaultParserCollection

  case class TestGenerator(
    generator: (FileTree) => ParsingTaskGenerator, treeType: TreeType
  )

  "a ZipTreeParsingRequestGenerator" when {
    val testCase = TestGenerator(x => new ZipTreeParsingTaskGenerator(x, parsers), TreeType.Zip)

    executeSharedTests(testCase)

  }

  "a DirectoryTreeParsingRequestGenerator" when {
    val testCase = TestGenerator(x => new DirectoryTreeParsingTaskGenerator(x, parsers), TreeType.Directory)

    executeSharedTests(testCase)

  }

  private def executeSharedTests(testCase: TestGenerator): Unit = {
    s"parsing a ${testCase.treeType} file tree with valid calculations" should {

      "extract all entries from the file tree" in {
        Seq(testData1, testData2).foreach { sampleData =>
          val inbound = createFileTreeScanRequest(sampleData, testCase.treeType)
          val generator = testCase.generator(inbound)
          val requests = generator.toSeq
          all(requests) should be(a[Right[_, _]])
          val tasks = requests.collect { case Right(x) => x }
          val associations = tasks.map(x => x.relativePath.toString -> x.parserName).toMap
          assert(
            associations === sampleData.candidateCalculationsWithParsers,
            "generated parsing requests don't match expectations"
          )
        }
      }

      "throw an exception if more elements are requested after the iterator is exhausted" in {
        val inbound = createFileTreeScanRequest(testData2, testCase.treeType)
        val generator = testCase.generator(inbound)
        generator.toSeq.size //fight some laziness
        assertThrows[NoSuchElementException](generator.next(), "calling next on exhausted iterator")
      }
    }

    s"receiving a request to parse a non-${testCase.treeType} file tree" should {
      "reject the request with an exception" in {
        TreeType.values.filter(_ != testCase.treeType).foreach { otherTreeType =>
          val inbound = createFileTreeScanRequest(testData1, otherTreeType)
          assertThrows[IllegalArgumentException] {
            testCase.generator(inbound)
          }
        }
      }
    }
  }
}
