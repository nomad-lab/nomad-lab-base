package eu.nomad_lab.integrated_pipeline_tests

import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.integrated_pipeline.{ EventListener, FileTree, ParsingTaskGenerator, TreeScanner }
import eu.nomad_lab.integrated_pipeline_tests.matchers._
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.mockito.{ ArgumentCaptor, Mockito }
import org.scalatest.WordSpec
import org.scalatest.mockito.MockitoSugar

import scala.collection.JavaConverters._

class TreeScannerSpec extends WordSpec with TestDataBuilders with MockitoSugar
    with MessageProcessorBehaviour[FileTree, TreeScanSignal] {

  private def argsCaptor(): ArgumentCaptor[TreeParserEvent] = ArgumentCaptor.forClass(classOf[TreeParserEvent])

  private val sampleFileTree = aFileTree().withBasePath(".").build()
  private val invalidFileTree = aFileTree().withBasePath("/imaginary/foo/magic").build()

  class Fixture(sampleData: Iterator[Either[TreeParserEventScanError, CandidateFound]]) {
    val events = mock[EventListener]
    val taskGenerator = mock[ParsingTaskGenerator]
    val treeParser = new TreeScanner {
      override val eventListener = events
      override def createProcessor(task: FileTree) = taskGenerator
      override protected[this] val eventReporterName: Option[String] = Some("TreeParserSpec")
    }

    when(taskGenerator.hasNext).thenAnswer {
      new Answer[Boolean] {
        override def answer(invocation: InvocationOnMock): Boolean = sampleData.hasNext
      }
    }
    when(taskGenerator.next()).thenAnswer {
      new Answer[Either[TreeParserEventScanError, CandidateFound]] {
        override def answer(invocation: InvocationOnMock): Either[TreeParserEventScanError, CandidateFound] = {
          sampleData.next()
        }
      }
    }
  }

  def noTaskReceived(): Fixture = {
    val data: Iterator[Either[TreeParserEventScanError, CandidateFound]] = (1 to 3).map { i =>
      Right(aCandidateFound().withFileTree(sampleFileTree).withRelativePath(s"file$i").build())
    }.iterator
    new Fixture(data)
  }

  def taskReceivedWillCreateScanError(): Fixture = {
    val data: Iterator[Either[TreeParserEventScanError, CandidateFound]] = Iterator.single(
      Left(aTreeParserEventScanError().withFileTree(sampleFileTree).withRelativePath("nullPointer").
        withError(new UnsupportedOperationException("failing test case")) build ())
    )
    val f = new Fixture(data)
    f.treeParser.processSignal(sampleFileTree)
    f
  }

  def taskReceivedNoSignalsFetched(): Fixture = {
    val f = noTaskReceived()
    f.treeParser.processSignal(sampleFileTree)
    f
  }

  def oneParsingTaskLeftToEmit(): Fixture = {
    val f = taskReceivedNoSignalsFetched()
    (1 to 2).foreach(_ => f.treeParser.getNextSignalToEmit())
    f
  }

  def allParsingTasksFetched(): Fixture = {
    val f = taskReceivedNoSignalsFetched()
    (1 to 3).foreach(_ => f.treeParser.getNextSignalToEmit())
    f
  }

  def allSignalsFetched(): Fixture = {
    val f = taskReceivedNoSignalsFetched()
    (1 to 4).foreach(_ => f.treeParser.getNextSignalToEmit())
    f
  }

  "A TreeParser" when {
    "not having received a file tree scan task" should {
      def createFixture = () => noTaskReceived()

      behave like processorWithNoSignalReady(() => createFixture().treeParser, Seq(aFileTree()))

      "register with the provided EventListener when receiving the first message" in {
        val f = createFixture()
        f.treeParser.processSignal(sampleFileTree)
        verify(f.events).registerReporter(f.treeParser, Some("TreeParserSpec"))
      }

      "notify the provided EventListener when receiving a new file tree scan task" in {
        import MatcherTreeParserEventStart._
        val f = createFixture()
        f.treeParser.processSignal(sampleFileTree)
        val captor = argsCaptor()
        verify(f.events).processEvent(any(), captor.capture())
        captor.getValue should (be(a[TreeParserEventStart]) and have(basePath(".")))
      }

      "ignore file tree scan tasks if the tree root does not exist on file-system" in {
        val f = createFixture()
        f.treeParser.processSignal(invalidFileTree)
        f.treeParser.hasSignalToEmit() should be(false)
      }

      "send an error event to the eventListener if the tree root does not exist on file-system" in {
        import MatcherTreeParserEventTreeFailure._
        val f = createFixture()
        f.treeParser.processSignal(invalidFileTree)
        val captor = argsCaptor()
        verify(f.events, atLeastOnce).processEvent(any(), captor.capture())
        val events = captor.getAllValues.asScala.collect { case x: TreeParserEventTreeFailure => x }
        exactly(1, events) should have(
          basePath("/imaginary/foo/magic"),
          errorMessage("tree root", "not exist")
        )
      }
    }

    "having received a new file tree scan task" should {
      def createFixture = () => taskReceivedNoSignalsFetched()

      behave like processorWithOutboundSignalReady(
        () => createFixture().treeParser,
        Seq(aFileTree()),
        be(a[CandidateFound]) and have(TreeScanSignalMatchers.relativePath("file1"))
      )

      "emit parse requests for all potential calculations in the file tree" in {
        import TreeScanSignalMatchers._
        val f = createFixture()
        val signals = (1 to 3).map { _ => f.treeParser.getNextSignalToEmit() }.collect { case x: CandidateFound => x }
        signals should have length 3
        exactly(1, signals) should have(relativePath("file1"))
        exactly(1, signals) should have(relativePath("file2"))
        exactly(1, signals) should have(relativePath("file3"))
      }

      "notify the provided EventListener for each emitted candidate calculation" in {
        import MatcherTreeParserEventCandidate._
        val f = createFixture()
        (1 to 3).map { _ => f.treeParser.getNextSignalToEmit() }
        val captor = argsCaptor()
        verify(f.events, Mockito.atLeast(0)).processEvent(any(), captor.capture())
        val events = captor.getAllValues.asScala.collect { case x: TreeParserEventCandidate => x }
        events should have length 3
        exactly(1, events) should have(relativePath("file1"))
        exactly(1, events) should have(relativePath("file2"))
        exactly(1, events) should have(relativePath("file3"))
      }
    }

    "having one parsing task left to emit" should {
      def createFixture = () => oneParsingTaskLeftToEmit()

      behave like processorWithOutboundSignalReady(
        () => createFixture().treeParser,
        Seq(aFileTree()),
        be(a[CandidateFound]) and have(TreeScanSignalMatchers.relativePath("file3"))
      )
    }

    "having emitted all parsing tasks, but not the end tree signal yet" should {
      def createFixture = () => allParsingTasksFetched()

      behave like processorWithOutboundSignalReady(
        () => createFixture().treeParser,
        Seq(aFileTree()),
        be(a[TreeScanCompleted]) and have(TreeScanSignalMatchers.numParsingTasks(3))
      )

      "emit an end tree signal with the correct number of entries" in {
        import TreeScanSignalMatchers._
        val f = createFixture()
        val result = f.treeParser.getNextSignalToEmit()
        result should be(a[TreeScanCompleted])
        result.asInstanceOf[TreeScanCompleted] should have(fileTree(sampleFileTree), numParsingTasks(3))
      }

      "notify the provided EventListener when emitting the end tree signal" in {
        import MatcherTreeParserEventEnd._
        val f = createFixture()
        f.treeParser.getNextSignalToEmit()
        val captor = argsCaptor()
        verify(f.events, Mockito.atLeast(1)).processEvent(any(), captor.capture())
        val event = captor.getValue
        event should be(a[TreeParserEventEnd])
        event.asInstanceOf[TreeParserEventEnd] should have(basePath("."), numCandidates(3))
      }
    }

    "having emitted all tasks and the end tree signal" should {
      def createFixture = () => allSignalsFetched()

      behave like processorWithNoSignalReady(
        () => createFixture().treeParser,
        Seq(aFileTree())
      )
    }

    "receiving a file scanning error from the task generator" should {
      def createFixture = () => taskReceivedWillCreateScanError()

      "send a notification to the event listener" in {
        import MatcherTreeParserEventScanError._
        val f = createFixture()
        f.treeParser.getNextSignalToEmit()
        val captor = argsCaptor()
        verify(f.events, Mockito.atLeast(1)).processEvent(any(), captor.capture())
        val events = captor.getAllValues.asScala.collect { case x: TreeParserEventScanError => x }
        events should have size 1
        events.head should have(errorMessage("failing", "test case"), relativePath("nullPointer"))
      }
    }
  }

}
