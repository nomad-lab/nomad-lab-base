package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.integrated_pipeline.{ CalculationParsingEngine, EventListener }
import eu.nomad_lab.integrated_pipeline_tests.matchers.{ FileParsingSignalMatchers, MatcherCalculationParserEventEnd, MatcherCalculationParserEventStart }
import eu.nomad_lab.meta.KnownMetaInfoEnvs
import eu.nomad_lab.parsers.ParseResult.ParseResult
import eu.nomad_lab.parsers._
import org.json4s.JsonAST.{ JInt, JObject, JString }
import org.mockito.ArgumentMatchers.{ eq => raw, _ }
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.mockito.{ ArgumentCaptor, Mockito }
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ Matchers, WordSpec }

import scala.collection.JavaConverters._

class CalculationParsingEngineSpec extends WordSpec with MockitoSugar with TestDataBuilders with Matchers {

  val parserInfo: JObject = JObject(
    "name" -> JString("dummyParser")
  )

  private def argsCaptor(): ArgumentCaptor[CalculationParserEvent] = ArgumentCaptor.forClass(classOf[CalculationParserEvent])

  def sampleEvents = Seq(
    OpenSectionWithGIndex("section_foo", -1),
    AddRealValue("bar", 0.42, -1),
    AddValue("blob", JString("some value"), -1),
    AddValue("blob_count", JInt(42), -1),
    CloseSection("section_foo", -1)
  )

  val metaInfo = KnownMetaInfoEnvs.all
  val sampleParseRequest = aCandidateFound().withParserName("dummyParser")

  class Fixture {
    val dummyParserCollection = mock[ParserCollection]
    val dummyParserGenerator = mock[ParserGenerator]
    val dummyParser = mock[OptimizedParser]
    val dummyEvents = mock[EventListener]
    when(dummyParserCollection.parsers).thenReturn(Map("dummyParser" -> dummyParserGenerator))
    when(dummyParserGenerator.optimizedParser(any())).thenReturn(dummyParser)
    when(dummyParser.parserGenerator).thenReturn(dummyParserGenerator)
    when(dummyParserGenerator.parserInfo).thenReturn(parserInfo)
    when(dummyParserGenerator.name).thenReturn("dummyParser")

    val worker = new CalculationParsingEngine(dummyParserCollection, metaInfo, dummyEvents, None)

    def prepareParserStandardInvocation(): Unit = {
      when(dummyParser.parseExternal(any(), any(), any(), any())).thenAnswer(new Answer[ParseResult] {
        override def answer(invocation: InvocationOnMock): ParseResult = {
          val backend = invocation.getArgument[ParserBackendExternal](2)
          val mainFileUri = invocation.getArgument[String](0)
          backend.startedParsingSession(Some(mainFileUri), parserInfo)
          sampleEvents.foreach(x => x.emitOnBackend(backend))
          backend.finishedParsingSession(
            Some(ParseResult.ParseSkipped),
            mainFileUri = Some(mainFileUri)
          )
          ParseResult.ParseSkipped
        }
      })
    }

    def prepareParserFailureInvocation(): Unit = {
      when(dummyParser.parseExternal(any(), any(), any(), any())).thenAnswer(new Answer[ParseResult] {
        override def answer(invocation: InvocationOnMock): ParseResult = {
          val backend = invocation.getArgument[ParserBackendExternal](2)
          val mainFileUri = invocation.getArgument[String](0)
          backend.startedParsingSession(Some(mainFileUri), parserInfo)
          sampleEvents.foreach(x => x.emitOnBackend(backend))
          throw new UnsupportedOperationException("parsing process just crashed!")
        }
      })
    }
  }

  "a CalculationParsingEngine" when {

    "being created" should {
      "register itself with the provided event listener" in {
        val f = new Fixture
        verify(f.dummyEvents).registerReporter(raw(f.worker), any())
      }
    }

    "handling parsing tasks" should {
      "record starting and finishing parse events" in {
        val f = new Fixture
        f.prepareParserStandardInvocation()
        val result = f.worker.processSignal(sampleParseRequest).asInstanceOf[FileParsingResult]
        assert(result.start.nonEmpty, "expected a start event to be emitted")
        assert(result.end.nonEmpty, "expected a finish event to be emitted")
      }

      "send a start parsing event to the event listener" in {
        import MatcherCalculationParserEventStart._
        val f = new Fixture
        f.prepareParserStandardInvocation()
        f.worker.processSignal(sampleParseRequest.withRelativePath("blabla"))
        val captor = argsCaptor()
        verify(f.dummyEvents, Mockito.atLeast(1)).processEvent(any(), captor.capture())
        val signals = captor.getAllValues.asScala.collect { case x: CalculationParserEventStart => x }
        exactly(1, signals) should have(relativePath("blabla"), parser("dummyParser"))
      }

      "send a finished parsing event to the event listener (parsing terminated normally)" in {
        import MatcherCalculationParserEventEnd._
        val f = new Fixture
        f.prepareParserStandardInvocation()
        f.worker.processSignal(sampleParseRequest.withRelativePath("blabla"))
        val captor = argsCaptor()
        verify(f.dummyEvents, Mockito.atLeast(1)).processEvent(any(), captor.capture())
        val signals = captor.getAllValues.asScala.collect { case x: CalculationParserEventEnd => x }
        exactly(1, signals) should have(relativePath("blabla"), parser("dummyParser"),
          status(ParseResult.ParseSkipped))
      }

      "send a finished parsing event to the event listener (parsing fails)" in {
        import MatcherCalculationParserEventEnd._
        val f = new Fixture
        f.prepareParserFailureInvocation()
        f.worker.processSignal(sampleParseRequest.withRelativePath("blabla"))
        val captor = argsCaptor()
        verify(f.dummyEvents, Mockito.atLeast(1)).processEvent(any(), captor.capture())
        val signals = captor.getAllValues.asScala.collect { case x: CalculationParserEventEnd => x }
        exactly(1, signals) should have(relativePath("blabla"), parser("dummyParser"),
          status(ParseResult.ParseFailure), errorMessage("just", "crashed"))
      }

      "record all events emitted by the parser in the correct order" in {
        import FileParsingSignalMatchers._
        val f = new Fixture
        f.prepareParserStandardInvocation()
        val result = f.worker.processSignal(sampleParseRequest).asInstanceOf[FileParsingResult]
        result should have(events(sampleEvents))
      }

      "handle parsing failures and return an appropriate result" in {
        import FileParsingSignalMatchers._
        val f = new Fixture
        f.prepareParserFailureInvocation()
        val result = f.worker.processSignal(sampleParseRequest).asInstanceOf[FileParsingResult]
        result should have(status(ParseResult.ParseFailure), errorMessage("had exception", "just crashed"))
      }

      "gracefully fail parsing requests with unknown parsers" in {
        import FileParsingSignalMatchers._
        val f = new Fixture
        val anotherRequest = sampleParseRequest.withParserName("nonSenseParser")
        val result = f.worker.processSignal(anotherRequest).asInstanceOf[FileParsingResult]
        result should have(status(ParseResult.ParseFailure), errorMessage("unknown", "nonSenseParser"))
      }

      "parse the main file when handling directories" in {
        val f = new Fixture()
        val treeTask = aFileTree().withTreeType(TreeType.Directory).withBasePath("/dir/bla")
        val task = sampleParseRequest.withFileTree(treeTask).withRelativePath("magic")
        f.worker.processSignal(task)
        val expectedPath = Paths.get("/dir/bla/magic")
        verify(f.dummyParser).parseExternal(any(), raw(expectedPath), any(), any())
      }

      "parse the temporarily extracted main file when handling zip archives" in {
        val f = new Fixture
        val treeTask = aFileTree().withTreeType(TreeType.Zip).withBasePath(s"/foo/R${"x" * 28}.zip")
        val extractedPath = Paths.get("/tmp/extracted/magic")
        val task = sampleParseRequest.withFileTree(treeTask).withExtractedPath(Some(extractedPath))
        f.worker.processSignal(task)
        verify(f.dummyParser).parseExternal(any(), raw(extractedPath), any(), any())
      }

      "gracefully fail parsing tasks from zip archives without extracted file path" in {
        import FileParsingSignalMatchers._
        val f = new Fixture
        val treeTask = aFileTree().withTreeType(TreeType.Zip)
        val task = sampleParseRequest.withFileTree(treeTask)
        val result = f.worker.processSignal(task).asInstanceOf[FileParsingResult]
        result should have(status(ParseResult.ParseFailure), errorMessage("extracted file path", "missing"))
      }

      "gracefully fail parsing requests with unknown or not supported file tree types" in {
        import FileParsingSignalMatchers._
        val f = new Fixture
        Seq(TreeType.Unknown, TreeType.File, TreeType.Tar).foreach { treeType =>
          val treeTask = aFileTree().withTreeType(treeType)
          val task = sampleParseRequest.withFileTree(treeTask)
          val result = f.worker.processSignal(task).asInstanceOf[FileParsingResult]
          result should have(
            status(ParseResult.ParseFailure),
            errorMessage("not supported", "file tree type", treeType.toString)
          )
        }
      }

    }

    "handling other signals" should {

      "forward file tree finished signals unchanged" in {
        val f = new Fixture
        val signal = aTreeScanCompleted().build()
        val result = f.worker.processSignal(signal)
        result should be(signal)
      }
    }
  }

}
