package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.h5.CalculationH5
import eu.nomad_lab.integrated_pipeline.io_integrations.WriteToHDF5MergedResultsProcessor
import eu.nomad_lab.integrated_pipeline.messages.CandidateFound
import eu.nomad_lab.meta.KnownMetaInfoEnvs
import eu.nomad_lab.{ H5Lib, JsonUtils, TreeType }
import org.scalatest.{ Matchers, WordSpec }

import scala.collection.mutable

class WriteToHDF5MergedResultsProcessorSpec extends WordSpec with Matchers with TestDataBuilders {

  val sampleTree = aFileTree().withBasePath(s"/foo/R${"x" * 28}.zip").withTreeType(TreeType.Directory).build()
  val metaData = KnownMetaInfoEnvs.all

  def validateHDFContent(expectedMainFileUris: mutable.Set[String])(calc: CalculationH5, mainFileUri: String): Unit = {
    assert(
      expectedMainFileUris.remove(mainFileUri),
      "could not map calculation's main file URI to reference data"
    )
    val attributeId = H5Lib.attributeOpen(calc.calculationGroup, "parserInfo", null)
    val parserInfoStrings = H5Lib.attributeReadStr(attributeId)
    assert(parserInfoStrings.length == 1, "expected exactly one parser info block")
    val parserInfo = JsonUtils.parseStr(parserInfoStrings.head)
    H5Lib.attributeClose(attributeId)
    assert((parserInfo \ "name").extract[String] == "dummyParser", "not the expected parser")
    val programNames = calc.valueTable(Seq("section_run", "program_name")).map(_.stringValue)
    assert(programNames.size == 1, s"expected exactly one program name")
    assert(programNames.head == "magic_code", "expected program name not found")
  }

  "A WriteToHDF5MergedResultsProcessor" when {
    "processing successful parsing results from a single tree" should {
      "write a merged valid NOMAD metaData HDF5 archive for all results" in {
        val tempDir = generateTempTestDirectory("hdf5merged-results-processor")
        tempDir.toFile.deleteOnExit()
        val writer = new WriteToHDF5MergedResultsProcessor(tempDir, metaData)
        val inputs = (1 to 3).map(x => CandidateFound(sampleTree, Paths.get(s"file$x"), "dummyParser"))
        writer.startProcessingTreeResults(sampleTree)
        inputs.foreach(x => writer.processFileParsingResult(createSuccessfulFileParsingResult(x)))
        writer.finishProcessingTreeResults(sampleTree)

        val id = sampleTree.archiveId
        val location = writer.outputLocation(sampleTree)
        val fileName = location.resolve(sampleTree.fileName)
        assert(fileName.toFile.exists(), s"parsing results HDF5 file '$fileName' does not exist")
        val expectedMainFileUris = inputs.map(x =>
          x.fileTree.treeBasePath.resolve(x.relativePath).toUri.toString).to[mutable.Set]
        validateHDF5(location, id, metaData, validateHDFContent(expectedMainFileUris))

      }

    }

    "determining the output location" should {
      "return the appropriate output location for a directory file tree" in {
        val f = new WriteToHDF5MergedResultsProcessor(Paths.get("/non/existing/location"), metaData)
        val fileTree = aFileTree().withTreeType(TreeType.Directory).withBasePath("foo/bargus")
        f.outputLocation(fileTree) should be(Paths.get("/non/existing/location/bar"))
      }

      "return the appropriate output location for a zip archive file tree" in {
        val f = new WriteToHDF5MergedResultsProcessor(Paths.get("/non/existing/location"), metaData)
        val fileTree = aFileTree().withTreeType(TreeType.Zip).withBasePath(s"foo/R${"x" * 28}.zip")
        f.outputLocation(fileTree) should be(Paths.get(s"/non/existing/location/Rxx"))
      }
    }

  }
}
