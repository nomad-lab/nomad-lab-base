package eu.nomad_lab.integrated_pipeline_tests.helpers

import org.scalatest.Matchers
import org.scalatest.matchers.Matcher

trait CustomMatchers extends Matchers {

  def includeAll(first: String, other: String*): Matcher[String] = {
    other.foldLeft(include(first)) { (condition, next) => condition and include(next) }
  }
}
