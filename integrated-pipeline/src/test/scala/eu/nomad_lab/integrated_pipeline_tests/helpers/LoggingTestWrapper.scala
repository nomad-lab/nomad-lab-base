package eu.nomad_lab.integrated_pipeline_tests.helpers

import org.scalatest.{ Outcome, fixture }

trait LoggingTestWrapper extends fixture.WordSpec with LoggingTestBase {

  override protected[this] val appenderName: String = "ParsingModule-EndToEndTests"

  def withFixture(test: OneArgTest): Outcome = {
    val logMessages = addAppender()
    try {
      withFixture(test.toNoArgTest(logMessages))
    } finally {
      purgeConfiguration()
    }
  }

}