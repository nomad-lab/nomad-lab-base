package eu.nomad_lab.integrated_pipeline_tests.helpers

import java.io.{ ByteArrayOutputStream, OutputStream }

import org.apache.logging.log4j.core.LoggerContext
import org.apache.logging.log4j.core.appender.OutputStreamAppender
import org.apache.logging.log4j.core.layout.PatternLayout

import scala.util.Properties

/**
 * Helper trait for testing log4j2 based logging implementations, the functions provided here can
 * be used to append the logged messages to a ByteArray for inspection after the test code ran.
 * This is an isolated trait because we have to provide this functionality both for synchronous
 * and asynchronous ScalaTest suites.
 */
trait LoggingTestBase {

  //TODO: consider a streaming implementation if the logging volume becomes significant

  type FixtureParam = LoggingOutput

  protected[this] val appenderName: String

  def addAppender(): LoggingOutput = {
    val rawBytes = new ByteArrayOutputStream()
    val context = LoggerContext.getContext(false)
    val config = context.getConfiguration
    val layout = PatternLayout.newBuilder().withPattern("%-5level [%marker] - %msg%n").build()
    val appender = OutputStreamAppender.createAppender(layout, null, rawBytes, appenderName, false, true)
    appender.start()
    config.addAppender(appender)
    val loggerConfig = config.getLoggerConfig("eu.nomad_lab")
    val (level, filter) = (null, null)
    loggerConfig.addAppender(appender, level, filter)
    new LoggingOutput(rawBytes)
  }

  def purgeConfiguration(): Unit = {
    val context = LoggerContext.getContext(false)
    context.reconfigure()
  }

  class LoggingOutput(private val byteStream: ByteArrayOutputStream) {

    def messages(): Seq[String] = {
      val output = byteStream.toString
      byteStream.reset()
      output.split(Properties.lineSeparator)
    }
  }
}
