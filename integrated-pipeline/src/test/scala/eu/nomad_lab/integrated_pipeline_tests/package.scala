package eu.nomad_lab

import java.nio.file.{ Files, Path, Paths }

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.h5.{ CalculationH5, FileH5 }
import eu.nomad_lab.integrated_pipeline.FileTree
import eu.nomad_lab.integrated_pipeline.messages.{ CandidateFound, ParsingResultInMemory }
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.parsers._
import org.json4s.JsonAST.{ JNothing, JObject, JString, JValue }
import org.scalatest.Tag

package object integrated_pipeline_tests {

  object EndToEnd extends Tag("eu.nomad_lab.tags.EndToEnd")
  object PendingEndToEnd extends Tag("eu.nomad_lab.tags.PendingEndToEnd")

  implicit val system = ActorSystem("IntegratedPipelineTests")
  implicit val materializer = ActorMaterializer()

  case class TestTreeData(
    baseName: String,
    candidateCalculationsWithParsers: Map[String, String]
  )

  private val cp2kParserName = "Cp2kParser"
  private val fhiAimsParserName = "FhiAimsParser"
  private val turbomoleParserName = "TurbomoleParser"

  def generateTempTestDirectory(name: String): Path = {
    Files.createTempDirectory(s"test-nomad-parsing-$name", LocalEnv.directoryPermissionsAttributes)
  }

  val testData1 = TestTreeData("RbUCrbTMFSzoPjoVh8ueVMyWXVk-C", Map(
    "RbUCrbTMFSzoPjoVh8ueVMyWXVk-C/data/test_simple/sp.out" -> cp2kParserName
  ))
  val testData2 = TestTreeData("RkRs2Nsb3oJnsIJkyEaXX69Na8vvS", Map(
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/211/1/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/221/10/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/222/1/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/221/1/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/222/10/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/211/10/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/221/5/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/222/5/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/211/5/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/221/2/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/222/2/PBE-tight-relax" -> fhiAimsParserName,
    "RkRs2Nsb3oJnsIJkyEaXX69Na8vvS/data/BaSiO3/211/2/PBE-tight-relax" -> fhiAimsParserName
  ))
  val testData3 = TestTreeData("RsAis5WiWp0NqkegeQaSLIUuAkO8N", Map(
    "RsAis5WiWp0NqkegeQaSLIUuAkO8N/data/def2-TZVP-pbe/dft1.out" -> turbomoleParserName,
    "RsAis5WiWp0NqkegeQaSLIUuAkO8N/data/def2-TZVP-pbe/dft2.out" -> turbomoleParserName,
    "RsAis5WiWp0NqkegeQaSLIUuAkO8N/data/def2-TZVP-pbe/dft3.out" -> turbomoleParserName,
    "RsAis5WiWp0NqkegeQaSLIUuAkO8N/data/def2-TZVP-pbe/escf.out" -> turbomoleParserName,
    "RsAis5WiWp0NqkegeQaSLIUuAkO8N/data/def2-TZVP-pbe/define.out" -> turbomoleParserName
  ))
  val testData4 = TestTreeData("R1LGuYmO0fvYzWlcgRFzZOqVEdfXi", Map(
    "R1LGuYmO0fvYzWlcgRFzZOqVEdfXi/data/CO-NiMgO.ricc2.out" -> turbomoleParserName,
    "R1LGuYmO0fvYzWlcgRFzZOqVEdfXi/data/CO-NiMgO.dscf.out" -> turbomoleParserName
  ))

  val allTestData = Seq(testData1, testData2, testData3, testData4)

  def createFileTreeScanRequest(archive: TestTreeData, mode: TreeType): FileTree = {
    val baseDir = sys.props.get("user.dir").get
    val prefix = archive.baseName.substring(0, 3)
    mode match {
      case TreeType.Zip => FileTree(
        treeBasePath = Paths.get(s"$baseDir/src/test/resources/$prefix/${archive.baseName}.zip"),
        treeType = mode
      )
      case TreeType.Directory => FileTree(
        treeBasePath = Paths.get(s"$baseDir/src/test/resources/$prefix/${archive.baseName}"),
        treeType = mode
      )
      case _ => FileTree(
        treeBasePath = Paths.get(s"$baseDir/src/test/resources/$prefix/${archive.baseName}"),
        treeType = mode
      )
    }

  }

  def createSuccessfulFileParsingResult(task: CandidateFound): ParsingResultInMemory = {
    val parserInfo = JObject(List(("name", JString(task.parserName))))
    ParsingResultInMemory(
      task = task,
      result = ParseResult.ParseSuccess,
      start = Some(StartedParsingSession(
        mainFileUri = Some(task.mainFileUri),
        parserInfo = parserInfo,
        parserStatus = None,
        parserErrors = JNothing
      )),
      events = Seq(
        OpenSectionWithGIndex("section_run", -1),
        AddValue("program_name", JString("magic_code")),
        CloseSection("section_run", -1)
      ),
      end = Some(FinishedParsingSession(
        parserStatus = Some(ParseResult.ParseSuccess),
        parserErrors = JNothing,
        mainFileUri = Some(task.mainFileUri),
        parserInfo = parserInfo,
        parsingStats = Map("foo-stat" -> 42l)
      )),
      error = None
    )
  }

  private val ignoredJsonEntries = Seq("type", "gIndex", "name", "references")

  def validateJsonMetaData(json: JValue, metaInfo: MetaInfoEnv): Unit = {
    json match {
      case x: JObject => x.obj.filter(ignoredJsonEntries.contains).foreach { entry =>
        assert(
          metaInfo.metaInfoRecordForName(entry._1.takeWhile(_ != '-')).nonEmpty,
          s"no metaInfo record found for entry '${entry._1}'"
        )
        validateJsonMetaData(entry._2, metaInfo)
      }
      case _ => ()
    }
  }

  def validateHDF5(archivePath: Path, archiveName: String, metaInfo: MetaInfoEnv,
    tests: (CalculationH5, String) => Unit, archiveFileName: Option[String] = None) = {
    val fileName = archiveFileName.getOrElse(archiveName + ".h5")
    val file = FileH5.open(archivePath.resolve(fileName), false, Some(metaInfo), false)
    val archive = file.openArchive(archiveName)
    var numCalculations = 0
    archive.calculations().foreach { calc =>
      val attributeId = H5Lib.attributeOpen(calc.calculationGroup, "mainFileUri", null)
      val mainFileUriList = H5Lib.attributeReadStr(attributeId)
      assert(mainFileUriList.size == 1, "expected exactly one main file URI")
      val mainFileUri = mainFileUriList.head
      H5Lib.attributeClose(attributeId)

      tests(calc, mainFileUri)
      numCalculations += 1
    }
    assert(numCalculations > 0, "no calculations found in archive")
  }

}
