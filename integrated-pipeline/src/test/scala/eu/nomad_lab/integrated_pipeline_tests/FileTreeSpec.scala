package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.TreeType
import org.scalatest.{ Matchers, WordSpec }

class FileTreeSpec extends WordSpec with Matchers with TestDataBuilders {

  "a FileTreeScanTask" should {
    "generate correct archive IDs for directory file trees" in {
      allTestData.foreach { data =>
        val fileTree = createFileTreeScanRequest(data, TreeType.Directory)
        fileTree.archiveId should be(data.baseName)
      }
    }

    "generate correct archive file names for directory file trees" in {
      allTestData.foreach { data =>
        val fileTree = createFileTreeScanRequest(data, TreeType.Directory)
        fileTree.fileName should be(Paths.get(s"${data.baseName}.h5"))
      }
    }

    "generate correct prefix folder names for directory file trees" in {
      allTestData.foreach { data =>
        val fileTree = createFileTreeScanRequest(data, TreeType.Directory)
        fileTree.prefixFolder should be(Paths.get(data.baseName.substring(0, 3)))
      }
    }

    "generate correct archive IDs for rawdata zip archive file trees" in {
      allTestData.foreach { data =>
        val fileTree = createFileTreeScanRequest(data, TreeType.Zip)
        fileTree.archiveId should be(data.baseName)
      }
    }

    "generate correct archive file names for rawdata zip archive file trees" in {
      allTestData.foreach { data =>
        val fileTree = createFileTreeScanRequest(data, TreeType.Zip)
        fileTree.fileName should be(Paths.get(s"S${data.baseName.substring(1)}.h5"))
      }
    }

    "generate correct prefix folder names for rawdata zip archive file trees" in {
      allTestData.foreach { data =>
        val fileTree = createFileTreeScanRequest(data, TreeType.Zip)
        fileTree.prefixFolder should be(Paths.get(data.baseName.substring(0, 3)))
      }
    }

    "throw an exception when handling non-rawdata zip archive file trees (unexpected filename)" in {
      val failTree = aFileTree().withTreeType(TreeType.Zip).withBasePath("/tmp/foo.zip")
      an[UnsupportedOperationException] should be thrownBy failTree.archiveId
      an[UnsupportedOperationException] should be thrownBy failTree.fileName
      an[UnsupportedOperationException] should be thrownBy failTree.prefixFolder
    }

  }

}
