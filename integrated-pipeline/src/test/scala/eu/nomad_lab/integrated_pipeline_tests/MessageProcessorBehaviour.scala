package eu.nomad_lab.integrated_pipeline_tests

import eu.nomad_lab.integrated_pipeline.MessageProcessor
import org.scalatest.matchers.Matcher
import org.scalatest.{ Matchers, WordSpec }

import scala.reflect.ClassTag

trait MessageProcessorBehaviour[I, O] extends Matchers { this: WordSpec =>

  def processorWithNoSignalReady(fixture: () => MessageProcessor[I, O], validInboundSignals: Seq[I]) {

    "have no signal ready to be fetched" in {
      fixture().hasSignalToEmit() should be(false)
    }

    "throw an exception when trying to fetch the next outbound signal" in {
      an[IllegalStateException] should be thrownBy fixture().getNextSignalToEmit()
    }

    "process valid next inbound signals" in {
      validInboundSignals.foreach(signal => fixture().processSignal(signal))
      succeed
    }
  }

  def processorWithOutboundSignalReady[U <: O: ClassTag](fixture: () => MessageProcessor[I, O], forbiddenInboundSignals: Seq[I],
    outboundTests: Matcher[U]) {

    "have a signal ready to be fetched" in {
      fixture().hasSignalToEmit() should be(true)
    }

    "return an outbound signal" in {
      fixture().getNextSignalToEmit() match {
        case x: U => x should outboundTests
        case x => fail(s"received signal $x did not have the expected class ${implicitly[ClassTag[U]].runtimeClass}")
      }
    }

    "throw an exception when trying to process the next inbound signal" in {
      forbiddenInboundSignals.foreach { signal =>
        an[IllegalStateException] should be thrownBy fixture().processSignal(signal)
      }
    }
  }
}
