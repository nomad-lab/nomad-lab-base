package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import akka.stream.ClosedShape
import akka.stream.scaladsl.{ GraphDSL, RunnableGraph }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.{ CalculationParsingEngine, FileTree }
import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.integrated_pipeline.stream_components._
import eu.nomad_lab.integrated_pipeline_tests.matchers.{ FileParsingSignalMatchers, StreamAssertions }
import eu.nomad_lab.parsers.ParseResult
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ Matchers, WordSpec }

class CalculationParsingFlowSpec extends WordSpec with MockitoSugar with TestDataBuilders with Matchers {

  val sampleTreeScan = FileTree(
    treeBasePath = Paths.get("/foo/bar"),
    treeType = TreeType.Directory
  )

  def sampleInput(fileName: String) = CandidateFound(
    fileTree = sampleTreeScan,
    relativePath = Paths.get(fileName),
    parserName = "dummyParser"
  )

  private class Fixture(numWorkers: Int) extends StreamAssertions[FileParsingSignal] {
    require(numWorkers >= 1, "need at least one dummy worker")
    val dummyWorkers = (1 to numWorkers).map { index =>
      val myMock = mock[CalculationParsingEngine]
      when(myMock.processSignal(any())).thenAnswer(new Answer[FileParsingResult] {
        override def answer(invocation: InvocationOnMock): FileParsingResult = {
          Thread.sleep(3) //pretend that each dummy task consumes a little time
          aParsingResultInMemory().
            withTask(invocation.getArgument[CandidateFound](0)).
            withParseResult(ParseResult.ParseSuccess).
            build()
        }
      })
      myMock
    }

    private val testInput = TestSource.probe[TreeScanSignal]
    private val testRequests = TestSink.probe[FileParsingSignal]
    private val testGraph = RunnableGraph.fromGraph(
      GraphDSL.create(testInput, testRequests)((_, _)) { implicit builder => (source, sink) =>
        import GraphDSL.Implicits._

        val calculationParser = CalculationParsingFlow.createParsingFlow(dummyWorkers)

        source ~> calculationParser ~> sink
        ClosedShape
      }
    )
    val (source, sink) = testGraph.run()
    val (input1, input2, input3) = (sampleInput("file1"), sampleInput("file2"), sampleInput("file3"))
  }

  "a CalculationParsingFlow" when {
    "managing a single worker" should {

      "emit parsing results for every incoming parsing request in order of input" in {
        import FileParsingSignalMatchers._
        val f = new Fixture(1)
        f.source.sendNext(f.input1).sendNext(f.input2).sendNext(f.input3)
        f.findMatchingStreamElementsOrdered(Seq(
          have(relativePath("file1")),
          have(relativePath("file2")),
          have(relativePath("file3"))
        ))

      }

      "complete the stage if upstream signals completion before all elements are processed" in {
        val f = new Fixture(1)
        (1 to 3).foreach(_ => f.source.sendNext(aCandidateFound()))
        f.source.sendComplete()
        f.expectStreamCompletion()
      }

      "complete the stage if upstream signals completion after all elements are processed" in {
        val f = new Fixture(1)
        (1 to 3).foreach(_ => f.source.sendNext(aCandidateFound()))
        f.drainStream()
        f.source.sendComplete()
        f.expectStreamCompletion()
      }

    }

    "managing multiple workers" should {
      "distribute workload among all workers in a balanced way" in {
        val f = new Fixture(4)
        val dummy = aCandidateFound().build()
        f.sink.request(Int.MaxValue)
        (1 to 1000).foreach(_ => f.source.sendNext(dummy))
        f.drainStream()
        f.dummyWorkers.foreach { mock => verify(mock, Mockito.atLeast(200)).processSignal(any())
        }
      }
    }

  }
}
