package eu.nomad_lab.integrated_pipeline_tests

import eu.nomad_lab.integrated_pipeline._
import eu.nomad_lab.integrated_pipeline.messages.{ CalculationParserEvent, ResultWriterEvent, TreeParserEvent }
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ Matchers, fixture }

trait EventListenerBehaviour extends Matchers with MockitoSugar with TestDataBuilders { this: fixture.WordSpec =>

  abstract class EventListenerFixture(val listener: EventListener) {
    val treeParser = mock[TreeScanner]
    val calculationParser = mock[CalculationParsingEngine]
    val resultWriter = mock[ParsingResultsProcessingManager]
  }

  private val sampleTreeParserEvents: Seq[TreeParserEvent] = Seq(
    aTreeParserEventStart(),
    aTreeParserEventCandidate(),
    aTreeParserEventEnd()
  )
  private val sampleCalculationParserEvents: Seq[CalculationParserEvent] = Seq(
    aCalculationParserEventStart(),
    aCalculationParserEventEnd()
  )
  private val sampleResultWriterEvents: Seq[ResultWriterEvent] = Seq(
    aResultWriterEventStart(),
    aResultWriterEventResult(),
    aResultWriterEventEnd()
  )

  def eventListener(fixture: () => EventListenerFixture) {

    "accept (multiple) registrations for each EventReporterType" in { () =>
      val f = fixture()
      (1 to 3).foreach(_ => f.listener.registerReporter(mock[TreeScanner], None))
      (1 to 3).foreach(_ => f.listener.registerReporter(mock[CalculationParsingEngine], None))
      (1 to 3).foreach(_ => f.listener.registerReporter(mock[ParsingResultsProcessingManager], None))
      succeed
    }

    "throw an exception if the same event reporter is registered twice" in { () =>
      val f = fixture()
      val treeParser = mock[TreeScanner]
      val calculationParser = mock[CalculationParsingEngine]
      val resultWriter = mock[ParsingResultsProcessingManager]
      f.listener.registerReporter(treeParser, None)
      a[IllegalArgumentException] should be thrownBy f.listener.registerReporter(treeParser, None)
      f.listener.registerReporter(calculationParser, None)
      a[IllegalArgumentException] should be thrownBy f.listener.registerReporter(calculationParser, None)
      f.listener.registerReporter(resultWriter, None)
      a[IllegalArgumentException] should be thrownBy f.listener.registerReporter(resultWriter, None)
    }

  }

  def eventListenerWithRegisteredTreeParser(fixture: () => EventListenerFixture) {

    "process incoming events from the registered file tree parser normally" in { () =>
      val f = fixture()
      val id = TreeScannerId(f.treeParser.toString, f.treeParser)
      sampleTreeParserEvents.foreach { message => f.listener.processEvent(id, message) }
      succeed
    }

  }

  def eventListenerWithRegisteredCalculationParser(fixture: () => EventListenerFixture) {

    "process incoming events from the registered calculation parser normally" in { () =>
      val f = fixture()
      val id = CalculationParserId(f.calculationParser.toString, f.calculationParser)
      sampleCalculationParserEvents.foreach { message => f.listener.processEvent(id, message) }
      succeed
    }

  }
  def eventListenerWithRegisteredResultWriter(fixture: () => EventListenerFixture) {

    "process incoming events from the registered result writer normally" in { () =>
      val f = fixture()
      val id = ResultWriterId(f.resultWriter.toString, f.resultWriter)
      sampleResultWriterEvents.foreach { message => f.listener.processEvent(id, message) }
      succeed
    }

  }

}
