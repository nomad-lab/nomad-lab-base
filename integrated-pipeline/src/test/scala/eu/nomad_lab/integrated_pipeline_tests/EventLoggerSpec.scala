package eu.nomad_lab.integrated_pipeline_tests

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline._
import eu.nomad_lab.integrated_pipeline_tests.helpers.{ CustomMatchers, LoggingTestWrapper }
import eu.nomad_lab.parsers.ParseResult
import org.scalatest.mockito.MockitoSugar

class EventLoggerSpec extends LoggingTestWrapper with MockitoSugar with EventListenerBehaviour
    with CustomMatchers with TestDataBuilders {

  class EventLoggerFixture(val logger: EventLogger) extends EventListenerFixture(logger) {
    var treeParserId: Option[TreeScannerId] = None
    var calculationParserId: Option[CalculationParserId] = None
    var resultWriterId: Option[ResultWriterId] = None
  }

  def noRegisteredReporters(): EventLoggerFixture = {
    new EventLoggerFixture(new EventLogger {})
  }

  def registeredTreeParser(): EventLoggerFixture = {
    val f = noRegisteredReporters()
    f.treeParserId = Some(f.logger.registerReporter(f.treeParser, None))
    f
  }

  def registeredCalculationParser(): EventLoggerFixture = {
    val f = noRegisteredReporters()
    f.calculationParserId = Some(f.logger.registerReporter(f.calculationParser, None))
    f
  }

  def registeredResultWriter(): EventLoggerFixture = {
    val f = noRegisteredReporters()
    f.resultWriterId = Some(f.logger.registerReporter(f.resultWriter, None))
    f
  }

  "An EventLogger" when {
    "having received no registrations from reporters" should {
      def createFixture = () => noRegisteredReporters()

      behave like eventListener(createFixture)
    }

    "having received a registration from a TreeParser" should {
      def createFixture = () => registeredTreeParser()

      behave like eventListener(createFixture)
      behave like eventListenerWithRegisteredTreeParser(createFixture)

      "log file tree started events as info messages" in { log =>
        val f = createFixture()
        val message = aTreeParserEventStart().withBasePath("/foo/bar").withTreeType(TreeType.Zip)
        f.logger.processEvent(f.treeParserId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "start", "Zip"))
      }

      "log file tree candidate events as debug messages" in { log =>
        val f = createFixture()
        val message = aTreeParserEventCandidate().withBasePath("/foo/bar").
          withRelativePath("magic/file.out").withParser("magicTestParser")
        f.logger.processEvent(f.treeParserId.get, message)
        exactly(1, log.messages()) should (startWith("DEBUG") and
          includeAll("/foo/bar", "magic/file.out", "magicTestParser", "found candidate"))
      }

      "log file scan error events as warning messages" in { log =>
        val f = createFixture()
        val message = aTreeParserEventScanError().withBasePath("/foo/bar").
          withRelativePath("magic/file.out").withError(new IllegalArgumentException("test crash"))
        f.logger.processEvent(f.treeParserId.get, message)
        exactly(1, log.messages()) should (startWith("WARN") and
          includeAll("/foo/bar", "magic/file.out", "test crash", "scanning failure"))
      }

      "log file tree finished events as info messages" in { log =>
        val f = createFixture()
        val message = aTreeParserEventEnd().withBasePath("/foo/bar").
          withTreeType(TreeType.Directory).withNumCandidates(42)
        f.logger.processEvent(f.treeParserId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "Directory", "42 candidate files", "finished"))
      }

      "log file tree failure events as error messages" in { log =>
        val f = createFixture()
        val message = aTreeParserEventTreeFailure().withBasePath("/foo/bar").
          withError(new IllegalArgumentException("test crash"))
        f.logger.processEvent(f.treeParserId.get, message)
        exactly(1, log.messages()) should (startWith("ERROR") and
          includeAll("/foo/bar", "encountered failure", "aborting", "test crash"))
      }
    }

    "having received a registration from a CalculationParsingEngine" should {
      def createFixture = () => registeredCalculationParser()

      behave like eventListener(createFixture)
      behave like eventListenerWithRegisteredCalculationParser(createFixture)

      "log calculation parsing started events as info messages" in { log =>
        val f = createFixture()
        val message = aCalculationParserEventStart().withBasePath("/foo/bar").
          withRelativePath("magic/file.out").withParser("magicTestParser")
        f.logger.processEvent(f.calculationParserId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "magic/file.out", "start parsing", "magicTestParser"))
      }

      "log successful calculation parsing end events as info messages" in { log =>
        val f = createFixture()
        val message = aCalculationParserEventEnd().withBasePath("/foo/bar").
          withRelativePath("magic/file.out").withParser("magicTestParser").
          withStatus(ParseResult.ParseSuccess)
        f.logger.processEvent(f.calculationParserId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "magic/file.out", "finished parsing", "magicTestParser",
            "ParsingSuccess"))
      }

      "log failed calculation parsing end events as warning messages" in { log =>
        val f = createFixture()
        val message = aCalculationParserEventEnd().withBasePath("/foo/bar").
          withRelativePath("magic/file.out").withParser("magicTestParser").
          withStatus(ParseResult.ParseFailure).withErrorMessage(Some("test crash"))
        f.logger.processEvent(f.calculationParserId.get, message)
        exactly(1, log.messages()) should (startWith("WARN") and
          includeAll("/foo/bar", "magic/file.out", "failed parsing", "magicTestParser",
            "test crash", "ParsingFailure"))
      }

      "log skipped calculation parsing end events as info messages" in { log =>
        val f = createFixture()
        val message = aCalculationParserEventEnd().withBasePath("/foo/bar").
          withRelativePath("magic/file.out").withParser("magicTestParser").
          withStatus(ParseResult.ParseSkipped)
        f.logger.processEvent(f.calculationParserId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "magic/file.out", "skipped parsing", "magicTestParser",
            "ParsingSkipped"))
      }
    }

    "having received a registration from a ResultsWriter" should {
      def createFixture = () => registeredResultWriter()

      behave like eventListener(createFixture)
      behave like eventListenerWithRegisteredResultWriter(createFixture)

      "log result writing start events as info messages" in { log =>
        val f = createFixture()
        val message = aResultWriterEventStart().withBasePath("/foo/bar")
        f.logger.processEvent(f.resultWriterId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "start", "writing results"))
      }

      "log result writing events as debug messages" in { log =>
        val f = createFixture()
        val message = aResultWriterEventResult().withBasePath("/foo/bar").
          withRelativePath("magic/file.out")
        f.logger.processEvent(f.resultWriterId.get, message)
        exactly(1, log.messages()) should (startWith("DEBUG") and
          includeAll("/foo/bar", "magic/file.out", "processes"))
      }

      "log result writing end events as info messages" in { log =>
        val f = createFixture()
        val message = aResultWriterEventEnd().withBasePath("/foo/bar").withNumCalculations(42).
          withNumParsingFailures(13).withOutputPath("/imaginary/output")
        f.logger.processEvent(f.resultWriterId.get, message)
        exactly(1, log.messages()) should (startWith("INFO") and
          includeAll("/foo/bar", "finished writing", "/imaginary/output", "42 calculations",
            "13 parsing failures"))
      }

    }
  }

}
