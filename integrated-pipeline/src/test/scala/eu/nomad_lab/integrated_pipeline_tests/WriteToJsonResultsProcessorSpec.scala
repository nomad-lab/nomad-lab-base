package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.integrated_pipeline.io_integrations.WriteToJsonResultsProcessor
import eu.nomad_lab.integrated_pipeline.messages.CandidateFound
import eu.nomad_lab.meta.KnownMetaInfoEnvs
import eu.nomad_lab.{ JsonUtils, TreeType }
import org.scalatest.{ Matchers, WordSpec }

import scala.io.Source

class WriteToJsonResultsProcessorSpec extends WordSpec with Matchers with TestDataBuilders {

  val sampleTree = aFileTree().withBasePath("/foo/bargus").withTreeType(TreeType.Directory)
  val metaData = KnownMetaInfoEnvs.all

  "A WriteToJsonResultsProcessor" when {
    "processing successful parsing results" should {
      "write a valid NOMAD metaData JSON for each result" in {
        val tempDir = generateTempTestDirectory("json-results-processor")
        tempDir.toFile.deleteOnExit()
        val writer = new WriteToJsonResultsProcessor(tempDir, metaData)
        val inputs = (1 to 3).map(x => CandidateFound(sampleTree, Paths.get(s"file$x"), "dummyParser"))
        inputs.foreach(x => writer.processFileParsingResult(createSuccessfulFileParsingResult(x)))
        val targetFolder = writer.outputLocation(sampleTree)
        inputs.foreach { entry =>
          val calcName = entry.calculationGid
          val filePath = targetFolder.resolve(s"$calcName.json")
          assert(filePath.toFile.exists(), s"calculation output json '$filePath' does not exist")
          val jsonData = JsonUtils.parseReader(Source.fromFile(filePath.toFile).bufferedReader())
          validateJsonMetaData(jsonData \ "sections", metaData)
          assert(
            (jsonData \ "parserInfo" \ "name").extract[String] == "dummyParser",
            "unexpected parser name in output file"
          )
        }
      }
    }

    "determining the output location" should {
      "return the appropriate output location for a directory file tree" in {
        val f = new WriteToJsonResultsProcessor(Paths.get("/non/existing/location"), metaData)
        val fileTree = aFileTree().withTreeType(TreeType.Directory).withBasePath("foo/bargus")
        f.outputLocation(fileTree) should be(Paths.get("/non/existing/location/bar/bargus"))
      }

      "return the appropriate output location for a zip archive file tree" in {
        val f = new WriteToJsonResultsProcessor(Paths.get("/non/existing/location"), metaData)
        val fileTree = aFileTree().withTreeType(TreeType.Zip).withBasePath(s"foo/R${"x" * 28}.zip")
        f.outputLocation(fileTree) should be(Paths.get(s"/non/existing/location/Rxx/R${"x" * 28}"))
      }
    }
  }

}
