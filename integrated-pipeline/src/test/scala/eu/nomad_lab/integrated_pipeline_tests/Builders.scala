package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.TreeType
import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.integrated_pipeline.FileTree
import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.integrated_pipeline_tests.EventBuilders._
import eu.nomad_lab.integrated_pipeline_tests.MessageBuilders._
import eu.nomad_lab.parsers.ParseResult.ParseResult
import eu.nomad_lab.parsers._

trait TestDataBuilders {
  import scala.language.implicitConversions

  def aFileTree() = BuilderFileTree()

  def aCandidateFound() = BuilderCandidateFound()
  def aTreeScanCompleted() = BuilderTreeScanCompleted()
  def aParsingResultInMemory() = BuilderParsingResultInMemory()

  def aTreeParserEventStart() = BuilderTreeParserEventStart()
  def aTreeParserEventCandidate() = BuilderTreeParserEventCandidate()
  def aTreeParserEventEnd() = BuilderTreeParserEventEnd()
  def aTreeParserEventTreeFailure() = BuilderTreeParserEventTreeFailure()
  def aTreeParserEventScanError() = BuilderTreeParserEventScanError()

  def aCalculationParserEventStart() = BuilderCalculationParserEventStart()
  def aCalculationParserEventEnd() = BuilderCalculationParserEventEnd()

  def aResultWriterEventStart() = BuilderResultWriterEventStart()
  def aResultWriterEventResult() = BuilderResultWriterEventResult()
  def aResultWriterEventEnd() = BuilderResultWriterEventEnd()

  implicit def build(x: BuilderFileTree): FileTree = x.build()

  implicit def build(x: BuilderCandidateFound): CandidateFound = x.build()
  implicit def build(x: BuilderTreeScanCompleted): TreeScanCompleted = x.build()
  implicit def build(x: BuilderParsingResultInMemory): ParsingResultInMemory = x.build()

  implicit def build(x: BuilderTreeParserEventStart): TreeParserEventStart = x.build()
  implicit def build(x: BuilderTreeParserEventCandidate): TreeParserEventCandidate = x.build()
  implicit def build(x: BuilderTreeParserEventEnd): TreeParserEventEnd = x.build()
  implicit def build(x: BuilderTreeParserEventTreeFailure): TreeParserEventTreeFailure = x.build()

  implicit def build(x: BuilderCalculationParserEventStart): CalculationParserEventStart = x.build()
  implicit def build(x: BuilderCalculationParserEventEnd): CalculationParserEventEnd = x.build()

  implicit def build(x: BuilderResultWriterEventStart): ResultWriterEventStart = x.build()
  implicit def build(x: BuilderResultWriterEventResult): ResultWriterEventResult = x.build()
  implicit def build(x: BuilderResultWriterEventEnd): ResultWriterEventEnd = x.build()

  implicit def build(x: BuilderTreeParserEventScanError): TreeParserEventScanError = x.build()
}

private object Defaults {
  val defaultPath = Paths.get("")
  val defaultParser = "noParser"
  val defaultTreeType = TreeType.Directory
  val defaultParseResult = ParseResult.ParseSkipped
  val defaultError = new NotImplementedError("placeholder for real errors")
}

object MessageBuilders {
  import Defaults._

  case class BuilderFileTree(
      treeBasePath: Path = defaultPath,
      treeType: TreeType = defaultTreeType
  ) {

    def withBasePath(path: Path) = copy(treeBasePath = path)
    def withBasePath(path: String) = copy(treeBasePath = Paths.get(path))
    def withTreeType(newType: TreeType) = copy(treeType = newType)

    def build() = FileTree(
      treeBasePath = treeBasePath,
      treeType = treeType
    )
  }

  case class BuilderCandidateFound(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val relativePath: Path = defaultPath,
      private val parserName: String = defaultParser,
      private val extractedPath: Option[Path] = None
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withRelativePath(path: Path) = copy(relativePath = path)
    def withRelativePath(path: String) = copy(relativePath = Paths.get(path))
    def withParserName(name: String) = copy(parserName = name)
    def withExtractedPath(newPath: Option[Path]) = copy(extractedPath = newPath)

    def build() = CandidateFound(
      fileTree = fileTree,
      relativePath = relativePath,
      parserName = parserName,
      extractedPath = extractedPath
    )
  }

  case class BuilderTreeScanCompleted(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val numTasks: Long = -1
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderTreeScanCompleted = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withTaskCount(count: Long) = copy(numTasks = count)

    def build() = TreeScanCompleted(
      fileTree = fileTree,
      numParsingTasks = numTasks
    )
  }

  case class BuilderParsingResultInMemory(
      private val task: CandidateFound = BuilderCandidateFound().build(),
      private val result: ParseResult = defaultParseResult,
      private val start: Option[StartedParsingSession] = None,
      private val events: Seq[ParseEvent] = Seq(),
      private val end: Option[FinishedParsingSession] = None,
      private val error: Option[String] = None
  ) {

    def withTask(newTask: CandidateFound) = copy(task = newTask)
    def withParseResult(newResult: ParseResult) = copy(result = newResult)
    def withStartEvent(event: Option[StartedParsingSession]) = copy(start = event)
    def withEvents(newEvents: Seq[ParseEvent]) = copy(events = newEvents)
    def withFinishEvent(event: Option[FinishedParsingSession]) = copy(end = event)
    def withFileTree(tree: FileTree) = copy(task = task.copy(fileTree = tree))
    def withRelativePath(newPath: Path) = copy(task = task.copy(relativePath = newPath))
    def withRelativePath(newPath: String) = copy(task = task.copy(relativePath = Paths.get(newPath)))
    def withErrorMessage(message: Option[String]) = copy(error = message)

    def build() = ParsingResultInMemory(
      task = task,
      result = result,
      start = start,
      events = events,
      end = end,
      error = error
    )
  }

}

object EventBuilders {
  import Defaults._

  case class BuilderTreeParserEventStart(
      private val fileTree: FileTree = BuilderFileTree().build()
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderTreeParserEventStart = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))

    def build() = TreeParserEventStart(
      fileTree = fileTree
    )
  }

  case class BuilderTreeParserEventCandidate(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val relativePath: Path = defaultPath,
      private val parser: String = defaultParser
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderTreeParserEventCandidate = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withRelativePath(path: Path) = copy(relativePath = path)
    def withRelativePath(path: String): BuilderTreeParserEventCandidate = withRelativePath(Paths.get(path))
    def withParser(parserName: String) = copy(parser = parserName)

    def build() = TreeParserEventCandidate(
      fileTree = fileTree,
      relativePath = relativePath,
      parser = parser
    )
  }

  case class BuilderTreeParserEventScanError(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val relativePath: Path = defaultPath,
      private val error: Throwable = defaultError
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderTreeParserEventScanError = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withRelativePath(path: Path) = copy(relativePath = path)
    def withRelativePath(path: String): BuilderTreeParserEventScanError = withRelativePath(Paths.get(path))
    def withError(newError: Throwable) = copy(error = newError)

    def build() = TreeParserEventScanError(
      fileTree = fileTree,
      relativePath = relativePath,
      error = error
    )
  }

  case class BuilderTreeParserEventEnd(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val numCandidates: Long = -1
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderTreeParserEventEnd = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withNumCandidates(count: Long) = copy(numCandidates = count)

    def build() = TreeParserEventEnd(
      fileTree = fileTree,
      numCandidates = numCandidates
    )
  }

  case class BuilderTreeParserEventTreeFailure(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val error: Throwable = defaultError
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderTreeParserEventTreeFailure = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withError(newError: Throwable) = copy(error = newError)

    def build() = TreeParserEventTreeFailure(
      fileTree = fileTree,
      error = error
    )
  }

  case class BuilderCalculationParserEventStart(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val relativePath: Path = defaultPath,
      private val parser: String = defaultParser
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderCalculationParserEventStart = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withRelativePath(path: Path) = copy(relativePath = path)
    def withRelativePath(path: String): BuilderCalculationParserEventStart = withRelativePath(Paths.get(path))
    def withParser(parserName: String) = copy(parser = parserName)

    def build() = CalculationParserEventStart(
      fileTree = fileTree,
      relativePath = relativePath,
      parser = parser
    )
  }

  case class BuilderCalculationParserEventEnd(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val relativePath: Path = defaultPath,
      private val parser: String = defaultParser,
      private val result: ParseResult = defaultParseResult,
      private val error: Option[String] = None
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderCalculationParserEventEnd = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withRelativePath(path: Path) = copy(relativePath = path)
    def withRelativePath(path: String): BuilderCalculationParserEventEnd = withRelativePath(Paths.get(path))
    def withParser(parserName: String) = copy(parser = parserName)
    def withStatus(newStatus: ParseResult) = copy(result = newStatus)
    def withErrorMessage(newError: Option[String]) = copy(error = newError)

    def build() = CalculationParserEventEnd(
      fileTree = fileTree,
      relativePath = relativePath,
      parser = parser,
      result = result,
      error = error
    )
  }

  case class BuilderResultWriterEventStart(
      private val fileTree: FileTree = BuilderFileTree().build()
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderResultWriterEventStart = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))

    def build() = ResultWriterEventStart(
      fileTree = fileTree
    )
  }

  case class BuilderResultWriterEventResult(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val relativePath: Path = defaultPath,
      private val parser: String = defaultParser,
      private val result: ParseResult = defaultParseResult,
      private val error: Option[String] = None
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderResultWriterEventResult = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withRelativePath(path: Path) = copy(relativePath = path)
    def withRelativePath(path: String): BuilderResultWriterEventResult = withRelativePath(Paths.get(path))
    def withParser(parserName: String) = copy(parser = parserName)
    def withStatus(newStatus: ParseResult) = copy(result = newStatus)
    def withErrorMessage(newError: Option[String]) = copy(error = newError)

    def build() = ResultWriterEventResult(
      fileTree = fileTree,
      relativePath = relativePath,
      parser = parser,
      result = result,
      error = error
    )
  }

  case class BuilderResultWriterEventEnd(
      private val fileTree: FileTree = BuilderFileTree().build(),
      private val numCalculations: Long = -1,
      private val numParsingFailures: Long = -1,
      private val outputLocation: Path = defaultPath
  ) {

    def withFileTree(tree: FileTree) = copy(fileTree = tree)
    def withBasePath(path: Path) = copy(fileTree = fileTree.copy(treeBasePath = path))
    def withBasePath(path: String): BuilderResultWriterEventEnd = withBasePath(Paths.get(path))
    def withTreeType(newType: TreeType) = copy(fileTree = fileTree.copy(treeType = newType))
    def withNumCalculations(count: Long) = copy(numCalculations = count)
    def withNumParsingFailures(count: Long) = copy(numParsingFailures = count)
    def withOutputPath(path: Path) = copy(outputLocation = path)
    def withOutputPath(path: String): BuilderResultWriterEventEnd = withOutputPath(Paths.get(path))

    def build() = ResultWriterEventEnd(
      fileTree = fileTree,
      numCalculations = numCalculations,
      numParsingFailures = numParsingFailures,
      outputLocation = outputLocation
    )
  }

}
