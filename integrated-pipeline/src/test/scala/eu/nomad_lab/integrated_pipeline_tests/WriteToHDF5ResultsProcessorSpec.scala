package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.TreeType
import eu.nomad_lab.h5.CalculationH5
import eu.nomad_lab.integrated_pipeline.io_integrations.WriteToHDF5ResultsProcessor
import eu.nomad_lab.integrated_pipeline.messages.CandidateFound
import eu.nomad_lab.meta.KnownMetaInfoEnvs
import org.scalatest.{ Matchers, WordSpec }

class WriteToHDF5ResultsProcessorSpec extends WordSpec with TestDataBuilders with Matchers {

  val sampleTree = aFileTree().withBasePath("/foo/bargus").withTreeType(TreeType.Directory)
  val metaData = KnownMetaInfoEnvs.all

  def validateHDFContent(expectedMainFileUri: String)(calc: CalculationH5, mainFileUri: String): Unit = {
    assert(expectedMainFileUri == mainFileUri, "main file URI does not match expectation")
    val programNames = calc.valueTable(Seq("section_run", "program_name")).map(_.stringValue)
    assert(programNames.size == 1, s"expected exactly one program name")
    assert(programNames.head == "magic_code", "expected program name not found")
  }

  "A WriteToHDF5ResultsProcessor" when {
    "processing successful parsing results" should {
      "write a valid NOMAD metaData HDF5 archive for each result" in {
        val tempDir = generateTempTestDirectory("hdf5-results-processor")
        tempDir.toFile.deleteOnExit()
        val writer = new WriteToHDF5ResultsProcessor(tempDir, metaData)
        val inputs = (1 to 3).map(x => CandidateFound(sampleTree, Paths.get(s"file$x"), "dummyParser"))
        inputs.foreach(x => writer.processFileParsingResult(createSuccessfulFileParsingResult(x)))
        val targetFolder = writer.outputLocation(sampleTree)
        val archiveId = sampleTree.archiveId
        inputs.foreach { entry =>
          val calcName = entry.calculationGid
          val filePath = targetFolder.resolve(s"$calcName.h5")
          assert(filePath.toFile.exists(), s"calculation output HDF5 '$filePath' does not exist")
          val mainFileUri = entry.fileTree.treeBasePath.resolve(entry.relativePath).toUri.toString
          validateHDF5(targetFolder, archiveId, metaData, validateHDFContent(mainFileUri),
            Some(s"$calcName.h5"))
        }
      }
    }

    "determining the output location" should {
      "return the appropriate output location for a directory file tree" in {
        val f = new WriteToHDF5ResultsProcessor(Paths.get("/non/existing/location"), metaData)
        val fileTree = aFileTree().withTreeType(TreeType.Directory).withBasePath("foo/bargus")
        f.outputLocation(fileTree) should be(Paths.get("/non/existing/location/bar/bargus"))
      }

      "return the appropriate output location for a zip archive file tree" in {
        val f = new WriteToHDF5ResultsProcessor(Paths.get("/non/existing/location"), metaData)
        val fileTree = aFileTree().withTreeType(TreeType.Zip).withBasePath(s"foo/R${"x" * 28}.zip")
        f.outputLocation(fileTree) should be(Paths.get(s"/non/existing/location/Rxx/R${"x" * 28}"))
      }
    }
  }

}
