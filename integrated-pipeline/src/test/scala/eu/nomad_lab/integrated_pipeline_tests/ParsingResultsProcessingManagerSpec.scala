package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.integrated_pipeline.{ EventListener, OutputType, ParsingResultsProcessingManager, ParsingResultsProcessor }
import eu.nomad_lab.integrated_pipeline_tests.matchers._
import eu.nomad_lab.parsers.ParseResult
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito._
import org.mockito.{ ArgumentCaptor, Mockito }
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ Matchers, WordSpec }

import scala.collection.JavaConverters._

class ParsingResultsProcessingManagerSpec extends WordSpec with MockitoSugar with TestDataBuilders
    with Matchers with MessageProcessorBehaviour[FileParsingSignal, FileTreeParsingResult] {

  class Fixture {

    val processingMock = mock[ParsingResultsProcessor]
    val eventMock = mock[EventListener]
    when(processingMock.outputType).thenReturn(OutputType.Json)
    when(processingMock.outputLocation(any())).thenReturn(Paths.get(s"/imaginary/foo/Rxx/R${"x" * 28}"))
    val orderingTest = Mockito.inOrder(processingMock)
    val manager = new ParsingResultsProcessingManager {
      override protected[this] val processor: ParsingResultsProcessor = processingMock
      override protected[this] val eventListener: EventListener = eventMock
    }
  }

  def noResultsNoEndReceived(): Fixture = {
    new Fixture
  }

  def twoResultsNoEndReceived(): Fixture = {
    val f = new Fixture
    f.manager.processSignal(aParsingResultInMemory().withParseResult(ParseResult.ParseFailure))
    f.manager.processSignal(aParsingResultInMemory())
    f
  }

  def oneResultEndWithTwoEntriesReceived(): Fixture = {
    val f = new Fixture
    f.manager.processSignal(aParsingResultInMemory().withParseResult(ParseResult.ParseFailure))
    f.manager.processSignal(aTreeScanCompleted().withTaskCount(2))
    f
  }

  def twoResultsEndWithTwoEntriesReceived(): Fixture = {
    val f = new Fixture
    f.manager.processSignal(aParsingResultInMemory().withParseResult(ParseResult.ParseFailure))
    f.manager.processSignal(aParsingResultInMemory())
    f.manager.processSignal(aTreeScanCompleted().withTaskCount(2))
    f
  }

  def fullyProcessedATree(): Fixture = {
    val f = new Fixture
    f.manager.processSignal(aParsingResultInMemory().withParseResult(ParseResult.ParseFailure))
    f.manager.processSignal(aParsingResultInMemory())
    f.manager.processSignal(aTreeScanCompleted().withTaskCount(2))
    f.manager.getNextSignalToEmit()
    f
  }

  "a ParsingResultsProcessorFlow" when {

    "not having received any events" should {
      val createFixture = () => noResultsNoEndReceived()

      behave like processorWithNoSignalReady(
        () => createFixture().manager,
        Seq(aParsingResultInMemory(), aTreeScanCompleted())
      )

      behave like readyToAcceptParsingResults(createFixture)
      behave like readyToAcceptParsingResultsFromAnotherTree(createFixture)
      behave like interleaveAnotherFileTree(createFixture)
      behave like startProcessingANewTree(createFixture)

      "register itself with the EventListener upon processing the first message (parsing result)" in {
        val f = createFixture()
        f.manager.processSignal(aParsingResultInMemory())
        verify(f.eventMock).registerReporter(f.manager, None)
      }

      "register itself with the EventListener upon processing the first message (tree end signal)" in {
        val f = createFixture()
        f.manager.processSignal(aParsingResultInMemory())
        verify(f.eventMock).registerReporter(f.manager, None)
      }
    }

    "having received all parsing tasks but no end signal" should {
      val createFixture = () => twoResultsNoEndReceived()

      behave like processorWithNoSignalReady(
        () => createFixture().manager,
        Seq(aParsingResultInMemory(), aTreeScanCompleted())
      )

      behave like readyToAcceptParsingResultsFromAnotherTree(createFixture)
      behave like interleaveAnotherFileTree(createFixture)
      behave like startProcessingANewTree(createFixture)
      behave like notRegisteringAgain(createFixture)

      "finish processing a file tree when the associated end signal arrives" in {
        val f = createFixture()
        f.manager.hasSignalToEmit() should be(false)
        f.manager.processSignal(aTreeScanCompleted().withTaskCount(2))
        verify(f.processingMock).finishProcessingTreeResults(any())
        f.manager.hasSignalToEmit() should be(true)
      }

    }

    "having received the end signal and all but one parsing results" should {
      val createFixture = () => oneResultEndWithTwoEntriesReceived()

      behave like processorWithNoSignalReady(
        () => createFixture().manager,
        Seq(aParsingResultInMemory(), aTreeScanCompleted())
      )

      behave like readyToAcceptParsingResults(createFixture)
      behave like readyToAcceptParsingResultsFromAnotherTree(createFixture)
      behave like interleaveAnotherFileTree(createFixture)
      behave like startProcessingANewTree(createFixture)
      behave like notRegisteringAgain(createFixture)

      "finish processing a file tree when the last associated parsing result arrives" in {
        val f = createFixture()
        f.manager.hasSignalToEmit() should be(false)
        f.manager.processSignal(aParsingResultInMemory())
        verify(f.processingMock).finishProcessingTreeResults(any())
        f.manager.hasSignalToEmit() should be(true)
      }

    }

    "having received the end signal and all parsing results" should {
      val createFixture = () => twoResultsEndWithTwoEntriesReceived()

      behave like processorWithOutboundSignalReady(
        () => createFixture().manager,
        Seq(aParsingResultInMemory(), aTreeScanCompleted()),
        have(
          FileTreeParsingResultMatchers.numParsingTasks(2),
          FileTreeParsingResultMatchers.fileTree(aFileTree())
        )
      )

      "have called the file tree pre- and postprocessing operations in the correct order" in {
        val f = createFixture()
        f.orderingTest.verify(f.processingMock).startProcessingTreeResults(aFileTree())
        f.orderingTest.verify(f.processingMock).finishProcessingTreeResults(aFileTree())
      }

      "emit an end signal with the correct output location and type" in {
        import FileTreeParsingResultMatchers._
        val f = createFixture()
        val result = f.manager.getNextSignalToEmit()
        result should have(outputPath(s"/imaginary/foo/Rxx/R${"x" * 28}"), outputType(OutputType.Json))
      }

      "emit an end signal with the correct number of total parsed and failed calculations" in {
        import FileTreeParsingResultMatchers._
        val f = createFixture()
        val result = f.manager.getNextSignalToEmit()
        result should have(numParsingTasks(2), numFailedTasks(1))
      }

      "have send an end event to the EventListener with the correct output location" in {
        import MatcherResultWriterEventEnd._
        val f = createFixture()
        val args: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
        verify(f.eventMock, atLeastOnce()).processEvent(any(), args.capture())
        val events = args.getAllValues.asScala.collect { case x: ResultWriterEventEnd => x }
        exactly(1, events) should have(
          basePath(aFileTree().build().treeBasePath),
          outputPath(s"/imaginary/foo/Rxx/R${"x" * 28}")
        )
      }

      "have send an end event to the EventListener with the correct parsing count stats" in {
        import MatcherResultWriterEventEnd._
        val f = createFixture()
        val args: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
        verify(f.eventMock, atLeastOnce()).processEvent(any(), args.capture())
        val events = args.getAllValues.asScala.collect { case x: ResultWriterEventEnd => x }
        exactly(1, events) should have(numCalculations(2), numFailures(1))
      }

    }

    "having fully processed a file tree and emitted the result" should {
      val createFixture = () => fullyProcessedATree()

      behave like processorWithNoSignalReady(
        () => createFixture().manager,
        Seq(aParsingResultInMemory(), aTreeScanCompleted())
      )

      behave like readyToAcceptParsingResults(createFixture)
      behave like readyToAcceptParsingResultsFromAnotherTree(createFixture)
      behave like interleaveAnotherFileTree(createFixture)
      behave like startProcessingANewTree(createFixture)
      behave like notRegisteringAgain(createFixture)

      "have called the file tree pre- and postprocessing operations exactly once" in {
        val f = createFixture()
        verify(f.processingMock, times(1)).startProcessingTreeResults(aFileTree())
        verify(f.processingMock, times(1)).finishProcessingTreeResults(aFileTree())
      }

      "have send a single start event to the EventListener" in {
        import MatcherResultWriterEventStart._
        val f = createFixture()
        val args: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
        verify(f.eventMock, atLeastOnce()).processEvent(any(), args.capture())
        val events = args.getAllValues.asScala.collect { case x: ResultWriterEventStart => x }
        exactly(1, events) should have(basePath(aFileTree().build().treeBasePath))
      }

      "have send a single end event to the EventListener" in {
        import MatcherResultWriterEventEnd._
        val f = createFixture()
        val args: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
        verify(f.eventMock, atLeastOnce()).processEvent(any(), args.capture())
        val events = args.getAllValues.asScala.collect { case x: ResultWriterEventEnd => x }
        exactly(1, events) should have(basePath(aFileTree().build().treeBasePath))
      }

      "be able to process the same file tree again after finishing it once" in {
        val f = createFixture()
        f.manager.processSignal(aParsingResultInMemory())
        f.manager.processSignal(aParsingResultInMemory())
        f.manager.processSignal(aTreeScanCompleted().withTaskCount(2))
        verify(f.processingMock, times(2)).finishProcessingTreeResults(aFileTree())
      }
    }
  }

  def readyToAcceptParsingResults(createFixture: () => Fixture): Unit = {
    "process a file parsing result" in {
      import FileParsingSignalMatchers._
      val f = createFixture()
      f.manager.processSignal(aParsingResultInMemory().withRelativePath("foo"))
      val argument: ArgumentCaptor[FileParsingResult] = ArgumentCaptor.forClass(classOf[FileParsingResult])
      verify(f.processingMock, atLeastOnce()).processFileParsingResult(argument.capture())
      val results = argument.getAllValues.asScala
      exactly(1, results) should have(relativePath("foo"))
    }

    "notify the EventListener when a parsing result arrives" in {
      import MatcherResultWriterEventResult._
      val f = createFixture()
      f.manager.processSignal(aParsingResultInMemory().withRelativePath("foo"))
      val argument: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
      verify(f.eventMock, atLeastOnce()).processEvent(any(), argument.capture())
      val results = argument.getAllValues.asScala.collect { case x: ResultWriterEventResult => x }
      exactly(1, results) should have(relativePath("foo"))
    }
  }

  def readyToAcceptParsingResultsFromAnotherTree(createFixture: () => Fixture): Unit = {
    "process a file parsing result from another file tree" in {
      import FileParsingSignalMatchers._
      val f = createFixture()
      val myFileTree = aFileTree().withBasePath("/universe/magic")
      f.manager.processSignal(aParsingResultInMemory().withFileTree(myFileTree).withRelativePath("bar"))
      val argument: ArgumentCaptor[FileParsingResult] = ArgumentCaptor.forClass(classOf[FileParsingResult])
      verify(f.processingMock, atLeastOnce()).processFileParsingResult(argument.capture())
      val results = argument.getAllValues.asScala
      exactly(1, results) should have(fileTree(myFileTree), relativePath("bar"))
    }
  }

  def interleaveAnotherFileTree(createFixture: () => Fixture): Unit = {
    "interleave requests from another file tree" in {
      import FileTreeParsingResultMatchers._
      val f = createFixture()
      val myFileTree = aFileTree().withBasePath("/universe/magic")
      f.manager.processSignal(aParsingResultInMemory().withFileTree(myFileTree))
      f.manager.processSignal(aTreeScanCompleted().withFileTree(myFileTree).withTaskCount(1))
      f.manager.hasSignalToEmit() should be(true)
      f.manager.getNextSignalToEmit() should have(fileTree(myFileTree), numParsingTasks(1))
    }
  }

  def startProcessingANewTree(createFixture: () => Fixture): Unit = {
    "start processing a file tree when the first signal associated with it arrives (result arrives first)" in {
      val f = createFixture()
      val fileTree = aFileTree().withBasePath("/tmp/foo/random")
      f.manager.processSignal(aParsingResultInMemory().withFileTree(fileTree))
      verify(f.processingMock, times(1)).startProcessingTreeResults(fileTree)
    }

    "start processing a file tree when the first signal associated with it arrives (end signal arrives first)" in {
      val f = createFixture()
      val fileTree = aFileTree().withBasePath("/tmp/foo/random")
      f.manager.processSignal(aTreeScanCompleted().withFileTree(fileTree))
      verify(f.processingMock, times(1)).startProcessingTreeResults(fileTree)
    }

    "send a start event to the EventListener when receiving a message from a new tree (result arrives first)" in {
      import MatcherResultWriterEventStart._
      val f = createFixture()
      val fileTree = aFileTree().withBasePath("/tmp/foo/random")
      f.manager.processSignal(aParsingResultInMemory().withFileTree(fileTree))
      val args: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
      verify(f.eventMock, atLeastOnce()).processEvent(any(), args.capture())
      val events = args.getAllValues.asScala.collect { case x: ResultWriterEventStart => x }
      exactly(1, events) should have(basePath("/tmp/foo/random"))
    }

    "send a start event to the EventListener when receiving a message from a new tree (end signal arrives first)" in {
      import MatcherResultWriterEventStart._
      val f = createFixture()
      val fileTree = aFileTree().withBasePath("/tmp/foo/random")
      f.manager.processSignal(aTreeScanCompleted().withFileTree(fileTree))
      val args: ArgumentCaptor[ResultWriterEvent] = ArgumentCaptor.forClass(classOf[ResultWriterEvent])
      verify(f.eventMock, atLeastOnce()).processEvent(any(), args.capture())
      val events = args.getAllValues.asScala.collect { case x: ResultWriterEventStart => x }
      exactly(1, events) should have(basePath("/tmp/foo/random"))
    }
  }

  def notRegisteringAgain(createFixture: () => Fixture): Unit = {
    "not register itself again with the EventListener after the first processed message" in {
      val f = createFixture()
      verify(f.eventMock, times(1)).registerReporter(f.manager, None)
    }
  }

}

