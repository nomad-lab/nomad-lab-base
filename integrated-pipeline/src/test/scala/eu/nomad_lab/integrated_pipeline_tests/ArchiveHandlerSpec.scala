package eu.nomad_lab.integrated_pipeline_tests

import java.nio.file.Paths

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.io_integrations.ArchiveHandler
import org.scalatest.WordSpec

class ArchiveHandlerSpec extends WordSpec {

  "An ArchiveHandler" when {
    "when handling zip-files" should {
      "should unpack the entire archive at the requested location" in {
        val tempDir = generateTempTestDirectory("zip-archive-handling-test")
        tempDir.toFile.deleteOnExit()
        val sample = testData2
        val task = createFileTreeScanRequest(sample, TreeType.Zip)
        val handler = new ArchiveHandler(tempDir)
        val location = handler.extractZipArchive(task.treeBasePath)
        sample.candidateCalculationsWithParsers.foreach { entry =>
          assert(
            location.resolve(Paths.get(entry._1)).toFile.exists(),
            s"expected file '${entry._1}' not extracted"
          )
        }
      }

      "should clean up a previously extracted file-tree upon request" in {
        val tempDir = generateTempTestDirectory("zip-archive-handling-test")
        tempDir.toFile.deleteOnExit()
        val sample = testData2
        val task = createFileTreeScanRequest(sample, TreeType.Zip)
        val handler = new ArchiveHandler(tempDir)
        val location = handler.extractZipArchive(task.treeBasePath)
        assert(location.toFile.exists(), "extraction folder does not exist")
        assert(location.toFile.listFiles().nonEmpty, "no files or folders extracted")
        handler.cleanUpExtractedArchive(task.treeBasePath)
        assert(!location.toFile.exists(), "extracted folders have not been deleted")
      }

      "should refuse to unpack the same archive again while it is already unpacked" in {
        val tempDir = generateTempTestDirectory("zip-archive-handling-test")
        tempDir.toFile.deleteOnExit()
        val sample = testData2
        val task = createFileTreeScanRequest(sample, TreeType.Zip)
        val handler = new ArchiveHandler(tempDir)
        handler.extractZipArchive(task.treeBasePath)
        assertThrows[IllegalArgumentException](handler.extractZipArchive(task.treeBasePath))
      }

      "should unpack the same archive again after the previous unpacked tree has been removed" in {
        val tempDir = generateTempTestDirectory("zip-archive-handling-test")
        tempDir.toFile.deleteOnExit()
        val sample = testData2
        val task = createFileTreeScanRequest(sample, TreeType.Zip)
        val handler = new ArchiveHandler(tempDir)
        handler.extractZipArchive(task.treeBasePath)
        handler.cleanUpExtractedArchive(task.treeBasePath)
        handler.extractZipArchive(task.treeBasePath)
        handler.cleanUpExtractedArchive(task.treeBasePath)
      }
    }
  }
}
