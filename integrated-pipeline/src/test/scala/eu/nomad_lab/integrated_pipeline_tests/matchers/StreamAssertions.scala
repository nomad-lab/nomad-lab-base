package eu.nomad_lab.integrated_pipeline_tests.matchers

import akka.stream.testkit.TestSubscriber
import akka.stream.testkit.TestSubscriber.{ OnComplete, OnError, OnNext, OnSubscribe }
import org.scalatest.matchers.{ MatchResult, Matcher }
import org.scalatest.{ Assertion, Assertions }

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.reflect.ClassTag

trait StreamAssertions[T] {

  private trait SearchResult
  private case object Found extends SearchResult
  private case class TimedOut(t: FiniteDuration) extends SearchResult
  private case object NotFound extends SearchResult
  private case class StreamError(e: Throwable) extends SearchResult

  val sink: TestSubscriber.Probe[T]
  val defaultTimeOut: FiniteDuration = 0.1.seconds

  private case class FailureReport(
    element: T,
    index: Long,
    reason: Either[String, MatchResult]
  )

  private class FailureRecords[U <: T](private val tests: Seq[Matcher[U]]) {
    private val failedTests = mutable.Map[Matcher[U], ListBuffer[FailureReport]]()
    tests.foreach { x => failedTests(x) = new ListBuffer() }

    def addResult(test: Matcher[U], element: T, index: Long, result: MatchResult): Unit = {
      failedTests.get(test).foreach(_.append(FailureReport(element, index, Right(result))))
    }

    def addResult(test: Matcher[U], element: T, index: Long, result: String): Unit = {
      failedTests.get(test).foreach(_.append(FailureReport(element, index, Left(result))))
    }

    def succeedTest(test: Matcher[U]): Unit = failedTests.remove(test)

    def printFailures(endOfTestReason: SearchResult): String = {
      val details = tests.filter(failedTests.contains).map { test =>
        val entry = failedTests(test)
        val title = s"unsatisfied match: ${test.toString()}"
        val failures = entry.toList.map {
          case FailureReport(elem, i, Right(result)) => s"  [$i] $elem : ${result.failureMessage}"
          case FailureReport(elem, i, Left(result)) => s"  [$i] $elem : $result"
        }
        failures.size match {
          case 0 => s"$title\n  <no stream elements received>"
          case _ => s"$title\n${failures.mkString("\n")}"
        }
      }
      val reason = endOfTestReason match {
        case TimedOut(t) => s"no new event received before time-out ($t)"
        case StreamError(e) => s"stream failed with an exception $e"
        case NotFound => "stream completed normally, but no match was found"
      }
      s"$reason\n${details.mkString("\n")}"
    }
  }

  /**
   * drain elements from the stream until it completes or times out. This function performs no
   * tests on the drained element and is intended to be used when testing side-effects of the
   * stream processing.
   * @param timeOut maximum wait time for each element
   * @return success if the stream completes or times out
   */
  def drainStream(timeOut: FiniteDuration = defaultTimeOut): Assertion = {
    sink.ensureSubscription().request(Int.MaxValue)
    def findMatch(): SearchResult = {
      try {
        sink.expectEventWithTimeoutPF(timeOut, {
          case OnNext(_) => findMatch()
          case OnComplete => NotFound
          case OnError(e) => StreamError(e)
          case OnSubscribe(_) => findMatch()
        })
      } catch {
        case _: AssertionError => TimedOut(timeOut)
      }
    }
    findMatch() match {
      case NotFound | TimedOut(_) => Assertions.succeed
      case StreamError(e) => Assertions.fail(s"encountered exception while draining stream: $e")
    }
  }

  /**
   * consumes elements from the stream until an element satisfies the given matcher. Fails if the
   * stream completes without match or times out.
   * @param test match criteria for the elements produced by the stream (can target subclasses)
   * @param timeOut maximum wait time for each element
   * @return success if the stream produced a matching element
   */
  def findFirstMatchingStreamElement[U <: T: ClassTag](test: Matcher[U], timeOut: FiniteDuration = defaultTimeOut): Assertion = {
    sink.ensureSubscription().request(Int.MaxValue)
    val messages = new FailureRecords(Seq(test))
    def findMatch(index: Long): SearchResult = {
      try {
        sink.expectEventWithTimeoutPF(timeOut, {
          case OnNext(element) => element match {
            case x: U =>
              val result = test(x)
              if (result.matches) {
                Found
              } else {
                messages.addResult(test, x, index, result)
                findMatch(index + 1)
              }
            case x =>
              messages.addResult(test, x.asInstanceOf[T], index, s"wrong class '${x.getClass}'")
              findMatch(index + 1)
          }
          case OnComplete => NotFound
          case OnError(e) => StreamError(e)
          case OnSubscribe(_) => findMatch(index)
        })
      } catch {
        case _: AssertionError => TimedOut(timeOut)
      }
    }
    findMatch(0) match {
      case Found => Assertions.succeed
      case failure => Assertions.fail(messages.printFailures(failure))
    }
  }

  /**
   * Consume elements from the stream until a matching element is found for each given matcher.
   * Each stream element will be used to satisfy at most one matcher. The stream must produce the
   * matching elements in the same order as the provided matcher list to satisfy the assertion.
   * Fails if the stream completes without satisfying all tests or times out.
   * @param tests match criteria for the elements produced by the stream (can target subclasses)
   * @param timeOut maximum wait time for each element
   * @return success if the stream produced a matching element
   */
  def findMatchingStreamElementsOrdered[U <: T: Manifest](tests: Seq[Matcher[U]], timeOut: FiniteDuration = defaultTimeOut): Assertion = {
    sink.ensureSubscription().request(Int.MaxValue)
    val messages = new FailureRecords(tests)
    var pendingTests = tests
    def findMatch(index: Long): SearchResult = {
      try {
        sink.expectEventWithTimeoutPF(timeOut, {
          case OnNext(element) => element match {
            case elem: U =>
              val test = pendingTests.head
              val result = test(elem)
              if (result.matches) {
                pendingTests = pendingTests.tail
                messages.succeedTest(test)
              } else {
                messages.addResult(test, elem, index, result)
              }
              if (pendingTests.isEmpty) {
                Found
              } else {
                findMatch(index + 1)
              }
            case x =>
              messages.addResult(pendingTests.head, x.asInstanceOf[T], index, s"wrong class '${x.getClass}'")
              findMatch(index + 1)
          }
          case OnComplete => NotFound
          case OnError(e) => StreamError(e)
          case OnSubscribe(_) => findMatch(index)
        })
      } catch {
        case _: AssertionError => TimedOut(timeOut)
      }
    }
    findMatch(0) match {
      case Found => Assertions.succeed
      case failure => Assertions.fail(messages.printFailures(failure))
    }
  }

  /**
   * Drains elements from the stream until it produces an error which is then checked with the
   * given matcher. The test fails if the stream completes normally or times out.
   * @param test criteria for the expected exception
   * @param timeOut maximum wait time for each element
   * @return success if the stream fails with a matching exception
   */
  def expectStreamFailure(test: Matcher[Throwable], timeOut: FiniteDuration = defaultTimeOut): Assertion = {
    sink.ensureSubscription().request(Int.MaxValue)
    def findMatch(): SearchResult = {
      try {
        sink.expectEventWithTimeoutPF(timeOut, {
          case OnNext(_) => findMatch()
          case OnComplete => NotFound
          case OnError(e) => StreamError(e)
          case OnSubscribe(_) => findMatch()
        })
      } catch {
        case _: AssertionError => TimedOut(timeOut)
      }
    }
    findMatch() match {
      case StreamError(e) =>
        val result = test(e)
        if (result.matches) {
          Assertions.succeed
        } else {
          Assertions.fail(result.failureMessage)
        }
      case noFailure => Assertions.fail(noFailure match {
        case TimedOut(t) => s"no new event received before time-out ($t)"
        case NotFound => "expected stream failure, but stream completed normally"
      })
    }
  }

  /**
   * Drains elements from the stream until it completes normally. If a time-out or stream failure
   * is encountered, the assertion will fail.
   * @param timeOut maximum wait time for each element
   * @return success if the stream completes normally
   */
  def expectStreamCompletion(timeOut: FiniteDuration = defaultTimeOut): Assertion = {
    sink.ensureSubscription().request(Int.MaxValue)

    def findMatch(): SearchResult = {
      try {
        sink.expectEventWithTimeoutPF(timeOut, {
          case OnNext(_) => findMatch()
          case OnComplete => NotFound
          case OnError(e) => StreamError(e)
          case OnSubscribe(_) => findMatch()
        })
      } catch {
        case _: AssertionError => TimedOut(timeOut)
      }
    }

    findMatch() match {
      case NotFound => Assertions.succeed
      case noComplete => Assertions.fail(noComplete match {
        case TimedOut(t) => s"no new event received before time-out ($t)"
        case StreamError(e) => s"expected stream completion, but stream failed with exception $e"
      })
    }
  }

}
