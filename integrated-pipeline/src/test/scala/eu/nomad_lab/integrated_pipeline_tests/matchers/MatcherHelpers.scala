package eu.nomad_lab.integrated_pipeline_tests.matchers

import org.scalatest.matchers.{ HavePropertyMatchResult, HavePropertyMatcher }

object MatcherHelpers {

  def propertyMatcher[T, E](propertyName: String, expected: E, test: T => E): HavePropertyMatcher[T, E] = {
    new HavePropertyMatcher[T, E] {
      def apply(element: T): HavePropertyMatchResult[E] = {
        val actual = test(element)
        HavePropertyMatchResult(actual == expected, propertyName, expected, actual)
      }
      override def toString(): String = s"$propertyName '$expected'"
    }
  }

  def subStringPropertyMatcher[T](propertyName: String, expected: Seq[String],
    test: T => String, display: T => String, ignoreCase: Boolean = true): HavePropertyMatcher[T, String] = {
    new HavePropertyMatcher[T, String] {
      def apply(element: T): HavePropertyMatchResult[String] = {
        val message = if (ignoreCase) test(element).toLowerCase else test(element)
        HavePropertyMatchResult(
          matches = expected.forall(test => message.contains(
            if (ignoreCase) test.toLowerCase else test
          )),
          propertyName = "error message content",
          expectedValue = expected.mkString("all of '", "', '", "'"),
          actualValue = display(element)
        )
      }
      override def toString(): String = s"$propertyName '${expected.mkString("all of '", "', '", "'")}'"
    }
  }
}
