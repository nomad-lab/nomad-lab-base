package eu.nomad_lab.integrated_pipeline_tests.matchers

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.integrated_pipeline.FileTree
import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.parsers.ParseResult.ParseResult
import eu.nomad_lab.parsers.{ FinishedParsingSession, ParseEvent, StartedParsingSession }
import org.scalatest.matchers.HavePropertyMatcher

object TreeScanSignalMatchers {
  def fileTree(expectedValue: FileTree): HavePropertyMatcher[TreeScanSignal, FileTree] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parent tree task",
      expected = expectedValue,
      test = (x: TreeScanSignal) => x.fileTree
    )

  def relativePath(expectedValue: Path): HavePropertyMatcher[CandidateFound, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: CandidateFound) => x.relativePath
    )

  def relativePath(expectedValue: String): HavePropertyMatcher[CandidateFound, Path] = {
    relativePath(Paths.get(expectedValue))
  }

  def extractedPath(expectedValue: Option[Path]): HavePropertyMatcher[CandidateFound, Option[Path]] =
    MatcherHelpers.propertyMatcher(
      propertyName = "temporary extracted file path",
      expected = expectedValue,
      test = (x: CandidateFound) => x.extractedPath
    )

  def extractedPath(expectedValue: String): HavePropertyMatcher[CandidateFound, Option[Path]] = {
    extractedPath(Some(Paths.get(expectedValue)))
  }

  def numParsingTasks(expectedValue: Long): HavePropertyMatcher[TreeScanCompleted, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of identified candidate calculations",
      expected = expectedValue,
      test = (x: TreeScanCompleted) => x.numParsingTasks
    )

}

object FileParsingSignalMatchers {

  def fileTree(expectedValue: FileTree): HavePropertyMatcher[FileParsingSignal, FileTree] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parent tree task",
      expected = expectedValue,
      test = (x: FileParsingSignal) => x.fileTree
    )

  def status(expectedValue: ParseResult): HavePropertyMatcher[FileParsingResult, ParseResult] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parsing result status",
      expected = expectedValue,
      test = (x: FileParsingResult) => x.result
    )

  def relativePath(expectedValue: Path): HavePropertyMatcher[FileParsingResult, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: FileParsingResult) => x.task.relativePath
    )

  def relativePath(expectedValue: String): HavePropertyMatcher[FileParsingResult, Path] =
    relativePath(Paths.get(expectedValue))

  def numParsingTasks(expectedValue: Long): HavePropertyMatcher[TreeScanCompleted, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of identified candidate calculations",
      expected = expectedValue,
      test = (x: TreeScanCompleted) => x.numParsingTasks
    )

  def startSignal(expectedValue: Option[StartedParsingSession]): HavePropertyMatcher[FileParsingResult, Option[StartedParsingSession]] =
    MatcherHelpers.propertyMatcher(
      propertyName = "session start signal",
      expected = expectedValue,
      test = (x: FileParsingResult) => x.start
    )

  def endSignal(expectedValue: Option[FinishedParsingSession]): HavePropertyMatcher[FileParsingResult, Option[FinishedParsingSession]] =
    MatcherHelpers.propertyMatcher(
      propertyName = "session end signal",
      expected = expectedValue,
      test = (x: FileParsingResult) => x.end
    )

  def events(expectedValue: Seq[ParseEvent]): HavePropertyMatcher[FileParsingResult, Seq[ParseEvent]] =
    MatcherHelpers.propertyMatcher(
      propertyName = "emitted parsing events",
      expected = expectedValue,
      test = (x: FileParsingResult) => x match {
      case x: ParsingResultInMemory => x.events
    }
    )

  def errorMessage(first: String, other: String*): HavePropertyMatcher[FileParsingResult, String] =
    MatcherHelpers.subStringPropertyMatcher(
      propertyName = "error message content",
      expected = first +: other,
      test = (x: FileParsingResult) => x.error.getOrElse(""),
      display = (x: FileParsingResult) => x.error.toString
    )

}

object FileTreeParsingResultMatchers {

  def fileTree(expectedValue: FileTree): HavePropertyMatcher[FileTreeParsingResult, FileTree] =
    MatcherHelpers.propertyMatcher(
      propertyName = "origin file tree",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.fileTree
    )

  def basePath(expectedValue: Path): HavePropertyMatcher[FileTreeParsingResult, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "tree base path",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.fileTree.treeBasePath
    )

  def basePath(expectedValue: String): HavePropertyMatcher[FileTreeParsingResult, Path] =
    basePath(Paths.get(expectedValue))

  def treeType(expectedValue: TreeType): HavePropertyMatcher[FileTreeParsingResult, TreeType] =
    MatcherHelpers.propertyMatcher(
      propertyName = "file tree type",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.fileTree.treeType
    )

  def numParsingTasks(expectedValue: Long): HavePropertyMatcher[FileTreeParsingResult, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of processed calculations",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.numCalculationsFound
    )

  def numFailedTasks(expectedValue: Long): HavePropertyMatcher[FileTreeParsingResult, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of failed calculations",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.numParsingFailures
    )

  def outputPath(expectedValue: Path): HavePropertyMatcher[FileTreeParsingResult, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "output location",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.outputLocation
    )

  def outputPath(expectedValue: String): HavePropertyMatcher[FileTreeParsingResult, Path] =
    outputPath(Paths.get(expectedValue))

  def outputType(expectedValue: OutputType): HavePropertyMatcher[FileTreeParsingResult, OutputType] =
    MatcherHelpers.propertyMatcher(
      propertyName = "output format",
      expected = expectedValue,
      test = (x: FileTreeParsingResult) => x.outputFormat
    )

}