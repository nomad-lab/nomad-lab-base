package eu.nomad_lab.integrated_pipeline_tests.matchers

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.parsers.ParseResult.ParseResult
import org.scalatest.matchers.HavePropertyMatcher

trait MatcherResultWriterEvent {

  def basePath(expectedValue: Path): HavePropertyMatcher[ResultWriterEvent, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "tree base path",
      expected = expectedValue,
      test = (x: ResultWriterEvent) => x.fileTree.treeBasePath
    )

  def basePath(expectedValue: String): HavePropertyMatcher[ResultWriterEvent, Path] =
    basePath(Paths.get(expectedValue))

}

object MatcherResultWriterEventStart extends MatcherResultWriterEvent

object MatcherResultWriterEventResult extends MatcherResultWriterEvent {

  def relativePath(expectedValue: Path): HavePropertyMatcher[ResultWriterEventResult, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: ResultWriterEventResult) => x.relativePath
    )

  def relativePath(expectedValue: String): HavePropertyMatcher[ResultWriterEventResult, Path] =
    relativePath(Paths.get(expectedValue))

  def parser(expectedValue: String): HavePropertyMatcher[ResultWriterEventResult, String] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parser name",
      expected = expectedValue,
      test = (x: ResultWriterEventResult) => x.parser
    )

  def status(expectedValue: ParseResult): HavePropertyMatcher[ResultWriterEventResult, ParseResult] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parsing result",
      expected = expectedValue,
      test = (x: ResultWriterEventResult) => x.result
    )

  def errorMessage(first: String, other: String*): HavePropertyMatcher[ResultWriterEventResult, String] =
    MatcherHelpers.subStringPropertyMatcher(
      propertyName = "error message content",
      expected = first +: other,
      test = (x: ResultWriterEventResult) => x.error.getOrElse(""),
      display = (x: ResultWriterEventResult) => x.error.toString
    )
}

object MatcherResultWriterEventEnd extends MatcherResultWriterEvent {

  def numCalculations(expectedValue: Long): HavePropertyMatcher[ResultWriterEventEnd, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of processed calculations",
      expected = expectedValue,
      test = (x: ResultWriterEventEnd) => x.numCalculations
    )

  def numFailures(expectedValue: Long): HavePropertyMatcher[ResultWriterEventEnd, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of failed calculations",
      expected = expectedValue,
      test = (x: ResultWriterEventEnd) => x.numParsingFailures
    )

  def outputPath(expectedValue: Path): HavePropertyMatcher[ResultWriterEventEnd, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: ResultWriterEventEnd) => x.outputLocation
    )

  def outputPath(expectedValue: String): HavePropertyMatcher[ResultWriterEventEnd, Path] =
    outputPath(Paths.get(expectedValue))

}