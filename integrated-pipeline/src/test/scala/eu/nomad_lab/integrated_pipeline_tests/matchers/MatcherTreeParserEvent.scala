package eu.nomad_lab.integrated_pipeline_tests.matchers

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.integrated_pipeline.messages._
import org.scalatest.matchers.HavePropertyMatcher

trait MatcherTreeParserEvent {

  def basePath(expectedValue: Path): HavePropertyMatcher[TreeParserEvent, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "tree base path",
      expected = expectedValue,
      test = (x: TreeParserEvent) => x.fileTree.treeBasePath
    )

  def basePath(expectedValue: String): HavePropertyMatcher[TreeParserEvent, Path] =
    basePath(Paths.get(expectedValue))

}

object MatcherTreeParserEventStart extends MatcherTreeParserEvent

object MatcherTreeParserEventCandidate extends MatcherTreeParserEvent {

  def relativePath(expectedValue: Path): HavePropertyMatcher[TreeParserEventCandidate, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: TreeParserEventCandidate) => x.relativePath
    )

  def relativePath(expectedValue: String): HavePropertyMatcher[TreeParserEventCandidate, Path] =
    relativePath(Paths.get(expectedValue))

  def parser(expectedValue: String): HavePropertyMatcher[TreeParserEventCandidate, String] =
    MatcherHelpers.propertyMatcher(
      propertyName = "candidate parser name",
      expected = expectedValue,
      test = (x: TreeParserEventCandidate) => x.parser
    )

}

object MatcherTreeParserEventScanError extends MatcherTreeParserEvent {

  def errorMessage(first: String, other: String*): HavePropertyMatcher[TreeParserEventScanError, String] =
    MatcherHelpers.subStringPropertyMatcher(
      propertyName = "error message content",
      expected = first +: other,
      test = (x: TreeParserEventScanError) => x.error.getMessage,
      display = (x: TreeParserEventScanError) => x.error.toString
    )

  def relativePath(expectedValue: Path): HavePropertyMatcher[TreeParserEventScanError, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: TreeParserEventScanError) => x.relativePath
    )

  def relativePath(expectedValue: String): HavePropertyMatcher[TreeParserEventScanError, Path] =
    relativePath(Paths.get(expectedValue))

}

object MatcherTreeParserEventEnd extends MatcherTreeParserEvent {

  def numCandidates(expectedValue: Long): HavePropertyMatcher[TreeParserEventEnd, Long] =
    MatcherHelpers.propertyMatcher(
      propertyName = "number of candidate calculations",
      expected = expectedValue,
      test = (x: TreeParserEventEnd) => x.numCandidates
    )

}

object MatcherTreeParserEventTreeFailure extends MatcherTreeParserEvent {

  def errorMessage(first: String, other: String*): HavePropertyMatcher[TreeParserEventTreeFailure, String] =
    MatcherHelpers.subStringPropertyMatcher(
      propertyName = "error message content",
      expected = first +: other,
      test = (x: TreeParserEventTreeFailure) => x.error.getMessage,
      display = (x: TreeParserEventTreeFailure) => x.error.toString
    )

}
