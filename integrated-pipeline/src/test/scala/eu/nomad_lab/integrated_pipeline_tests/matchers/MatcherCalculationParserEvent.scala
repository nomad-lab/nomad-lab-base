package eu.nomad_lab.integrated_pipeline_tests.matchers

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.integrated_pipeline.messages.{ CalculationParserEvent, CalculationParserEventEnd }
import eu.nomad_lab.parsers.ParseResult.ParseResult
import org.scalatest.matchers.HavePropertyMatcher

trait MatcherCalculationParserEvent {

  def basePath(expectedValue: Path): HavePropertyMatcher[CalculationParserEvent, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "tree base path",
      expected = expectedValue,
      test = (x: CalculationParserEvent) => x.fileTree.treeBasePath
    )

  def basePath(expectedValue: String): HavePropertyMatcher[CalculationParserEvent, Path] =
    basePath(Paths.get(expectedValue))

  def relativePath(expectedValue: Path): HavePropertyMatcher[CalculationParserEvent, Path] =
    MatcherHelpers.propertyMatcher(
      propertyName = "relative file path",
      expected = expectedValue,
      test = (x: CalculationParserEvent) => x.relativePath
    )

  def relativePath(expectedValue: String): HavePropertyMatcher[CalculationParserEvent, Path] =
    relativePath(Paths.get(expectedValue))

  def parser(expectedValue: String): HavePropertyMatcher[CalculationParserEvent, String] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parser name",
      expected = expectedValue,
      test = (x: CalculationParserEvent) => x.parser
    )

}

object MatcherCalculationParserEventStart extends MatcherCalculationParserEvent

object MatcherCalculationParserEventEnd extends MatcherCalculationParserEvent {

  def status(expectedValue: ParseResult): HavePropertyMatcher[CalculationParserEventEnd, ParseResult] =
    MatcherHelpers.propertyMatcher(
      propertyName = "parsing result",
      expected = expectedValue,
      test = (x: CalculationParserEventEnd) => x.result
    )

  def errorMessage(first: String, other: String*): HavePropertyMatcher[CalculationParserEventEnd, String] =
    MatcherHelpers.subStringPropertyMatcher(
      propertyName = "error message content",
      expected = first +: other,
      test = (x: CalculationParserEventEnd) => x.error.getOrElse(""),
      display = (x: CalculationParserEventEnd) => x.error.toString
    )

}
