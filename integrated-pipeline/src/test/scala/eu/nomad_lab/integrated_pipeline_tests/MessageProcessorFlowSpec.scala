package eu.nomad_lab.integrated_pipeline_tests

import akka.stream.ClosedShape
import akka.stream.scaladsl.{ GraphDSL, RunnableGraph }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import eu.nomad_lab.integrated_pipeline.MessageProcessor
import eu.nomad_lab.integrated_pipeline.stream_components.MessageProcessorFlow
import eu.nomad_lab.integrated_pipeline_tests.matchers.StreamAssertions
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ Matchers, WordSpec }

class MessageProcessorFlowSpec extends WordSpec with MockitoSugar with Matchers {

  private class Fixture extends StreamAssertions[String] {
    val processorMock = mock[MessageProcessor[Int, String]]
    val flow = new MessageProcessorFlow[Int, String] {
      override val processor: MessageProcessor[Int, String] = processorMock
      override val stageName: String = "testStage"
    }

    /**
     * More flexible than a simple "thenReturn" stubbing because hasSignalToEmit can be called as
     * often as the implementation desires
     * @param values values to emit in order
     */
    def emitValues(values: Seq[String]): Unit = {
      var remaining = values
      when(processorMock.hasSignalToEmit()).thenAnswer(new Answer[Boolean] {
        def answer(x: InvocationOnMock) = remaining.nonEmpty
      })
      when(processorMock.getNextSignalToEmit()).thenAnswer(new Answer[String] {
        def answer(x: InvocationOnMock) = {
          if (remaining.nonEmpty) {
            val result = remaining.head
            remaining = remaining.tail
            result
          } else {
            throw new IllegalStateException("dummy iterator exhausted")
          }
        }
      })
    }

    private val testInput = TestSource.probe[Int]
    private val testOutput = TestSink.probe[String]
    val (source, sink) = RunnableGraph.fromGraph(
      GraphDSL.create(testInput, testOutput)((_, _)) { implicit builder => (source, sink) =>
        import GraphDSL.Implicits._

        source ~> flow ~> sink
        ClosedShape
      }
    ).run()
  }

  "A MessageProcessorFlow" when {
    "having no message ready to be emitted" should {
      "fetch new messages from the stream until a message is ready to be emitted" in {
        val f = new Fixture
        f.source.sendNext(42).sendNext(13).sendNext(19)
        when(f.processorMock.hasSignalToEmit()).thenReturn(false, false, false, true, false)
        when(f.processorMock.getNextSignalToEmit()).thenReturn("foo")
        f.sink.ensureSubscription().requestNext()
        verify(f.processorMock, times(1)).getNextSignalToEmit()
        verify(f.processorMock, times(3)).processSignal(any())
      }

      "should complete when the processor has no unfinished messages left when upstream completes" in {
        val f = new Fixture
        when(f.processorMock.hasSignalToEmit()).thenReturn(false)
        when(f.processorMock.requiresMoreMessages).thenReturn(false)
        f.source.sendComplete()
        f.expectStreamCompletion()
        verify(f.processorMock, Mockito.atLeast(1)).requiresMoreMessages
      }

      "should fail the stage when the processor has unfinished messages left when upstream completes" in {
        val f = new Fixture
        when(f.processorMock.hasSignalToEmit()).thenReturn(false)
        when(f.processorMock.requiresMoreMessages).thenReturn(true)
        f.source.sendComplete()
        f.expectStreamFailure(be(a[IllegalStateException]))
        verify(f.processorMock, Mockito.atLeast(1)).requiresMoreMessages
      }
    }

    "having one or more messages ready to be emitted" should {
      "continue emitting messages until no more message is ready to be sent" in {
        val f = new Fixture
        f.emitValues(Seq("foo1", "foo2", "foo3"))
        f.findMatchingStreamElementsOrdered(Seq(be("foo1"), be("foo2"), be("foo3")))
        f.drainStream()
        verify(f.processorMock, times(3)).getNextSignalToEmit()
      }
    }

    "having one or more messages ready to be emitted when upstream completes" should {
      "should emit all remaining messages and then complete if no unfinished messages remain" in {
        val f = new Fixture
        f.emitValues(Seq("foo1", "foo2", "foo3"))
        when(f.processorMock.requiresMoreMessages).thenReturn(false)
        f.source.sendComplete()
        f.findMatchingStreamElementsOrdered(Seq(be("foo1"), be("foo2"), be("foo3")))
        f.expectStreamCompletion()
        verify(f.processorMock, Mockito.atLeast(1)).requiresMoreMessages
      }

      "should emit all remaining messages and then fail if unfinished messages remain" in {
        val f = new Fixture
        f.emitValues(Seq("foo1", "foo2", "foo3"))
        when(f.processorMock.requiresMoreMessages).thenReturn(true)
        f.source.sendComplete()
        f.findMatchingStreamElementsOrdered(Seq(be("foo1"), be("foo2"), be("foo3")))
        f.expectStreamFailure(be(a[IllegalStateException]))
        verify(f.processorMock, Mockito.atLeast(1)).requiresMoreMessages
      }
    }
  }

}
