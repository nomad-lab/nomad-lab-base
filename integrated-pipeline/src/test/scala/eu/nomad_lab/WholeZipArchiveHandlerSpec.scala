package eu.nomad_lab

import java.nio.file.Paths

import eu.nomad_lab.integrated_pipeline.io_integrations.{ ArchiveHandler, WholeZipArchiveHandler }
import eu.nomad_lab.integrated_pipeline_tests._
import eu.nomad_lab.integrated_pipeline_tests.helpers.CustomMatchers
import org.scalatest.WordSpec
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.mockito.ArgumentMatchers._

class WholeZipArchiveHandlerSpec extends WordSpec with TestDataBuilders with CustomMatchers with MockitoSugar {

  abstract class Fixture {
    val underlying = mock[ArchiveHandler]
    val handler = new WholeZipArchiveHandler(underlying)
    val sample = aFileTree().withBasePath("/some/path.zip").withTreeType(TreeType.Zip).build()
    when(underlying.extractZipArchive(any())).thenReturn(Paths.get("/tmp/extracted/directory"))
  }

  class NoSignalsReceived extends Fixture {}

  class OneCandidateSignalReceived extends Fixture {
    handler.processTreeScanSignal(aCandidateFound().withFileTree(sample))
  }

  class EndTreeScanSignalReceived extends Fixture {
    handler.processTreeScanSignal(aCandidateFound().withFileTree(sample))
    handler.processFileParsingSignal(aTreeScanCompleted().withFileTree(sample).withTaskCount(1))
  }

  class FileParsingResultReceived extends Fixture {
    handler.processTreeScanSignal(aCandidateFound().withFileTree(sample))
    handler.processFileParsingSignal(aParsingResultInMemory().withFileTree(sample))
  }

  class FullyProcessedATree extends Fixture {
    handler.processTreeScanSignal(aCandidateFound().withFileTree(sample))
    handler.processFileParsingSignal(aParsingResultInMemory().withFileTree(sample))
    handler.processFileParsingSignal(aTreeScanCompleted().withFileTree(sample).withTaskCount(1))
  }

  "A WholeZipArchiveHandler" when {

    "when not having processed any signals" should {
      def createFixture = () => new NoSignalsReceived

      behave like startANewZipArchive(createFixture)
    }

    "when having processed a candidate file signal" should {
      def createFixture = () => new OneCandidateSignalReceived

      behave like startANewZipArchive(createFixture)
    }

    "when having received a parsing result" should {
      def createFixture = () => new FileParsingResultReceived

      behave like startANewZipArchive(createFixture)

      "should clean up temporary extracted files when the end parsing signal arrives with the appropriate task count" in {
        val f = createFixture()
        f.handler.processFileParsingSignal(aTreeScanCompleted().withFileTree(f.sample).withTaskCount(1))
        verify(f.underlying).cleanUpExtractedArchive(f.sample.treeBasePath)
      }

      "not extract the same archive again when the next candidate signal arrives" in {
        val f = createFixture()
        verify(f.underlying, times(1)).extractZipArchive(f.sample.treeBasePath)
        f.handler.processTreeScanSignal(aCandidateFound().withFileTree(f.sample))
        verify(f.underlying, times(1)).extractZipArchive(f.sample.treeBasePath)
      }
    }

    "when having received a end parsing signal" should {
      def createFixture = () => new EndTreeScanSignalReceived

      behave like startANewZipArchive(createFixture)

      "should clean up temporary extracted files when the appropriate number of parsing results arrived" in {
        val f = createFixture()
        f.handler.processFileParsingSignal(aParsingResultInMemory().withFileTree(f.sample))
        verify(f.underlying).cleanUpExtractedArchive(f.sample.treeBasePath)
      }
    }

    "when having finished processing a tree" should {
      def createFixture = () => new FullyProcessedATree

      behave like startANewZipArchive(createFixture)

      "should restart an already fully processed archive if a new candidate task arrives" in {
        val f = createFixture()
        f.handler.processTreeScanSignal(aCandidateFound().withFileTree(f.sample))
        verify(f.underlying, times(2)).extractZipArchive(f.sample.treeBasePath)
      }
    }
  }

  def startANewZipArchive(fixture: () => Fixture): Unit = {
    val zipArchiveFileTree = aFileTree().withBasePath("/foo/bar.zip").withTreeType(TreeType.Zip)

    "should unpack the entire archive when the first candidate signal is processed" in {
      val f = fixture()
      f.handler.processTreeScanSignal(aCandidateFound().withFileTree(zipArchiveFileTree))
      verify(f.underlying).extractZipArchive(Paths.get("/foo/bar.zip"))
    }

    "should assign the correct extracted file path to processed candidate found signals" in {
      val f = fixture()
      val signal = aCandidateFound().withFileTree(zipArchiveFileTree).withRelativePath("foo/relative.out")
      val extractedPath = f.handler.processTreeScanSignal(signal)
      extractedPath should be(Some(Paths.get("/tmp/extracted/directory/foo/relative.out")))
    }

    "do nothing when a end tree file scan signal is processed" in {
      val f = fixture()
      f.handler.processTreeScanSignal(aTreeScanCompleted().withFileTree(zipArchiveFileTree))
      verify(f.underlying, never()).extractZipArchive(Paths.get("/foo/bar.zip"))
    }
  }

}
