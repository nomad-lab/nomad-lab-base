package eu.nomad_lab.integrated_pipeline.io_integrations

import java.io.{ FileOutputStream, IOException }
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{ FileVisitResult, Files, Path, SimpleFileVisitor }

import eu.nomad_lab.LocalEnv
import org.apache.commons.compress.archivers.zip.ZipFile
import org.apache.commons.compress.utils.IOUtils

import scala.collection.parallel.mutable

class ArchiveHandler(extractionLocation: Path) {

  private val extractedArchives: mutable.ParMap[Path, Path] = mutable.ParMap()

  def extractZipArchive(archive: Path): Path = {
    require(archive.toFile.exists())
    require(!extractedArchives.contains(archive.toAbsolutePath), "archive already extracted!")
    val zip = new ZipFile(archive.toFile)
    val entries = zip.getEntries
    val destination = Files.createTempDirectory(extractionLocation, archive.getFileName.toString)
    extractedArchives += archive.toAbsolutePath -> destination
    while (entries.hasMoreElements) {
      val entry = entries.nextElement()
      val filePath = destination.resolve(entry.getName)
      Files.createDirectories(filePath.getParent, LocalEnv.directoryPermissionsAttributes)
      val zIn = zip.getInputStream(entry)
      val out = new FileOutputStream(
        Files.createFile(filePath, LocalEnv.filePermissionsAttributes).toFile
      )
      IOUtils.copy(zIn, out)
      IOUtils.closeQuietly(zIn)
      out.close()
    }
    zip.close()
    destination
  }

  def cleanUpExtractedArchive(archive: Path): Unit = {
    val location = extractedArchives(archive.toAbsolutePath)
    Files.walkFileTree(location, new SimpleFileVisitor[Path]() {
      override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
        Files.delete(file)
        FileVisitResult.CONTINUE
      }

      override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
        Files.delete(dir)
        FileVisitResult.CONTINUE
      }
    })
    location.toFile.delete()
    extractedArchives -= archive.toAbsolutePath
  }

}
