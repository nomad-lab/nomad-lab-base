package eu.nomad_lab.integrated_pipeline.io_integrations

import java.nio.file.Path

import eu.nomad_lab.integrated_pipeline.RawDataArchiveHandler
import eu.nomad_lab.integrated_pipeline.messages._

import scala.collection.{ mutable, parallel }
import scala.collection.parallel.mutable.ParMap

/**
 * This implementation unpacks the entire zip-archive once to a temporary location when the first
 * candidate file signal is received and then points all follow-up candidate signals to the already
 * extracted content.
 * Cleanup is performed once all the end tree signal has been received as well as the appropriate
 * number of file-parsing results.
 */
class WholeZipArchiveHandler(handler: ArchiveHandler) extends RawDataArchiveHandler {

  private val pathMap: parallel.mutable.ParMap[Path, Path] = ParMap()
  private val processed: mutable.Map[Path, Long] = mutable.Map()
  private val expected: mutable.Map[Path, Long] = mutable.Map()

  override def processTreeScanSignal(signal: TreeScanSignal): Option[Path] = {
    signal match {
      case _: TreeScanCompleted => None
      case x: CandidateFound =>
        val path = pathMap.get(x.fileTree.treeBasePath)
        if (path.isEmpty) {
          val location = handler.extractZipArchive(x.fileTree.treeBasePath)
          pathMap += x.fileTree.treeBasePath -> location
          Some(location.resolve(x.relativePath))
        } else {
          path.map(_.resolve(x.relativePath))
        }
    }
  }

  override def processFileParsingSignal(signal: FileParsingSignal): Unit = {
    val key = signal.fileTree.treeBasePath
    signal match {
      case x: TreeScanCompleted => expected += key -> x.numParsingTasks
      case _: FileParsingResult => processed += key -> (processed.getOrElse(key, 0l) + 1)
    }
    if (expected.getOrElse(key, -1) == processed.getOrElse(key, -2)) {
      pathMap -= key
      expected.remove(key)
      processed.remove(key)
      handler.cleanUpExtractedArchive(key)
    }
  }
}
