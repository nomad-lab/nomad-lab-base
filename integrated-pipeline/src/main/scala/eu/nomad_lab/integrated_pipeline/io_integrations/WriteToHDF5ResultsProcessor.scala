package eu.nomad_lab.integrated_pipeline.io_integrations

import java.nio.file.{ Files, Path, Paths }

import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline.messages.{ FileParsingResult, ParsingResultInMemory }
import eu.nomad_lab.integrated_pipeline.{ FileTree, OutputType, ParsingResultsProcessor }
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.parsers.H5Backend.H5File
import eu.nomad_lab.parsers.{ H5Backend, ReindexBackend }

class WriteToHDF5ResultsProcessor(outputLocation: Path, metaInfo: MetaInfoEnv) extends ParsingResultsProcessor {
  override def processFileParsingResult(result: FileParsingResult): Unit = {
    val id = result.task.calculationGid
    val archiveGid = result.task.fileTree.archiveId
    val fileName = Paths.get(id + ".h5")
    val targetPath = outputLocation(result.fileTree).resolve(fileName)
    Files.createDirectories(outputLocation(result.fileTree))
    val h5file = H5File.create(targetPath, Paths.get("/", archiveGid, id))
    val h5Backend = H5Backend(metaEnv = metaInfo, h5File = h5file, closeFileOnFinishedParsing = false)
    val backend = new ReindexBackend(h5Backend)
    try {
      result match {
        case x: ParsingResultInMemory =>
          x.start.foreach(_.emitOnBackend(backend))
          x.events.foreach(_.emitOnBackend(backend))
          x.end.foreach(_.emitOnBackend(backend))
      }
      backend.cleanup()
    } finally {
      h5Backend.close()
    }
  }

  override val outputType: OutputType = OutputType.HDF5

  override def startProcessingTreeResults(fileTree: FileTree): Unit = ()

  override def finishProcessingTreeResults(fileTree: FileTree): Unit = ()

  override def outputLocation(fileTree: FileTree): Path = {
    outputLocation.resolve(fileTree.prefixFolder).resolve(fileTree.archiveId)
  }
}
