package eu.nomad_lab.integrated_pipeline.io_integrations

import java.nio.file.{ Files, Path, Paths }

import eu.nomad_lab.{ H5Lib, TreeType }
import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline.messages.{ FileParsingResult, ParsingResultInMemory }
import eu.nomad_lab.integrated_pipeline.{ FileTree, OutputType, ParsingResultsProcessor }
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.parsers.H5Backend.H5File
import eu.nomad_lab.parsers.{ H5Backend, ReindexBackend }

import scala.collection.mutable

/**
 * Write parsing results to one HDF5 file per file tree.
 * @param outputLocation target folder for generated HDF5s
 * @param metaInfo meta-info to use when writing HDF5
 */
class WriteToHDF5MergedResultsProcessor(
    outputLocation: Path,
    metaInfo: MetaInfoEnv
) extends ParsingResultsProcessor {

  private val fileMap: mutable.Map[FileTree, H5File] = mutable.Map()

  override def outputLocation(fileTree: FileTree): Path = {
    outputLocation.resolve(fileTree.prefixFolder)
  }

  override def processFileParsingResult(result: FileParsingResult): Unit = {
    val id = result.task.calculationGid
    val archiveFile = fileMap(result.task.fileTree)
    val archiveHandle = archiveFile.groupHandle
    val calcHandle = H5Lib.groupGet(archiveHandle, id, create = true)
    val storageLocation = new H5File(
      filePath = archiveFile.filePath,
      mainGroupPath = archiveFile.mainGroupPath.resolve(id),
      h5FileHandle = -1,
      groupHandle = calcHandle
    )
    val h5Backend = H5Backend(
      metaEnv = metaInfo,
      h5File = storageLocation,
      closeFileOnFinishedParsing = false
    )
    val backend = new ReindexBackend(h5Backend)
    try {
      result match {
        case x: ParsingResultInMemory =>
          x.start.foreach(_.emitOnBackend(backend))
          x.events.foreach(_.emitOnBackend(backend))
          x.end.foreach(_.emitOnBackend(backend))
      }
      backend.cleanup()
    } finally {
      h5Backend.close()
    }
  }

  override val outputType: OutputType = OutputType.HDF5

  override def startProcessingTreeResults(fileTree: FileTree): Unit = {
    Files.createDirectories(outputLocation(fileTree))
    val targetPath = outputLocation(fileTree).resolve(fileTree.fileName)
    val h5file = H5File.create(targetPath, Paths.get("/", fileTree.archiveId))
    fileMap(fileTree) = h5file
  }

  override def finishProcessingTreeResults(fileTree: FileTree): Unit = {
    val h5file = fileMap.remove(fileTree)
    //TODO: error handling
    if (h5file.nonEmpty) {
      h5file.get.close()
    }
  }

}
