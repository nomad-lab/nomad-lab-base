package eu.nomad_lab.integrated_pipeline.io_integrations

import java.io.FileWriter
import java.nio.file.{ Files, Path, Paths }

import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline.messages.{ FileParsingResult, ParsingResultInMemory }
import eu.nomad_lab.integrated_pipeline.{ FileTree, OutputType, ParsingResultsProcessor }
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.parsers.JsonWriterBackend

/**
 * Writes one json-file with NOMAD meta-data for each parsed calculation.
 */
class WriteToJsonResultsProcessor(outputLocation: Path, metaInfo: MetaInfoEnv) extends ParsingResultsProcessor {
  override def processFileParsingResult(result: FileParsingResult): Unit = {
    val id = result.task.calculationGid
    val fileName = Paths.get(id + ".json")
    val targetPath = outputLocation(result.fileTree).resolve(fileName)
    Files.createDirectories(outputLocation(result.fileTree))
    val fileWriter = new FileWriter(targetPath.toFile)
    try {
      val backend = new JsonWriterBackend(metaInfoEnv = metaInfo, outF = fileWriter)
      result match {
        case x: ParsingResultInMemory =>
          x.start.foreach(_.emitOnBackend(backend))
          x.events.foreach(_.emitOnBackend(backend))
          x.end.foreach(_.emitOnBackend(backend))
      }
      backend.cleanup()
    } finally {
      fileWriter.close()
    }
  }

  override val outputType: OutputType = OutputType.Json

  override def startProcessingTreeResults(fileTree: FileTree): Unit = ()

  override def finishProcessingTreeResults(fileTree: FileTree): Unit = ()

  override def outputLocation(fileTree: FileTree): Path = {
    outputLocation.resolve(fileTree.prefixFolder).resolve(fileTree.archiveId)
  }
}
