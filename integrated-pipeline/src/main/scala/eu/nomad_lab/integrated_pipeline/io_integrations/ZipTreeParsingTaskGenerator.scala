package eu.nomad_lab.integrated_pipeline.io_integrations

import java.nio.file.Paths

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.{ FileTree, ParsingTaskGenerator }
import eu.nomad_lab.integrated_pipeline.messages.{ CandidateFound, TreeParserEventScanError }
import eu.nomad_lab.parsers.{ CandidateParser, ParserCollection }
import org.apache.commons.compress.archivers.zip.{ ZipArchiveEntry, ZipFile }

import scala.annotation.tailrec
import scala.util.control.NonFatal

class ZipTreeParsingTaskGenerator(request: FileTree, parserCollection: ParserCollection)
    extends ParsingTaskGenerator {

  require(request.treeType == TreeType.Zip, "file tree to process must be a Zip archive")

  private val zipFile = new ZipFile(request.treeBasePath.toFile)
  private val zipEntries: java.util.Enumeration[ZipArchiveEntry] = zipFile.getEntries

  override def finalize(): Unit = {
    zipFile.close()
  }

  @tailrec protected[this] final override def findNextParsingCandidate(): Option[Either[TreeParserEventScanError, CandidateFound]] = {
    if (zipEntries.hasMoreElements) {
      val zipEntry: ZipArchiveEntry = zipEntries.nextElement()
      val internalFilePath = Paths.get(zipEntry.getName)
      val candidateParsers = if (!zipEntry.isDirectory && !zipEntry.isUnixSymlink) {
        val zIn = zipFile.getInputStream(zipEntry)
        try {
          scanInputStream(parserCollection, zIn, internalFilePath.toString)
        } catch {
          case NonFatal(e) => return Some(Left(TreeParserEventScanError(request, internalFilePath, e)))
        } finally {
          zIn.close()
        }
      } else {
        Seq[CandidateParser]()
      }
      generateRequest(request, internalFilePath, candidateParsers) match {
        case Some(x) => Some(Right(x))
        case None => findNextParsingCandidate()
      }
    } else {
      zipFile.close()
      None
    }
  }
}
