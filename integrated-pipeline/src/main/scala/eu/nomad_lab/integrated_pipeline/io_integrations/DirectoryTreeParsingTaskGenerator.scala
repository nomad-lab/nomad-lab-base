package eu.nomad_lab.integrated_pipeline.io_integrations

import java.io.FileInputStream
import java.nio.file.Files

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.{ FileTree, ParsingTaskGenerator }
import eu.nomad_lab.integrated_pipeline.messages.{ CandidateFound, TreeParserEventScanError }
import eu.nomad_lab.parsers.{ CandidateParser, ParserCollection }

import scala.annotation.tailrec
import scala.collection.JavaConverters._
import scala.util.control.NonFatal

class DirectoryTreeParsingTaskGenerator(request: FileTree, parserCollection: ParserCollection)
    extends ParsingTaskGenerator {

  require(request.treeType == TreeType.Directory, "file tree to process must be a directory")
  private val basePath = request.treeBasePath
  private val fileIterator = Files.walk(basePath).iterator().asScala.filter(Files.isRegularFile(_))

  @tailrec final protected[this] override def findNextParsingCandidate(): Option[Either[TreeParserEventScanError, CandidateFound]] = {
    if (fileIterator.hasNext) {
      val file = fileIterator.next()
      val internalFilePath = basePath.relativize(file)
      val candidateParsers = if (!file.toFile.isDirectory) { //TODO: filter symylinks
        scala.io.Source.fromFile(file.toFile)
        val in = new FileInputStream(file.toFile)
        try {
          scanInputStream(parserCollection, in, internalFilePath.toString)
        } catch {
          case NonFatal(e) => return Some(Left(TreeParserEventScanError(request, internalFilePath, e)))
        } finally {
          in.close()
        }
      } else {
        Seq[CandidateParser]()
      }
      generateRequest(request, internalFilePath, candidateParsers) match {
        case Some(x) => Some(Right(x))
        case None => findNextParsingCandidate()
      }
    } else {
      None
    }
  }
}
