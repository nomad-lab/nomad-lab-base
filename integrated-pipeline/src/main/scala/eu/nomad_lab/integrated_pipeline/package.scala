package eu.nomad_lab

package object integrated_pipeline {

  object OutputType extends Enumeration {
    type OutputType = Value
    val Json, HDF5, HDF5merged = Value
  }

  object OperationMode extends Enumeration {
    type OperationMode = Value
    val Console = Value
  }

  sealed abstract trait EventReporterId { val name: String }
  case class TreeScannerId(name: String, reference: TreeScanner) extends EventReporterId
  case class CalculationParserId(name: String, reference: CalculationParsingEngine) extends EventReporterId
  case class ResultWriterId(name: String, reference: ParsingResultsProcessingManager) extends EventReporterId

}
