package eu.nomad_lab.integrated_pipeline

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.messages.{ FileParsingSignal, TreeScanSignal }

class ArchiveCleanUp(zipHandler: RawDataArchiveHandler) extends MessageProcessor[FileParsingSignal, FileParsingSignal] {

  private var toEmit: Option[FileParsingSignal] = None

  override def processSignal(in: FileParsingSignal): Unit = {
    if (toEmit.isEmpty) {
      in.fileTree.treeType match {
        case TreeType.Directory => ()
        case TreeType.Zip => zipHandler.processFileParsingSignal(in)
      }
      toEmit = Some(in)
    } else {
      throw new IllegalStateException("must fetch all outbound signals before next inbound signal")
    }
  }

  override def hasSignalToEmit(): Boolean = toEmit.nonEmpty

  override def getNextSignalToEmit(): FileParsingSignal = {
    if (toEmit.nonEmpty) {
      val signal = toEmit.get
      toEmit = None
      signal
    } else {
      throw new IllegalStateException("no signal ready to be fetched")
    }
  }

  override def requiresMoreMessages: Boolean = false
}
