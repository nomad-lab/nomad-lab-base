package eu.nomad_lab.integrated_pipeline.messages

import java.nio.file.Path

import eu.nomad_lab.integrated_pipeline.FileTree
import eu.nomad_lab.parsers.ParseResult.ParseResult

/*These events are distinct from the messages passed between the different processing stage because
  they model both input and output of each stage. Also, they do not contain any heavy payload (like
  the in-memory results of a parsing task) and thus can be stored by EventListener implementations.
 */

sealed trait TreeParserEvent {
  def fileTree: FileTree
}

case class TreeParserEventStart(
  fileTree: FileTree
) extends TreeParserEvent

case class TreeParserEventCandidate(
  fileTree: FileTree,
  relativePath: Path,
  parser: String
) extends TreeParserEvent

case class TreeParserEventScanError(
  fileTree: FileTree,
  relativePath: Path,
  error: Throwable
) extends TreeParserEvent

case class TreeParserEventTreeFailure(
  fileTree: FileTree,
  error: Throwable
) extends TreeParserEvent

case class TreeParserEventEnd(
  fileTree: FileTree,
  numCandidates: Long
) extends TreeParserEvent

sealed trait CalculationParserEvent {
  def fileTree: FileTree
  def relativePath: Path
  def parser: String
}

case class CalculationParserEventStart(
  fileTree: FileTree,
  relativePath: Path,
  parser: String
) extends CalculationParserEvent

case class CalculationParserEventEnd(
  fileTree: FileTree,
  relativePath: Path,
  parser: String,
  result: ParseResult,
  error: Option[String]
) extends CalculationParserEvent

sealed trait ResultWriterEvent {
  def fileTree: FileTree
}

case class ResultWriterEventStart(
  fileTree: FileTree
) extends ResultWriterEvent

case class ResultWriterEventResult(
  fileTree: FileTree,
  relativePath: Path,
  parser: String,
  result: ParseResult,
  error: Option[String]
) extends ResultWriterEvent

case class ResultWriterEventEnd(
  fileTree: FileTree,
  numCalculations: Long,
  numParsingFailures: Long,
  outputLocation: Path
) extends ResultWriterEvent