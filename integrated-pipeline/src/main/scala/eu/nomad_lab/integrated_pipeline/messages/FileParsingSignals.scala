package eu.nomad_lab.integrated_pipeline.messages

import java.nio.file.Path

import eu.nomad_lab.integrated_pipeline.FileTree
import eu.nomad_lab.parsers.ParseResult.ParseResult
import eu.nomad_lab.parsers.{ FinishedParsingSession, ParseEvent, StartedParsingSession }
import eu.nomad_lab.{ CompactSha, TreeType }

sealed trait TreeScanSignal {
  val fileTree: FileTree
}

sealed trait FileParsingSignal {
  val fileTree: FileTree
}

/**
 * An candidate calculation which should be analyzed in detail by the given parser.
 * @param fileTree the file-tree scan request from which this parsing task was generated
 * @param relativePath path inside the given file tree
 * @param parserName name of the parser to use for processing the file
 * @param extractedPath the path to the temporarily extracted main file (if originating from an
 *                      archive)
 */
case class CandidateFound(
    fileTree: FileTree,
    relativePath: Path,
    parserName: String,
    extractedPath: Option[Path] = None
) extends TreeScanSignal {

  def mainFileUri: String = {
    fileTree.treeType match {
      case TreeType.Zip => s"nmd://${fileTree.archiveId}/${relativePath.toString}"
      case TreeType.Directory => fileTree.treeBasePath.resolve(relativePath).toAbsolutePath.toUri.toString
      case _ => "unknown://${treeTask.treeBasePath.resolve(relativePath).toAbsolutePath.toString}"
    }
  }

  def calculationGid: String = {
    val checksum = CompactSha()
    checksum.updateStr(mainFileUri)
    checksum.gidStr("C")
  }

}

case class TreeScanCompleted(
  fileTree: FileTree,
  numParsingTasks: Long = 0
) extends TreeScanSignal with FileParsingSignal

sealed trait FileParsingResult extends FileParsingSignal {
  val result: ParseResult
  val task: CandidateFound
  val start: Option[StartedParsingSession]
  val end: Option[FinishedParsingSession]
  val error: Option[String]

  val fileTree: FileTree = task.fileTree
}

case class ParsingResultInMemory(
  task: CandidateFound,
  result: ParseResult,
  start: Option[StartedParsingSession],
  events: Seq[ParseEvent],
  end: Option[FinishedParsingSession],
  error: Option[String]
) extends FileParsingResult
