package eu.nomad_lab.integrated_pipeline.messages

import java.nio.file.Path

import eu.nomad_lab.integrated_pipeline.{ FileTree, OutputType }

case class FileTreeParsingResult(
  fileTree: FileTree,
  numCalculationsFound: Long,
  numParsingFailures: Long,
  outputLocation: Path,
  outputFormat: OutputType.OutputType
)
