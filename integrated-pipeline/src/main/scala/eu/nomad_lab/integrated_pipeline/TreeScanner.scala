package eu.nomad_lab.integrated_pipeline

import eu.nomad_lab.integrated_pipeline.messages._

import scala.annotation.tailrec

/**
 * A TreeScanner examines ("scans") all files inside a file tree (e.g. a directory or a
 * zip-archive) for files that are potential calculations of interest for NOMAD and emits file
 * parsing requests for these candidate files.
 */
trait TreeScanner extends MessageProcessor[FileTree, TreeScanSignal] {

  protected[this] def createProcessor(fileTree: FileTree): ParsingTaskGenerator
  protected[this] val eventListener: EventListener
  protected[this] val eventReporterName: Option[String] = None

  private lazy val myId = eventListener.registerReporter(this, eventReporterName)

  private var generator: Option[ParsingTaskGenerator] = None
  private var readyForInbound = true
  private var request: Option[FileTree] = None
  private var taskCount = 0l

  final override def processSignal(in: FileTree): Unit = {
    if (readyForInbound) {
      eventListener.processEvent(myId, TreeParserEventStart(in))
      if (in.treeBasePath.toFile.exists()) {
        generator = Some(createProcessor(in))
        request = Some(in)
        taskCount = 0
        readyForInbound = false
      } else {
        eventListener.processEvent(myId, TreeParserEventTreeFailure(
          in,
          new IllegalArgumentException(s"file tree root '${in.treeBasePath}' does not exist, ignoring task")
        ))
      }
    } else {
      throw new IllegalStateException("cannot process message, still digesting previous message")
    }
  }

  final override def hasSignalToEmit(): Boolean = !readyForInbound

  @tailrec final override def getNextSignalToEmit(): TreeScanSignal = {
    if (generator.exists(_.hasNext)) {
      generator.get.next() match {
        case Left(scanError) =>
          eventListener.processEvent(myId, scanError)
          getNextSignalToEmit()
        case Right(task) =>
          taskCount += 1
          eventListener.processEvent(
            myId,
            TreeParserEventCandidate(request.get, task.relativePath, task.parserName)
          )
          task
      }
    } else if (!readyForInbound) {
      readyForInbound = true
      eventListener.processEvent(myId, TreeParserEventEnd(request.get, taskCount))
      TreeScanCompleted(request.get, taskCount)
    } else {
      throw new IllegalStateException("no message ready for fetching")
    }
  }

  final override def requiresMoreMessages: Boolean = false
}