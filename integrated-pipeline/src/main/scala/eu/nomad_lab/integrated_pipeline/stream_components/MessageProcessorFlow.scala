package eu.nomad_lab.integrated_pipeline.stream_components

import akka.stream.stage.{ GraphStage, GraphStageLogic, InHandler, OutHandler }
import akka.stream.{ Attributes, FlowShape, Inlet, Outlet }
import eu.nomad_lab.integrated_pipeline.MessageProcessor

trait MessageProcessorFlow[Input, Output] extends GraphStage[FlowShape[Input, Output]] {

  val processor: MessageProcessor[Input, Output]
  val stageName: String

  val in: Inlet[Input] = Inlet(s"$stageName.in")
  val out: Outlet[Output] = Outlet(s"$stageName.out")
  override val shape = new FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {

    setHandler(out, new OutHandler {
      override def onPull(): Unit = {
        if (processor.hasSignalToEmit())
          push(out, processor.getNextSignalToEmit())
        else if (!isClosed(in))
          pull(in)
        else
          finishStage()
      }
    })

    setHandler(in, new InHandler {
      override def onPush(): Unit = {
        processor.processSignal(grab(in))
        if (processor.hasSignalToEmit())
          push(out, processor.getNextSignalToEmit())
        else if (!isClosed(in))
          pull(in)
        else
          finishStage()
      }

      override def onUpstreamFinish(): Unit = {
        if (!processor.hasSignalToEmit()) {
          finishStage()
        }
      }
    })

    private def finishStage(): Unit = {
      if (!processor.requiresMoreMessages)
        completeStage()
      else
        failStage(new IllegalStateException("finished stage with incomplete messages in processor"))
    }
  }

}
