package eu.nomad_lab.integrated_pipeline.stream_components

import akka.NotUsed
import akka.stream._
import akka.stream.scaladsl.{ Balance, Flow, GraphDSL, Merge }
import eu.nomad_lab.integrated_pipeline.CalculationParsingEngine
import eu.nomad_lab.integrated_pipeline.messages._

object CalculationParsingFlow {

  def createParsingFlow(engines: Seq[CalculationParsingEngine]): Flow[TreeScanSignal, FileParsingSignal, NotUsed] = {
    require(engines.nonEmpty, "must have at least one parsing engine")
    Flow.fromGraph(GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val parsers = engines.map(x => Flow.fromFunction(x.processSignal))
      val fanOut = builder.add(Balance[TreeScanSignal](engines.length))
      val fanIn = builder.add(Merge[FileParsingSignal](engines.length))

      parsers.foreach { worker => fanOut ~> worker.async ~> fanIn }
      FlowShape(fanOut.in, fanIn.out)
    })
  }

}