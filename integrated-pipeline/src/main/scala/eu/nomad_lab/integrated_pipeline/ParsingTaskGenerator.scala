package eu.nomad_lab.integrated_pipeline

import java.io.InputStream
import java.nio.file.Path
import java.util.NoSuchElementException

import eu.nomad_lab.integrated_pipeline.messages.{ CandidateFound, TreeParserEventScanError }
import eu.nomad_lab.parsers.{ CandidateParser, ParserCollection }

/**
 * contains methods shared by the different TreeParserLogic Implementations.
 * These are streams-adapted versions of the code found in the original TreeParser class.
 */
trait ParsingTaskGenerator extends Iterator[Either[TreeParserEventScanError, CandidateFound]] {

  protected[this] def findNextParsingCandidate(): Option[Either[TreeParserEventScanError, CandidateFound]]

  private var nextRequest: Option[Either[TreeParserEventScanError, CandidateFound]] = None
  private var searchStarted: Boolean = false

  def hasNext: Boolean = {
    if (!searchStarted) {
      nextRequest = findNextParsingCandidate()
      searchStarted = true
    }
    nextRequest.isDefined
  }

  def next(): Either[TreeParserEventScanError, CandidateFound] = {
    if (!searchStarted) {
      nextRequest = findNextParsingCandidate()
      searchStarted = true
    }
    val toReturn = nextRequest
    nextRequest = findNextParsingCandidate()
    if (toReturn.isDefined) {
      toReturn.get
    } else {
      throw new NoSuchElementException("No more elements in this iterator")
    }
  }

  def scanInputStream(parsers: ParserCollection, input: InputStream,
    internalFilePath: String): Seq[CandidateParser] = {
    val buf = Array.fill[Byte](8 * 1024)(0)
    val nRead = parsers.tryRead(input, buf)
    val minBuf = buf.dropRight(buf.length - nRead)
    parsers.scanFile(internalFilePath, minBuf).sorted
  }

  def generateRequest(fileTreeRequest: FileTree, relativeFilePath: Path,
    candidateParsers: Seq[CandidateParser]): Option[CandidateFound] = {
    require(!relativeFilePath.isAbsolute, "internal file paths must be relative to file tree root")

    if (candidateParsers.nonEmpty) {
      Some(CandidateFound(
        fileTree = fileTreeRequest,
        relativePath = relativeFilePath,
        parserName = candidateParsers.head.parserName
      ))
    } else {
      None
    }
  }

}
