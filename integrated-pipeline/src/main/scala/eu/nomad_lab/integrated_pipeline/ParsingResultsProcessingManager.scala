package eu.nomad_lab.integrated_pipeline

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.parsers.ParseResult

import scala.collection.mutable

trait ParsingResultsProcessingManager extends MessageProcessor[FileParsingSignal, FileTreeParsingResult] {

  protected[this] val processor: ParsingResultsProcessor
  protected[this] val eventListener: EventListener
  protected[this] val eventReporterName: Option[String] = None

  private lazy val myId = eventListener.registerReporter(this, eventReporterName)

  private val expectedCounts: mutable.Map[Path, Long] = mutable.Map()
  private val processedCounts: mutable.Map[Path, Long] = mutable.Map()
  private val failedCounts: mutable.Map[Path, Long] = mutable.Map()
  private var nextResult: Option[FileTreeParsingResult] = None

  override def requiresMoreMessages: Boolean = processedCounts.nonEmpty

  override def processSignal(signal: FileParsingSignal): Unit = {
    if (nextResult.nonEmpty)
      throw new IllegalStateException("previously generated signal not fetched!")
    signal match {
      case signal: TreeScanCompleted =>
        if (!processedCounts.contains(signal.fileTree.treeBasePath))
          startFileTree(signal.fileTree)
        expectedCounts(signal.fileTree.treeBasePath) = signal.numParsingTasks
      case signal: FileParsingResult =>
        if (!processedCounts.contains(signal.fileTree.treeBasePath))
          startFileTree(signal.fileTree)
        eventListener.processEvent(myId, ResultWriterEventResult(
          fileTree = signal.fileTree,
          relativePath = signal.task.relativePath,
          parser = signal.task.parserName,
          result = signal.result,
          error = signal.error
        ))
        processor.processFileParsingResult(signal)
        processedCounts(signal.fileTree.treeBasePath) += 1
        if (signal.result == ParseResult.ParseFailure)
          failedCounts(signal.fileTree.treeBasePath) += 1
    }
    val path = signal.fileTree.treeBasePath
    if (expectedCounts.getOrElse(path, -1) == processedCounts.getOrElse(path, -2)) {
      processor.finishProcessingTreeResults(signal.fileTree)
      val numCalculations = expectedCounts.remove(path).get
      processedCounts.remove(path)
      val numFailures = failedCounts.remove(path).get
      eventListener.processEvent(myId, ResultWriterEventEnd(
        fileTree = signal.fileTree,
        numCalculations = numCalculations,
        numParsingFailures = numFailures,
        outputLocation = processor.outputLocation(signal.fileTree)
      ))
      nextResult = Some(FileTreeParsingResult(
        fileTree = signal.fileTree,
        numCalculationsFound = numCalculations,
        numParsingFailures = numFailures,
        outputLocation = processor.outputLocation(signal.fileTree),
        outputFormat = processor.outputType
      ))
    }

  }

  override def hasSignalToEmit(): Boolean = nextResult.nonEmpty

  override def getNextSignalToEmit(): FileTreeParsingResult = {
    if (nextResult.isEmpty)
      throw new IllegalStateException("no result ready to emit")
    else {
      val response = nextResult.get
      nextResult = None
      response
    }
  }

  private def startFileTree(fileTree: FileTree): Unit = {
    processedCounts(fileTree.treeBasePath) = 0
    failedCounts(fileTree.treeBasePath) = 0
    eventListener.processEvent(myId, ResultWriterEventStart(fileTree))
    processor.startProcessingTreeResults(fileTree)
  }
}