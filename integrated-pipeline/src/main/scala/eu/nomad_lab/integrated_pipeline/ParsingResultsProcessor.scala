package eu.nomad_lab.integrated_pipeline

import java.nio.file.Path

import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline.messages.FileParsingResult

trait ParsingResultsProcessor {

  def processFileParsingResult(result: FileParsingResult): Unit

  val outputType: OutputType

  /**
   * obtain the folder where the result file(s) for the given file tree will be written
   * @param fileTree the processed file tree
   * @return the folder where the generated results are located
   */
  def outputLocation(fileTree: FileTree): Path

  def startProcessingTreeResults(fileTree: FileTree): Unit

  def finishProcessingTreeResults(fileTree: FileTree): Unit

}
