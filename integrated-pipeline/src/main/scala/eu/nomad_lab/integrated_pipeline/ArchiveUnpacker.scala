package eu.nomad_lab.integrated_pipeline

import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.messages.{ CandidateFound, TreeScanCompleted, TreeScanSignal }

class ArchiveUnpacker(zipHandler: RawDataArchiveHandler) extends MessageProcessor[TreeScanSignal, TreeScanSignal] {

  private var toEmit: Option[TreeScanSignal] = None

  override def processSignal(in: TreeScanSignal): Unit = {
    if (toEmit.isEmpty) {
      toEmit = in.fileTree.treeType match {
        case TreeType.Directory => Some(in)
        case TreeType.Zip =>
          val extractedPath = zipHandler.processTreeScanSignal(in)
          in match {
            case signal: CandidateFound => Some(signal.copy(extractedPath = extractedPath))
            case signal: TreeScanCompleted => Some(signal)
          }
      }
    } else {
      throw new IllegalStateException("must fetch all outbound signals before next inbound signal")
    }
  }

  override def hasSignalToEmit(): Boolean = toEmit.nonEmpty

  override def getNextSignalToEmit(): TreeScanSignal = {
    if (toEmit.nonEmpty) {
      val signal = toEmit.get
      toEmit = None
      signal
    } else {
      throw new IllegalStateException("no signal ready to be fetched")
    }
  }

  override def requiresMoreMessages: Boolean = false
}
