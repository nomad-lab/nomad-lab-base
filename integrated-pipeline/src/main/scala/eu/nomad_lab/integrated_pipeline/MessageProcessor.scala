package eu.nomad_lab.integrated_pipeline

/**
 * A message processing interface, used for integration a generic integration with streaming
 * frameworks
 * @tparam In type of the incoming signals
 * @tparam Out type of the generated signals
 */
trait MessageProcessor[-In, +Out] {

  /**
   * Process the given input signal. A signal can only be processed once all ready to emit
   * signals have been fetched with [[getNextSignalToEmit()]].
   * @param in the signal to process
   */
  @throws[IllegalStateException]("if there are still results to be emitted first")
  def processSignal(in: In): Unit

  /**
   * Query if the given processing unit has a signal ready to be emitted
   * @return true if a signal is ready to be fetched, false otherwise
   */
  def hasSignalToEmit(): Boolean

  /**
   * Get the next signal to emit. Use [[hasSignalToEmit()]] to check if a signal can be fetched.
   * @return the next signal to emit
   */
  @throws[IllegalStateException]("if no signal is ready to be emitted")
  def getNextSignalToEmit(): Out

  /**
   * Check whether this processor expects more inbound messages before it can terminate cleanly.
   * @return true if this processor expects more messages at present
   */
  def requiresMoreMessages: Boolean

}
