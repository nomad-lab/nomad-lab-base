package eu.nomad_lab.integrated_pipeline
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.parsers.ParseResult
import org.apache.logging.log4j.scala.Logging
import org.apache.logging.log4j.{ Marker, MarkerManager }

object EventLogger {
  val baseMarker: Marker = MarkerManager.getMarker("ParsingManager")
  val parseResultMarker: Marker = MarkerManager.getMarker("ParsingResult").setParents(baseMarker)
  val parseSuccessMarker: Marker = MarkerManager.getMarker("ParsingSuccess").setParents(parseResultMarker)
  val parseSkippedMarker: Marker = MarkerManager.getMarker("ParsingSkipped").setParents(parseResultMarker)
  val parseFailureMarker: Marker = MarkerManager.getMarker("ParsingFailure").setParents(parseResultMarker)
}

/**
 * Write incoming events to a logfile with ScalaLogging and the underlying logging framework.
 * This trait stacks and can thus be combined with other EventListener-traits to create a custom
 * event logging class with multiple features.
 */
trait EventLogger extends EventListener with Logging {
  import EventLogger._

  override def processEvent(reporter: TreeScannerId, message: TreeParserEvent): Unit = {
    message match {
      case x: TreeParserEventStart => logger.info(
        baseMarker,
        s"[TreeParser ${reporter.name}] start processing file tree " +
          s"'${x.fileTree.treeBasePath}' (tree type ${x.fileTree.treeType})"
      )
      case x: TreeParserEventEnd => logger.info(
        baseMarker,
        s"[TreeParser ${reporter.name}] finished processing file tree " +
          s"'${x.fileTree.treeBasePath}', found ${x.numCandidates} candidate files " +
          s"(tree type ${x.fileTree.treeType})"
      )
      case x: TreeParserEventCandidate => logger.debug(
        baseMarker,
        s"[TreeParser ${reporter.name}] found candidate in file tree " +
          s"'${x.fileTree.treeBasePath}' at '${x.relativePath}' (parser '${x.parser}')"
      )
      case x: TreeParserEventScanError => logger.warn(
        baseMarker,
        s"[TreeParser ${reporter.name}] scanning failure in file tree " +
          s"'${x.fileTree.treeBasePath}' at '${x.relativePath}' (${x.error.toString})"
      )
      case x: TreeParserEventTreeFailure => logger.error(
        baseMarker,
        s"[TreeParser ${reporter.name}] encountered failure while processing file tree " +
          s"'${x.fileTree.treeBasePath}', aborting: ${x.error.toString}"
      )
    }
    super.processEvent(reporter, message)
  }

  override def processEvent(reporter: CalculationParserId, message: CalculationParserEvent): Unit = {
    message match {
      case x: CalculationParserEventStart => logger.info(
        baseMarker,
        s"[CalculationParser ${reporter.name}] start parsing file " +
          s"'${x.relativePath}' in file tree '${x.fileTree.treeBasePath}' (parser '${x.parser}')"
      )
      case x: CalculationParserEventEnd =>
        x.result match {
          case ParseResult.ParseSuccess => logger.info(
            parseSuccessMarker,
            s"[CalculationParser ${reporter.name}] finished parsing file '${x.relativePath}' in " +
              s"file tree '${x.fileTree.treeBasePath}' (parser '${x.parser}')"
          )
          case ParseResult.ParseSkipped => logger.info(
            parseSkippedMarker,
            s"[CalculationParser ${reporter.name}] skipped parsing file '${x.relativePath}' in " +
              s"file tree '${x.fileTree.treeBasePath}' (parser '${x.parser}')"
          )
          case ParseResult.ParseFailure => logger.warn(
            parseFailureMarker,
            s"[CalculationParser ${reporter.name}] failed parsing file '${x.relativePath}' in " +
              s"file tree '${x.fileTree.treeBasePath}': ${x.error.get} (parser '${x.parser}')"
          )
        }
    }
    super.processEvent(reporter, message)
  }

  override def processEvent(reporter: ResultWriterId, message: ResultWriterEvent): Unit = {
    message match {
      case x: ResultWriterEventStart => logger.info(
        baseMarker,
        s"[ResultWriter ${reporter.name}] started writing results for file tree " +
          s"'${x.fileTree.treeBasePath}'"
      )
      case x: ResultWriterEventResult => logger.debug(
        baseMarker,
        s"[ResultWriter ${reporter.name}] processes parsing results for file '${x.relativePath}' " +
          s"from file tree '${x.fileTree.treeBasePath}'"
      )
      case x: ResultWriterEventEnd => logger.info(
        baseMarker,
        s"[ResultWriter ${reporter.name}] finished writing results for file tree " +
          s"'${x.fileTree.treeBasePath}', output location: '${x.outputLocation}' " +
          s"(${x.numCalculations} calculations, ${x.numParsingFailures} parsing failures)"
      )
    }
    super.processEvent(reporter, message)
  }

}
