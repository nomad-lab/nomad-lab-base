/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import java.nio.file.{ Files, Path, Paths }

import akka.Done
import akka.actor._
import akka.stream.scaladsl.{ Flow, GraphDSL, RunnableGraph, Sink, Source }
import akka.stream.{ ActorMaterializer, ClosedShape }
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.integrated_pipeline.Main.PipelineSettings
import eu.nomad_lab.integrated_pipeline.OutputType.OutputType
import eu.nomad_lab.integrated_pipeline.io_integrations._
import eu.nomad_lab.integrated_pipeline.messages.{ FileParsingSignal, TreeScanSignal, FileTreeParsingResult }
import eu.nomad_lab.integrated_pipeline.stream_components._
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }
import eu.nomad_lab.parsers.AllParsers
import eu.nomad_lab.{ LocalEnv, TreeType }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Main extends StrictLogging {

  val metaInfo: MetaInfoEnv = KnownMetaInfoEnvs.all
  private val parsers = AllParsers.defaultParserCollection
  private val outputDirRe = """^--outputdir="(.+)"$""".r
  private val outputModeRe = "^--output=(.+)$".r
  private val numWorkersRe = "^--numWorkers=([0-9]+)$".r

  def main(args: Array[String]): Unit = mainWithFuture(args)

  def mainWithFuture(args: Array[String]): Future[Done] = {
    if (args.isEmpty) {
      //TODO: show usage
      Future.successful(Done)
    } else {
      val main = new Main()
      val verbose = args.contains("--noConfigDump")
      main.parseTreesGivenByCommandLine(getSettings(LocalEnv.defaultConfig(verbose), args))
    }
  }

  case class PipelineSettings(
    task: Option[FileTree],
    mode: OutputType,
    numWorkers: Int,
    treeType: TreeType,
    targetDirectory: Path
  )

  /**
   * Generate a settings object, the parameters are taken from the global configuration files and
   * can optionally be overwritten by the command-line configuration
   * @param config the NOMAD configuration to use as default values
   * @param commandLine the given command-line parameters
   * @return merged settings
   */
  def getSettings(config: Config, commandLine: Array[String]): PipelineSettings = {
    val (operation, treeType) = commandLine.head match {
      case "parseDirectory" => (OperationMode.Console, TreeType.Directory)
      case "parseRawDataArchive" => (OperationMode.Console, TreeType.Zip)
    }
    var settings = PipelineSettings(
      task = None,
      mode = OutputType.HDF5merged,
      numWorkers = config.getInt("nomad_lab.integrated_pipeline.numWorkers"),
      treeType = treeType,
      targetDirectory = Paths.get(config.getString("nomad_lab.integrated_pipeline.targetDirectory"))
    )
    var args = commandLine.tail
    while (args.nonEmpty) {
      val head = args.head
      args = args.tail
      settings = head match {
        case outputModeRe(mode) => settings.copy(mode = OutputType.withName(mode))
        case outputDirRe(dir) => settings.copy(targetDirectory = Paths.get(dir))
        case numWorkersRe(count) => settings.copy(numWorkers = count.toInt)
        case "--noConfigDump" => settings //already consumed by the calling function
        case x if args.isEmpty && operation == OperationMode.Console =>
          settings.copy(task = Some(FileTree(Paths.get(x).toAbsolutePath, treeType)))
        case _ => ??? //TODO: show usage instructions
      }
    }
    settings
  }
}

class Main {
  import Main.metaInfo

  implicit val system: ActorSystem = ActorSystem("Calculation-Actor-System")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  def parseTreesGivenByCommandLine(params: PipelineSettings): Future[Done] = {
    require(params.task.nonEmpty, "no task specified in console mode")
    val source = Source.single(params.task.get)
    val sinkSummaries: Sink[FileTreeParsingResult, Future[Done]] = Sink.ignore
    val tempExtracted = Files.createTempDirectory(
      "nomad-parsing-extracted-files",
      LocalEnv.directoryPermissionsAttributes
    )
    tempExtracted.toFile.deleteOnExit()

    val eventProcessor = new EventLogger {}

    val graph = RunnableGraph.fromGraph(
      GraphDSL.create(sinkSummaries) { implicit builder => (sink) =>
        import GraphDSL.Implicits._

        val treeParser = Flow.fromGraph(new MessageProcessorFlow[FileTree, TreeScanSignal] {
          override val stageName = "TreeParser"
          override val processor = new TreeScanner {
            override val eventListener = eventProcessor
            override def createProcessor(fileTree: FileTree) = params.treeType match {
              case TreeType.Zip => new ZipTreeParsingTaskGenerator(fileTree, Main.parsers)
              case TreeType.Directory => new DirectoryTreeParsingTaskGenerator(fileTree, Main.parsers)
            }
          }
        })

        val archiveHandler = new WholeZipArchiveHandler(new ArchiveHandler(tempExtracted))
        val unpacker = Flow.fromGraph(new MessageProcessorFlow[TreeScanSignal, TreeScanSignal] {
          override val stageName = "Zip-Archive-Unpacker"
          override val processor = new ArchiveUnpacker(archiveHandler)
        })
        val cleanUp = Flow.fromGraph(new MessageProcessorFlow[FileParsingSignal, FileParsingSignal] {
          override val stageName = "Zip-Archive-CleanUp"
          override val processor = new ArchiveCleanUp(archiveHandler)
        })
        val parsing = CalculationParsingFlow.createParsingFlow(
          (1 to params.numWorkers).map(i =>
            new CalculationParsingEngine(Main.parsers, metaInfo, eventProcessor, Some(f"Worker-$i%2d")))
        )

        val processor = Flow.fromGraph(new MessageProcessorFlow[FileParsingSignal, FileTreeParsingResult] {
          override val stageName = "ResultWriter"
          override val processor = new ParsingResultsProcessingManager {
            override val eventListener = eventProcessor
            override val processor: ParsingResultsProcessor = params.mode match {
              case OutputType.Json => new WriteToJsonResultsProcessor(params.targetDirectory, metaInfo)
              case OutputType.HDF5 => new WriteToHDF5ResultsProcessor(params.targetDirectory, metaInfo)
              case OutputType.HDF5merged => new WriteToHDF5MergedResultsProcessor(params.targetDirectory, metaInfo)
            }
          }
        })

        source ~> treeParser.async ~> unpacker.async ~> parsing.async ~> cleanUp.async ~> processor.async ~> sink
        ClosedShape
      }
    )

    val done = graph.run()
    done.foreach(_ => system.terminate())
    done
  }

}
