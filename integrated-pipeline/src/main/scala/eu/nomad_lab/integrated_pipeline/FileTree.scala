package eu.nomad_lab.integrated_pipeline

import java.nio.file.{ Path, Paths }

import eu.nomad_lab.TreeType

import scala.util.matching.Regex

object FileTree {
  val rawDataArchive: Regex = "(R[A-z0-9_-]{28}).zip".r
}

/**
 * abstract representation of a file tree
 * @param treeBasePath path to the root of the file-tree
 * @param treeType type of the file tree, e.g. zip or directory
 */
case class FileTree(
    treeBasePath: Path,
    treeType: TreeType.TreeType
) {
  /**
   * Generate the archive ID for the tree-task, this is not necessarily identical to the
   * filename of the parsing results.
   * @return the associated internal archive ID and optionally a warning
   * @throws UnsupportedOperationException if a zip-archive file tree doesn't match the Id pattern
   */
  def archiveId: String = treeType match {
    case TreeType.Directory => treeBasePath.getFileName.toString
    case TreeType.Zip => treeBasePath.getFileName.toString match {
      case FileTree.rawDataArchive(id) => id
      case path => throw new UnsupportedOperationException(s"$path is not a valid rawdata archive name")
    }
  }

  /**
   * The canonical file-name of the HDF5-file containing the parsing results of the tree scan task.
   * @return the canonical file name of the parsing results HDF5 and optionally a warning
   * @throws UnsupportedOperationException if a zip-archive file tree doesn't match the Id pattern
   */
  def fileName: Path = {
    val id = archiveId
    Paths.get(treeType match {
      case TreeType.Directory => s"$id.h5"
      case TreeType.Zip => s"S${id.substring(1)}.h5"
    })
  }

  /**
   * The prefix-folder for the results from this file tree. Usually used as a subfolder of the
   * general output location to avoid having a single giant folder with all results in it.
   * @return the prefix path associated with this file tree and optionally a warning
   * @throws UnsupportedOperationException if a zip-archive file tree doesn't match the Id pattern
   */
  def prefixFolder: Path = {
    val id = archiveId
    Paths.get(treeType match {
      case TreeType.Directory => id.substring(0, 3)
      case TreeType.Zip => s"R${id.substring(1, 3)}"
    })
  }
}
