package eu.nomad_lab.integrated_pipeline

import java.nio.file.Path

import eu.nomad_lab.integrated_pipeline.messages.{ FileParsingSignal, TreeScanSignal }

/**
 * Base trait for archive-handling IO modules. How exactly the archive is extracted, e.g. once the
 * whole archive or partial extractions for each task is the choice of the implementing class.
 *
 * Note that classes implementing this trait will be used in two different MessageProcessor
 * instances at the same time (unpacking and cleanup) to allow for sanity-checks of file deletion
 * tasks. Any internal state that is accessed by both functions must therefore be properly
 * synchronized.
 */
trait RawDataArchiveHandler {

  /**
   * Process the given TreeScanSignal and temporarily extract any files necessary for the parsing
   * procedure.
   * @param signal the inbound signal
   * @return the path to the extracted main file, if applicable
   */
  def processTreeScanSignal(signal: TreeScanSignal): Option[Path]

  /**
   * Process the given parsing signal and invoke clean-up procedures as necessary.
   * @param signal the inbound signal
   */
  def processFileParsingSignal(signal: FileParsingSignal): Unit
}
