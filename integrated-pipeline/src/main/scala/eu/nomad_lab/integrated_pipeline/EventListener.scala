package eu.nomad_lab.integrated_pipeline

import eu.nomad_lab.integrated_pipeline.messages.{ CalculationParserEvent, ResultWriterEvent, TreeParserEvent }

import scala.collection.parallel.mutable

trait EventListener {

  private final val registeredTreeParsers: mutable.ParMap[TreeScanner, TreeScannerId] = mutable.ParMap()
  private final val registeredCalculationParsers: mutable.ParMap[CalculationParsingEngine, CalculationParserId] = mutable.ParMap()
  private final val registeredResultWriters: mutable.ParMap[ParsingResultsProcessingManager, ResultWriterId] = mutable.ParMap()

  def registerReporter(reference: TreeScanner, reporterId: Option[String]): TreeScannerId = {
    if (registeredTreeParsers.contains(reference)) {
      throw new IllegalArgumentException("reference already registered as reporter")
    } else {
      val id = TreeScannerId(reporterId.getOrElse(reference.toString), reference)
      registeredTreeParsers.put(reference, id)
      id
    }
  }

  def registerReporter(reference: CalculationParsingEngine, reporterId: Option[String]): CalculationParserId = {
    if (registeredCalculationParsers.contains(reference)) {
      throw new IllegalArgumentException("reference already registered as reporter")
    } else {
      val id = CalculationParserId(reporterId.getOrElse(reference.toString), reference)
      registeredCalculationParsers.put(reference, id)
      id
    }
  }

  def registerReporter(reference: ParsingResultsProcessingManager, reporterId: Option[String]): ResultWriterId = {
    if (registeredResultWriters.contains(reference)) {
      throw new IllegalArgumentException("reference already registered as reporter")
    } else {
      val id = ResultWriterId(reporterId.getOrElse(reference.toString), reference)
      registeredResultWriters.put(reference, id)
      id
    }
  }

  def processEvent(reporter: TreeScannerId, message: TreeParserEvent): Unit = {}

  def processEvent(reporter: CalculationParserId, message: CalculationParserEvent): Unit = {}

  def processEvent(reporter: ResultWriterId, message: ResultWriterEvent): Unit = {}

}
