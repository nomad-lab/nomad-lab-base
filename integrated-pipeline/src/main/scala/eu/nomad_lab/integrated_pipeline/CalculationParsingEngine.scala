package eu.nomad_lab.integrated_pipeline

import java.nio.file.Path

import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.TreeType
import eu.nomad_lab.integrated_pipeline.messages._
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.parsers._

import scala.collection.mutable.ListBuffer

/**
 * Parsing engine that runs the actual parsing process for a single request. The information
 * extracted by the parser is stored as a list of parsing events. These parsing events can then be
 * fed into other backends, like an HDF5-writer.
 * TODO: event streams exceeding a size limit should be stored on disk to ensure that the memory
 * usage remains bounded
 * @param parsers the to be used parsers
 * @param metaInfo the NOMAD meta information the parser can extract
 * @param eventListener processor for event notifications (logging etc)
 * @param name name of this object when registering to the eventListener
 */
class CalculationParsingEngine(
    private val parsers: ParserCollection,
    private val metaInfo: MetaInfoEnv,
    private val eventListener: EventListener,
    private val name: Option[String]
) extends StrictLogging {

  val myId = eventListener.registerReporter(this, name)

  private def getParser(request: CandidateFound): Option[OptimizedParser] = {
    parsers.parsers.get(request.parserName).map(_.optimizedParser(Seq()))
  }

  /**
   * stores the events emitted by the real backend
   */
  private class BufferForBackend {
    val events = new ListBuffer[ParseEvent]
    var startEvent: Option[StartedParsingSession] = None
    var endEvent: Option[FinishedParsingSession] = None

    def handleStartAndEndParsing(event: ParseEvent): Unit = {
      event match {
        case ev: StartedParsingSession if startEvent.isEmpty => startEvent = Some(ev)
        case ev: FinishedParsingSession if endEvent.isEmpty => endEvent = Some(ev)
        case x => logger.error(s"received unexpected event $x")
      }
    }

    def handleParseEvents(event: ParseEvent): Unit = events.append(event)

  }

  /**
   * Process the given parsing task signal. Parsing tasks will trigger a parsing run on the
   * specified file and return the collected events. Other signals will be forwarded directly.
   * Exceptions thrown during parsing will be handled gracefully and indicated in the parsing
   * results.
   * @param task the original parsing task signal
   * @return the generated parsing results signal
   */
  def processSignal(task: TreeScanSignal): FileParsingSignal = {
    task match {
      case signal: TreeScanCompleted => signal
      case task: CandidateFound =>
        eventListener.processEvent(myId, CalculationParserEventStart(
          task.fileTree, task.relativePath, task.parserName
        ))
        val pathToMainFile = task.fileTree.treeType match {
          case TreeType.Directory => Right(task.fileTree.treeBasePath.resolve(task.relativePath))
          case TreeType.Zip => task.extractedPath match {
            case Some(path) => Right(path)
            case None => Left("extracted file path is missing in the request")
          }
          case x => Left(s"requests with file tree type $x are not supported")
        }
        (pathToMainFile, getParser(task)) match {
          case (Right(path), Some(parser)) => parseCalculation(task, parser, path)
          case (Left(error), _) => failParseRequest(task, error)
          case (_, None) => failParseRequest(task, s"parser ${task.parserName} is unknown")
        }
    }
  }

  /**
   * Create a failed parsing result for the given task with the specified reason.
   * @param request the original parsing request
   * @param reason explanation why the parsing has failed
   * @return a FileParsingResult representing the failure
   */
  private def failParseRequest(request: CandidateFound, reason: String): FileParsingResult = {
    eventListener.processEvent(myId, CalculationParserEventEnd(
      request.fileTree, request.relativePath, request.parserName, ParseResult.ParseFailure, Some(reason)
    ))
    ParsingResultInMemory(
      task = request,
      result = ParseResult.ParseFailure,
      start = None,
      events = Seq(),
      end = None,
      error = Some(reason)
    )
  }

  private def parseCalculation(request: CandidateFound, parser: OptimizedParser,
    pathToMainFile: Path): FileParsingResult = {
    val buffer = new BufferForBackend
    val backend = new ParseEventsEmitter(metaInfo, buffer.handleParseEvents,
      buffer.handleStartAndEndParsing)
    val result = SafeParsing.parse(parser, request.mainFileUri, pathToMainFile, backend, request.parserName)
    val error = result match {
      case ParseResult.ParseFailure => buffer.endEvent match {
        case Some(end) => end.parserErrors.children.headOption match {
          case Some(message) => Some(message.extract[String])
          case None => Some("no specific error message provided by parser")
        }
        case None => Some("parsing did not terminate cleanly")
      }
      case _ => None
    }
    eventListener.processEvent(myId, CalculationParserEventEnd(
      request.fileTree, request.relativePath, request.parserName, result, error
    ))
    ParsingResultInMemory(
      task = request,
      result = result,
      start = buffer.startEvent,
      events = buffer.events.result(),
      end = buffer.endEvent,
      error = error
    )
  }

}
