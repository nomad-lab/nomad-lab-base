#!/bin/bash

cd ..
base=${1-$HOME}

if ! [[ -e "$base/labEnv" ]] ; then
    virtualenv -p python "$base/labEnv"
fi
. $base/labEnv/bin/activate
cd ./python-common
git pull --rebase
pip install --upgrade pip
cat requirements.txt | xargs -n1 pip install --upgrade
cd ../3rd-party/phonopy
python setup.py build
python setup.py install
deactivate

if ! [[ -e "$base/labEnv3" ]] ; then
    virtualenv -p python "$base/labEnv3"
fi
. $base/labEnv3/bin/activate
cd ../../python-common
pip install --upgrade pip
cat requirements.txt | xargs -n1 pip install --upgrade
cd ../3rd-party/phonopy
python setup.py build
python setup.py install
deactivate
