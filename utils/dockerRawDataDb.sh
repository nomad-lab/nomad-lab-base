# stats db

mkdir -p /nomad/nomadlab/raw-data/dbdata

docker run -d --restart=unless-stopped -v /nomad/nomadlab/raw-data/dbdata:/var/lib/postgresql/data --name rawdatadb -e POSTGRES_USER=rawdata -e POSTGRES_PASSWORD='pippo' -p 5432:5432 postgres:9.5
