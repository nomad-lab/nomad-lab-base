#!/bin/bash
## run in /bin/bash
#SBATCH -J collect
## execute job from the current working directory
## this is default slurm behavior
#SBATCH -D ./
## do not send mail
#SBATCH --mem 110G
#SBATCH --mail-type=NONE
#SBATCH --nodes=1
#SBATCH -t 20:00:00
#SBATCH --qos=shm
#source /u/fawzi/bin/scala.sh
#module load git
#module load jdk/1.7
source /u/fawzi/.bashrc
#######################################################
#example SGE batch script for a plain MPI program using
#512 cores (16 nodes x 32 cores per node) with 32 MPI 
#tasks per node
#executed under the intel runtime 
#######################################################

#module load intel impi

#source /u/fawzi/bin/scala.sh
#module load git
#module load jdk/1.7
#source /u/fawzi/.bashrc

# 
#
#-Dnomad_lab.integrated_pipeline.inFile="001" 
SBT_OPTS="-Xms1024M -Xmx4096M -Xss8M -XX:MaxPermSize=512M"
CMD="java  -Dnomad_lab.configurationToUse=eos_prod -Deos_prod.nomad_lab.replacements.rootNamespace=prod-032 -Deos_prod.nomad_lab.parser_worker_rabbitmq.numberOfWorkers=1 -Djava.library.path=/u/fawzi/lib -Deos_prod.nomad_lab.parsing_stats.jdbcUrl=\"\" -Dnomad_lab.integrated_pipeline.treeparserMinutesOfRuntime=530 -Dnomad_lab.parser_worker_rabbitmq.rabbitMQHost=labdev-nomad.esc.rzg.mpg.de   -jar /u/fawzi/bin/nomadCalculationParserWorker-assembly-1.3.0-15-g406258c-dirty.jar"

#CMD=
cd /nomad/nomadlab/parsed/prod-1.11.4
for src in ../prod-1.11.4-logs/remerge-a[a-d].todo; do
    (
    for r in $( cat $src ) ; do
        echo $r
        if [ -e "../prod-1.11.4s/S${r:1:2}/S${r:1}.h5.lock" ] ; then
            rm "../prod-1.11.4s/S${r:1:2}/S${r:1}.h5.lock"
        fi
        mkdir -p ../prod-1.11.4s/S${r:1:2}
        java -Djava.library.path=/u/fawzi/lib -jar /u/fawzi/jars/nomadTool-assembly-1.10.1-510-g8b6a2aa5-dirty.jar merge --merge-h5 ../prod-1.11.4s/S${r:1:2}/S${r:1}.h5 */$r/*.h5 &> $src.log
     done
     ) &
done
wait
