#!/bin/bash
# prints hdf files which have a lock file that marks them as unclosed.
# Useful to find crashed, potentially corrupted files for example with
#   find . -name \*.h5.lock -exec ./unclosed.sh '{}'  ';' > unclosed.txt
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if diff -s "$1" "$DIR/empty.lock" >& /dev/null ; then
 echo $1
fi
