#!/bin/bash

cd /nomad/nomadlab/user-data/shared/tutorialsNew/demos
git pull --rebase
/labEnv3/bin/python collect.py > ~/collected-demos.json
cp ~/collected-demos.json /nomad/nomadlab/servers/labtest-nomad/frontend-nginx/www-root/userapi/demos/index.json
