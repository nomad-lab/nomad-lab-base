# on analytics-toolkit

docker run -d \
 -v /u/ankar/certi/labtest:/certs:ro \
 -v /root/nomad-coe:/etc/certs:ro \
 -v /nomad/nomadlab/beaker-notebooks/labtest-prod/nginx.conf:/etc/nginx/nginx.conf:ro \
 -v /nomad/nomadlab/beaker-notebooks/labtest-prod/www-root:/usr/share/nginx/html:ro \
 -v /nomad/nomadlab/beaker-notebooks/labtest-prod/client_temp:/etc/nginx/client_temp \
 -p 80:80 -p 443:443 -p 5509:5509 --name frontendNginx2 nginx

# on labdev

docker run -d \
 --restart=unless-stopped \
 -v /u/ankar/certi:/certs:ro \
 -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/nginx.conf:/etc/nginx/nginx.conf:ro \
 -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/www-root:/usr/share/nginx/html:ro \
 -p 80:80 -p 443:443 -p 5509:5509 --name frontendNginx nginx
