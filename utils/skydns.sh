#!/bin/bash

# A script to setup the skydns for kubernetes cluster in docker containers.
# Authors @kariryaa

set -e

# Make sure docker daemon is running
if ( ! ps -ef | grep "/usr/bin/docker" | grep -v 'grep' &> /dev/null ); then
    echo "Docker is not running on this machine!"
    exit 1
fi

# Make sure skydns env is properly set
DNS_REPLICAS=${DNS_REPLICAS:-"1"}
DNS_DOMAIN=${DNS_DOMAIN:-"cluster.local"}
DNS_SERVER_IP=${DNS_SERVER_IP:-"10.0.0.10"}

echo "DNS_REPLICAS is set to: ${DNS_REPLICAS}"
echo "DNS_DOMAIN is set to: ${DNS_DOMAIN}"
echo "DNS_SERVER_IP is set to: ${DNS_SERVER_IP}"


start_skydns(){
    wget http://kubernetes.io/docs/getting-started-guides/docker-multinode/skydns.yaml.in
    sed -e "s/{{ pillar\['dns_replicas'\] }}/${DNS_REPLICAS}/g;s/{{ pillar\['dns_domain'\] }}/${DNS_DOMAIN}/g;s/{{ pillar\['dns_server'\] }}/${DNS_SERVER_IP}/g" skydns.yaml.in > ./skydns.yaml

    echo "$(kubectl get ns)"
    wget https://raw.githubusercontent.com/kubernetes/kubernetes/master/docs/getting-started-guides/docker-multinode/kube-system.yaml
    # Create kube-system namespace
    kubectl create -f ./kube-system.yaml
    # Create skydns  service and pod
    kubectl create -f ./skydns.yaml
}

echo "Starting dns ..."
start_skydns

echo "Master done!"