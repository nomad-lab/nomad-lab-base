#!/usr/bin/env bash
# Starts a tree parser

docker run -d -e NOMAD_ENV=labdev --user $UID:$(getent group docker | cut -d: -f3) --volumes-from parserVolContainer eu.nomad-laboratory/nomadtreeparserworker
