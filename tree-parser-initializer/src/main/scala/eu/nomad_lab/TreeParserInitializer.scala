/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import java.nio.file.Paths

import com.typesafe.config.{ ConfigFactory, Config }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.TreeParserRequest
import eu.nomad_lab.QueueMessage.NormalizerRequest
import eu.nomad_lab.parsing_queue.TreeParser
import org.json4s.JsonAST.JValue
import scala.collection.mutable
import scala.io.Source

import com.rabbitmq.client._

object TreeParserInitilaizer extends StrictLogging {

  /**
   * The settings required to get the read and write queue
   */
  class Settings(config: Config, queueToInitialize: String) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")
    val rabbitMQHost = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQHost")
    val rabbitMQPort = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQPort")
    val (writeToExchange, writeQueue) = queueToInitialize match {
      case "treeParserInitializer" => (
        config.getString("nomad_lab.parser_worker_rabbitmq.treeParserExchange"),
        config.getString("nomad_lab.parser_worker_rabbitmq.treeParserQueue")
      )
      case "toBeNormalizedInitializer" => (
        config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedExchange"),
        config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedQueue")
      )
      case _ => throw new Exception(s"Queue name not recognized: $queueToInitialize")
    }
    def toJValue: JValue = {
      import org.json4s.JsonDSL._
      (("rabbitMQHost" -> rabbitMQHost) ~
        ("writeToExchange" -> writeToExchange))
    }
  }

  //  val settings = new Settings(LocalEnv.defaultConfig, queueToInitialize)
  val parserCollection = parsers.AllParsers.defaultParserCollection
  val treeParser = new TreeParser(
    parserCollection = parserCollection
  )

  val usage = """treeparserinitializer [--help] [--verbose]
       [--file file1 [--file file2 ...]]
       [pathToArchive1 pathToArchive2...]"""

  /**
   * Main function of TreeParserInitilaizer
   *
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val filesToScan = mutable.ListBuffer[String]()
    var list: List[String] = args.toList
    var queueToInitialize: String = "treeParserInitializer"
    var verbose: Boolean = false
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-help" | "-h" =>
          println(usage)
          return
        case "--verbose" =>
          verbose = true
        case "--queue" =>
          if (list.isEmpty) {
            println(s"Error: expected a treeParserInitializer or toBeNormalizedInitializer after --queue, $usage")
          }
          queueToInitialize = list.head
          list = list.tail
        case "--file" =>
          if (list.isEmpty) {
            println(s"Error: expected a file after --file, $usage")
            return
          }
          filesToScan ++= Source.fromFile(list.head).getLines
          list = list.tail
        case _ =>
          filesToScan += arg
      }
    }
    writeToQueue(filesToScan, queueToInitialize)
  }

  /**
   * Find the parsable files and parsers. Write this information for the single step parser
   */
  def writeToQueue(args: Seq[String], queueToInitialize: String) = {
    val settings = new Settings(LocalEnv.defaultConfig, queueToInitialize)

    val prodFactory: ConnectionFactory = new ConnectionFactory
    prodFactory.setHost(settings.rabbitMQHost)
    prodFactory.setPort(settings.rabbitMQPort.toInt)
    val prodConnection: Connection = prodFactory.newConnection
    val prodchannel: Channel = prodConnection.createChannel
    prodchannel.exchangeDeclare(settings.writeToExchange, "fanout")
    prodchannel.queueDeclare(settings.writeQueue, true, false, false, null)
    prodchannel.queueBind(settings.writeQueue, settings.writeToExchange, "")
    for (filePath <- args) {
      val message = queueToInitialize match {
        case "treeParserInitializer" => TreeParserRequest.fromPath(Paths.get(filePath))
        case "toBeNormalizedInitializer" => NormalizerRequest.fromPath(Paths.get(filePath))
      }
      val msgBytes = JsonSupport.writeUtf8(message)
      logger.info(s"Message: $message, bytes Array size: ${msgBytes.length}")
      try {
        prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
      } catch {
        case e: Exception =>
          logger.error(s"Error while publishing to the exchange.Message: ${e.getMessage}")
          System.exit(1)
      }
    }
    logger.info(s"Wrote to Exchange: ${settings.writeToExchange}")
    prodchannel.close
    prodConnection.close
  }
}
