-- alter table upload
--   drop column upload_name
--   drop author_id;

alter table upload
  add column upload_name varchar(80) not null,
  add column author_id integer,
  add constraint u_upload_upload unique (upload_name),
  add constraint fk_upload_author foreign key (author_id) references author (author_id);
