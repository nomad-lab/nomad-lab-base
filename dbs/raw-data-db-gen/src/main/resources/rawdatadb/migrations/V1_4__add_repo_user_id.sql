-- alter table upload
--   drop column repo_user_id,
--   drop column last_user_change;

alter table author
  add column repo_user_id integer,
  add column last_user_change timestamp with time zone not null,
  add constraint u_author_repo_user_id unique (repo_user_id);
