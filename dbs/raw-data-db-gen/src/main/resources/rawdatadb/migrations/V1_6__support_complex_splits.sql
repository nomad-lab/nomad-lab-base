-- alter table upload
--   drop column repo_username;

create sequence chunk_piece_id_seq;
create table chunk_piece (
       chunk_piece_id integer not null default nextval('chunk_piece_id_seq'),
       upload_chunk_id integer not null,
       name varchar(128),
       constraint pk_chunk_piece primary key (chunk_piece_id),
       constraint fk_chunk_piece_upload_chunk foreign key (upload_chunk_id) references upload_chunk (upload_chunk_id)
);

