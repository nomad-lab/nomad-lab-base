-- drop table calculation ;
-- drop sequence calculation_id_seq;
-- drop table raw_data ;
-- drop sequence raw_data_id_seq;
-- drop table upload_chunk ;
-- drop sequence upload_chunk_id_seq;
-- drop table author_author_group ;
-- drop table author ;
-- drop sequence author_id_seq;
-- drop table author_group ;
-- drop sequence author_group_id_seq;
-- drop table citation_citation_group ;
-- drop table citation_group ;
-- drop sequence citation_group_id_seq;
-- drop table citation ;
-- drop sequence citation_id_seq;
-- drop table upload ;
-- drop sequence upload_id_seq;

create sequence upload_id_seq;
create table upload (
       upload_id integer not null default nextval('upload_id_seq'),
       upload_path varchar(128),
       upload_size integer,
       upload_date timestamp with time zone,
       publication_date timestamp with time zone,
       is_large boolean,
       processed_date timestamp with time zone,
       is_processed boolean not null default false,
       constraint pk_upload primary key (upload_id)
);

create sequence citation_id_seq;
create table citation (
       citation_id integer not null default nextval('citation_id_seq'),
       citation_text text not null,
       constraint pk_citation primary key (citation_id)
);

create sequence citation_group_id_seq;
create table citation_group (
       citation_group_id integer not null default nextval('citation_group_id_seq'),
       checksum varchar(30),
       constraint pk_citation_group primary key (citation_group_id),
       constraint u_citation_group_checksum unique (checksum)
);

create table citation_citation_group (
       citation_id integer not null,
       citation_group_id integer not null,
       constraint pk_citation_citation_group primary key (citation_group_id, citation_id),
       constraint fk_citation_citation_group_citation_id foreign key (citation_id) references citation (citation_id),
       constraint fk_citation_citation_group_citation_group_id foreign key (citation_group_id) references citation_group (citation_group_id)
);

create sequence author_group_id_seq;
create table author_group (
       author_group_id integer not null default nextval('author_group_id_seq'),
       authors_string text not null,
       constraint pk_author_group primary key (author_group_id),
       constraint u_author_group_authors_string unique (authors_string)
);

create sequence author_id_seq;
create table author (
       author_id integer not null default nextval('author_id_seq'),
       first_name varchar(40),
       last_name varchar(40),
       username varchar(40),
       affiliation text,
       constraint pk_author primary key (author_id),
       constraint u_author_username unique (username)
);

create table author_author_group (
       author_id integer not null,
       author_group_id integer not null,
       author_seq integer,
       constraint pk_author_author_group primary key (author_group_id, author_id),
       constraint fk_author_author_group_author_id foreign key (author_id) references author (author_id),
       constraint fk_author_author_group_author_group_id foreign key (author_group_id) references author_group (author_group_id)
);

create sequence upload_chunk_id_seq;
create table upload_chunk (
       upload_chunk_id integer not null default nextval('upload_chunk_id_seq'),
       upload_id integer not null,
       relative_path varchar(128),
       processed_date timestamp with time zone,
       is_processed boolean not null default false,
       author_group_id integer,
       citation_group_id integer,
       last_author_change timestamp with time zone,
       last_citation_change timestamp with time zone,
       constraint pk_upload_chunk primary key (upload_chunk_id),
       constraint fk_upload_chunk_upload_id foreign key (upload_id) references upload (upload_id),
       constraint fk_upload_chunk_author_group_id foreign key (author_group_id) references author_group (author_group_id),
       constraint fk_upload_chunk_citation_group_id foreign key (citation_group_id) references citation_group (citation_group_id)
);

create sequence raw_data_id_seq;
create table raw_data (
       raw_data_id integer not null default nextval('raw_data_id_seq'),
       raw_data_gid varchar(30),
       owner varchar(128),
       base_path varchar(128),
       upload_chunk_id integer,
       assimilation_date timestamp with time zone,
       author_group_id integer,
       citation_group_id integer,
       last_author_change timestamp with time zone,
       last_citation_change timestamp with time zone,
       constraint pk_raw_data primary key(raw_data_id),
       constraint u_raw_data_raw_data_gid unique (raw_data_gid),
       constraint fk_raw_data_author_group foreign key (author_group_id) references author_group (author_group_id),
       constraint fk_raw_data_upload_chunk_id foreign key (upload_chunk_id) references upload_chunk (upload_chunk_id),
       constraint fk_raw_data_citation_group_id foreign key (citation_group_id) references citation_group (citation_group_id)
);

create sequence calculation_id_seq;
create table calculation (
       calculation_id bigint not null default nextval('calculation_id_seq'),
       calculation_gid varchar(30),
       raw_data_id integer not null,
       repository_checksum varchar(60),
       path_from_base varchar(128),
       constraint pk_calculation primary key (calculation_id),
       constraint fk_calculation_raw_data foreign key(raw_data_id) references raw_data (raw_data_id),
       constraint u_calculation_calculation_gid unique (calculation_gid)
);
