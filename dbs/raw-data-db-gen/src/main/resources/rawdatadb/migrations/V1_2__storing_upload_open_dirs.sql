-- alter table upload
--   drop column open_directories,
--   drop column open_directories_change;

alter table upload
  add column open_directories text not null default '',
  add column open_directories_change timestamp with time zone;
