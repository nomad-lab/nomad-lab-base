/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables;


import eu.nomad_lab.repo.db.DefaultSchema;
import eu.nomad_lab.repo.db.Keys;
import eu.nomad_lab.repo.db.tables.records.SpacegroupsRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Spacegroups extends TableImpl<SpacegroupsRecord> {

    private static final long serialVersionUID = 1569025141;

    /**
     * The reference instance of <code>spacegroups</code>
     */
    public static final Spacegroups SPACEGROUPS = new Spacegroups();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<SpacegroupsRecord> getRecordType() {
        return SpacegroupsRecord.class;
    }

    /**
     * The column <code>spacegroups.n</code>.
     */
    public final TableField<SpacegroupsRecord, Integer> N = createField("n", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>spacegroups.calc_id</code>.
     */
    public final TableField<SpacegroupsRecord, Integer> CALC_ID = createField("calc_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>spacegroups</code> table reference
     */
    public Spacegroups() {
        this("spacegroups", null);
    }

    /**
     * Create an aliased <code>spacegroups</code> table reference
     */
    public Spacegroups(String alias) {
        this(alias, SPACEGROUPS);
    }

    private Spacegroups(String alias, Table<SpacegroupsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Spacegroups(String alias, Table<SpacegroupsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<SpacegroupsRecord> getPrimaryKey() {
        return Keys.SPACEGROUPS_PK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<SpacegroupsRecord>> getKeys() {
        return Arrays.<UniqueKey<SpacegroupsRecord>>asList(Keys.SPACEGROUPS_PK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<SpacegroupsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<SpacegroupsRecord, ?>>asList(Keys.SPACEGROUPS__SPACEGROUPS_CALC_ID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Spacegroups as(String alias) {
        return new Spacegroups(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Spacegroups rename(String name) {
        return new Spacegroups(name, null);
    }
}
