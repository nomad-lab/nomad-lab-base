/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables.records;


import eu.nomad_lab.repo.db.tables.Energies;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EnergiesRecord extends UpdatableRecordImpl<EnergiesRecord> implements Record3<byte[], Double, Integer> {

    private static final long serialVersionUID = -1012322684;

    /**
     * Setter for <code>energies.convergence</code>.
     */
    public void setConvergence(byte... value) {
        set(0, value);
    }

    /**
     * Getter for <code>energies.convergence</code>.
     */
    public byte[] getConvergence() {
        return (byte[]) get(0);
    }

    /**
     * Setter for <code>energies.total</code>.
     */
    public void setTotal(Double value) {
        set(1, value);
    }

    /**
     * Getter for <code>energies.total</code>.
     */
    public Double getTotal() {
        return (Double) get(1);
    }

    /**
     * Setter for <code>energies.calc_id</code>.
     */
    public void setCalcId(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>energies.calc_id</code>.
     */
    public Integer getCalcId() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<byte[], Double, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<byte[], Double, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<byte[]> field1() {
        return Energies.ENERGIES.CONVERGENCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Double> field2() {
        return Energies.ENERGIES.TOTAL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return Energies.ENERGIES.CALC_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] value1() {
        return getConvergence();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double value2() {
        return getTotal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getCalcId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnergiesRecord value1(byte... value) {
        setConvergence(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnergiesRecord value2(Double value) {
        setTotal(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnergiesRecord value3(Integer value) {
        setCalcId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnergiesRecord values(byte[] value1, Double value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached EnergiesRecord
     */
    public EnergiesRecord() {
        super(Energies.ENERGIES);
    }

    /**
     * Create a detached, initialised EnergiesRecord
     */
    public EnergiesRecord(byte[] convergence, Double total, Integer calcId) {
        super(Energies.ENERGIES);

        set(0, convergence);
        set(1, total);
        set(2, calcId);
    }
}
