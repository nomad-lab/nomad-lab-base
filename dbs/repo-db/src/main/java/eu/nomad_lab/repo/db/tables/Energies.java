/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables;


import eu.nomad_lab.repo.db.DefaultSchema;
import eu.nomad_lab.repo.db.Keys;
import eu.nomad_lab.repo.db.tables.records.EnergiesRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Energies extends TableImpl<EnergiesRecord> {

    private static final long serialVersionUID = 1326150628;

    /**
     * The reference instance of <code>energies</code>
     */
    public static final Energies ENERGIES = new Energies();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<EnergiesRecord> getRecordType() {
        return EnergiesRecord.class;
    }

    /**
     * The column <code>energies.convergence</code>.
     */
    public final TableField<EnergiesRecord, byte[]> CONVERGENCE = createField("convergence", org.jooq.impl.SQLDataType.BLOB, this, "");

    /**
     * The column <code>energies.total</code>.
     */
    public final TableField<EnergiesRecord, Double> TOTAL = createField("total", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>energies.calc_id</code>.
     */
    public final TableField<EnergiesRecord, Integer> CALC_ID = createField("calc_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>energies</code> table reference
     */
    public Energies() {
        this("energies", null);
    }

    /**
     * Create an aliased <code>energies</code> table reference
     */
    public Energies(String alias) {
        this(alias, ENERGIES);
    }

    private Energies(String alias, Table<EnergiesRecord> aliased) {
        this(alias, aliased, null);
    }

    private Energies(String alias, Table<EnergiesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<EnergiesRecord> getPrimaryKey() {
        return Keys.ENERGIES_PK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<EnergiesRecord>> getKeys() {
        return Arrays.<UniqueKey<EnergiesRecord>>asList(Keys.ENERGIES_PK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<EnergiesRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<EnergiesRecord, ?>>asList(Keys.ENERGIES__ENERGIES_CALC_ID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Energies as(String alias) {
        return new Energies(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Energies rename(String name) {
        return new Energies(name, null);
    }
}
