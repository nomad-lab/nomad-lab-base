/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables;


import eu.nomad_lab.repo.db.DefaultSchema;
import eu.nomad_lab.repo.db.Keys;
import eu.nomad_lab.repo.db.tables.records.StructuresRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Structures extends TableImpl<StructuresRecord> {

    private static final long serialVersionUID = -1189452552;

    /**
     * The reference instance of <code>structures</code>
     */
    public static final Structures STRUCTURES = new Structures();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<StructuresRecord> getRecordType() {
        return StructuresRecord.class;
    }

    /**
     * The column <code>structures.struct_id</code>.
     */
    public final TableField<StructuresRecord, Integer> STRUCT_ID = createField("struct_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('structures_struct_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>structures.step</code>.
     */
    public final TableField<StructuresRecord, Integer> STEP = createField("step", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>structures.final</code>.
     */
    public final TableField<StructuresRecord, Boolean> FINAL = createField("final", org.jooq.impl.SQLDataType.BOOLEAN.nullable(false), this, "");

    /**
     * The column <code>structures.calc_id</code>.
     */
    public final TableField<StructuresRecord, Integer> CALC_ID = createField("calc_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>structures</code> table reference
     */
    public Structures() {
        this("structures", null);
    }

    /**
     * Create an aliased <code>structures</code> table reference
     */
    public Structures(String alias) {
        this(alias, STRUCTURES);
    }

    private Structures(String alias, Table<StructuresRecord> aliased) {
        this(alias, aliased, null);
    }

    private Structures(String alias, Table<StructuresRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<StructuresRecord, Integer> getIdentity() {
        return Keys.IDENTITY_STRUCTURES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<StructuresRecord> getPrimaryKey() {
        return Keys.STRUCTURES_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<StructuresRecord>> getKeys() {
        return Arrays.<UniqueKey<StructuresRecord>>asList(Keys.STRUCTURES_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<StructuresRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<StructuresRecord, ?>>asList(Keys.STRUCTURES__STRUCTURES_CALC_ID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Structures as(String alias) {
        return new Structures(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Structures rename(String name) {
        return new Structures(name, null);
    }
}
