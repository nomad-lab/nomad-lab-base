/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.rawdata

import org.jooq.impl.DSL
import com.typesafe.scalalogging.StrictLogging
import com.typesafe.config.{ Config, ConfigFactory }
import org.{ json4s => jn }
import org.json4s.native.JsonMethods.{ compact, render }
import org.jooq
import java.sql.{ Connection, DriverManager }

import scala.util.control.NonFatal
import eu.nomad_lab.rawdata.db.{ Tables => RAWDATA }
import org.jooq.impl.DSL.arrayAgg
import org.jooq.Field
import org.jooq.Condition
import org.jooq.impl.DSL.trueCondition
import java.nio.file.Paths

import eu.nomad_lab.LocalEnv

import collection.JavaConverters._

/**
 * Methods to connect to the local relational DB
 */
object RawDataDb extends StrictLogging {
  lazy val driver = Class.forName("org.postgresql.Driver")

  /**
   * The settings required to connect to the local relational DB
   */
  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val username: String = config.getString("nomad_lab.rawdata.username")
    val password: String = config.getString("nomad_lab.rawdata.password")
    val jdbcUrl: String = config.getString("nomad_lab.rawdata.jdbcUrl")
    val rawDataDir: String = config.getString("nomad_lab.rawdata.rawDataDir")

    def toJson: jn.JValue = {
      import org.json4s.JsonDSL._;
      (("username" -> username) ~
        ("password" -> password) ~
        ("jdbcUrl" -> jdbcUrl))
    }
  }

  private var privateDefaultSettings: Settings = null

  /**
   * default settings, should be initialized on startup
   */
  def defaultSettings(): Settings = {
    RawDataDb.synchronized {
      if (privateDefaultSettings == null) {
        privateDefaultSettings = new Settings(LocalEnv.defaultConfig)
      }
      privateDefaultSettings
    }
  }

  /**
   * sets the default settings
   */
  def defaultSettings_=(newValue: Settings): Unit = {
    RawDataDb.synchronized {
      if (privateDefaultSettings != null)
        logger.warn(s"eu.nomad_lab.rawdata overwriting old settings ${compact(render(privateDefaultSettings.toJson))} with ${compact(render(newValue.toJson))}")
      privateDefaultSettings = newValue
    }
  }

  /**
   * Error connected to the DB (for example connection failed)
   */
  class DBError(msg: String, reason: Throwable) extends Exception(msg, reason) {}

  /**
   * Creates a new connection to the DB
   */
  def newConnection(settings: Settings): Connection = {
    try {
      driver

      DriverManager.getConnection(settings.jdbcUrl, settings.username, settings.password)
    } catch {
      case NonFatal(e) =>
        throw new DBError(s"failed to connect to the db using ${compact(render(settings.toJson))}", e)
    }
  }

  /**
   * Returns a jooq context connected to the DB
   */
  def newContext(settings: Settings): jooq.DSLContext = {
    val conn = newConnection(settings)
    jooq.impl.DSL.using(conn, jooq.SQLDialect.POSTGRES)
  }

  /**
   * Returns a new context connected to the raw data database
   * using the default settings
   */
  def apply(): RawDataDb = {
    apply(defaultSettings())
  }

  /**
   * Returns a new context connected to the raw data database
   * using the given settings
   */
  def apply(settings: Settings): RawDataDb = {
    new RawDataDb(newContext(settings), java.nio.file.Paths.get(settings.rawDataDir))
  }
}

class RawDataDb(
    val dbContext: jooq.DSLContext,
    val rawDataDir: java.nio.file.Path
) {

  def authorWithAuthorId(authorId: Integer): Option[Author] = {
    dbContext.select(
      db.Tables.AUTHOR.USERNAME,
      db.Tables.AUTHOR.FIRST_NAME,
      db.Tables.AUTHOR.LAST_NAME,
      db.Tables.AUTHOR.AFFILIATION,
      db.Tables.AUTHOR.LAST_USER_CHANGE,
      db.Tables.AUTHOR.REPO_USERNAME,
      db.Tables.AUTHOR.REPO_USER_ID
    ).from(
      db.Tables.AUTHOR
    ).where(
      db.Tables.AUTHOR.AUTHOR_ID.equal(authorId)
    ).fetchOne() match {
        case null => None
        case x =>
          Some(Author(x.value1(), x.value2(), x.value3(), x.value4(), x.value5(),
            x.value6() match {
              case null => None
              case x => Some(x)
            },
            x.value7() match {
              case null => None
              case x => Some(x.toInt)
            }))
      }
  }

  def authorWithUsername(username: String): Option[Author] = {
    dbContext.select(
      db.Tables.AUTHOR.USERNAME,
      db.Tables.AUTHOR.FIRST_NAME,
      db.Tables.AUTHOR.LAST_NAME,
      db.Tables.AUTHOR.AFFILIATION,
      db.Tables.AUTHOR.LAST_USER_CHANGE,
      db.Tables.AUTHOR.REPO_USERNAME,
      db.Tables.AUTHOR.REPO_USER_ID
    ).from(
      db.Tables.AUTHOR
    ).where(
      db.Tables.AUTHOR.USERNAME.equal(DSL.inline(username))
    ).fetchOne() match {
        case null => None
        case x =>
          Some(Author(x.value1(), x.value2(), x.value3(), x.value4(), x.value5(),
            x.value6() match {
              case null => None
              case x => Some(x)
            },
            x.value7() match {
              case null => None
              case x => Some(x.toInt)
            }))
      }
  }

  /**
   * Loops on the calculation objects constructed querying the db
   * you can give bounds, os selected calculationIds.
   * By default loops on *all* database
   */
  def loopOnRawData(
    rawDataIds: Option[Seq[Int]] = None,
    rawDataIdMin: Option[Int] = None,
    rawDataIdMax: Option[Int] = None
  )(
    rawDataOp: (Int, RawDataInfo) => Unit
  ): Unit = {
    // select the raw data to loop on (where part)
    var selectRawData: Condition = trueCondition()
    rawDataIds match {
      case Some(ids) =>
        selectRawData = selectRawData.and(RAWDATA.RAW_DATA.RAW_DATA_ID.in(ids.asJava))
      case None => ()
    }
    rawDataIdMin match {
      case Some(m) =>
        selectRawData = selectRawData.and(RAWDATA.RAW_DATA.RAW_DATA_ID.ge(new Integer(m.toInt)))
      case None => ()
    }
    rawDataIdMax match {
      case Some(m) =>
        selectRawData = selectRawData.and(RAWDATA.RAW_DATA.RAW_DATA_ID.le(new Integer(m.toInt)))
      case None => ()
    }
    loopOnRawDataWithCondition(selectRawData)(rawDataOp)
  }

  val specialUploads: Map[String, String] = Map(
    "ftp_upload_for_uid_125_2" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib1_lib" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib2_201706_0" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib2_lib" -> "ftp_upload_for_uid_125/aflowlib_data/LIB2_LIB",
    "ftp_upload_for_uid_125_lib3_201706_2" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib3_201706_3" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib3_201706_4" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib3_201706_5" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_125_lib3_lib" -> "ftp_upload_for_uid_125",
    "ftp_upload_for_uid_290_201707_1_icsd" -> "ftp_upload_for_uid_290",
    "ftp_upload_for_uid_290_201707_2_proto_etc" -> "ftp_upload_for_uid_290"
  )

  /**
   * Loops on the calculation objects constructed querying the db that satisfy the given condition
   */
  def loopOnRawDataWithCondition(condition: Condition)(rawDataOp: (Int, RawDataInfo) => Unit): Unit = {
    val content_roots: Field[Array[String]] = dbContext.select(
      arrayAgg(RAWDATA.CHUNK_PIECE.NAME)
    ).from(
        RAWDATA.CHUNK_PIECE
      ).where(
        RAWDATA.CHUNK_PIECE.UPLOAD_CHUNK_ID.equal(RAWDATA.RAW_DATA.UPLOAD_CHUNK_ID)
      ).asField("content_roots")

    val rawDatas = dbContext.select(
      content_roots,
      RAWDATA.RAW_DATA.RAW_DATA_ID,
      RAWDATA.RAW_DATA.RAW_DATA_GID,
      RAWDATA.RAW_DATA.ASSIMILATION_DATE,
      RAWDATA.UPLOAD_CHUNK.RELATIVE_PATH,
      RAWDATA.UPLOAD.UPLOAD_NAME,
      RAWDATA.UPLOAD.UPLOAD_DATE,
      RAWDATA.AUTHOR.FIRST_NAME,
      RAWDATA.AUTHOR.LAST_NAME
    ).from(
      RAWDATA.RAW_DATA.
        join(RAWDATA.UPLOAD_CHUNK).on(RAWDATA.UPLOAD_CHUNK.UPLOAD_CHUNK_ID.equal(RAWDATA.RAW_DATA.UPLOAD_CHUNK_ID)).
        join(RAWDATA.UPLOAD).on(RAWDATA.UPLOAD.UPLOAD_ID.equal(RAWDATA.UPLOAD_CHUNK.UPLOAD_ID)).
        leftOuterJoin(RAWDATA.AUTHOR).on(RAWDATA.UPLOAD.AUTHOR_ID.equal(RAWDATA.AUTHOR.AUTHOR_ID))
    ).where(
        condition
      ).orderBy(
        RAWDATA.RAW_DATA.RAW_DATA_ID
      ).fetchLazy()

    for (r <- rawDatas.asScala) {
      val uName = r.getValue(RAWDATA.UPLOAD.UPLOAD_NAME)
      val firstName = Option(r.get(RAWDATA.AUTHOR.FIRST_NAME))
      val lastName = Option(r.get(RAWDATA.AUTHOR.LAST_NAME))
      val uploader = (firstName ++ lastName).mkString(" ")

      val rInfo = RawDataInfo(
        gid = r.getValue(RAWDATA.RAW_DATA.RAW_DATA_GID),
        creation_date = r.getValue(RAWDATA.RAW_DATA.ASSIMILATION_DATE),
        upload_date = r.getValue(RAWDATA.UPLOAD.UPLOAD_DATE),
        base_path = Paths.get(specialUploads.getOrElse(uName, uName), r.getValue(RAWDATA.UPLOAD_CHUNK.RELATIVE_PATH)).toString,
        content_roots = r.value1(),
        uploader = if (uploader == "") None else Some(uploader)
      )
      rawDataOp(r.getValue(RAWDATA.RAW_DATA.RAW_DATA_ID).toInt, rInfo)
    }
  }

}
