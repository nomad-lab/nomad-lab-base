/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.rawdata;
import java.text.Normalizer
import eu.nomad_lab.CompactSha
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.Base64
import org.{ json4s => jsn }

object Author {
  /**
   * Removes all accents and diacritical marks from the string
   */
  def stripAccents(s: String): String = {
    val normalF = Normalizer.normalize(s, Normalizer.Form.NFKD)
    normalF.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
  }

  def nameGenerator(
    firstName: String,
    lastName: String,
    affiliation: String,
    repoUserName: Option[String] = None,
    repoUserId: Option[Int] = None
  ): Int => String = {
    val nonAscii = "[^A-Za-z0-9]+".r
    val separators = "[\\s.]+".r
    val initials = separators.split(stripAccents(firstName)).map { (f: String) =>
      if (f.isEmpty)
        f
      else
        f.substring(0, 1) + "."
    }.mkString("")
    val baseName = nonAscii.split(stripAccents(lastName)).mkString("_")
    val info = jsn.JObject(
      ("firstName" -> jsn.JString(firstName)) ::
        ("lastName" -> jsn.JString(lastName)) ::
        ("affiliation" -> jsn.JString(affiliation)) ::
        ("repoUserName" -> (repoUserName match {
          case Some(s) => jsn.JString(s)
          case None => jsn.JNothing
        })) ::
        ("repoUserId" -> (repoUserId match {
          case Some(idNr) => jsn.JInt(idNr)
          case None => jsn.JNothing
        })) :: Nil
    )
    val sha = CompactSha()
    JsonUtils.normalizedOutputStream(info, sha.outputStream())
    val makeUnique = sha.digest.takeRight(2)
    val tmpArray = Array.fill[Byte](2)(0)
    def genName(i: Int): String = {
      tmpArray(0) = (makeUnique(0) + (0xFF & i)).toByte
      tmpArray(1) = (makeUnique(1) + (0xF0 & (i >> 4))).toByte
      val uniqueStr = Base64.b64EncodeStr(tmpArray).slice(0, 2)
      s"${initials}${baseName}_${uniqueStr}"
    }
    genName
  }
}

case class Author(
    val userName: String,
    val firstName: String,
    val lastName: String,
    val affiliation: String,
    val lastChange: java.sql.Timestamp,
    val repoUserName: Option[String],
    val repoUserId: Option[Int]
) {
  def toJValue: jsn.JValue = {
    jsn.JObject(
      ("userName" -> jsn.JString(userName)) ::
        ("firstName" -> jsn.JString(firstName)) ::
        ("lastName" -> jsn.JString(lastName)) ::
        ("affiliation" -> jsn.JString(affiliation)) ::
        ("lastChange" -> jsn.JString(lastChange.toString())) ::
        ("repoUserName" -> (repoUserName match {
          case Some(s) => jsn.JString(s)
          case None => jsn.JNothing
        })) ::
        ("repoUserId" -> (repoUserId match {
          case Some(idNr) => jsn.JInt(idNr)
          case None => jsn.JNothing
        })) :: Nil
    )
  }
}

/**
 * Json serialization to and deserialization support for MetaInfoRecord
 */
class AuthorSerializer extends jsn.CustomSerializer[Author](format => (
  {
    case jsn.JObject(obj) => {
      var userName: String = ""
      var firstName: String = ""
      var lastName: String = ""
      var affiliation: String = ""
      var lastChange: java.sql.Timestamp = null
      var repoUserName: Option[String] = None
      var repoUserId: Option[Int] = None
      implicit val formats = format;
      obj foreach {
        case jsn.JField("userName", value) =>
          value match {
            case jsn.JString(s) => userName = s
            case jsn.JNothing | jsn.JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "userName", "Author", JsonUtils.prettyStr(value), "a string"
            )
          }
        case jsn.JField("firstName", value) =>
          value match {
            case jsn.JString(s) => firstName = s
            case jsn.JNothing | jsn.JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "firstName", "Author", JsonUtils.prettyStr(value), "a string"
            )
          }
        case jsn.JField("lastName", value) =>
          value match {
            case jsn.JString(s) => lastName = s
            case jsn.JNothing | jsn.JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "lastName", "Author", JsonUtils.prettyStr(value), "a string"
            )
          }
        case jsn.JField("affiliation", value) =>
          value match {
            case jsn.JString(s) => affiliation = s
            case jsn.JArray(arr) =>
              val sb = new StringBuilder()
              arr.foreach {
                case jsn.JString(s) => sb ++= (s)
                case jsn.JNothing => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "affiliation", "Author", JsonUtils.prettyStr(value), "either a string or an array of strings"
                )
              }
              affiliation = sb.toString
            case jsn.JNothing | jsn.JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "affiliation", "Author", JsonUtils.prettyStr(value), "either a string or an array of strings"
            )
          }
        case jsn.JField("lastChange", value) =>
          value match {
            case jsn.JString(s) => lastChange = java.sql.Timestamp.valueOf(s)
            case jsn.JNothing | jsn.JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "lastChange", "Author", JsonUtils.prettyStr(value), "a string"
            )
          }
        case jsn.JField("repoUserId", value) =>
          if (!value.toOption.isEmpty)
            repoUserId = value.extract[Option[Int]]
        case jsn.JField("repoUserName", value) =>
          if (!value.toOption.isEmpty)
            repoUserName = value.extract[Option[String]]
      }
      if (userName.isEmpty) throw new JsonUtils.MissingFieldError("userName", "Author")
      if (lastChange == null) {
        val now = new java.util.Date
        lastChange = new java.sql.Timestamp(now.getTime)
      }
      new Author(userName, firstName, lastName, affiliation, lastChange, repoUserName, repoUserId)
    }
  },
  {
    case x: Author => {
      x.toJValue
    }
  }
))
