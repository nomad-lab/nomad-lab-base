/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.rawdata
import java.sql.Timestamp

/**
 * Information on a raw data archive
 */
case class RawDataInfo(
    gid: String,
    creation_date: Timestamp,
    upload_date: Timestamp,
    base_path: String,
    content_roots: Seq[String],
    uploader: Option[String]
) {
  def sectionName: String = "section_raw_data"

  def toMap: Map[String, Any] = {
    Map(
      "raw_data_gid" -> gid,
      "raw_data_creation_date" -> creation_date,
      "raw_data_upload_date" -> upload_date,
      "raw_data_base_path" -> base_path,
      "raw_data_content_roots" -> content_roots,
      "raw_data_uploader" -> uploader.getOrElse("unknown")
    )
  }
}
