/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.rawdata
import java.nio.file.Paths
import collection.breakOut
import collection.JavaConverters._
import com.typesafe.scalalogging.StrictLogging

/**
 * Superclass representing the tree of original paths contained in the raw data archives
 *
 * Conceptually starting at a given point all sub directories should be in a single archive.
 * This point is a RPathLeaf
 */
abstract sealed class RPathTree extends StrictLogging {
  def resolvePath(path: Seq[String], pIndex: Int = 0): List[RawDataInfo];

  def insertRawDataInfo(rawData: RawDataInfo): RPathTree = {
    val pathIt = Paths.get(rawData.base_path).iterator.asScala
    val path: Seq[String] = (for (p <- pathIt) yield p.toString).toSeq
    insertRawData(rawData, path, 0)
  }

  def insertRawData(rawData: RawDataInfo, path: Seq[String], pIndex: Int = 0): RPathTree;
}

///**
// * A point from which on all paths are in a single raw data archives
// *
// * To support for temporary mistakes, or repacking, multiple archives might be found
// */
//case class RPathLeaf(
//    rawDatas: List[RawDataInfo]
//) extends RPathTree {
//  override def resolvePath(path: Seq[String], pIndex: Int = 0): List[RawDataInfo] = rawDatas
//
//  override def insertRawData(rawData: RawDataInfo, path: Seq[String], pIndex: Int = 0): RPathTree = {
//    if (pIndex < path.length) {
//      val s = path(pIndex)
//      throw new Exception(s"RPathLeaf path ${path.slice(0, pIndex).mkString("/")} for $rawDatas is continued to ${path.mkString("/")} by $rawData")
//    } else if (rawDatas.contains(rawData)) {
//      this
//    } else {
//      logger.warn(s"RPathLeaf path ${path.mkString("/")} belongs to multiple raw data archives: ${rawDatas.map(_.gid)} and ${rawData.gid}")
//      RPathLeaf(rawData :: rawDatas)
//    }
//  }
//}

/**
 * Represent a directory in the original path
 *
 * Might contains directories and files belonging to multiple archives (i.e. RPathNode and RPathLeaf)
 */
case class RPathNode(
    subTrees: Map[String, RPathTree],
    rawDataEntries: List[RawDataInfo]
) extends RPathTree {
  override def resolvePath(path: Seq[String], pIndex: Int = 0): List[RawDataInfo] = {
    if (pIndex < path.length) {
      val s = path(pIndex)
      subTrees.get(s) match {
        case Some(t) =>
          rawDataEntries ++ t.resolvePath(path, pIndex + 1)
        case None =>
          if (pIndex > 0 && rawDataEntries.isEmpty) {
            logger.warn(s"partial match until ${pIndex - 1} without leaf for $path")
          }
          rawDataEntries
      }
    } else {
      if (pIndex > 0 && rawDataEntries.isEmpty) {
        logger.warn(s"partial match until ${pIndex - 1} without leaf for $path")
      }
      rawDataEntries
    }
  }

  override def insertRawData(rawData: RawDataInfo, path: Seq[String], pIndex: Int = 0): RPathTree = {
    if (pIndex < path.length) {
      val s = path(pIndex)
      subTrees.get(s) match {
        case Some(t) =>
          RPathNode(subTrees + (s -> t.insertRawData(rawData, path, pIndex + 1)), rawDataEntries)
        case None =>
          RPathNode(subTrees + (s ->
            RPathNode(Map(), Nil).insertRawData(rawData, path, pIndex + 1)), rawDataEntries)
      }
    } else if (rawData.content_roots == null || rawData.content_roots.isEmpty) {
      if (subTrees.nonEmpty) {
        logger.warn(s"$rawData finishes at path ${path.mkString("/")} which is not a leaf path")
      }
      RPathNode(subTrees, rawData :: rawDataEntries)
    } else {
      RPathNode(subTrees ++ rawData.content_roots.map { x =>
        subTrees.get(x) match {
          case Some(RPathNode(nodeSubTree, nodeRawData)) =>
            if (nodeRawData.nonEmpty) {
              logger.warn(s"multiple archives for the same path ${path.mkString("/")}/$x: ${nodeRawData.map(_.gid)}, ${rawData.gid}")
            }
            if (nodeSubTree.nonEmpty) {
              logger.warn(s"archive $rawData ends at path ${path.mkString("/")}/$x which is not a leaf path")
            }
            x -> RPathNode(nodeSubTree, rawData :: nodeRawData)
          case None => x -> RPathNode(Map(), rawData :: Nil)
        }
      }, rawDataEntries)
    }
  }
}
