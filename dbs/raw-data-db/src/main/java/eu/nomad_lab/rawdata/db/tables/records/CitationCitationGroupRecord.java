/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.rawdata.db.tables.records;


import eu.nomad_lab.rawdata.db.tables.CitationCitationGroup;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CitationCitationGroupRecord extends UpdatableRecordImpl<CitationCitationGroupRecord> implements Record2<Integer, Integer> {

    private static final long serialVersionUID = -2071713612;

    /**
     * Setter for <code>citation_citation_group.citation_id</code>.
     */
    public void setCitationId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>citation_citation_group.citation_id</code>.
     */
    public Integer getCitationId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>citation_citation_group.citation_group_id</code>.
     */
    public void setCitationGroupId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>citation_citation_group.citation_group_id</code>.
     */
    public Integer getCitationGroupId() {
        return (Integer) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record2<Integer, Integer> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Integer, Integer> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Integer, Integer> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return CitationCitationGroup.CITATION_CITATION_GROUP.CITATION_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return CitationCitationGroup.CITATION_CITATION_GROUP.CITATION_GROUP_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getCitationId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getCitationGroupId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CitationCitationGroupRecord value1(Integer value) {
        setCitationId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CitationCitationGroupRecord value2(Integer value) {
        setCitationGroupId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CitationCitationGroupRecord values(Integer value1, Integer value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached CitationCitationGroupRecord
     */
    public CitationCitationGroupRecord() {
        super(CitationCitationGroup.CITATION_CITATION_GROUP);
    }

    /**
     * Create a detached, initialised CitationCitationGroupRecord
     */
    public CitationCitationGroupRecord(Integer citationId, Integer citationGroupId) {
        super(CitationCitationGroup.CITATION_CITATION_GROUP);

        set(0, citationId);
        set(1, citationGroupId);
    }
}
