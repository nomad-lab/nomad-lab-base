/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.rawdata.db;


import javax.annotation.Generated;

import org.jooq.Sequence;
import org.jooq.impl.SequenceImpl;


/**
 * Convenience access to all sequences in 
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Sequences {

    /**
     * The sequence <code>author_group_id_seq</code>
     */
    public static final Sequence<Long> AUTHOR_GROUP_ID_SEQ = new SequenceImpl<Long>("author_group_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>author_id_seq</code>
     */
    public static final Sequence<Long> AUTHOR_ID_SEQ = new SequenceImpl<Long>("author_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>calculation_id_seq</code>
     */
    public static final Sequence<Long> CALCULATION_ID_SEQ = new SequenceImpl<Long>("calculation_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>chunk_piece_id_seq</code>
     */
    public static final Sequence<Long> CHUNK_PIECE_ID_SEQ = new SequenceImpl<Long>("chunk_piece_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>citation_group_id_seq</code>
     */
    public static final Sequence<Long> CITATION_GROUP_ID_SEQ = new SequenceImpl<Long>("citation_group_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>citation_id_seq</code>
     */
    public static final Sequence<Long> CITATION_ID_SEQ = new SequenceImpl<Long>("citation_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>raw_data_id_seq</code>
     */
    public static final Sequence<Long> RAW_DATA_ID_SEQ = new SequenceImpl<Long>("raw_data_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>upload_chunk_id_seq</code>
     */
    public static final Sequence<Long> UPLOAD_CHUNK_ID_SEQ = new SequenceImpl<Long>("upload_chunk_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));

    /**
     * The sequence <code>upload_id_seq</code>
     */
    public static final Sequence<Long> UPLOAD_ID_SEQ = new SequenceImpl<Long>("upload_id_seq", DefaultSchema.DEFAULT_SCHEMA, org.jooq.impl.SQLDataType.BIGINT.nullable(false));
}
