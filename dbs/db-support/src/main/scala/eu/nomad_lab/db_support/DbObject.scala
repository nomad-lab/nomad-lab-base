/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.db_support

import com.typesafe.scalalogging.StrictLogging
import org.json4s.JValue
import com.typesafe.config.{ Config, ConfigFactory }
import eu.{ nomad_lab => lab }
import org.jooq
import java.sql.{ Connection, DriverManager }
import scala.util.control.NonFatal
import org.flywaydb.core.Flyway

object DbObject {
  /**
   * Error to connected to the DB (for example connection failed)
   */
  class DbError(msg: String, reason: Throwable = null) extends Exception(msg, reason)

  class DbSettings(val baseConfigPath: String, val config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val enabled: Boolean = config.getBoolean(baseConfigPath + ".enabled")
    val username: String = config.getString(baseConfigPath + ".username")
    val password: String = config.getString(baseConfigPath + ".password")
    val jdbcUrl: String = config.getString(baseConfigPath + ".jdbcUrl")

    def toJson: JValue = {
      import org.json4s.JsonDSL._;
      (("username" -> username) ~
        ("password" -> password) ~
        ("jdbcUrl" -> jdbcUrl) ~
        ("enabled" -> enabled))
    }
  }
}

/**
 * Methods to connect to a postgres DB
 */
class DbObject[Settings <: DbObject.DbSettings](
    val createSettings: (Config) => Settings,
    val migrationLocations: Seq[String]
) extends StrictLogging {
  lazy val driver = Class.forName("org.postgresql.Driver")

  private var privateDefaultSettings: Option[Settings] = None

  /**
   * default settings, should be initialized on startup
   */
  def defaultSettings(): Settings = {
    this.synchronized {
      privateDefaultSettings match {
        case None =>
          val newSettings = createSettings(lab.LocalEnv.defaultConfig)
          privateDefaultSettings = Some(newSettings)
          newSettings
        case Some(settings) =>
          settings
      }
    }
  }

  /**
   * sets the default settings
   */
  def defaultSettings_=(newValue: Settings): Unit = {
    this.synchronized {
      privateDefaultSettings match {
        case Some(oldSettings) =>
          logger.warn(s"eu.nomad_lab.parsing_stats overwriting old settings ${lab.JsonUtils.normalizedStr(oldSettings.toJson)} with ${lab.JsonUtils.normalizedStr(newValue.toJson)}")
        case None => ()
      }
      privateDefaultSettings = Some(newValue)
    }
  }

  /**
   * Checks the db, and possibly applies migrations.
   *
   * throws an exception if the DB is not consistent with the expected
   * state.
   *
   * If migrateEmpty is true (the default) then an empty database is migrated
   * to the latest version.
   * If migrateNonEmpty is true (false by default) then the database is migrated
   * if it is in a version that is not the latest one.
   * If migrations fail then an exception is also thrown.
   * If validate is true (the default) performs a complete validation against
   * the migrations to detect manual changes to the schema.
   */
  def checkDb(
    settings: Settings,
    migrateEmpty: Boolean = true,
    migrateNonEmpty: Boolean = false,
    validate: Boolean = true
  ): Unit = {
    if (migrationLocations.isEmpty)
      return
    driver
    val flywayObj = new Flyway()
    flywayObj.setDataSource(settings.jdbcUrl, settings.username, settings.password)
    flywayObj.setLocations(migrationLocations: _*)
    val migrationStatus = flywayObj.info()
    var migrate: Boolean = false

    if (!migrationStatus.pending().isEmpty) {
      if (null == migrationStatus.current()) {
        if (migrateEmpty)
          migrate = true
        else
          throw new DbObject.DbError(s"no database and migrateEmpty is false in checkDb of ${settings.jdbcUrl}")
      } else {
        if (migrateNonEmpty)
          migrate = true
        else
          throw new DbObject.DbError(s"database has an older version and migrateNonEmpty is false in checkDb of ${settings.jdbcUrl}")
      }
      if (migrate) {
        logger.info(s"migrating db ${settings.jdbcUrl}")
        flywayObj.migrate()
      }
    }
    if (validate)
      flywayObj.validate()
  }

  /**
   * Creates a new connection to the DB
   */
  def newConnection(settings: Settings, check: Boolean = true): Connection = {
    if (check)
      checkDb(settings)
    if (!settings.enabled) {
      throw new Exception(s"disabled DB in the settings: ${lab.JsonUtils.normalizedStr(settings.toJson)}")
    } else {
      try {
        driver

        DriverManager.getConnection(settings.jdbcUrl, settings.username, settings.password)
      } catch {
        case NonFatal(e) =>
          throw new DbObject.DbError(s"failed to connect to the db using ${lab.JsonUtils.normalizedStr(settings.toJson)}", e)
      }
    }
  }

  /**
   * Returns a jooq context connected to the DB
   */
  def newContext(settings: Settings, check: Boolean = true): jooq.DSLContext = {
    val conn = newConnection(settings, check = check)
    jooq.impl.DSL.using(conn, jooq.SQLDialect.POSTGRES)
  }

  /**
   * Tries to execute the given code in a transaction at most maxTries and
   * returns the last exception it gave if it did fail
   */
  def failureForTransaction(ctx: jooq.DSLContext, maxTries: Int = 10)(body: (jooq.Configuration, Int) => Unit): Option[Throwable] = {
    var success: Boolean = false
    var iTries: Int = 0
    var lastExc: Option[Throwable] = None
    while (!success && iTries < maxTries) {
      iTries += 1
      try {
        ctx.transaction(new jooq.TransactionalRunnable {
          override def run(conf: jooq.Configuration): Unit = {
            body(conf, iTries)
          }
        })
        success = true
      } catch {
        case e: RuntimeException =>
          lastExc = Some(e)
        case NonFatal(e) =>
          iTries = maxTries
          lastExc = Some(e)
      }
    }
    if (success)
      None
    else
      lastExc
  }
}
