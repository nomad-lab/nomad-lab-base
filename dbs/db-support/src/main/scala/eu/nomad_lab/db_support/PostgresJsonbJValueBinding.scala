/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.db_support

import org.jooq.tools.Convert.convert
import java.sql._
import org.jooq._
import org.jooq.impl.DSL
import org.json4s.JValue
import org.json4s.JNothing
import eu.{ nomad_lab => lab }

// We're binding <T> = Object (unknown database type), and <U> = JValue (user type)
class PostgresJsonbJValueBinding extends Binding[Object, JValue] {

  // The converter does all the work
  override def converter(): Converter[Object, JValue] = {
    new Converter[Object, JValue]() {
      override def from(t: Object): JValue = {
        if (t == null)
          JNothing
        else
          lab.JsonUtils.parseStr("" + t)
      }

      override def to(u: JValue): Object = {
        u match {
          case JNothing => null
          case _ => lab.JsonUtils.normalizedStr(u)
        }
      }

      override def fromType(): Class[Object] = {
        return classOf[Object]
      }

      override def toType(): Class[JValue] = {
        classOf[JValue]
      }
    }
  }

  // Rending a bind variable for the binding context's value and casting it to the json type
  override def sql(ctx: BindingSQLContext[JValue]): Unit = {
    ctx.render().visit(DSL.`val`(ctx.convert[Object](converter()).value())).sql("::jsonb");
  }

  override def register(ctx: BindingRegisterContext[JValue]): Unit = {
    ctx.statement().registerOutParameter(ctx.index(), Types.VARCHAR)
  }

  // Converting the JsonElement to a String value and setting that on a JDBC PreparedStatement
  override def set(ctx: BindingSetStatementContext[JValue]): Unit = {
    ctx.statement().setString(ctx.index(), java.util.Objects.toString(ctx.convert[Object](converter()).value(), null))
  }

  // Getting a String value from a JDBC ResultSet and converting that to a JValue
  override def get(ctx: BindingGetResultSetContext[JValue]): Unit = {
    ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()))
  }

  // Getting a String value from a JDBC CallableStatement and converting that to a JValue
  override def get(ctx: BindingGetStatementContext[JValue]): Unit = {
    ctx.convert(converter()).value(ctx.statement().getString(ctx.index()))
  }

  // Setting a value on a JDBC SQLOutput (useful for Oracle OBJECT types)
  override def set(ctx: BindingSetSQLOutputContext[JValue]): Unit = {
    throw new SQLFeatureNotSupportedException();
  }

  // Getting a value from a JDBC SQLInput (useful for Oracle OBJECT types)
  override def get(ctx: BindingGetSQLInputContext[JValue]) = {
    throw new SQLFeatureNotSupportedException();
  }
}
