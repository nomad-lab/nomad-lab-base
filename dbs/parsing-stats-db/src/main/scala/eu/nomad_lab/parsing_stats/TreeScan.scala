/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_stats

import java.time.OffsetDateTime
import org.{ json4s => jn }
import eu.nomad_lab.CompactSha
import eu.nomad_lab.JsonUtils

object TreeScan {
  /**
   * Rounds down date to Milliseconds
   */
  def dateMs(date: OffsetDateTime): OffsetDateTime = {
    val n = date.getNano()
    val n2 = (((n + 500000) / 1000000) * 1000000) % 1000000000
    date.withNano(n2)
  }

  def calculateGid(
    tree_scan_tree_uri: String,
    tree_scan_root_namespace: String,
    tree_scan_description: String,
    tree_scan_start: OffsetDateTime
  ): String = {
    val jV = jn.JObject(
      ("type" -> jn.JString("tree_scan")) ::
        ("tree_scan_tree_uri" -> jn.JString(tree_scan_tree_uri)) ::
        ("tree_scan_root_namespace" -> jn.JString(tree_scan_root_namespace)) ::
        ("tree_scan_description" -> jn.JString(tree_scan_description)) ::
        ("tree_scan_start" -> jn.JString(dateMs(tree_scan_start).toString)) :: Nil
    )
    CompactSha(JsonUtils.normalizedStr(jV)).gidStr("ts")
  }

  def apply(
    tree_scan_tree_uri: String,
    tree_scan_root_namespace: String,
    tree_scan_description: String,
    tree_scan_start: Option[OffsetDateTime] = None,
    tree_scan_end: Option[OffsetDateTime] = None,
    tree_scan_processing_start: Option[OffsetDateTime] = None,
    tree_scan_processing_end: Option[OffsetDateTime] = None,
    tree_scan_clean_end: Boolean = false
  ): TreeScan = {
    // round to ms
    val tree_scan_start1 = dateMs(tree_scan_start.getOrElse(OffsetDateTime.now()))
    val gid = calculateGid(
      tree_scan_tree_uri = tree_scan_tree_uri,
      tree_scan_root_namespace = tree_scan_root_namespace,
      tree_scan_description = tree_scan_description,
      tree_scan_start = tree_scan_start1
    )
    TreeScan(
      tree_scan_identifier = gid,
      tree_scan_tree_uri = tree_scan_tree_uri,
      tree_scan_root_namespace = tree_scan_root_namespace,
      tree_scan_description = tree_scan_description,
      tree_scan_start = tree_scan_start1,
      tree_scan_end = tree_scan_end,
      tree_scan_processing_start = tree_scan_processing_start,
      tree_scan_processing_end = tree_scan_processing_end,
      tree_scan_clean_end = tree_scan_clean_end
    )
  }
}

case class TreeScan(
  tree_scan_identifier: String,
  tree_scan_tree_uri: String,
  tree_scan_root_namespace: String,
  tree_scan_description: String,
  tree_scan_start: OffsetDateTime,
  tree_scan_end: Option[OffsetDateTime],
  tree_scan_processing_start: Option[OffsetDateTime],
  tree_scan_processing_end: Option[OffsetDateTime],
  tree_scan_clean_end: Boolean
)
