/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_stats

import org.{ json4s => jn }

/** Statistics for a specific parser */
case class ProcessorStats(
  processor_info: ProcessorInfo,
  processor_stats_tree_scan_identifier: String,
  processor_cumulative_work_seconds: Double,
  processor_n_successful: Long,
  processor_n_failed: Long,
  processor_n_skipped: Long,
  processor_n_pending: Long
)
