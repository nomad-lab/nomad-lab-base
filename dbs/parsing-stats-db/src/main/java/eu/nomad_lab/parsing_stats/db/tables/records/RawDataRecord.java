/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.parsing_stats.db.tables.records;


import eu.nomad_lab.parsing_stats.db.tables.RawData;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RawDataRecord extends UpdatableRecordImpl<RawDataRecord> implements Record2<Integer, String> {

    private static final long serialVersionUID = 2014793685;

    /**
     * Setter for <code>raw_data.raw_data_id</code>.
     */
    public void setRawDataId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>raw_data.raw_data_id</code>.
     */
    public Integer getRawDataId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>raw_data.raw_data_gid</code>.
     */
    public void setRawDataGid(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>raw_data.raw_data_gid</code>.
     */
    public String getRawDataGid() {
        return (String) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Integer, String> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Integer, String> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return RawData.RAW_DATA.RAW_DATA_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return RawData.RAW_DATA.RAW_DATA_GID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getRawDataId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getRawDataGid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RawDataRecord value1(Integer value) {
        setRawDataId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RawDataRecord value2(String value) {
        setRawDataGid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RawDataRecord values(Integer value1, String value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached RawDataRecord
     */
    public RawDataRecord() {
        super(RawData.RAW_DATA);
    }

    /**
     * Create a detached, initialised RawDataRecord
     */
    public RawDataRecord(Integer rawDataId, String rawDataGid) {
        super(RawData.RAW_DATA);

        set(0, rawDataId);
        set(1, rawDataGid);
    }
}
