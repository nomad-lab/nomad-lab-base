/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.parsing_stats.db.enums;


import eu.nomad_lab.parsing_stats.db.DefaultSchema;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum ParseResultEnum implements EnumType {

    ParseFailure("ParseFailure"),

    ParseSkipped("ParseSkipped"),

    ParseWithWarnings("ParseWithWarnings"),

    ParseSuccess("ParseSuccess");

    private final String literal;

    private ParseResultEnum(String literal) {
        this.literal = literal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return getSchema() == null ? null : getSchema().getCatalog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "parse_result_enum";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLiteral() {
        return literal;
    }
}
