-- -- Initial setup of raw data
--
-- drop index i_processor_stats_p_id_as;
-- drop index i_processor_stats_as_p_id;
-- drop table processor_stats;
-- drop sequence processor_stats_id_seq;
-- drop index i_processor_p_idf_p_id;
-- drop table processor;
-- drop sequence processor_info_id_seq;
-- drop index i_tree_scan_rnsp_aid;
-- drop index i_tree_scan_aid_rnsp;
-- drop table tree_scan;
-- drop sequence _id_seq;
-- drop table tree_scan__tree_scan_tree_uri;
-- drop sequence tree_scan__tree_scan_tree_uri_id_seq;
-- drop table tree_scan__tree_scan_root_namespace;
-- drop sequence tree_scan__tree_scan_root_namespace_id_seq;
--

create sequence tree_scan__tree_scan_root_namespace_id_seq;
create table tree_scan__tree_scan_root_namespace(
       tree_scan__tree_scan_root_namespace_id Integer not null default nextval('tree_scan__tree_scan_root_namespace_id_seq'),
       tree_scan_root_namespace varchar(80) not null,
       constraint pk_tstsrn primary key (tree_scan__tree_scan_root_namespace_id),
       constraint u_tstsrn_root_namespace_name unique (tree_scan_root_namespace)
);

create sequence tree_scan__tree_scan_tree_uri_id_seq;
create table tree_scan__tree_scan_tree_uri(
       tree_scan__tree_scan_tree_uri_id Integer not null default nextval('tree_scan__tree_scan_tree_uri_id_seq'),
       tree_scan_uri varchar(200) not null,
       constraint pk_tree primary key (tree_scan__tree_scan_tree_uri_id),
       constraint u_raw_data_raw_data_gid unique (tree_scan_uri)
);

create sequence tree_scan_id_seq;
create table tree_scan(
       tree_scan_id Integer not null default nextval('tree_scan_id_seq'),
       tree_scan__tree_scan_tree_uri_id Integer not null,
       tree_scan__tree_scan_root_namespace_id Integer not null,
       tree_scan_description varchar(400) not null,
       tree_scan_start timestamp with time zone default CURRENT_TIMESTAMP,
       tree_scan_identifier varchar(30) not null,
       tree_scan_end timestamp with time zone,
       tree_scan_processing_start timestamp with time zone,
       tree_scan_processing_end timestamp with time zone,
       tree_scan_clean_end boolean not null default false,
       constraint pk_tree_scan primary key (tree_scan_id),
       constraint fk_tree_scan_tststu foreign key (tree_scan__tree_scan_tree_uri_id) references tree_scan__tree_scan_tree_uri(tree_scan__tree_scan_tree_uri_id),
       constraint fk_tree_scan_tstsrn_id foreign key (tree_scan__tree_scan_root_namespace_id) references tree_scan__tree_scan_root_namespace (tree_scan__tree_scan_root_namespace_id),
       constraint u_tree_scan_tree_scan_identifier unique(tree_scan_identifier)
);
create unique index i_tree_scan_tid_rnsp on tree_scan (tree_id, tree_scan__tree_scan_root_namespace_id, tree_scan_id);
create unique index i_tree_scan_rnsp_tid on tree_scan (tree_scan__tree_scan_root_namespace_id, tree_id, tree_scan_id);

create sequence processor_info_id_seq;
create table processor_info(
       processor_info_id Integer not null default nextval('processor_info_id_seq'),
       processor_identifier varchar(120) not null,
       processor_name varchar(80) not null,
       processor_version_info jsonb not null,
       constraint pk_processor primary key (processor_info_id)
);
create unique index i_processor_p_idf_p_id on processor(processor_identifier, processor_info_id);

create sequence processor_stats_id_seq;
create table processor_stats(
       tree_scan_id Integer not null,
       processor_info_id Integer not null,
       processor_cumulative_work_seconds double precision,
       processor_n_successful Integer,
       processor_n_failed Integer,
       processor_n_skipped Integer,
       processor_n_pending Integer,
       constraint pk_processor_stats primary key (tree_scan, processor_info_id));
create unique index i_processor_stats_as_p_id on processor_stats (tree_scan_id, processor_info_id);
create unique index i_processor_stats_p_id_as on processor_stats (processor_info_id, tree_scan_id);
