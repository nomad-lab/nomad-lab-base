/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.normalizers

import eu.nomad_lab.normalize.NormalizerGenerator
import eu.nomad_lab.normalize.MetaIndexRebuildNormalizer
import eu.nomad_lab.normalize.SectionSystemNormalizer
import eu.nomad_lab.normalize.DosValuesByAtomNormalizer
import eu.nomad_lab.normalize.FhiDosNormalizer
import eu.nomad_lab.normalize.CalculationInfoNormalizer

import scala.collection.breakOut
import org.{ json4s => jn }
import eu.nomad_lab.JsonUtils

object AllNormalizers {
  /**
   * Exception raised when fromJValue fails
   */
  class SerializationError(
    msg: String
  ) extends Exception(msg)

  /**
   * Exception raised if there are duplicate normalizers (i.e. normalizers with the same name)
   */
  class DuplicateNormalizer(name: String) extends Exception(
    s"Duplicate normalizer with name $name"
  ) {}

  /**
   * transforms a list of normalizers to a map
   */
  private def listToMap(normalizersList: Seq[NormalizerGenerator]): Map[String, NormalizerGenerator] = {
    val res: Map[String, NormalizerGenerator] = normalizersList.map { (pg: NormalizerGenerator) =>
      pg.name -> pg
    }(breakOut)

    if (res.size != normalizersList.length) {
      for (pg <- normalizersList) {
        if (pg != res(pg.name))
          throw new DuplicateNormalizer(pg.name)
      }
    }
    res
  }

  /**
   * All active normalizers
   */
  val activeNormalizers: Map[String, NormalizerGenerator] = {
    listToMap(Seq(
      StatsNormalizer,
      FhiAimsBasisNormalizer,
      SymmetryNormalizer,
      SpringerNormalizer,
      MetaIndexRebuildNormalizer,
      SectionSystemNormalizer,
      DosValuesByAtomNormalizer,
      RepoTagsNormalizer,
      PrototypesNormalizer,
      BandStructureNormalizer,
      FhiDosNormalizer,
      CalculationInfoNormalizer,
      SystemTypeNormalizer,
      UploadInfoNormalizer
    ))
  }

  /**
   * All inactive normalizers
   */
  val inactiveNormalizers: Map[String, NormalizerGenerator] = {
    listToMap(Seq())
  }

  /**
   * All known normalizers
   */
  val knownNormalizers: Map[String, NormalizerGenerator] = {
    for ((k, v) <- inactiveNormalizers) {
      if (activeNormalizers.contains(k))
        throw new DuplicateNormalizer(k)
    }
    activeNormalizers ++ inactiveNormalizers
  }

  /**
   * Instantiates (or returns) a normalization generator from a jValue
   */
  def fromJValue(jValue: jn.JValue): NormalizerGenerator = {
    (jValue \ "name") match {
      case jn.JString(n) =>
        knownNormalizers.get(n) match {
          case Some(g) => g
          case None =>
            throw new SerializationError(s"cannot instantiate unknown normalizer '$n'")
        }
      case v =>
        throw new SerializationError(s"cannot instantiate normalizer without name from '${JsonUtils.normalizedStr(jValue)}'")
    }
  }
}
