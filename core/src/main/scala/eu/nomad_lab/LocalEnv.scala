/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import java.io.File

import com.typesafe.config.{ Config, ConfigException, ConfigFactory, ConfigValue }

import scala.collection.JavaConverters._
import scala.collection.breakOut
import scala.collection.mutable
import java.util.Map.Entry

import org.{ json4s => jsn }
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.attribute

import com.typesafe.scalalogging.StrictLogging

import scala.util.control.NonFatal
import java.text.Normalizer

import com.typesafe.config.ConfigException.Missing

object LocalEnv extends StrictLogging {
  /**
   * makes a string "kubernetes safe" by skipping characters that are not accepted
   * does not check to maximum length (63 for pod names), or that it is not empty,
   * but otherwise it should produce valid pod or service names
   */
  def kubernetesSafe(str: String): String = {
    val normalF = Normalizer.normalize(str, Normalizer.Form.NFKD)
    val alpha = normalF.replaceAll("[^a-zA-Z0-9-]+", "").toLowerCase()
    val alphaStart = alpha.dropWhile((x: Char) => !x.isLetter)
    var toDrop: Int = 0
    while (toDrop < alphaStart.length() && alphaStart.charAt(alphaStart.length() - toDrop - 1) == '-')
      toDrop += 1
    alphaStart.dropRight(toDrop)
  }

  /** searches for file either in the current directory or its parents */
  def searchUp(file: String, base: String = "."): Option[Path] = {
    var baseDir: Path = Paths.get(base).toAbsolutePath()
    var hasMore: Boolean = !Paths.get(file).isAbsolute()
    var iDepth = 0
    def calcFilePath(): Option[Path] = {
      val f = baseDir.resolve(file)
      if (Files.isReadable(f))
        Some(f)
      else
        None
    }
    if (file == null || file.isEmpty)
      None
    else if (Paths.get(file).isAbsolute())
      calcFilePath()
    else {
      var fPath: Option[Path] = calcFilePath()
      while (fPath.isEmpty && hasMore && iDepth < 5) {
        val oldPath = baseDir
        baseDir = baseDir.getParent()
        if (baseDir == null)
          return None
        if (baseDir == oldPath) hasMore = false
        fPath = calcFilePath()
        iDepth += 1
      }
      fPath
    }
  }

  /**
   * Loads a configuration
   * This can use an external file and specific configuration to use.
   * Normally you should not call this, but rather use defaultConfig.
   */
  def loadConfiguration(configToUse: Option[String] = None, externalConf: Option[String] = None, verbose: Boolean = true): Config = {
    val refConfig = ConfigFactory.parseResources("application.conf").withFallback(
      ConfigFactory.parseResources("reference.conf").resolve()
    )
    val consoleConfig = ConfigFactory.systemProperties()
    var filePath: Option[Path] = searchUp(externalConf.getOrElse(
      consoleConfig.withFallback(refConfig).getString("nomad_lab.externalConfigFile")
    ))
    val applicationConfig = filePath match {
      case Some(path) =>
        try {
          ConfigFactory.parseFile(path.toFile).resolve()
        } catch {
          case ex: ConfigException =>
            throw new Exception(s"configuration loading of $filePath failed", ex)
        }
      case None =>
        ConfigFactory.empty()
    }
    lazy val configDescription = filePath match {
      case Some(p) => s"'$p' as external settings"
      case None => "only the internal settings"
    }
    val mergedConfig = consoleConfig.withFallback(applicationConfig).withFallback(refConfig)
    val configToUseNow = configToUse.getOrElse(mergedConfig.getString("nomad_lab.configurationToUse"))
    val finalConfig: Config = (if (configToUseNow == null || configToUseNow == "") {
      mergedConfig
    } else {
      try {
        mergedConfig.getConfig("overrides").getConfig(configToUseNow).withFallback(mergedConfig)
      } catch {
        case ex: Missing =>
          if (verbose)
            logger.warn(s"Configuration $configToUse has no overrides.")
          mergedConfig
      }
    }).withoutPath("overrides")
    if (verbose) {
      logger.info(s"Loaded configuration '$configToUseNow' using $configDescription")
      logger.info(finalConfig.root().render(com.typesafe.config.ConfigRenderOptions.defaults().setOriginComments(false).setComments(false)))
    }
    finalConfig
  }

  val workerId = java.util.UUID.randomUUID.toString

  /**
   * The settings required to connect to the local relational DB
   */
  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val user: String = java.lang.System.getenv("nomadUser") match {
      case null =>
        java.lang.System.getenv("USER") match {
          case null => "unknown"
          case usr => usr
        }
      case usr => usr
    }
    val replacements: Map[String, String] = {
      val baseRepl: Map[String, String] = config.getObject("nomad_lab.replacements").entrySet().asScala.map {
        (entry: Entry[String, ConfigValue]) =>
          entry.getKey() -> entry.getValue.unwrapped().toString()
      }(breakOut)

      val preRepl = baseRepl ++ Seq(
        "workerId" -> workerId,
        "procTmpDir" -> Paths.get(config.getString("nomad_lab.roots.baseTmpDir"), workerId).toString,
        "baseTmpDir" -> config.getString("nomad_lab.roots.baseTmpDir"),
        "parsedRoot" -> config.getString("nomad_lab.roots.parsedRoot"),
        "rawDataRoot" -> config.getString("nomad_lab.roots.rawDataRoot"),
        "normalizedRoot" -> config.getString("nomad_lab.roots.normalizedRoot"),
        "coreVersion" -> NomadCoreVersionInfo.version,
        "user" -> user,
        "remoteHost" -> config.getString("nomad_lab.remoteSource.remoteHost"),
        "remoteRawDataRoot" -> (config.getString("nomad_lab.remoteSource.remoteRawDataRoot") match {
          case null => "${rawDataRoot}"
          case "" => "${rawDataRoot}"
          case rRoot => rRoot
        }),
        "remoteParsedRoot" -> (config.getString("nomad_lab.remoteSource.remoteParsedRoot") match {
          case null => "${rawDataRoot}"
          case "" => "${parsedRoot}"
          case rRoot => rRoot
        })
      )
      // perform expansions that can be done
      val repl = preRepl.map {
        case (k, v) =>
          k -> makeReplacements(preRepl, v)
      }
      repl + ("rootnamespace" -> kubernetesSafe(repl("rootNamespace")))
    }
    // utility variables
    val baseTmpDir: Path = Paths.get(replacements("baseTmpDir"))
    val rootNamespace: String = replacements("rootNamespace")
    val procTmpDir: Path = Paths.get(replacements("procTmpDir"))
    val parsedRoot: Path = Paths.get(replacements("parsedRoot"))
    val rawDataRoot: Path = Paths.get(replacements("rawDataRoot"))
    val normalizedRoot: Path = Paths.get(replacements("normalizedRoot"))
    val remoteHost: Option[String] = replacements("remoteHost") match {
      case null | "" => None
      case host => Some(host)
    }
    val nomadCodeRoot: Option[Path] = {
      val codeRootStr = config.getString("nomad_lab.nomadCodeRoot")
      if (codeRootStr == null || codeRootStr.isEmpty)
        None
      else {
        val codeRootPath = Paths.get(codeRootStr)
        if (Files.exists(codeRootPath))
          Some(codeRootPath)
        else
          None
      }
    }

    val h5LockOnRead: Boolean = config.getBoolean("nomad_lab.h5LockOnRead")
    val rawDataArchives: String = config.getString("nomad_lab.rawDataArchives")
    val remoteRawDataArchives: String = config.getString("nomad_lab.remoteSource.remoteRawDataArchives")
    val remoteParsedArchives: String = config.getString("nomad_lab.remoteSource.remoteParsedArchives")

    def toJson: jsn.JValue = jsn.JObject(
      replacements.map {
        case (k, v) =>
          k -> jsn.JString(v)
      }(breakOut): List[(String, jsn.JString)]
    )

  }

  // convenience method to access the rootNamespace
  def rootNamespace: String = defaultSettings.rootNamespace

  private var privateConfig: Option[Config] = None

  def defaultConfig: Config = {
    LocalEnv.synchronized {
      privateConfig match {
        case None =>
          val newConf = loadConfiguration(verbose = true)
          privateConfig = Some(newConf)
          newConf
        case Some(conf) =>
          conf
      }
    }
  }

  def defaultConfig(verbose: Boolean): Config = {
    LocalEnv.synchronized {
      privateConfig match {
        case None =>
          val newConf = loadConfiguration(verbose = verbose)
          privateConfig = Some(newConf)
          newConf
        case Some(conf) =>
          conf
      }
    }
  }

  def defaultConfig_=(newValue: Config): Unit = {
    LocalEnv.synchronized {
      privateConfig match {
        case None =>
          privateConfig = Some(newValue)
        case Some(oldConfig) =>
          logger.warn(s"eu.nomad_lab.LocalEnv overwriting old config with new value")
          privateConfig = Some(newValue)
      }
    }
  }

  private var privateDefaultSettings: Settings = null

  /**
   * default settings, should be initialized on startup
   */
  def defaultSettings: Settings = {
    LocalEnv.synchronized {
      if (privateDefaultSettings == null) {
        privateDefaultSettings = new Settings(defaultConfig)
      }
      privateDefaultSettings
    }
  }

  /**
   * sets the default settings
   */
  def defaultSettings_=(newValue: Settings): Unit = {
    LocalEnv.synchronized {
      if (privateDefaultSettings != null)
        logger.warn(s"eu.nomad_lab.LocalEnv overwriting old settings ${JsonUtils.prettyStr(privateDefaultSettings.toJson)} with ${JsonUtils.prettyStr(newValue.toJson)}")
      privateDefaultSettings = newValue
    }
  }

  def setup(config: Config): Unit = {
    defaultSettings_=(new Settings(config))
  }

  val varToReplaceRe = """\$\{([a-zA-Z][a-zA-Z0-9._]*)\}""".r
  /**
   * replaces ${var} expressions that have a var -> value pair in the replacements with value
   *
   * Repeats replacements up to 3 times to allow the replacements themselves to use variables
   * that need expansion.
   */
  def makeReplacements(repl: collection.Map[String, String], string: String): String = {
    var str: String = string
    for (irecursive <- 1.to(3)) {
      var i: Int = 0
      val s = new mutable.StringBuilder()
      for (m <- varToReplaceRe.findAllMatchIn(str)) {
        val varNow = m.group(1)
        repl.get(varNow) match {
          case Some(value) =>
            s ++= str.substring(i, m.start) // would subSequence be more efficient?
            s ++= value
            i = m.end
          case None =>
            ()
        }
      }
      if (i == 0) {
        return str
      } else {
        s ++= str.substring(i)
        str = s.toString()
      }
    }
    str
  }

  /**
   * recursively deletes a directory (or a file)
   */
  def deleteRecursively(path: Path): Boolean = {
    val pathF = path.toFile()
    var hasErrors: Boolean = false
    if (pathF.isDirectory()) {
      Files.walkFileTree(path, new java.nio.file.SimpleFileVisitor[Path]() {
        import java.nio.file.FileVisitResult
        import java.nio.file.attribute.BasicFileAttributes

        override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
          try {
            Files.delete(file)
          } catch {
            case NonFatal(exc) =>
              logger.warn(s"error while deleting $file in $path: $exc")
              hasErrors = true
          }
          return FileVisitResult.CONTINUE
        }

        override def postVisitDirectory(dir: Path, exc: java.io.IOException): FileVisitResult = {
          if (exc == null) {
            Files.delete(dir)
          } else {
            logger.warn(s"error while deleting $dir in $path: $exc")
            hasErrors = true
          }
          return FileVisitResult.CONTINUE
        }
      })
    } else if (pathF.isFile()) {
      try {
        Files.delete(path)
      } catch {
        case NonFatal(exc) =>
          logger.warn(s"error while deleting $path: $exc")
          hasErrors = true
      }
    } else {
      logger.warn(s"requested recursive delete of non existing path $path")
      hasErrors = true
    }
    !hasErrors
  }

  val directoryPermissions = Set(
    attribute.PosixFilePermission.OWNER_READ, attribute.PosixFilePermission.OWNER_WRITE, attribute.PosixFilePermission.OWNER_EXECUTE,
    attribute.PosixFilePermission.GROUP_READ, attribute.PosixFilePermission.GROUP_WRITE, attribute.PosixFilePermission.GROUP_EXECUTE,
    attribute.PosixFilePermission.OTHERS_READ, attribute.PosixFilePermission.OTHERS_EXECUTE
  ).asJava

  val directoryPermissionsAttributes = attribute.PosixFilePermissions.asFileAttribute(directoryPermissions)

  val filePermissions = Set(
    attribute.PosixFilePermission.OWNER_READ, attribute.PosixFilePermission.OWNER_WRITE,
    attribute.PosixFilePermission.GROUP_READ, attribute.PosixFilePermission.GROUP_WRITE,
    attribute.PosixFilePermission.OTHERS_READ
  ).asJava

  val filePermissionsAttributes = attribute.PosixFilePermissions.asFileAttribute(filePermissions)
}
