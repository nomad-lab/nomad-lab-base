/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import org.{ json4s => jn }
import eu.nomad_lab.JsonUtils
import scala.collection.breakOut
import scala.collection.mutable
import scala.util.matching.Regex
import eu.nomad_lab.h5._
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.resolve.ResolvedRef

/**
 * query expression
 */
sealed abstract class QueryExpression;

object AtomicTerm {
  val nrRe = new Regex("""\A\s*((?<sign>[-+]?)(?:(?<nr>(?:[0-9]+\.?[0-9]*|\.[0-9]+)(?:[eE][-+][0-9]+)?)|(?<inf>[Ii][Nn][Ff](?:[Ii][Nn][Ii][Tt][Yy])?)|(?<nan>[Nn][Aa][Nn])))""", "all", "sign", "nr", "inf", "nan")

  val termRe = new Regex(
    """\A\s*(?:(?<not>[Nn][Oo][Tt])\s+)?(?:(?<quantifier>[aA][nN][yY]|[aA][lL][lL](?:|[dD][aA][tT][aA]|[tT][aA][rR][gG][eE][tT])|[mM][aA][tT][cC][hH][iI][nN][gG])\s+)?(?<metaName>[a-zA-Z_0-9]+)(?:.(?<metaElement>shape|length|value|[cg]_?[Ii]ndex|split))?\s*(?<compareOp>!=|~|==?|<=?|>=?|eq|EQ|mc|MC|ne|NE|lt|LT|le|LE|gt|GT|ge|GE)?\s*""",
    "not", "quantifier", "metaName", "metaElement", "compareOp"
  )

  /**
   * finds the index of the " that closes the string that starts at pos
   * (and the first " at pos - 1)
   */
  def skipString(inStr: String, pos: Int): Int = {
    var i: Int = pos
    while (i < inStr.length) {
      val c = inStr(i)
      if (c == '\\')
        i += 1
      else if (c == '"')
        return i
      i += 1
    }
    throw new InvalidTerm(s"unterminated string starting a $pos in $inStr")
  }

  /**
   * returns the string evaluated and the index of the " that closes the string
   * that starts at pos (and the first " at pos - 1)
   */
  def parseString(inStr: String, pos: Int): (String, Int) = {
    var i: Int = pos
    var j: Int = i
    val s = new mutable.StringBuilder
    while (i < inStr.length) {
      val c = inStr(i)
      if (c == '\\') {
        s ++= inStr.slice(j, i)
        i += 1
        j = i + 1
        val cc = inStr(i)
        val ec = if (cc == 'n')
          '\n'
        else if (cc == 'r')
          '\r'
        else if (cc == 't')
          '\t'
        else if (cc == '\\')
          '\\'
        else if (cc == '"')
          '"'
        else
          throw new InvalidTerm(s"Unknown escape sequence \\$cc")
        s += ec
      } else if (c == '"') {
        s ++= inStr.slice(j, i)
        return s.toString() -> i
      }
      i += 1
    }
    throw new InvalidTerm(s"unterminated string starting a $pos in $inStr")
  }

  def parseJson(inStr: String): (jn.JValue, String) = {
    var i = 0
    while (i < inStr.length && inStr(i).isSpaceChar)
      i += 1
    if (i >= inStr.length)
      return (jn.JNothing, "")
    val firstC = inStr(i)
    if (firstC == '[' || firstC == '{') {
      val stack = mutable.ListBuffer[Char](firstC)
      var j = i + 1
      while (j < inStr.length) {
        val c = inStr(j)
        j += 1
        if (c == '[' || c == '{')
          stack.append(c)
        else if (c == '}') {
          if (stack.isEmpty || stack.last != '{')
            throw new InvalidTerm(s"mismatched braces ${stack.last} $c in $inStr")
          stack.trimEnd(1)
          if (stack.isEmpty)
            return (JsonUtils.parseStr(inStr.slice(i, j)) -> inStr.drop(j))
        } else if (c == ']') {
          if (stack.isEmpty || stack.last != '[')
            throw new InvalidTerm(s"mismatched braces ${stack.last} $c in §inStr")
          stack.trimEnd(1)
          if (stack.isEmpty)
            return (JsonUtils.parseStr(inStr.slice(i, j)) -> inStr.drop(j))
        } else if (c == '"') {
          j = skipString(inStr, j) + 1
        }
      }
      throw new InvalidTerm(s"unclosed braces ${stack} in $inStr")
    } else if (firstC == '"') {
      val (str, strEnd) = parseString(inStr, i + 1)
      jn.JString(str) -> inStr.drop(strEnd + 1)
    } else if (firstC == 'n' && inStr.slice(i, i + 4) == "null") {
      jn.JNull -> inStr.drop(i + 4)
    } else if (firstC == 't' && inStr.slice(i, i + 4) == "true") {
      jn.JBool(true) -> inStr.drop(i + 4)
    } else if (firstC == 'f' && inStr.slice(i, i + 5) == "false") {
      jn.JBool(false) -> inStr.drop(i + 5)
    } else {
      nrRe.findFirstMatchIn(inStr) match {
        case Some(m) =>
          m.group("inf") match {
            case null | "" => ()
            case _ =>
              val value = if (m.group("sign") == "-")
                Double.NegativeInfinity
              else
                Double.PositiveInfinity
              return jn.JDouble(value) -> inStr.drop(m.end)
          }
          m.group("nan") match {
            case null | "" => ()
            case _ =>
              return (jn.JDouble(Double.NaN), inStr.drop(m.end))
          }
          if (m.group("nr").foldLeft(true) { (b, v) => b && v.isDigit })
            jn.JInt(m.group(1).toLong) -> inStr.drop(m.end)
          else
            jn.JDouble(m.group(1).toDouble) -> inStr.drop(m.end)
        case None =>
          var i: Int = 0
          while (i < inStr.length && inStr(i).isSpaceChar) i += 1
          var j: Int = i
          while (j < inStr.length && (inStr(j).isLetterOrDigit || inStr(j) == '_')) j += 1
          if (j > i)
            jn.JString(inStr.slice(i, j)) -> inStr.drop(j)
          else
            jn.JNothing -> inStr.dropWhile(_.isSpaceChar)
      }
    }
  }

  def parseJsonSeq(inStr: String): (Seq[jn.JValue], String) = {
    var res: Seq[jn.JValue] = Seq()
    var hasMore: Boolean = true
    var restStr: String = inStr
    while (hasMore) {
      val (v, rest) = parseJson(restStr)
      restStr = rest
      if (jn.JNothing == v) {
        hasMore = false
      } else {
        res = res :+ v
        var i: Int = 0
        while (i < restStr.length && restStr(i).isSpaceChar) i += 1
        if (i < restStr.length && restStr(i) == ',')
          restStr = restStr.drop(i + 1)
        else
          hasMore = false
      }
    }
    res -> restStr
  }

  def parse(inStr: String): (Option[AtomicTerm], String) = {
    termRe.findFirstMatchIn(inStr) match {
      case Some(m) =>
        var restStr: String = inStr.drop(m.end)
        val invert: Boolean = m.group("not") match {
          case null | "" => false
          case _ => true
        }
        val quantifier: QueryQuantifier.Value = m.group("quantifier") match {
          case null | "" => QueryQuantifier.Any
          case v => QueryQuantifier.fromString(v)
        }
        val metaName: String = m.group("metaName")
        val metaElement: Option[MetaElement.Value] = m.group("metaElement") match {
          case null | "" => None
          case v => Some(MetaElement.fromString(v))
        }
        val compareOp: Option[CompareOp.Value] = m.group("compareOp") match {
          case null | "" => None
          case v => Some(CompareOp.fromString(v))
        }
        val values = if (!compareOp.isEmpty) {
          val (v, r) = parseJsonSeq(restStr)
          restStr = r
          v
        } else {
          Seq()
        }
        Some(AtomicTerm(
          invert = invert,
          quantifier = quantifier,
          metaName = metaName,
          metaElement = metaElement,
          compareOp = compareOp,
          values = values
        )) -> restStr
      case None =>
        None -> inStr
    }
  }
}

/**
 * A filtering term related to a single meta info
 */
case class AtomicTerm(
    invert: Boolean = false,
    quantifier: QueryQuantifier.Value = QueryQuantifier.Any,
    metaName: String,
    metaElement: Option[MetaElement.Value] = None,
    compareOp: Option[CompareOp.Value] = None,
    values: Seq[jn.JValue] = Seq()
) extends QueryExpression {
  final override def equals(other: Any): Boolean = {
    other match {
      case AtomicTerm(i, q, m, mel, op, vv) =>
        val cmp1 = (invert == i && quantifier == q && metaName == m &&
          metaElement == mel && compareOp == op && values.length == vv.length)
        if (cmp1)
          values.zip(vv).foldLeft(true) { case (b, (v1, v2)) => b && JsonUtils.normalizedStr(v1) == JsonUtils.normalizedStr(v2) }
        else
          false
      case _ => false
    }
  }

  def normalizeTerm(metaInfoEnv: MetaInfoEnv): AtomicTerm = {
    if (metaElement.isEmpty || compareOp.isEmpty) {
      val metaInfo = metaInfoEnv.metaInfoRecordForName(metaName) match {
        case Some(mi) => mi
        case None =>
          throw new InvalidTerm(s"could not find meta info $metaName used in '$this'")
      }
      val el = metaElement match {
        case None =>
          metaInfo.kindStr match {
            case "type_section" =>
              MetaElement.Length
            case "type_document_section" =>
              if (compareOp.isEmpty)
                MetaElement.Length
              else
                MetaElement.Data
            case t =>
              throw new InvalidTerm(s"meta info of a term should be either a section or a concrete value, not $t as in '$this'")
          }
        case Some(e) => e
      }
      var vals: Seq[jn.JValue] = values
      val op = compareOp match {
        case Some(o) => o
        case None =>
          el match {
            case MetaElement.Length =>
              if (vals.isEmpty) {
                vals = Seq(jn.JInt(0))
                CompareOp.OpBigger
              } else {
                CompareOp.OpEqual
              }
            case _ =>
              CompareOp.OpEqual
          }
      }
      copy(
        metaElement = Some(el),
        compareOp = Some(op),
        values = vals
      )
    } else {
      this
    }
  }

  override def toString: String = {
    val sb = new mutable.StringBuilder
    if (invert)
      sb ++= "not "
    if (quantifier != QueryQuantifier.Any)
      sb ++= QueryQuantifier.toString(quantifier) ++= " "
    sb ++= metaName
    metaElement match {
      case Some(el) =>
        sb ++= "."
        sb ++= MetaElement.toString(el)
      case None => ()
    }
    compareOp match {
      case Some(op) =>
        sb ++= CompareOp.toString(op)
      case None =>
        if (!values.isEmpty)
          sb ++= " = "
    }
    var first: Boolean = true
    for (v <- values) {
      if (first)
        first = false
      else
        sb ++= ", "
      sb ++= JsonUtils.normalizedStr(v)
    }
    sb.toString
  }

  def jToSeqLong(jValue: jn.JValue): Seq[Long] = {
    jValue match {
      case jn.JInt(n) => Seq(n.toLong)
      case jn.JArray(a) =>
        a.map {
          case jn.JInt(n) => n.toLong
          case v => throw new InvalidTerm(s"expected and integer, but got ${JsonUtils.normalizedStr(v)} in term '$this'")
        }(breakOut)
      case v => throw new InvalidTerm(s"expected and integer or an array of integers, but got ${JsonUtils.normalizedStr(v)} in term '$this'")
    }
  }

  def jToLong(jValue: jn.JValue): Long = {
    jValue match {
      case jn.JInt(n) => n.toLong
      case jn.JArray(jn.JInt(n) :: Nil) => n.toLong
      case v => throw new InvalidTerm(s"expected and integer or an array with a single integer, but got ${JsonUtils.normalizedStr(v)} in term '$this'")
    }
  }

  def jToBool(jValue: jn.JValue): Boolean = {
    def fail() = throw new InvalidTerm(s"expected a boolean (or 0 or 1) or an array with a single boolean (or 0 or 1), but got ${JsonUtils.normalizedStr(jValue)} in term '$this'")

    jValue match {
      case jn.JBool(b) => b
      case jn.JInt(n) =>
        n.toLong match {
          case 0 => false
          case 1 => true
          case v => fail()
        }
      case jn.JArray(jn.JInt(n) :: Nil) =>
        n.toLong match {
          case 0 => false
          case 1 => true
          case v => fail()
        }
      case jn.JArray(jn.JBool(b) :: Nil) => b
      case _ =>
        fail()
    }
  }

  def jToDouble(jValue: jn.JValue): Double = {
    jValue match {
      case jn.JInt(n) => n.toDouble
      case jn.JArray(jn.JInt(n) :: Nil) => n.toDouble
      case jn.JDouble(n) => n
      case jn.JArray(jn.JDouble(n) :: Nil) => n
      case v => throw new InvalidTerm(s"expected a double or an array with a single double, but got ${JsonUtils.normalizedStr(v)} in term '$this'")
    }
  }

  def jToSeqStr(jValue: jn.JValue): Seq[String] = {
    jValue match {
      case jn.JString(s) => Seq(s)
      case jn.JArray(a) =>
        a.map {
          case jn.JString(s) => s
          case v => throw new InvalidTerm(s"expected a string, but got ${JsonUtils.normalizedStr(v)}")
        }(breakOut)
      case v => throw new InvalidTerm(s"expected a string or an array of strings, but got ${JsonUtils.normalizedStr(v)}")
    }
  }

  def jToString(jValue: jn.JValue): String = {
    jValue match {
      case jn.JString(s) => s
      case jn.JArray(jn.JString(s) :: Nil) => s
      case v => throw new InvalidTerm(s"expected a string or an array with a string, but got ${JsonUtils.normalizedStr(v)}")
    }
  }

  /**
   * If this term requires or excludes its metaName
   */
  def metaInfoStatus: MetaStatus.Value = {
    metaElement match {
      case Some(MetaElement.Length) =>
        compareOp match {
          case None =>
            if (invert)
              MetaStatus.Excluded
            else
              MetaStatus.Required
          case Some(op) =>
            val targets = values.map(jToLong)
            if (targets.isEmpty)
              throw new InvalidTerm(s"missing targets after operation in '$this'")
            var includeAll: Boolean = false
            var includeSome: Boolean = false
            var excludeAll: Boolean = false
            var impossible: Boolean = false
            val requireAll = quantifier == QueryQuantifier.AllTarget || quantifier == QueryQuantifier.All || quantifier == QueryQuantifier.Matching
            op match {
              case CompareOp.OpSmallerEqual =>
                excludeAll = if (requireAll)
                  targets.filter(_ >= 0).length == targets.length
                else
                  !targets.filter(_ >= 0).isEmpty
                includeSome = if (requireAll)
                  targets.filter(_ > 0).length == targets.length
                else
                  !targets.filter(_ > 0).isEmpty
                impossible = !targets.filter(_ < 0).isEmpty
              case CompareOp.OpEqual =>
                if (requireAll && targets.toSet.size > 1) {
                  impossible = true
                } else {
                  excludeAll = !targets.filter(_ == 0).isEmpty
                  includeSome = !targets.filter(_ > 0).isEmpty
                  impossible = !targets.filter(_ < 0).isEmpty
                }
              case CompareOp.OpBigger =>
                includeAll = if (requireAll)
                  targets.filter(_ < 1).length == targets.length
                else
                  !targets.filter(_ < 1).isEmpty
                includeSome = !targets.isEmpty
                excludeAll = if (requireAll)
                  targets.filter(_ < 0).length == targets.length
                else
                  !targets.filter(_ < 0).isEmpty
              case CompareOp.OpBiggerEqual =>
                includeAll = if (requireAll)
                  targets.filter(_ < 2).length == targets.length
                else
                  !targets.filter(_ < 2).isEmpty
                includeSome = !targets.isEmpty
                excludeAll = if (requireAll)
                  targets.filter(_ < 1).length == targets.length
                else
                  !targets.filter(_ < 1).isEmpty
              case CompareOp.OpNotEqual =>
                excludeAll = if (requireAll)
                  targets.filter(_ != 0).length == targets.length
                else
                  !targets.filter(_ != 0).isEmpty
                includeAll = if (requireAll)
                  targets.filter(_ <= 0).length == targets.length
                else
                  !targets.filter(_ <= 0).isEmpty || targets.toSet.size > 1
                includeSome = !targets.isEmpty
            }
            if (includeAll) {
              if (excludeAll) {
                if (invert)
                  MetaStatus.Impossible
                else
                  MetaStatus.Unknown
              } else {
                if (invert)
                  MetaStatus.Excluded
                else
                  MetaStatus.Required
              }
            } else if (includeSome) {
              if (excludeAll) {
                if (invert)
                  MetaStatus.Required
                else
                  MetaStatus.Unknown
              } else {
                if (invert)
                  MetaStatus.Unknown
                else
                  MetaStatus.Required
              }
            } else {
              if (excludeAll) {
                if (invert)
                  MetaStatus.Required
                else
                  MetaStatus.Excluded
              } else {
                if (invert)
                  MetaStatus.Unknown
                else
                  MetaStatus.Impossible
              }
            }
        }
      case _ =>
        if (!invert)
          MetaStatus.Required
        else if (compareOp.isEmpty)
          MetaStatus.Excluded
        else
          MetaStatus.Unknown
    }
  }
}

/**
 * Filtering term consisting of a logical "and" conjunction between all terms
 */
case class AndConjunction(
    terms: Seq[AtomicTerm]
) extends QueryExpression {
  override def toString: String = {
    val sb = new mutable.StringBuilder
    var first: Boolean = true
    for (t <- terms) {
      if (first)
        first = false
      else
        sb ++= ", "
      sb ++= t.toString
    }
    sb.toString
  }
}

object AndConjunction {
  val andRe = """\A\s*(?:[aA][nN][dD]\s+|&\s*)""".r

  /**
   * parses an and conjunction
   */
  def parse(firstTerm: AtomicTerm, inStr: String): (Option[AndConjunction], String) = {
    var terms: Seq[AtomicTerm] = Seq(firstTerm)
    var restStr = inStr
    while (true) {
      andRe.findFirstMatchIn(restStr) match {
        case Some(m) =>
          restStr = restStr.drop(m.end)
          val (t, r) = AtomicTerm.parse(restStr)
          restStr = r
          t match {
            case Some(term) =>
              terms = terms :+ term
            case None =>
              if (inStr.foldLeft(true) { case (b, c) => b && c.isSpaceChar })
                throw new InvalidTerm(s"missing term after and in $inStr")
              else
                throw new InvalidTerm(s"could not parse term after and in $inStr")
          }
        case None =>
          return Some(AndConjunction(terms)) -> restStr
      }
    }
    None -> inStr // never reached...
  }
}

object QueryExpression {
  val traceFiltering: Boolean = false

  /**
   * Parses a string into a query expression
   */
  def parseExpression(queryStr: String): QueryExpression = {
    val (exp, rest) = parse(queryStr)
    if (!rest.foldLeft(true) { (b, c) => b && c.isSpaceChar })
      throw new InvalidTerm(s"Could not parse all query string, '$rest' left over")
    exp match {
      case Some(e) => e
      case None => throw new InvalidTerm(s"empty query string") // accept?
    }
  }

  def apply(queryStr: String): QueryExpression = parseExpression(queryStr)

  def parse(queryStr: String): (Option[QueryExpression], String) = {
    val (term1, restStr) = AtomicTerm.parse(queryStr)
    term1 match {
      case Some(t) =>
        AndConjunction.andRe.findFirstMatchIn(restStr) match {
          case Some(_) =>
            AndConjunction.parse(t, restStr)
          case None =>
            term1 -> restStr
        }
      case None =>
        None -> restStr
    }
  }
}
