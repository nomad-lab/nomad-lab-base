/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

/**
 * comparison operation allowed in an atomic query term
 */
object CompareOp extends Enumeration {
  type CompareOp = Value
  val OpEqual, OpMatch, OpNotEqual, OpSmaller, OpSmallerEqual, OpBigger, OpBiggerEqual = Value

  /**
   * The symbol corresponding to this operation
   */
  def toString(op: CompareOp): String = {
    op match {
      case OpMatch => "~"
      case OpEqual => "="
      case OpNotEqual => "!="
      case OpSmaller => "<"
      case OpSmallerEqual => "<="
      case OpBigger => ">"
      case OpBiggerEqual => ">="
    }
  }

  /**
   * Converts a symbol into a CompareOp
   */
  def fromString(op: String): CompareOp = {
    op match {
      case "=" | "==" | "eq" | "EQ" => OpEqual
      case "~" | "mc" | "MC" => OpMatch
      case "!=" | "ne" | "NE" => OpNotEqual
      case "<" | "lt" | "LT" => OpSmaller
      case "<=" | "le" | "LE" => OpSmallerEqual
      case ">" | "gt" | "GT" => OpBigger
      case ">=" | "ge" | "GE" => OpBiggerEqual
      case s => throw new InvalidTerm(s"Unknown CompareOp $s")
    }
  }

  val compareRe = """\A\s*(!=|~|==?|<=?|=?>|eq|EQ|mc|MC|ne|NE|lt|LT|le|LE|gt|GT|ge|GE)\s*""".r
  def parse(inStr: String): (Option[CompareOp], String) = {
    compareRe.findFirstMatchIn(inStr) match {
      case Some(m) =>
        (Some(fromString(m.group(1))), inStr.drop(m.end))
      case None =>
        (None, inStr)
    }
  }

}
