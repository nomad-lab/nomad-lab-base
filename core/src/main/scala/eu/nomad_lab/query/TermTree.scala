/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import eu.nomad_lab.h5._
import com.typesafe.scalalogging.LazyLogging

/**
 * a SectionCollectionFilter that collects several compiled terms in a form that
 * reflects the sections tree
 */
class TermTree(
    val sectionMetaName: String,
    var terms: Seq[CompiledSectionTerm] = Seq(),
    var subTrees: Map[String, TermTree] = Map()
) extends SectionCollectionFilter with LazyLogging {
  override def toString: String = {
    s"""TermTree($sectionMetaName,
      terms: ${terms.mkString("[", ",", "]")},
      subTrees: ${subTrees.mkString("[", ",", "]")}
    )"""
  }

  def insertTerm(path: List[String], term: CompiledSectionTerm): Unit = {
    if (path.isEmpty) {
      terms = terms :+ term
    } else {
      val subTree = subTrees.get(path.head) match {
        case Some(subT) => subT
        case None =>
          val subT = new TermTree(path.head)
          subTrees = subTrees + (path.head -> subT)
          subT
      }
      subTree.insertTerm(path.tail, term)
    }
  }

  /**
   * currently a stupid execution without trying to prioritize any filters
   */
  def evaluateLocalOnSectionCollection(collection: SectionCollectionH5): Boolean = {
    if (QueryExpression.traceFiltering)
      logger.debug(s"evaluating $this on $collection")
    for (v <- terms) {
      if (!v.evaluateOnSectionCollection(collection))
        return false
    }
    true
  }

  /**
   * currently a stupid execution without trying to prioritize any filters
   */
  override def evaluateOnSectionCollection(collection: SectionCollectionH5): Boolean = {
    if (!evaluateLocalOnSectionCollection(collection))
      return false
    for ((k, v) <- subTrees) {
      if (!v.evaluateOnSectionCollection(collection.subSectionCollection(k)))
        return false
    }
    true
  }

  /**
   * currently a stupid execution without trying to prioritize any filters
   */
  def evaluateLocalOnSectionIterable(iterable: Iterable[SectionH5]): Boolean = {
    for (v <- terms) {
      if (!v.evaluateOnSectionIterable(iterable))
        return false
    }
    true
  }

  override def evaluateOnValueCollection(collection: ValueCollectionH5): Boolean = {
    throw new QueryError(s"evaluateOnValueCollection cannot be evaluated on composite TermTree")
  }

  /**
   * currently a stupid execution without trying to prioritize any filters
   */
  override def evaluateOnSectionIterable(iterable: Iterable[SectionH5]): Boolean = {
    if (!evaluateLocalOnSectionIterable(iterable))
      return false
    for ((k, v) <- subTrees) {
      if (!v.evaluateOnSectionIterable(iterable.flatMap(_.subSectionCollection(k))))
        return false
    }
    true
  }
}
