/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query
import eu.nomad_lab.resolve.ResolvedRef
import org.{ json4s => jn }
import eu.nomad_lab.meta

/**
 * An operation that scans the results of the query
 *
 * This is a kind of loop body, for a loop on the results of a query
 */
abstract class QueryScanOp {
  def metaInfoEnv: meta.MetaInfoEnv

  def context: String

  def filter: Filter

  def workOnContext(context: ResolvedRef): Unit

  def cleanup(): Unit

  def workerJValue: jn.JValue

  def jValue: jn.JValue = {
    jn.JObject(
      ("type" -> jn.JString(this.getClass.getName)) ::
        ("context" -> jn.JString(context)) ::
        ("filter" -> filter.jValue) ::
        ("worker" -> workerJValue) :: Nil
    )
  }
}
