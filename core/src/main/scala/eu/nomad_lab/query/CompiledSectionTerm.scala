/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import org.{ json4s => jn }
import scala.collection.breakOut
import eu.nomad_lab.h5._
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import scala.util.control.NonFatal
import com.typesafe.scalalogging.LazyLogging

/**
 * A term that has that can be evaluated on a section collection
 */
abstract class CompiledSectionTerm(
    val term: AtomicTerm,
    val metaInfo: MetaInfoRecord
) extends SectionCollectionFilter with LazyLogging {
  def comparer: AnyRef

  override def toString: String = {
    s"""${this.getClass().getName()}($term)"""
  }
}

/**
 * Construct a compiled term that can be evaluated on a section collection of the
 * sectionMetaName
 */
object CompiledSectionTerm extends LazyLogging {
  import QueryExpression.traceFiltering

  def apply(
    metaInfoEnv: MetaInfoEnv,
    term: AtomicTerm
  ): CompiledSectionTerm = {
    metaInfoEnv.metaInfoRecordForName(term.metaName) match {
      case Some(metaInfo) =>
        metaInfo.kindStr match {
          case "type_section" =>
            term.metaElement match {
              case None =>
                if (term.compareOp == None)
                  apply(metaInfoEnv, term.copy(
                    metaElement = Some(MetaElement.Length),
                    compareOp = Some(CompareOp.OpNotEqual),
                    values = Seq(jn.JInt(0))
                  ))
                else
                  apply(metaInfoEnv, term.copy(metaElement = Some(MetaElement.CIndex)))
              case Some(el) => el match {
                case MetaElement.Length =>
                  new CompiledSectionTerm(term, metaInfo) {
                    val comparer = new BaseComparer[Long](
                      term = term,
                      metaInfo = metaInfo,
                      targetVals = term.values.map(term.jToLong)(breakOut)
                    )

                    val iterEval = comparer.evalWithIterator

                    override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                      if (traceFiltering) logger.debug(s"$term sectionCollection ${sectionCollection.table.name} ${sectionCollection.lengthL}")
                      iterEval(Iterator(sectionCollection.lengthL))
                    }

                    override def evaluateOnSectionIterable(iterable: Iterable[SectionH5]): Boolean = {
                      if (traceFiltering) logger.debug(s"$term sectionIterable")
                      iterEval(Iterator(iterable.size.toLong)) // iterate explicitly ?
                    }

                    override def evaluateOnValueCollection(collection: ValueCollectionH5): Boolean = {
                      throw new QueryError(s"Internal error, cannot evaluate the section filter '$term' on a value collection")
                    }

                    val sectionMetaName = metaInfo.name
                  }
                case MetaElement.Shape =>
                  throw new InvalidTerm(s".shape makes no sense for sections in '$term'")
                case MetaElement.Data =>
                  throw new InvalidTerm(s".value makes no sense for sections in '$term'")
                case MetaElement.CIndex =>
                  new CompiledSectionTerm(term, metaInfo) {
                    val comparer = new BaseComparer[Long](
                      term = term,
                      metaInfo = metaInfo,
                      targetVals = term.values.map(term.jToLong)(breakOut)
                    )
                    val iterEval = comparer.evalWithIterator

                    override def evaluateOnSectionIterable(iterable: Iterable[SectionH5]): Boolean = {
                      iterEval(iterable.iterator.map(_.gIndex))
                    }

                    override def evaluateOnValueCollection(collection: ValueCollectionH5): Boolean = {
                      throw new QueryError(s"Internal error, cannot evaluate the section filter '$term' on a value collection")
                    }

                    val sectionMetaName = metaInfo.name
                  }
              }
            }
          case "type_document_content" =>
            val parentSection = metaInfoEnv.parentSectionName(term.metaName) match {
              case Some(s) => s
              case None => throw new InvalidTerm(s"concrete meta info ${term.metaName} is in no section in '$term'")
            }
            term.metaElement match {
              case None =>
                if (term.compareOp == None)
                  apply(metaInfoEnv, term.copy(
                    metaElement = Some(MetaElement.Length),
                    compareOp = Some(CompareOp.OpNotEqual),
                    values = Seq(jn.JInt(0))
                  ))
                else
                  apply(metaInfoEnv, term.copy(metaElement = Some(MetaElement.Data)))
              case Some(el) => el match {
                case MetaElement.Length =>
                  new CompiledSectionTerm(term, metaInfo) {
                    val comparer = new BaseComparer[Long](
                      term = term,
                      metaInfo = metaInfo,
                      targetVals = term.values.map(term.jToLong)(breakOut)
                    )
                    val iterEval = comparer.evalWithIterator

                    override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                      val valueCollection = sectionCollection.subValueCollection(term.metaName)
                      if (traceFiltering) logger.debug(s"$term sectionCollection ${sectionCollection.table.name} -> valueCollection ${valueCollection.table.name} ${valueCollection.lengthL}")
                      iterEval(Iterator(valueCollection.lengthL))
                    }

                    override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                      if (traceFiltering) logger.debug(s"$term sectionIterable")
                      iterEval(Iterator(sectionIterable.foldLeft(0L) {
                        case (tot, sect) =>
                          tot + sect.valueCollection(term.metaName).lengthL
                      }))
                    }

                    override def evaluateOnValueCollection(collection: ValueCollectionH5): Boolean = {
                      if (traceFiltering) logger.debug(s"$term valueCollection ${collection.table.name} ${collection.lengthL}")
                      iterEval(Iterator(collection.lengthL))
                    }

                    val sectionMetaName = parentSection
                  }
                case MetaElement.Shape =>
                  {
                    new CompiledSectionTerm(term, metaInfo) {
                      import BaseComparer.seq2ordered
                      val comparer = new BaseComparer[Seq[Long]](
                        term = term,
                        metaInfo = metaInfo,
                        targetVals = term.values.map(term.jToSeqLong)(breakOut)
                      )
                      val iterEval = comparer.evalWithIterator
                      override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                        val valueCollection = sectionCollection.subValueCollection(term.metaName)
                        evaluateOnValueCollection(valueCollection)
                      }

                      override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                        val iter = sectionIterable.flatMap { sect =>
                          sect.valueCollection(term.metaName).map { _.arrayShape }
                        }
                        iterEval(iter.iterator)
                      }

                      override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                        iterEval(valueCollection.iterator.map { _.arrayShape })
                      }

                      val sectionMetaName = parentSection
                    }
                  }
                case MetaElement.Data =>
                  if (metaInfo.shape.getOrElse(Seq()) != Seq())
                    throw new InvalidTerm(s"Value comparising for non scalar values not yet implemented for '$term'")
                  metaInfo.dtypeStr.getOrElse("") match {
                    case "f" | "f64" | "f32" =>
                      new CompiledSectionTerm(term, metaInfo) {
                        val comparer = new BaseComparer[Double](
                          term = term,
                          metaInfo = metaInfo,
                          targetVals = term.values.map(term.jToDouble)(breakOut)
                        )
                        val iterEval = comparer.evalWithIterator

                        override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                          val valueCollection = sectionCollection.subValueCollection(term.metaName)
                          evaluateOnValueCollection(valueCollection)
                        }

                        override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                          val iter = sectionIterable.flatMap { sect =>
                            sect.valueCollection(term.metaName).map { _.doubleValue }
                          }
                          iterEval(iter.iterator)
                        }

                        override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                          iterEval(valueCollection.iterator.map { _.doubleValue })
                        }

                        val sectionMetaName = parentSection
                      }
                    case "r" | "i" | "i64" | "i32" =>
                      new CompiledSectionTerm(term, metaInfo) {
                        val comparer = new BaseComparer[Long](
                          term = term,
                          metaInfo = metaInfo,
                          targetVals = term.values.map(term.jToLong)(breakOut)
                        )
                        val iterEval = comparer.evalWithIterator

                        override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                          val valueCollection = sectionCollection.subValueCollection(term.metaName)
                          evaluateOnValueCollection(valueCollection)
                        }

                        override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                          val iter = sectionIterable.flatMap { sect =>
                            sect.valueCollection(term.metaName).map { _.longValue }
                          }
                          iterEval(iter.iterator)
                        }

                        override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                          iterEval(valueCollection.iterator.map { _.longValue })
                        }

                        val sectionMetaName = parentSection
                      }
                    case "b" =>
                      new CompiledSectionTerm(term, metaInfo) {
                        val comparer = new BaseComparer[Boolean](
                          term = term,
                          metaInfo = metaInfo,
                          targetVals = term.values.map(term.jToBool)(breakOut)
                        )
                        val iterEval = comparer.evalWithIterator

                        override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                          val valueCollection = sectionCollection.subValueCollection(term.metaName)
                          evaluateOnValueCollection(valueCollection)
                        }

                        override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                          val iter = sectionIterable.flatMap { sect =>
                            sect.valueCollection(term.metaName).map { _.boolValue }
                          }
                          iterEval(iter.iterator)
                        }

                        override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                          iterEval(valueCollection.iterator.map { _.boolValue })
                        }

                        val sectionMetaName = parentSection
                      }
                    case "C" =>
                      if (term.compareOp == CompareOp.OpMatch) {
                        new CompiledSectionTerm(term, metaInfo) {
                          val targetStr = term.values.map(term.jToString)(breakOut)
                          if (targetStr.length != 1)
                            throw new InvalidTerm(s"regex matching expect exactly one string, not ${targetStr.length} as in '$term'")
                          val comparer = new RegexMatcher(
                            term = term,
                            metaInfo = metaInfo,
                            targetRegex = try {
                            targetStr.head.r
                          } catch {
                            case NonFatal(e) =>
                              throw new InvalidTerm(s"regexp is invalid in term '$term': $e")
                          }
                          )

                          val iterEval = comparer.evalWithIterator
                          override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                            val valueCollection = sectionCollection.subValueCollection(term.metaName)
                            evaluateOnValueCollection(valueCollection)
                          }

                          override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                            val iter = sectionIterable.flatMap { sect =>
                              sect.valueCollection(term.metaName).map { _.stringValue }
                            }
                            iterEval(iter.iterator)
                          }

                          override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                            iterEval(valueCollection.iterator.map { _.stringValue })
                          }

                          val sectionMetaName = parentSection
                        }
                      } else {
                        new CompiledSectionTerm(term, metaInfo) {
                          val comparer = new BaseComparer[String](
                            term = term,
                            metaInfo = metaInfo,
                            targetVals = term.values.map(term.jToString)(breakOut)
                          )
                          val iterEval = comparer.evalWithIterator

                          override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                            val valueCollection = sectionCollection.subValueCollection(term.metaName)
                            evaluateOnValueCollection(valueCollection)
                          }

                          override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                            val iter = sectionIterable.flatMap { sect =>
                              sect.valueCollection(term.metaName).map { _.stringValue }
                            }
                            iterEval(iter.iterator)
                          }

                          override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                            iterEval(valueCollection.iterator.map { _.stringValue })
                          }

                          val sectionMetaName = parentSection
                        }
                      }
                    case "" =>
                      throw new InvalidTerm(s"Comparison with value having no dtypeStr in '$term'")
                    case t =>
                      throw new InvalidTerm(s"Comparison with values of type $t not supported in '$term'")
                  }
                case MetaElement.CIndex =>
                  new CompiledSectionTerm(term, metaInfo) {
                    val comparer = new BaseComparer[Long](
                      term = term,
                      metaInfo = metaInfo,
                      targetVals = term.values.map(term.jToLong)(breakOut)
                    )
                    val iterEval = comparer.evalWithIterator

                    override def evaluateOnSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
                      val valueCollection = sectionCollection.subValueCollection(term.metaName)
                      evaluateOnValueCollection(valueCollection)
                    }

                    override def evaluateOnSectionIterable(sectionIterable: Iterable[SectionH5]): Boolean = {
                      val iter = sectionIterable.flatMap { sect =>
                        sect.valueCollection(term.metaName).map { _.gIndex }
                      }
                      iterEval(iter.iterator)
                    }

                    override def evaluateOnValueCollection(valueCollection: ValueCollectionH5): Boolean = {
                      iterEval(valueCollection.iterator.map(_.gIndex))
                    }

                    val sectionMetaName = parentSection
                  }
              }
            }
          case t =>
            throw new InvalidTerm(s"only section or concrete value meta info is supported, not $t, used in '$term'")
        }
      case None =>
        throw new InvalidTerm(s"could not find metaInfo ${term.metaName} used in '$term'")
    }

  }
}
