/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

object MetaElement extends Enumeration {
  type MetaElement = Value
  val Data, Shape, Length, CIndex, Split = Value

  def toString(metaElement: MetaElement): String = {
    metaElement match {
      case Data =>
        "value"
      case Shape =>
        "shape"
      case Length =>
        "length"
      case CIndex =>
        "c_index"
      case Split =>
        "split"
    }
  }

  def fromString(metaElement: String): MetaElement = {
    if (metaElement.compareToIgnoreCase("value") == 0)
      Data
    else if (metaElement.compareToIgnoreCase("shape") == 0)
      Shape
    else if (metaElement.compareToIgnoreCase("length") == 0)
      Length
    else if (Seq("c_index", "cIndex", "g_index", "gIndex").foldLeft(false) { (b: Boolean, x: String) => b || metaElement.compareToIgnoreCase(x) == 0 })
      CIndex
    else if (metaElement.compareToIgnoreCase("split") == 0)
      Split
    else
      throw new InvalidTerm(s"MetaElement $metaElement unknown")
  }
}
