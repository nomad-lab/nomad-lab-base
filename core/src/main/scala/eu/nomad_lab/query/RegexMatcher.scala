/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import scala.util.matching.Regex
import eu.nomad_lab.meta.MetaInfoRecord

/**
 * An helper object that generates code to evaluate a term consisting
 * of a regular expression match against a string iterator
 */
class RegexMatcher(
    val term: AtomicTerm,
    val metaInfo: MetaInfoRecord,
    val targetRegex: Regex
) {
  val ok: Boolean = !term.invert

  def eval(v: String): Boolean = {
    !targetRegex.findFirstMatchIn(v).isEmpty
  }

  def requireAny(vals: Iterator[String]): Boolean = {
    for (v <- vals)
      if (eval(v))
        return ok
    !ok
  }

  def requireAllValues(vals: Iterator[String]): Boolean = {
    for (v <- vals)
      if (!eval(v))
        return !ok
    ok
  }

  def matching(vals: Iterator[String]): Boolean = {
    if (!vals.hasNext || targetRegex.findFirstMatchIn(vals.next).isEmpty || vals.hasNext)
      !ok
    else
      ok
  }

  def evalWithIterator: (Iterator[String]) => Boolean = {
    term.quantifier match {
      case QueryQuantifier.Any =>
        requireAny
      case QueryQuantifier.AllData =>
        requireAllValues
      case QueryQuantifier.AllTarget =>
        requireAny
      case QueryQuantifier.All =>
        requireAllValues
      case QueryQuantifier.Matching =>
        matching
    }
  }
}
