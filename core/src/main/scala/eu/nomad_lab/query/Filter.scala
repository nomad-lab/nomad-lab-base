/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import eu.nomad_lab.h5._
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.meta
import org.{ json4s => jn }

object Filter {
  def fromJValue(jValue: jn.JValue, metaInfoEnv: Option[meta.MetaInfoEnv] = None): Filter = {
    jValue \ "type" match {
      case jn.JString(t) => t match {
        case "query" =>
          CompiledQuery.fromJValue(jValue, metaInfoEnv)
        case "YesFilter" =>
          YesFilter
        case "NoFilter" =>
          NoFilter
        case s =>
          throw new QueryError(s"Do not know how to generate Filter for type $s, '${JsonUtils.normalizedStr(jValue)}'")
      }
      case jn.JNothing =>
        throw new QueryError(s"Do not know how to generate Filter for object without type entry, '${JsonUtils.normalizedStr(jValue)}'")
      case v =>
        throw new QueryError(s"type in filter should be a string, not ${JsonUtils.normalizedStr(v)} in '${JsonUtils.normalizedStr(jValue)}'")
    }
  }
}

/**
 * A high level object that pre filters archives and calculations and context
 */
abstract class Filter {
  def jValue: jn.JValue
  def filterArchive(archive: ArchiveH5): Boolean
  def filterCalculation(calculation: CalculationH5): Boolean
  def filterContext(context: ResolvedRef): Boolean
}

object YesFilter extends Filter {
  def jValue: jn.JValue = {
    jn.JObject(
      ("type" -> jn.JString("YesFilter")) :: Nil
    )
  }

  def filterArchive(archive: ArchiveH5): Boolean = true

  def filterCalculation(calculation: CalculationH5): Boolean = true

  def filterContext(context: ResolvedRef): Boolean = true
}

object NoFilter extends Filter {
  def jValue: jn.JValue = {
    jn.JObject(
      ("type" -> jn.JString("NoFilter")) :: Nil
    )
  }

  def filterArchive(archive: ArchiveH5): Boolean = false

  def filterCalculation(calculation: CalculationH5): Boolean = false

  def filterContext(context: ResolvedRef): Boolean = false
}
