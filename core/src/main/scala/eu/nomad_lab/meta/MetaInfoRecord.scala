/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import org.json4s.CustomSerializer
import scala.collection.breakOut
import eu.nomad_lab.JsonUtils
import com.typesafe.scalalogging.StrictLogging

/**
 * Represents a piece of nomad meta info referring to other meta info by name.
 *
 * Can be interpreted within a context [[eu.nomad_lab.MetaInfoEnv]], but matches the
 * preferred way one describes it in a json file.
 */
case class MetaInfoRecord(
    val name: String,
    val kindStr: String,
    val description: String,
    val superNames: Seq[String] = Seq(),
    val units: Option[String] = None,
    val dtypeStr: Option[String] = None,
    val repeats: Option[Boolean] = None,
    val shape: Option[Seq[Either[Long, String]]] = None,
    val gid: String = "",
    val superGids: Seq[String] = Seq(),
    val redundant: Option[Seq[String]] = None,
    val derived: Option[Boolean] = None,
    val referencedSections: Option[Seq[String]] = None,
    val otherKeys: List[JField] = Nil
) {

  /**
   * returns a JValue (json) representation of the current record
   *
   * It is possible to control if the extra arguments are output
   * just like other fields, as a sub dictionary or not at all
   */
  def toJValue(extraArgs: Boolean = true, inlineExtraArgs: Boolean = true): JValue = {
    import org.json4s.JsonDSL._;
    val jShape = shape match {
      case None => JNothing
      case Some(s) =>
        val listShape: List[JValue] = s.map {
          case Left(i) => JInt(i)
          case Right(s) => JString(s)
        }(breakOut)
        JArray(listShape)
    }
    val baseObj = (
      ("name" -> name) ~
      ("gid" -> (if (gid.isEmpty) None else Some(gid))) ~
      ("kindStr" -> (if (kindStr == "type_document_content") None else Some(kindStr))) ~
      ("description" -> description) ~
      ("superNames" -> superNames) ~
      ("superGids" -> (if (superGids.isEmpty) None else Some(superGids))) ~
      ("units" -> units) ~
      ("dtypeStr" -> dtypeStr) ~
      ("repeats" -> repeats) ~
      ("shape" -> jShape) ~
      ("redundant" -> redundant) ~
      ("referencedSections" -> referencedSections) ~
      ("derived" -> derived)
    );
    if (extraArgs) {
      if (inlineExtraArgs)
        JObject(baseObj.obj ++ otherKeys)
      else
        baseObj ~ ("extraArgs" -> JObject(otherKeys))
    } else {
      baseObj
    }
  }

  override def toString(): String = JsonUtils.prettyStr(toJValue())
}

object MetaInfoRecord extends StrictLogging {
  /**
   * valid dtypes
   * f: generic float (not specified, default dependent on settings)
   * i: generic integer (not specified, default dependent on settings)
   * f32: 32 bit floating point (single precision)
   * i32: signed 32 bit integer
   * f64: 64 bit floating point (double precision)
   * i64: 64 bit signed integer
   * b: byte
   * B: byte array (blob)
   * C: unicode string
   * D: a json dictionary (currently not very efficient)
   *
   * Should probably be migrated to an Enumeration.
   */
  final val dtypes = Seq("f", "i", "f32", "i32", "f64", "i64", "b", "B", "C", "D", "r")

  def fromMetaInfoEntry(e: MetaInfoEntry): MetaInfoRecord = {
    val kindStr = e.meta_type match {
      case MetaType.type_value => "type_document_content"
      case MetaType.type_section => "type_section"
      case MetaType.type_dimension => "type_dimension"
      case MetaType.type_abstract => "type_abstract_document_content"
    }
    val superNames = e.meta_parent_section.toSeq ++ e.meta_abstract_types
    val dtypeStrMap = Map(
      MetaDataType.float -> "f",
      MetaDataType.int -> "i",
      MetaDataType.float32 -> "f32",
      MetaDataType.int32 -> "i32",
      MetaDataType.float64 -> "f64",
      MetaDataType.int64 -> "i64",
      MetaDataType.boolean -> "b",
      MetaDataType.string -> "C",
      MetaDataType.json -> "D",
      MetaDataType.reference -> "r"
    )
    e match {
      case MetaInfoValue(
        meta_name,
        meta_description,
        meta_parent_sectionStr,
        meta_data_type,
        meta_deprecated,
        meta_gid,
        meta_parent_section_gid,
        meta_abstract_types,
        meta_abstract_types_gids,
        meta_dimension,
        meta_enum,
        meta_enum_from,
        meta_query_enum,
        meta_range_expected,
        meta_repeats,
        meta_required,
        meta_referenced_section,
        meta_value_validate,
        meta_units,
        otherKeys) =>
        val shape: Seq[Either[Long, String]] = meta_dimension.flatMap {
          el: MetaDimension =>
            el.meta_dimension_fixed match {
              case Some(n) =>
                if (!el.meta_dimension_symbolic.isEmpty)
                  logger.warn(s"Both meta_dimension_symbolic and meta_dimension_fixed are given, meta_dimension_fixed hat priority ($el) in $meta_name")
                Some(Left(n))
              case None =>
                el.meta_dimension_symbolic match {
                  case Some(d) =>
                    Some(Right(d))
                  case None =>
                    logger.warn(s"Neither meta_dimension_symbolic nor meta_dimension_fixed are given, ignoring $el in $meta_name")
                    None
                }
            }
        }(breakOut)
        val otherKeys2: List[JField] =
          ("meta_deprecated" -> (if (meta_deprecated) JBool(true) else JNothing)) ::
            ("values" -> (if (meta_enum.isEmpty)
              JNothing
            else
              JObject(meta_enum.map {
                case MetaEnum(
                  meta_enum_value,
                  meta_enum_description) =>
                  meta_enum_value -> JString(meta_enum_description)
              }.sortBy(_._1)))) ::
            ("meta_enum_from" -> (meta_enum_from match {
              case Some(s) => JString(s)
              case None => JNothing
            })) ::
            ("meta_query_enum" -> (if (meta_query_enum.isEmpty)
              JNothing
            else
              JArray(meta_query_enum.map { el => el.toJValue }))) ::
            ("meta_range_expected" -> (if (meta_range_expected.isEmpty)
              JNothing
            else
              JArray(meta_range_expected.map { el => el.toJValue }))) ::
            ("meta_required" -> (if (meta_required)
              JBool(true)
            else
              JNothing)) ::
            ("meta_value_validate" -> (meta_value_validate match {
              case None => JNothing
              case Some(el) => JString(el)
            })) :: otherKeys
        MetaInfoRecord(
          name = meta_name,
          kindStr = kindStr,
          description = meta_description,
          superNames = superNames,
          units = meta_units,
          dtypeStr = dtypeStrMap.get(meta_data_type),
          repeats = (if (meta_repeats) Some(true) else None),
          shape = (if (shape.isEmpty) None else Some(shape)),
          gid = meta_gid.getOrElse(""),
          superGids = meta_parent_section_gid.toSeq ++ meta_abstract_types_gids,
          redundant = None,
          derived = None,
          referencedSections = meta_referenced_section.map { el => Seq(el) },
          otherKeys = otherKeys2
        )
      case MetaInfoSection(
        meta_name,
        meta_description,
        meta_deprecated,
        meta_gid,
        meta_parent_section,
        meta_parent_section_gid,
        meta_abstract_types,
        meta_abstract_types_gids,
        meta_repeats,
        meta_required,
        meta_chosen_key,
        meta_context_identifier,
        meta_inject_if_section_regexp,
        meta_inject_if_abstract_type,
        otherKeys
        ) =>
        val otherKeys2: List[JField] =
          ("meta_deprecated" -> (if (meta_deprecated) JBool(true) else JNothing)) ::
            ("meta_required" -> (if (meta_required)
              JBool(true)
            else
              JNothing)) ::
            ("meta_chosen_key" -> (meta_chosen_key match {
              case Some(s) => JString(s)
              case None => JNothing
            })) ::
            ("meta_inject_if_section_regexp" -> (if (meta_inject_if_section_regexp.isEmpty)
              JNothing
            else
              JArray(meta_inject_if_section_regexp.map { el => JString(el) }))) ::
            ("meta_inject_if_abstract_type" -> (if (meta_inject_if_abstract_type.isEmpty)
              JNothing
            else
              JArray(meta_inject_if_abstract_type.map { el => JString(el) }))) ::
            ("meta_context_identifier" -> (if (meta_context_identifier.isEmpty)
              JNothing
            else
              JArray(meta_context_identifier.map { el => JString(el) }))) :: otherKeys
        MetaInfoRecord(
          name = meta_name,
          kindStr = kindStr,
          description = meta_description,
          superNames = superNames,
          repeats = (if (!meta_repeats) Some(false) else None),
          gid = meta_gid.getOrElse(""),
          superGids = meta_parent_section_gid.toSeq ++ meta_abstract_types_gids,
          redundant = None,
          derived = None,
          otherKeys = otherKeys2
        )
      case dim @ MetaInfoDimension(
        meta_name,
        meta_description,
        meta_deprecated,
        meta_gid,
        meta_parent_section,
        meta_parent_section_gid,
        meta_abstract_types,
        meta_abstract_types_gids,
        otherKeys
        ) =>
        val otherKeys2: List[JField] =
          ("meta_deprecated" -> (if (meta_deprecated) JBool(true) else JNothing)) ::
            otherKeys
        MetaInfoRecord(
          name = meta_name,
          kindStr = kindStr,
          description = meta_description,
          superNames = superNames,
          dtypeStr = dtypeStrMap.get(dim.meta_data_type),
          repeats = None,
          gid = meta_gid.getOrElse(""),
          superGids = meta_parent_section_gid.toSeq ++ meta_abstract_types_gids,
          redundant = None,
          derived = None,
          otherKeys = otherKeys2
        )
      case MetaInfoAbstract(
        meta_name,
        meta_description,
        meta_deprecated,
        meta_gid,
        meta_abstract_types,
        meta_abstract_types_gids,
        otherKeys
        ) =>
        val otherKeys2: List[JField] =
          ("meta_deprecated" -> (if (meta_deprecated) JBool(true) else JNothing)) ::
            otherKeys
        MetaInfoRecord(
          name = meta_name,
          kindStr = kindStr,
          description = meta_description,
          superNames = superNames,
          repeats = None,
          gid = meta_gid.getOrElse(""),
          superGids = meta_abstract_types_gids,
          redundant = None,
          derived = None,
          otherKeys = otherKeys2
        )
    }
  }
}

/**
 * Json serialization to and deserialization support for MetaInfoRecord
 */
class MetaInfoRecordSerializer extends CustomSerializer[MetaInfoRecord](format => (
  {
    case JObject(obj) => {
      implicit val formats = format;
      var name: String = "";
      var gid: String = "";
      var kindStr: String = "type_document_content";
      var description: String = "";
      var superNames: Seq[String] = Seq();
      var superGids: Seq[String] = Seq();
      var units: Option[String] = None;
      var dtypeStr: Option[String] = None;
      var repeats: Option[Boolean] = None;
      var shape: Option[Seq[Either[Long, String]]] = None;
      var redundant: Option[Seq[String]] = None
      var derived: Option[Boolean] = None
      var referencedSections: Option[Seq[String]] = None
      var otherKeys: List[JField] = Nil;
      obj foreach {
        case JField("name", value) =>
          value match {
            case JString(s) => name = s
            case JNothing | JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "name", "NomadMetaInfo", JsonUtils.prettyStr(value), "a string"
            )
          }
        case JField("gid", value) =>
          value match {
            case JString(s) => gid = s
            case JNothing | JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "gid", "NomadMetaInfo", JsonUtils.prettyStr(value), "a string"
            )
          }
        case JField("kindStr", value) =>
          value match {
            case JString(s) => kindStr = s
            case JNothing | JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "kindString", "NomadMetaInfo", JsonUtils.prettyStr(value), MetaInfoRecord.dtypes.foldLeft("one of the following strings:") { _ + " " + _ }
            )
          }
        case JField("description", value) =>
          value match {
            case JString(s) => description = s
            case JArray(arr) =>
              val sb = new StringBuilder()
              arr.foreach {
                case JString(s) => sb ++= (s)
                case JNothing => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "description", "NomadMetaInfo", JsonUtils.prettyStr(value), "either a string or an array of strings"
                )
              }
              description = sb.toString
            case JNothing | JNull => ()
            case _ => throw new JsonUtils.InvalidValueError(
              "description", "NomadMetaInfo", JsonUtils.prettyStr(value), "either a string or an array of strings"
            )
          }
        case JField("superNames", value) =>
          if (!value.toOption.isEmpty)
            superNames = value.extract[Seq[String]]
        case JField("referencedSections", value) =>
          if (!value.toOption.isEmpty)
            referencedSections = value.extract[Option[Seq[String]]]
        case JField("superGids", value) =>
          if (!value.toOption.isEmpty)
            superGids = value.extract[Seq[String]]
        case JField("units", value) =>
          if (!value.toSome.isEmpty)
            units = value.extract[Option[String]]
        case JField("dtypeStr", value) =>
          if (!value.toSome.isEmpty)
            dtypeStr = value.extract[Option[String]]
        case JField("repeats", value) =>
          if (!value.toSome.isEmpty)
            repeats = value.extract[Option[Boolean]]
        case JField("redundant", value) =>
          if (!value.toSome.isEmpty)
            redundant = value.extract[Option[Seq[String]]]
        case JField("derived", value) =>
          if (!value.toSome.isEmpty)
            derived = value.extract[Option[Boolean]]
        case JField("shape", value) =>
          shape = value.extract[Option[Seq[Either[Long, String]]]]
        case JField(key, value) =>
          if (!value.toSome.isEmpty)
            otherKeys = JField(key, value) +: otherKeys
      }
      if (name.isEmpty) throw new JsonUtils.MissingFieldError("name", "NomadMetaInfo")
      if (description.isEmpty) throw new JsonUtils.MissingFieldError("description", "NomadMetaInfo")
      if (!dtypeStr.isEmpty && !(MetaInfoRecord.dtypes contains dtypeStr.get))
        throw new JsonUtils.InvalidValueError("dtypeStr", "NomadMetaInfo", dtypeStr.get, MetaInfoRecord.dtypes.foldLeft("") { _ + " " + _ })
      if (!superGids.isEmpty && superNames.length != superGids.length)
        throw new JsonUtils.InvalidValueError("superGids", "NomadMetaInfo", superGids.mkString("[", ", ", "]"), s"incompatible length with superNames ${superNames.mkString("[", ",", "]")}")
      if (shape.isEmpty && kindStr == "type_document_content")
        shape = Some(Seq())

      new MetaInfoRecord(name, kindStr, description, superNames, units,
        dtypeStr, repeats, shape, gid, superGids, redundant, derived,
        referencedSections, otherKeys)
    }
  },
  {
    case x: MetaInfoRecord => {
      x.toJValue()
    }
  }
))

/*class MetaInfoDbEnv(
  val name: String,
  val dbContext: () => jooq.DSLContext,
  val lazyLoad: Boolean) {
}*/
