/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta;
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import scala.collection.mutable
import scala.collection.breakOut
import org.{ json4s => jn }
import java.nio.file.Path
import java.nio.file.Paths
import com.typesafe.scalalogging.StrictLogging

object KnownMetaInfoEnvs extends MetaInfoCollection with StrictLogging {
  val envs: mutable.Map[String, MetaInfoEnv] = mutable.Map()

  /**
   * loads a meta info env from the resouces
   */
  def loadMetaEnv(metaPath: String, versionName: String = "", description: String = ""): MetaInfoEnv = {
    try {
      val classLoader: ClassLoader = getClass().getClassLoader();
      val filePath = Paths.get(classLoader.getResource(metaPath).getFile())
      //      logger.info("Path is :"+filePath);
      val baseName = filePath.getName(filePath.getNameCount() - 1).toString()
      val vName = if (!versionName.isEmpty)
        versionName
      else if (baseName.endsWith(".nomadmetainfo.json"))
        baseName.dropRight(19)
      else if (baseName.endsWith(".json"))
        baseName.dropRight(5)
      else
        baseName
      val resolver = new ResourceDependencyResolver(classLoader)
      val mainEnv = SimpleMetaInfoEnv.fromInputStream(classLoader.getResourceAsStream(metaPath), metaPath,
        JObject(
          ("path" -> JString(metaPath))
            :: Nil
        ),
        resolver)
      val desc = if (!description.isEmpty)
        description
      else
        mainEnv.description
      val res = new SimpleMetaInfoEnv(
        name = vName,
        description = desc,
        source = jn.JObject(jn.JField("path", jn.JString(filePath.getParent().toString()))),
        nameToGid = Map[String, String](),
        gidToName = Map[String, String](),
        metaInfosMap = Map[String, MetaInfoRecord](),
        dependencies = Seq(mainEnv),
        kind = MetaInfoEnv.Kind.Version
      )
      envs += (vName -> res)
      return res
    } catch {
      case e: Exception =>
        logger.warn(s"Exception $e while loading meta env at ${metaPath}")
        throw new Exception(s"Exception $e while loading meta env at ${metaPath}", e)
    }
  }

  val publicMeta = {
    loadMetaEnv(
      "nomad_meta_info/public.nomadmetainfo.json",
      versionName = "public",
      description = "public metadata of [NOMAD Laboratory](http://nomad-lab.eu/)"
    )
  }
  val common = {
    loadMetaEnv(
      "nomad_meta_info/common.nomadmetainfo.json",
      versionName = "common",
      description = "common (code unspecific) values of the "
    )
  }
  val all = {
    loadMetaEnv(
      "nomad_meta_info/all.nomadmetainfo.json",
      versionName = "all",
      description = "All the metaInfo (including code specific)"
    )
  }
  val last = {
    loadMetaEnv(
      "nomad_meta_info/main.nomadmetainfo.json",
      versionName = "last",
      description = "common values and meta types"
    )
  }
  val fhiAims = {
    loadMetaEnv(
      "nomad_meta_info/fhi_aims.nomadmetainfo.json",
      description = "FHI aims meta info and its dependencies"
    )
  }
  val cp2k = {
    loadMetaEnv(
      "nomad_meta_info/cp2k.nomadmetainfo.json",
      description = "CP2K meta info and its dependencies"
    )
  }
  val cpmd = {
    loadMetaEnv(
      "nomad_meta_info/cpmd.nomadmetainfo.json",
      description = "CPMD meta info and its dependencies"
    )
  }
  val exciting = {
    loadMetaEnv(
      "nomad_meta_info/exciting.nomadmetainfo.json",
      description = "exciting meta info and its dependencies"
    )
  }
  val gaussian = {
    loadMetaEnv(
      "nomad_meta_info/gaussian.nomadmetainfo.json",
      description = "Gaussian meta info and its dependencies"
    )
  }
  val sampleParser = {
    loadMetaEnv(
      "nomad_meta_info/sample_parser.nomadmetainfo.json",
      description = "sample_parser and its dependencies"
    )
  }
  val lammps = {
    loadMetaEnv(
      "nomad_meta_info/lammps.nomadmetainfo.json",
      description = "LAMMPS meta info and its dependencies"
    )
  }
  val amber = {
    loadMetaEnv(
      "nomad_meta_info/amber.nomadmetainfo.json",
      description = "Amber meta info and its dependencies"
    )
  }
  val gromacs = {
    loadMetaEnv(
      "nomad_meta_info/gromacs.nomadmetainfo.json",
      description = "GROMACS meta info and its dependencies"
    )
  }
  val gromos = {
    loadMetaEnv(
      "nomad_meta_info/gromos.nomadmetainfo.json",
      description = "GROMOS meta info and its dependencies"
    )
  }
  val namd = {
    loadMetaEnv(
      "nomad_meta_info/namd.nomadmetainfo.json",
      description = "NAMD meta info and its dependencies"
    )
  }
  val charmm = {
    loadMetaEnv(
      "nomad_meta_info/charmm.nomadmetainfo.json",
      description = "charmm meta info and its dependencies"
    )
  }
  val tinker = {
    loadMetaEnv(
      "nomad_meta_info/tinker.nomadmetainfo.json",
      description = "Tinker meta info and its dependencies"
    )
  }
  val octopus = {
    loadMetaEnv(
      "nomad_meta_info/octopus.nomadmetainfo.json",
      description = "octopus meta info and its dependencies"
    )
  }
  val quantumEspresso = {
    loadMetaEnv(
      "nomad_meta_info/quantum_espresso.nomadmetainfo.json",
      description = "Quantum Espresso meta info and its dependencies"
    )
  }
  val turbomole = {
    loadMetaEnv(
      "nomad_meta_info/turbomole.nomadmetainfo.json",
      description = "TURBOMOLE meta info and its dependencies"
    )
  }
  val gpaw = {
    loadMetaEnv(
      "nomad_meta_info/gpaw.nomadmetainfo.json",
      description = "GPAW meta info and its dependencies"
    )
  }
  val castep = {
    loadMetaEnv(
      "nomad_meta_info/castep.nomadmetainfo.json",
      description = "CASTEP meta info and its dependencies"
    )
  }
  val onetep = {
    loadMetaEnv(
      "nomad_meta_info/onetep.nomadmetainfo.json",
      description = "Onetep meta info and its dependencies"
    )
  }
  val dlPoly = {
    loadMetaEnv(
      "nomad_meta_info/dl_poly.nomadmetainfo.json",
      description = "DL_POLY meta info and its dependencies"
    )
  }
  val libAtoms = {
    loadMetaEnv(
      "nomad_meta_info/lib_atoms.nomadmetainfo.json",
      description = "libAtoms meta info and its dependencies"
    )
  }
  val dmol3 = {
    loadMetaEnv(
      "nomad_meta_info/dmol3.nomadmetainfo.json",
      description = "Dmol^3 meta info and its dependencies"
    )
  }
  val vasp = {
    loadMetaEnv(
      "nomad_meta_info/vasp.nomadmetainfo.json",
      description = "VASP meta info and its dependencies"
    )
  }
  val wien2k = {
    loadMetaEnv(
      "nomad_meta_info/wien2k.nomadmetainfo.json",
      description = "WIEN2k meta info and its dependencies"
    )
  }
  val fleur = {
    loadMetaEnv(
      "nomad_meta_info/fleur.nomadmetainfo.json",
      description = "Fleur meta info and its dependencies"
    )
  }
  val elk = {
    loadMetaEnv(
      "nomad_meta_info/elk.nomadmetainfo.json",
      description = "Elk meta info and its dependencies"
    )
  }
  val nwchem = {
    loadMetaEnv(
      "nomad_meta_info/nwchem.nomadmetainfo.json",
      description = "NWChem meta info and its dependencies"
    )
  }
  val bigdft = {
    loadMetaEnv(
      "nomad_meta_info/big_dft.nomadmetainfo.json",
      description = "BigDFT meta info and its dependencies"
    )
  }
  val dftbPlus = {
    loadMetaEnv(
      "nomad_meta_info/dftb_plus.nomadmetainfo.json",
      description = "DFTB+ meta info and its dependencies"
    )
  }
  val asap = {
    loadMetaEnv(
      "nomad_meta_info/asap.nomadmetainfo.json",
      description = "Asap meta info and its dependencies"
    )
  }
  val atk = {
    loadMetaEnv(
      "nomad_meta_info/atk.nomadmetainfo.json",
      description = "ATK meta info and its dependencies"
    )
  }
  val gamess = {
    loadMetaEnv(
      "nomad_meta_info/gamess.nomadmetainfo.json",
      description = "Gamess meta info and its dependencies"
    )
  }
  val gulp = {
    loadMetaEnv(
      "nomad_meta_info/gulp.nomadmetainfo.json",
      description = "Gulp meta info and its dependencies"
    )
  }
  val orca = {
    loadMetaEnv(
      "nomad_meta_info/orca.nomadmetainfo.json",
      description = "Orca meta info and its dependencies"
    )
  }
  val fplo = {
    loadMetaEnv(
      "nomad_meta_info/fplo.nomadmetainfo.json",
      description = "Fplo meta info and its dependencies"
    )
  }
  val molcas = {
    loadMetaEnv(
      "nomad_meta_info/molcas.nomadmetainfo.json",
      description = "Molcas meta info and its dependencies"
    )
  }
  val phonopy = {
    loadMetaEnv(
      "nomad_meta_info/phonopy.nomadmetainfo.json",
      description = "Phonopy meta info and its dependencies"
    )
  }
  val atomicData = {
    loadMetaEnv(
      "nomad_meta_info/atomic_data.nomadmetainfo.json",
      description = "atomic data and its dependencies"
    )
  }

  val qhp = {
    loadMetaEnv(
      "nomad_meta_info/quasi_harmonic_properties.nomadmetainfo.json",
      description = "Quasi-harmonic meta info and its dependencies"
    )
  }

  val elastic = {
    loadMetaEnv(
      "nomad_meta_info/elastic.nomadmetainfo.json",
      description = "Elastic meta info and its dependencies"
    )
  }

  val stats = {
    loadMetaEnv(
      "nomad_meta_info/stats.nomadmetainfo.json",
      description = "statistics meta info and its dependencies"
    )
  }

  val repo = {
    loadMetaEnv(
      "nomad_meta_info/repository.nomadmetainfo.json",
      description = "meta info for the NOMAD repository and its dependencies"
    )
  }
  val archive = {
    loadMetaEnv(
      "nomad_meta_info/archive.nomadmetainfo.json",
      description = "meta info for the Nomad Archive and its dependencies"
    )
  }

  /**
   * returns all versions defined (might contain duplicates!)
   */
  override def allEnvs: Iterator[MetaInfoEnv] = envs.valuesIterator

  /**
   * returns the versions with the given name
   */
  override def versionsWithName(name: String): Iterator[MetaInfoEnv] = {
    envs.get(name) match {
      case Some(env) => Iterator(env)
      case None => Iterator.empty
    }
  }

  /**
   * returns the versions that contain that gid
   *
   * If recursive is true, inclusion through a dependency is also
   * considered.
   */
  override def versionsForGid(gid: String, recursive: Boolean = false): Iterator[String] = {
    envs.flatMap {
      case (vName, env) =>
        if (env.allGids.contains(gid))
          Some(vName)
        else
          None
    }.toIterator
  }

  /**
   * All gids of the meta infos in this collection
   *
   * might contain duplicates
   */
  override def allGids: Iterator[String] = {
    envs.foldLeft(Iterator.empty: Iterator[String])(_ ++ _._2.allGids)
  }

  /**
   * returns the MetaInfoRecord corresponding to the given gid
   *
   * gids and superGids are added only if requested
   */
  override def metaInfoRecordForGid(gid: String, selfGid: Boolean = false, superGids: Boolean = false): Option[MetaInfoRecord] = {
    for (env <- allEnvs) {
      metaInfoRecordForGid(gid, selfGid, superGids) match {
        case Some(value) =>
          return Some(value)
        case None =>
          ()
      }
    }
    None
  }

}
