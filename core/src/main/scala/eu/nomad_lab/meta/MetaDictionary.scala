/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import scala.collection.breakOut
import scala.collection.mutable
import eu.nomad_lab.JsonUtils
import com.typesafe.scalalogging.StrictLogging
import scala.collection.SortedMap
import eu.nomad_lab.SemVer
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.CompactSha
import java.nio.file.StandardOpenOption
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.charset.StandardCharsets
import java.io.Reader
import java.io.InputStreamReader
import scala.util.control.NonFatal

object MetaDictRequire extends StrictLogging {

  def fromJValue(jValue: JValue)(implicit formats: org.json4s.Formats): MetaDictRequire = {
    jValue match {
      case JObject(o) =>
        var metadict_required_name: Option[String] = None
        var metadict_relative_path: Option[String] = None
        var metadict_expected_version: Option[String] = None
        var metadict_dependency_gid: Option[String] = None
        o.foreach {
          case JField("metadict_required_name", value) =>
            value match {
              case JString(p) => metadict_required_name = Some(p)
              case JNothing => ()
              case JNull => metadict_required_name = None
              case _ => throw new JsonUtils.InvalidValueError(
                "metadict_require.metadict_required_name", "meta_dictionary", JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField("metadict_relative_path", value) =>
            value match {
              case JString(p) => metadict_relative_path = Some(p)
              case JNothing => ()
              case JNull => metadict_relative_path = None
              case _ => throw new JsonUtils.InvalidValueError(
                "metadict_require.metadict_relative_path", "meta_dictionary", JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField("metadict_expected_version", value) =>
            value match {
              case JString(v) =>
                metadict_expected_version = Some(v)
                SemVer.tryParse(v) match {
                  case Some(_) =>
                  case None =>
                    logger.warn(s"version string '$v' not in semantic versioning in metadict_require ${JsonUtils.normalizedStr(jValue)}") // throw instead?
                }
              case JNothing => ()
              case JNull => metadict_expected_version = None
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_require.metadict_expected_version", "meta_dictionary", JsonUtils.prettyStr(value), "A string with the expected version of the dependency"
                )
            }
          case JField("metadict_dependency_gid", value) =>
            value match {
              case JString(p) => metadict_dependency_gid = Some(p)
              case JNothing => ()
              case JNull => metadict_dependency_gid = None
              case _ => throw new JsonUtils.InvalidValueError(
                "metadict_require.metadict_dependency_gid", "meta_dictionary", JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField(k, v) =>
            throw new JsonUtils.InvalidValueError(
              "metadict_require", "meta_dictionary", JsonUtils.prettyStr(jValue), s"a known field (metadict_relative_path, metadict_relative_path) not $k"
            )
        }
        metadict_required_name match {
          case Some(p) =>
            MetaDictRequire(
              metadict_required_name = p,
              metadict_relative_path = metadict_relative_path,
              metadict_expected_version = metadict_expected_version,
              metadict_dependency_gid = metadict_dependency_gid
            )
          case None =>
            throw new JsonUtils.InvalidValueError(
              "metadict_require.metadict_required_name", "meta_dictionary", JsonUtils.prettyStr(jValue), s"metadict_required_name is required, but missing"
            )
        }
      case v =>
        throw new JsonUtils.InvalidValueError(
          "metadict_require", "meta_dictionary", JsonUtils.prettyStr(jValue), "a dictionary (object) with metadict_required_name, metadict_relative_path, metadict_expected_version, metadict_dependency_gid"
        )
    }
  }
}

case class MetaDictRequire(
    metadict_required_name: String,
    metadict_relative_path: Option[String] = None,
    metadict_expected_version: Option[String] = None,
    metadict_dependency_gid: Option[String] = None
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._
    ("metadict_required_name" -> metadict_required_name) ~
      ("metadict_relative_path" -> metadict_relative_path) ~
      ("metadict_expected_version" -> metadict_expected_version) ~
      ("metadict_dependency_gid" -> metadict_dependency_gid)
  }
}

object MetaDictionary extends StrictLogging {

  def fromJValue(jValue: JValue, context: String, extraArgs: Boolean = true)(implicit formats: org.json4s.Formats): MetaDictionary = {
    val ctx = context + " meta_dictionary"
    jValue match {
      case JObject(o) =>
        var metadict_name: Option[String] = None
        var metadict_description: Option[String] = None
        var metadict_version: Option[String] = None
        var metadict_require: List[MetaDictRequire] = Nil
        var meta_info_entry: SortedMap[String, MetaInfoEntry] = SortedMap()
        var metadict_gid: Option[String] = None
        var otherKeys: List[(String, JValue)] = Nil
        o.foreach {
          case JField("type", value) =>
            value match {
              case JString(t) =>
                if (t != "meta_dictionary 2.0.0") {
                  if (t.startsWith("meta_dictionary ")) {
                    SemVer.tryParse(t.drop("meta_dictionary ".length)) match {
                      case Some(v) =>
                        if (v.major != 2) {
                          throw JsonUtils.InvalidValueError(
                            "type", ctx, JsonUtils.prettyStr(value), "the same major version (meta_dictionary 2.0)"
                          )
                        } else if (v.minor > 0) {
                          logger.warn(s"new minor version ${v.minor} of meta_dictionary,  might have issues reading")
                        }
                      case None =>
                        throw JsonUtils.InvalidValueError(
                          "type", ctx, JsonUtils.prettyStr(value), "A semantic Version number after \"meta_dictionary \" (to be more precise 2.0.0)"
                        )
                    }
                  } else {
                    throw JsonUtils.InvalidValueError(
                      "type", ctx, JsonUtils.prettyStr(value), "\"meta_dictionary 2.0.0\""
                    )
                  }
                }
              case _ =>
                throw JsonUtils.InvalidValueError(
                  "type", ctx, JsonUtils.prettyStr(value), "the string \"meta_dictionary 2.0.0\""
                )
            }
          case JField("metadict_description", value) =>
            JsonUtils.jValueToString(value, "metadict_description", ctx) match {
              case Some(s) => metadict_description = Some(s)
              case None => ()
            }
          case JField("metadict_name", value) =>
            JsonUtils.jValueToString(value, "metadict_name", ctx) match {
              case Some(s) => metadict_name = Some(s)
              case None => ()
            }
          case JField("metadict_version", value) =>
            value match {
              case JString(v) =>
                metadict_version = Some(v)
                if (SemVer.tryParse(v).isEmpty) {
                  throw new JsonUtils.InvalidValueError(
                    "metadict_version", ctx, JsonUtils.prettyStr(value), "A string with the version using [sematic versioning](https://semver.org/)"
                  )
                }
              case JNothing => ()
              case JNull => metadict_version = None
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_version", ctx, JsonUtils.prettyStr(value), "A string with the version of the dictionary"
                )
            }
          case JField("metadict_gid", value) =>
            value match {
              case JString(v) =>
                metadict_gid = Some(v)
              case JNothing => ()
              case JNull => metadict_gid = None
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_gid", ctx, JsonUtils.prettyStr(value), "A string with the gid of the dictionary (and all its dependencies)"
                )
            }
          case JField("metadict_require", value) =>
            value match {
              case JArray(a) =>
                metadict_require = a.map { v =>
                  MetaDictRequire.fromJValue(v)(formats)
                }
              case JNothing => ()
              case JNull => ()
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_require", ctx, JsonUtils.prettyStr(value), s"a list of metadict_require objects"
                )
            }
          case JField("meta_info_entry", value) =>
            value match {
              case JArray(a) =>
                val knownNames = mutable.Set[String]()
                meta_info_entry = a.map { v =>
                  val e = MetaInfoEntry.fromJValue(v, ctx, extraArgs = extraArgs)(formats)
                  if (knownNames.contains(e.meta_name))
                    throw new JsonUtils.InvalidValueError(
                      "meta_info_entry", ctx, JsonUtils.prettyStr(value), s"unique meta_name, ${e.meta_name} is duplicated"
                    )
                  knownNames += e.meta_name
                  e.meta_name -> e
                }(breakOut): SortedMap[String, MetaInfoEntry]
              case JNothing => ()
              case JNull => ()
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "meta_info_entry", ctx, JsonUtils.prettyStr(value), s"a list of meta_info_entry objects"
                )
            }
          case JField(k, v) =>
            if (extraArgs)
              otherKeys = (k -> v) :: otherKeys
            else
              throw JsonUtils.UnexpectedFieldError(
                context = ctx,
                field = k,
                value = v,
                info = s"In strict mode (extraArgs = false) only known fields (metadict_description, metadict_version, metadict_require, meta_info_entry, metadict_gid) are accepted, not $k."
              )
        }
        otherKeys = otherKeys.reverse
        val desc = metadict_description match {
          case Some(d) => d
          case None => throw new JsonUtils.InvalidValueError(
            "metadict_description", ctx, JsonUtils.prettyStr(jValue), s"metadict_description is required, but missing"
          )
        }
        val vers = metadict_version match {
          case Some(v) => v
          case None => throw new JsonUtils.InvalidValueError(
            "metadict_version", ctx, JsonUtils.prettyStr(jValue), s"metadict_version is required, but missing"
          )
        }
        val name = metadict_name match {
          case Some(n) => n
          case None => throw new JsonUtils.InvalidValueError(
            "metadict_name", ctx, JsonUtils.prettyStr(jValue), s"a string required, but missing"
          )
        }
        MetaDictionary(
          metadict_name = name,
          metadict_description = desc,
          metadict_version = vers,
          metadict_source = context,
          metadict_require = metadict_require,
          meta_info_entry = meta_info_entry,
          metadict_gid = metadict_gid,
          otherKeys = otherKeys
        )
      case v =>
        throw new JsonUtils.InvalidValueError(
          "-", ctx, JsonUtils.prettyStr(jValue), "a dictionary (object) with metadict_description, metadict_version, metadict_require, meta_info_entry,..."
        )
    }
  }

  /**
   * initializes a MetaDictionary from a path of the filesystem
   */
  def fromFilePath(
    filePath: String, extraArgs: Boolean
  )(implicit formats: org.json4s.Formats): MetaDictionary =
    fromFilePath(Paths.get(filePath), extraArgs)(formats)

  def fromFilePath(
    filePath: String
  )(implicit formats: org.json4s.Formats): MetaDictionary =
    fromFilePath(Paths.get(filePath), true)(formats)

  def fromFilePath(
    filePath: Path, extraArgs: Boolean = true, checkName: Boolean = true
  )(implicit formats: org.json4s.Formats): MetaDictionary = {
    val r = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)
    val d = fromReader(
      reader = r,
      context = s"file://$filePath",
      extraArgs = extraArgs
    )(formats)
    d.nameCheck(filePath).map { s => logger.warn(s) }
    d
  }

  /**
   * initializes a SimpleMetaInfoEnv with an input stream containing UTF-8 encoded json
   */
  def fromReader(
    reader: Reader, context: String, extraArgs: Boolean = true
  )(implicit formats: org.json4s.Formats): MetaDictionary = {
    val metaInfoJson = JsonUtils.parseReader(reader)
    fromJValue(metaInfoJson, context, extraArgs = extraArgs)(formats)
  }

  /**
   * intializes a SimpleMetaInfoEnv with an input stream containing UTF-8 encoded json
   */
  def fromInputStream(stream: java.io.InputStream, context: String, extraArgs: Boolean = true)(implicit formats: org.json4s.Formats): MetaDictionary = {
    val r = new InputStreamReader(stream, StandardCharsets.UTF_8)
    fromReader(r, context, extraArgs = extraArgs)(formats)
  }

  /**
   * Loads a dictionary from an exploded path
   */
  def fromExplodedPath(dirPath: Path)(implicit formats: org.json4s.Formats): MetaDictionary = {
    val fileI = Files.list(dirPath).iterator()
    var meta_dictionary: Option[MetaDictionary] = None
    var meta_info_entry: SortedMap[String, MetaInfoEntry] = SortedMap()
    val metaEntryRe = "^.*\\.meta_info_entry\\.json$".r
    val metaDictionaryRe = "^_\\.meta_dictionary\\.json$".r
    val bkRe = "^.*\\.bk[0-9]*$".r
    while (fileI.hasNext) {
      val pNow = fileI.next
      try {
        pNow.getFileName.toString match {
          case metaEntryRe() =>
            meta_info_entry += {
              val r = Files.newBufferedReader(pNow, StandardCharsets.UTF_8)
              val v = JsonUtils.parseReader(r)
              r.close()
              val e = MetaInfoEntry.fromJValue(v, context = s"file://$dirPath")(formats)
              (e.meta_name -> e)
            }
          case metaDictionaryRe() =>
            meta_dictionary match {
              case Some(d) =>
                throw new Exception(s"Multiple dictionary definitions had ${d.metadict_name}, found another dictionary in ${pNow}")
              case None =>
                meta_dictionary = {
                  val r = Files.newBufferedReader(pNow, StandardCharsets.UTF_8)
                  val v = JsonUtils.parseReader(r)
                  r.close()
                  val d = MetaDictionary.fromJValue(v, context = pNow.toString)(formats)
                  d.nameCheck(pNow).map { s => logger.warn(s) }
                  Some(d)
                }
            }
          case bkRe() =>
          case f =>
            logger.warn(s"Ignoring unexpected file $f")
        }
      } catch {
        case NonFatal(e) =>
          throw new Exception(s"Error handling $pNow", e)
      }
    }
    meta_dictionary match {
      case Some(d) =>
        d.copy(meta_info_entry = meta_info_entry)
      case None =>
        throw new Exception(s"Missing dictionary entry (*.meta_dictionary.json) from exploded dictionary in $dirPath")
    }
  }

  def fromPath(path: Path)(implicit formats: org.json4s.Formats): MetaDictionary = {
    if (Files.isDirectory(path))
      fromExplodedPath(path)(formats)
    else
      fromFilePath(path)(formats)
  }

  /**
   * Generates a backup path of the target path
   *
   * An empty file is created at that path
   */
  def bkPath(targetPath: Path): Path = {
    import java.util.Calendar
    val c = Calendar.getInstance()
    val baseName = targetPath.getFileName.toString + f".${c.get(Calendar.YEAR)}%d-${c.get(Calendar.MONTH)}%02d-${c.get(Calendar.DAY_OF_WEEK)}%02d"
    var path: Path = targetPath.getParent.resolve(baseName + ".bk")
    var iPath: Int = 0
    var created: Boolean = false
    var tries: Int = 0
    val maxTries: Int = 10
    while (!created && tries < maxTries) {
      tries += 1
      while (Files.exists(path)) {
        iPath += 1
        path = targetPath.getParent.resolve(baseName + s"-$iPath.bk")
      }
      try {
        Files.createFile(path, LocalEnv.filePermissionsAttributes)
        created = true
      } catch {
        case NonFatal(e) =>
          if (tries == maxTries)
            logger.warn(s"bkPath could not create file at $path (too many tries? try cleaning up backup files, will try to overwrite)", e) // throw?
      }
    }
    path
  }

  /**
   * Writes to a targetPath, backing up the old file, and doing a change only if the contents are different
   * Returns the paths that were touched (i.e. the path of the target and backup)
   */
  def writeToPath(targetPath: Path)(writeOp: java.io.Writer => Unit): Seq[Path] = {
    val parent = targetPath.getParent
    if (!Files.exists(parent))
      parent.toFile.mkdirs()
    val outPath = Files.createTempFile(targetPath.getParent, targetPath.getFileName.toString.slice(0, 3), ".tmp", LocalEnv.filePermissionsAttributes)
    val targetW = Files.newBufferedWriter(outPath, StandardCharsets.UTF_8, java.nio.file.StandardOpenOption.CREATE, java.nio.file.StandardOpenOption.TRUNCATE_EXISTING)
    val s1 = CompactSha()
    val checksumW = s1.filterWriter(targetW)
    val writeOk = try {
      writeOp(checksumW)
      checksumW.flush()
      checksumW.close()
      true
    } catch {
      case NonFatal(e) =>
        logger.error(s"Error while trying to generate $targetPath, leaving temp file $outPath", e)
        false
    }
    if (writeOk) {
      try {
        Files.move(outPath, targetPath, StandardCopyOption.ATOMIC_MOVE)
        Seq(targetPath)
      } catch {
        case e: java.nio.file.FileAlreadyExistsException =>
          // check diffs
          val buf = Array.ofDim[Byte](32 * 1024)
          val s0 = CompactSha()
          val origF = Files.newInputStream(targetPath)
          s0.readAllFromInputStream(origF)
          origF.close()
          val d0 = s0.digest
          val d1 = s1.digest
          if (d0 == d1) {
            Files.delete(outPath)
            Seq(targetPath)
          } else {
            val bPath = bkPath(targetPath)
            Files.copy(targetPath, bPath, StandardCopyOption.REPLACE_EXISTING)
            Files.move(outPath, targetPath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE)
            Seq(targetPath, bPath)
          }
      }
    } else {
      Files.delete(outPath) // avoid delete?
      Seq()
    }
  }
}

case class MetaDictionary(
    metadict_name: String,
    metadict_description: String,
    metadict_version: String,
    metadict_source: String,
    metadict_require: List[MetaDictRequire] = Nil,
    meta_info_entry: SortedMap[String, MetaInfoEntry] = SortedMap(),
    metadict_deprecated: Boolean = false,
    metadict_gid: Option[String] = None,
    otherKeys: List[(String, JValue)] = Nil
) extends StrictLogging {

  def toJValue(extraArgs: Boolean = true, inlineExtraArgs: Boolean = true, selfGid: Boolean = true, subGids: Boolean = true, metaInfoEntries: Boolean = true): JValue = {
    import org.json4s.JsonDSL._
    ("metadict_deprecated" -> (if (metadict_deprecated) JBool(true) else JNothing)) ~
      ("metadict_name" -> metadict_name) ~
      ("metadict_description" -> JsonUtils.stringToJValue(metadict_description)) ~
      ("metadict_version" -> metadict_version) ~
      ("metadict_require" -> JArray(metadict_require.sortBy(_.metadict_relative_path).map(r => r.toJValue))) ~
      ("meta_info_entry" -> (if (metaInfoEntries)
        JArray((meta_info_entry.map {
        case (k, v) =>
          v.toJValue(
            extraArgs = extraArgs,
            inlineExtraArgs = inlineExtraArgs,
            selfGid = selfGid,
            subGids = subGids
          )
      })(breakOut))
      else
        JNothing)) ~
      ("metadict_gid" -> (if (selfGid) metadict_gid else None))
  }

  def nameCheck(filePath: Path): Option[String] = {
    var res: String = ""
    val vString: String = SemVer.tryParse(metadict_version) match {
      case Some(v) =>
        s"${v.major}(?:\\.${v.minor}(?:\\.${v.patch})?)?(?:-${v.label})?(?:\\+${v.build})?"
      case None =>
        throw new Exception("A valid semantic version is expected in metadict_version of $filePath, not $metadict_version. $e")
        "[0-9]+(?:\\.[0-9]+(?:\\.[0-9]+)?)?(?:-[-a-zA-Z0-9]+(?:\\.[-a-zA-Z0-9]+)*)?(?:\\+[-a-zA-Z0-9]+(?:\\.[-a-zA-Z0-9]+)*)?"
    }
    val fileName = filePath.getFileName.toString
    val fileNameRe = s"^$metadict_name(?:\\s+$vString)?\\.meta_dictionary\\.json$$".r
    val dirNameRe = s"^$metadict_name(?:\\s+$vString)?\\.meta_dictionary$$".r
    fileName match {
      case "_.meta_dictionary.json" =>
        val p = filePath.getParent
        val ps = p.getFileName.toString
        if (ps == "..")
          throw new Exception(s"invalid path, could not check parent of $filePath to check directory name of exploded dictionary $metadict_name from $metadict_source")
        else
          ps match {
            case dirNameRe() =>
            case d =>
              res ++= s"unexpected directory name '$d' for dictionary $metadict_name and version $metadict_version at $filePath, could not match $fileNameRe"
          }
      case fileNameRe() =>
      case f =>
        res ++= s"unexpected filename '$f' for dictionary $metadict_name and version $metadict_version at $filePath, could not match $fileNameRe"
    }
    if (res.isEmpty)
      None
    else
      Some(res)
  }

  def toWriter[W <: java.io.Writer](outF: W): Unit = {
    val w = new JsonUtils.StringArraySplitWriter(outF)
    JsonUtils.prettyWriter(toJValue(), w)
    w.write("\n")
    w.flush()
  }

  def toFilePath(p: String): Unit = {
    toFilePath(Paths.get(p))
  }

  /**
   * Writes the current dictionary to the given path (keeping a backup of the old data)
   *
   * Checks the name after writing (i.e. exceptions can be catched and ignored)
   */
  def toFilePath(p: Path): Unit = {
    MetaDictionary.writeToPath(p) {
      w => toWriter(w)
    }
    nameCheck(p).map { s => logger.warn(s) }
  }

  /**
   * Saves the dictionary "exploding" it in a directory (a file per entry)
   */
  def toExplodedPath(dirPath: Path): Unit = {
    import java.nio.file.{ Files, Paths }
    import scala.collection.JavaConverters._
    val dirContents: Set[String] = Files.list(dirPath).iterator().asScala.map { el => el.getFileName.toString }.toSet
    var generatedContents: Set[String] = Set()
    val dictPath = dirPath.resolve(s"_.meta_dictionary.json")
    generatedContents ++= MetaDictionary.writeToPath(dictPath) { writer =>
      val w = new JsonUtils.StringArraySplitWriter(writer)
      JsonUtils.prettyWriter(toJValue(metaInfoEntries = false, selfGid = false, subGids = false), w)
      w.write("\n")
    }.map {
      el => el.getFileName.toString
    }
    for ((meta_name, entry) <- meta_info_entry) {
      val ePath = dirPath.resolve(s"$meta_name.meta_info_entry.json")
      generatedContents ++= MetaDictionary.writeToPath(ePath) { writer =>
        val w = new JsonUtils.StringArraySplitWriter(writer)
        JsonUtils.prettyWriter(entry.toJValue(selfGid = false, subGids = false), w)
        w.write("\n")
      }.map {
        el => el.getFileName.toString
      }
    }
    val oldVal = "^(.*\\.meta_(?:dictionary|info_entry).json)$".r
    val bkRe = "^.*\\.bk[0-9]*$".r
    val tmpRe = "^.*\\.tmp$".r
    var unexpectedDir: Set[String] = Set()
    var unexpectedFile: Set[String] = Set()
    dirContents.filter { el => !generatedContents(el) }.foreach {
      case oldVal(v) =>
        val oldPath = dirPath.resolve(v)
        val bPath = MetaDictionary.bkPath(oldPath)
        Files.move(oldPath, bPath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE)
      case bkRe() =>
      case tmpRe() =>
      case f =>
        val p = dirPath.resolve(f)
        if (Files.isDirectory(p))
          unexpectedDir += f
        else
          unexpectedFile += f
    }
    if (!unexpectedDir.isEmpty)
      logger.warn(s"Unexpected directories in target of meta dictionary $metadict_name explode ($dirPath): $unexpectedDir")
    if (!unexpectedFile.isEmpty)
      logger.warn(s"Unexpected directories in target of meta dictionary $metadict_name explode ($dirPath): $unexpectedFile")
  }

}
