/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import scala.collection.mutable
import scala.collection.breakOut
import eu.nomad_lab.JsonUtils
import scala.util.control.NonFatal
import com.typesafe.scalalogging.StrictLogging

/**
 * Interface to a set of MetaInfoRecords in which MetaInfoRecord.name is
 * unique
 *
 * An environment in which names have a unique meaning is required to
 * calculate gids if the dependencies are expressed only through superNames.
 * This can also be seen as a "Version" of the meta infos.
 */
trait MetaInfoEnv extends MetaInfoCollection with StrictLogging {
  /**
   * Name of the version
   */
  def name: String;

  /**
   * A description of this environment
   */
  def description: String;

  /**
   * Info describing from where this version was created
   *
   * example: path of the file read
   */
  def source: JObject;

  def kind: MetaInfoEnv.Kind.Kind;

  /**
   * The names of the meta info contained directly in this environment,
   * no dependencies
   */
  def names: Iterator[String]

  /**
   * The meta infos contained in this environment
   */
  def metaInfos(selfGid: Boolean, superGids: Boolean): Iterator[MetaInfoRecord]

  /**
   * gids of the meta infos contained in this environment (no dependencies)
   */
  def gids: Seq[String]

  /**
   * All gids of the meta infos in this collection
   *
   * might contain duplicates
   */
  override def allGids: Iterator[String] = {
    allUniqueEnvs(_ => true).foldLeft(gids.toIterator)(_ ++ _.gids.toIterator)
  }

  /**
   * returns the versions with the given name
   */
  override def versionsWithName(name: String): Iterator[MetaInfoEnv] = {
    val subVers = allUniqueEnvs { env: MetaInfoEnv => env.kind == MetaInfoEnv.Kind.Version && env.name == name }
    if (kind == MetaInfoEnv.Kind.Version && this.name == name)
      Iterator(this) ++ subVers
    else
      subVers
  }

  /**
   * All names of the meta infos in this environment (including dependencies)
   *
   * Might contain duplicates
   */
  def allNames: Iterator[String] = {
    allUniqueEnvs(_ => true).foldLeft(names.toIterator)(_ ++ _.names.toIterator)
  }

  /**
   * Maps names to gids
   *
   * If recursive is false only direct descendents are mapped.
   */
  def gidForName(name: String, recursive: Boolean = true): Option[String]

  /**
   * Maps gids to names
   *
   * If recursive is false only direct descendents are mapped.
   */
  def nameForGid(gid: String, recursive: Boolean = true): Option[String]

  /**
   * returns the MetaInfoRecord corresponding to the given name
   *
   * gids and superGids are added only if requested
   */
  def metaInfoRecordForName(name: String, selfGid: Boolean = false, superGids: Boolean = false): Option[MetaInfoRecord];
  def metaInfoRecordForName(name: String): Option[MetaInfoRecord];

  /**
   * Converts the environment to json
   *
   * metaInfoWriter should write out the MetaInfo it receives (if necessary), and return a
   * JObject that can be written out as dependency to load that MetaInfoEnv.
   */
  def toJValue(metaInfoWriter: MetaInfoEnv => JValue, selfGid: Boolean = false,
    superGids: Boolean = false, flat: Boolean = true): JObject = {
    val deps = JArray(if (!flat) {
      allUniqueEnvs().map(metaInfoWriter).toList
    } else {
      Nil
    })

    val mInfos = JArray(if (!flat) {
      names.toSeq.sorted.flatMap { name: String =>
        metaInfoRecordForName(name, selfGid = selfGid, superGids = superGids) match {
          case Some(r) => Some(r.toJValue())
          case None => None
        }
      }(breakOut): List[JValue]
    } else {
      allNames.toSet.toSeq.sorted.flatMap { name: String =>
        metaInfoRecordForName(name, selfGid = selfGid, superGids = superGids) match {
          case Some(r) => Some(r.toJValue())
          case None => None
        }
      }(breakOut): List[JValue]
    })

    JObject(JField("type", JString("nomad_meta_info_1_0")) ::
      JField("name", JString(name)) ::
      JField("description", JString(description)) ::
      JField("dependencies", deps) ::
      JField("metaInfos", mInfos) :: Nil)

  }
  /**
   * Iterator that starting with base iterates on all its ancestors
   *
   * call next once if you want to skip base itself.
   *
   * nest in method and use context implicitly?
   */
  class SubIter(context: MetaInfoEnv, base: MetaInfoRecord, selfGid: Boolean, superGids: Boolean) extends Iterator[MetaInfoRecord] {

    val known = mutable.Set[String](base.name)
    val toDo = mutable.ListBuffer(base.name)

    override def hasNext: Boolean = !toDo.isEmpty

    override def next(): MetaInfoRecord = {
      val now = toDo.head
      toDo.trimStart(1)
      val nowR = context.metaInfoRecordForName(now, selfGid = selfGid, superGids = superGids)
      if (nowR.isEmpty)
        throw new MetaInfoEnv.DependsOnUnknownNameException(context.name, known.toString, now)
      nowR.get.superNames.foreach { superName: String =>
        if (!known(superName)) {
          toDo.append(superName)
          known += superName
        }
      }
      nowR.get
    }
  }

  /**
   * Iterates on the given name and all its ancestors
   */
  def metaInfoRecordForNameWithAllSuper(name: String, selfGid: Boolean = false, superGids: Boolean = false): Iterator[MetaInfoRecord] = {
    this.metaInfoRecordForName(name, selfGid = selfGid, superGids = superGids) match {
      case Some(r) => new SubIter(this, r, selfGid, superGids)
      case None => Iterator()
    }
  }

  /**
   * Iterates on the given name and all its ancestors
   */
  def metaInfoRecordForNameWithAllSuperNameList(name: String, selfGid: Boolean = false, superGids: Boolean = false): List[String] = {
    var allParents: List[String] = Nil
    this.metaInfoRecordForNameWithAllSuper(name, selfGid, superGids).foreach {
      (metaInfo: MetaInfoRecord) => allParents = (metaInfo.name) :: allParents
    }
    allParents
  }

  /**
   * root super section of the meta info with the given metaName
   *
   * These are the first ancestors of type type_section in the inheritance DAG
   * of metaName
   */
  def parentSectionName(metaName: String): Option[String] = {
    rootAnchestorsOfType("type_section", metaName).headOption
  }

  /**
   * Returns the path from root("/") to the meta info following type_sections
   *
   */
  def pathViaSections(name: String): String = {
    var path = java.nio.file.Paths.get("")
    //check if name exists in the all names list
    if (allNames.contains(name)) {
      path = java.nio.file.Paths.get(name)
      var parentSection = parentSectionName(name)
      var flag = true
      while (flag) {
        parentSection match {
          case Some(pSection) =>
            path = java.nio.file.Paths.get(pSection).resolve(path)
            parentSection = parentSectionName(pSection)
          case None =>
            path = java.nio.file.Paths.get("/").resolve(path)
            flag = false
        }
      }
    }
    path.toString
  }
  /**
   * ancestors by type, subdivided in roots, and children
   *
   * The roots of type X are the ancestors of type X that cannot be reached starting
   * from others ancestors of type X going though ancestors of all types.
   * children are those than can be reached
   */
  def firstAncestorsByType(name: String): Map[String, Tuple2[Set[String], Set[String]]] = {
    val mInfo = metaInfoRecordForNameWithAllSuper(name, true, true).toArray
    val nameMap: Map[String, Int] = mInfo.zipWithIndex.map {
      case (mInfo, i) =>
        mInfo.name -> i
    }(breakOut)
    val edges: Map[Int, Seq[Int]] = mInfo.zipWithIndex.map {
      case (mInfo, i) =>
        i -> mInfo.superNames.map(nameMap(_)).toSeq
    }(breakOut)
    val typeGroups = (1.until(mInfo.length)).groupBy(i => mInfo(i).kindStr)
    val childsByType = mutable.Map[String, mutable.Set[Int]]()
    val toDo = mutable.Set[Int](1.until(mInfo.length): _*)

    while (!toDo.isEmpty) {
      val now = toDo.head
      val kindNow = mInfo(now).kindStr
      toDo -= now
      val toDo2 = mutable.Set(edges(now): _*)
      val known2 = mutable.Set(edges(now): _*)
      while (!toDo2.isEmpty) {
        val now2 = toDo2.head
        toDo2 -= now2
        if (mInfo(now2).kindStr == kindNow) {
          if (childsByType.contains(kindNow))
            childsByType(kindNow) += now2
          else
            childsByType += (kindNow -> mutable.Set(now2))
          toDo -= now2
        }
        for (el <- edges(now2)) {
          if (!known2(el)) {
            toDo2 += el
            known2 += el
          }
        }
      }
    }

    typeGroups.map {
      case (kindStr, allChilds) =>
        val childs = childsByType.getOrElse(kindStr, mutable.Set())
        val allForKind = allChilds.toSet
        val rootNames: Set[String] = (allForKind -- childs).map(mInfo(_).name)(breakOut)
        val childNames: Set[String] = childs.map(mInfo(_).name)(breakOut)
        kindStr -> (rootNames -> childNames)
    }(breakOut): Map[String, Tuple2[Set[String], Set[String]]]
  }

  /**
   * returns the rootAnchestors of the given type
   */
  def rootAnchestorsOfType(kindStr: String, metaName: String): Set[String] = {
    firstAncestorsByType(metaName).get(kindStr) match {
      case Some((roots, _)) =>
        roots
      case None =>
        Set()
    }
  }

  def toMetaInfoEntry(metaName: String): MetaInfoEntry = {
    val ctx = s"MetaInfo($metaName)"
    try {
      import MetaDataType._
      import eu.nomad_lab.JsonSupport.formats
      val ancestors = firstAncestorsByType(metaName)
      val parentSection: Option[String] = ancestors.get("type_section") match {
        case None => None
        case Some((r, _)) =>
          if (r.size != 1)
            throw new Exception(s"expected only one parent section, not $r")
          Some(r.head)
      }
      var abstractParents: Seq[String] = ancestors.get("type_abstract_document_content") match {
        case None => Nil
        case Some((r, _)) => r.toSeq.sorted
      }
      val r = metaInfoRecordForName(metaName) match {
        case Some(rr) => rr
        case None => throw new Exception(s"could not get meta info record for $metaName")
      }
      if (r.derived.getOrElse(false))
        abstractParents = abstractParents :+ "derived_quantity"
      // dropping (as never used) redundant: Option[Seq[String]] = None, and derived: Option[Boolean] = None
      var meta_deprecated: Boolean = false
      var meta_required: Boolean = false
      var otherKeys: List[(String, JValue)] = r.otherKeys.filter {
        case JField(k, value) =>
          k match {
            case "meta_deprecated" =>
              value match {
                case JBool(v) => meta_deprecated = v
                case JNothing =>
                case JNull =>
                case _ =>
                  throw new Exception(s"expected a boolean for meta_deprecated in $r")
              }
              false
            case "meta_required" =>
              value match {
                case JBool(v) => meta_required = v
                case JNothing =>
                case JNull =>
                case v => throw new Exception(s"Unexpected value ${JsonUtils.normalizedStr(v)}")
              }
              false
            case _ =>
              true
          }
      }
      val meta_name = {
        val invalidCharsRe = "[^a-zA-Z0-9_]+".r
        var nAtt: String = metaName.toLowerCase
        nAtt = invalidCharsRe.replaceAllIn(nAtt, "_")
        var done: Boolean = false
        while (!done) {
          val nNew = nAtt.replaceAll("__", "_")
          if (nNew == nAtt)
            done = true
          else
            nAtt = nNew
        }
        nAtt
      }

      if (metaName != meta_name)
        logger.warn(s"meta_name change: $metaName -> $meta_name")
      r.kindStr match {
        case "type_document_content" =>
          var meta_dimension: List[MetaDimension] = Nil
          var meta_enum: List[MetaEnum] = Nil
          var meta_enum_from: Option[String] = None
          var meta_query_enum: List[MetaQueryEnum] = Nil
          var meta_range_expected: List[MetaRangeExpected] = Nil
          var meta_value_validate: Option[String] = None
          r.shape match {
            case Some(shape) =>
              meta_dimension = (for (d <- shape) yield {
                d match {
                  case Left(i) => MetaDimension(meta_dimension_fixed = Some(i))
                  case Right(dName) => MetaDimension(meta_dimension_symbolic = Some(dName))
                }
              })(breakOut)
            case None => ()
          }
          otherKeys = otherKeys.filter {
            case (k, v) =>
              k match {
                case "values" =>
                  val vals = v.extract[Map[String, String]]
                  meta_enum = (for ((k, v) <- vals) yield MetaEnum(meta_enum_value = k, meta_enum_description = v))(breakOut)
                  false
                case "meta_enum_from" =>
                  v match {
                    case JString(s) => meta_enum_from = Some(s)
                    case JNull => meta_enum_from = None
                    case JNothing => ()
                    case _ => throw new Exception(s"meta_enum_from is supposed to be a string")
                  }
                  false
                case "meta_query_enum" =>
                  v match {
                    case JArray(a) =>
                      meta_query_enum = a.map { el => MetaQueryEnum.fromJValue(el, s"NomadMeta(meta_name:${r.name})") }
                    case JNull | JNothing => ()
                    case _ => throw new Exception(s"meta_query_enum expected an array of meta_query_enum")
                  }
                  false
                case "meta_range_expected" =>
                  v match {
                    case JArray(a) =>
                      meta_range_expected = a.map { el => MetaRangeExpected.fromJValue(el, s"NomadMeta(meta_name:${r.name})") }
                    case JNull | JNothing => ()
                    case _ => throw new Exception(s"meta_range_expected expected an array of meta_range_expected")
                  }
                  false
                case "meta_value_validate" =>
                  meta_value_validate = JsonUtils.jValueToString(v, ctx, "meta_value_validate")
                  false
                case _ => true
              }
          }
          val dtypeStrMap = Map(
            "f" -> float, "i" -> int,
            "f32" -> float32, "i32" -> int32,
            "f64" -> float64, "i64" -> int64,
            "b" -> boolean, "C" -> string,
            "r" -> reference, "D" -> json
          )
          val referencedSection: Option[String] = r.referencedSections match {
            case Some(ss) =>
              ss.length match {
                case 0 => None
                case 1 => Some(ss(0))
                case l => throw new Exception("Multiple referenced Sections not supported anymore ($ss in $metaName)")
              }
            case None => None
          }

          MetaInfoValue(
            meta_name = meta_name,
            meta_description = r.description,
            meta_data_type = dtypeStrMap(r.dtypeStr.get),
            meta_deprecated = meta_deprecated,
            meta_gid = None,
            meta_parent_sectionStr = parentSection.get,
            meta_parent_section_gid = None,
            meta_abstract_types = abstractParents,
            meta_abstract_types_gids = Seq(),
            meta_dimension = meta_dimension,
            meta_enum = meta_enum,
            meta_enum_from = meta_enum_from,
            meta_query_enum = meta_query_enum,
            meta_range_expected = meta_range_expected,
            meta_repeats = r.repeats.getOrElse(false),
            meta_required = meta_required,
            meta_referenced_section = referencedSection,
            meta_value_validate = meta_value_validate,
            meta_units = r.units,
            otherKeys = otherKeys
          )
        case "type_section" =>
          var meta_chosen_key: Option[String] = None
          var meta_context_identifier: List[String] = Nil
          var meta_inject_if_section_regexp: List[String] = Nil
          var meta_inject_if_abstract_type: List[String] = Nil
          otherKeys = otherKeys.filter {
            case JField(k, value) =>
              k match {
                case "meta_chosen_key" =>
                  meta_chosen_key = value match {
                    case JString(v) => Some(v)
                    case JNothing => None
                    case JNull => None
                    case v => throw new Exception(s"Unexpected value ${JsonUtils.normalizedStr(v)}")
                  }
                  false
                case "meta_context_identifier" =>
                  meta_context_identifier = value match {
                    case JString(v) => v :: Nil
                    case JArray(a) => JArray(a).extract[List[String]]
                    case JNothing => Nil
                    case JNull => Nil
                    case v => throw new Exception(s"Unexpected value ${JsonUtils.normalizedStr(v)}")
                  }
                  false
                case "meta_inject_if_section_regexp" =>
                  meta_inject_if_section_regexp = value match {
                    case JString(v) => v :: Nil
                    case JArray(a) => JArray(a).extract[List[String]]
                    case JNothing => Nil
                    case JNull => Nil
                    case v => throw new Exception(s"Unexpected value ${JsonUtils.normalizedStr(v)}")
                  }
                  false
                case "meta_inject_if_abstract_type" =>
                  meta_inject_if_abstract_type = value match {
                    case JString(v) => v :: Nil
                    case JArray(a) => JArray(a).extract[List[String]]
                    case JNothing => Nil
                    case JNull => Nil
                    case v => throw new Exception(s"Unexpected value ${JsonUtils.normalizedStr(v)}")
                  }
                  false
                case _ => true
              }
          }
          MetaInfoSection(
            meta_name = meta_name,
            meta_description = r.description,
            meta_deprecated = meta_deprecated,
            meta_gid = None,
            meta_parent_section = parentSection,
            meta_parent_section_gid = None,
            meta_abstract_types = abstractParents,
            meta_abstract_types_gids = Seq(),
            otherKeys = otherKeys,
            meta_repeats = r.repeats.getOrElse(true),
            meta_required = meta_required,
            meta_chosen_key = meta_chosen_key,
            meta_context_identifier = meta_context_identifier,
            meta_inject_if_section_regexp = meta_inject_if_section_regexp,
            meta_inject_if_abstract_type = meta_inject_if_abstract_type
          )
        case "type_abstract_document_content" =>
          MetaInfoAbstract(
            meta_name = meta_name,
            meta_description = r.description,
            meta_deprecated = meta_deprecated,
            meta_gid = None,
            meta_abstract_types = abstractParents,
            meta_abstract_types_gids = Seq(),
            otherKeys = otherKeys
          )
        case "type_dimension" =>
          MetaInfoDimension(
            meta_name = meta_name,
            meta_description = r.description,
            meta_deprecated = meta_deprecated,
            meta_gid = None,
            meta_parent_section = parentSection,
            meta_parent_section_gid = None,
            meta_abstract_types = abstractParents,
            meta_abstract_types_gids = Seq(),
            otherKeys = otherKeys
          )
        case _ =>
          throw new Exception(s"could not convert unknown type of MetaInfoRecord(kindStr:${r.kindStr}) ${r}")
      }
    } catch {
      case NonFatal(e) =>
        throw new Exception(s"Error trying to convert $metaName", e)
    }
  }

  /**
   * Returns the names of the direct children of the metaInfo with the given name
   *
   * Direct children are those that have name in superNames.
   * Only meta info in the environment are returned (no dependencies)
   */
  def directChildrenOf(name: String): Iterator[String] = {
    metaInfos(false, false).flatMap { metaInfo: MetaInfoRecord =>
      if (metaInfo.superNames.contains(name))
        Some(metaInfo.name)
      else
        None
    }.toIterator
  }

  /**
   * Returns all the names of the direct childrens of the metaInfo with the given name
   * (with dependencies)
   *
   * Direct children are those that have name in superNames.
   */
  def allDirectChildrenOf(name: String): Iterator[String] = {
    allUniqueEnvs(_ => true).foldLeft(directChildrenOf(name))(_ ++ _.directChildrenOf(name))
  }

  /**
   * writes a dot format description of the graph
   */
  def writeDot(s: java.io.Writer, params: MetaInfoEnv.DotParams = MetaInfoEnv.DotParams()): Unit = {
    s.write("""strict digraph meta_info_env {
  ranksep=1;
  ratio=1.41;
  rankdir=RL;
""")
    allNames.foreach { name: String =>
      metaInfoRecordForName(name, false, false) match {
        case Some(m) =>
          var toRemove: Boolean = (
            params.removeUnintresting && (metaInfoRecordForNameWithAllSuperNameList(name).find {
              (n: String) => n == "time_info" || n == "parsing_message_debug" || n == "message_debug"
            } match {
              case Some(_) => true
              case None => false
            })
            || !(name.contains("energy") || name.contains("settings"))
            || params.removeMeta && m.kindStr == "type_meta"
            || !params.abstractParents && m.kindStr == "type_abstract_document_content"
            || params.abstractOnly && m.kindStr != "type_abstract_document_content"
            || params.removeDimensions && m.kindStr == "type_dimension"
            || !params.sectionParents && m.kindStr == "type_section"
          )
          if (!toRemove) {
            val attributes = m.kindStr match {
              case "type_document_content" =>
                if (m.dtypeStr == Some("r"))
                  "shape=box; color=red"
                else
                  "shape=box"
              case "type_unknown" => "color=green"
              case "type_unknown_meta" => "color=green"
              case "type_dimension" => "color=green"
              case "type_document" => "color=grey"
              case "type_meta" => "color=blue"
              case "type_abstract_document_content" => "color=black"
              case "type_section" => "color=red"
              case "type_connection" => "color=orange"
              case _ => "color=pink"
            }
            s.write(s"""  ${m.name} [$attributes; URL="${params.urlBase}ui/index.html#/last/${m.name}"];\n""")
            val parents: Seq[String] = if (params.abstractParents) {
              if (params.sectionParents)
                m.superNames
              else
                firstAncestorsByType(name).getOrElse("type_abstract_document_content", (Set[String]() -> Set()))._1.toSeq
            } else {
              if (params.sectionParents)
                firstAncestorsByType(name).getOrElse("type_section", (Set() -> Set()))._1.toSeq
              else
                Seq()
            }
            for (superN <- parents)
              s.write(s"  ${m.name} -> $superN;\n")
            if (m.dtypeStr == Some("r") && params.sectionParents) {
              m.referencedSections match {
                case Some(sects) =>
                  for (refSect <- sects)
                    s.write(s"  ${m.name} -> $refSect [color=red];\n")
                case None => ()
              }
            }
          }
        case None =>
          throw new Exception(s"Could not ger meta info with name $name")
      }
    }
    s.write("}\n")
  }
}

object MetaInfoEnv {
  /**
   * parameters for dot plot
   */
  case class DotParams(
    removeUnintresting: Boolean = false,
    removeMeta: Boolean = true,
    removeDimensions: Boolean = false,
    sectionParents: Boolean = true,
    abstractParents: Boolean = true,
    abstractOnly: Boolean = false,
    urlBase: String = "http://localhost:8081/"
  )

  /**
   * Enum for various kinds of environment
   */
  object Kind extends Enumeration {
    type Kind = Value
    /**
     * The environment represents a file
     */
    val File = Value

    /**
     * The environment represents a version (spanning possibly multiple files)
     */
    val Version = Value

    /**
     * A pseudo environment (for example one corresponding to a gid)
     */
    val Pseudo = Value
  }

  /**
   * two meta infos with the same name detected
   */
  class DuplicateNameException(
    name: String,
    metaInfo1: MetaInfoRecord,
    metaInfo2: MetaInfoRecord
  ) extends Exception(s"DuplicateNameException, found two meta infos with the same name ($name): $metaInfo1 vs $metaInfo2") {}

  /**
   * parsing error interpreting metainfo json
   */
  class ParseException(
    msg: String, what: Throwable = null
  ) extends Exception(msg, what) {}

  /**
   * a meta info depends on an unknown name
   */
  class DependsOnUnknownNameException(
    envName: String,
    name: String,
    unknownName: String
  ) extends Exception(
    s"Meta info '$name' depends on unknown meta info '$unknownName' in '$envName'"
  ) {}

  /**
   * a meta info has a circular dependency
   */
  class MetaInfoCircularDepException(
    envName: String,
    name: String,
    nameInCicle: String,
    inProgress: Iterable[String]
  ) extends Exception(
    s"found loop to '$nameInCicle' evaluating '$name' in '$envName', currently in progress: ${inProgress.mkString("[", ", ", "]")}"
  ) {}

}
