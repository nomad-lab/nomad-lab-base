/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta;
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import scala.util.control.NonFatal
import eu.nomad_lab.SemVer
import com.typesafe.scalalogging.StrictLogging

object MetaInfo extends StrictLogging {
  private var _knownDictionaries: Map[String, MetaDictionary] = Map()

  def loadDictFromFilePath(filePath: Path): Option[MetaDictionary] = {
    import eu.nomad_lab.JsonSupport.formats
    val absPath = filePath.toAbsolutePath
    val rPath = filePath.toRealPath()
    synchronized {
      _knownDictionaries.get(absPath.toString) match {
        case Some(d) => return Some(d)
        case None =>
          _knownDictionaries.get(rPath.toString) match {
            case Some(d) => return Some(d)
            case None =>
          }
      }
    }
    if (!Files.exists(absPath))
      return None
    val dNew = MetaDictionary.fromFilePath(absPath)
    synchronized {
      _knownDictionaries.get(absPath.toString) match {
        case Some(d) => return Some(d)
        case None =>
          _knownDictionaries.get(rPath.toString) match {
            case Some(d) => return Some(d)
            case None =>
          }
      }
      _knownDictionaries += (absPath.toString -> dNew)
      _knownDictionaries += (rPath.toString -> dNew)
    }
    Some(dNew)
  }

  def loadDictFromResourcePath(resourcePath: String): Option[MetaDictionary] = {
    import eu.nomad_lab.JsonSupport.formats
    val classLoader: ClassLoader = getClass().getClassLoader();
    //val filePath = Paths.get(classLoader.getResource(resourcePath).getFile())
    val rString = s"resources://$resourcePath"
    synchronized {
      _knownDictionaries.get(rString) match {
        case Some(d) => return Some(d)
        case None =>
      }
    }
    val dNew = try {
      MetaDictionary.fromInputStream(classLoader.getResourceAsStream(resourcePath), rString)
    } catch {
      case NonFatal(e) =>
        return None
    }
    synchronized {
      _knownDictionaries.get(rString) match {
        case Some(d) => return Some(d)
        case None =>
      }
      _knownDictionaries += (rString -> dNew)
    }
    Some(dNew)
  }

  def loadDictFromUri(uri: Uri): Option[MetaDictionary] = {
    uri.protocol match {
      case UriProtocol.Resources =>
        loadDictFromResourcePath(uri.path.toString)
      case UriProtocol.File =>
        loadDictFromFilePath(uri.path)
    }
  }

  object UriProtocol extends Enumeration {
    type UriProtocol = Value
    val Resources, File = Value
  }

  case class Uri(
      protocol: UriProtocol.Value,
      path: Path
  ) {
    def resolve(rPath: String): Uri = {
      Uri(protocol, path.resolve(rPath))
    }

    def parent: Uri = {
      Uri(protocol, path.getParent)
    }

    override def toString: String = {
      protocol match {
        case UriProtocol.Resources =>
          "resources://" + path.toString
        case UriProtocol.File =>
          "file://" + path.toString
      }
    }
  }

  object Uri {
    def fromString(uri: String): Uri = {
      val resourceRe = "^resources://(.*)$".r
      val fileRe = "^file://(.*)$".r
      uri match {
        case resourceRe(rPath) =>
          Uri(UriProtocol.Resources, Paths.get(rPath))
        case fileRe(fPath) =>
          Uri(UriProtocol.File, Paths.get(fPath))
        case p =>
          Uri(UriProtocol.File, Paths.get(p))
      }
    }
  }

  def resolve(baseUri: Uri, name: String, version: Option[SemVer] = None): MetaDictionary = {
    val possibleUris = version match {
      case Some(v) =>
        Seq(
          baseUri.resolve(s"$name-$v.meta_dictionary.json"),
          baseUri.resolve(s"$name-${v.major}.${v.minor}.meta_dictionary.json"),
          baseUri.resolve(s"$name-${v.major}.meta_dictionary.json"),
          baseUri.resolve(s"$name.meta_dictionary.json")
        )
      case None =>
        Seq(baseUri.resolve(s"$name.meta_dictionary.json"))
    }
    var toCheck = possibleUris
    while (!toCheck.isEmpty) {
      val uri = toCheck.head
      toCheck = toCheck.tail
      loadDictFromUri(uri) match {
        case Some(d) => return d
        case None =>
      }
    }
    throw new Exception("Could not resolve $name $version at $baseUri, did try $possibleUris")
  }

  def fromUriString(uriString: String): MetaInfo = {
    val uri = Uri.fromString(uriString)
    fromUri(uri)
  }

  def fromUri(uri: Uri): MetaInfo = {
    val mainDict = loadDictFromUri(uri) match {
      case Some(d) => d
      case None => throw new Exception(s"Could not load $uri")
    }
    fromMetaDictionary(mainDict)
  }

  def fromMetaDictionary(mainDict: MetaDictionary): MetaInfo = {
    try {
      val uri = Uri.fromString(mainDict.metadict_source)
      val baseUri = uri.parent
      var dicts: Map[String, MetaDictionary] = Map(mainDict.metadict_name -> mainDict)
      var toDo: List[String] = mainDict.metadict_name :: Nil
      // ensure that the dependencies of all dependencies are loaded and of the correct version
      while (!toDo.isEmpty) {
        val dName = toDo.head
        toDo = toDo.tail
        val dNow = dicts(dName)
        for (req <- dNow.metadict_require) {
          // ensure loading
          var existed: Boolean = true
          val dictNow = dicts.get(req.metadict_required_name) match {
            case None =>
              existed = false
              val ddict = resolve(
                baseUri = req.metadict_relative_path match {
                  case Some(p) => baseUri.resolve(p)
                  case None => baseUri
                },
                name = req.metadict_required_name,
                version = req.metadict_expected_version.flatMap { v =>
                  SemVer.tryParse(v)
                }
              )
              if (ddict.metadict_name != req.metadict_required_name)
                throw new Exception(s"Inconsistent name in dictionary for $req: ${ddict.metadict_name}")
              dicts += (ddict.metadict_name -> ddict)
              toDo = ddict.metadict_name :: toDo
              ddict
            case Some(d) => d
          }
          // check if version is consistent with requirement
          val dictDesc = (if (existed)
            s"already loaded dictionary ${dictNow.metadict_name} version ${dictNow.metadict_version} from ${dictNow.metadict_source}"
          else
            s"newly loaded dictionary ${dictNow.metadict_name} version ${dictNow.metadict_version} from ${dictNow.metadict_source}")
          SemVer.tryParse(dictNow.metadict_version) match {
            case None =>
              logger.warn(s"Invalid version in $dictDesc")
            case Some(dVers) =>
              req.metadict_expected_version match {
                case None => ()
                case Some(rv) =>
                  SemVer.tryParse(rv) match {
                    case None =>
                      logger.warn(s"Invalid required version $rv in $dictDesc")
                    case Some(rVers) =>
                      if (rVers.major != dVers.major || rVers.basic < dVers.basic)
                        throw new Exception(s"The $dictDesc contains incompatible version with requirement $req")
                  }
              }
          }
        }
      }
      MetaInfo(mainDict.metadict_name, dicts)
    } catch {
      case NonFatal(e) =>
        throw new Exception("Failure loading dependencies of ${mainDict.metadict_name} version ${mainDict.metadict_version} from ${mainDict.metadict_source}", e)
    }
  }

}

case class MetaInfo(
    main_dict_name: String,
    dictionaries: Map[String, MetaDictionary]
) {
  /*
  /**
   * Calculates the Gid of name, resolving all dependencies and calculating their gid
   * if required.
   *
   * nameToGidsCache will be updated with all the gids calculated.
   */
  def calculateGid(
    name: String,
    nameToGidCache: mutable.Map[String, String],
    metaInfos: Map[String, MetaInfoEntry],
    dependencies: Seq[MetaDictionary],
    context: String,
    precalculated: Boolean = false
  ): String = {

    def firstMetaFromDeps(n: String): Option[MetaInfoRecord] = {
      for (d <- dependencies) {
        val value = d.meta_info_entry.get(n)
        if (!value.isEmpty)
          return value
      }
      None
    }

    nameToGidCache.get(name) match {
      case Some(v) =>
        v
      case None =>
        if (precalculated) {
          throw new GidNotPrecalculatedError(name, context)
        } else {
          val inProgress = mutable.ListBuffer[String]()
          var hasPending: Boolean = false
          val toDo = mutable.ListBuffer[String](name)

          for (i <- 1 to 2) {
            while (!toDo.isEmpty) {
              var now: String = ""
              if (!hasPending && !inProgress.isEmpty) {
                now = inProgress.last
                inProgress.trimEnd(1)
              } else {
                now = toDo.last
                toDo.trimEnd(1)
              }
              hasPending = false
              val nowMeta = metaInfos.get(now) match {
                case Some(meta) => meta
                case None => {
                  firstMetaFromDeps(now) match {
                    case Some(meta) => meta
                    case None => throw new MetaInfoEnv.DependsOnUnknownNameException(
                      context, name, now
                    )
                  }
                }
              }
              for (superName <- nowMeta.superNames) {
                if (!nameToGidCache.contains(superName)) {
                  hasPending = true
                  if (toDo.contains(superName))
                    toDo -= superName
                  if (inProgress.contains(superName))
                    throw new MetaInfoEnv.MetaInfoCircularDepException(
                      context, name, superName, inProgress
                    )
                  toDo += superName
                }
              }
              if (!hasPending) {
                val gidNow = evalGid(nowMeta, nameToGidCache)
                nameToGidCache += (now -> gidNow)
                if (inProgress.contains(now))
                  inProgress -= now
              } else {
                if (inProgress.contains(now))
                  throw new MetaInfoEnv.MetaInfoCircularDepException(
                    context, name, now, inProgress
                  )
                inProgress += now
              }
            }
            toDo ++= inProgress
            inProgress.clear()
          }
          nameToGidCache(name)
        }
    }
  }
 */

  def metaInfoEntry(metaName: String): Option[MetaInfoEntry] = {
    for ((n, d) <- dictionaries) {
      d.meta_info_entry.get(metaName) match {
        case Some(e) => return Some(e)
        case None =>
      }
    }
    None
  }
}
