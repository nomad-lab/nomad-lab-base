/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta
import scala.collection.mutable

/**
 * Interface to a collection of nomad meta info records
 */
trait MetaInfoCollection {

  /**
   * returns all versions defined (might contain duplicates!)
   */
  def allEnvs: Iterator[MetaInfoEnv]

  /**
   * returns all versions just once
   */
  def allUniqueEnvs(filter: MetaInfoEnv => Boolean = { _ => true }): Iterator[MetaInfoEnv] = {
    val seen = mutable.Set[MetaInfoEnv]()

    allEnvs.filter { el =>
      if (!filter(el) || seen(el)) {
        false
      } else {
        seen += el
        true
      }
    }
  }

  /**
   * returns the versions with the given name
   */
  def versionsWithName(name: String): Iterator[MetaInfoEnv] = {
    allUniqueEnvs { env: MetaInfoEnv => env.kind == MetaInfoEnv.Kind.Version && env.name == name }
  }

  /**
   * returns the versions that contain that gid
   *
   * If recursive is true, inclusion through a dependency is also
   * considered.
   */
  def versionsForGid(gid: String, recursive: Boolean = false): Iterator[String]

  /**
   * All gids of the meta infos in this collection
   *
   * might contain duplicates
   */
  def allGids: Iterator[String]

  /**
   * returns the MetaInfoRecord corresponding to the given gid
   *
   * gids and superGids are added only if requested
   */
  def metaInfoRecordForGid(gid: String, selfGid: Boolean = false, superGids: Boolean = false): Option[MetaInfoRecord];
}
