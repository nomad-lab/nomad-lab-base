/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta;
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import scala.collection.mutable
import scala.collection.breakOut
import org.{ json4s => jn }
import java.nio.file.Path
import java.nio.file.Paths
import com.typesafe.scalalogging.StrictLogging

object KnownMetaInfo extends StrictLogging {
  val envs: mutable.Map[String, MetaInfoEnv] = mutable.Map()

  /**
   * loads a meta info env from the resouces
   */
  def loadMetaEnv(metaPath: String): MetaInfo = {
    import MetaInfo.Uri
    import MetaInfo.UriProtocol

    MetaInfo.fromUri(Uri(
      protocol = UriProtocol.Resources,
      path = Paths.get(metaPath)
    ))
  }

  lazy val publicMeta = {
    loadMetaEnv(
      "meta_dictionary/public.meta_dictionary.json"
    )
  }
  lazy val common = {
    loadMetaEnv(
      "meta_dictionary/common.meta_dictionary.json"
    )
  }
  lazy val all = {
    loadMetaEnv(
      "meta_dictionary/all.meta_dictionary.json"
    )
  }
  lazy val last = {
    loadMetaEnv(
      "meta_dictionary/main.meta_dictionary.json"
    )
  }
  lazy val fhiAims = {
    loadMetaEnv(
      "meta_dictionary/fhi_aims.meta_dictionary.json"
    )
  }
  lazy val cp2k = {
    loadMetaEnv(
      "meta_dictionary/cp2k.meta_dictionary.json"
    )
  }
  lazy val cpmd = {
    loadMetaEnv(
      "meta_dictionary/cpmd.meta_dictionary.json"
    )
  }
  lazy val exciting = {
    loadMetaEnv(
      "meta_dictionary/exciting.meta_dictionary.json"
    )
  }
  lazy val gaussian = {
    loadMetaEnv(
      "meta_dictionary/gaussian.meta_dictionary.json"
    )
  }
  lazy val sampleParser = {
    loadMetaEnv(
      "meta_dictionary/sample_parser.meta_dictionary.json"
    )
  }
  lazy val lammps = {
    loadMetaEnv(
      "meta_dictionary/lammps.meta_dictionary.json"
    )
  }
  lazy val amber = {
    loadMetaEnv(
      "meta_dictionary/amber.meta_dictionary.json"
    )
  }
  lazy val gromacs = {
    loadMetaEnv(
      "meta_dictionary/gromacs.meta_dictionary.json"
    )
  }
  lazy val gromos = {
    loadMetaEnv(
      "meta_dictionary/gromos.meta_dictionary.json"
    )
  }
  lazy val namd = {
    loadMetaEnv(
      "meta_dictionary/namd.meta_dictionary.json"
    )
  }
  lazy val charmm = {
    loadMetaEnv(
      "meta_dictionary/charmm.meta_dictionary.json"
    )
  }
  lazy val tinker = {
    loadMetaEnv(
      "meta_dictionary/tinker.meta_dictionary.json"
    )
  }
  lazy val octopus = {
    loadMetaEnv(
      "meta_dictionary/octopus.meta_dictionary.json"
    )
  }
  lazy val quantumEspresso = {
    loadMetaEnv(
      "meta_dictionary/quantum_espresso.meta_dictionary.json"
    )
  }
  lazy val turbomole = {
    loadMetaEnv(
      "meta_dictionary/turbomole.meta_dictionary.json"
    )
  }
  lazy val gpaw = {
    loadMetaEnv(
      "meta_dictionary/gpaw.meta_dictionary.json"
    )
  }
  lazy val castep = {
    loadMetaEnv(
      "meta_dictionary/castep.meta_dictionary.json"
    )
  }
  lazy val onetep = {
    loadMetaEnv(
      "meta_dictionary/onetep.meta_dictionary.json"
    )
  }
  lazy val dlPoly = {
    loadMetaEnv(
      "meta_dictionary/dl_poly.meta_dictionary.json"
    )
  }
  lazy val libAtoms = {
    loadMetaEnv(
      "meta_dictionary/lib_atoms.meta_dictionary.json"
    )
  }
  lazy val dmol3 = {
    loadMetaEnv(
      "meta_dictionary/dmol3.meta_dictionary.json"
    )
  }
  lazy val vasp = {
    loadMetaEnv(
      "meta_dictionary/vasp.meta_dictionary.json"
    )
  }
  lazy val wien2k = {
    loadMetaEnv(
      "meta_dictionary/wien2k.meta_dictionary.json"
    )
  }
  lazy val fleur = {
    loadMetaEnv(
      "meta_dictionary/fleur.meta_dictionary.json"
    )
  }
  lazy val elk = {
    loadMetaEnv(
      "meta_dictionary/elk.meta_dictionary.json"
    )
  }
  lazy val nwchem = {
    loadMetaEnv(
      "meta_dictionary/nwchem.meta_dictionary.json"
    )
  }
  lazy val bigdft = {
    loadMetaEnv(
      "meta_dictionary/big_dft.meta_dictionary.json"
    )
  }
  lazy val dftbPlus = {
    loadMetaEnv(
      "meta_dictionary/dftb_plus.meta_dictionary.json"
    )
  }
  lazy val asap = {
    loadMetaEnv(
      "meta_dictionary/asap.meta_dictionary.json"
    )
  }
  lazy val atk = {
    loadMetaEnv(
      "meta_dictionary/atk.meta_dictionary.json"
    )
  }
  lazy val gamess = {
    loadMetaEnv(
      "meta_dictionary/gamess.meta_dictionary.json"
    )
  }
  lazy val gulp = {
    loadMetaEnv(
      "meta_dictionary/gulp.meta_dictionary.json"
    )
  }
  lazy val orca = {
    loadMetaEnv(
      "meta_dictionary/orca.meta_dictionary.json"
    )
  }
  lazy val fplo = {
    loadMetaEnv(
      "meta_dictionary/fplo.meta_dictionary.json"
    )
  }
  lazy val molcas = {
    loadMetaEnv(
      "meta_dictionary/molcas.meta_dictionary.json"
    )
  }
  lazy val phonopy = {
    loadMetaEnv(
      "meta_dictionary/phonopy.meta_dictionary.json"
    )
  }
  lazy val atomicData = {
    loadMetaEnv(
      "meta_dictionary/atomic_data.meta_dictionary.json"
    )
  }

  lazy val qhp = {
    loadMetaEnv(
      "meta_dictionary/quasi_harmonic_properties.meta_dictionary.json"
    )
  }

  lazy val elastic = {
    loadMetaEnv(
      "meta_dictionary/elastic.meta_dictionary.json"
    )
  }

  lazy val stats = {
    loadMetaEnv(
      "meta_dictionary/stats.meta_dictionary.json"
    )
  }

  lazy val repo = {
    loadMetaEnv(
      "meta_dictionary/repository.meta_dictionary.json"
    )
  }
  lazy val archive = {
    loadMetaEnv(
      "meta_dictionary/archive.meta_dictionary.json"
    )
  }

}
