/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import org.json4s.CustomSerializer
import scala.collection.breakOut
import eu.nomad_lab.JsonUtils
import scala.util.control.NonFatal
import scala.util.matching.Regex
import com.typesafe.scalalogging.StrictLogging

/**
 * Enumeration of values used in the metainfo, and using - as separator
 * In scala the - is replaced by the _, the "external" values are those using -.
 */
class ExtValues extends Enumeration {
  /**
   * list of external values (using '-')
   */
  def extValues: Seq[String] = values.map(_.toString.replace('_', '-')).toSeq

  /**
   * returns the external value corresponding to the given enumeration value
   */
  def toExtType(v: Value): String = v.toString.replace('_', '-')

  /**
   * returns the enumeration value corresponding to the external string
   */
  def withExtNameOpt(s: String): Option[Value] = {
    val ss = s.replace('-', '_')
    values.find(_.toString == ss)
  }
}

object MetaType extends ExtValues {
  type MetaType = Value
  val type_value, type_section, type_dimension, type_abstract = Value
}

object MetaDataType extends ExtValues {
  type MetaDataType = Value
  val int, int32, int64, reference, float32, float, float64, boolean, string, json = Value
  // binary
}

object MetaRangeKind extends ExtValues {
  type MetaRangeKind = Value
  val value, abs_value, norm2, utf8_length, repetitions = Value
}

case class MetaRangeExpected(
    meta_range_kind: MetaRangeKind.Value = MetaRangeKind.value,
    meta_range_minimum: Option[Double] = None,
    meta_range_maximum: Option[Double] = None,
    meta_range_units: Option[String] = None
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("meta_range_kind", MetaRangeKind.toExtType(meta_range_kind)) ~
      ("meta_range_minimum", meta_range_minimum) ~
      ("meta_range_maximum", meta_range_maximum) ~
      ("meta_range_units", meta_range_units)
  }
}

object MetaRangeExpected {
  def fromJValue(jValue: JValue, context: String)(implicit formats: org.json4s.Formats): MetaRangeExpected = {
    val ctx = context + ".meta_range_expected"
    jValue match {
      case JObject(o) =>
        var kind: Option[MetaRangeKind.Value] = None
        var min: Option[Double] = None
        var max: Option[Double] = None
        var units: Option[String] = None
        o.foreach {
          case JField("meta_range_kind", value) =>
            value match {
              case JString(m) =>
                kind = try {
                  MetaRangeKind.withExtNameOpt(m)
                } catch {
                  case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                    "meta_range_kind", ctx, JsonUtils.prettyStr(value), s"expected one of ${MetaRangeKind.extValues.mkString(", ")}"
                  )
                }
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_range_kind", ctx, JsonUtils.prettyStr(value), s"expected one of the following string values ${MetaRangeKind.extValues.mkString(", ")}"
              )
            }
          case JField("meta_range_minimum", value) =>
            min = try {
              value match {
                case JNull => None
                case _ =>
                  value.extract[Option[Double]]
              }
            } catch {
              case NonFatal(e) =>
                throw new JsonUtils.InvalidValueError(
                  "meta_range_minimum", ctx, JsonUtils.prettyStr(value), "null or a double"
                )
            }
          case JField("meta_range_maximum", value) =>
            max = try {
              value match {
                case JNull => None
                case _ =>
                  value.extract[Option[Double]]
              }
            } catch {
              case NonFatal(e) =>
                throw new JsonUtils.InvalidValueError(
                  "meta_range_maximum", ctx, JsonUtils.prettyStr(value), "null or a double"
                )
            }
          case JField("meta_range_units", value) =>
            value match {
              case JNull => units = None
              case JNothing => ()
              case JString(u) => units = Some(u)
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_range_expected.meta_range_units", JsonUtils.prettyStr(jValue), JsonUtils.prettyStr(value), "null or a string"
              )
            }
          case JField(k, v) =>
            throw new JsonUtils.InvalidValueError(
              "meta_range_expected", ctx, JsonUtils.prettyStr(jValue), s"a known field (meta_range_kind, meta_range_minimum,  meta_range_maximum, meta_range_units) not $k"
            )
        }
        MetaRangeExpected(
          meta_range_kind = kind.getOrElse(MetaRangeKind.value),
          meta_range_minimum = min,
          meta_range_maximum = max,
          meta_range_units = units
        )
      case v =>
        throw new JsonUtils.InvalidValueError(
          "meta_range_expected", context, JsonUtils.prettyStr(jValue), "a dictionary (object) with meta_range_kind, meta_range_minimum, meta_range_maximum, meta_range_units"
        )
    }
  }
}

/**
 * Describes one of the possible values of an enumeration
 */
case class MetaEnum(
    meta_enum_value: String,
    meta_enum_description: String
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("meta_enum_value", meta_enum_value) ~
      ("meta_enum_description", JsonUtils.stringToJValue(meta_enum_description))
  }
}

object MetaEnum extends StrictLogging {
  def fromJValue(jValue: JValue, context: String)(implicit formats: org.json4s.Formats): MetaEnum = {
    jValue match {
      case JObject(o) =>
        var meta_enum_value: Option[String] = None
        var meta_enum_description: Option[String] = None
        o.foreach {
          case JField("meta_enum_value", value) =>
            value match {
              case JString(m) => meta_enum_value = Some(m)
              case JNothing =>
              case JNull => meta_enum_value = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_enum_value", context, JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField("meta_enum_description", value) =>
            JsonUtils.jValueToString(value, "meta_enum_description", "meta_info_entry").foreach { s: String =>
              meta_enum_description = Some(s)
            }
          case _ => throw new JsonUtils.InvalidValueError(
            "meta_enum", context, JsonUtils.prettyStr(jValue), s"a dictionary with a description of a meta_enum (i.e. with meta_enum_value, meta_enum_description)"
          )
        }
        meta_enum_value match {
          case None => throw new JsonUtils.InvalidValueError(
            "meta_enum", context, JsonUtils.prettyStr(jValue), "field meta_enum_value"
          )
          case Some(v) =>
            MetaEnum(
              meta_enum_value = v,
              meta_enum_description = meta_enum_description match {
                case None =>
                  logger.warn(s"Missing meta_enum_description in ${JsonUtils.prettyStr(jValue)}")
                  ""
                case Some(d) => d
              }
            )
        }
      case v =>
        throw new JsonUtils.InvalidValueError(
          "meta_enum", context, JsonUtils.prettyStr(jValue), "a dictionary with a description of a meta_enum (i.e. with meta_enum_value, meta_enum_description)"
        )
    }
  }
}

/**
 * Describes a value usable for querying that might get mapped to a query expression
 */
case class MetaQueryEnum(
    meta_query_values: Seq[String] = Seq(),
    meta_query_regex: Option[Regex] = None,
    meta_query_expansion: Option[String] = None
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("meta_query_values", (if (meta_query_values.isEmpty) None else Some(meta_query_values))) ~
      ("meta_query_regex", meta_query_regex.map { _.regex }) ~
      ("meta_query_expansion", meta_query_expansion)
  }
}

object MetaQueryEnum {
  def fromJValue(jValue: JValue, context: String)(implicit formats: org.json4s.Formats): MetaQueryEnum = {
    var meta_query_values: Seq[String] = Seq()
    var meta_query_regex: Option[Regex] = None
    var meta_query_expansion: Option[String] = None
    jValue match {
      case JObject(o) =>
        o.foreach {
          case JField("meta_query_values", value) =>
            value match {
              case JString(m) => meta_query_values = meta_query_values :+ m
              case JNothing =>
              case JNull =>
                meta_query_values = Seq()
              case _ =>
                meta_query_values ++= (try {
                  value.extract[Seq[String]]
                } catch {
                  case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                    "meta_query_values", context, JsonUtils.prettyStr(value), s"a list of strings"
                  )
                })
            }
          case JField("meta_query_regex", value) =>
            value match {
              case JString(m) =>
                val r: Regex = try {
                  m.r
                } catch {
                  case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                    "meta_query_regex", context, JsonUtils.prettyStr(value), s"could not compile regexp ($e)"
                  )
                }
                meta_query_regex = Some(r)
              case JNothing =>
              case JNull => meta_query_regex = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_query_regex", context, JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField("meta_query_expansion", value) =>
            value match {
              case JString(m) => meta_query_expansion = Some(m)
              case JNothing =>
              case JNull => meta_query_expansion = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_query_expansion", context, JsonUtils.prettyStr(value), s"a string"
              )
            }
          case _ => throw new JsonUtils.InvalidValueError(
            "meta_query_enum", context, JsonUtils.prettyStr(jValue), s"a dictionary with a description of a meta_query_enum (i.e. with meta_query_values, meta_query_regex, meta_query_expansion)"
          )
        }
      case v =>
      // error catched in the if below...
    }
    if (meta_query_values.isEmpty && meta_query_regex.isEmpty && meta_query_expansion.isEmpty) throw new JsonUtils.InvalidValueError(
      "meta_query_enum", context, JsonUtils.prettyStr(jValue), "a dictionary with a description of a meta_query_enum (i.e. with meta_query_values, meta_query_regex, meta_query_expansion)"
    )
    MetaQueryEnum(
      meta_query_values = meta_query_values,
      meta_query_regex = meta_query_regex,
      meta_query_expansion = meta_query_expansion
    )
  }
}

/**
 * Describes one dimension of a potentially multi dimensional value
 */
case class MetaDimension(
    meta_dimension_fixed: Option[Long] = None,
    meta_dimension_symbolic: Option[String] = None
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("meta_dimension_fixed", meta_dimension_fixed) ~
      ("meta_dimension_symbolic", meta_dimension_symbolic)
  }
}

object MetaDimension extends StrictLogging {
  def fromJValue(jValue: JValue, context: String)(implicit formats: org.json4s.Formats): MetaDimension = {
    jValue match {
      case JObject(o) =>
        var meta_dimension_fixed: Option[Long] = None
        var meta_dimension_symbolic: Option[String] = None
        o.foreach {
          case JField("meta_dimension_fixed", value) =>
            value match {
              case JNothing =>
              case JNull => meta_dimension_fixed = None
              case _ =>
                meta_dimension_fixed = Some(try {
                  value.extract[Long]
                } catch {
                  case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                    "meta_dimension_fixed", context, JsonUtils.prettyStr(value), s"an integer"
                  )
                })
            }
          case JField("meta_dimension_symbolic", value) =>
            value match {
              case JString(m) => meta_dimension_symbolic = Some(m)
              case JNothing =>
              case JNull => meta_dimension_symbolic = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_dimension_symbolic", context, JsonUtils.prettyStr(value), s"a string"
              )
            }
          case _ => throw new JsonUtils.InvalidValueError(
            "meta_dimension_symbolic", context, JsonUtils.prettyStr(jValue), s"a dictionary with a description of a dimension (i.e. with meta_dimension_fixed, meta_dimension_symbolic)"
          )
        }
        meta_dimension_fixed match {
          case None =>
            meta_dimension_symbolic match {
              case None =>
                throw new JsonUtils.InvalidValueError(
                  "meta_dimension", context, JsonUtils.prettyStr(jValue), "either meta_dimension_fixed or meta_dimension_symbolic should be defined"
                )
              case Some(s) =>
                MetaDimension(meta_dimension_symbolic = Some(s))
            }
          case Some(d) =>
            meta_dimension_symbolic match {
              case None =>
                MetaDimension(meta_dimension_fixed = Some(d))
              case Some(s) =>
                throw new JsonUtils.InvalidValueError(
                  "meta_dimension", context, JsonUtils.prettyStr(jValue), "either meta_dimension_fixed or meta_dimension_symbolic should be defined, not both"
                )
            }
        }
      case v =>
        throw new JsonUtils.InvalidValueError(
          "meta_dimension", context, JsonUtils.prettyStr(jValue), "a dictionary with a meta_dimension (i.e. with either meta_dimension_fixed or meta_dimension_symbolic)"
        )
    }
  }
}

/**
 * Represents a constraint on a the data corresponding to a meta_entry
 */
case class MetaConstraint(
    meta_constraint_select_query: String,
    meta_constraint_name: Option[String] = None,
    meta_constraint_description: Option[String] = None,
    meta_constraint_required_query: Option[String] = None,
    meta_constraint_expected_meta_info: List[String] = Nil
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("meta_constraint_name" -> (meta_constraint_name match {
      case None => JNothing
      case Some(s) => JString(s)
    })) ~
      ("meta_constraint_description" -> (meta_constraint_description match {
        case Some(d) => JsonUtils.stringToJValue(d)
        case None => JNothing
      })) ~
      ("meta_constraint_select_query" -> meta_constraint_select_query) ~
      ("meta_constraint_required_query" -> meta_constraint_required_query) ~
      ("meta_constraint_expected_meta_info" -> meta_constraint_expected_meta_info)
  }
}

object MetaConstraint {
  def fromJValue(jValue: JValue, context: String): MetaConstraint = {
    val ctx = context + ".meta_constraint"
    jValue match {
      case JObject(o) =>
        var meta_constraint_name: Option[String] = None
        var meta_constraint_description: Option[String] = None
        var meta_constraint_select_query: Option[String] = None
        var meta_constraint_required_query: Option[String] = None
        var meta_constraint_expected_meta_info: List[String] = Nil
        o.foreach {
          case JField("meta_constraint_name", value) =>
            meta_constraint_name = JsonUtils.jValueToString(value, "meta_constraint_name", ctx)
          case JField("meta_constraint_description", value) =>
            meta_constraint_description = JsonUtils.jValueToString(value, "meta_constraint_description", ctx)
          case JField("meta_constraint_select_query", value) =>
            meta_constraint_select_query = JsonUtils.jValueToString(value, "meta_constraint_select_query", ctx)
          case JField("meta_constraint_required_query", value) =>
            meta_constraint_select_query = JsonUtils.jValueToString(value, "meta_constraint_select_query", ctx)
          case JField("meta_constraint_expected_meta_info", value) =>
            value match {
              case JString(m) => meta_constraint_expected_meta_info = m :: Nil
              case JNothing =>
              case JNull => meta_constraint_expected_meta_info = Nil
              case JArray(a) =>
                a.flatMap {
                  case JNothing => None
                  case JNull => None
                  case JString(s) => Some(s)
                  case v => throw new JsonUtils.InvalidValueError(
                    "meta_constraint_expected_meta_info", ctx, JsonUtils.prettyStr(v), s"only strings in the list"
                  )
                }
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_constraint_expected_meta_info", ctx, JsonUtils.prettyStr(value), s"a list of strings"
              )
            }
          case JField(k, v) => throw new JsonUtils.UnexpectedFieldError(
            context = ctx,
            field = k,
            value = v,
            info = s"a dictionary with a description of a constraint (i.e. with meta_constraint_name, meta_constraint_description, meta_constraint_select_query, meta_constraint_required_query, meta_constraint_expected_meta_info)"
          )
        }
        val selectQuery = meta_constraint_select_query match {
          case Some(q) => q
          case None => throw new JsonUtils.MissingFieldError(
            fieldName = "meta_constraint_select_query",
            context = ctx,
            info = "meta_constraint requires meta_constraint_select_query"
          )
        }
        MetaConstraint(
          meta_constraint_name = meta_constraint_name,
          meta_constraint_description = meta_constraint_description,
          meta_constraint_select_query = selectQuery,
          meta_constraint_required_query = meta_constraint_required_query,
          meta_constraint_expected_meta_info = meta_constraint_expected_meta_info
        )
      case v =>
        throw new JsonUtils.InvalidValueError(
          "meta_dimension", context, JsonUtils.prettyStr(jValue), "a dictionary with a meta_dimension (i.e. with either meta_dimension_fixed or meta_dimension_symbolic)"
        )
    }

  }
}
/**
 * Represents a piece of nomad meta info referring to other meta info by name.
 *
 * Can be interpreted within a context [[eu.nomad_lab.MetaInfoEnv]], but matches the
 * preferred way one describes it in a json file.
 */
sealed abstract class MetaInfoEntry {
  def meta_name: String

  def meta_gid: Option[String]

  def meta_type: MetaType.Value

  def meta_description: String

  def meta_parent_section: Option[String]

  def meta_parent_section_gid: Option[String]

  def meta_abstract_types: Seq[String]

  def meta_abstract_types_gids: Seq[String]

  def meta_deprecated: Boolean

  def otherKeys: List[(String, JValue)]

  protected def baseJValue: JObject

  /**
   * returns a JValue (json) representation of the current record
   *
   * It is possible to control if the extra arguments are output
   * just like other fields, as a sub dictionary or not at all
   */
  def toJValue(extraArgs: Boolean = true, inlineExtraArgs: Boolean = true, selfGid: Boolean = true, subGids: Boolean = true): JValue = {
    import org.json4s.JsonDSL._;
    val newLineRe = "[\n\r]".r
    val jDesc = JsonUtils.stringToJValue(meta_description)
    val coreObj = (("meta_name" -> meta_name) ~
      ("meta_type" -> meta_type.toString.replace('_', '-')) ~
      ("meta_gid" -> (if (selfGid) meta_gid else None)) ~
      ("meta_deprecated" -> (if (meta_deprecated) JBool(meta_deprecated) else JNothing)) ~
      ("meta_description" -> jDesc) ~
      ("meta_parent_section" -> meta_parent_section) ~
      ("meta_parent_section_gid" -> (if (subGids) meta_parent_section_gid else None)) ~
      ("meta_abstract_types" -> (if (meta_abstract_types.isEmpty) None else Some(meta_abstract_types))) ~
      ("meta_abstract_types_gids" -> (if (!subGids || meta_abstract_types_gids.isEmpty) None else Some(meta_abstract_types_gids))))

    val baseObj = baseJValue

    if (extraArgs) {
      if (inlineExtraArgs)
        JObject(coreObj.obj ++ baseObj.obj ++ otherKeys)
      else
        JObject(coreObj.obj ++ baseObj.obj :+ ("extraArgs" -> JObject(otherKeys)))
    } else {
      JObject(coreObj.obj ++ baseObj.obj)
    }
  }

  override def toString: String = {
    JsonUtils.normalizedStr(toJValue())
  }
}

case class MetaInfoValue(
    val meta_name: String,
    val meta_description: String,
    val meta_parent_sectionStr: String,
    val meta_data_type: MetaDataType.Value,
    val meta_deprecated: Boolean = false,
    val meta_gid: Option[String] = None,
    val meta_parent_section_gid: Option[String] = None,
    val meta_abstract_types: Seq[String] = Seq(),
    val meta_abstract_types_gids: Seq[String] = Seq(),
    val meta_dimension: List[MetaDimension] = Nil,
    val meta_enum: List[MetaEnum] = Nil,
    val meta_enum_from: Option[String] = None,
    val meta_query_enum: List[MetaQueryEnum] = Nil,
    val meta_range_expected: List[MetaRangeExpected] = Nil,
    val meta_repeats: Boolean = false,
    val meta_required: Boolean = false,
    val meta_referenced_section: Option[String] = None,
    val meta_value_validate: Option[String] = None,
    val meta_units: Option[String] = None,
    val otherKeys: List[(String, JValue)] = Nil
) extends MetaInfoEntry {

  override def meta_parent_section: Option[String] = Some(meta_parent_sectionStr)

  override def meta_type: MetaType.Value = MetaType.type_value

  override def baseJValue: JObject = {
    import org.json4s.JsonDSL._;
    ("meta_data_type" -> MetaDataType.toExtType(meta_data_type)) ~
      ("meta_dimension" -> (if (meta_dimension.isEmpty)
        JNothing
      else
        JArray(meta_dimension.map { (x: MetaDimension) => x.toJValue }))) ~
      ("meta_enum" -> (if (meta_enum.isEmpty)
        JNothing
      else
        JArray(meta_enum.map { (x: MetaEnum) => x.toJValue }))) ~
      ("meta_enum_from" -> meta_enum_from) ~
      ("meta_query_enum" -> (if (meta_query_enum.isEmpty)
        JNothing
      else
        JArray(meta_query_enum.map { (x: MetaQueryEnum) => x.toJValue }))) ~
      ("meta_range_expected" -> (if (meta_range_expected.isEmpty)
        JNothing
      else
        JArray(meta_range_expected.map {
          case m: MetaRangeExpected =>
            m.toJValue
        }))) ~
      ("meta_repeats" -> (if (meta_repeats)
        JBool(meta_repeats)
      else
        JNothing)) ~
      ("meta_required" -> (meta_required match {
        case false => JNothing
        case true => JBool(true)
      })) ~
      ("meta_referenced_section" -> meta_referenced_section) ~
      ("meta_units" -> meta_units)
  }
}

case class MetaInfoSection(
    val meta_name: String,
    val meta_description: String,
    val meta_deprecated: Boolean = false,
    val meta_gid: Option[String] = None,
    val meta_parent_section: Option[String] = None,
    val meta_parent_section_gid: Option[String] = None,
    val meta_abstract_types: Seq[String] = Seq(),
    val meta_abstract_types_gids: Seq[String] = Seq(),
    val meta_repeats: Boolean = true,
    val meta_required: Boolean = false,
    val meta_chosen_key: Option[String],
    val meta_context_identifier: List[String] = Nil,
    val meta_inject_if_section_regexp: List[String] = Nil,
    val meta_inject_if_abstract_type: List[String] = Nil,
    val otherKeys: List[(String, JValue)] = Nil
) extends MetaInfoEntry {

  override def meta_type: MetaType.Value = MetaType.type_section

  override def baseJValue: JObject = {
    import org.json4s.JsonDSL._;

    ("meta_repeats" -> JBool(meta_repeats)) ~
      ("meta_required" -> (meta_required match {
        case false => JNothing
        case true => JBool(true)
      })) ~
      ("meta_context_identifier" -> meta_context_identifier) ~
      ("meta_inject_if_section_regexp" -> (
        if (meta_inject_if_section_regexp.isEmpty) None else Some(meta_inject_if_section_regexp)
      )) ~
        ("meta_inject_if_abstract_type" -> (
          if (meta_inject_if_abstract_type.isEmpty) None else Some(meta_inject_if_abstract_type)
        )) ~
          ("meta_chosen_key" -> meta_chosen_key)
  }
}

case class MetaInfoDimension(
    val meta_name: String,
    val meta_description: String,
    val meta_deprecated: Boolean = false,
    val meta_gid: Option[String] = None,
    val meta_parent_section: Option[String] = None,
    val meta_parent_section_gid: Option[String] = None,
    val meta_abstract_types: Seq[String] = Seq(),
    val meta_abstract_types_gids: Seq[String] = Seq(),
    val otherKeys: List[(String, JValue)] = Nil
) extends MetaInfoEntry {
  override def meta_type: MetaType.Value = MetaType.type_dimension

  def meta_data_type: MetaDataType.Value = MetaDataType.int

  override def baseJValue: JObject = {
    JObject(Nil)
  }
}

case class MetaInfoAbstract(
    val meta_name: String,
    val meta_description: String,
    val meta_deprecated: Boolean = false,
    val meta_gid: Option[String] = None,
    val meta_abstract_types: Seq[String] = Seq(),
    val meta_abstract_types_gids: Seq[String] = Seq(),
    val otherKeys: List[(String, JValue)] = Nil
) extends MetaInfoEntry {
  override def meta_type: MetaType.Value = MetaType.type_abstract

  override def meta_parent_section: Option[String] = None

  override def meta_parent_section_gid: Option[String] = None

  override def baseJValue: JObject = {
    JObject(Nil)
  }
}

object MetaInfoEntry {
  def fromJValue(jValue: JValue, context: String, extraArgs: Boolean = true)(implicit formats: org.json4s.Formats): eu.nomad_lab.meta.MetaInfoEntry = {
    var meta_name: String = ""
    var meta_type: MetaType.Value = MetaType.type_value
    var meta_description: String = ""
    var meta_deprecated: Boolean = false
    var meta_data_type: Option[MetaDataType.Value] = None
    var meta_gid: Option[String] = None
    var meta_dimension: List[MetaDimension] = Nil
    var meta_chosen_key: Option[String] = None
    var meta_parent_section: Option[String] = None
    var meta_parent_section_gid: Option[String] = None
    var meta_abstract_types: Seq[String] = Seq()
    var meta_abstract_types_gids: Seq[String] = Seq()
    var meta_enum: List[MetaEnum] = Nil
    var meta_enum_from: Option[String] = None
    var meta_query_enum: List[MetaQueryEnum] = Nil
    var meta_range_expected: List[MetaRangeExpected] = Nil
    var meta_repeats: Option[Boolean] = None
    var meta_required: Boolean = false
    var meta_referenced_section: Option[String] = None
    var meta_units: Option[String] = None
    var meta_context_identifier: List[String] = Nil
    var meta_inject_if_section_regexp: List[String] = Nil
    var meta_inject_if_abstract_type: List[String] = Nil
    var meta_value_validate: Option[String] = None
    var otherKeys: List[(String, JValue)] = Nil
    var meta_derived: Boolean = false
    def ctx: String = {
      if (meta_name.isEmpty)
        context + ".meta_info_entry"
      else
        s"""${context}.meta_info_entry(meta_name:$meta_name)"""
    }

    jValue match {
      case JObject(obj) =>
        obj.foreach {
          case JField("meta_name", value) =>
            JsonUtils.jValueToString(value, "meta_name", "meta_info_entry").foreach { s: String =>
              meta_name = s
            }
          case JField("meta_type", value) =>
            value match {
              case JString(tStr) =>
                meta_type = MetaType.withExtNameOpt(tStr) match {
                  case Some(v) => v
                  case None => throw new JsonUtils.InvalidValueError(
                    "meta_type", ctx, JsonUtils.prettyStr(value), s"one of the following string values: ${MetaType.values.map(_.toString.replace('_', '-')).mkString(", ")}"
                  )
                }
              case JNothing | JNull => ()
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_type", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_description", value) =>
            JsonUtils.jValueToString(value, ctx, "meta_info_entry").foreach { s: String =>
              meta_description = s
            }
          case JField("meta_deprecated", value) =>
            value match {
              case JBool(v) =>
                meta_deprecated = v
              case JNull =>
                meta_deprecated = false
              case JNothing => ()
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_deprecated", ctx, JsonUtils.prettyStr(value), s"a boolean: true or false"
              )
            }
          case JField("meta_data_type", value) =>
            value match {
              case JString(m) =>
                meta_data_type = try {
                  Some(MetaDataType.withExtNameOpt(m).get)
                } catch {
                  case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                    "meta_data_type", ctx, JsonUtils.prettyStr(value), s"expected one of ${MetaDataType.extValues.mkString(", ")}"
                  )
                }
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_data_type", ctx, JsonUtils.prettyStr(value), s"expected one of the following string values ${MetaDataType.extValues.mkString(", ")}"
              )
            }
          case JField("meta_gid", value) =>
            value match {
              case JString(s) => meta_gid = Some(s)
              case JNothing => ()
              case JNull => meta_gid = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_gid", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_chosen_key", value) =>
            value match {
              case JString(s) => meta_chosen_key = Some(s)
              case JNothing => ()
              case JNull => meta_chosen_key = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_chosen_key", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_parent_section", value) =>
            value match {
              case JString(s) => meta_parent_section = Some(s)
              case JNothing => ()
              case JNull => meta_parent_section = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_parent_section", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_parent_section_gid", value) =>
            value match {
              case JString(s) => meta_parent_section_gid = Some(s)
              case JNothing => ()
              case JNull => meta_parent_section_gid = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_parent_section_gid", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_abstract_types", value) =>
            if (!value.toOption.isEmpty) {
              meta_abstract_types = try {
                value.extract[Seq[String]]
              } catch {
                case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                  "meta_abstract_types", ctx, JsonUtils.prettyStr(value), "a list of strings"
                )
              }
            }
          case JField("meta_abstract_types_gids", value) =>
            if (!value.toOption.isEmpty) {
              meta_abstract_types_gids = try {
                value.extract[Seq[String]]
              } catch {
                case NonFatal(e) => throw new JsonUtils.InvalidValueError(
                  "meta_abstract_types_gids", ctx, JsonUtils.prettyStr(value), "a list of strings"
                )
              }
            }
          case JField("meta_dimension", value) =>
            value match {
              case JArray(a) =>
                meta_dimension = a.map { (v: JValue) =>
                  MetaDimension.fromJValue(v, ctx)(formats)
                }
              case JNothing =>
              case JNull => meta_dimension = Nil
              case JObject =>
                meta_dimension = MetaDimension.fromJValue(value, ctx)(formats) :: Nil
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_enum", ctx, JsonUtils.prettyStr(value), "a list of objects defining the valid values"
              )
            }
          case JField("meta_enum", value) =>
            value match {
              case JArray(a) =>
                meta_enum = a.map { (v: JValue) =>
                  MetaEnum.fromJValue(v, ctx)(formats)
                }
              case JNothing =>
              case JNull => meta_enum = Nil
              case JObject =>
                meta_enum = MetaEnum.fromJValue(value, ctx)(formats) :: Nil
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_enum", ctx, JsonUtils.prettyStr(value), "a list of objects defining the valid values"
              )
            }
          case JField("meta_enum_from", value) =>
            value match {
              case JString(s) => meta_enum_from = Some(s)
              case JNothing => ()
              case JNull => meta_enum_from = None
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_enum_from", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_query_enum", value) =>
            value match {
              case JArray(a) =>
                meta_query_enum = a.map { (v: JValue) =>
                  MetaQueryEnum.fromJValue(v, ctx)(formats)
                }
              case JNothing =>
              case JNull => meta_query_enum = Nil
              case JObject =>
                meta_query_enum = MetaQueryEnum.fromJValue(value, ctx)(formats) :: Nil
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_enum", ctx, JsonUtils.prettyStr(value), "a list of objects defining the valid values"
              )
            }
          case JField("meta_range_expected", value) =>
            value match {
              case JArray(a) =>
                meta_range_expected = a.map { (v: JValue) =>
                  MetaRangeExpected.fromJValue(v, ctx)(formats)
                }
              case JNothing =>
              case JNull => meta_range_expected = Nil
              case JObject =>
                MetaRangeExpected.fromJValue(value, ctx)(formats)
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_range_expected", ctx, JsonUtils.prettyStr(value), "a list of objects defining the expected ranges (meta_range_kind, meta_range_minimum, meta_range_maximum, meta_range_units)"
              )
            }
          case JField("meta_repeats", value) =>
            value match {
              case JNull => meta_repeats = None
              case JNothing => ()
              case JBool(b) => meta_repeats = Some(b)
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_repeats", ctx, JsonUtils.prettyStr(value), "a boolean"
              )
            }
          case JField("meta_required", value) =>
            value match {
              case JNull => meta_required = false
              case JNothing => ()
              case JBool(b) => meta_required = b
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_required", ctx, JsonUtils.prettyStr(value), "a boolean"
              )
            }
          case JField("meta_referenced_section", value) =>
            value match {
              case JNothing => ()
              case JNull => meta_referenced_section = None
              case JString(s) => meta_referenced_section = Some(s)
              case _ => throw new JsonUtils.InvalidValueError(
                "meta_referenced_section", ctx, JsonUtils.prettyStr(value), "a string"
              )
            }
          case JField("meta_units", value) =>
            value match {
              case JNull => meta_units = None
              case JNothing =>
              case JString(s) =>
                meta_units = Some(s)
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "meta_units", ctx, JsonUtils.prettyStr(value), "a string"
                )
            }
          case JField("meta_context_identifier", value) =>
            value match {
              case JNull => meta_units = None
              case JNothing =>
              case JString(s) =>
                meta_context_identifier = s :: meta_context_identifier
              case JArray(a) =>
                meta_context_identifier = a.flatMap {
                  case JString(s) => Some(s)
                  case JNothing => None
                  case v => throw new JsonUtils.InvalidValueError(
                    "meta_context_identifier", ctx, JsonUtils.prettyStr(v), "only strings in the array"
                  )
                }
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "meta_context_identifier", ctx, JsonUtils.prettyStr(value), "a list of strings"
                )
            }
          case JField("meta_value_validate", value) =>
            meta_value_validate = JsonUtils.jValueToString(value, ctx, "meta_value_validate")
          case JField("meta_inject_if_section_regexp", value) =>
            value match {
              case JNull => meta_units = None
              case JNothing =>
              case JString(s) =>
                meta_inject_if_section_regexp = s :: meta_inject_if_section_regexp
              case JArray(a) =>
                meta_inject_if_section_regexp = a.flatMap {
                  case JString(s) => Some(s)
                  case JNothing => None
                  case v => throw new JsonUtils.InvalidValueError(
                    "meta_inject_if_section_regexp", ctx, JsonUtils.prettyStr(v), "only strings in the array"
                  )
                }
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "meta_inject_if_section_regexp", ctx, JsonUtils.prettyStr(value), "a list of strings"
                )
            }
          case JField("meta_inject_if_abstract_type", value) =>
            value match {
              case JNull => meta_units = None
              case JNothing =>
              case JString(s) =>
                meta_inject_if_abstract_type = s :: meta_inject_if_abstract_type
              case JArray(a) =>
                meta_inject_if_abstract_type = a.flatMap {
                  case JString(s) => Some(s)
                  case JNothing => None
                  case v => throw new JsonUtils.InvalidValueError(
                    "meta_inject_if_abstract_type", ctx, JsonUtils.prettyStr(v), "only strings in the array"
                  )
                }
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "meta_inject_if_abstract_type", ctx, JsonUtils.prettyStr(value), "a list of strings"
                )
            }
          case JField(k, v) =>
            if (extraArgs)
              otherKeys = (k -> v) :: otherKeys
            else
              throw JsonUtils.UnexpectedFieldError(
                context = ctx,
                field = k,
                value = v
              )
        }
      case _ => JsonUtils.InvalidValueError(
        "meta_info_entry", ctx, JsonUtils.normalizedStr(jValue), s"A dictionary object describing a meta_info_entry"
      )
    }
    otherKeys = otherKeys.reverse
    if (meta_name.isEmpty) throw new JsonUtils.MissingFieldError("meta_name", ctx)
    if (meta_description == "") throw new JsonUtils.MissingFieldError("meta_description", ctx)
    if (!meta_parent_section_gid.isEmpty && meta_parent_section.isEmpty)
      throw new JsonUtils.MissingFieldError("meta_parent_section", ctx, info = "when meta_parent_section_gid is given")
    if (!meta_abstract_types_gids.isEmpty && meta_abstract_types.length != meta_abstract_types_gids.length)
      throw new JsonUtils.InvalidValueError("meta_abstract_types_gids", ctx, meta_abstract_types_gids.mkString("[", ", ", "]"), s"incompatible length with meta_abstract_types ${meta_abstract_types.mkString("[", ",", "]")}")
    meta_type match {
      case MetaType.type_value =>
        val meta_parent_sectionStr = meta_parent_section match {
          case Some(s) => s
          case None => throw new JsonUtils.MissingFieldError("meta_parent_section", ctx, info = "for type-value")
        }
        if (meta_parent_section.isEmpty)
          throw new JsonUtils.MissingFieldError("meta_parent_section", ctx)
        meta_data_type match {
          case Some(dataType) =>
            MetaInfoValue(
              meta_name = meta_name,
              meta_description = meta_description,
              meta_data_type = dataType,
              meta_deprecated = meta_deprecated,
              meta_gid = meta_gid,
              meta_parent_sectionStr = meta_parent_sectionStr,
              meta_parent_section_gid = meta_parent_section_gid,
              meta_abstract_types = meta_abstract_types,
              meta_abstract_types_gids = meta_abstract_types_gids,
              meta_dimension = meta_dimension,
              meta_enum = meta_enum,
              meta_enum_from = meta_enum_from,
              meta_query_enum = meta_query_enum,
              meta_range_expected = meta_range_expected,
              meta_repeats = meta_repeats.getOrElse(false),
              meta_required = meta_required,
              meta_referenced_section = meta_referenced_section,
              meta_units = meta_units,
              meta_value_validate = meta_value_validate,
              otherKeys = otherKeys
            )
          case None =>
            throw JsonUtils.MissingFieldError(
              fieldName = "meta_data_type",
              context = ctx,
              info = "meta_type type-value requires meta_data_type"
            )
        }
      case MetaType.type_section =>
        MetaInfoSection(
          meta_name = meta_name,
          meta_description = meta_description,
          meta_deprecated = meta_deprecated,
          meta_gid = meta_gid,
          meta_parent_section = meta_parent_section,
          meta_parent_section_gid = meta_parent_section_gid,
          meta_abstract_types = meta_abstract_types,
          meta_abstract_types_gids = meta_abstract_types_gids,
          meta_chosen_key = meta_chosen_key,
          meta_context_identifier = meta_context_identifier,
          meta_repeats = meta_repeats.getOrElse(true),
          meta_required = meta_required,
          meta_inject_if_section_regexp = meta_inject_if_section_regexp,
          meta_inject_if_abstract_type = meta_inject_if_abstract_type,
          otherKeys = otherKeys
        )
      case MetaType.type_abstract =>
        MetaInfoAbstract(
          meta_name = meta_name,
          meta_description = meta_description,
          meta_deprecated = meta_deprecated,
          meta_gid = meta_gid,
          meta_abstract_types = meta_abstract_types,
          meta_abstract_types_gids = meta_abstract_types_gids,
          otherKeys = otherKeys
        )
      case MetaType.type_dimension =>
        MetaInfoDimension(
          meta_name = meta_name,
          meta_description = meta_description,
          meta_deprecated = meta_deprecated,
          meta_gid = meta_gid,
          meta_parent_section = meta_parent_section,
          meta_parent_section_gid = meta_parent_section_gid,
          meta_abstract_types = meta_abstract_types,
          meta_abstract_types_gids = meta_abstract_types_gids,
          otherKeys = otherKeys
        )
    }
  }
}

/**
 * Json serialization to and deserialization support for MetaInfoRecord
 */
class MetaInfoEntrySerializer extends CustomSerializer[MetaInfoEntry](format => (
  {
    case (jValue: JValue) =>
      MetaInfoEntry.fromJValue(jValue, "")(format)
  },
  {
    case x: MetaInfoEntry => {
      x.toJValue()
    }
  }
))
