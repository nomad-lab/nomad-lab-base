/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_queue

import java.io._
import java.nio.file.{ Files, Path, Paths, StandardCopyOption }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.{ CalculationParserResult, CalculationParserRequest, ToBeNormalizedQueueMessage }
import eu.nomad_lab.parsers.ParseResult
import eu.nomad_lab.parsers.ParserGenerator
import eu.nomad_lab.parsers.OptimizedParser
import eu.nomad_lab.parsers.ParseResult._
import eu.nomad_lab.{ JsonSupport, CompactSha, TreeType, parsers }
import org.apache.commons.compress.archivers.zip.{ ZipArchiveEntry, ZipFile }
import org.apache.commons.compress.utils.IOUtils
import org.json4s._
import eu.{ nomad_lab => lab }
import eu.nomad_lab.parsers.H5Backend
import eu.nomad_lab.parsers.JsonWriterBackend
import eu.nomad_lab.parsers.CalculationBackendGenerator
import eu.nomad_lab.parsers.CalculationBackend
import eu.nomad_lab.parsers.CombinedBackend
import eu.nomad_lab.parsers.AncillaryFilesPrefilter._
import scala.collection.mutable
import scala.util.control.NonFatal
import scala.util.Random

object CalculationParser extends StrictLogging {

  /**
   * An exception that indicates failure during parsing.
   *
   * @param message Incoming message to parse
   * @param calculationParser
   * @param msg
   * @param what
   */
  class CalculationParserException(
    message: CalculationParserRequest,
    calculationParser: CalculationParser,
    msg: String,
    what: Throwable = null
  ) extends Exception(
    s"$msg when parsing ${JsonSupport.writeStr(message)} (${calculationParser.ucRoot})",
    what
  )

  case class StatsValues(
    rootNamespace: String,
    parserName: String,
    parserId: String,
    parserInfo: JValue,
    mainFileUri: String,
    calculationGid: String,
    parsingStats: collection.Map[String, Long]
  )

  /**
   * If stats update callbacks are defined then wraps the backend created
   * so they call the callbacks
   */
  def addStats(

    backendCreate: lab.parsers.CalculationBackendGenerator.BackendCreate,
    statsUpdateCallback: Option[(CalculationParser.StatsValues) => Unit] = None,
    statsFinishedCallback: Option[(CalculationParser.StatsValues) => Unit] = None
  ): lab.parsers.CalculationBackendGenerator.BackendCreate = {
    if (!statsUpdateCallback.isEmpty || !statsFinishedCallback.isEmpty) {
      { (parser: OptimizedParser, targetPath: Path, repl: Map[String, String]) =>

        def callbackWrapper(statsCallback: Option[CalculationParser.StatsValues => Unit]): Option[(lab.parsers.StatsBackend.StatsBackendBase) => Unit] = {
          statsCallback match {
            case Some(callback) =>
              Some({ (statsBackend: lab.parsers.StatsBackend.StatsBackendBase) =>
                callback(
                  CalculationParser.StatsValues(
                    rootNamespace = lab.LocalEnv.defaultSettings.rootNamespace,
                    parserName = parser.parserGenerator.name,
                    parserId = parser.parserGenerator.parserIdentifier,
                    parserInfo = parser.parserGenerator.parserInfo,
                    mainFileUri = repl("mainFileUri"),
                    calculationGid = repl("calculationGid"),
                    parsingStats = statsBackend.stats
                  )
                )
              })
            case None => None
          }
        }

        parsers.StatsBackend.statsBackend(
          superBackend = backendCreate(parser, targetPath, repl),
          statsUpdateCallback = callbackWrapper(statsUpdateCallback),
          statsFinishedCallback = callbackWrapper(statsFinishedCallback)
        )
      }
    } else {
      backendCreate
    }
  }

  /**
   * computes the replacements for the current calculation
   */
  def baseReplForCalculation(
    parserGenerator: ParserGenerator,
    mainFileUri: String,
    replacements: Map[String, String]
  ): Map[String, String] = {
    var repl: Map[String, String] = replacements
    repl += ("mainFileUri" -> mainFileUri)
    repl += ("parserName" -> parserGenerator.name)
    repl += ("parserId" -> parserGenerator.parserIdentifier)
    val archiveGidRe = "^nmd://(R[-_a-zA-Z0-9]{28})(?:/.*)?$".r
    val nomadUrlRe = "^nmd://.*$".r
    val archiveGid = mainFileUri match {
      case archiveGidRe(aId) => aId
      case nomadUrlRe() =>
        logger.warn(s"encountred undexpected non archive nomadUrl ${mainFileUri}")
        "undefined"
      case _ =>
        "undefined"
    }
    repl += ("archiveGid" -> archiveGid)
    repl += ("archiveGidPrefix" -> archiveGid.take(3))
    repl += ("baseArchiveGid" -> archiveGid.drop(1))
    repl += ("baseArchiveGidPrefix" -> archiveGid.take(3).drop(1))
    val cSha = lab.CompactSha()
    cSha.updateStr(mainFileUri)
    val calculationGid = cSha.gidStr("C")
    val parsedFileGid = "P" + calculationGid.drop(1)
    val parsedArchiveGid = "n" + archiveGid.drop(1)
    val normalizedArchiveGid = "N" + archiveGid.drop(1)
    repl += ("parsedFileGid" -> parsedFileGid)
    repl += ("parsedFileGidPrefix" -> parsedFileGid.take(3))
    repl += ("calculationGid" -> calculationGid)
    repl += ("calculationGidPrefix" -> calculationGid.take(3))
    repl += ("parsedArchiveGid" -> parsedArchiveGid)
    repl += ("normalizedArchiveGid" -> normalizedArchiveGid)
    repl
  }

  def h5Generator: CalculationBackendGenerator = {
    val settings = lab.LocalEnv.defaultSettings
    val config = lab.LocalEnv.defaultConfig
    new CalculationBackendGenerator(
      //targetPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedSingleH5Path"),
      targetPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedCalculationH5Path"),
      targetTempPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedTempH5Path"),
      replacements = settings.replacements,
      createBackend = H5Backend.createBackend,
      overwrite = config.getBoolean("nomad_lab.parser_worker_rabbitmq.overwrite")
    )
  }

  def jsonGenerator: CalculationBackendGenerator = {
    val settings = lab.LocalEnv.defaultSettings
    val config = lab.LocalEnv.defaultConfig
    new CalculationBackendGenerator(
      targetPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedJsonPath"),
      replacements = settings.replacements,
      createBackend = JsonWriterBackend.createBackend,
      overwrite = config.getBoolean("nomad_lab.parser_worker_rabbitmq.overwrite")
    )
  }

  def jsonH5Generator: CalculationBackendGenerator = {
    val settings = lab.LocalEnv.defaultSettings
    val config = lab.LocalEnv.defaultConfig

    def createBackend(
      parser: OptimizedParser,
      fileToWrite: Path,
      repl: Map[String, String]
    ): CombinedBackend = {
      new CombinedBackend(
        internalBackend = H5Backend.createBackend(parser, fileToWrite, repl),
        externalBackends =
          (jsonGenerator.backendForCalculation(
            parser = parser,
            mainFileUri = repl("mainFileUri"),
            mainFilePath = Paths.get(repl("mainFilePath")),
            uncompressRoot = Paths.get(repl("uncompressRoot")),
            repl = repl
          ) match {
              case None =>
                Seq()
              case Some(jBackend) =>
                Seq(jBackend match {
                  case b: lab.parsers.ParserBackendExternal => b
                })
            } // use flatMap?
          )
      )
    }
    new CalculationBackendGenerator(
      targetPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedCalculationH5Path"),
      //targetPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedSingleH5Path"),
      targetTempPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedTempH5Path"), //DSB
      replacements = settings.replacements,
      createBackend = createBackend,
      overwrite = config.getBoolean("nomad_lab.parser_worker_rabbitmq.overwrite")
    )
  }

}

/**
 * release of a file with ref count 0
 */
class FileReleaseException(
  msg: String
) extends Exception(msg)

/**
 * This class implements methods to run the uncompress the archive (if required) and to run parser.
 *
 */
class CalculationParser(
    val ucRoot: String,
    val backendGenerator: lab.parsers.CalculationBackendGenerator,
    val parserCollection: parsers.ParserCollection,
    val replacements: Map[String, String],
    val ignoreMissingParsers: Boolean = false,
    val decompressWindow: Int = 60
) extends StrictLogging {
  val alreadyUncompressed = mutable.Set[String]()
  var lastArchivePath: String = ""
  var lastUncompressRoot: Option[Path] = None
  val cachedParsers = mutable.Map[String, OptimizedParser]()

  /**
   * increases the retain count in the file at the given path
   */
  def retainFile(path: Path): Unit = {
    if (!Files.exists(path)) {
      try {
        Files.createDirectories(path.getParent(), lab.LocalEnv.directoryPermissionsAttributes)
        Files.createFile(path, lab.LocalEnv.filePermissionsAttributes)
      } catch {
        case NonFatal(e) =>
          logger.warn(s"Error creating file for reference counting (retaining file $path) probably it was created by someone else", e)
      }
    }
    val f = new RandomAccessFile(path.toFile(), "rw")
    val lock = f.getChannel().lock()
    try {
      f.seek(0)
      val refCount = try {
        f.readInt()
      } catch {
        case NonFatal(e) =>
          0
      }
      f.seek(0)
      f.writeInt(refCount + 1)
    } finally {
      f.close()
    }
  }

  /** decreases the retain count in the file at the given path. If it hits 0 calls deleteOp */
  def releaseFile(path: Path, deleteOp: () => Unit): Int = {
    val f = new RandomAccessFile(path.toFile(), "rw")
    val lock = f.getChannel().lock()
    try {
      f.seek(0)
      val refCount = try {
        f.readInt()
      } catch {
        case NonFatal(e) =>
          0
      }
      if (refCount <= 0)
        throw new FileReleaseException(s"release fo file $path with reference count $refCount")
      else if (refCount == 1)
        deleteOp()
      f.seek(0)
      f.writeInt(refCount - 1)
      refCount - 1
    } finally {
      f.close()
    }
  }

  /**
   * Hierarchically uncompress all the files from the current level on.
   *
   */
  def uncompress(inMsg: CalculationParserRequest, uncompressRoot: Path, ancillaryFiles: AncillaryFilesPrefilter): Option[Path] = {
    val treeFilePath = if (!inMsg.treeFilePath.startsWith(lab.LocalEnv.defaultSettings.rawDataRoot.toString))
      inMsg.treeFilePath.replace(
        "/raw-data/data/",
        lab.LocalEnv.defaultSettings.rawDataRoot.resolve("data").toString() + "/"
      )
    else
      inMsg.treeFilePath
    val uncompressArchivePath = uncompressRoot.resolve(Paths.get(treeFilePath).getFileName())
    val lockFilePath = Paths.get(uncompressArchivePath.toString() + ".lock")
    if (treeFilePath != lastArchivePath || lastUncompressRoot != Some(uncompressArchivePath)) {
      lastUncompressRoot match {
        case Some(toRm) =>
          releaseFile(Paths.get(toRm.toString() + ".lock"), { () =>
            lab.LocalEnv.deleteRecursively(toRm)
          })
        case None => ()
      }
      retainFile(lockFilePath)
      val store = Files.getFileStore(uncompressArchivePath.getParent())
      var wait: Boolean = true
      var i = 0
      // wait up to 16m40s to have more space
      while (i < 100 && wait) {
        wait = false
        if (store.getUsableSpace() < 30 * 1024 * 1024 * 1024) { // less than 30G
          // check # archives
          val dirStream = Files.newDirectoryStream(uncompressArchivePath.getParent())
          val it = dirStream.iterator()
          var nArch = 0
          while (it.hasNext()) {
            val p = it.next();
            if (Files.isDirectory(p) && p != uncompressArchivePath) {
              nArch += 1
            }
          }
          if (nArch > 0)
            wait = true
        }
        if (wait) {
          logger.info(s"waiting to more space available for $uncompressArchivePath")
          Thread.sleep(10000) // wait 10s
        }
      }
      if (wait)
        logger.warn(s"failed to wait for space for $uncompressArchivePath")
      alreadyUncompressed.clear()
    }
    lastUncompressRoot = Some(uncompressArchivePath)
    lastArchivePath = treeFilePath

    def processOneDelayed(zipFile: ZipFile, delayed: mutable.Buffer[(Path, ZipArchiveEntry)]): Unit = {
      val idx = Random.nextInt(delayed.size)
      val (dest, entry) = delayed(idx)
      delayed(idx) = delayed(delayed.size - 1)
      delayed.remove(delayed.size - 1)
      if (!Files.exists(dest)) {
        Files.createDirectories(dest.getParent(), lab.LocalEnv.directoryPermissionsAttributes)
        val zIn: InputStream = zipFile.getInputStream(entry)
        val tmpPath = Files.createTempFile(dest.getParent(), "t", ".tmp", lab.LocalEnv.filePermissionsAttributes)
        val out: OutputStream = new FileOutputStream(tmpPath.toFile())
        IOUtils.copy(zIn, out)
        IOUtils.closeQuietly(zIn)
        out.close()
        Files.move(tmpPath, dest, java.nio.file.StandardCopyOption.ATOMIC_MOVE)
      }
    }
    inMsg.treeType match {
      case TreeType.Zip =>
        val delayed = mutable.Buffer[(Path, ZipArchiveEntry)]()
        val prefix = if (inMsg.relativeFilePath.contains("/")) {
          val prefixBasePath = Paths.get(inMsg.relativeFilePath)
          val prefixStr = ancillaryFiles match {
            case WholeUpload =>
              ""
            case ParentSubtree =>
              prefixBasePath.getParent.getParent.toString + "/"
            case WholeSubtree | SubtreeDepth1 | SameLevelOnly =>
              prefixBasePath.getParent.toString + "/"
            case MainFileOnly =>
              prefixBasePath.toString
          }
          if (prefixStr == "/")
            ""
          else
            prefixStr
        } else {
          ""
        }
        if (!alreadyUncompressed.contains(prefix)) {
          alreadyUncompressed += prefix
          val zipFile = new ZipFile(treeFilePath)
          try {
            val entries = zipFile.getEntries()
            while (entries.hasMoreElements()) {
              val zipEntry: ZipArchiveEntry = entries.nextElement()
              if (!zipEntry.isDirectory && !zipEntry.isUnixSymlink) {
                //Only check non directory and symlink for now; TODO: Add support to read symlink
                if (zipEntry.getName.startsWith(prefix)) {
                  val destinationPath = uncompressArchivePath.resolve(zipEntry.getName)
                  if (!destinationPath.toFile().exists()) {
                    delayed += (destinationPath -> zipEntry)
                    if (delayed.size > decompressWindow)
                      processOneDelayed(zipFile, delayed)
                  }
                }
              }
            }
            while (!delayed.isEmpty) {
              processOneDelayed(zipFile, delayed)
            }
          } finally {
            zipFile.close()
          }
        }
        Some(uncompressArchivePath.resolve(inMsg.relativeFilePath).toAbsolutePath)
      case _ => None
    }
  }

  /**
   * Main function that handles the incoming request to parse a file. Check if the parser, file to parse exists and whether to overwrite if parsedFileOutputPath exists.
   * Finally uncompress and call the parser and check the parser result.
   *
   */
  def handleParseRequest(inMsg: CalculationParserRequest): CalculationParserResult = {
    val t0 = new java.util.Date
    var didExist = false
    var created = false
    var errorMessage: Option[String] = None
    var parserId = inMsg.parserName + "_undefined"
    var parserInfo: JValue = JNothing
    var parsedFileUri: Option[String] = None
    var parsedFilePath: Option[String] = None
    var pResult: ParseResult = ParseFailure
    var backendInfo: JValue = JNothing
    val cSha = CompactSha()
    cSha.updateStr(inMsg.mainFileUri)
    val calculationGid = cSha.gidStr("C")
    parserCollection.parsers.get(inMsg.parserName) match {
      case None =>
        errorMessage = Some(s"Could not find parser named ${inMsg.parserName}")
      case Some(parser) =>
        parserInfo = parser.parserInfo
        parserId = parser.parserIdentifier
        var repl: Map[String, String] = CalculationParser.baseReplForCalculation(parser, inMsg.mainFileUri, replacements)
        val uncompressRoot = Paths.get(lab.LocalEnv.makeReplacements(repl, ucRoot))
        repl += ("uncompressRoot" -> uncompressRoot.toString())
        //        didExist = Files.exists(Paths.get(lab.LocalEnv.makeReplacements(repl, backendGenerator.targetPath)))targetTempPath
        didExist = Files.exists(Paths.get(lab.LocalEnv.makeReplacements(repl, backendGenerator.targetTempPath)))
        if (backendGenerator.overwrite || !didExist) {
          uncompress(inMsg, uncompressRoot, parser.ancillaryFilesPrefilter) match {
            case Some(filePath) =>
              repl += ("mainFilePath" -> filePath.toString())
              val optimizedParser = cachedParsers.get(inMsg.parserName) match {
                case Some(optParser) =>
                  cachedParsers.remove(inMsg.parserName)
                  optParser
                case None =>
                  parser.optimizedParser(Seq())
              }
              backendGenerator.backendForCalculation(
                parser = optimizedParser,
                mainFileUri = inMsg.mainFileUri,
                mainFilePath = filePath,
                uncompressRoot = uncompressRoot,
                repl = repl
              ) match {
                case None =>
                  pResult = ParseResult.ParseSkipped
                  didExist = Files.exists(Paths.get(lab.LocalEnv.makeReplacements(repl, backendGenerator.targetPath)))
                  created = false
                case Some(backend) =>
                  didExist = Files.exists(backend.targetPath)
                  backendInfo = backend.backendInfo
                  pResult = lab.parsers.SafeParsing.parse(optimizedParser, inMsg.mainFileUri, filePath, backend, parser.name)
                  backend.cleanup()
                  if (pResult != ParseResult.ParseFailure) {
                    if (optimizedParser.canBeReused)
                      cachedParsers += (inMsg.parserName -> optimizedParser)
                    else
                      optimizedParser.cleanup()
                  }
                  created = backend.targetPath.toFile().exists()
                  logger.info(s"ParserResult: $pResult when parsing $filePath by ${parser.name}")

                  pResult match {
                    case ParseSuccess | ParseWithWarnings =>
                      parsedFileUri = Some(lab.LocalEnv.makeReplacements(repl, "nmd://${parsedFileGid}"))
                      parsedFilePath = Some(backend.targetPath.toString)
                    case _ => ()
                  }
              }
            case None =>
              errorMessage = Some("Error uncompressing files")
          }
        } else {
          pResult = ParseResult.ParseSkipped
          created = false
        }
    }
    val t1 = new java.util.Date
    CalculationParserResult(
      rootNamespace = lab.LocalEnv.defaultSettings.rootNamespace,
      parseResult = pResult,
      parserId = parserId,
      calculationGid = calculationGid,
      parserInfo = parserInfo,
      parsedFileUri = parsedFileUri,
      parsedFilePath = parsedFilePath,
      didExist = didExist,
      created = created,
      errorMessage = errorMessage,
      parsingTimeSec = (t1.getTime() - t0.getTime()) / 1000.0,
      parseRequest = inMsg,
      backendInfo = backendInfo
    )
  }

  /**
   * Cleans up the after successful completion of calculation parser
   */
  def cleanup(): Unit = {
    for ((_, optParser) <- cachedParsers)
      optParser.cleanup()
    cachedParsers.clear()
    lastUncompressRoot match {
      case Some(toRm) =>
        releaseFile(Paths.get(toRm.toString() + ".lock"), { () =>
          lab.LocalEnv.deleteRecursively(toRm)
        })
        lastUncompressRoot = None
      case None => ()
    }
  }
}
