/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_queue

/**
 * gives access to section, calculation and archive
 */
class SectionAccessor {
}

/**
 * calculates other metadata that is contained in the same section that is analyzed
 * namely sectionMetaName.
 */
abstract class SectionCompleter(
    val name: String,
    val sectionMetaName: String,
    val dependencies: Seq[String] = Seq(),
    val supportedCodes: Seq[String] = Seq()
) {
  def completeSection(section: SectionAccessor): org.json4s.JObject
}

object StructureKind extends Enumeration {
  type StructureKind = Value
  val Bulk, Surface, Molecule, Atom = Value
}

abstract class StructuralClassifier(
  name: String,
  val supportedStuctureKinds: Seq[StructureKind.Value] = Seq(),
  dependencies: Seq[String] = Seq(),
  supportedCodes: Seq[String] = Seq()
) extends SectionCompleter(
  name = name,
  sectionMetaName = "section_system",
  dependencies = dependencies,
  supportedCodes = supportedCodes
) {
}

abstract class SingleConfigurationCalculationClassifier(
  name: String,
  val supportedStuctureKinds: Seq[StructureKind.Value] = Seq(),
  dependencies: Seq[String] = Seq(),
  supportedCodes: Seq[String] = Seq()
) extends SectionCompleter(
  name = name,
  sectionMetaName = "section_system_description",
  dependencies = dependencies,
  supportedCodes = supportedCodes
) {
}

object SingleConfigurationCalculationClusteringKind extends Enumeration {
  type SingleConfigurationCalculationClusteringKind = Value
  val SameStructureCalculations, ContainsTranslatedSubsystem = Value
}

abstract class SingleConfigurationCalculationClustering {
  // to define
}

/**
 * computes a property that depends on the whole archive (i.e it can depend on multiple calculations)
 */
abstract class ArchiveOperation(
    val name: String,
    val isStateless: Boolean = false,
    val requiredStructureClustering: Seq[SingleConfigurationCalculationClusteringKind.Value]
) {
  def addLocallyCompletedCalculation(locallyCompletedCalculation: SectionAccessor): Unit

  def registerClustering(structureClustering: Map[SingleConfigurationCalculationClusteringKind.Value, SingleConfigurationCalculationClustering]): Unit

  def archiveProperties(): org.json4s.JObject
}
