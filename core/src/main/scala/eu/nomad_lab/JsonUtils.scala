/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab;

import java.io.{ Reader, StringWriter, Writer, InputStream, OutputStream, ByteArrayInputStream, BufferedWriter, OutputStreamWriter, ByteArrayOutputStream }
import java.nio.charset.StandardCharsets
import org.json4s.native.JsonMethods.{ pretty, render, parse, compact }
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField, JLong, JSet }
import org.json4s.{ Diff, Merge, MergeDep, JsonInput }
import scala.collection.mutable.ListBuffer
import java.lang.{ Character => CC }

/**
 * methods to handle json (excluding custom serialization)
 *
 * In particular:
 * - parse to AST (parse*)
 * - serialize it in a predictable compact way (normalized*)
 * - serialize in a nicer humar readable format (pretty*)
 * - diff two json (diff*)
 * - merge two json (merge*)
 *
 * Aside these methods a user probably also wants to get AST
 *
 *     import org.json4s.{JNothing, JNull, JBool, JDouble, JDecimal, JInt,
 *                        JString, JArray, JObject, JValue, JField}
 *
 * add support for custom object serialization
 *
 *     import eu.nomad_lab.JsonSupport.{formats, read, write}
 *
 * and possibly for the DSL to write literals
 *
 *     import org.json4s.JsonDSL._
 *
 * First implementation, could be improved, but excluding obvious things
 * like the pretty print, that is worth doing only with real benchmarks.
 */
object JsonUtils {
  /**
   * parse the given string to the json AST
   */
  def parseStr(s: String): JValue = parse(s)

  /**
   * parse the given UTF_8 string to the json AST
   */
  def parseUtf8(s: Array[Byte]): JValue = parse(new ByteArrayInputStream(s))

  /**
   * parse the given string to the json AST
   */
  def parseReader(s: Reader): JValue = parse(s)

  /**
   * parse the given string to the json AST
   */
  def parseInputStream(s: InputStream): JValue = parse(s)

  /**
   * Adds the given amount of extra indent at the beginning of each line
   */
  class ExtraIndenter[W <: Writer](val extraIndent: Int, val writer: W) extends Writer {
    val indent: String = "\n" + (" " * extraIndent)

    override def close(): Unit = writer.close()

    override def flush(): Unit = writer.flush()

    override def write(c: Int): Unit = {
      if (c == '\n'.toByte)
        writer.write(indent)
      else
        writer.write(c)
    }

    override def write(s: Array[Char], offset: Int, len: Int): Unit = {
      // writer(s.replace("\n", indent))
      var i0 = offset
      var i1 = s.indexOf('\n', i0)
      var i2 = math.min(s.length, i0 + len)
      while (i1 >= 0 && i1 <= i2) {
        writer.write(s, i0, i1 - i0)
        writer.write(indent)
        i0 = i1 + 1
        i1 = s.indexOf('\n', i0)
      }
      if (i0 < i2)
        writer.write(s, i0, i2 - i0)
    }
  }

  /**
   * Dumps the given string escaping \ and "
   */
  def dumpString[W <: Writer](s: String, writer: W): Unit = {
    val cMap = Map(('\n' -> 'n'), ('\r' -> 'r'))
    writer.write('"')
    var i0 = 0
    var j = i0
    while (j < s.length) {
      val c = s(j)
      if (c == '\\' || c == '"' || c == '\n' || c == '\r') {
        writer.write(s, i0, j - i0)
        writer.write('\\')
        writer.write(cMap.getOrElse(c, c))
        i0 = j + 1
      }
      j += 1
    }
    writer.write(s, i0, s.length - i0)
    writer.write('"')
  }

  def escapeString(s: String): String = {
    val w = new java.io.StringWriter()
    dumpString(s, w)
    w.toString()
  }

  def unescapeQuotedString(s: String): String = {
    if (s.isEmpty || s(0) != '"')
      throw new Exception(s"expected a string in double quotes, not '$s'")
    parseStr(s"[$s]") match {
      case JArray(arr) =>
        arr match {
          case JString(res) :: Nil => res
          case v => throw new Exception(s"failed to parse a string, got '${normalizedStr(JArray(v))}'")
        }
      case v => throw new Exception(s"failed to parse an array, got '${normalizedStr(v)}'")
    }
  }

  /**
   * Dumps an normalized ordered json
   *
   * Object keys are alphabetically ordered.
   * This is the main reason that we cannot use the default writers.
   */
  def normalizedWriter[W <: Writer](obj: JValue, writer: W): Unit = {
    obj match {
      case JString(s) =>
        dumpString(s, writer)
      case JNothing =>
        ()
      case JDouble(num) =>
        writer.write(num.toString)
      case JDecimal(num) =>
        writer.write(num.toString)
      case JInt(num) =>
        writer.write(num.toString)
      case JLong(num) =>
        writer.write(num.toString)
      case JBool(value) =>
        if (value)
          writer.write("true")
        else
          writer.write("false")
      case JNull =>
        writer.write("null")
      case JObject(obj) =>
        writer.write('{')
        val iter = obj.toArray.sortBy(_._1).iterator
        while (iter.hasNext) {
          val (key, value) = iter.next
          if (value != JNothing) {
            dumpString(key, writer)
            writer.write(':')
            normalizedWriter(value, writer) // recursive call
            while (iter.hasNext) {
              val (key2, value2) = iter.next
              if (value2 != JNothing) {
                writer.write(',')
                dumpString(key2, writer)
                writer.write(':')
                normalizedWriter(value2, writer) // recursive call
              }
            }
          }
        }
        writer.write('}')
      case JArray(arr) =>
        writer.write('[');
        val iter = arr.iterator
        while (iter.hasNext) {
          val value = iter.next
          if (value != JNothing) {
            normalizedWriter(value, writer)
            while (iter.hasNext) {
              val value2 = iter.next
              if (value2 != JNothing) {
                writer.write(',')
                normalizedWriter(value2, writer) // recursive call
              }
            }
          }
        }
        writer.write(']')
      case JSet(arr) =>
        writer.write('[');
        val iter = arr.iterator
        while (iter.hasNext) {
          val value = iter.next
          if (value != JNothing) {
            normalizedWriter(value, writer)
            while (iter.hasNext) {
              val value2 = iter.next
              if (value2 != JNothing) {
                writer.write(',')
                normalizedWriter(value2, writer) // recursive call
              }
            }
          }
        }
        writer.write(']')
    }
  }

  /**
   * Dumps a normalized ordered json
   *
   * Object keys are alphabetically ordered.
   * This is the main reason that we cannot use the default writers.
   * Probably this could be slighlty faster if *all* string would be UTF_8
   * as this is not the case going thought a Writer looses little.
   */
  def normalizedOutputStream[W <: OutputStream](value: JValue, out: W): Unit = {
    val writer = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8))
    normalizedWriter(value, writer)
    writer.flush() // we do not want to propagate the close to out, so we just flush (correct behaviour or we need to reimplement?)
  }

  /**
   * returns a normalized sorted json representation
   */
  def normalizedUtf8(value: JValue): Array[Byte] = {
    val writer = new ByteArrayOutputStream()
    normalizedOutputStream(value, writer)
    writer.toByteArray()
  }

  /**
   * returns a string with a normalized sorted json representation
   *
   * this is just for convenience, try to avoid its use
   */
  def normalizedStr(value: JValue): String = {
    val writer = new StringWriter()
    normalizedWriter(value, writer)
    writer.toString()
  }

  /**
   * calculates a measure of the complexity (size) of the json
   */
  def jsonComplexity(value: JValue): Int = value match {
    case JNothing => 1
    case JNull => 1
    case JBool(_) => 1
    case JDouble(_) => 1
    case JDecimal(_) => 1
    case JInt(_) => 1
    case JLong(_) => 1
    case JString(_) => 1
    case JSet(arr) =>
      arr.foldLeft(1)(_ + jsonComplexity(_))
    case JArray(arr) =>
      arr.foldLeft(1)(_ + jsonComplexity(_))
    case JObject(obj) =>
      obj.foldLeft(1) { (i: Int, value: JField) =>
        value match {
          case JField(k, v) => i + jsonComplexity(v)
        }
      }
  }

  /**
   * Dumps an indented json
   *
   * Object keys are output in generation order (not necessarily alphabetically).
   *
   * Currently inefficent, use Serialization.write?
   */
  def prettyWriter[W <: Writer](value: JValue, writer: W, extraIndent: Int = 0): Unit = {
    if (JNothing != value) {
      val w = if (extraIndent > 0)
        new ExtraIndenter(extraIndent, writer)
      else
        writer
      // width could be given if we would use something where a line can be broken by default
      // currently we use width=0 (infinite)
      render(value).format(0, w)
    }
  }

  /**
   * Dumps an indented json
   *
   * Object keys are output in generation order (not necessarily alphabetically).
   *
   * Currently inefficent, use Serialization.write?
   */
  def prettyOutputStream[W <: OutputStream](value: JValue, writer: W, extraIndent: Int = 0): Unit = {
    val out = new BufferedWriter(new OutputStreamWriter(writer, StandardCharsets.UTF_8))
    prettyWriter(value, out, extraIndent)
    out.flush()
  }

  /**
   * Returns an UTF_8 encoded indented json
   *
   * Object keys are output in generation order (not necessarily alphabetically)
   */
  def prettyUft8(value: JValue, extraIndent: Int = 0): Array[Byte] =
    prettyStr(value, extraIndent).getBytes(StandardCharsets.UTF_8)

  /**
   * Returns a string with indented json
   *
   * Object keys are output in generation order (not necessarily alphabetically)
   */
  def prettyStr(value: JValue, extraIndent: Int = 0): String = {
    val w = new StringWriter()
    prettyWriter(value, w, extraIndent)
    w.toString()
  }

  /**
   * Returns a string with compact json
   *
   * Object keys are output in generation order (not necessarily alphabetically)
   */
  def compactStr(value: JValue): String = {
    value match {
      case JNothing => ""
      case _ => compact(render(value))
    }
  }

  /**
   * Dumps a compact json
   *
   * Object keys are output in generation order (not necessarily alphabetically).
   *
   * Currently inefficent, use Serialization.write?
   */
  def compactWriter[W <: Writer](value: JValue, writer: W): Unit = {
    writer.write(compactStr(value))
  }

  /**
   * Returns a json array by merging the two arguments
   */
  def mergeArrays(val1: JArray, val2: JArray): JArray =
    Merge.merge(val1, val2)

  /**
   * Returns a json object by merging the two arguments
   */
  def mergeObjects(val1: JObject, val2: JObject): JObject =
    Merge.merge(val1, val2)

  /**
   * Returns a json value by merging the two arguments
   */
  def mergeValues(val1: JValue, val2: JValue): JValue =
    Merge.merge(val1, val2)

  /**
   * Returns the differences between two json values
   */
  def diff(val1: JValue, val2: JValue): Diff = Diff.diff(val1, val2)

  /**
   * Converts a JString or an JArray of JStrings to a String
   *
   * Raises InvalidValueError if the value cannot be converted to a string
   */
  def jValueToString(value: JValue, field: String, context: String): Option[String] = {
    value match {
      case JString(s) => Some(s)
      case JArray(arr) =>
        val sb = new StringBuilder()
        arr.foreach {
          case JString(s) => sb ++= (s)
          case JNothing => ()
          case v => JsonUtils.InvalidValueError(
            "meta_description", "NomadMetaInfo", JsonUtils.prettyStr(v), "either a string or an array of *only* strings"
          )
        }
        Some(sb.toString)
      case JNothing | JNull => None
      case _ => throw new JsonUtils.InvalidValueError(
        "meta_description", "NomadMetaInfo", JsonUtils.prettyStr(value), "either a string or an array of strings"
      )
    }
  }

  /**
   * Converts a string to either a JString or an JArray of JStrings
   */
  def maybeStringToJValue(v: Option[String], maxLineLength: Int = 80, forceArray: Boolean = false): JValue = {
    v match {
      case Some(s) =>
        stringToJValue(s, maxLineLength, forceArray)
      case None =>
        if (forceArray)
          JArray(Nil)
        else
          JNothing
    }
  }

  /**
   * Converts a string to either a JString or an JArray of JStrings
   */
  def stringToJValue(s: String, maxLineLength: Int = 80, forceArray: Boolean = false): JValue = {
    val maxL = maxLineLength.max(10)
    val maxL2 = maxL / 2
    var i: Int = 0;
    var l: List[String] = Nil
    while (i < s.length) {
      val j: Int = s.indexOf('\n', i)
      if (j > -1) {
        if (j - i < maxL) {
          l = s.slice(i, j + 1) :: l
          i = j + 1
        } else {
          val j2 = s.lastIndexWhere({ c: Char => !c.isWhitespace }, j)
          if (j2 - i < maxL) {
            if (j2 - i < maxL2) {
              val iEnd = fixSplitPoint(s, i + maxL, i + maxL2)
              l = s.slice(i, iEnd) :: l
              i = iEnd
            } else {
              val iEnd = fixSplitPoint(s, j2 + 1, i + maxL2)
              l = s.slice(i, iEnd) :: l
              i = iEnd
            }
          } else {
            val k = s.lastIndexWhere({ c: Char => c.isWhitespace }, i + maxL)
            if (k >= i + maxL2) {
              val iEnd = fixSplitPoint(s, k + 1, i + maxL2)
              l = s.slice(i, iEnd) :: l
              i = iEnd
            } else {
              val iEnd = fixSplitPoint(s, i + maxL, i + maxL2)
              l = s.slice(i, iEnd) :: l
              i = iEnd
            }
          }
        }
      } else {
        val k = s.lastIndexWhere({ c: Char => c.isWhitespace }, i + maxL - 1)
        if (k >= i + maxL2) {
          val iEnd = fixSplitPoint(s, k + 1, i + maxL2)
          l = s.slice(i, iEnd) :: l
          i = iEnd
        } else {
          val iEnd = fixSplitPoint(s, s.length.min(i + maxL), i + maxL2)
          l = s.slice(i, iEnd) :: l
          i = iEnd
        }
      }
    }
    l match {
      case Nil =>
        if (forceArray)
          JArray(JString("") :: Nil)
        else
          JString("")
      case el :: Nil =>
        if (forceArray)
          JArray(JString(el) :: Nil)
        else
          JString(el)
      case a =>
        JArray(a.reverseMap { s: String => JString(s) })
    }
  }

  /**
   * Fix a proposed split point of a string trying to avoid splitting "special" unicode characters.
   *
   * i is the split point, minI the wanted minimum split point
   */
  def fixSplitPoint(s: String, i: Int, minI: Int = 0): Int = {
    // Quick & dirty list done without much research might need tweaking...
    val splittableSet = Set[Int](
      CC.CONNECTOR_PUNCTUATION, // General category "Pc" in the Unicode specification.
      CC.CURRENCY_SYMBOL, // General category "Sc" in the Unicode specification.
      CC.DASH_PUNCTUATION, // General category "Pd" in the Unicode specification.
      CC.DECIMAL_DIGIT_NUMBER, // General category "Nd" in the Unicode specification.
      CC.END_PUNCTUATION, // General category "Pe" in the Unicode specification.
      CC.FINAL_QUOTE_PUNCTUATION, // General category "Pf" in the Unicode specification.
      CC.INITIAL_QUOTE_PUNCTUATION, // General category "Pi" in the Unicode specification.
      CC.LETTER_NUMBER, // General category "Nl" in the Unicode specification.
      CC.LINE_SEPARATOR, // General category "Zl" in the Unicode specification.
      CC.LOWERCASE_LETTER, // General category "Ll" in the Unicode specification.
      CC.MATH_SYMBOL, // General category "Sm" in the Unicode specification.
      CC.OTHER_LETTER, // General category "Lo" in the Unicode specification.
      CC.OTHER_NUMBER, // General category "No" in the Unicode specification.
      CC.OTHER_PUNCTUATION, // General category "Po" in the Unicode specification.
      CC.OTHER_SYMBOL, // General category "So" in the Unicode specification.
      CC.PARAGRAPH_SEPARATOR, // General category "Zp" in the Unicode specification.
      CC.SPACE_SEPARATOR, // General category "Zs" in the Unicode specification.
      CC.START_PUNCTUATION, // General category "Ps" in the Unicode specification.
      CC.TITLECASE_LETTER, // General category "Lt" in the Unicode specification.
      CC.UPPERCASE_LETTER
    ) // General category "Lu" in the Unicode specification.

    // excluded:
    //  ENCLOSING_MARK,         // General category "Me" in the Unicode specification.
    //  MODIFIER_LETTER,        // General category "Lm" in the Unicode specification.
    //  NON_SPACING_MARK,       // General category "Mn" in the Unicode specification.
    //  MODIFIER_SYMBOL,        // General category "Sk" in the Unicode specification.
    //  COMBINING_SPACING_MARK, // General category "Mc" in the Unicode specification.
    //  FORMAT,                 // General category "Cf" in the Unicode specification.
    //  CONTROL,                // General category "Cc" in the Unicode specification.
    //
    //  PRIVATE_USE,            // General category "Co" in the Unicode specification.
    //  SURROGATE,              // General category "Cs" in the Unicode specification.
    //  UNASSIGNED,             // General category "Cn" in the Unicode specification.

    if (i >= s.length)
      return s.length
    var resI: Int = i
    while (resI >= minI) {
      if (s(i).isLowSurrogate)
        resI -= 1
      else {
        val cp = s.codePointAt(resI)
        if (CC.isLetter(cp) || CC.isWhitespace(cp) || splittableSet.contains(CC.getType(cp)))
          return resI
        else
          resI -= 1
      }
    }
    // nice split failed, just avoid splitting surrogates
    resI = i
    if (s(i).isLowSurrogate)
      resI -= 1
    return resI
  }

  /**
   * Error when reading from json and a required field is missing or empty
   */
  case class MissingFieldError(
    val fieldName: String,
    val context: String,
    val info: String = ""
  ) extends Exception(
    s"missing or empty required field $fieldName in $context. $info"
  ) {
  }

  /**
   * Error when reading a json field and the value is unexpected
   */
  case class InvalidValueError(
    val fieldName: String,
    val context: String,
    val value: String,
    val expected: String
  ) extends Exception(
    "invalid value for field " ++ fieldName ++ " in " ++ context ++ ", expected " ++ expected ++ " but got " ++ value
  ) {
  }

  /**
   * Error when reading from json and an unexpected extra value is found
   *
   * Often you want to ignore such errors, think carefully before using this.
   */
  case class UnexpectedFieldError(
    val context: String,
    val field: String,
    val value: JValue,
    val info: String = ""
  ) extends Exception(
    s"Unexpected field $field in $context with value ${JsonUtils.prettyStr(value)}. $info"
  ) {
  }

  object SplitterStatus extends Enumeration {
    type SplitterStatus = Value

    val Normal, InIndent, InArray, InArrayIndent, InOuterString, InArrayString, InOuterStringEscape, InArrayStringEscape = Value
  }

  /**
   * Writer that indents arrays of strings
   */
  class StringArraySplitWriter[W <: Writer](val writer: W) extends Writer {
    var status: SplitterStatus.Value = SplitterStatus.Normal
    var currentIndent = 0
    var stringArrayIndent = 0

    def stepStatus(c: Int, out: String => Unit): Unit = {
      import SplitterStatus._
      import java.lang.Character
      status match {
        case Normal =>
          c match {
            case '\n' =>
              status = InIndent
              currentIndent = 0
            case '"' =>
              status = InOuterString
            case '[' =>
              status = InArray
              stringArrayIndent = currentIndent + 2
            case _ =>
          }
        case InIndent =>
          c match {
            case ' ' =>
              currentIndent += 1
            case '\t' =>
              currentIndent += 8
            case '\n' =>
              status = InIndent
              currentIndent = 0
            case '"' =>
              status = InOuterString
            case '[' =>
              status = InArray
              stringArrayIndent = currentIndent + 2
            case _ =>
              status = Normal
          }
        case InArray =>
          c match {
            case '{' =>
              status = Normal
            case '\n' =>
              status = InArrayIndent
              currentIndent = 0
            case '"' =>
              status = InArrayString
              out("\n" + (" " * stringArrayIndent))
              currentIndent = 0
            case '[' =>
              status = InArray
            case ']' =>
              status = Normal
            case ' ' | ',' => ()
            case _ =>
              status = Normal
          }
        case InArrayIndent =>
          c match {
            case ' ' =>
              currentIndent += 1
            case '\t' =>
              currentIndent += 8
            case '{' =>
              status = Normal
            case '\n' =>
              status = InArrayIndent
              currentIndent = 0
            case '"' =>
              status = InArrayString
              if (stringArrayIndent > currentIndent)
                out(" " * (stringArrayIndent - currentIndent))
            case '[' =>
              status = InArray
              stringArrayIndent = currentIndent + 2
            case ',' =>
              status = InArray
            case _ =>
              status = Normal
          }
        case InArrayString =>
          c match {
            case '"' =>
              status = InArray
            case '\\' =>
              status = InArrayStringEscape
            case _ =>
          }
        case InArrayStringEscape =>
          status = InArrayString
        case InOuterString =>
          c match {
            case '"' =>
              status = Normal
            case '\\' =>
              status = InOuterStringEscape
            case _ =>
          }
        case InOuterStringEscape =>
          status = InOuterString
      }
    }

    override def close(): Unit = writer.close()

    override def flush(): Unit = writer.flush()

    override def write(c: Int): Unit = {
      stepStatus(c, { s => writer.write(s) })
      writer.write(c)
    }

    override def write(s: Array[Char], offset: Int, len: Int): Unit = {
      var i = offset
      var iWritten = offset
      val ll = s.length.min(offset + len)
      while (i < ll) {
        val c = Character.codePointAt(s, i)
        try {
          stepStatus(c, { ss =>
            writer.write(s.slice(iWritten, i))
            writer.write(ss)
            iWritten = i
          })
        } catch {
          case util.control.NonFatal(e) =>
            throw new Exception(s"error with char '$c' at $i in '$s'", e)
        }
        i += Character.charCount(c)
      }
      writer.write(s, iWritten, ll - iWritten)
    }
  }

}
