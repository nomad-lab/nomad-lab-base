/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.ref

import collection.breakOut
import collection.mutable
import collection.SortedMap

/**
 * References to objects in the nomad archive
 */
object NomadUri {

  object PathKind extends Enumeration {
    type PathKind = Value
    val NoPath, MetaPath, PathWithSomeLocalIndexes, PathWithAllLocalIndexes, MixedPath = Value
  }

  val indexRe = "^([0-9]+)([a-zA-Z]*)$".r

  /**
   * tries to expand a path as pathSegment, index pairs
   */
  def toIndexedPath(pathSeq: Seq[String]): Seq[(String, SortedMap[String, Long])] = {
    var iPath: Int = 0
    val res = mutable.ListBuffer[(String, SortedMap[String, Long])]()
    var indexes: SortedMap[String, Long] = SortedMap[String, Long]()
    var segmentName: String = ""
    while (iPath < pathSeq.length) {
      pathSeq(iPath) match {
        case "" =>
          iPath += 1
        case indexRe(nStr, indexKind) =>
          if (segmentName.isEmpty)
            throw new NomadUriException(s"a segment name is expected before an index $pathSeq")
          iPath += 1
          if (indexes.contains(indexKind))
            throw new NomadUriException(s"repeated index of kind '$indexKind' in $pathSeq")
          val key = if (indexKind == null)
            ""
          else
            indexKind
          indexes += (key -> nStr.toLong)
        case str =>
          if (!segmentName.isEmpty)
            res += (segmentName -> indexes)
          indexes = SortedMap[String, Long]()
          segmentName = str
          iPath += 1
      }
    }
    if (segmentName != "")
      res += (segmentName -> indexes)
    res
  }

  def kindOfPath(indexedPath: Seq[(String, Option[Long])]): PathKind.Value = {
    indexedPath.foldLeft(PathKind.NoPath) { (pathKind: PathKind.Value, pathSegment: (String, Option[Long])) =>
      pathSegment match {
        case (pSegm, index) =>
          if (pSegm.isEmpty()) {
            index match {
              case Some(_) => PathKind.MixedPath
              case None => pathKind
            }
          } else {
            index match {
              case None =>
                pathKind match {
                  case PathKind.NoPath => PathKind.MetaPath
                  case PathKind.MetaPath => pathKind
                  case PathKind.PathWithSomeLocalIndexes => pathKind
                  case PathKind.PathWithAllLocalIndexes => PathKind.PathWithSomeLocalIndexes
                  case PathKind.MixedPath => pathKind
                }
              case Some(_) =>
                pathKind match {
                  case PathKind.NoPath => PathKind.PathWithAllLocalIndexes
                  case PathKind.MetaPath => PathKind.PathWithSomeLocalIndexes
                  case PathKind.PathWithSomeLocalIndexes => pathKind
                  case PathKind.PathWithAllLocalIndexes => pathKind
                  case PathKind.MixedPath => pathKind
                }
            }
          }
      }
    }
  }

  val uriRe = "^nmd://([^/?#\\s]*)(?:/([^?#\\s]*))?(?:\\?([^#\\s]*))?(?:#([\\s]*))?$".r
  val objectRe = "^([RSN])([-_a-zA-Z0-9]{28})$".r
  val calculationGidRe = "^(C[-_a-zA-Z0-9]{28}|_)$".r
  val calcRe = "^(C[-_a-zA-Z0-9]{28})".r
  val intRe = "^([0-9]+)$".r
  /**
   * returns a parsed nomad uri
   */
  def apply(uri: String): NomadUri = {
    uri match {
      case uriRe(objectId, pathStr, queryStr, fragmentStr) =>
        val archiveGid = "R" + objectId.substring(1)
        val fragment = if (fragmentStr == null || fragmentStr.isEmpty()) None else Some(fragmentStr)
        var queryMap: Map[String, String] = if (queryStr == null || queryStr.isEmpty()) Map() else queryStr.split('&').map { x: String =>
          val kv = x.split('=')
          if (kv.length != 2)
            throw new NomadUriException(s"query fragment '$x' does not have the expected key=value from in nomad uri $uri")
          kv(0) -> kv(1)
        }(breakOut)
        val versionInfo: Map[String, String] = queryMap.getOrElse("version", "").split(',').
          flatMap { vEl =>
            if (vEl.isEmpty()) {
              None
            } else {
              val kv = vEl.split(':')
              if (kv.length != 2)
                throw new NomadUriException(s"version fragment $vEl does not have the expected key:value form in $uri")
              Some(kv(0) -> kv(1))
            }
          }(breakOut)

        val pathSeq: Seq[String] = if (pathStr == null || pathStr.isEmpty)
          Seq()
        else
          pathStr.split('/')
        queryMap --= Seq("version")
        objectId match {
          case objectRe(objKindStr, objId) =>
            objKindStr match {
              case "R" =>
                val objectKind = ObjectKind.RawData
                val uriDepth = if (pathSeq.isEmpty) UriDepth.Archive else UriDepth.Path
                if (!versionInfo.isEmpty)
                  throw new NomadUriException(s"version does not make sense for raw data URI $uri")
                NomadUri(
                  objectKind = objectKind,
                  uriDepth = uriDepth,
                  archiveGid = archiveGid,
                  calculationGid = None,
                  indexedPath = pathSeq.map { x: String =>
                    (x -> SortedMap[String, Long]())
                  },
                  versionInfo = Map(),
                  extraQueryArgs = queryMap,
                  fragment = fragment
                )
              case "S" | "N" =>
                val objectKind = if (objKindStr == "S") ObjectKind.ParsedData else ObjectKind.NormalizedData
                if (pathSeq.isEmpty) {
                  NomadUri(
                    objectKind = objectKind,
                    uriDepth = UriDepth.Archive,
                    archiveGid = archiveGid,
                    calculationGid = None,
                    indexedPath = pathSeq.map { x: String =>
                      (x -> SortedMap[String, Long]())
                    },
                    versionInfo = Map(),
                    extraQueryArgs = queryMap,
                    fragment = fragment
                  )
                } else {
                  val calcId: Option[String] = pathSeq.head match {
                    case intRe(nStr) =>
                      throw new NomadUriException(s"Expected a calculation GID (C...) as second path component, not '$nStr' in URI $uri")
                    case calculationGidRe(cId) => Some(cId)
                    case str =>
                      throw new NomadUriException(s"Expected a calculation GID (C...) as second path component, not '$str' in URI $uri")
                  }
                  val path = toIndexedPath(pathSeq.drop(1))
                  if (path.isEmpty) {
                    NomadUri(
                      objectKind = objectKind,
                      uriDepth = UriDepth.Calculation,
                      archiveGid = archiveGid,
                      calculationGid = calcId,
                      indexedPath = Seq(),
                      versionInfo = versionInfo,
                      extraQueryArgs = queryMap,
                      fragment = fragment
                    )
                  } else if (path.foldLeft(0) { case (i, (metaName, idxs)) => i + idxs.size } == 0) {
                    NomadUri(
                      objectKind = objectKind,
                      uriDepth = UriDepth.Table,
                      archiveGid = archiveGid,
                      calculationGid = calcId,
                      indexedPath = path,
                      versionInfo = versionInfo,
                      extraQueryArgs = queryMap,
                      fragment = fragment
                    )
                  } else if (path.foldLeft(true) {
                    case (i, (metaName, idxs)) =>
                      idxs.contains("c") || (i && idxs.contains("l"))
                  }) {
                    NomadUri(
                      objectKind = objectKind,
                      uriDepth = UriDepth.Row,
                      archiveGid = archiveGid,
                      calculationGid = calcId,
                      indexedPath = path,
                      versionInfo = versionInfo,
                      extraQueryArgs = queryMap,
                      fragment = fragment
                    )
                  } else {
                    throw new NomadUriException(s"Invalid meta info path in URI $uri. It should contain only meta info names plus optionally either all local indexes (<nr>l) or the index inside the calculation for the last path segment (<nr>c).")
                  }
                }
              case _ =>
                throw new NomadUriException(s"could not identify objectId '$objectId' either archiveGid(R...), parsedArchiveGid(S..) or normalized archive (N...) in nomad uri $uri")
            }
          case _ =>
            throw new NomadUriException(s"ObjectId '$objectId' does not have the expected form of a letter plush 28 url safe base64 characters with the firts letter either R (archiveGid), S (parsedArchiveGid) or N (normalizedArchiveGid) in nomad uri $uri")
        }
      case _ =>
        throw new NomadUriException(s"Uri '$uri' does not have the form of a nomad uris (nmd://<objectId>...)")
    }
  }

  /**
   * converts an indexed path to a normal path
   */
  def toPathSeq(indexedPath: Seq[(String, SortedMap[String, Long])]): Seq[String] = {
    indexedPath.flatMap {
      case (meta, indexes) =>
        val strIdxs: Seq[String] = indexes.map {
          case (k, idx) =>
            idx.toString + k
        }(breakOut)
        meta +: strIdxs
    }
  }
}

/**
 * represents a parsed nomad uri
 */
case class NomadUri(
    objectKind: ObjectKind.Value,
    uriDepth: UriDepth.Value,
    archiveGid: String,
    calculationGid: Option[String],
    indexedPath: Seq[(String, SortedMap[String, Long])],
    versionInfo: Map[String, String],
    extraQueryArgs: Map[String, String],
    fragment: Option[String]
) {
  import ObjectKind._

  def path: Seq[String] = {
    NomadUri.toPathSeq(indexedPath)
  }

  def toUri(): String = {
    val baseUri = (objectKind match {
      case RawData => s"nmd://${archiveGid}"
      case ParsedData => s"nmd://S${archiveGid.substring(1)}"
      case NormalizedData => s"nmd://N${archiveGid.substring(1)}"
    }) + (calculationGid match {
      case Some(s) => "/" + s
      case None => ""
    })

    val pathStr = (if (indexedPath.isEmpty) "" else "/" + path.mkString("/"))
    val queryEls = mutable.ListBuffer[String]()
    val vString = if (!versionInfo.isEmpty)
      queryEls += "version=" + versionInfo.map { case (k, v) => s"$k:$v" }.mkString(",")
    queryEls ++= extraQueryArgs.map {
      case (k, v) =>
        k + "=" + v
    }
    val query = if (queryEls.isEmpty)
      ""
    else
      "?" + queryEls.mkString("&")
    baseUri + pathStr + query
  }

  def toRef: NomadRef = {
    objectKind match {
      case ObjectKind.RawData =>
        uriDepth match {
          case UriDepth.Archive =>
            ArchiveRef(archiveGid)
          case UriDepth.Path =>
            RawDataRef(archiveGid, path)
          case UriDepth.Calculation | UriDepth.Table | UriDepth.Row =>
            throw new NomadUriException(s"uri depth Path does not make sense for normalized and parsed data in URI $this")
        }
      case ObjectKind.NormalizedData | ObjectKind.ParsedData =>
        uriDepth match {
          case UriDepth.Archive =>
            ArchiveRef(archiveGid)
          case UriDepth.Path =>
            throw new NomadUriException(s"uri depth Path does not make sense for normalized and parsed data in URI $this")
          case UriDepth.Calculation =>
            CalculationRef(archiveGid, calculationGid.get)
          case UriDepth.Table =>
            TableRef(archiveGid, calculationGid.get, path)
          case UriDepth.Row =>
            RowRef(archiveGid, calculationGid.get, indexedPath)
        }
    }
  }
}
