/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi
import org.{ json4s => jn }
import scala.collection.breakOut
import java.io.Writer
import eu.nomad_lab.JsonUtils
import scala.collection.mutable
import JsonApi.mStr
import JsonApi.mDict

object WriterStatus extends Enumeration {
  type WriterStatus = Value
  val ShouldStart, ShouldWriteSinglePrimaryData, ShouldWritePrimaryData, IsWritingPrimaryData, DidWritePrimaryData, IsWritingInclude, DidWriteAll = Value
}

/**
 * Object to simplify the writing out of a json api response
 * Stream like does not cache the value outputted, but caches the values
 * to include, meta and errors.
 *
 * Usage is start, addValue/addValues, finish
 */
class ResultWriter(
    val writer: Writer,
    val loneValue: Boolean = false,
    var meta: Map[String, jn.JValue] = Map(),
    var errors: Seq[Error] = Seq(),
    var links: Map[String, Link] = Map(),
    val addSupportingValues: Boolean = true,
    val uniqueEmit: Boolean = true
) {
  val emittedTypeIds = mutable.Set[(String, String)]()
  val knownTypeIds = mutable.Set[(String, String)]()
  val toInclude = mutable.ListBuffer[BaseValue]()

  var status: WriterStatus.Value = WriterStatus.ShouldStart

  def start(): Unit = {
    status match {
      case WriterStatus.ShouldStart =>
        writer.write("{")
        if (loneValue)
          status = WriterStatus.ShouldWriteSinglePrimaryData
        else
          status = WriterStatus.ShouldWritePrimaryData
      case s =>
        throw new Exception(s"called start when in status $status (expected ShouldStart)")
    }
  }

  def addValue(value: BaseValue): Unit = {
    val idNow = value.iid
    if (uniqueEmit && emittedTypeIds.contains(idNow))
      return ()
    emittedTypeIds += idNow
    status match {
      case WriterStatus.ShouldWritePrimaryData =>
        writer.write("\n  \"data\": [\n    ")
        status = WriterStatus.IsWritingPrimaryData
      case WriterStatus.ShouldWriteSinglePrimaryData =>
        writer.write("\n  \"data\":\n    ")
        status = WriterStatus.DidWritePrimaryData
      case WriterStatus.IsWritingPrimaryData =>
        writer.write(",\n    ")
      case s =>
        throw new Exception(s"addValue called while in status $status")
    }
    value.writeOut(writer, 4)
    if (addSupportingValues) {
      for (valueV <- value.supportingValuesIterator) {
        val idV = valueV.iid
        if (!emittedTypeIds.contains(idV) && !knownTypeIds.contains(idV)) {
          knownTypeIds += idV
          toInclude += valueV
        }
      }
    }
  }

  def addValues(values: Iterator[BaseValue]): Unit = {
    values.foreach(addValue)
  }

  protected def emitInclude(inc: BaseValue): Unit = {
    val idNow = inc.iid
    if (emittedTypeIds.contains(idNow)) {
      knownTypeIds -= idNow
    } else {
      status match {
        case WriterStatus.DidWritePrimaryData =>
          writer.write(",\n  \"include\": [\n    ")
          status = WriterStatus.IsWritingInclude
        case WriterStatus.IsWritingInclude =>
          writer.write(",\n    ")
        case s =>
          throw new Exception(s"addInclude called while in status $status")
      }
      inc.writeOut(writer, 4)
      knownTypeIds -= idNow
      emittedTypeIds += idNow
      if (addSupportingValues) {
        for (s <- inc.supportingValuesIterator) {
          val idS = s.iid
          if (!emittedTypeIds.contains(idS) && !knownTypeIds.contains(idS)) {
            knownTypeIds += idS
            toInclude += s
          }
        }
      }
    }
  }

  protected def emitMeta(meta0: Iterator[(String, jn.JValue)]): Unit = {
    val metaK = mutable.Set[String]()
    for ((k, v) <- (meta0)) {
      if (jn.JNothing != v && !metaK.contains(k)) {
        if (metaK.isEmpty)
          writer.write(",\n  \"meta\": {\n    ")
        else
          writer.write(",\n    ")
        metaK += k
        writer.write(JsonUtils.escapeString(k))
        writer.write(": ")
        JsonUtils.compactWriter(v, writer)
      }
    }
    if (!metaK.isEmpty)
      writer.write("\n  }")
  }

  protected def emitLinks(linksIt: Iterator[(String, Link)]): Unit = {
    val metaK = mutable.Set[String]()
    for ((k, v) <- (linksIt)) {
      if (jn.JNothing != v && !metaK.contains(k)) {
        if (metaK.isEmpty)
          writer.write(",\n  \"links\": {\n    ")
        else
          writer.write(",\n    ")
        metaK += k
        writer.write(JsonUtils.escapeString(k))
        writer.write(": ")
        JsonUtils.compactWriter(v.toJValue, writer)
      }
    }
    if (!metaK.isEmpty)
      writer.write("\n  }")
  }

  def finish(
    included0: Iterator[BaseValue] = Iterator(),
    errors0: Iterator[Error] = Iterator(),
    links0: Iterator[(String, Link)] = Iterator(),
    meta0: Iterator[(String, jn.JValue)] = Iterator()
  ): Unit = {
    status match {
      case WriterStatus.ShouldStart =>
        throw new Exception(s"finish called before start")
      case WriterStatus.ShouldWritePrimaryData | WriterStatus.ShouldWriteSinglePrimaryData =>
        writer.write("\n  \"errors\":[")
        var hasSomeErrors: Boolean = false
        for (err <- (errors0 ++ errors.toIterator)) {
          if (hasSomeErrors)
            writer.write(",")
          else
            hasSomeErrors = true
          writer.write("\n    ")
          JsonUtils.compactWriter(err.toJValue, writer)
        }
        if (!hasSomeErrors) {
          writer.write("\n    ")
          val err = Error(
            title = "No data in response",
            detail = "response with neither data nor errors was generated"
          )
          JsonUtils.compactWriter(err.toJValue, writer)
        }
        writer.write("]")
        emitMeta(meta0 ++ meta.toIterator)
        emitLinks(links0 ++ links.toIterator)
      case WriterStatus.IsWritingPrimaryData | WriterStatus.DidWritePrimaryData =>
        if (status == WriterStatus.IsWritingPrimaryData) {
          writer.write("]")
          status = WriterStatus.DidWritePrimaryData
        }
        // include
        for (inc <- included0) {
          emitInclude(inc)
        }
        while (!toInclude.isEmpty) {
          val inc = toInclude.last
          toInclude.trimEnd(1)
          emitInclude(inc)
        }
        status match {
          case WriterStatus.IsWritingInclude =>
            writer.write("]")
          case WriterStatus.DidWritePrimaryData =>
            ()
          case s =>
            throw new Exception(s"In status $s when about to emit meta")
        }
        // meta
        emitMeta(meta0 ++ meta.toIterator)
        // links
        emitLinks(links0 ++ links.toIterator)
        // errors
        var hasSomeErrors: Boolean = false
        for (err <- (errors0 ++ errors.toIterator)) {
          if (hasSomeErrors) {
            writer.write(",\n    ")
          } else {
            writer.write(",\n  \"errors\":[\n    ")
            hasSomeErrors = true
          }
          JsonUtils.compactWriter(err.toJValue, writer)
        }
        if (hasSomeErrors)
          writer.write("]")
      case WriterStatus.IsWritingInclude =>
        throw new Exception(s"finish called when writing includes")
      case WriterStatus.DidWriteAll =>
        throw new Exception(s"finish called on already finished writer")
    }
    writer.write("\n}\n")
    writer.flush()
  }
}
