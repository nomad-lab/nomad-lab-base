/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi
import org.{ json4s => jn }
import scala.collection.breakOut
import java.io.Writer
import eu.nomad_lab.JsonUtils
import scala.collection.mutable
import JsonApi.mStr
import JsonApi.mDict

/**
 * A json api object (either a member of data or included) with concrete
 *
 *
 * Supporting values are values that should be "included" with this one
 */
case class Value(
    val typeStr: String,
    val idStr: String,
    override val attributes: Map[String, jn.JValue] = Map(),
    override val includedRelationships: Map[String, Relationship[BaseValue]] = Map(),
    override val links: Map[String, Link] = Map(),
    override val meta: Map[String, jn.JValue] = Map()
) extends BaseValue {
}
