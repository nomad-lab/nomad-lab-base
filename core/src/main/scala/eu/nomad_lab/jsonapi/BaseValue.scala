/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi
import java.io.Writer
import org.{ json4s => jn }
import scala.collection.breakOut
import eu.nomad_lab.JsonUtils
import JsonApi.mStr
import JsonApi.mDict

/**
 * Base class for a json api object (either a member of data or included)
 *
 * Supporting values are values that should be "included" with this one
 */
abstract class BaseValue {
  def typeStr: String

  def idStr: String

  def iid: (String, String) = typeStr -> idStr

  def attributes: Map[String, jn.JValue] = Map()

  def includedRelationships: Map[String, Relationship[BaseValue]] = Map()

  def includedRelationshipsRefs: Map[String, Relationship[(String, String)]] = {
    includedRelationships.map {
      case (k, v) => k -> v.map(_.iid)
    }
  }

  def relationships: Map[String, Relationship[(String, String)]] = includedRelationshipsRefs

  def links: Map[String, Link] = Map()

  def meta: Map[String, jn.JValue] = Map()

  def supportingValuesIterator: Iterator[BaseValue] = includedRelationships.values.flatMap { r: Relationship[BaseValue] => r.values }.toIterator

  def toJValue: jn.JValue = {
    val attrib = if (attributes.isEmpty)
      jn.JNothing
    else
      jn.JObject(attributes.toList)
    val rels = if (relationships.isEmpty)
      jn.JNothing
    else
      jn.JObject(relationships.map {
        case (rName, r) =>
          val dataV: jn.JValue = (r match {
            case ToOne(v, _, _) =>
              v match {
                case Some((typeS, idS)) =>
                  jn.JObject(
                    ("type" -> jn.JString(typeS)) ::
                      ("id" -> jn.JString(idS)) :: Nil
                  )
                case None =>
                  jn.JNull
              }
            case ToMany(v, _, _) =>
              jn.JArray(
                v.map {
                  case (typeS, idS) =>
                    jn.JObject(
                      ("type" -> jn.JString(typeS)) ::
                        ("id" -> jn.JString(idS)) :: Nil
                    )
                }(breakOut): List[jn.JValue]
              )
          })
          rName -> jn.JObject(
            ("data" -> dataV) ::
              ("links" -> JsonApi.linksToJValue(r.links)) ::
              ("meta" -> mDict(r.meta)) :: Nil
          )
      }(breakOut): List[(String, jn.JValue)])

    jn.JObject(
      ("type" -> jn.JString(typeStr)) ::
        ("id" -> jn.JString(idStr)) ::
        ("attributes" -> attrib) ::
        ("relationships" -> rels) ::
        ("links" -> JsonApi.linksToJValue(links)) ::
        ("meta" -> mDict(meta)) :: Nil
    )
  }

  def writeOut(writer: Writer, indent: Int): Unit = {
    JsonUtils.compactWriter(toJValue, writer)
  }

}
