/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi
import org.{ json4s => jn }
import scala.collection.breakOut
import java.io.Writer
import eu.nomad_lab.JsonUtils
import scala.collection.mutable

object JsonApi {
  /**
   * A Json string if not empty
   */
  def mStr(s: String): jn.JValue = {
    if (s.isEmpty)
      jn.JNothing
    else
      jn.JString(s)
  }

  /**
   * An json object if the map is not empty
   */
  def mDict(m: Map[String, jn.JValue]): jn.JValue = {
    if (m.filter { case (k, v) => jn.JNothing != v }.isEmpty)
      jn.JNothing
    else
      jn.JObject(m.toList)
  }

  /**
   * Formats an exception as error
   */
  def errorToJsonapiError(e: Throwable, source: Option[ErrorSource] = None): Error = {
    val w = new java.io.StringWriter
    val pw = new java.io.PrintWriter(w)
    e.printStackTrace(pw)
    pw.flush()
    pw.close()
    Error(e.getClass().getName(), e.getMessage() + "\n" + w.toString, source = source)
  }

  /**
   * returns a json apy repsonse containing the given error
   */
  def errorToStr(e: Throwable): String = {
    JsonUtils.prettyStr(
      jn.JObject(
        ("errors" -> jn.JArray(errorToJsonapiError(e).toJValue :: Nil)) :: Nil
      )
    )
  }

  /**
   * Converts links to a JValue
   */
  def linksToJValue(links: Map[String, Link]): jn.JValue = {
    if (links.isEmpty)
      jn.JNothing
    else
      jn.JObject(
        links.map {
          case (k, l) =>
            k -> l.toJValue
        }(breakOut): List[(String, jn.JValue)]
      )
  }

  /**
   * Returns a string containing a jsonapi formatted response
   */
  def responseStr(values: Seq[BaseValue] = Seq(), loneValue: Boolean = false, errors: Seq[Error] = Seq(), meta: Map[String, jn.JValue] = Map(), links: Map[String, Link] = Map()): String = {
    val w = new java.io.StringWriter
    val rw = new ResultWriter(w, loneValue = loneValue, errors = errors, meta = meta, links = links)
    rw.start()
    rw.addValues(values.iterator)
    rw.finish()
    w.flush()
    w.close()
    w.toString()
  }
}
