/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab
import scala.collection.SortedSet
import scala.collection.breakOut

object AtomData {
  /**
   * list of atom symbols ordered by atom number
   */
  val chemicalSymbols = Seq("X", "H", "He", "Li", "Be",
    "B", "C", "N", "O", "F",
    "Ne", "Na", "Mg", "Al", "Si",
    "P", "S", "Cl", "Ar", "K",
    "Ca", "Sc", "Ti", "V", "Cr",
    "Mn", "Fe", "Co", "Ni", "Cu",
    "Zn", "Ga", "Ge", "As", "Se",
    "Br", "Kr", "Rb", "Sr", "Y",
    "Zr", "Nb", "Mo", "Tc", "Ru",
    "Rh", "Pd", "Ag", "Cd", "In",
    "Sn", "Sb", "Te", "I", "Xe",
    "Cs", "Ba", "La", "Ce", "Pr",
    "Nd", "Pm", "Sm", "Eu", "Gd",
    "Tb", "Dy", "Ho", "Er", "Tm",
    "Yb", "Lu", "Hf", "Ta", "W",
    "Re", "Os", "Ir", "Pt", "Au",
    "Hg", "Tl", "Pb", "Bi", "Po",
    "At", "Rn", "Fr", "Ra", "Ac",
    "Th", "Pa", "U", "Np", "Pu",
    "Am", "Cm", "Bk", "Cf", "Es",
    "Fm", "Md", "No", "Lr")

  /**
   * mapping chemical symbol -> atom number
   */
  val atomNumber: Map[String, Int] = chemicalSymbols.zipWithIndex.toMap

  /**
   * returns the atom number for the given label, or 0 if no chemical atom could be identified
   */
  def atomNrFromLabel(label: String): Int = {
    val baseStr = label.take(3).capitalize
    atomNumber.get(baseStr) match {
      case Some(n) => n
      case None =>
        atomNumber.get(baseStr.take(2)) match {
          case Some(n) => n
          case None =>
            atomNumber.get(baseStr.take(1)) match {
              case Some(n) => n
              case None => 0
            }
        }
    }
  }

  /**
   * mapping chemical symbol -> atom number, preserving undefined species differences
   */
  def atomNrsFromLabels(labels: Seq[String], strict: Boolean = true): Seq[Int] = {
    val atomNrs = if (strict)
      labels.map { x: String => atomNumber.getOrElse(x, 0) }
    else
      labels.map(atomNrFromLabel)
    val undefLabels: SortedSet[String] = labels.zip(atomNrs).flatMap {
      case (label: String, atNr: Int) =>
        if (atNr == 0)
          Some(label)
        else
          None
    }(breakOut)
    val undefIdx = undefLabels.zipWithIndex.toMap
    labels.zip(atomNrs).map {
      case (label, atNr) =>
        if (atNr == 0)
          -undefIdx(label)
        else
          atNr
    }
  }

  /**
   * returns normalized label names from atom numbers.
   *
   * Ensures that the order for undefined species is preserved:
   * the label of more negative numbers comes later in alphabetic ordering.
   */
  def labelForAtomNr(atomNr: Int): String = {
    if (atomNr > 0) {
      if (atomNr > chemicalSymbols.length)
        throw new Exception(s"Invalid atom number $atomNr in normalizedLabel")
      chemicalSymbols(atomNr)
    } else {
      val alpha = "abcdfghijklmnopqrstuvwxy" // missing 'e' as Xe is a valid chemical symbol, and 'z' to encode larger numbers
      var s: String = ""
      var ii: Int = -atomNr
      while (ii > 0) {
        val r = ii % alpha.length
        s += alpha(r)
        ii = ii / alpha.length
      }
      if (s.length > 1) {
        // z, nr of digits, number starting with largest exponent first (big endian ;)
        s += alpha(s.length) // limits number to 24**24-1 which is (much) bigger than 2**31 :)
        s += "z"
        s = s.reverse
      }
      "X" + s
    }
  }

}
