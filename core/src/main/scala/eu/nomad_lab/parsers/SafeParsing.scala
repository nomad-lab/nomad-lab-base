/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import com.typesafe.scalalogging.StrictLogging
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import eu.{ nomad_lab => lab }
import eu.nomad_lab.meta
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import scala.util.control.NonFatal
import java.nio.file.Path

/**
 * methods the parse in a way that if an exception happens parsing still ends consistently
 */
object SafeParsing extends StrictLogging {
  object BackendStatus extends Enumeration {
    type BackendStatus = Value
    val Neutral, ExpectStart, DidStart, DidFinish = Value
  }

  class GuaranteedEndException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  abstract class GuaranteedEndBaseBackend extends BaseParserBackend.ForwardBackendBase {
    protected var status: BackendStatus.Value = BackendStatus.Neutral
    var mainFileUri: Option[String] = None
    var parserInfo: JValue = JNothing
    var parseResult: Option[ParseResult.Value] = None
    protected var parserErrors: Seq[String] = Seq()
    var parsingStats: Map[String, Long] = Map()

    def addError(err: String): Unit = {
      parserErrors = parserErrors :+ err
    }

    override def backendInfo: org.json4s.JValue = {
      JObject(
        ("backendType" -> JString("GuaranteedEndBackend")) ::
          ("mainFileUri" -> (mainFileUri match {
            case None => JNothing
            case Some(v) => JString(v)
          })) ::
          ("superBackend" -> superBackend.backendInfo) :: Nil
      )
    }

    def willStartParsing(
      mainFileUri: String,
      parserInfo: JValue
    ): Unit = {
      this.parserErrors = Seq()
      this.mainFileUri = Some(mainFileUri)
      this.parserInfo = parserInfo
      this.parsingStats = Map()
      status match {
        case BackendStatus.Neutral =>
          status = BackendStatus.ExpectStart
        case BackendStatus.ExpectStart =>
          throw new GuaranteedEndException(s"unexpected double call to willStartParsingSession")
        case BackendStatus.DidStart =>
          throw new GuaranteedEndException(s"unexpected call to willStartParsingSession while parsing")
        case BackendStatus.DidFinish =>
          throw new GuaranteedEndException(s"unexpected call to willStartParsingSession in DidFinish status")
      }
    }

    def didFinishParsing(): ParseResult.Value = {
      status match {
        case BackendStatus.Neutral =>
          throw new GuaranteedEndException(s"unexpected double call to finishedParsingSession in neutral state")
        case BackendStatus.ExpectStart =>
          try {
            superBackend.startedParsingSession(mainFileUri, parserInfo)
          } catch {
            case NonFatal(e) =>
              //logger.warn(s"Exception executing skipped startedParsingSession for $mainFileUri", e)
              addError(s"Exception $e executing skipped startedParsingSession for $mainFileUri")
          }
          parseResult = Some(ParseResult.ParseSkipped)
          try {
            superBackend.finishedParsingSession(parseResult, parserErrors = JArray(parserErrors.map(JString).toList), parsingStats = parsingStats)
          } catch {
            case NonFatal(e) =>
              logger.warn(s"Exception in skipped finishedParsingSession parsing $mainFileUri", e)
          }
          logger.warn(s"GuaranteedEnd recovered skipped parsing of $mainFileUri")
        case BackendStatus.DidStart =>
          logger.warn(s"GuaranteedEnd missing end of $mainFileUri")
          parseResult = Some(ParseResult.ParseFailure)
          try {
            superBackend.finishedParsingSession(parseResult, parserErrors = JArray(parserErrors.map(JString).toList), parsingStats = parsingStats)
          } catch {
            case NonFatal(e) =>
              logger.warn(s"Exception in finishedParsingSession parsing $mainFileUri", e)
          }
          parserErrors = Seq()
        case BackendStatus.DidFinish => ()
      }
      if (!parserErrors.isEmpty)
        logger.error(s"Error parsing $mainFileUri, ignoring leftover errors $parserErrors")
      mainFileUri = None
      parserInfo = JNothing
      parserErrors = Seq()
      this.parserErrors = Seq()
      val res = parseResult.get
      parseResult = None
      status = BackendStatus.Neutral
      res
    }

    /**
     * Started a parsing session
     */
    override def startedParsingSession(
      mainFileUri: Option[String],
      parserInfo: JValue,
      parserStatus: Option[ParseResult.Value] = None,
      parserErrors: JValue = JNothing
    ): Unit = {
      parseResult match {
        case None =>
          parseResult = parserStatus
        case Some(_) => ()
      }
      status match {
        case BackendStatus.Neutral =>
          throw new GuaranteedEndException(s"unexpected startParsing when in neutral state")
        case BackendStatus.ExpectStart =>
          status = BackendStatus.DidStart
        case BackendStatus.DidStart =>
          throw new GuaranteedEndException(s"unexpected double startParsing")
        case BackendStatus.DidFinish =>
          throw new GuaranteedEndException(s"unexpected startParsing in DidFinish")
      }
      superBackend.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
    }

    /**
     * finished a parsing session
     */
    override def finishedParsingSession(
      parserStatus: Option[ParseResult.Value],
      parserErrors: JValue = JNothing,
      mainFileUri: Option[String] = None,
      parserInfo: JValue = JNothing,
      parsingStats: Map[String, Long] = Map()
    ): Unit = {
      parseResult match {
        case None =>
          parseResult = parserStatus
        case Some(_) => ()
      }
      status match {
        case BackendStatus.Neutral =>
          throw new GuaranteedEndException(s"unexpected finishedParsingSession when in neutral state")
        case BackendStatus.ExpectStart =>
          throw new GuaranteedEndException(s"finishedParsingSession without start")
        case BackendStatus.DidStart =>
          status = BackendStatus.DidFinish
        case BackendStatus.DidFinish =>
          throw new GuaranteedEndException(s"double finishedParsingSession")
      }
      superBackend.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
    }
  }

  /**
   * Callbacks that are called by an internal streaming parser
   *
   * This kind of backend is in control of the GIndexes of the sections
   * and chooses them
   * The ReindexBackend can adapt a backend wanting to control gIndex
   * with one that wants to set indexes
   */
  final class GuaranteedEndInternalBackend(val superBackend: ParserBackendInternal) extends GuaranteedEndBaseBackend with BaseParserBackend.ForwardBackendInternal

  /**
   * Callbacks that are called by an external streaming parser
   *
   * Here index generation is controlled externally, this is good for external
   * parsers as they become effectively decoupled, and can generate the whole
   * stream of events without waiting for any answer (no latency or roundtrip)
   * A GenIndexBackend (to do), can adapt and external backend to an internal one
   */
  final class GuaranteedEndExternalBackend(
    val superBackend: ParserBackendExternal
  ) extends GuaranteedEndBaseBackend with BaseParserBackend.ForwardBackendExternal

  /**
   * parses the file at the given path, calling the given internal backend with the parser events
   *
   * parserName is used to identify the parser, mainly for logging/debugging
   */
  def parseInternal(parser: OptimizedParser, mainFileUri: String, mainFilePath: Path, backend: ParserBackendInternal, parserName: String): ParseResult.ParseResult = {
    val guaranteeParsing = new GuaranteedEndInternalBackend(backend)
    guaranteeParsing.willStartParsing(mainFileUri, parser.parserGenerator.parserInfo)
    try {
      parser.parseInternal(mainFileUri, mainFilePath, guaranteeParsing, parserName)
    } catch {
      case NonFatal(e) =>
        guaranteeParsing.addError(e.toString)
    }

    guaranteeParsing.didFinishParsing()
  }

  /**
   * parses the file at the given path, calling the external backend with the parser events
   *
   * parserName is used to identify the parser, mainly for logging/debugging
   */
  def parseExternal(parser: OptimizedParser, mainFileUri: String, mainFilePath: Path, backend: ParserBackendExternal, parserName: String): ParseResult.ParseResult = {
    val guaranteeParsing = new GuaranteedEndExternalBackend(backend)
    guaranteeParsing.willStartParsing(mainFileUri, parser.parserGenerator.parserInfo)
    try {
      val parseStatus = parser.parseExternal(mainFileUri, mainFilePath, guaranteeParsing, parserName)
    } catch {
      case NonFatal(e) =>
        guaranteeParsing.addError(s"Had exception $e parsing $mainFileUri")
        logger.warn(s"Exception while parsing $mainFileUri", e)
    }
    guaranteeParsing.didFinishParsing()
  }

  /**
   * parses the file at the given path, calling the given backend with the parser events
   *
   * parserName is used to identify the parser, mainly for logging/debugging
   */
  def parse(parser: OptimizedParser, mainFileUri: String, mainFilePath: Path, backend: ParserBackendBase, parserName: String): ParseResult.ParseResult = {
    backend match {
      case b: ParserBackendInternal =>
        parseInternal(parser, mainFileUri, mainFilePath, b, parserName)
      case b: ParserBackendExternal =>
        parseExternal(parser, mainFileUri, mainFilePath, b, parserName)
    }
  }
}
