/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import ucar.ma2.{ Array => NArray }
import ucar.ma2.{ IndexIterator => NIndexIterator }
import ucar.ma2.DataType
import ucar.ma2.ArrayString
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import java.io.Writer
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.meta

/**
 * contains helper traits to define new backends
 */
object BaseParserBackend {

  /**
   * trait implementing a backend that just forwards to its super backend
   *
   * You need to override superBackend
   */
  trait ForwardBackendBase extends ParserBackendBase with StrictLogging {
    def superBackend: ParserBackendBase

    def metaInfoEnv: meta.MetaInfoEnv = superBackend.metaInfoEnv

    def backendInfo: org.json4s.JValue = {
      JObject(
        ("backendType" -> JString(getClass().getName())) ::
          ("superBackend" -> superBackend.backendInfo) :: Nil
      )
    }

    def cleanup(): Unit = {
      superBackend.cleanup
    }

    /**
     * Started a parsing session
     */
    override def startedParsingSession(
      mainFileUri: Option[String],
      parserInfo: JValue,
      parserStatus: Option[ParseResult.Value] = None,
      parserErrors: JValue = JNothing
    ): Unit = {
      superBackend.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
    }

    /**
     * finished a parsing session
     */
    override def finishedParsingSession(
      parserStatus: Option[ParseResult.Value],
      parserErrors: JValue = JNothing,
      mainFileUri: Option[String] = None,
      parserInfo: JValue = JNothing,
      parsingStats: Map[String, Long] = Map()
    ): Unit = {
      superBackend.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
    }

    /**
     * returns the sections that are still open
     *
     * sections are identified by metaName and their gIndex
     */
    override def openSections(): Iterator[(String, Long)] = superBackend.openSections

    /**
     * returns information on an open section (for debugging purposes)
     */
    override def sectionInfo(metaName: String, gIndex: Long): String = superBackend.sectionInfo(metaName, gIndex)

    /**
     * sets info values of an open section.
     *
     * references should be references to gIndex of the root sections this section refers to.
     */
    override def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {
      superBackend.setSectionInfo(metaName, gIndex, references)
    }

    /**
     * closes a section
     *
     * after this no other value can be added to the section.
     * metaName is the name of the meta info, gIndex the index of the section
     */
    override def closeSection(metaName: String, gIndex: Long): Unit = {
      superBackend.closeSection(metaName, gIndex)
    }

    /**
     * Adds a json value corresponding to metaName.
     *
     * The value is added to the section the meta info metaName is in.
     * A gIndex of -1 means the latest section.
     */
    override def addValue(metaName: String, value: JValue, gIndex: Long = -1): Unit = {
      superBackend.addValue(metaName, value, gIndex)
    }

    /**
     * Adds a floating point value corresponding to metaName.
     *
     * The value is added to the section the meta info metaName is in.
     * A gIndex of -1 means the latest section.
     */
    override def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {
      superBackend.addRealValue(metaName, value, gIndex)
    }

    /**
     * Adds a new array of the given size corresponding to metaName.
     *
     * The value is added to the section the meta info metaName is in.
     * A gIndex of -1 means the latest section.
     * The array is unitialized.
     */
    override def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {
      superBackend.addArray(metaName, shape, gIndex)
    }

    /**
     * Adds values to the last array added
     */
    override def setArrayValues(
      metaName: String,
      values: NArray,
      offset: Option[Seq[Long]] = None,
      gIndex: Long = -1
    ): Unit = {
      superBackend.setArrayValues(metaName, values, offset, gIndex)
    }

    /**
     * Adds an array value with the given array values
     */
    override def addArrayValues(metaName: String, values: NArray, gIndex: Long = -1): Unit = {
      superBackend.addArrayValues(metaName, values, gIndex)
    }
  }

  /**
   * trait implementing an internal backend that just forwards to its super backend
   *
   * You need to override superBackend
   */
  trait ForwardBackendInternal extends ForwardBackendBase with ParserBackendInternal {
    def superBackend: ParserBackendInternal

    /**
     * opens a new section, returning a valid gIndex
     */
    override def openSection(metaName: String): Long = superBackend.openSection(metaName)
  }

  /**
   * trait implementing an external backend that just forwards to its super backend
   *
   * You need to override superBackend
   */
  trait ForwardBackendExternal extends ForwardBackendBase with ParserBackendExternal {
    def superBackend: ParserBackendExternal

    /**
     * Informs tha backend that a section with the given gIndex should be opened
     *
     * The index is assumed to be unused, it is an error to reopen an existing section.
     */
    def openSectionWithGIndex(metaName: String, gIndex: Long): Unit = {
      superBackend.openSectionWithGIndex(metaName, gIndex)
    }
  }
}

/**
 * Base Parser Backend
 *
 * a backend that keeps track of the open sections, useful as basis
 * for other backends.
 */
abstract class BaseParserBackend(
    val metaInfoEnv: MetaInfoEnv
) extends ParserBackendBase {
  val _openSections = mutable.Set[(String, Long)]()

  /**
   * returns the sections that are still open
   *
   * sections are identified by metaName and their gIndex
   */
  def openSections(): Iterator[(String, Long)] = _openSections.iterator

  /**
   * returns information on an open section (for debugging purposes)
   */
  def sectionInfo(metaName: String, gIndex: Long): String = {
    if (_openSections.contains(metaName -> gIndex))
      s"section $metaName, gIndex $gIndex is open"
    else
      s"section $metaName, gIndex $gIndex is closed"
  }

  /**
   * closes a section
   *
   * after this no other value can be added to the section.
   * metaName is the name of the meta info, gIndex the index of the section
   */
  def closeSection(metaName: String, gIndex: Long): Unit = {
    _openSections -= (metaName -> gIndex)
  }

  /**
   * Informs tha backend that a section with the given gIndex should be opened
   *
   * The index is assumed to be unused, it is an error to reopen an existing section.
   */
  def openSectionWithGIndex(metaName: String, gIndex: Long): Unit = {
    _openSections += (metaName -> gIndex)
  }

  def backendInfo: JValue = {
    JObject(
      ("backendType" -> JString(getClass().getName())) :: Nil
    )
  }

  def cleanup(): Unit = {}

}
