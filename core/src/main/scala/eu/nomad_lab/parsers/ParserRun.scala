/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import java.nio.file.Paths
import eu.nomad_lab.parsers
import scala.collection._
import scala.collection.mutable.ListBuffer
import com.typesafe.scalalogging.StrictLogging

object ParserRun extends StrictLogging {

  /**
   * Parse the given file path with the given parser and options
   * @param parserGen: Parser generator to create the optimized parser to use.
   * @param pPath: Path of the file to Parse
   * @param opt: Backend type of the parsed file
   */
  def parse(parserGen: SimpleExternalParserGenerator, pPath: String, opt: String): ParseResult.Value =
    {
      object BackendType extends Enumeration {
        type Enum = Value
        val JsonWriter, JsonEventEmitter, Netcdf = Value
      }
      val p = Paths.get(pPath)
      val path = (if (!p.toFile().exists()) {
        //val basePath = Paths.get((new java.io.File(".")).getCanonicalPath())
        //val relPath = p.subpath(2, p.getNameCount()).toString()
        //println(s"XXX $basePath + $relPath")
        //basePath.resolve(relPath)
        p.subpath(2, p.getNameCount())
      } else
        p).toAbsolutePath()
      val uri = p.toUri.toString
      val tmpFile = java.io.File.createTempFile("parserTest", ".log")
      val outF = new java.io.FileWriter(tmpFile, true)
      val parser = parserGen.optimizedParser(Seq())
      var backendType: BackendType.Enum = BackendType.JsonEventEmitter
      opt match {
        case "json" =>
          backendType = BackendType.JsonWriter
        case "hdf5" | "netcdf" =>
          backendType = BackendType.Netcdf
        case _ =>
          backendType = BackendType.JsonEventEmitter
      }
      val extBackend: parsers.ParserBackendExternal = backendType match {
        case BackendType.JsonWriter =>
          new parsers.JsonWriterBackend(parser.parseableMetaInfo, outF)
        case BackendType.Netcdf => null
        case BackendType.JsonEventEmitter =>
          new parsers.JsonParseEventsWriterBackend(parser.parseableMetaInfo, outF)
      }
      val res = parser.parseExternal(uri, path, extBackend, parserGen.name)
      outF.close()
      if (res == ParseResult.ParseSuccess) {
        tmpFile.delete()
        parser.cleanup()
      } else {
        logger.warn(s"parsing failure, leaving parsing output in $tmpFile and avoiding parser cleanup")
      }
      res
    }
}
