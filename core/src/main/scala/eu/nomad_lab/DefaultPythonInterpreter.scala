/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab;
import org.{ json4s => jn }
import com.typesafe.scalalogging.StrictLogging
import com.typesafe.config
import scala.collection.JavaConversions._
import scala.collection.breakOut

object DefaultPythonInterpreter extends StrictLogging {
  /**
   * The settings required to connect to the local relational DB
   */
  class Settings(conf: config.Config) {
    // validate vs. reference.conf
    conf.checkValid(config.ConfigFactory.defaultReference(), "simple-lib")

    val pythonExe: String = conf.getString("nomad_lab.python.pythonExe")
    val commonFiles: Seq[String] = conf.getStringList("nomad_lab.python.commonFiles").toSeq
    val commonDirMapping: Map[String, String] = conf.getObject("nomad_lab.python.commonDirMapping").unwrapped().map {
      case (k, v) =>
        k -> (v match { case value: String => value })
    }(breakOut)

    def toJson: jn.JValue = {
      import org.json4s.JsonDSL._
      ("pythonExe" -> jn.JString(pythonExe)) ~
        ("commonFiles" -> commonFiles) ~
        ("commonDirMapping" -> commonDirMapping)
    }
  }

  private var privateDefaultSettings: Settings = null

  /**
   * default settings, should be initialized on startup
   */
  def defaultSettings(): Settings = {
    DefaultPythonInterpreter.synchronized {
      if (privateDefaultSettings == null) {
        privateDefaultSettings = new Settings(LocalEnv.defaultConfig)
      }
      privateDefaultSettings
    }
  }

  def pythonExe(conf: Settings = null): String = {
    if (conf == null)
      defaultSettings().pythonExe
    else
      conf.pythonExe
  }

  def commonFiles(conf: Settings = null): Seq[String] = {
    if (conf == null)
      defaultSettings().commonFiles
    else
      conf.commonFiles
  }

  def commonDirMapping(conf: Settings = null): Map[String, String] = {
    if (conf == null)
      defaultSettings().commonDirMapping
    else
      conf.commonDirMapping
  }
}
