/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab
import scala.util.control.NonFatal
import scala.util.matching.Regex

/**
 * Semantic version (http://semver.org) parsing
 */
object SemVer {
  def tryParse(vString: String): Option[SemVer] = {
    try {
      Some(apply(vString))
    } catch {
      case NonFatal(e) =>
        None
    }
  }

  def apply(vString: String): SemVer = {
    val semanticVerRe = """^([0-9]+)(?:\.([0-9]+)(?:\.([0-9]+))?)?(?:-([-a-zA-Z0-9]+(?:\.[-a-zA-Z0-9]+)*))?(?:\+([-a-zA-Z0-9]+(?:\.[-a-zA-Z0-9]+)*))?$""".r
    vString match {
      case semanticVerRe(major, minor, patch, label, build) =>
        SemVer(
          major.toInt,
          if (null == minor) 0
          else minor.toInt,
          if (null == patch) 0
          else patch.toInt,
          if (null == label) None
          else Some(label),
          if (null == build) None
          else Some(build)
        )
      case _ =>
        throw new Exception(s"Invalid sematic version '$vString'")
    }
  }
}

/**
 * class that represents a sematic version according to the semantic versioning
 * which can be found on http://semver.org
 */
case class SemVer(
    major: Int,
    minor: Int = 0,
    patch: Int = 0,
    label: Option[String] = None,
    build: Option[String] = None
) extends Ordered[SemVer] {
  override def compare(that: SemVer): Int = {
    if (major < that.major)
      return -1
    if (major > that.major)
      return 1
    if (minor < that.minor)
      return -1
    if (minor > that.minor)
      return 1
    if (patch < that.patch)
      return -1
    if (patch > that.patch)
      return 1
    label match {
      case Some(l1) =>
        that.label match {
          case None => return -1
          case Some(l2) =>
            val v1 = l1.split('.')
            val v2 = l2.split('.')
            var ii: Int = 0
            val nrRe = "^([0-9]+)$".r
            val minL = v1.length.min(v2.length)
            while (ii < minL) {
              val ll1 = v1(ii)
              val ll2 = v2(ii)
              ll1 match {
                case nrRe(n1) =>
                  ll2 match {
                    case nrRe(n2) =>
                      val c = n1.toInt.compare(n2.toInt)
                      if (c != 0) return c
                    case _ =>
                      return -1
                  }
                case _ =>
                  ll2 match {
                    case nrRe(n2) => return 1
                    case _ =>
                      if (ll1 < ll2) return -1
                      if (ll1 > ll2) return 1
                  }
              }
              ii += 1
            }
            if (v1.length < v2.length) return -1
            if (v1.length > v2.length) return 1
        }
      case None =>
        that.label match {
          case Some(l1) => return 1
          case None =>
        }
    }
    // order by build to make it a total ordering (this should not be done according to the spec, but is accepted)
    // equiv consider them equal in the comparison
    build match {
      case Some(b1) =>
        that.build match {
          case Some(b2) =>
            b1.compare(b2)
          case None =>
            -1
        }
      case None =>
        that.build match {
          case Some(b2) =>
            1
          case None =>
            0
        }
    }
  }

  /**
   * Equivalence that ignores the build part
   */
  def equiv(that: SemVer): Boolean = {
    this.copy(build = None) == that.copy(build = None)
  }

  /**
   * formats the string according to the semantic versioning standard
   */
  override def toString: String = {
    var res = s"$major.$minor.$patch"
    label match {
      case Some(l) =>
        res += s"-$l"
      case None => ()
    }
    build match {
      case Some(b) =>
        res += s"+$b"
      case None => ()
    }
    return res
  }

  /**
   * The basic version (no build part)
   */
  def basic: SemVer = SemVer(major, minor, patch, label)

  /**
   * If this version can be considered stable
   */
  def stable: Boolean = {
    major != 0 && label.isEmpty
  }
}
