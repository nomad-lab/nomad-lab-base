/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

case class ChildObjectInfoH5(
  oNames: String,
  oTypes: Int,
  lTypes: Int,
  oRefs: Long
)

trait IteratorH5[T] extends Iterable[T] {

  protected def _group: Long
  //Correct initialization of childNames, childTypes depends upon count.
  private var _count = -1
  protected def count: Int = {
    if (_count == -1) {
      if (_group >= 0) _count = H5Lib.groupMemberCount(_group)
      else _count = 0
    }
    _count
  }
  protected lazy val childNames: Array[String] = new Array[String](count)
  protected lazy val childTypes: Array[Int] = new Array[Int](count)
  protected lazy val lTypes: Array[Int] = new Array[Int](count)
  protected lazy val childRefs: Array[Long] = new Array[Long](count)

  // Keeping them in the trait can be problematic. Since the childGroups depends upon the initialization of the childNames, childTypes etc,
  // it is important to consistently determine when that initialization happens
  // lazy val should solve the problem. See: http://stackoverflow.com/questions/19642053/when-to-use-val-or-def-in-scala-traits
  protected lazy val childGroups: Array[ChildObjectInfoH5] = {
    if (_group >= 0) {
      H5Lib.groupMembers(_group, childNames, childTypes, lTypes, childRefs)
      (childNames zip childTypes zip lTypes zip childRefs) filter { case (((a, b), c), d) => b == H5Lib.groupObjectType } map { case (((a, b), c), d) => ChildObjectInfoH5(a, b, c, d) }
    } else
      Array()
  }
}
