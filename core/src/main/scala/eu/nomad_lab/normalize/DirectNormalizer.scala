/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.normalize

import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.query.QueryScanOp
import eu.nomad_lab.query.Filter
import eu.nomad_lab.parsers.BackendH5
import eu.nomad_lab.parsers.ParserBackendInternal
import eu.nomad_lab.meta
import org.{ json4s => jn }

class DirectNormalizer(
    generator: NormalizerGenerator,
    backend: ParserBackendInternal
) extends QueryScanOp {
  def metaInfoEnv: meta.MetaInfoEnv = generator.metaInfoEnv

  def name: String = generator.name

  def context: String = generator.context

  def filter: Filter = generator.filter

  def workOnContext(context: ResolvedRef): Unit = {
    val n = generator.normalizer()
    n.normalizeInternal(context, backend)
    n.cleanup()
  }

  def cleanup(): Unit = {
    generator.cleanup()
  }

  def workerJValue: jn.JValue = {
    jn.JObject(
      ("type" -> jn.JString("normalizer")) ::
        ("name" -> jn.JString(name)) :: Nil
    )
  }
}
