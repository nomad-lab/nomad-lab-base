/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.normalize

import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.h5.SectionH5
import eu.nomad_lab.meta
import eu.nomad_lab.normalize.SectionSystemNormalizer.name
import eu.nomad_lab.parsers.{ BackendH5, ParserBackendInternal }
import eu.nomad_lab.query.{ CompiledQuery, Filter, QueryExpression }
import eu.nomad_lab.ref.NomadUri
import eu.nomad_lab.resolve.{ FailedResolution, ParsedRef, ResolvedRef, Section }
import org.{ json4s => jn }
import eu.{ nomad_lab => lab }
import ucar.ma2
import ucar.ma2.ArrayLong

import scala.collection.{ SortedMap, breakOut }

object DosValuesByAtomNormalizer extends NormalizerGenerator {

  val name: String = "DosValuesByAtomNormalizer"
  val context: String = "section_dos"
  /**
   * description of the data that can be generated by this normalizer
   */
  override val metaInfoEnv: meta.MetaInfoEnv = meta.KnownMetaInfoEnvs.all
  //  override val filter: Filter = CompiledQuery(QueryExpression("not system_configuration_consistent"), metaInfoEnv)

  override def normalizer(): Normalizer = {
    new DosValuesByAtomNormalize
  }

  /**
   * normalizer information: name, description,...
   */
  def info: jn.JObject = {
    jn.JObject(
      ("name" -> jn.JString(name)) ::
        ("parserId" -> jn.JString(name + lab.NomadCoreVersionInfo.version)) ::
        ("versionInfo" -> jn.JObject(
          ("nomadCoreVersion" -> jn.JObject(lab.NomadCoreVersionInfo.toMap.map {
            case (k, v) => k -> jn.JString(v.toString)
          }(breakOut): List[(String, jn.JString)]))
        )) :: Nil
    )
  }
}

class DosValuesByAtomNormalize() extends Normalizer with StrictLogging {

  var context: ResolvedRef = FailedResolution("", "")
  //Define CalckBackend
  var calcBackend: BackendH5 = null
  // Define archiveInfo
  var archiveInfo: SortedMap[String, Long] = SortedMap[String, Long]()

  def normalizeDOS(section: SectionH5, backend: ParserBackendInternal): Unit = {

    val singleConf: Option[SectionH5] = section.parentSection
    val refSingleConf: Option[Long] = singleConf match {
      case Some(s) =>
        s.maybeValue("single_configuration_calculation_to_system_ref").map(_.longValue)
      case None =>
        None
    }
    val sectionSys: Option[SectionH5] = refSingleConf match {
      case Some(sysIndex) =>
        singleConf match {
          case Some(s) => s.parentSection.map { runSection =>
            runSection.table.subSectionTable("section_system")(sysIndex)
          }
          case None => None
        }
      case None => None
    }

    val objectKind = context match {
      case pRef: ParsedRef => pRef.objectKind
      case _ => throw new Exception(s"incorrect context $context")
    }
    //Open the context

    backend.openContext(NomadUri(section.toRef.toUriStr(objectKind)))
    try {
      // Getting values
      val number_of_atoms: Option[Long] = sectionSys match {
        case None => None
        case Some(sec) => sec.maybeValue("atom_positions").map(_.arrayShape(0))
      }

      val volume: Option[Float] = sectionSys match {
        case None => None
        case Some(sec) => {
          sec.maybeValue("simulation_cell").map(_.arrayValue()) match {
            case Some(matrix) => {
              if (matrix.getShape().toList != List(3, 3))
                None
              else {
                val detPos =
                  matrix.getFloat(0) * matrix.getFloat(4) * matrix.getFloat(8) +
                    matrix.getFloat(3) * matrix.getFloat(2) * matrix.getFloat(7) +
                    matrix.getFloat(6) * matrix.getFloat(1) * matrix.getFloat(5)
                val detNeg =
                  matrix.getFloat(0) * matrix.getFloat(5) * matrix.getFloat(7) +
                    matrix.getFloat(3) * matrix.getFloat(1) * matrix.getFloat(8) +
                    matrix.getFloat(6) * matrix.getFloat(2) * matrix.getFloat(4)
                val det = scala.math.abs(detPos - detNeg)
                det match {
                  case x if x > 0 => Some(det.toFloat)
                  case _ => None
                }
              }
            }
            case None => None
          }
        }
      }
      //
      val dosValuesByAtom: Option[ma2.Array] = section.maybeValue("dos_values").map(_.arrayValue())

      // Addining dosValuesAvgByVolume
      (dosValuesByAtom, number_of_atoms) match {
        case (Some(vector), Some(number)) =>
          for (i <- 0 until vector.getSize.toInt) yield {
            val oldValue = vector.getDouble(i)
            vector.setDouble(i, oldValue / number)
          }
          backend.addArrayValues("dos_values_per_atoms", vector)
        case _ => None
      }

      // Addining dosValuesAvgByVolume
      (dosValuesByAtom, volume) match {
        case (Some(vector), Some(volume)) =>

          for (i <- 0 until vector.getSize.toInt) yield {
            val oldValue = vector.getDouble(i)
            vector.setDouble(i, oldValue / volume)
          }

          backend.addArrayValues("dos_values_per_unit_volume", vector)
        case _ => None
      }

    } finally {
      backend.closeContext(NomadUri(section.toRef.toUriStr(objectKind)))
    }
  }

  def generator: NormalizerGenerator = DosValuesByAtomNormalizer

  def normalizeInternal(context: ResolvedRef, backend: ParserBackendInternal): Unit = {
    this.context = context
    context match {
      case Section(archiveSet, sectionH5) =>
        normalizeDOS(sectionH5, backend)
      case _ =>
        throw new Exception(s"context should be a section_system, not $context")
    }
  }

  def cleanup(): Unit = {}
}