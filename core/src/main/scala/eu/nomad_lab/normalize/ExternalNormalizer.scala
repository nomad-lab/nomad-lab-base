/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.normalize

import eu.nomad_lab.meta
import org.{ json4s => jn }
import eu.{ nomad_lab => lab }
import eu.nomad_lab.query
import eu.nomad_lab.resolve._
import eu.nomad_lab.parsers.ParserBackendInternal
import eu.nomad_lab.parsers.ParserBackendExternal
import eu.nomad_lab.parsers.SimpleExternalParserGenerator
import eu.nomad_lab.parsers.ExternalParserWrapper
import eu.nomad_lab.parsers.ReindexBackend
import java.nio.file.Path
import java.nio.file.Files
import com.typesafe.scalalogging.StrictLogging
import scala.util.control.NonFatal
import eu.nomad_lab.normalize.Normalizer.trace

class ExternalNormalizerException(
  msg: String
) extends Exception(msg)

class ExternalNormalizerGenerator(
    val name: String,
    val info: jn.JObject,
    val context: String,
    val cmd: Seq[String],
    override val filter: query.Filter = query.YesFilter,
    val cmdCwd: String = "${tmpDir}",
    val cmdEnv: Map[String, String] = Map(),
    val resList: Seq[String] = Seq(),
    val dirMap: Map[String, String] = Map(),
    val extraCmdVars: Map[String, String] = Map(),
    val wantsTmp: Boolean = false,
    override val metaInfoEnv: meta.MetaInfoEnv = meta.KnownMetaInfoEnvs.all,
    override val archiveDependencies: Set[String] = Set(),
    override val contextDependencies: Set[String] = Set()
) extends NormalizerGenerator with StrictLogging {

  override def cleanup(): Unit = {
    this.synchronized {
      if (_envDir != null) {
        lab.LocalEnv.defaultSettings.nomadCodeRoot match {
          case Some(p) => logger.debug(s"parser $name is using external code path, will not delete anything")
          case None =>
            lab.LocalEnv.deleteRecursively(_envDir)
            if (trace) logger.info(s"parser $name deleting envDir ${_envDir}")
            // avoid? risks reallocation...
            _envDir = null
        }
      } else {
        if (trace) logger.debug(s"parser $name has nothing to delete")
      }
    }
  }

  var _envDir: Path = null
  def envDir: Path = {
    var res: Path = null
    this.synchronized {
      if (_envDir == null)
        _envDir = setupEnv()
      res = _envDir
    }
    res
  }

  def setupEnv(): Path = {
    lab.LocalEnv.defaultSettings.nomadCodeRoot match {
      case Some(p) => p
      case None =>
        val envDir = lab.LocalEnv.defaultSettings.procTmpDir.resolve("parserEnvs")
        Files.createDirectories(envDir, lab.LocalEnv.directoryPermissionsAttributes)
        val tDir = Files.createTempDirectory(envDir, name, lab.LocalEnv.directoryPermissionsAttributes)
        val ttDir = Files.createDirectories(tDir.resolve("tmp"), lab.LocalEnv.directoryPermissionsAttributes)
        try {
          SimpleExternalParserGenerator.copyAndRenameFromResources(resList, tDir, dirMap)
          if (trace) logger.info(s"Did setup of environment for parser $name in $tDir")
        } catch {
          case NonFatal(e) =>
            if (trace) logger.warn(s"Failed setup of environment for parser $name in $tDir due to $e")
            throw new SimpleExternalParserGenerator.UnpackEnvException(s"Failed setup of environment for parser $name in $tDir", e)
        }
        tDir
    }
  }

  def normalizer: Normalizer = {
    new ExternalNormalizer(this)
  }

  def stdInHandler(context: ResolvedRef)(wrapper: ExternalParserWrapper)(pIn: java.io.OutputStream): Unit = {
    pIn.close()
  }

  /**
   * logs stderr output as warning
   */
  def stdErrHandler(context: ResolvedRef)(wrapper: ExternalParserWrapper)(pErr: java.io.InputStream): Unit = {
    val lIn = new java.io.LineNumberReader(new java.io.BufferedReader(new java.io.InputStreamReader(pErr)))
    var line = lIn.readLine()
    if (line != null)
      logger.warn(s"<$name.stderr> => ${context.uriString}")
    while (line != null) {
      logger.warn(s"<$name.stderr>:${lIn.getLineNumber()}: $line")
      line = lIn.readLine()
    }
    lIn.close()
    pErr.close()
  }
}

class ExternalNormalizer(
    val generator: ExternalNormalizerGenerator
) extends Normalizer with StrictLogging {
  Files.createDirectories(lab.LocalEnv.defaultSettings.procTmpDir, lab.LocalEnv.directoryPermissionsAttributes)
  val tmpDir: Path = if (generator.wantsTmp)
    Files.createTempDirectory(lab.LocalEnv.defaultSettings.procTmpDir, "normalizerTmp", lab.LocalEnv.directoryPermissionsAttributes)

  else
    generator.envDir.resolve("tmp")
  if (trace) logger.info(s"parser $name created tmpDir for optimized parser at $tmpDir")

  def name: String = generator.name

  def normalizeInternal(context: ResolvedRef, backend: ParserBackendInternal): Unit = {
    val externalBackend = new ReindexBackend(backend)
    normalizeExternal(context, externalBackend)
  }

  def normalizeExternal(context: ResolvedRef, externalBackend: ParserBackendExternal): Unit = {
    val archivePath = context match {
      case Section(_, s) => s.table.calculation.archive.fileH5.path
      case Value(_, v) => v.table.parentSectionTable.calculation.archive.fileH5.path
      case SectionTable(_, s) => s.calculation.archive.fileH5.path
      case ValueTable(_, v) => v.parentSectionTable.calculation.archive.fileH5.path
      case Calculation(_, c) => c.archive.fileH5.path
      case Archive(_, a) => a.fileH5.path
      case RawData(_, archiveGid, path) =>
        throw new ExternalNormalizerException(s"normalizer $name called with path $path in raw data archive $archiveGid, which is not supported")
      case FailedResolution(uri, msg) =>
        throw new ExternalNormalizerException(s"normalizer $name called with context $uri that failed the resolution ($msg)")
    }

    val allReplacements = generator.extraCmdVars +
      ("envDir" -> generator.envDir.toString()) +
      ("tmpDir" -> tmpDir.toString()) +
      ("contextUri" -> context.uriString) +
      ("archivePath" -> archivePath.toString())

    val command = generator.cmd.map {
      lab.LocalEnv.makeReplacements(allReplacements, _)
    }

    val wrapper = new ExternalParserWrapper(
      mainFileUri0 = Some(context.uriString),
      mainFilePath0 = Some(archivePath.toString),
      backend = externalBackend,
      envDir = generator.envDir,
      tmpDir = tmpDir,
      cmd = command,
      cmdCwd = generator.cmdCwd,
      cmdEnv = generator.cmdEnv,
      parserName = name,
      fixedParserInfo0 = generator.info,
      stream = false,
      extraCmdVars = generator.extraCmdVars,
      stdInHandler = Some(generator.stdInHandler(context)),
      stdErrHandler = Some(generator.stdErrHandler(context))
    )
    wrapper.parseRequest(context.uriString, archivePath.toString, externalBackend, name)
  }

  def cleanup(): Unit = {
    if (generator.wantsTmp) {
      if (trace)
        logger.info(s"normalizer of ${name} deleting temporary directory $tmpDir")
      lab.LocalEnv.deleteRecursively(tmpDir)
    }
  }

}
