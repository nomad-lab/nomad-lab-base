/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5;
import java.nio.file.Path
import scala.io.Source
import eu.nomad_lab.JsonUtils

/**
 * Allow loading replacements fom a file with the following format
 *
 * # meta/path
 *   from -> to
 *   "from " -> " to"
 */
object Replacements {
  def fromFile(path: Path): Seq[Replacements] = {
    val f = Source.fromFile(path.toFile, "UTF-8")
    var metaPath: Option[Seq[String]] = None
    val metaPathRe = """^#\s+([a-zA-Z0-9_/]+)\s*$""".r
    val replPathRe = """^\s+("(?:[^\"]|\\.)*"|[-+a-zA-Z0-9_]+)\s+->\s+("(?:[^\\"]|\\.)*"|[-+a-zA-Z0-9_]+)\s*$""".r
    var repl: Map[String, String] = Map()
    var res: Seq[Replacements] = Seq()
    for (line <- f.getLines()) {
      line match {
        case replPathRe(rawFromStr, rawToStr) =>
          if (metaPath.isEmpty)
            throw new Exception(s"replacement '$line' without a meta path")
          val fromStr = if (!rawFromStr.isEmpty && rawFromStr(0) == '"')
            JsonUtils.unescapeQuotedString(rawFromStr)
          else
            rawFromStr
          val toStr = if (!rawToStr.isEmpty && rawToStr(0) == '"')
            JsonUtils.unescapeQuotedString(rawToStr)
          else
            rawToStr
          repl += (fromStr -> toStr)
        case metaPathRe(mPath) =>
          metaPath match {
            case Some(p) =>
              res = res :+ Replacements(p, repl)
              metaPath = Some(mPath.split("/"))
              repl = Map()
            case None =>
              metaPath = Some(mPath.split("/"))
          }
        case l =>
          if (!l.foldLeft(true)((x: Boolean, y: Char) => x && y.isWhitespace))
            throw new Exception(s"Invalid line '$line' in replacements file $path")
      }
    }
    metaPath match {
      case Some(p) =>
        res :+ Replacements(p, repl)
      case None =>
        res
    }
  }
}

/**
 *  Describes replacements to perform on string values in an Hdf5 file
 *
 * see H5Rename to apply them
 */
case class Replacements(
  metaPath: Seq[String] = Seq(),
  replacements: Map[String, String] = Map()
)
