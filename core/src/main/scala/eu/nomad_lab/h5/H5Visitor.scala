/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5
import eu.nomad_lab.ref._
import eu.nomad_lab.resolve._
import eu.nomad_lab.resolve.Resolver
import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.parsers.ParserBackendExternal
import scala.util.control.NonFatal
import scala.collection.mutable
import eu.nomad_lab.JsonUtils

class H5Visitor {
  def shouldVisitArchive(archive: ArchiveH5): Boolean = true
  def didVisitArchive(archive: ArchiveH5): Unit = ()

  def shouldVisitCalculation(calculation: CalculationH5): Boolean = true
  def didVisitCalculation(calculation: CalculationH5): Unit = ()

  def shouldVisitSectionTable(sectionTable: SectionTableH5): Boolean = true
  def didVisitSectionTable(sectionTable: SectionTableH5): Unit = ()

  def shouldVisitValueTable(valueTable: ValueTableH5): Boolean = true
  def didVisitValueTable(valueTable: ValueTableH5): Unit = ()

  def shouldVisitSectionCollection(sectionCollection: SectionCollectionH5): Boolean = {
    shouldVisitSectionTable(sectionCollection.table)
  }

  def didVisitSectionCollection(sectionCollection: SectionCollectionH5): Unit = {
    didVisitSectionTable(sectionCollection.table)
  }

  def shouldVisitSection(section: SectionH5): Boolean = true
  def didVisitSection(section: SectionH5): Unit = ()

  def shouldVisitSubValueTables(sectioTable: SectionTableH5): Boolean = true
  def didVisitSubValueTables(sectioTable: SectionTableH5): Unit = ()

  def shouldVisitSectionSubValues(section: SectionH5): Boolean = {
    shouldVisitSubValueTables(section.table)
  }

  def didVisitSectionSubValues(section: SectionH5): Unit = {
    didVisitSubValueTables(section.table)
  }

  def shouldVisitSubSectionTables(sectionTable: SectionTableH5): Boolean = true
  def didVisitSubSectionTables(sectionTable: SectionTableH5): Unit = ()

  def shouldVisitSectionSubSections(section: SectionH5): Boolean = {
    shouldVisitSubSectionTables(section.table)
  }

  def didVisitSectionSubSections(section: SectionH5): Unit = {
    didVisitSubSectionTables(section.table)
  }

  def shouldVisitValueCollection(valueCollection: ValueCollectionH5): Boolean = {
    shouldVisitValueTable(valueCollection.table)
  }

  def didVisitValueCollection(valueCollection: ValueCollectionH5): Unit = {
    didVisitValueTable(valueCollection.table)
  }

  def visitScalarValue(value: ValueH5): Unit = ()
  def visitArrayValue(value: ValueH5): Unit = ()

  def willVisitContext(context: ResolvedRef): Unit = ()
  def didVisitContext(context: ResolvedRef): Unit = ()
}

case class MetaInfoScanOptions(
  name: String,
  toVisit: Set[String],
  toSkip: Set[String],
  recurse: Option[Boolean],
  maxEls: Option[Long]
)

case class SectionStack(
  section: SectionH5,
  sectionOptions: MetaInfoScanOptions
)
