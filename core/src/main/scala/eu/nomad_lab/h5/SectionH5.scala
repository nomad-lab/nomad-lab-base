/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.{ IteratorH5, ChildObjectInfoH5, H5Lib, JsonUtils }
import ucar.ma2
import scala.collection.mutable
import scala.collection.SortedMap
import eu.nomad_lab.ref._
import eu.nomad_lab.parsers.CachingBackend.CachingSection
import org.{ json4s => jn }

/**
 * represents a single section
 */
case class SectionH5(
    table: SectionTableH5,
    gIndex: Long
) {

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[SectionH5]
    if (that == null)
      false
    else
      gIndex == that.gIndex && table == that.table
  }

  def metaName: String = {
    table.name
  }

  def indexedPath: Seq[(String, SortedMap[String, Long])] = {
    val path = mutable.ListBuffer[(String, SortedMap[String, Long])]()
    var hasMore: Boolean = true
    var sectionAtt: SectionH5 = this
    while (hasMore) {
      (sectionAtt.table.name -> SortedMap("c" -> sectionAtt.gIndex)) +=: path
      sectionAtt.parentSection match {
        case Some(s) =>
          if (path.contains(s.table.name))
            throw new SectionTableH5.SectionTableException(s"circular ref to ${s.table.name} in sections $path")
          sectionAtt = s
        case None => hasMore = false
      }
    }
    path
  }

  /**
   * Reference to this section table
   */
  def toRef: RowRef = {
    RowRef(table.calculation.archive.archiveGid, table.calculation.calculationGid, indexedPath)
  }

  override def toString: String = {
    s"SectionH5(${table.metaInfo.name}, $gIndex)"
  }

  /**
   * If the section is empty (does not exist)
   */
  def isMissing: Boolean = {
    if (table.sectionGroup() < 0 || gIndex < 0)
      true
    else
      false
  }

  /** the parent of this section */
  def parentSection: Option[SectionH5] = {
    table.parentSection match {
      case None =>
        None
      case Some(parentTable) =>
        if (gIndex < 0)
          return Some(parentTable(-1))
        val indexId = table.openIndexDataset()
        val resIndex = new ma2.ArrayLong.D2(1, 1)
        resIndex.setLong(0, -1)
        try {
          H5Lib.datasetRead(indexId, "i64", dimsToRead = Seq(1: Long, 1), offset = Seq(gIndex, 0), value = Some(resIndex))
        } finally {
          H5Lib.datasetClose(indexId)
        }
        Some(parentTable(resIndex.getLong(0)))
    }
  }

  /**
   * Section with metaName == sectionName contained in this section
   *
   * sectionName should be a direct subsection
   * It is the subset of table.subSectionTable(sectionName) in this section
   */
  def subSectionCollection(sectionName: String): SectionCollectionH5 = {
    val sTable = table.subSectionTable(sectionName)
    new SectionCollectionH5(sTable, gIndex, gIndex)
  }

  /**
   * Iterates on all non empty direct subsection collections in this section
   */
  def subSectionCollections(sortMeta: Boolean = false): Traversable[SectionCollectionH5] = {
    new Traversable[SectionCollectionH5] {
      def foreach[U](f: SectionCollectionH5 => U): Unit = {
        for (sectionTable <- table.subSectionTables(sortMeta)) {
          val coll = new SectionCollectionH5(sectionTable, gIndex, gIndex)
          if (!coll.isEmpty)
            f(coll)
        }
      }
    }
  }

  /**
   *  Values of type metaName contained in this section
   */
  def valueCollection(metaName: String): ValueCollectionH5 = {
    table.calculation.metaInfoEnv.metaInfoRecordForName(metaName) match {
      case None =>
        throw new CalculationH5.CalculationH5Error(s"could not find name $metaName in the metainfo")
      case Some(valueMetaInfo) =>
        val vTable = table.subValueTable(metaName)
        new ValueCollectionH5(vTable, gIndex, gIndex)
    }
  }

  /**
   * Iterates on all non empty value collections in this section
   */
  def valueCollections(sortMeta: Boolean = false): Traversable[ValueCollectionH5] = {
    new Traversable[ValueCollectionH5] {
      def foreach[U](f: ValueCollectionH5 => U): Unit = {
        for (valueTable <- table.subValueTables(sortMeta)) {
          val vCollection = valueCollection(valueTable.name)
          if (!vCollection.isEmpty)
            f(vCollection)
        }
      }
    }
  }

  /**
   * Single value contained in valueCollection if present, None otherwise.
   * repeated values trigger an exception
   */
  def maybeValue(metaName: String): Option[ValueH5] = {
    val vColl = valueCollection(metaName)
    vColl.length match {
      case 0 =>
        None
      case 1 =>
        Some(vColl.head)
      case l =>
        throw new CalculationH5.CalculationH5Error(s"cannot get maybeValue for $metaName from $this as it is repeated and has $l repetitions")
    }
  }

  /**
   * adds a value to this section
   */
  def addValue(metaName: String, jValue: jn.JValue): ValueIndexes = {
    val valueTable = table.subValueTable(metaName)
    valueTable.addValue(jValue, sectionGIndex = gIndex)
  }

  /**
   * adds a real value to this section
   */
  def addRealValue(metaName: String, value: Double): ValueIndexes = {
    val valueTable = table.subValueTable(metaName)
    valueTable.addRealValue(value, sectionGIndex = gIndex)
  }

  /**
   * adds an array value to this section
   */
  def addArrayValues(metaName: String, value: ma2.Array): ValueIndexes = {
    val valueTable = table.subValueTable(metaName)
    valueTable.addArrayValues(value, sectionGIndex = gIndex)
  }

  /**
   * adds an array to this section
   */
  def addArray(metaName: String, shape: Seq[Long]): ValueIndexes = {
    val valueTable = table.subValueTable(metaName)
    valueTable.addArray(shape, sectionGIndex = gIndex)
  }

  /**
   * adds an array value to this section
   */
  def setArrayValues(metaName: String, values: ma2.Array, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
    val valueTable = table.subValueTable(metaName)
    valueTable.setArrayValues(values, offset, indexes = indexes)
  }

  /**
   * adds the cached values to this section
   */
  def addValuesToSection(cachedSection: CachingSection): Unit = {
    table.addValuesToSection(cachedSection, sectionGIndex = gIndex)
  }
}

object SectionH5 {
  /**
   * binary search that finds the lower index position, i.e. the smallest index i such arr(i) >= key.
   * Inserting key before this index keeps the array sorted
   */
  def lowerGIndexSearch(arr: ma2.ArrayLong.D1, key: Long, low0: Option[Long], high0: Option[Long]): Long = {
    var low: Long = low0.getOrElse(0)
    var high: Long = high0.getOrElse(arr.getSize - 1)
    var mid: Long = (low + high) / 2 //dummy initialization
    val i = arr.getIndex()
    while (low <= high) {
      mid = (low + high) / 2 //true initialization
      if (arr.getLong(i.set(mid.toInt)) < key)
        low = mid + 1
      else
        high = mid - 1
    }
    low
  }

  /**
   * binary search that finds the upper index position, i.e. the biggest index i such arr(i) <= key.
   * Inserting key after this index keeps the array sorted
   */
  def upperGIndexSearch(arr: ma2.ArrayLong.D1, key: Long, low0: Option[Long], high0: Option[Long]): Long = {
    var low: Long = low0.getOrElse(0)
    var high: Long = high0.getOrElse(arr.getSize - 1)
    var mid: Long = (low + high) / 2 //dummy initialization
    val i = arr.getIndex()
    while (low <= high) {
      mid = (low + high + 1) / 2 //true initialization
      if (arr.getLong(i.set(mid.toInt)) <= key)
        low = mid + 1
      else
        high = mid - 1
    }
    high
  }
}
