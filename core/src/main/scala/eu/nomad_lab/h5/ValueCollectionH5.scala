/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.{ Base64, IteratorH5, H5Lib, JsonUtils }
import eu.nomad_lab.meta.MetaInfoRecord

import scala.util.control.NonFatal
import scala.collection.breakOut
import scala.collection
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import org.{ json4s => jn }

/**
 * represents a collection of values that are children of the sections
 * in the parentSectionLowerGIndex-parentSectionUpperGIndex range (inclusive)
 */
class ValueCollectionH5(
    val table: ValueTableH5,
    val parentSectionLowerGIndex: Long,
    val parentSectionUpperGIndex: Long,
    lowerGIndex0: Option[Long] = None,
    upperGIndex0: Option[Long] = None
) extends collection.IndexedSeq[ValueH5] {

  final override def iterator: Iterator[ValueH5] = {
    LongRange(lengthL).iterator.map(apply)
  }

  final override def foreach[U](f: ValueH5 => U): Unit = {
    LongRange(lengthL).foreach { i: Long =>
      f(apply(i))
    }
  }

  /**
   * gIndex of the last section in this collection (inclusive)
   *
   * If there are no entires then upperGIndex < lowerGIndex
   */
  val upperGIndex: Long = upperGIndex0 match {
    case Some(i) => i
    case None => if (table.isEmpty()) {
      -1
    } else {
      val index = table.openIndexDataset()
      val indexArray = H5Lib.datasetRead(index, "i64")
      val lArr = indexArray.slice(1, 0) match {
        case arr: ma2.ArrayLong.D1 => arr
      }
      val up = SectionH5.upperGIndexSearch(lArr, parentSectionUpperGIndex, None, None)
      H5Lib.datasetClose(index)
      up
    }
  }

  /**
   * gIndex of the first section in this collection (inclusive)
   *
   * If there are no entires then upperGIndex < lowerGIndex
   */
  var lowerGIndex: Long = lowerGIndex0 match {
    case Some(i) => i
    case None =>
      if (table.isEmpty()) {
        0
      } else {
        val index = table.openIndexDataset()
        val indexArray = H5Lib.datasetRead(index, "i64")
        val lArr = indexArray.slice(1, 0) match {
          case arr: ma2.ArrayLong.D1 => arr
        }
        val low = SectionH5.lowerGIndexSearch(lArr, parentSectionLowerGIndex, None, None)
        H5Lib.datasetClose(index)
        low
      }
  }

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[ValueCollectionH5]
    if (that == null)
      false
    else
      parentSectionLowerGIndex == that.parentSectionLowerGIndex && parentSectionUpperGIndex == that.parentSectionUpperGIndex && upperGIndex == that.upperGIndex && lowerGIndex == that.lowerGIndex && table == that.table
  }

  /**
   * returns the Value at index i
   */
  def apply(i: Long): ValueH5 = {
    val idx = i + lowerGIndex
    if (idx <= upperGIndex && i >= 0)
      ValueH5(table, idx)
    else
      ValueH5(table, -1)
  }

  /**
   * Number of sections in this collection
   */
  def lengthL: Long = {
    upperGIndex - lowerGIndex + 1
  }

  override def apply(i: Int): ValueH5 = apply(i.toLong)

  /**
   * Number of sections in this collection
   */
  override def length: Int = {
    if (lengthL < Int.MaxValue)
      lengthL.toInt
    else
      Int.MaxValue
  }

  /**
   * if there are no entries in this section collection
   */
  override def isEmpty: Boolean = {
    lengthL < 1
  }

  override def toString(): String = {
    s"ValueCollection(${table.metaInfo.name}, parentGIndex: $parentSectionLowerGIndex..$parentSectionUpperGIndex, lowerGIndex: $lowerGIndex, upperGIndex: $upperGIndex)"
  }

  /**
   * first value (will always return a value, but it might be empty)
   * should use headOption ?
   */
  def first: ValueH5 = {
    apply(0: Long)
  }

  /**
   * last value (will always return a value, but it might be empty)
   * should use lastOption ?
   */
  override def last: ValueH5 = {
    apply(lengthL - 1)
  }

}
