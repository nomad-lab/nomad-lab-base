/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.{ IteratorH5, H5Lib, RefCount }
import eu.nomad_lab.ref._
import com.typesafe.scalalogging.StrictLogging
import scala.util.control.NonFatal

object ArchiveH5 {
  def unapply(arch: ArchiveH5): Option[Tuple3[FileH5, String, Long]] = {
    Some((arch.fileH5, arch.archiveGid, arch.archiveGroup))
  }

  val calculationRe = """\A(?:C[-_a-zA-Z0-9]{28}|_)\Z""".r
  val realCalculationRe = """\A(?:C[-_a-zA-Z0-9]{28})\Z""".r

  /**
   * an object that loops on all calculations of an archive in an H5 file
   *
   * It releases all objects created, thus never leaking HDF5 stuff, but you need
   * to map a retain if you want to build a collection out of this.
   */
  class CalculationTraverser(
      archive: ArchiveH5
  ) extends Traversable[CalculationH5] with StrictLogging {
    private val subGroups = H5Lib.GroupContent(baseGroup = archive.archiveGroup)

    def lengthL: Long = {
      var len: Long = 0L
      try {
        for (gInfo <- subGroups) {
          if (gInfo.otype == H5Lib.GroupContent.ObjectType.Group) {
            gInfo.oname match {
              case realCalculationRe() => len += 1L
              case _ => ()
            }
          }
        }
      } catch {
        case NonFatal(e) =>
          logger.error(s"failure to calculate number of calculations in $archive", e)
      }
      len
    }

    def length: Long = lengthL

    /** loops on the real calculations, be careful you need to retain the object if you want to build a collection */
    def foreach[U](f: CalculationH5 => U): Unit = {
      var current: Option[CalculationH5] = None
      try {
        for (gInfo <- subGroups) {
          if (gInfo.otype == H5Lib.GroupContent.ObjectType.Group && (gInfo.oname match {
            case realCalculationRe() => true
            case _ => false
          })) {
            current match {
              case Some(arc) =>
                arc.release()
                current = None
              case None => ()
            }
            val el = archive.openCalculation(gInfo.oname)
            current = Some(el)
            f(el)
          }
        }
      } finally {
        current match {
          case Some(arc) =>
            arc.release()
            current = None
          case None => ()
        }
      }
    }
  }
}

/**
 * represents an archive in an hdf5 file, takes ownership of archiveGroup
 *
 * Is reference counted, and should be released to ensure that the underlying HDF5 resources are closed.
 */
class ArchiveH5(
    val fileH5: FileH5,
    val archiveGid: String,
    val archiveGroup: Long
) extends RefCount {
  /**
   * Returns a reference to this archive
   */
  def toRef: ArchiveRef = {
    ArchiveRef(archiveGid)
  }

  fileH5.retain()

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[ArchiveH5]
    if (that == null)
      false
    else
      archiveGid == that.archiveGid && fileH5 == that.fileH5
  }

  override def release0(): Unit = {
    H5Lib.groupClose(archiveGroup)
    fileH5.release()
  }

  /**
   * returns a calculation with the given gid if contained in this archive
   *
   * the calculation is reference counted remember to release it
   */
  def openCalculation(calculationGid: String, create: Boolean = false): CalculationH5 = {
    val calcGroup: Long = H5Lib.groupGet(archiveGroup, calculationGid, create)
    if (calcGroup < 0)
      throw new FileH5.FileH5Error(s"cannot open calculation $calculationGid in $this")
    new CalculationH5(
      archive = this,
      calculationGid = calculationGid,
      calculationGroup = calcGroup
    )
  }

  /**
   * loops on the calculations of this archive.
   * automatically releases the elements, to build a collection you have to map retain
   */
  def calculations(): ArchiveH5.CalculationTraverser = {
    new ArchiveH5.CalculationTraverser(this)
  }

  override def toString(): String = {
    s"ArchiveH5($archiveGid)"
  }
}
