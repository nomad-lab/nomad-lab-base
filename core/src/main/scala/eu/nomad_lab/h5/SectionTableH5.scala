/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.{ IteratorH5, ChildObjectInfoH5, H5Lib, JsonUtils }
import eu.nomad_lab.parsers.CachingBackend.CachingSection
import ucar.ma2
import scala.collection.mutable
import eu.nomad_lab.ref._
import eu.nomad_lab.H5Lib
import scala.util.control.NonFatal

object SectionTableH5 {
  val superGIndexOfRootSections: Long = 0 // use -1?

  /**
   * exception in Section table
   */
  class SectionTableException(
    msg: String
  ) extends Exception(msg)

  def apply(
    metaInfo: MetaInfoRecord,
    parent: SectionTableContainerH5,
    calculation: CalculationH5,
    sectionGroup: Option[Long] = None
  ): SectionTableH5 = {
    new SectionTableH5(
      metaInfo = metaInfo,
      parent = parent,
      calculation = calculation,
      _sectionGroup = sectionGroup
    )
  }

  val valueIndexRe = "^([a-zA-Z_][a-zA-Z0-9_]*)-index$".r

  /**
   * An object that loops on all subvalue tables of the current sectionTable
   */
  class ValueTableTraverser(
      val mainSectionTable: SectionTableH5,
      subGroups0: Option[H5Lib.GroupContent] = None,
      sortMeta: Boolean = false
  ) extends Traversable[ValueTableH5] {
    val subGroups = {
      val sg = subGroups0 match {
        case Some(g) => g
        case None =>
          H5Lib.GroupContent(baseGroup = mainSectionTable.sectionGroup(false))
      }
      if (sortMeta)
        sg.sortBy(_.oname)
      else
        sg
    }

    /** loops on the subvalue tables */
    def foreach[U](f: ValueTableH5 => U): Unit = {
      var current: Option[ValueTableH5] = None
      for (gInfo <- subGroups) {
        if (gInfo.otype == H5Lib.GroupContent.ObjectType.Dataset) {
          gInfo.oname match {
            case valueIndexRe(valueMetaName) =>
              if (valueMetaName != mainSectionTable.name) {
                val el = mainSectionTable.subValueTable(valueMetaName)
                f(el)
              }
            case _ => ()
          }
        }
      }
    }
  }
}

/**
 * Represents a table with all sections with a given metadata stored in a calculation in an HDF5 file
 */
class SectionTableH5(
    val metaInfo: MetaInfoRecord,
    val parent: SectionTableContainerH5,
    val calculation: CalculationH5,
    private var _sectionGroup: Option[Long] = None,
    protected var _subSectionTables: Map[String, SectionTableH5] = Map(),
    protected var _subValueTables: Map[String, ValueTableH5] = Map()
) extends SectionTableContainerH5 with IndexedSeq[SectionH5] {
  import SectionTableH5.SectionTableException

  override def name: String = metaInfo.name

  def parentSection: Option[SectionTableH5] = {
    parent match {
      case sectionTable: SectionTableH5 => Some(sectionTable)
      case _ => None
    }
  }

  if (metaInfo.kindStr != "type_section")
    throw new CalculationH5.CalculationH5Error(s"Cannot instantiate a SectionTableH5 with a non section ${JsonUtils.normalizedStr(metaInfo.toJValue())}")

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[SectionTableH5]
    if (that == null)
      false
    else
      metaInfo.name == that.metaInfo.name && calculation == that.calculation
  }

  /**
   *  kindString of the given entry (prioritizing the info in the HDF5 over the meta info)
   */
  def entryKindStr(entryName: String): Option[String] = {
    val gContent = H5Lib.GroupContent(baseGroup = sectionGroup(false))
    for (gInfo <- gContent) {
      if (gInfo.otype == H5Lib.GroupContent.ObjectType.Group && gInfo.oname == entryName)
        return Some("type_section")
      if (gInfo.otype == H5Lib.GroupContent.ObjectType.Dataset && gInfo.oname == entryName + "-index" && entryName != name)
        return Some("type_document_content")
    }
    calculation.metaInfoEnv.metaInfoRecordForName(entryName) match {
      case Some(m) =>
        if (calculation.metaInfoEnv.parentSectionName(entryName) != Some(name))
          None
        else
          Some(m.kindStr)
      case None =>
        None
    }
  }

  /**
   * returns the section path (i.e. a path from the root section to this one included)
   */
  def metaPath: Seq[String] = {
    val path = mutable.ListBuffer[String]()
    var hasMore: Boolean = true
    var sectionAtt: SectionTableH5 = this
    while (hasMore) {
      sectionAtt.name +=: path
      sectionAtt.parentSection match {
        case Some(s) =>
          if (path.contains(s.name))
            throw new SectionTableH5.SectionTableException(s"circular ref in sections $path")
          sectionAtt = s
        case None => hasMore = false
      }
    }
    path
  }

  /**
   * Reference to this section table
   */
  def toRef: TableRef = {
    TableRef(calculation.archive.archiveGid, calculation.calculationGid, metaPath)
  }

  /**
   * Returns the group of this section in the hdf file, creating if requested ane required
   * this stays cached until one call closeSectionGroup
   */
  def sectionGroup(create: Boolean = false): Long = {
    _sectionGroup match {
      case Some(g) =>
        if (g >= 0 || !create)
          return g
      case None => ()
    }
    val parentGroup = parentSection match {
      case Some(s) =>
        s.sectionGroup(create)
      case None =>
        calculation.calculationGroup
    }
    val resG: Long = if (parentGroup >= 0) {
      val existingG = try {
        H5Lib.groupOpen(locId = parentGroup, metaInfo.name)
      } catch {
        case NonFatal(e) =>
          -1L
      }
      if (existingG >= 0) {
        existingG
      } else if (create) {
        val newG = H5Lib.groupCreate(parentGroup, groupPath = metaInfo.name)
        val dimId = H5Lib.dimsCreate(Array(1: Long))
        val mAttrib = H5Lib.attributeCreate(
          newG,
          attributeName = "metaInfo",
          attributeType = "C",
          dimensionsId = dimId
        )
        H5Lib.dimsClose(dimId)
        H5Lib.attributeWriteStr(mAttrib, Array(JsonUtils.normalizedStr(metaInfo.toJValue(extraArgs = true, inlineExtraArgs = true))))
        H5Lib.attributeClose(mAttrib)
        newG
      } else {
        -1
      }
    } else {
      -1
    }
    _sectionGroup = Some(resG)
    resG
  }

  /**
   * closes the cached section group
   */
  def clearLocalCache(): Unit = {
    _sectionGroup match {
      case Some(g) =>
        _sectionGroup = None
        if (g >= 0)
          H5Lib.groupClose(g)
      case None => ()
    }
  }

  /**
   * Returns the index dataset, creating it when requested.
   *
   *  You get ownership of the returned dataset (i.e. remember to call close on it)
   */
  def openIndexDataset(create: Boolean = false): Long = {
    val name = metaInfo.name + "-index"
    val indexDim: Long = 1
    H5Lib.datasetGet(locId = sectionGroup(create), name, "i64",
      dimensions = Seq(0: Long, indexDim),
      maxDimensions = Seq(H5Lib.unlimitedDim, indexDim),
      create = create)
  }

  /**
   * true if current section table is empty
   */
  override def isEmpty: Boolean = {
    lengthL == 0
  }

  /**
   * override iteration (on longs so it works even if very large)
   */
  override def foreach[U](f: SectionH5 => U): Unit = {
    for (i <- (0: Long).until(lengthL))
      f(apply(i))
  }

  /**
   * Number of sections in the table
   *
   * cache? currently always recalculated, wich gives a better interaction with changes
   */
  def lengthL: Long = {
    if (sectionGroup() < 0) {
      0
    } else {
      val index = openIndexDataset()
      val dimsId = H5Lib.datasetGetDimensionsId(index)
      val dims = H5Lib.dimsExtents(dimsId)
      val l = dims(0)
      H5Lib.dimsClose(dimsId)
      H5Lib.datasetClose(index)
      l
    }
  }

  def length: Int = {
    val l = lengthL
    if (l > Int.MaxValue)
      throw new CalculationH5.CalculationH5Error(s"number of sections overflowed 32 bits")
    l.toInt
  }

  /**
   * First section of the table
   */
  def first: SectionH5 = {
    apply(0)
  }

  /**
   * Last section of the table
   */
  override def last: SectionH5 = {
    apply(lengthL - 1)
  }

  /**
   * i th section of the table
   */
  def apply(i: Int): SectionH5 = {
    apply(i.toLong)
  }

  /**
   * i th section of the table, i.e. the section with gIndex i
   */
  def apply(i: Long): SectionH5 = {
    val n = if (lengthL <= i) -1 else i
    new SectionH5(table = this, gIndex = n)
  }

  /**
   * Table of the direct child values
   */
  def subValueTable(name: String): ValueTableH5 = {
    calculation.synchronized {
      _subValueTables.get(name) match {
        case Some(t) => t
        case None =>
          val newVal = calculation.instantiateValueTable(name, this)
          if (newVal.parentSectionTable != this)
            throw new CalculationH5.CalculationH5Error(s"asking for $name in $this which is not a direct subvalue, but is ${newVal.parentSectionTable}")
          _subValueTables += (name -> newVal)
          newVal
      }
    }
  }

  override def toString: String = {
    s"SectionTable(${metaInfo.name}, parent: ${parent.name}, calculation: ${calculation.calculationGid})"
  }

  def lastSectionGIndex: Long = {
    lengthL - 1
  }

  /**
   * extracts the gIndex of the super section of the current section
   */
  def getSuperGIndex(references: Map[String, Long]): Long = {
    parentSection match {
      case Some(superSection) =>
        references.get(superSection.metaInfo.name) match {
          case Some(i) =>
            if (i == -1)
              superSection.lastSectionGIndex
            else
              i
          case None =>
            throw new SectionTableException(s"setSectionInfo missing super section ${superSection.metaInfo.name} of ${metaInfo.name} in references $references")
        }
      case None => SectionTableH5.superGIndexOfRootSections
    }
  }

  /**
   * sets the super section of the section gIndex
   *
   * references should be references to gIndex of the root sections this section refers to.
   */
  def setSuperSectionGIndex(gIndex: Long, superGIndex: Long): Unit = {
    val gIdx = if (gIndex == -1)
      lastSectionGIndex
    else
      gIndex
    val newIndexValues = new ma2.ArrayLong.D2(1, 1)
    newIndexValues.set(0, 0, superGIndex)
    val idx = openIndexDataset(false)
    if (idx >= 0) {
      H5Lib.datasetWrite(
        idx,
        value = newIndexValues,
        offset = Seq(gIdx, 0),
        dtypeStr = "i64"
      )
      H5Lib.datasetClose(idx)
    } else {
      throw new SectionTableException(s"setSectionInfo called with invalid section index $gIndex for ${metaInfo.name}")
    }
  }

  /**
   * returns the gIndex of a newly created section
   */
  def addSection(superGIndex: Option[Long] = None): Long = {
    sectionGroup(true)
    val indexDatasetId = openIndexDataset(true)
    val indexDimId = H5Lib.datasetGetDimensionsId(indexDatasetId)
    val indexDims = H5Lib.dimsExtents(indexDimId)
    val globalIndex = indexDims(0)
    indexDims(0) += 1
    H5Lib.datasetResize(indexDatasetId, indexDims)
    val newIndexValues = new ma2.ArrayLong.D2(1, 1)
    newIndexValues.set(0, 0, superGIndex match {
      case Some(superI) => superI
      case None =>
        parentSection match {
          case Some(section) => section.lastSectionGIndex
          case None => SectionTableH5.superGIndexOfRootSections
        }
    })
    H5Lib.datasetWrite(
      indexDatasetId,
      value = newIndexValues,
      offset = Seq(globalIndex, 0),
      dtypeStr = "i64"
    )
    H5Lib.datasetClose(indexDatasetId)
    globalIndex
  }

  /**
   * returns the value tables directly contained in this section table
   */
  def subValueTables(sortMeta: Boolean = false): SectionTableH5.ValueTableTraverser = {
    new SectionTableH5.ValueTableTraverser(this, sortMeta = sortMeta)
  }

  /**
   * adds all the values in the section as new values
   */
  def addValuesToSection(cachingSection: CachingSection, sectionGIndex: Long = -1): Unit = {
    val gIndex = ((if (sectionGIndex >= 0)
      sectionGIndex
    else
      cachingSection.gIndex) match {
      case -1 =>
        addSection()
      case v =>
        v
    })
    for ((k, values) <- cachingSection.cachedSimpleValues) {
      val t = subValueTable(k)
      for (v <- values)
        t.addValue(v, sectionGIndex = gIndex)
    }
    for ((k, values) <- cachingSection.cachedArrayValues)
      for (v <- values)
        subValueTable(k).addArrayValues(v, sectionGIndex = gIndex)
    for ((k, sects) <- cachingSection.cachedSubSections) {
      for (sect <- sects) {
        val table = subSectionTable(k)
        table.addValuesToSection(sect)
      }
    }
  }
}
