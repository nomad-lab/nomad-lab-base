/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

object LongRange {
  def apply(to: Long): LongRange = {
    new LongRange(0, to, 1)
  }

  def apply(from: Long, to: Long): LongRange = {
    new LongRange(from, to, 1)
  }

}

case class LongRange(
    fromI: Long,
    toI: Long,
    stepI: Long
) extends Iterable[Long] with Traversable[Long] {
  if (stepI == 0) throw new Exception(s"stepI is 0 in $this")

  override def iterator: Iterator[Long] = {
    if (stepI == 1) {
      new Iterator[Long] {
        var i: Long = fromI
        override def hasNext: Boolean = {
          i < toI
        }

        override def next: Long = {
          val res = i
          i += 1
          res
        }
      }
    } else if (stepI > 0) {
      new Iterator[Long] {
        var i: Long = fromI
        override def hasNext: Boolean = {
          i < toI
        }

        override def next: Long = {
          val res = i
          i += stepI
          res
        }
      }
    } else {
      new Iterator[Long] {
        var i: Long = fromI
        override def hasNext: Boolean = {
          i > toI
        }

        override def next: Long = {
          val res = i
          i += stepI
          res
        }
      }
    }
  }

  override def foreach[U](f: Long => U): Unit = {
    var i: Long = fromI
    if (stepI > 0) {
      while (i < toI) {
        f(i)
        i += stepI
      }
    } else {
      while (i > toI) {
        f(i)
        i += stepI
      }
    }
  }
}
