/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import java.nio.file.Path
import java.nio.file.Files
import java.util.NoSuchElementException

import eu.nomad_lab.{ IteratorH5, H5Lib, RefCount, JsonUtils }
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.KnownMetaInfoEnvs
import eu.nomad_lab.LocalEnv
import scala.util.control.NonFatal
import scala.collection.mutable
import com.typesafe.scalalogging.StrictLogging
import com.typesafe.scalalogging.LazyLogging

object FileH5 extends LazyLogging {
  class FileH5Error(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  def acquire(filePath: java.nio.file.Path, write: Boolean): Unit = {
    val lockPath = filePath.getParent.resolve(filePath.getFileName.toString + ".lock")
    if (!write && !LocalEnv.defaultSettings.h5LockOnRead) {
      if (Files.exists(lockPath)) {
        val (readers, writers) = (try {
          val f = new java.io.RandomAccessFile(lockPath.toFile(), "r")
          (try {
            f.readInt() -> f.readInt()
          } catch {
            case NonFatal(e) =>
              0 -> 0
          } finally {
            f.close()
          }): (Int, Int)
        } catch {
          case NonFatal(e) =>
            0 -> 0
        }): (Int, Int)
        if (writers > 0)
          throw new Exception(s"cannot read because a process is writing and requires exclusive access, $readers and $writers writers are already registered for $filePath")
      }
      return ()
    }
    val f = new java.io.RandomAccessFile(lockPath.toFile(), "rw")
    synchronized {
      val lock = f.getChannel().lock()
      try {
        f.seek(0)
        val (readers, writers) = (try {
          f.readInt() -> f.readInt()
        } catch {
          case NonFatal(e) =>
            0 -> 0
        }): (Int, Int)
        if (write) {
          if (readers > 0 | writers > 0)
            throw new Exception(s"writing requires exclusive access, $readers and $writers writers are already registered for $filePath")
          f.seek(0)
          f.writeInt(readers)
          f.writeInt(writers + 1)
        } else {
          if (writers > 0)
            throw new Exception(s"concurrent read and writing is not yet supported, $readers and $writers writers are already registered for $filePath")
          f.seek(0)
          f.writeInt(readers + 1)
          f.writeInt(writers)
        }
        f.seek(f.length())
        f.writeBoolean(write)
        val currentProcessName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName()
        f.writeUTF(currentProcessName)
      } finally {
        f.close()
      }
    }
  }

  def release(filePath: java.nio.file.Path, write: Boolean): Unit = {
    if (!write && !LocalEnv.defaultSettings.h5LockOnRead)
      return ()
    val lockPath = filePath.getParent.resolve(filePath.getFileName.toString + ".lock")
    val f = new java.io.RandomAccessFile(lockPath.toFile, "rw")
    synchronized {
      val lock = f.getChannel().lock()
      try {
        f.seek(0)
        val readers = f.readInt()
        val writers = f.readInt()
        val vals = mutable.ListBuffer[(Boolean, String)]()
        val currentProcessName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName()
        var found = false
        while (f.getFilePointer() < f.length()) {
          val w = f.readBoolean()
          val r = f.readUTF()
          if (r != currentProcessName || w != write)
            vals += (w -> r)
          else
            found = true
        }
        if (write) {
          f.seek(0)
          f.writeInt(readers)
          f.writeInt(writers - 1)
          if (writers != 1 || readers != 0)
            throw new Exception(s"Unexpected status when releasing write, there were $readers and $writers writers registered for $filePath")
        } else {
          f.seek(0)
          f.writeInt(readers - 1)
          f.writeInt(writers)
          if (writers != 0 || readers < 1)
            throw new Exception(s"Unexpected status when releasing read, there were $readers and $writers writers registered for $filePath")
        }
        if (!found)
          logger.warn(s"Unexpected status in release, could not find current process $currentProcessName in $filePath")
        for ((w, r) <- vals) {
          f.writeBoolean(w)
          f.writeUTF(r)
        }
        f.setLength(f.getFilePointer())
      } finally {
        f.close()
      }
    }
  }

  /**
   * opens an existing Hdf5 file, reference counted, remember to release
   */
  def open(filePath: java.nio.file.Path, write: Boolean = false,
    metaInfoEnv: Option[MetaInfoEnv] = None, acquired: Boolean = false): FileH5 = {
    try {
      if (!acquired)
        acquire(filePath, write)
      val h5Group = H5Lib.fileOpen(filePath.toString, write = write)
      new FileH5(filePath, h5Group, metaInfoEnv.getOrElse(KnownMetaInfoEnvs.all), writeable = write, releaseLock = !acquired)
    } catch {
      case NonFatal(e) =>
        throw new FileH5Error(s"failure opening $filePath (write:$write)", e)
    }
  }

  /**
   * creates a new Hdf5 file, reference counted, remember to release
   */
  def create(filePath: java.nio.file.Path, metaInfoEnv: Option[MetaInfoEnv] = None,
    acquired: Boolean = false): FileH5 = {
    try {
      if (!acquired)
        acquire(filePath, write = true)
      val h5Group = H5Lib.fileCreate(filePath.toString)
      new FileH5(filePath, h5Group, metaInfoEnv.getOrElse(KnownMetaInfoEnvs.all), writeable = true)
    } catch {
      case NonFatal(e) =>
        throw new FileH5Error(s"could not create file $filePath", e)
    }
  }

  def unapply(fileH5: FileH5): Option[(Path, Long, MetaInfoEnv, Boolean)] = {
    Some((fileH5.path, fileH5.h5FileGroup, fileH5.metaInfoEnv, fileH5.writeable))
  }

  val archiveRe = "[RA][-_a-zA-Z0-9]{28}|unknown|undefined$".r

  /**
   * an object that loops on all archives of an h5 file
   *
   * It releases all objects created, thus never leaking HDF5 stuff, but you need
   * to map a retain if you want to build a collection out of this.
   */
  class ArchiveTraverser(
      file: FileH5
  ) extends Traversable[ArchiveH5] {
    private val subGroups = H5Lib.GroupContent(baseGroup = file.h5FileGroup)

    /** loops on the archives, be careful you need to retain the object if you want to build a collection */
    def foreach[U](f: ArchiveH5 => U): Unit = {
      var current: Option[ArchiveH5] = None
      try {
        for (
          gInfo <- subGroups.filter { gI =>
            gI.otype == H5Lib.GroupContent.ObjectType.Group && (gI.oname match {
              case archiveRe() => true
              case _ => false
            })
          }
        ) {
          current match {
            case Some(arc) =>
              arc.release()
              current = None
            case None => ()
          }
          val el = file.openArchive(gInfo.oname)
          current = Some(el)
          f(el)
        }
      } finally {
        current match {
          case Some(arc) =>
            arc.release()
            current = None
          case None => ()
        }
      }
    }
  }

}

/**
 * represents an HDF5 file
 *
 * This object is reference counted and should be released to ensure that the underlying HDF file is closed.
 */
class FileH5(
    val path: java.nio.file.Path,
    val h5FileGroup: Long,
    val metaInfoEnv: MetaInfoEnv,
    val writeable: Boolean,
    val releaseLock: Boolean = true
) extends RefCount with StrictLogging {

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[FileH5]
    if (that == null)
      false
    else
      path == that.path
  }

  override def release0(): Unit = {
    logger.debug(s"closing file $path")
    if (h5FileGroup >= 0) {
      H5Lib.fileFlush(h5FileGroup)
      H5Lib.fileClose(h5FileGroup)
    }
    if (releaseLock)
      FileH5.release(path, writeable)
  }

  /**
   * returns the archive with the given gid
   *
   * reference counted, remember to release!
   */
  def openArchive(archiveGid: String, create: Boolean = false): ArchiveH5 = {
    val archGroup = H5Lib.groupGet(h5FileGroup, archiveGid, create)
    if (archGroup < 0)
      throw new FileH5.FileH5Error(s"could not open archive $archiveGid in $this")
    new ArchiveH5(
      fileH5 = this,
      archiveGid = archiveGid,
      archiveGroup = archGroup
    )
  }

  /**
   * Iterator on the archives in this file.
   * On call to next, it closes the last iterated archive.
   */
  def archives(): FileH5.ArchiveTraverser = {
    new FileH5.ArchiveTraverser(this)
  }

  override def toString(): String = {
    s"FileH5{ path: $path metaInfo: ${JsonUtils.normalizedStr(metaInfoEnv.source)} refCount: $refCount}"
  }
}
