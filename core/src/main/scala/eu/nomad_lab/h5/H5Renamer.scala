/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5;
import ucar.ma2
import eu.nomad_lab.H5Lib

object H5Rename {

  /**
   * Renames the values of the value table at repl.metaPath of all calculations in the archive according to the repl.replacements mapping
   */
  def renameStr(file: FileH5, repls: Seq[Replacements]): Unit = {
    for (arch <- file.archives()) {
      renameStr(arch, repls)
    }
  }

  /**
   * Renames the values of the value table at repl.metaPath of all calculations in the archive according to the repl.replacements mapping
   */
  def renameStr(archive: ArchiveH5, repls: Seq[Replacements]): Unit = {
    for (calculation <- archive.calculations()) {
      renameStr(calculation, repls)
    }
  }

  /**
   * Renames the values of the value table at repl.metaPath of the given calculation
   * according to the repl.replacements mapping
   */
  def renameStr(calculation: CalculationH5, repls: Seq[Replacements]): Unit = {
    for (repl <- repls) {
      val valueTablePath = repl.metaPath
      val valueTable = calculation.sectionTable(valueTablePath.dropRight(1)).subValueTable(valueTablePath.last)
      renameStr(valueTable, repl.replacements)
    }
  }

  /**
   * Renames the values of the value table according to the renames mapping
   */
  def renameStr(valueTable: ValueTableH5, renames: Map[String, String]): Unit = {
    val nStrings = valueTable.lengthL
    if (nStrings > 1000000000L)
      throw new Exception(s"Excessive size in $valueTable ($nStrings) batch rename should be implemented")

    if (nStrings > 0L) {
      val (valueDatasetName, valueDatasetId) = valueTable.openValueDataset(Seq(1), false)
      try {
        val vals = new ma2.ArrayString.D1(nStrings.toInt)
        val strings = H5Lib.datasetRead(
          datasetId = valueDatasetId,
          dtypeStr = "C",
          dimsToRead = Seq(), offset = Seq(),
          value = Some(vals)
        )

        var hasRenames: Boolean = false

        {
          for (ii <- 0 until nStrings.toInt) {
            val sNow = vals.get(ii)
            renames.get(sNow) match {
              case Some(repl) =>
                vals.set(ii, repl)
                hasRenames = true
              case None =>
            }
          }
        }
        if (hasRenames) {
          //logger.debug(s"$valueTable has replacements")
          H5Lib.datasetWrite(valueDatasetId, value = vals, dtypeStr = "C")
        }
      } finally {
        H5Lib.datasetClose(valueDatasetId)
      }
    }
  }

}
