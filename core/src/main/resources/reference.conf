nomad_lab {
  configurationToUse=""
  configurationToUse=${?NOMAD_ENV}
  //  configurationToUse: use the given subtree; Perhaps the most important variable. Set this to the subtree you want to use.
  //  Important: Config below is only the fallback option !!! Change the config in the subtree set by configurationToUse.
  externalConfigFile="nomad.conf"
  // externalConfigFile should be overwritten in the application.conf of individual programs
  // using this config and specifies the name of the external configuration file to use
  nomadCodeRoot = ""
  nomadCodeRoot = ${?NOMAD_CODE_ROOT}
  h5LockOnRead = true
  replacements {
    rootNamespace = "${user}-default"
  }
  normalizers.trace = false
  processorBackend.trace = false
  rdb {
    enabled = false
    username = "nomad_lab"
    password = "pippo"
    jdbcUrl = "jdbc:h2:mem:rdb;DB_CLOSE_DELAY=-1"
  }
  parsed.archiveSet {
    baseDirectory = "${parsedRoot}/productionH5"
    hasPrefix = true
    writeable = false
    defaultMetaEnv = "all"
    filePrefix = "S"
  }
  toNormalize.archiveSet {
    baseDirectory = "${normalizedRoot}/${rootNamespace}"
    hasPrefix = true
    writeable = true
    defaultMetaEnv = "all"
    filePrefix = "N"
  }
  normalized.archiveSet {
    baseDirectory = "${normalizedRoot}/productionH5"
//    baseDirectory = "${normalizedRoot}/${rootNamespace}"
    hasPrefix = true
    writeable = false
    defaultMetaEnv = "all"
    filePrefix = "N"
  }
  parsing_stats {
    enabled = false
    username = parsing_stats
    password = "pippo"
    jdbcUrl = "jdbc:postgresql://parsingstatsdb.default.svc.cluster.local:5432/parsing_stats"
  }
  roots {
    baseTmpDir = "/work-local"
    parsedRoot = "/parsed"
    rawDataRoot = "/raw-data"
    normalizedRoot = "/normalized"
  }
  remoteSource {
    remoteHost = ""
    remoteRawDataRoot = "/raw-data"
    remoteParsedRoot = "/parsed"
    remoteRawDataArchives = "${remoteRawDataRoot}/data/${archiveGidPrefix}/${archiveGid}.zip"
    remoteParsedArchives = "${remoteParsedRoot}/${rootNamespace}/parch-${coreVersion}/${parsedArchiveFGidPrefix}/${parsedArchiveGid}.zip"
  }
  hostPaths{
    baseTmpDir = "/scratch/nomadlab/work-local"
    parsedRoot = "/nomad/nomadlab/parsed"
    rawDataRoot = "/nomad/nomadlab/raw-data"
    normalizedRoot = "/nomad/nomadlab/normalized"
    baseSharedTmp = "/nomad/nomadlab/work-local"
  }
  elasticQuery {
    elasticDataDumpPath = "${normalizedRoot}/elastic/${archiveGid}/${calculationGid}.json"
    elasticScripts = "/scripts"
    elasticTemporary = "/temporary-downloads"
  }
  archive{
    elastic{
      clientUri = "elasticsearch://localhost:9200"
      indexNameData = "archive"
      indexNameTopics = "<unused>"
      typeNameData = "calculation"
      typeNameTopics = "<unused>"
      nCachedCalculations = 1000
      verbose = false
      waitTime = "10 s"
    }
    webservice{
      interface: 0.0.0.0
      interface: ${?NOMADARCHIVE_WEBSERVICE_INTERFACE}
      port: 8112
      port: ${?NOMADARCHIVE_WEBSERVICE_PORT}
      protocol: ""
      protocol: ${?NOMADARCHIVE_WEBSERVICE_PROTOCOL}
      hostName: "localhost:"${nomad_lab.archive.webservice.port}
      hostName: ${?NOMADARCHIVE_WEBSERVICE_HOSTNAME}
      rootPath: ""
      rootPath: ${?NOMADARCHIVE_WEBSERVICE_ROOTPATH}
    }
    file_service{
      interface: 0.0.0.0
      port: 8113
      protocol: ""
      hostName: "localhost:"${nomad_lab.archive.file_service.port}
      rootPath: ""
      fileLocations: []
    }
  }
  repo{
    elastic{
      clientUri = "elasticsearch://localhost:9200"
      indexNameData = "repo"
      indexNameTopics = "topics"
      typeNameData = "repository_calculation"
      typeNameTopics = "section_repository_topic"
      nCachedCalculations = 1000
      verbose = false
      waitTime = "30 s"
    }
    webservice{
      interface: 0.0.0.0
      interface: ${?NOMADREPO_WEBSERVICE_INTERFACE}
      port: 8111
      port: ${?NOMADREPO_WEBSERVICE_PORT}
      protocol: ""
      protocol: ${?NOMADREPO_WEBSERVICE_PROTOCOL}
      hostName: "localhost:"${nomad_lab.repo.webservice.port}
      hostName: ${?NOMADREPO_WEBSERVICE_HOSTNAME}
      rootPath: ""
      rootPath: ${?NOMADREPO_WEBSERVICE_ROOTPATH}
    }
  }
  rawDataArchives = "${rawDataRoot}/data/${archiveGidPrefix}/${archiveGid}.zip"
  parser_worker_rabbitmq {
    whichWorker="NormalizerWorker"
    overwrite = false
    normalizationQueue = "NormalizationQueue"
    mergingQueue = "MergingQueue"
    treeParserExchange = "TreeParserExchange"
    treeParserQueue = "TreeParserInitializationQueue"
    singleParserExchange = "SingleParserExchange"
    singleParserQueue = "SingleParserInitializationQueue"
    toBeNormalizedExchange = "ToBeNormalizedExchange"
    toBeNormalizedQueue = "ToBeNormalizedInitializationQueue"
    normalizerExchange = "NormalizerExchange"
    normalizerQueue = "NormalizerInitializationQueue"
    normalizerCompletedQueue ="NormalizationFinishedQueue"
    normalizerCompletedExchange ="NormalizationFinishedExchange"
    uncompressRoot = "${procTmpDir}/uncompress"
    parsedJsonPath = "${parsedRoot}/${rootNamespace}/${parserId}/${archiveGid}/${parsedFileGid}.json"
    parsedCalculationH5Path = "${parsedRoot}/${rootNamespace}/${parserId}/${archiveGid}/${parsedFileGid}.h5"
    parsedTempH5Path="${parsedRoot}/${rootNamespace}/${archiveGidPrefix}/${parsedFileGid}.tmp"
    parsedSingleH5Path = "${parsedRoot}/${rootNamespace}s/S${baseArchiveGidPrefix}/S${baseArchiveGid}.h5"
    parsedArchiveH5Path = "${parsedRoot}/${rootNamespace}/${parsedArchiveGid}.h5"
    rabbitMQHost = "rabbitmq.${rootNamespace}.svc.cluster.local"
    rabbitMQHost = ${?RABBITMQ_SERVICE_HOST}
    rabbitMQPort = "5672"
    rabbitMQPort = ${?RABBITMQ_SERVICE_PORT}
    numberOfWorkers = 2
  }
  calculation_info_normalizer {
    pidEndpoint = "http://localhost:8111/repo/utility/pids?gid=\"${archiveGid}\""
  }
  integrated_pipeline {
    //TODO: give meaningful defaults here
    targetDirectory = "."
    numWorkers = 1

    inFile = ""
    treeparserMinutesOfRuntime = -1
  }
  python {
    pythonExe = "/labEnv3/bin/python"
    pythonExe= ${?PYTHONEXE}
    commonFiles = [
    "python/nomadcore/model_archive.py",
    "python/nomadcore/json_support.py",
    "python/nomadcore/annotator.py",
    "python/nomadcore/match_highlighter.py",
    "python/nomadcore/unit_conversion/__init__.py",
    "python/nomadcore/unit_conversion/unit_conversion.py",
    "python/nomadcore/unit_conversion/constants.txt",
    "python/nomadcore/unit_conversion/units.txt",
    "python/nomadcore/md_data_access/MDDataAccess.py",
    "python/nomadcore/md_data_access/GromosTopoObjects.py",
    "python/nomadcore/md_data_access/__init__.py",
    "python/nomadcore/metainfo_storage/MetaInfoStorage.py",
    "python/nomadcore/metainfo_storage/__init__.py",
    "python/nomadcore/smart_parser/SmartParserCommon.py",
    "python/nomadcore/smart_parser/SmartParserDictionary.py",
    "python/nomadcore/smart_parser/__init__.py",
    "python/nomadcore/basic_meta_info.py",
    "python/nomadcore/compact_sha.py",
    "python/nomadcore/__init__.py",
    "python/nomadcore/utils.py",
    "python/nomadcore/coordinate_reader.py",
    "python/nomadcore/model_base.py",
    "python/nomadcore/model_meta_info.py",
    "python/nomadcore/parse_streamed_dicts.py",
    "python/nomadcore/ActivateLogging.py",
    "python/nomadcore/caching_backend.py",
    "python/nomadcore/local_backend.py",
    "python/nomadcore/local_meta_info.py",
    "python/nomadcore/parser_backend.py",
    "python/nomadcore/simple_parser.py",
    "python/nomadcore/baseclasses.py",
    "python/nomadcore/elements.py",
    "python/nomadcore/csvparsing.py",
    "python/nomadcore/configurationreading.py",
    "python/nomadcore/archive.py",
    "python/nomadcore/structure_types.py"
    ],
    commonDirMapping = {
      "python": "python-common/common/python"
    }
  }
}
overrides{
default.nomad_lab {
  normalizers.trace = true
  processorBackend.trace = true
  replacements {
    rootNamespace = "${user}-default"
  }
}

flink.nomad_lab {
  h5LockOnRead = false
  toNormalize.archiveSet.baseDirectory = "${normalizedRoot}/newProductionH5"
  replacements {
    rootNamespace = "prod-${coreVersion}"
  }
  roots {
    baseTmpDir = "/tmp"
    parsedRoot = "/nomad/nomadlab/parsed"
    rawDataRoot = "/nomad/nomadlab/raw-data"
    normalizedRoot = "/nomad/nomadlab/normalized"
  }
  calculation_info_normalizer {
    pidEndpoint = "http://staging-nomad.esc.rzg.mpg.de:8111/repo/utility/pids?gid=\"${archiveGid}\""
  }
}

production.nomad_lab {
  replacements {
    rootNamespace = "production"
  }
}

test.nomad_lab {
  replacements {
    rootNamespace = "test"
  }
}

localDir.nomad_lab {
  replacements {
    rootNamespace = "${user}-local"
  }
  roots{
    baseTmpDir = ${?HOME}"/NOMAD/work-local"
    parsedRoot = ${?HOME}"/NOMAD/parsed"
    rawDataRoot = ${?HOME}"/NOMAD/raw-data"
    normalizedRoot = ${?HOME}"/NOMAD/normalized"
  }
  python.pythonExe = ${?HOME}"/NOMAD/labEnv3/bin/python"
  parser_worker_rabbitmq {
    rabbitMQHost = "localhost"
    rabbitMQHost = ${?RABBITMQ_SERVICE_HOST}
    numberOfWorkers = 1
  }
}

local.nomad_lab {
  replacements {
    rootNamespace = "${user}-local"
  }
  parsing_stats {
    username = parsing_stats
    password = "pippo"
    jdbcUrl = "jdbc:postgresql://localhost:5435/parsing_stats"
  }
  roots{
    baseTmpDir = "/work-local"
    parsedRoot = "/parsed"
    rawDataRoot = "/raw-data"
    normalizedRoot = "/normalized"
  }
  parser_worker_rabbitmq {
    rabbitMQHost = "localhost"
    rabbitMQHost = ${?RABBITMQ_SERVICE_HOST}
    numberOfWorkers = 1
  }
}
labdev.nomad_lab {
  h5LockOnRead = false
  replacements {
    rootNamespace = "prod-${coreVersion}"
  }
  toNormalize.archiveSet.baseDirectory = "${normalizedRoot}/${rootNamespace}"
  calculation_info_normalizer {
    pidEndpoint = "http://staging-nomad.esc.rzg.mpg.de:8111/repo/utility/pids?gid=\"${archiveGid}\""
  }
  parsing_stats {
    enabled = false
    username = parsing_stats
    password = "pippo"
    jdbcUrl = "jdbc:postgresql://localhost:5435/parsing_stats"
  }
  roots{
    baseTmpDir = "/work-local"
    parsedRoot = "/parsed"
    rawDataRoot = "/raw-data"
    normalizedRoot = "/normalized"
  }
  parser_worker_rabbitmq {
    rabbitMQHost = "130.183.207.77"
    rabbitMQHost = ${?RABBITMQ_SERVICE_HOST}
    numberOfWorkers = 1
  }
}
eos_prod.nomad_lab {
  remoteSource {
    remoteHost = "130.183.207.77"
  }
  replacements {
    rootNamespace = "prod-${coreVersion}"
  }
  parsing_stats {
    jdbcUrl = "jdbc:postgresql://130.183.207.77:5435/parsing_stats"
  }
  roots{
    baseTmpDir = "/scratch/${user}/nomadlab/work-local"
    baseTmpDir = ${?TMPDIR}
    parsedRoot = "/scratch/${user}/nomadlab/parsed"
    rawDataRoot = "/scratch/${user}/nomadlab/raw-data"
    normalizedRoot = "/scratch/${user}/nomadlab/normalized"
  }
  python {
    pythonExe = "/u/fawzi/labEnv3/bin/python"
  }
  parser_worker_rabbitmq {
    rabbitMQHost = "labdev-nomad.esc.rzg.mpg.de"
    rabbitMQPort = "5672"
    numberOfWorkers = 31
    uncompressRoot = "${baseTmpDir}/uncompress"
  }
  calculation_info_normalizer {
    pidEndpoint = "http://eos01:6544/repo/utility/pids?gid=\"${archiveGid}\""
  }
}
staging.nomad_lab {
  repo {
    jdbcUrl = "jdbc:postgresql://localhost:5432/nomad_staging"
    username = "nomadstaging"
    jdbcUrl = ${?REPO_DB_JDBC_URL}
    username = ${?REPO_DB_USERNAME}
    webservice {
      protocol: "https://"
      hostName: "labdev-nomad.esc.rzg.mpg.de"
      rootPath: ""
    }
  }
}
kubernetes_prod.nomad_lab {
  h5LockOnRead = false
  replacements {
    rootNamespace = "prod-${coreVersion}"
  }
  toNormalize.archiveSet.baseDirectory = "${normalizedRoot}/${rootNamespace}"
  calculation_info_normalizer {
    pidEndpoint = "http://staging-nomad.esc.rzg.mpg.de:8111/repo/utility/pids?gid=\"${archiveGid}\""
  }
  elastic{
    clientUri = "elasticsearch://nomad-flink-02.esc:9200"
    indexNameData = "archive"
  }
  archive{
    elastic{
      clientUri = "elasticsearch://nomad-flink-02.esc:9200"
    }
    webservice{
      protocol: "https://"
      hostName: "analytics-toolkit.nomad-coe.eu"
      rootPath: ""
    }
  }
  repo{
    elastic{
      clientUri = "elasticsearch://localhost:9200"
    }
  }
  roots {
    baseTmpDir = "/tmp"
    parsedRoot = "/nomad/nomadlab/parsed"
    rawDataRoot = "/nomad/nomadlab/raw-data"
    normalizedRoot = "/nomad/nomadlab/normalized"
  }
}
//only used to expose the simpleStats API call while we need to keep the old version running
kubernetes_prod_dev_archive.nomad_lab {
  elastic{
    clientUri = "elasticsearch://nomad-flink-02.esc:9200"
    indexNameData = "archive-dev"
  }
  archive{
    webservice{
      protocol: "https://"
      hostName: "analytics-toolkit.nomad-coe.eu"
      rootPath: ""
    }
  }
}
kubernetes_dev.nomad_lab {
  archive{
    elastic{
      clientUri = "elasticsearch://nomad-flink-02.esc:9200"
      indexNameData = "archive-dev"
    }
    webservice{
      protocol: "https://"
      hostName: "labdev-nomad.esc.rzg.mpg.de"
      rootPath: "dev/"
    }
  }
}
}