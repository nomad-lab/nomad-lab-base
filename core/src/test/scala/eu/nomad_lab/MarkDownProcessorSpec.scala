/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification

//TODO: Write more test
class MarkDownProcessorSpec extends Specification {
  val strList = Set("cat", "ground", "list", "link2")

  val testCases = Seq(
    ("str", "<p>str</p>"),
    ("# heading", "<h1>heading</h1>"), //Simple Markdown
    ("## heading2", "<h2>heading2</h2>"), //Simple Markdown
    ("[some Random String]", "<p>[some Random String]</p>"), //Square Bracker without link; no change expected
    ("[String](link)", """<p><a href="link">String</a></p>"""),
    ("some Random [String](link)", """<p>some Random <a href="link">String</a></p>"""),
    ("""escape Math `\[ x^2 *r* 1 < 4 > 7 \]`""", """<p>escape Math <code>\[ x^2 *r* 1 &lt; 4 &gt; 7 \]</code></p>"""),
    ("""escape Math2 $ x^2 *r* 1 < 4 > 7 $""", """<p>escape Math2 $ x^2 *r* 1 &lt; 4 &gt; 7 $</p>"""),
    ("cat", """<p>(link:cat)</p>"""),
    ("""\\\\""", """<p>\\</p>"""),
    ("""\\ cat list random \\""", """<p>\ (link:cat) (link:list) random \</p>"""),
    ("""\\ $ cat list random $ \\""", """<p>\ $ cat list random $ \</p>"""),
    ("""\\ [ cat list random ](link) \\""", """<p>\ <a href="link"> cat list random </a> \</p>"""),
    ("""\\ [ cat list random ] \\""", """<p>\ [ cat list random ] \</p>"""),
    ("""\\ < cat \\ "list" random > \\""", """<p>\ &lt; cat \ &quot;list&quot; random &gt; \</p>"""),
    (
      """1.  Bird
       |2.  McHale
       |3.  Parish""".stripMargin,
      """<ol>
      |<li>Bird</li>
      |<li>McHale</li>
      |<li>Parish</li>
      |</ol>""".stripMargin
    ), //Complex Mardown
    (""" "tagString$tag  [ ] < > list" """, """<p>&quot;tagString$tag  [ ] &lt; &gt; (link:list)&quot;</p>""")

  )

  val falseCases = Seq( //Test cases that should not pass
    ("""escape Math \[ x^2 *r* 1 < 4 > 7 \]""", """<p>escape Math <code>\[ x^2 *r* 1 &lt; 4 &gt; 7 \]</code></p>"""),
    ("simple String", "simple String"),
    ("""\\ $ cat list $ \\""", """<p>\ $ (link:cat) (link:list) $ \</p>"""),
    ("""\\ $ cat list $ \\""", """<p>\ [ (link:cat) (link:list) ] \</p>""")
  )
  "markDownProcessor" in {

    val testString: String =
      """ cat is going in two the ground, cat is playing """.stripMargin

    testCases.foreach {
      case (inp, out) => MarkDownProcessor.processMarkDown(inp, strList, (s: String) => "(link:" + s + ")").trim must_== out
      case _ => ()
    }
    falseCases.foreach {
      case (inp, out) => MarkDownProcessor.processMarkDown(inp, strList, (s: String) => "(link:" + s + ")").trim must_!= out
      case _ => ()
    }
    MarkDownProcessor.processMarkDown(testString, strList, (s: String) => "(link:" + s + ")").trim must_==
      "<p>(link:cat) is going in two the (link:ground), (link:cat) is playing</p>"
  }
}
