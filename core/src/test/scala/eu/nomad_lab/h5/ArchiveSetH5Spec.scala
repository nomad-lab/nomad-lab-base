/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import org.specs2.mutable.Specification
import collection.mutable
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.H5Lib
import eu.nomad_lab.ref._
import eu.nomad_lab.CompactSha
import eu.nomad_lab.JsonSupport
import com.typesafe.scalalogging.StrictLogging
import org.{ json4s => jn }
import scala.collection.breakOut

object ArchiveSetH5Spec extends Specification with StrictLogging {
  skipAllUnless(H5Lib.readSupport)

  lazy val nArchivePath = Paths.get("src/test/testFiles/normalized/prod-016")
  lazy val nArchive = ArchiveSetH5(
    basePath = nArchivePath,
    hasPrefixDir = true,
    writable = false,
    metaInfoEnv0 = None,
    filePrefix = "N"
  )

  def lookupTest() = {
    val arch = nArchive.acquireArchiveFromRef(ArchiveRef("R-fPi7pA1WE8LOShgEBR3TgCvgeFn"))
    val archRef = arch.toRef
    val arch2 = nArchive.acquireArchiveFromRef(archRef)
    var hasSome: Boolean = false
    arch must_== arch2
    nArchive.giveBackArchive(arch2)
    for (calc <- arch.calculations) {
      val calcRef = calc.toRef
      val calc2 = nArchive.acquireCalculationFromRef(calcRef)
      calc must_== calc2
      nArchive.giveBackCalculation(calc2)
      for (sectionTable <- calc.subSectionTables()) {
        val sectRef = sectionTable.toRef
        val sectTable2 = nArchive.acquireSectionTableFromRef(sectRef)
        sectTable2 must_== sectionTable
        nArchive.giveBackSectionTable(sectTable2)
        for (section <- sectionTable) {
          val sectionRef = section.toRef
          val section2 = nArchive.acquireSectionFromRef(sectionRef)
          section must_== section2
          section2.isMissing must_== false
          nArchive.giveBackSection(section2)
        }

        for (values <- sectionTable.subValueTables()) {
          hasSome = true
          //logger.info(s"${values.toRef.toUriStr(ObjectKind.NormalizedData)} -> ${values.length}")
        }
      }
    }
    nArchive.giveBackArchive(arch)
    hasSome must_== true
  }

  def normalizationTest() = {
    val tmpDir = Files.createTempDirectory("normalizationTest")
    lazy val nArchivePath = Paths.get("src/test/testFiles/normalized/prod-016")
    lazy val nArchive = ArchiveSetH5(
      basePath = nArchivePath,
      hasPrefixDir = true,
      writable = false,
      metaInfoEnv0 = None,
      filePrefix = "N"
    )
    lazy val normWriteSet = ArchiveSetH5(
      basePath = tmpDir,
      hasPrefixDir = true,
      writable = true,
      metaInfoEnv0 = Some(nArchive.metaInfoEnv),
      filePrefix = "N",
      superArchiveSet = Some(nArchive)
    )
    val arch = normWriteSet.acquireArchiveFromRef(ArchiveRef("R-fPi7pA1WE8LOShgEBR3TgCvgeFn"))
    var nSysTot: Int = 0
    for (calc <- arch.calculations) {
      val checksums = mutable.Map[Long, String]()
      val refChecksums = Map(((0: Long) -> "sy7KQPzZHNOws62ScGIcSqBHFsPCh"))
      for (sect <- calc.sectionTable(Seq("section_run", "section_system"))) {
        //logger.info(s"$sect")
        sect.maybeValue("atom_labels") match {
          case None => logger.warn(s"${sect.toRef.toUriStr(ObjectKind.NormalizedData)} missing atom_label")
          case Some(labels) =>
            sect.maybeValue("atom_positions") match {
              case None => logger.warn(s"${sect.toRef.toUriStr(ObjectKind.NormalizedData)} missing atom_positions")
              case Some(positions) =>
                val cell = sect.maybeValue("simulation_cell") match {
                  case None => jn.JNothing
                  case Some(c) => c.jValue
                }
                val rep = sect.maybeValue("configuration_periodic_dimensions") match {
                  case Some(pDim) =>
                    val a = pDim.arrayValue()
                    Seq(a.getBoolean(0), a.getBoolean(1), a.getBoolean(2))
                  case None =>
                    Seq(false, false, false)
                }
                if (rep.foldLeft(false)(_ || _) && cell == jn.JNothing)
                  logger.warn(s"${sect.toRef.toUriStr(ObjectKind.NormalizedData)} periodic and no cell info")
                val geoJVal = jn.JObject(("atom_labels" -> labels.jValue) ::
                  ("atom_positions" -> positions.jValue) ::
                  ("configuration_periodic_dimensions" -> jn.JArray(rep.map(jn.JBool(_))(breakOut): List[jn.JBool])) ::
                  ("simulation_cell" -> cell) :: Nil)
                val sha = CompactSha()
                JsonSupport.writeNormalizedOutputStream(geoJVal, sha.outputStream)
                val checksum = sha.gidStr("s")
                checksums += (sect.gIndex -> checksum)
                sect.addValue("configuration_raw_gid", jn.JString(checksum))
            }
        }
      }
      var nSys: Int = 0
      val configIds = calc.valueTable(Seq("section_run", "section_system", "configuration_raw_gid"))
      for (configId <- configIds) {
        nSys += 1
        val sId = configId.parentSection.gIndex
        checksums.get(sId) must_== Some(configId.stringValue)
      }
      nSysTot += nSys
      nSys must_== checksums.size
      checksums == refChecksums
    }
    nSysTot must_== 1
  }

  "ArchiveSet" >> {
    "lookupTest" >> { lookupTest() }
    "normalizationTest" >> { normalizationTest() }
  }
}
