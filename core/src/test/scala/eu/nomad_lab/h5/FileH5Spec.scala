/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import org.specs2.mutable.Specification
import java.nio.file.Paths
import eu.nomad_lab.H5Lib

object FileH5Spec extends Specification {
  skipAllUnless(H5Lib.readSupport)

  lazy val fileH5 = FileH5.open(filePath = Paths.get("src/test/testFiles/Pwm-y8fa439uxcRW0hxcwTqQW8LP6.h5"))
  lazy val archiveH5 = fileH5.openArchive("R1aNcTgAUaWYzTkPBH0o3QOngzLqC")
  lazy val calculation = archiveH5.openCalculation("Cwm-y8fa439uxcRW0hxcwTqQW8LP6")

  def testExtract() = {
    val section_run: SectionTableH5 = calculation.subSectionTable("section_run")
    val firstRun = section_run(0)
    firstRun.isMissing must_== false
    val singleConfInFirstRun = firstRun.subSectionCollection("section_single_configuration_calculation")
    singleConfInFirstRun.isEmpty must_== false
    val singleConfTable = firstRun.table.subSectionTable("section_single_configuration_calculation")
    val firstConf = singleConfTable(0)
    singleConfInFirstRun(0) must_== firstConf
    val energies = firstConf.table.subValueTable("energy_total")
    val energy = energies(0)
    val en2 = firstConf.valueCollection("energy_total")
    energy.isEmpty must_== false
    energy.doubleValue must_== -7.809771684118892E-19
    en2.isEmpty must_== false
    en2.head must_== energy
  }

  def testEmpty() = {
    val section_run: SectionTableH5 = calculation.subSectionTable("section_run")
    val firstRun = section_run(1)
    firstRun.isMissing must_== true
    val singleConfInFirstRun = firstRun.subSectionCollection("section_single_configuration_calculation")
    singleConfInFirstRun.isEmpty must_== true
    val firstConf = singleConfInFirstRun.first
    val en2 = firstConf.valueCollection("energy_total")
    en2.isEmpty must_== true
  }

  def testIterator() = {
    val section_run: SectionTableH5 = calculation.subSectionTable("section_run")
    val fileH5i = FileH5.open(filePath = Paths.get("src/test/testFiles/Pwm-y8fa439uxcRW0hxcwTqQW8LP6.h5"))
    var visited: Boolean = false
    for (archH5 <- fileH5i.archives()) {
      archH5 must_== archiveH5
      for (calcH5 <- archH5.calculations()) {
        calcH5 must_== calculation
        for (secT <- calcH5.subSectionTables()) {
          visited = true
          secT must_== section_run
        }
      }
    }
    fileH5i.release()
    visited must_== true
  }
  def testReferenceCounting() = {
    val fileRH5 = FileH5.open(filePath = Paths.get("src/test/testFiles/Pwm-y8fa439uxcRW0hxcwTqQW8LP6.h5"))
    val archiveRH5 = fileRH5.openArchive("R1aNcTgAUaWYzTkPBH0o3QOngzLqC")
    val calculationR = archiveRH5.openCalculation("Cwm-y8fa439uxcRW0hxcwTqQW8LP6")
    fileRH5.refCount must_== 2
    archiveRH5.refCount must_== 2
    calculationR.refCount must_== 1
    val section_run: SectionTableH5 = calculationR.subSectionTable("section_run")
    calculationR.refCount must_== 1
    calculationR.release()
    fileRH5.refCount must_== 2
    archiveRH5.refCount must_== 1
    calculationR.refCount must_== 0
    archiveRH5.release()
    fileRH5.refCount must_== 1
    archiveRH5.refCount must_== 0
    calculationR.refCount must_== 0
    fileRH5.release()
    fileRH5.refCount must_== 0
    archiveRH5.refCount must_== 0
    calculationR.refCount must_== 0
  }
  def testJValue() = {
    //    println(fileH5.toJValue)
  }

  "h5Tests" >> {
    "testExtract" >> { testExtract() }
    "testEmpty" >> { testJValue(); testEmpty() }
    "testIterator" >> { testIterator() }
    "testReferenceCounting" >> { testReferenceCounting() }
  }

}
