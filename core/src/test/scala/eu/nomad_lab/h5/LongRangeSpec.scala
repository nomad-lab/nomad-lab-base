/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import org.specs2.mutable.Specification
import collection.mutable

object LongRangeSpec extends Specification {

  "LongRange" >> {
    val r1 = LongRange(3L)
    val i1 = r1.iterator
    for (i <- 0 until 3) {
      i1.hasNext must_== true
      i1.next must_== i.toLong
    }
    i1.hasNext must_== false
    var ii: Long = 0L
    r1.foreach { v: Long =>
      v must_== ii
      ii += 1L
    }
    ii must_== 3L
  }
}
