/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import org.specs2.mutable.Specification
import ucar.ma2

object SectionH5Spec extends Specification {
  val values = Array(0, 1, 2, 2, 2, 4, 4, 5, 6, 7)
  val arr = new ma2.ArrayLong.D1(10)
  for (i <- values.indices)
    arr.setLong(i, values(i))

  val singleKeyLowerIndex = SectionH5.lowerGIndexSearch(arr, 5, None, None)
  val singleKeyUpperIndex = SectionH5.upperGIndexSearch(arr, 5, None, None)
  val noLowKeyLowerIndex = SectionH5.lowerGIndexSearch(arr, -12, None, None)
  val noLowKeyUpperIndex = SectionH5.upperGIndexSearch(arr, -12, None, None)
  val noMidKeyLowerIndex = SectionH5.lowerGIndexSearch(arr, 3, None, None)
  val noMidKeyUpperIndex = SectionH5.upperGIndexSearch(arr, 3, None, None)
  val noHighKeyLowerIndex = SectionH5.lowerGIndexSearch(arr, 21, None, None)
  val noHighKeyUpperIndex = SectionH5.upperGIndexSearch(arr, 21, None, None)
  val multiKeyLowerIndex = SectionH5.lowerGIndexSearch(arr, 2, None, None)
  val multiKeyUpperIndex = SectionH5.upperGIndexSearch(arr, 2, None, None)
  val firstEleLowerIndex = SectionH5.lowerGIndexSearch(arr, 0, None, None)
  val firstEleUpperIndex = SectionH5.upperGIndexSearch(arr, 0, None, None)
  val lastEleLowerIndex = SectionH5.lowerGIndexSearch(arr, 7, None, None)
  val lastEleUpperIndex = SectionH5.upperGIndexSearch(arr, 7, None, None)

  "binary search" should {
    "return same lower and upper index for single occurrence of key" in {
      println(s"size:${arr.getSize}, 7th ele(also key): ${arr.getLong(7)}")
      singleKeyLowerIndex must_== singleKeyUpperIndex
      singleKeyLowerIndex must_== 7
    }
    "return correct lower and upper(lower greater than upper) index for no occurrence of key thats lower then lowest key in list" in {
      println(s"noLowKeyLowerIndex:$noLowKeyLowerIndex, noLowKeyUpperIndex:$noLowKeyUpperIndex")
      noLowKeyLowerIndex must_== (0)
      noLowKeyUpperIndex must_== (-1)
    }
    "return correct lower and upper(lower greater than upper) index for no occurrence of key that's greater than lowest key but lower than highest key in list" in {
      println(s"noMidKeyLowerIndex:$noMidKeyLowerIndex, noMidKeyUpperIndex:$noMidKeyUpperIndex")
      noMidKeyLowerIndex must_== (5)
      noMidKeyUpperIndex must_== (4)
    }
    "return correct lower and upper(lower greater than upper) index for no occurrence of key thats greater highest key in list" in {
      println(s"noHighKeyLowerIndex:$noHighKeyLowerIndex, noHighKeyUpperIndex:$noHighKeyUpperIndex")
      noHighKeyLowerIndex must_== (10)
      noHighKeyUpperIndex must_== (9)
    }
    "return correct lower and upper index for multi occurrence of key" in {
      println(s"multiKeyLowerIndex:$multiKeyLowerIndex, multiKeyUpperIndex:$multiKeyUpperIndex")
      multiKeyLowerIndex must_== (2)
      multiKeyUpperIndex must_== (4)
    }
    "return correct lower and upper index for first Element" in {
      println(s"firstEleLowerIndex:$firstEleLowerIndex, firstEleUpperIndex:$firstEleUpperIndex")
      firstEleLowerIndex must_== (0)
      firstEleUpperIndex must_== (0)
    }
    "return correct lower and upper index for last Element" in {
      println(s"lastEleLowerIndex:$lastEleLowerIndex, lastEleUpperIndex:$lastEleUpperIndex")
      lastEleLowerIndex must_== (arr.getSize - 1)
      lastEleUpperIndex must_== (arr.getSize - 1)
    }

  }
}
