/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets

/**
 * Specification (fixed tests) for MetaInfo serialization
 */
class Base32Spec extends Specification {
  val b32Samples: Seq[Long] = Seq(0L, -1L, 34L, 7890L, -2345L)

  "b32Nr RFC4648Uppercase" >> {
    Base32.b32Nr(0, Base32.RFC4648Uppercase) must_== "A"
    Base32.b32Nr(1, Base32.RFC4648Uppercase) must_== "B"
    Base32.b32Nr(33, Base32.RFC4648Uppercase) must_== "BB"
    Base32.b32Nr(-1, Base32.RFC4648Uppercase) must_== "777777777777P"
  }
  "b32Nr RFC4648Lowercase" >> {
    Base32.b32Nr(0, Base32.RFC4648Lowercase) must_== "a"
    Base32.b32Nr(1, Base32.RFC4648Lowercase) must_== "b"
    Base32.b32Nr(33, Base32.RFC4648Lowercase) must_== "bb"
    Base32.b32Nr(-1, Base32.RFC4648Lowercase) must_== "777777777777p"
  }
  "b32Nr Base32Lower" >> {
    Base32.b32Nr(0, Base32.Base32Lower) must_== "0"
    Base32.b32Nr(1, Base32.Base32Lower) must_== "1"
    Base32.b32Nr(33, Base32.Base32Lower) must_== "11"

    Base32.b32Nr(-1, Base32.Base32Lower) must_== "vvvvvvvvvvvvf"
    Base32.b32Nr(1001, Base32.Base32Lower) must_== "9v"
    Base32.b32Nr(1002, Base32.Base32Lower) must_== "av"
    Base32.b32Nr(1003, Base32.Base32Lower) must_== "bv"
  }

  "b32DecodeLong" >> {
    examplesBlock {
      for ((l, i) <- b32Samples.zipWithIndex) {
        "decode RFC4648Uppercase" + i in {
          Base32.b32DecodeLong(Base32.b32Nr(l, Base32.RFC4648Uppercase), Base32.RFC4648Uppercase) must_== l
        }
        "decode RFC4648Lowercase" + i in {
          Base32.b32DecodeLong(Base32.b32Nr(l, Base32.RFC4648Lowercase), Base32.RFC4648Lowercase) must_== l
        }
        "decode Base32Lower" + i in {
          Base32.b32DecodeLong(Base32.b32Nr(l, Base32.Base32Lower), Base32.Base32Lower) must_== l
        }
      }
    }
  }

  "b32DecodeLongRepository" >> {
    examplesBlock {
      for ((l, i) <- b32Samples.zipWithIndex) {
        "decode Base32Lower" + i in {
          Base32.b32DecodeLongRepository(Base32.b32NrRepository(l)) must_== l
        }
      }
    }
  }

}
