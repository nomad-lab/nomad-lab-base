/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import org.specs2.mutable.Specification
import collection.mutable
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.H5Lib
import eu.nomad_lab.ref._
import eu.nomad_lab.h5._
import eu.nomad_lab.CompactSha
import eu.nomad_lab.resolve._
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.JsonSupport
import com.typesafe.scalalogging.StrictLogging
import org.{ json4s => jn }
import scala.collection.breakOut

object BackendH5Spec extends Specification with StrictLogging {
  skipAllUnless(H5Lib.readSupport && H5Lib.writeSupport)

  lazy val inArchivePath = Paths.get("src/test/testFiles/normalized/prod-016")
  lazy val inArchive = ArchiveSetH5(
    basePath = inArchivePath,
    hasPrefixDir = true,
    writable = false,
    metaInfoEnv0 = None,
    filePrefix = "N"
  )
  lazy val outArchivePath = Files.createTempDirectory("NormalizationTest", LocalEnv.directoryPermissionsAttributes)
  lazy val outArchive = ArchiveSetH5(
    basePath = outArchivePath,
    hasPrefixDir = true,
    writable = true,
    metaInfoEnv0 = None,
    filePrefix = "N",
    superArchiveSet = Some(inArchive)
  )

  lazy val backend = new BackendH5(outArchive)

  def addChecksumTest() = {
    val sysUri = "nmd://N0jqPXU7FP2SaaH8sMvXrW0DZsK0Z/CGivpJxhUx8d3FzFw7jHDjE51ZlOK/section_run/section_system/0c"
    val sysRef = NomadUri(sysUri).toRef
    val calcRef = NomadUri("nmd://N0jqPXU7FP2SaaH8sMvXrW0DZsK0Z/CGivpJxhUx8d3FzFw7jHDjE51ZlOK").toRef match {
      case c: CalculationRef => c
      case _ => throw new Exception("expected a calculationRef")
    }
    backend.openCalculation(calcRef)
    backend.startedParsingSession(
      mainFileUri = None,
      parserInfo = jn.JObject(
        ("parserId" -> jn.JString("testParser")) :: Nil
      )
    )
    backend.openContext(sysRef)
    backend.addValue("configuration_raw_gid", jn.JString("pippo_pippo"))
    backend.closeContext(sysRef)
    backend.openContext(calcRef)
    val sstats = backend.openSection("section_stats")
    val spp = backend.openSection("section_stats_per_parser")
    backend.addValue("stats_per_parser_parser_id", jn.JString("pippo_pippo_v1"))
    backend.closeSection("section_stats_per_parser", spp)
    backend.closeSection("section_stats", sstats)
    backend.closeContext(calcRef)
    backend.finishedParsingSession(
      parserStatus = Some(ParseResult.ParseSuccess),
      parserErrors = jn.JNothing
    )
    backend.closeCalculation(calcRef)
    val confGid = Resolver.resolveInArchiveSet(outArchive, NomadUri(sysUri + "/configuration_raw_gid/0l").toRef)
    val pid = Resolver.resolveInArchiveSet(outArchive, NomadUri("nmd://N0jqPXU7FP2SaaH8sMvXrW0DZsK0Z/CGivpJxhUx8d3FzFw7jHDjE51ZlOK/section_processor_info/processor_id/0c").toRef)
    confGid match {
      case Value(_, v) =>
        t
        v.stringValue must_== "pippo_pippo"
      case v =>
        v.toString must_== "StringValue(pippo_pippo)"
    }
    confGid.giveBack()
    val res = pid match {
      case Value(_, v) =>
        v.stringValue must_== "testParser"
      case v =>
        v.toString must_== "StringValue(pippo_pippo_v1)"
    }
    pid.giveBack()
    res
  }

  "BackedH5" >> {
    "normalizationTest" >> { addChecksumTest() }
  }
}
