/** to build the first time you need to execute
  * jooq:codegen which depends on flywayMigrate to generate the jooq db code
  */
scalaVersion  := "2.11.8"
scalaVersion in ThisBuild := "2.11.8"
// # libs

// ## input configuration
val configLib     = "com.typesafe"        % "config"         % "1.2.1"

lazy val FullTest = config("full") extend(Test)

def testFilter(name: String): Boolean = {
  name.endsWith("Tests") || name.endsWith("Spec")
}

concurrentRestrictions in Global := Seq(
  Tags.limitSum(2, Tags.CPU, Tags.Untagged),
  Tags.limit(Tags.Network, 10),
  Tags.limit(Tags.Test, 1),
  Tags.limitAll( 15 )
)

// caching
val cachingLibs = Seq(
  "com.github.ben-manes.caffeine" % "caffeine" % "2.6.1",
  "com.github.cb372" %% "scalacache-caffeine" % "0.21.0",
  "com.google.code.findbugs" % "jsr305" % "2.0.3"
)

// ## logging libs
val loggingLibs = {
  val scalalogLib      = "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2"
  val log4j2Libs       = Seq(
    "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.10.0",
    "org.apache.logging.log4j" % "log4j-api"        % "2.10.0",
    "org.apache.logging.log4j" % "log4j-core"       % "2.10.0",
    "org.apache.logging.log4j" % "log4j-1.2-api"    % "2.10.0",
    "org.apache.logging.log4j" % "log4j-api-scala_2.11"  % "11.0")
  scalalogLib +: log4j2Libs
}
val commonsLoggingBridgeLib =  "org.apache.logging.log4j" % "log4j-jcl"    % "2.5"

// ## test libs
val specs2Lib        = "org.specs2"         %% "specs2-core"    % "2.3.11"
val scalacheckLib    = "org.scalacheck"     %% "scalacheck"     % "1.12.4"
val scalatestLib     = "org.scalatest" % "scalatest_2.11" % "3.0.1"
val mockitoLib       = "org.mockito" % "mockito-core" % "2.18.3"
val testLibs         = {
  Seq(specs2Lib  % "test", scalacheckLib  % "test", scalatestLib % "test", mockitoLib % Test)
}

val fullTestLibs         = {
  Seq(specs2Lib  % FullTest, scalacheckLib  % FullTest)
}

// ## json libs
val json4sNativeLib  = "org.json4s"         %% "json4s-native"  % "3.5.1"
val json4sJacksonLib = "org.json4s"         %% "json4s-jackson" % "3.5.1"
val json4sExtLib = "org.json4s"             %% "json4s-ext"     % "3.5.1"

// ## Hadoop libs
val hadoopLib = "org.apache.hadoop" % "hadoop-client" % "2.7.3" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12")

// ## Parquet libs
val parquetLib = Seq(
  "org.apache.parquet" % "parquet-common" % "1.9.0" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  "org.apache.parquet" % "parquet-encoding" % "1.9.0" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  "org.apache.parquet" % "parquet-column" % "1.9.0" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  //"org.apache.parquet" % "parquet" % "1.9.0" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  "org.apache.parquet" % "parquet-hadoop" % "1.9.0"  exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12")
)

// ## avro (binary serialization)
val avroLib = "org.apache.avro" % "avro" % "1.8.1"

// ## mime type recognition lib
val tikaLib          = "org.apache.tika"     % "tika-core"       % "1.10"

// ## compression handling libs
val compressionLibs = {
  val commonsCompressLib = "org.apache.commons"  % "commons-compress" % "1.10"
  val xzForJavaLib       = "org.tukaani"         % "xz"               % "1.5"
  Seq(commonsCompressLib, xzForJavaLib)
}

// ## archive management libs
val bagitLib = "gov.loc" % "bagit" % "4.11.0" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12") exclude("commons-logging", "commons-logging") exclude("commons-logging", "commons-logging-api")

// ## queuing libs
//val kafkaLib         = "org.apache.kafka"   %% "kafka"           % "0.9.0.0" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12")
val rabbitmqLib      = "com.rabbitmq"        % "amqp-client"      %"5.2.0"

val flinkResolvers = Seq("Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/", Resolver.mavenLocal)
val flinkVersion = "1.3.0"
val flinkDependencies = Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion, // exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  "org.apache.flink" %% "flink-clients" % flinkVersion) // , exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
//  "org.apache.flink" %% "flink-connector-rabbitmq" % flinkVersion % "provided", //exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"))
val flinkDependenciesProv = Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided", // exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  "org.apache.flink" %% "flink-clients" % flinkVersion % "provided") // , exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),


// ## markdown interpreter lib
val flexmarkLib      = "com.vladsch.flexmark" % "flexmark-all" % "0.19.8"

// ## spray and akka (web service and actors)
lazy val sprayLibs = {
  val akkaV = "2.3.12"
  val sprayV = "1.3.3"
  Seq(
    "io.spray"            %%  "spray-caching" % sprayV,
    "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-testkit" % sprayV  % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test")
}

lazy val akkaV = "2.5.3"
lazy val akkaStreamLibs = {
  Seq(
    "com.typesafe.akka" %% "akka-stream" % akkaV
  )
}

lazy val akkaStreamTestLibs = {
  Seq(
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaV % Test
  )
}


lazy val akkaHttpLibs = {
  val akkaHttpV = "10.0.9"
  val akkaSwaggerV = "0.13.0"
  Seq(
    "com.typesafe.akka" %% "akka-http"            % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpV,
    "com.github.swagger-akka-http" %% "swagger-akka-http" % akkaSwaggerV//,

    //"com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV % Test,
    //"org.scalatest"     %% "scalatest"         % "3.0.1"         % Test
  ) ++ akkaStreamLibs
}

val akkaRabbitmqResolver = "The New Motion Public Repo" at "http://nexus.thenewmotion.com/content/groups/public/"
lazy val akkaRabbitmq = "com.thenewmotion.akka" %% "akka-rabbitmq" % "1.2.7"


// ## db libs
val h2Lib            = "com.h2database"      % "h2"             % "1.4.196"
val postgresLib      = "org.postgresql"      % "postgresql"     % "42.1.1"
val flywayLib        = "org.flywaydb"        % "flyway-core"    % "4.2.0"
val jooqLibVersion   = "3.9.3"
val jooqLib          = "org.jooq"            % "jooq"           % jooqLibVersion
val jooqMetaLib      = "org.jooq"            % "jooq-meta"      % jooqLibVersion

// ## elastic search libraries
val elastic4sVersion = "5.5.4"
val elastic4sCoreLib = "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion
val elastic4sTcpLib  = "com.sksamuel.elastic4s" %% "elastic4s-tcp" % elastic4sVersion
val elastic4sHttpLib = "com.sksamuel.elastic4s" %% "elastic4s-http" % elastic4sVersion
val elastic4sStreams = "com.sksamuel.elastic4s" %% "elastic4s-streams" % elastic4sVersion
val elastic4sJson4s = "com.sksamuel.elastic4s" %% "elastic4s-json4s" % elastic4sVersion
val elastic4sTestingLibs = Seq(
  "com.sksamuel.elastic4s" %% "elastic4s-testkit" % elastic4sVersion % "test",
  "com.sksamuel.elastic4s" %% "elastic4s-embedded" % elastic4sVersion % "test")

val elastic4sLibs = Seq(
  elastic4sCoreLib,
  elastic4sHttpLib,
  elastic4sJson4s
)

// discarded libs
// val re2j          = "com.google.re2j"     % "re2j"            % "1.0" // faster regexps
// val fastring      = "com.dongxiguo"      %% "fastring"        % "0.2.4" // faster string templates
// val playJson      = "com.typesafe.play"  %% "play-json"       % "2.4.3" // json4s alternative
val netcdfLibs        = Seq(
    "edu.ucar"            % "netcdf4"         % "4.6.10" exclude("net.java.dev.jna","jna"),
    "org.elasticsearch" % "jna" % "4.4.0"
  ) // pure java netcdf lib (writes only netcdf3, used only for ucar.ma2)

val versionRe = """v([0-9]+(?:\.[0-9]+(?:\.[0-9]+)?)?)-?(.*)?""".r

lazy val commonSettings = Seq(
concurrentRestrictions in Global := Seq(
  Tags.limitSum(2, Tags.CPU, Tags.Untagged),
  Tags.limit(Tags.Network, 10),
  Tags.limit(Tags.Test, 1),
  Tags.limitAll( 15 )
),
  organization  := "eu.nomad-laboratory",
  scalaVersion  := "2.11.8",
  scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-feature"),
  resolvers     += "netcdf releases" at "https://artifacts.unidata.ucar.edu/content/repositories/unidata-releases",
  libraryDependencies ++= testLibs,
  libraryDependencies ++= loggingLibs,
  fork in run   := true,
  fork in Test  := true
)

val dockerRegistry = "analytics-toolkit.nomad-coe.eu:5509"
val dockerProjectName = "nomadlab"
lazy val gitVersionSettings = Seq(
  buildInfoPackage := "eu.nomad_lab",
  buildInfoOptions += BuildInfoOption.ToMap,
  buildInfoObject := name.value.capitalize + "VersionInfo",
  version := {
    val gitV: String = Process("git" :: "describe" :: "--tags" :: "--dirty" :: "--always" :: Nil, baseDirectory.value) !!;
    gitV.trim
  },
  buildInfoKeys := Seq[BuildInfoKey](
    name,
    version,
    scalaVersion,
    sbtVersion,
    BuildInfoKey.action("buildTime") {
      System.currentTimeMillis
    })
)

lazy val hdf5Support = (project in file("hdf5/hdf5-support")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        netcdfLibs ++:
        json4sNativeLib +:
        loggingLibs),
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "lib").toString()),
    name := "hdf5Support"
  )


lazy val hdf5Placeholder = (project in file("hdf5/hdf5-placeholder")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        netcdfLibs ++:
        loggingLibs),
    name := "hdf5Placeholder"
  )

val hdf5Wrapper = if (file("hdf5/hdf5-support/lib/libhdf5_java.so").canRead() ||
    file("hdf5/hdf5-support/lib/libhdf5_java.dylib").canRead())
  hdf5Support
else
  hdf5Placeholder

lazy val core = (project in file("core")).
  dependsOn(hdf5Wrapper).
  enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoPackage := "eu.nomad_lab",
    buildInfoOptions += BuildInfoOption.ToMap,
    buildInfoObject := name.value.capitalize + "VersionInfo",
    version := {
      val gitV: String = Process("git" :: "describe" :: "--tags" :: "--dirty" :: "--always" :: Nil, baseDirectory.value) !!;
      gitV.trim
    },
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      BuildInfoKey.action("metaInfoVersion") {
        val gitV: String = Process("git" :: "describe" :: "--tags" :: "--dirty" :: "--always" :: Nil, (baseDirectory.value / "../nomad-meta-info").getCanonicalFile) !!;
        gitV.trim
      },
      BuildInfoKey.action("pythonCommonVersion") {
        val gitV: String = Process("git" :: "describe" :: "--tags" :: "--dirty" :: "--always" :: Nil, (baseDirectory.value / "../python-common").getCanonicalFile)!!;
        gitV.trim
      },
      scalaVersion,
      sbtVersion,
      BuildInfoKey.action("buildTime") {
        System.currentTimeMillis
      })
  ).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        json4sNativeLib +:
        json4sJacksonLib +:
        json4sExtLib +:
        flexmarkLib +:
        tikaLib +:
	avroLib +:
        netcdfLibs ++:
        compressionLibs ++:
        cachingLibs ++:
        loggingLibs),
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString()),
    name := "nomadCore",
    (unmanagedResourceDirectories in Compile) += (baseDirectory.value / "../nomad-meta-info/meta_info").getCanonicalFile(),
    (unmanagedResourceDirectories in Compile) += (baseDirectory.value / "../python-common/common").getCanonicalFile()
  )

lazy val jooqCommon = Seq(jooqSettings:_*) ++ Seq(
  jooqVersion := jooqLibVersion
)

// general db support (oriented a bit toward postgres dbs)
lazy val dbSupport = (project in file("dbs/db-support")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "dbSupport"
  )

// local database settings (using embedded h2), currently not used
/*
lazy val rdbUrl = settingKey[String]("url to the rdb used during building")

//libraryDependencies ++= loggingLibs

lazy val rdbGen = (project in file("dbs/rdb-gen")).
  settings(commonSettings: _*).
  settings(
    name := "rdbGen"
  ).
  settings(flywaySettings: _*).
  settings(jooqCommon: _*).
  settings(
    rdbUrl := {
    "jdbc:h2:file:" + ((resourceManaged in Compile).value / "localdb_h2")
  } ).
  settings(
    flywayUrl := rdbUrl.value,
    flywayLocations := Seq( "classpath:rdb/migrations")
  ).
  settings(jooqCommon: _*).
  settings(
    (codegen in JOOQ) := { (codegen in JOOQ).dependsOn(flywayMigrate).value },
    libraryDependencies +=  h2Lib % "jooq",
    jooqOptions := Seq(
      "jdbc.driver" -> "org.h2.Driver",
      "jdbc.url" -> rdbUrl.value,
      "generator.database.name" -> "org.jooq.util.h2.H2Database",
      "generator.database.inputSchema" -> "PUBLIC",
      "generator.database.outputSchemaToDefault" -> "true",
      "generator.database.includes" -> ".*",
      "generator.target.packageName" -> "eu.nomad_lab.rdb.db",
      "generator.generate.pojos" -> "false",
      "generator.generate.immutablePojos" -> "false",
      "generator.generate.deprecated" -> "false"
    ),
    jooqOutputDirectory := (baseDirectory.value / "../rdb/src/main/java").getCanonicalFile()
  )

lazy val rdb = (project in file("dbs/rdb")).
  settings(commonSettings: _*).
  settings(
    name := "rdb",
    libraryDependencies ++= (
      configLib +:
        h2Lib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    (unmanagedResourceDirectories in Compile) += (baseDirectory.value / "../rdb-gen/src/main/resources").getCanonicalFile()
  )
 */

// interface to the raw data database

//val rawDataUrl = "jdbc:postgresql://localhost:5432/rawdata"
val rawDataUrl = "jdbc:postgresql://172.17.0.3:5432/rawdata"
val rawDataUser = "rawdata"
val rawDataPassword = "pippo"
val rawDataDriver = "org.postgresql.Driver"

lazy val rawDataDbGen = (project in file("dbs/raw-data-db-gen")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "rawDataDbGen"
  ).
  settings(flywaySettings: _*).
  settings(
    flywayUrl := rawDataUrl,
    flywayUser := rawDataUser,
    flywayPassword := rawDataPassword,
    flywayLocations := Seq( "classpath:rawdatadb/migrations"),
    flywayDriver := rawDataDriver
  ).
  settings(jooqCommon: _*).
  settings(
    (codegen in JOOQ) := { (codegen in JOOQ).dependsOn(flywayMigrate).value },
    libraryDependencies += postgresLib % "jooq",
    jooqOptions := Seq(
      "jdbc.driver" -> rawDataDriver,
      "jdbc.url" -> rawDataUrl,
      "jdbc.user" -> rawDataUser,
      "jdbc.password" -> rawDataPassword,
      "generator.database.name" -> "org.jooq.util.postgres.PostgresDatabase",
      "generator.database.inputSchema" -> "public",
      "generator.database.outputSchemaToDefault" -> "true",
      "generator.database.includes" -> ".*",
      "generator.target.packageName" -> "eu.nomad_lab.rawdata.db",
      "generator.generate.pojos" -> "false",
      "generator.generate.immutablePojos" -> "false",
      "generator.generate.deprecated" -> "false"
    ),
    jooqOutputDirectory := (baseDirectory.value / "../raw-data-db/src/main/java").getCanonicalFile()
  )

lazy val rawDataDb = (project in file("dbs/raw-data-db")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "rawDataDb"
  ).
  settings(
        (unmanagedResourceDirectories in Compile) += (baseDirectory.value / "../raw-data-db-gen/src/main/resources").getCanonicalFile()
  )

// interface to the repository db
//val repoUrl = "jdbc:postgresql://db-repository-nomad.esc.rzg.mpg.de:5432/nomad_prod"
val repoUrl = "jdbc:postgresql://172.17.0.5:5432/repo"
//val repoUser = "nomadlab"
val repoUser = "repo"
val repoPassword = "pippo"
val repoDriver = "org.postgresql.Driver"

lazy val repoDbGen = (project in file("dbs/repo-db-gen")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "repoDbGen"
  ).
  settings(jooqCommon: _*).
  settings(
    libraryDependencies += postgresLib % "jooq",
    jooqOptions := Seq(
      "jdbc.driver" -> repoDriver,
      "jdbc.url" -> repoUrl,
      "jdbc.user" -> repoUser,
      "jdbc.password" -> repoPassword,
      "generator.database.name" -> "org.jooq.util.postgres.PostgresDatabase",
      "generator.database.inputSchema" -> "public",
      "generator.database.outputSchemaToDefault" -> "true",
      "generator.database.includes" -> ".*",
      "generator.target.packageName" -> "eu.nomad_lab.repo.db",
      "generator.generate.pojos" -> "false",
      "generator.generate.immutablePojos" -> "false",
      "generator.generate.deprecated" -> "false"
    ),
    jooqOutputDirectory := (baseDirectory.value / "../repo-db/src/main/java").getCanonicalFile()
  )

lazy val repoDb = (project in file("dbs/repo-db")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "repoDb"
  )

// interface to the parsing_stats database

//val parsingStatsUrl = "jdbc:postgresql://localhost:5435/parsing_stats"
val parsingStatsUrl = "jdbc:postgresql://172.17.0.6:5432/stats"
//val parsingStatsUser = "parsing_stats"
val parsingStatsUser = "stats"
val parsingStatsPassword = "pippo"
val parsingStatsDriver = "org.postgresql.Driver"

lazy val parsingStatsDbGen = (project in file("dbs/parsing-stats-db-gen")).
  dependsOn(dbSupport).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "parsingStatsDbGen"
  ).
  settings(flywaySettings: _*).
  settings(
    flywayUrl := parsingStatsUrl,
    flywayUser := parsingStatsUser,
    flywayPassword := parsingStatsPassword,
    flywayLocations := Seq( "classpath:parsing-stats-db/migrations"),
    flywayDriver := parsingStatsDriver
  ).
  settings(jooqCommon: _*).
  settings(
    (codegen in JOOQ) := { (codegen in JOOQ).dependsOn(flywayMigrate).value },
    libraryDependencies += postgresLib % "jooq",
    jooqOptions := Seq(
      "jdbc.driver" -> parsingStatsDriver,
      "jdbc.url" -> parsingStatsUrl,
      "jdbc.user" -> parsingStatsUser,
      "jdbc.password" -> parsingStatsPassword,
      "generator.database.name" -> "org.jooq.util.postgres.PostgresDatabase",
      "generator.database.inputSchema" -> "public",
      "generator.database.outputSchemaToDefault" -> "true",
      "generator.database.includes" -> ".*",
      "generator.target.packageName" -> "eu.nomad_lab.parsing_stats.db",
      "generator.generate.pojos" -> "false",
      "generator.generate.immutablePojos" -> "false",
      "generator.generate.deprecated" -> "false",
      "generator.database.customTypes.customType.name" -> "JValue",
      "generator.database.customTypes.customType.type" -> "org.json4s.JsonAST.JValue",
      "generator.database.customTypes.customType.binding" -> "eu.nomad_lab.db_support.PostgresJsonbJValueBinding",
      "generator.database.forcedTypes.forcedType.name" -> "JValue",
      "generator.database.forcedTypes.forcedType.types" -> "JSONB"
    ),
    jooqOutputDirectory := (baseDirectory.value / "../parsing-stats-db/src/main/java").getCanonicalFile()
  )

lazy val parsingStatsDb = (project in file("dbs/parsing-stats-db")).
  dependsOn(dbSupport).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        postgresLib +:
        jooqLib +:
        flywayLib +:
        loggingLibs),
    name := "parsingStatsDb"
  ).
  settings(
        (unmanagedResourceDirectories in Compile) += (baseDirectory.value / "../parsing-stats-db-gen/src/main/resources").getCanonicalFile()
  )

// parquet support
lazy val parquet = (project in file("parquet")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        parquetLib ++:
        hadoopLib +:
        loggingLibs),
    name := "parquet"
  )

// ---- parsers ----

lazy val fhiAims = (project in file("parsers/fhi-aims")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "fhiAims",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val abinit = (project in file("parsers/abinit")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "abinit",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val castep = (project in file("parsers/castep")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "castep",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val onetep = (project in file("parsers/onetep")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "onetep",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val cp2k = (project in file("parsers/cp2k")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "cp2k",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val cpmd = (project in file("parsers/cpmd")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "cpmd",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val nwchem = (project in file("parsers/nwchem")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "nwchem",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val bigdft = (project in file("parsers/big-dft")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "bigdft",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val crystal = (project in file("parsers/crystal")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "crystal",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val dlPoly = (project in file("parsers/dl-poly")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "dlPoly",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val libAtoms = (project in file("parsers/lib-atoms")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "libAtoms",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val dmol3 = (project in file("parsers/dmol3")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "dmol3",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val exciting = (project in file("parsers/exciting")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "exciting",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val gaussian = (project in file("parsers/gaussian")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "gaussian",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val gpaw = (project in file("parsers/gpaw")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "gpaw",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val lammps = (project in file("parsers/lammps")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "lammps",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val amber = (project in file("parsers/amber")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "amber",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val gromacs = (project in file("parsers/gromacs")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "gromacs",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val gromos = (project in file("parsers/gromos")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "gromos",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val namd = (project in file("parsers/namd")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "namd",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val charmm = (project in file("parsers/charmm")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "charmm",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val tinker = (project in file("parsers/tinker")).
  dependsOn(core).
  configs(FullTest).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  settings(
    name := "tinker",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val mopac = (project in file("parsers/mopac")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "mopac",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val octopus = (project in file("parsers/octopus")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "octopus",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val orca = (project in file("parsers/orca")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "orca",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val qbox = (project in file("parsers/qbox")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "qbox",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)


lazy val quantumEspresso = (project in file("parsers/quantum-espresso")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "quantumEspresso",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val siesta = (project in file("parsers/siesta")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "siesta",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val turbomole = (project in file("parsers/turbomole")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "turbomole",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser",
    libraryDependencies ++= testLibs
  ).
  settings(Revolver.settings: _*)

lazy val vasp = (project in file("parsers/vasp")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "vasp",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val wien2k = (project in file("parsers/wien2k")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "wien2k",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val asap = (project in file("parsers/asap")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "asap",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val atk = (project in file("parsers/atk")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "atk",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val gamess = (project in file("parsers/gamess")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "gamess",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val elk = (project in file("parsers/elk")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "elk",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val fleur = (project in file("parsers/fleur")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "fleur",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val dftbPlus = (project in file("parsers/dftb-plus")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "dftbPlus",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val gulp = (project in file("parsers/gulp")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "gulp",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val fplo = (project in file("parsers/fplo")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "fplo",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val molcas = (project in file("parsers/molcas")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "molcas",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val phonopy = (project in file("parsers/phonopy")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "phonopy",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val atomicData = (project in file("parsers/atomic-data")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "atomicData",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val qhp = (project in file("parsers/quasi-harmonic-properties")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "qhp",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val elastic = (project in file("parsers/elastic")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "elastic",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "parser"
  ).
  settings(Revolver.settings: _*)

lazy val base = (project in file("base")).
  dependsOn(core).
  dependsOn(fhiAims).
  dependsOn(abinit).
  dependsOn(castep).
  dependsOn(onetep).
  dependsOn(cp2k).
  dependsOn(cpmd).
  dependsOn(nwchem).
  dependsOn(bigdft).
  dependsOn(dlPoly).
  dependsOn(libAtoms).
  dependsOn(gaussian).
  dependsOn(exciting).
  dependsOn(gpaw).
  dependsOn(quantumEspresso).
  dependsOn(lammps).
  dependsOn(amber).
  dependsOn(gromacs).
  dependsOn(gromos).
  dependsOn(namd).
  dependsOn(charmm).
  dependsOn(tinker).
  dependsOn(dmol3).
  dependsOn(vasp).
  dependsOn(wien2k).
  dependsOn(asap).
  dependsOn(atk).
  dependsOn(gamess).
  dependsOn(elk).
  dependsOn(qbox).
  dependsOn(lammps).
  dependsOn(crystal).
  dependsOn(octopus).
  dependsOn(siesta).
  dependsOn(fleur).
  dependsOn(dftbPlus).
  dependsOn(mopac).
  dependsOn(orca).
  dependsOn(turbomole).
  dependsOn(gulp).
  dependsOn(fplo).
  dependsOn(molcas).
  dependsOn(phonopy).
  dependsOn(atomicData).
  dependsOn(qhp).
  dependsOn(elastic).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= loggingLibs,
    name := "nomadBase"
  )

// ---- normalizers ----

lazy val stats = (project in file("normalizers/stats")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "stats",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val fhiAimsBasis = (project in file("normalizers/fhi-aims-basis")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "fhiAimsBasis",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val symmetry = (project in file("normalizers/symmetry")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "symmetry",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val systemType = (project in file("normalizers/system-type")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "systemType",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val uploadInfo = (project in file("normalizers/upload-info")).
  dependsOn(core).
  dependsOn(rawDataDb).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "uploadInfo",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)


lazy val springer = (project in file("normalizers/springer")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "springer",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val repoTags = (project in file("normalizers/repo-tags")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "repoTags",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val prototypesNormalizer = (project in file("normalizers/prototypes")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "prototypes",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val bandStructureNormalizer = (project in file("normalizers/band-structure")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "bandStructure",
    (unmanagedResourceDirectories in Compile) += baseDirectory.value / "normalizer"
  ).
  settings(Revolver.settings: _*)

lazy val normalize = (project in file("normalize")).
  dependsOn(stats).
  dependsOn(base).
  dependsOn(query).
  dependsOn(fhiAimsBasis).
  dependsOn(systemType).
  dependsOn(uploadInfo).
  dependsOn(symmetry).
  dependsOn(springer).
  dependsOn(repoTags).
  dependsOn(prototypesNormalizer).
  dependsOn(bandStructureNormalizer).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= loggingLibs,
    name := "normalize",
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  )

// ---- top level projects

lazy val webservice = (project in file("webservice")).
  dependsOn(core).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    //resolvers ++= flinkResolvers,
    libraryDependencies ++= sprayLibs, // ++ flinkDependencies,
    name := "nomadWebService",
    assemblyJarName in assembly := s"${organization.value.toLowerCase}-${name.value.toLowerCase}.jar",
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")),
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString()),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        expose(8081)
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  ).
  settings(Revolver.settings: _*)

lazy val tool = (project in file("tool")).
  dependsOn(base).
  dependsOn(normalize).
  dependsOn(parsingStatsDb).
  dependsOn(query).
//  dependsOn(parquet).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "nomadTool",
//    resolvers ++= flinkResolvers,
//    libraryDependencies ++= flinkDependencies ++ parquetLib,
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
//      ImageName(
//        namespace = Some(organization.value.toLowerCase),
//        repository = name.value.toLowerCase,
//        tag = Some("v" + version.value)
//      )
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")

    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/tool.jar"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        add(baseDirectory.value / "src/main/docker", "/app")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-Djava.library.path=/lib", "-jar", artifactTargetPath)
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  ).
  settings(Revolver.settings: _*)

lazy val calculationparser = (project in file("calculation-parser-worker")).
  dependsOn(base).
  dependsOn(parsingStatsDb).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "nomadCalculationParserWorker",
    resolvers += akkaRabbitmqResolver,
    libraryDependencies += rabbitmqLib,
    libraryDependencies += akkaRabbitmq,
    libraryDependencies ++= sprayLibs,
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
//      ImageName(
//        namespace = Some(organization.value.toLowerCase),
//        repository = name.value.toLowerCase,
//        tag = Some("v" + version.value)
//      )
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        expose(8081)
        add(artifact, artifactTargetPath)
	add(baseDirectory.value / "src/main/docker/run_many.sh", "/app/run_many.sh")
        //        entryPoint("bash")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  ).
  settings(Revolver.settings: _*)

lazy val normalizerWorker = (project in file("normalizer-worker")).
  dependsOn(normalize).
  settings(commonSettings: _*).
  settings(
    name := "nomadNormalizerWorker",
    mainClass in assembly := Some("eu.nomad_lab.Normalizer"),
    libraryDependencies += rabbitmqLib,
    resolvers ++= flinkResolvers,
    libraryDependencies ++= flinkDependencies,
//    run in Compile := { Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run)).evaluated },
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
  ).
  settings(Revolver.settings: _*)

lazy val query = (project in file("query")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    name := "nomadQuery",
    mainClass in assembly := Some("eu.nomad_lab.query.flink.FlinkAlone"),
    libraryDependencies += rabbitmqLib,
    resolvers ++= flinkResolvers,
    libraryDependencies ++= flinkDependenciesProv,
//    run in Compile := { Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run)).evaluated },
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
  ).
  settings(Revolver.settings: _*)

//elasticsearch
lazy val elasticsearch = (project in file("elasticsearch")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    name := "nomadElasticsearch",
    libraryDependencies := elastic4sLibs
  ).
  settings(Revolver.settings: _*)



lazy val treeparserinitializer = (project in file("tree-parser-initializer")).
  dependsOn(base).
  settings(commonSettings: _*).
  settings(
    name := "treeparserinitializer",
    libraryDependencies += rabbitmqLib
  ).
  settings(Revolver.settings: _*)

lazy val treeparser = (project in file("tree-parser-worker")).
  dependsOn(normalize).
  dependsOn(query).
  dependsOn(base).
  dependsOn(parsingStatsDb).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "nomadTreeParserWorker",
    resolvers += akkaRabbitmqResolver,
    libraryDependencies += rabbitmqLib,
    libraryDependencies += akkaRabbitmq,
    libraryDependencies ++= sprayLibs,
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
//      ImageName(
//        namespace = Some(organization.value),
//        repository = name.value.toLowerCase,
//        tag = Some("v" + version.value)
//      )
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        expose(8081)
        add(artifact, artifactTargetPath)
//        entryPoint("bash")
        entryPoint("java", "-jar", artifactTargetPath,"--worker","TreeParserWorker")
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  ).
  settings(Revolver.settings: _*)

lazy val kubernetes = (project in file("kubernetes")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    name := "kubernetes"
  ).
  settings(Revolver.settings: _*)

lazy val integratedpipeline = (project in file("integrated-pipeline")).
  dependsOn(base).
  dependsOn(parsingStatsDb).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "integratedPipeline",
    resolvers += akkaRabbitmqResolver,
    libraryDependencies += akkaRabbitmq,
    libraryDependencies += rabbitmqLib,
    libraryDependencies ++= sprayLibs,
    libraryDependencies ++= akkaStreamLibs,
    libraryDependencies ++= akkaStreamTestLibs,
    libraryDependencies --= testLibs,
    libraryDependencies ++= Seq(scalatestLib % Test, mockitoLib % Test),
    testOptions in Test += Tests.Argument("-oD"),
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")
//        namespace = Some(organization.value.toLowerCase),
//        repository = Some(dockerRegistry),
//        tag = Some("v" + version.value)
//      )
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        add(artifact, artifactTargetPath)
        //        entryPoint("bash")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())

  ).
  settings(Revolver.settings: _*)

lazy val notebook = (project in file("notebook")).
  dependsOn(base).
  enablePlugins(DockerPlugin).
  enablePlugins(BuildInfoPlugin).
  settings(commonSettings: _*).
  settings(gitVersionSettings: _*).
  settings(
    name := "notebook",
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
	  ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")
      // Sets a name with a tag that contains the project version; namespace/repository:tag
//      ImageName(
//        namespace = Some($dockerProjectName),
//        repository = name.value.toLowerCase,
//        tag = Some("v" + version.value)
//      )
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = "/app/notebook.jar"
      new Dockerfile {
        from("nomadlab/beaker:v1.6.0-22")
        expose(8801)
	add(baseDirectory.value / "../analysis-tools/soap-plus-plus", "/home/beaker/lib/soap-plus-plus")
	run("bash", "-c", "cd /home/beaker/lib/soap-plus-plus; ./build.sh; ln -s /home/beaker/lib/soap-plus-plus/soap /home/beaker/src/core/soap")
        run("pip", "install", "psutil")
        add(baseDirectory.value / "../analysis-tools/encyclopedia/Nomad", "/usr/lib/python2.7/Nomad")
        add(baseDirectory.value / "../analysis-tools/encyclopedia/Nomad", "/home/beaker/py3k/lib/python3.5/Nomad")
        add(baseDirectory.value / "../analysis-tools/structural-similarity/python-modules", "/usr/lib/python2.7")
        add(baseDirectory.value / "../analysis-tools/structural-similarity/python-modules", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "../analysis-tools/delta-ml/python-modules", "/usr/lib/python2.7")
        add(baseDirectory.value / "../analysis-tools/delta-ml/python-modules", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "../python-common/common/python", "/usr/lib/python2.7")
        add(baseDirectory.value / "../python-common/common/python", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "../analysis-tools/atomic-data/atomic_data", "/usr/lib/python2.7/atomic_data")
        add(baseDirectory.value / "../analysis-tools/atomic-data/atomic_data", "/home/beaker/py3k/lib/python3.5/atomic_data")
        add(baseDirectory.value / "../analysis-tools/phase-diagram", "/usr/lib/python2.7")
        add(baseDirectory.value / "../analysis-tools/phase-diagram", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "../analysis-tools/ErrorBars", "/usr/lib/python2.7")
        add(baseDirectory.value / "../analysis-tools/ErrorBars", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "startup.sh", "/home/beaker/startup.sh")
        add(baseDirectory.value / "../analysis-tools/force-field/python-module", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "../analysis-tools/structural-similarity/python-modules/nomad_sim/nomad_viewer.css", "/home/beaker/.beaker/v1/web/css/nomad_viewer.css")
        add(baseDirectory.value / "../analysis-tools/structural-similarity/jsmol", "/home/beaker/.beaker/v1/web/tmp/jsmol")
	add(baseDirectory.value / "../analysis-tools/ClusterX/cell", "/usr/lib/python2.7/cell")
	add(baseDirectory.value / "../analysis-tools/ClusterX/cell", "/home/beaker/py3k/lib/python3.5")
        add(baseDirectory.value / "../nomad-meta-info/meta_info/nomad_meta_info", "/nomad-meta-info/meta_info/nomad_meta_info")
        add(artifact, artifactTargetPath)
        entryPoint("/bin/bash", "/home/beaker/startup.sh")
      }
    }
  )

lazy val webserviceBase = (project in file("webservicesBase")).
  dependsOn(core).
  dependsOn(elasticsearch).
  settings(commonSettings: _*).
  settings(
    name := "webservicesBase",
    libraryDependencies ++= akkaHttpLibs ++ testLibs
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*)

lazy val repoBase = (project in file("repo/repo-base")).
  dependsOn(core).
  dependsOn(repoDb).
  dependsOn(rawDataDb).
  dependsOn(elasticsearch).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  settings(commonSettings: _*).
  settings(
    name := "repoBase",
    libraryDependencies ++= elastic4sLibs
  )

lazy val repoWebservice = (project in file("repo/repo-webservice")).
  dependsOn(repoBase).
  dependsOn(webserviceBase).
  settings(commonSettings: _*).
  settings(
    name := "repoWebservice",
    libraryDependencies ++= akkaHttpLibs ++ testLibs
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*)

lazy val archiveTool = (project in file("archive/archive-tool")).
  dependsOn(core).
  dependsOn(elasticsearch).
  settings(commonSettings: _*).
  settings(
    name := "nomadArchiveTool",
    mainClass in assembly := Some("eu.nomad_lab.archive.ArchiveTool")
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")

    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/archiveTool.jar"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        add(baseDirectory.value / "src/main/docker", "/app")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-Djava.library.path=/lib", "-jar", artifactTargetPath)
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  )

lazy val h5Tool = (project in file("archive/h5-tool")).
  dependsOn(core).
  settings(commonSettings: _*).
  settings(
    name := "h5Tool",
    mainClass in assembly := Some("eu.nomad_lab.h5.H5Tool"),
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")

    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/h5Tool.jar"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        add(baseDirectory.value / "src/main/docker", "/app")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-Djava.library.path=/lib", "-jar", artifactTargetPath)
      }
    })

lazy val archiveWebservice = (project in file("archive/archive-webservice")).
  dependsOn(core).
  dependsOn(webserviceBase).
  dependsOn(elasticsearch).
  settings(commonSettings: _*).
  settings(
    name := "archiveWebservice",
    libraryDependencies ++= akkaHttpLibs ++ testLibs
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")),
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString()),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        expose(8112)
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )

lazy val fileWebservice = (project in file("shared-webservices/file-webservice")).
  dependsOn(core).
  dependsOn(webserviceBase).
  settings(commonSettings: _*).
  settings(
    name := "fileWebservice",
    libraryDependencies ++= akkaHttpLibs ++ testLibs ++ compressionLibs
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      ImageName(s"$dockerRegistry/$dockerProjectName/${name.value.toLowerCase}:v${version.value}")),
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString()),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("nomadlab/nomadlabbase")
        expose(8112)
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )

lazy val rawDataInjection = (project in file("repo/raw-data-injection")).
  dependsOn(repoDb).
  dependsOn(rawDataDb).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        json4sNativeLib +:
        bagitLib +:
        postgresLib +:
        commonsLoggingBridgeLib +:
        loggingLibs),
    libraryDependencies ++= compressionLibs,
    libraryDependencies ++= testLibs,
    name := "rawDataInjection"
  )

lazy val rawDataTool = (project in file("repo/raw-data-tool")).
  dependsOn(rawDataInjection).
  settings(commonSettings: _*).
  settings(
    version       := "0.3",
    libraryDependencies ++= (
      configLib +:
        loggingLibs),
    name := "rawDataTool"
  )


lazy val repoTool = (project in file("repo/repo-tool")).
  dependsOn(repoBase).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "repoTool",
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
      ImageName(
        namespace = Some(organization.value.toLowerCase),
        repository = name.value.toLowerCase,
        tag = Some("v" + version.value)
      )
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("java:7")
        add(artifact, artifactTargetPath)
        entryPoint("bash")
//        entryPoint("java", "-jar", artifactTargetPath)
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*)

lazy val metaInfoTool = (project in file("meta-info-tool")).
  dependsOn(core).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "metaInfoTool",
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
      ImageName(
        namespace = Some(organization.value.toLowerCase),
        repository = name.value.toLowerCase,
        tag = Some("v" + version.value)
      )
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("java:openjdk-8-jre")
        add(artifact, artifactTargetPath)
        entryPoint("bash")
//        entryPoint("java", "-jar", artifactTargetPath)
      }
    },
    javaOptions += ("-Djava.library.path=" + (baseDirectory.value / "../hdf5/hdf5-support/lib").getCanonicalFile().toString())
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*)

lazy val repoDataManager = (project in file("repo/repo-data-manager")).
  dependsOn(repoBase).
  settings(commonSettings: _*).
  enablePlugins(DockerPlugin).
  settings(
    name := "repoDataManager",
    docker := { (docker dependsOn assembly).value },
    imageNames in docker := Seq(
      // Sets a name with a tag that contains the project version; namespace/repository:tag
      ImageName(
        namespace = Some(organization.value.toLowerCase),
        repository = name.value.toLowerCase,
        tag = Some("v" + version.value)
      )
    ),
    dockerfile in docker := {
      val artifact = (assemblyOutputPath in assembly).value
      val artifactTargetPath = s"/app/${artifact.name}"
      new Dockerfile {
        from("java:7")
        add(artifact, artifactTargetPath)
        entryPoint("bash")
        //        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  ).
  settings(Revolver.settings: _*).
  enablePlugins(BuildInfoPlugin).
  settings(gitVersionSettings: _*)

lazy val splitSupport = (project in file("repo/split-support")).
  dependsOn(rawDataInjection).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= (
      configLib +:
        loggingLibs),
    name := "splitSupport"
  )

lazy val root = (project in file(".")).
  configs(FullTest).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= loggingLibs
  ).
  settings(inConfig(FullTest)(Defaults.testTasks): _*).
  settings(
    libraryDependencies ++= fullTestLibs,
    testOptions in Test := Seq(Tests.Filter(testFilter)),
    testOptions in FullTest := Seq()
  ).
  aggregate(
    core, dbSupport, parsingStatsDb, rawDataDb, repoDb,
    fhiAims,  abinit, castep, onetep, cp2k, cpmd, nwchem, bigdft, dlPoly, libAtoms, dmol3, exciting, gaussian, gpaw, lammps, amber, gromacs, gromos, namd, charmm, tinker, mopac, octopus, orca, qbox, quantumEspresso, turbomole, vasp, wien2k, elk, fleur, dftbPlus, asap, atk, gamess, gulp, fplo, molcas, phonopy, atomicData, qhp, elastic,
    stats, fhiAimsBasis, symmetry, systemType, springer, repoTags, bandStructureNormalizer,
    base, normalize, webservice, tool, calculationparser, normalizerWorker, treeparser, kubernetes, integratedpipeline, query, parquet, elasticsearch, repoBase, repoTool, repoDataManager, repoWebservice, archiveTool, h5Tool, archiveWebservice, rawDataInjection, rawDataTool, splitSupport, fileWebservice, metaInfoTool)
