/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query.flink
import org.apache.flink.api.scala._
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.Path
import org.apache.flink.api.java.utils.ParameterTool
import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.ref._
import eu.nomad_lab.meta
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.CompactSha
import eu.nomad_lab.resolve._
import scala.collection.mutable
import eu.nomad_lab.query.CompiledQuery
import eu.nomad_lab.query.QueryExpression
import eu.nomad_lab.query.Filter
import eu.nomad_lab.query.YesFilter
import eu.nomad_lab.JsonUtils
import org.apache.flink.configuration.Configuration
import org.apache.flink.api.common.functions.RichFilterFunction
import org.apache.flink.api.common.functions.RichFlatMapFunction
import org.apache.flink.util.Collector
import org.{ json4s => jn }
import collection.breakOut
import java.sql.Timestamp

object Serializers {
  def writeArchiveSetH5(output: java.io.ObjectOutputStream, archiveSet: ArchiveSetH5): Unit = {
    output.writeObject(archiveSet.basePath.toString)
    output.writeBoolean(archiveSet.hasPrefixDir)
    output.writeBoolean(archiveSet.writable)
    // metaInfoEnv not serialized, assuming all
    output.writeObject(archiveSet.filePrefix)
    // superArchiveSet
    archiveSet.superArchiveSet match {
      case Some(a) =>
        output.writeBoolean(true)
        writeArchiveSetH5(output, a) // assume no circular deps
      case None =>
        output.writeBoolean(false)
    }
  }

  def readArchiveSetH5(input: java.io.ObjectInputStream): ArchiveSetH5 = {
    val basePath = Paths.get(input.readObject().toString)
    val hasPrefixDir = input.readBoolean()
    val writable = input.readBoolean()
    // metaInfoEnv not serialized, assuming all
    val filePrefix = input.readObject().toString
    // superArchiveSet
    val hasSuper = input.readBoolean()
    val superArchiveSet = if (hasSuper)
      Some(readArchiveSetH5(input))
    else
      None

    def compareAS(archiveSet: ArchiveSetH5): Boolean = {
      (basePath == archiveSet.basePath &&
        hasPrefixDir == archiveSet.hasPrefixDir &&
        writable == archiveSet.writable &&
        filePrefix == archiveSet.filePrefix &&
        superArchiveSet == archiveSet.superArchiveSet)
    }

    for (a <- Seq(ArchiveSetH5.normalizedSet, ArchiveSetH5.parsedSet, ArchiveSetH5.toNormalizeSet)) {
      if (compareAS(a))
        return a
    }

    new ArchiveSetH5(
      basePath = basePath,
      hasPrefixDir = hasPrefixDir,
      writable = writable,
      metaInfoEnv0 = None,
      filePrefix = filePrefix,
      superArchiveSet = superArchiveSet
    )
  }

  def writeFilter(output: java.io.ObjectOutputStream, filter: Filter): Unit = {
    val filterJValue = JsonUtils.normalizedUtf8(filter.jValue)
    output.writeInt(filterJValue.size)
    output.write(filterJValue)
  }

  def readFilter(input: java.io.ObjectInputStream): Filter = {
    val fLength = input.readInt()
    val fData = Array.fill[Byte](fLength)(0)
    input.read(fData, 0, fLength)
    val filterJValue = JsonUtils.parseUtf8(fData)
    Filter.fromJValue(filterJValue)
  }
}

/**
 * Scans an archive and executes the given query op on it
 */
class FlinkScanner(
    val filter: Filter,
    val context: String,
    val fields: Seq[String],
    val user: String,
    val archiveSet: ArchiveSetH5
) {
  def jValue: jn.JValue = {
    jn.JObject(
      ("filter" -> filter.jValue) ::
        ("context" -> jn.JString(context)) ::
        ("fields" -> jn.JArray(fields.map { x: String => jn.JString(x) }(breakOut))) :: Nil
    )
  }

  def run(env: ExecutionEnvironment): Path = {
    val indexFilename = "indexSmall.txt"
    val indexPath = archiveSet.basePath.resolve(indexFilename)
    val index = env.readTextFile("file://" + indexPath.toAbsolutePath.toString)
    val uris: DataSet[String] = context match {
      case "archive" | "archive_context" =>
        index.flatMap(new ArchiveContext(filter, archiveSet))
      case "calculation" | "calculation_context" =>
        val calcs = index.flatMap(new ArchiveToCalculations(filter, archiveSet))
        calcs.flatMap(new CalculationContext(filter, archiveSet))
      case metaName =>
        val calcs = index.flatMap(new ArchiveToCalculations(filter, archiveSet))
        val fCalcs = calcs.filter(new CalculationFilter(filter, archiveSet))
        fCalcs.flatMap(new CalculationToContext(filter, context, archiveSet))
    }
    // to do
    // use queryFields (a list of uri, <metaName>, or <metaName>.(uri|gid|cid|value|shape|lenght) and handle them just as it is done in CompiledQuery with TermTree, and create tuples after a call to resolve...
    val matchingFields = uris

    //    val basePath = Paths.get(s"/nomad/nomadlab/user-data/private/$user/query")
    val basePath = Paths.get(s"/nomad/nomadlab/flink/users-transfer/query")
    if (!Files.isDirectory(basePath))
      Files.createDirectories(basePath, LocalEnv.directoryPermissionsAttributes)
    val qSha = CompactSha()
    JsonUtils.normalizedOutputStream(jValue, qSha.outputStream)
    val now = new Timestamp(System.currentTimeMillis())
    val nowStr = now.toString
    val baseName = s"${nowStr.replace(" ", "").replace(":", "")}-${qSha.gidStr("q")}"
    val queryInfoFile = basePath.resolve(baseName + ".query")
    //Files.createFile(queryInfoFile, LocalEnv.filePermissionsAttributes)
    val qiFile = new java.io.FileOutputStream(queryInfoFile.toFile)
    JsonUtils.prettyOutputStream(
      jn.JObject(
        ("query" -> jValue) ::
          ("queryStart" -> jn.JString(nowStr)) ::
          ("user" -> jn.JString(user)) :: Nil
      ),
      qiFile
    )
    qiFile.flush()
    qiFile.close()
    val targetPath = basePath.resolve(baseName + ".csv")
    //Files.createFile(targetPath, LocalEnv.filePermissionsAttributes)
    //matchingFields.writeAsCsv("file://" + targetPath.toAbsolutePath.toString, "\n", "|")
    matchingFields.map("Resultado: " + _).print()
    matchingFields.writeAsText("file://" + targetPath.toAbsolutePath.toString)
    env.execute(s"Flink Query $baseName for user $user")
    targetPath
  }

  def cleanup(): Unit = {
  }
}

/**
 * returns the calculations of the archives not filtered out
 */
class ArchiveToCalculations(var filter: Filter, var archiveSet: ArchiveSetH5) extends RichFlatMapFunction[String, (String, String)] with java.io.Serializable {
  def this() = this(null, null)

  private def writeObject(output: java.io.ObjectOutputStream): Unit = {
    Serializers.writeFilter(output, filter)
    Serializers.writeArchiveSetH5(output, archiveSet)
  }

  private def readObject(input: java.io.ObjectInputStream): Unit = {
    filter = Serializers.readFilter(input)
    archiveSet = Serializers.readArchiveSetH5(input)
  }

  private def readObjectNoData(): Unit = {
    filter = null
    archiveSet = null
  }

  override def flatMap(archiveGid: String, out: Collector[(String, String)]): Unit = {
    val archive = archiveSet.acquireArchive("R" + archiveGid.drop(1))
    try {
      if (filter.filterArchive(archive)) {
        for (calc <- archive.calculations) {
          out.collect(archiveGid -> calc.calculationGid)
        }
      }
    } finally {
      archiveSet.giveBackArchive(archive)
    }
  }
}

/**
 * handles an archive context if given an archive
 */
class ArchiveContext(var filter: Filter, var archiveSet: ArchiveSetH5) extends RichFlatMapFunction[String, String] with java.io.Serializable {
  def this() = this(null, null)

  private def writeObject(output: java.io.ObjectOutputStream): Unit = {
    Serializers.writeFilter(output, filter)
    Serializers.writeArchiveSetH5(output, archiveSet)
  }

  private def readObject(input: java.io.ObjectInputStream): Unit = {
    filter = Serializers.readFilter(input)
    archiveSet = Serializers.readArchiveSetH5(input)
  }

  private def readObjectNoData(): Unit = {
    filter = null
    archiveSet = null
  }

  override def flatMap(archiveGid: String, out: Collector[String]): Unit = {
    val archive = archiveSet.acquireArchive("R" + archiveGid.drop(1))
    try {
      if (filter.filterArchive(archive) && filter.filterContext(Archive(archiveSet, archive))) {
        out.collect("nmd://" + archiveGid)
      }
    } finally {
      archiveSet.giveBackArchive(archive)
    }
  }
}

/**
 * filters out the calculations
 */
class CalculationFilter(var filter: Filter, var archiveSet: ArchiveSetH5) extends RichFilterFunction[(String, String)] with java.io.Serializable {
  def this() = this(null, null)

  private def writeObject(output: java.io.ObjectOutputStream): Unit = {
    Serializers.writeFilter(output, filter)
    Serializers.writeArchiveSetH5(output, archiveSet)
  }

  private def readObject(input: java.io.ObjectInputStream): Unit = {
    filter = Serializers.readFilter(input)
    archiveSet = Serializers.readArchiveSetH5(input)
  }

  private def readObjectNoData(): Unit = {
    filter = null
    archiveSet = null
  }

  override def filter(c: (String, String)): Boolean = {
    val calc = archiveSet.acquireCalculation("R" + c._1.drop(1), c._2)
    try {
      filter.filterCalculation(calc)
    } finally {
      archiveSet.giveBackCalculation(calc)
    }
  }
}

/**
 * handles a calculation context
 */
class CalculationContext(var filter: Filter, var archiveSet: ArchiveSetH5) extends RichFlatMapFunction[(String, String), String] with java.io.Serializable {
  def this() = { this(null, null) }

  private def writeObject(output: java.io.ObjectOutputStream): Unit = {
    Serializers.writeFilter(output, filter)
    Serializers.writeArchiveSetH5(output, archiveSet)
  }

  private def readObject(input: java.io.ObjectInputStream): Unit = {
    filter = Serializers.readFilter(input)
    archiveSet = Serializers.readArchiveSetH5(input)
  }

  private def readObjectNoData(): Unit = {
    filter = null
    archiveSet = null
  }

  override def flatMap(c: (String, String), out: Collector[String]): Unit = {
    val calc = archiveSet.acquireCalculation("R" + c._1.drop(1), c._2)
    try {
      if (filter.filterCalculation(calc) &&
        filter.filterContext(Calculation(archiveSet, calc))) {
        out.collect(s"nmd://${c._1}/${c._2}")
      }
    } finally {
      archiveSet.giveBackCalculation(calc)
    }
  }
}

/**
 * filters section context
 */
class CalculationToContext(
    var filter: Filter, var context: String, var archiveSet: ArchiveSetH5
) extends RichFlatMapFunction[(String, String), String] with java.io.Serializable {
  var metaNamePath: List[String] = Nil
  var isValue: Boolean = false

  def this() = {
    this(null, null, null)
  }

  private def writeObject(output: java.io.ObjectOutputStream): Unit = {
    Serializers.writeFilter(output, filter)
    Serializers.writeArchiveSetH5(output, archiveSet)
    output.writeObject(context)
  }

  private def readObject(input: java.io.ObjectInputStream): Unit = {
    filter = Serializers.readFilter(input)
    archiveSet = Serializers.readArchiveSetH5(input)
    context = input.readObject().toString
    updatePath()
  }

  private def readObjectNoData(): Unit = {
    filter = null
    archiveSet = null
    context = null
  }

  def updatePath(): Unit = {
    if (archiveSet != null) {
      val metaInfoEnv = archiveSet.metaInfoEnv
      metaNamePath = context :: Nil
      var metaNameNow: String = context
      while (metaInfoEnv.parentSectionName(metaNameNow) match {
        case None => false
        case Some(s) =>
          if (metaNamePath contains s)
            throw new Exception(s"circular reference in meta info for $s: $metaNamePath")
          metaNameNow = s
          metaNamePath = s :: metaNamePath
          true
      }) ()
      metaInfoEnv.metaInfoRecordForName(context) match {
        case None => throw new Exception(s"could not find meta info for context $context")
        case Some(metaInfo) =>
          metaInfo.kindStr match {
            case "type_section" => isValue = false
            case "type_document_content" => isValue = true
            case t =>
              throw new Exception(s"Invalid context $t")
          }
      }
    }
  }

  updatePath()

  override def flatMap(c: (String, String), out: Collector[String]): Unit = {
    val calc = archiveSet.acquireCalculation("R" + c._1.drop(1), c._2)
    try {
      if (isValue) {
        val valueTable = calc.valueTable(metaNamePath.toSeq)
        for (value <- valueTable) {
          val ctx = Value(archiveSet, value)
          if (filter.filterContext(ctx)) {
            val ctxUri = ctx.uriString
            out.collect(ctxUri)
          }
        }
      } else {
        val sectionTable = calc.sectionTable(metaNamePath.toSeq)
        for (section <- sectionTable) {
          val ctx = Section(archiveSet, section)
          if (filter.filterContext(ctx)) {
            val ctxUri = ctx.uriString
            out.collect(ctxUri)
          }
        }
      }
    } finally {
      archiveSet.giveBackCalculation(calc)
    }
  }
}

object FlinkAlone {

  def main(args: Array[String]) {

    //Check if the number of input parameters is right
    // @todo: control the parameters
    if (args.length != 3) {
      System.err.println("Number of parameter: " + args.length + "\n  USAGE:\nquery <contextString> <queryString> <user>")
      return
    }

    // File containing the archives
    val contextStr = args(0)
    // Query String
    val dirtyQueryString = args(1).replace("%", " ")
    // User
    val user = args(2)

    def getIndexFromQuery(query: String): (String, Set[String]) = {
      /*
      Set(
        "N6Xj2oPW9TKJd1ZfjOmqca-ylv3pg",
        "NoyCQUolPW301Us0FEN4X8g4itEbD",
        "NSPo7tnXC-Xd7kwIAJPsDw4lb-hX8",
        "NSP_uf7XzTBdv2z1LrErJWfD3kV70"
      )*/
      val parts = query.split("AND")
      val uploaders: Option[String] = parts.filter(x => x.contains("uploader")) match {
        case Array() => None
        case x => Some(x.map(y => y.split("=").last).mkString(","))
      }
      val atoms: Option[String] = parts.filter(x => x.contains("atom_labels")) match {
        case Array() => None
        case x => Some(x.head.split("=").last)
      }
      val metadata: String = parts
        .map(x => x.split("=").head)
        .filter(x => !(x.contains("uploader") | x.contains("atom_labels")))
        .mkString(",")

      val newQuery = parts.filter(x => !(x.contains("uploader") | x.contains("atom_labels"))).mkString(" AND ")
      import sys.process._

      val indexes = (uploaders, atoms, metadata) match {
        case (None, None, meta) =>
          (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromES.sh $meta".!!).toString.split("\n")
            .take(10)
            .toSet
        case (Some(upl), None, meta) =>
          val ind = (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromES.sh $meta".!!).toString.split("\n")
            .toSet
          val other = (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromESUploaders.sh $upl".!!).toString.split("\n")
            .toSet
          ind.filter(x => other.contains(x)).take(10)

        case (None, Some(atl), meta) =>
          val ind = (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromES.sh $meta".!!).toString.split("\n")
            .toSet
          val other = (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromESAtoms.sh $atl".!!).toString.split("\n")
            .toSet
          ind.filter(x => other.contains(x)).take(10)

        case (Some(upl), Some(atl), meta) =>
          val ind = (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromES.sh $meta".!!).toString.split("\n")
            .toSet
          val other = (s"bash /nomad/nomadlab/flink/users-transfer/tools/getIndexFromESUploadersAtoms.sh $upl $atl".!!).toString.split("\n")
            .toSet
          ind.filter(x => other.contains(x)).take(10)
      }

      (newQuery, indexes)

    }

    val (queryString, indexes) = getIndexFromQuery(dirtyQueryString)

    //    val queryString = "program_name=\"FHI-aims\" AND program_compilation_host=\"dr2\""
    // Prepare the query
    val filter = CompiledQuery(QueryExpression(queryString), meta.KnownMetaInfoEnvs.all)

    val env = ExecutionEnvironment.getExecutionEnvironment

    // Call for the query
    val scanner = new FlinkScanner2(
      indexes = indexes,
      filter = filter,
      context = contextStr,
      fields = Seq(),
      user = user,
      archiveSet = ArchiveSetH5.normalizedSet
    )

    val csvFile = scanner.run(env)

    scanner.cleanup()
  }

  class FlinkScanner2(
      val indexes: Set[String],
      val filter: Filter,
      val context: String,
      val fields: Seq[String],
      val user: String,
      val archiveSet: ArchiveSetH5
  ) {
    def jValue: jn.JValue = {
      jn.JObject(
        ("filter" -> filter.jValue) ::
          ("context" -> jn.JString(context)) ::
          ("fields" -> jn.JArray(fields.map { x: String => jn.JString(x) }(breakOut))) :: Nil
      )
    }

    def run(env: ExecutionEnvironment): Path = {
      val index = env.fromCollection(indexes).setParallelism(1)
      val uris: DataSet[String] = context match {
        case "archive" | "archive_context" =>
          index.flatMap(new ArchiveContext(filter, archiveSet))
        case "calculation" | "calculation_context" =>
          val calcs = index.flatMap(new ArchiveToCalculations(filter, archiveSet))
          calcs.flatMap(new CalculationContext(filter, archiveSet))
        case metaName =>
          val calcs = index.flatMap(new ArchiveToCalculations(filter, archiveSet))
          val fCalcs = calcs.filter(new CalculationFilter(filter, archiveSet))
          fCalcs.flatMap(new CalculationToContext(filter, context, archiveSet))
      }
      // to do
      // use queryFields (a list of uri, <metaName>, or <metaName>.(uri|gid|cid|value|shape|lenght) and handle them just as it is done in CompiledQuery with TermTree, and create tuples after a call to resolve...
      val matchingFields = uris

      //    val basePath = Paths.get(s"/nomad/nomadlab/user-data/private/$user/query")
      val basePath = Paths.get(s"/nomad/nomadlab/flink/users-transfer/query/$user")
      if (!Files.isDirectory(basePath))
        Files.createDirectories(basePath, LocalEnv.directoryPermissionsAttributes)
      val qSha = CompactSha()
      JsonUtils.normalizedOutputStream(jValue, qSha.outputStream)
      val now = new Timestamp(System.currentTimeMillis())
      val nowStr = now.toString
      val baseName = s"${nowStr.replace(" ", "").replace(":", "")}-${qSha.gidStr("q")}"
      val queryInfoFile = basePath.resolve(baseName + ".query")
      //Files.createFile(queryInfoFile, LocalEnv.filePermissionsAttributes)
      val qiFile = new java.io.FileOutputStream(queryInfoFile.toFile)
      JsonUtils.prettyOutputStream(
        jn.JObject(
          ("query" -> jValue) ::
            ("queryStart" -> jn.JString(nowStr)) ::
            ("user" -> jn.JString(user)) :: Nil
        ),
        qiFile
      )
      qiFile.flush()
      qiFile.close()
      val targetPath = basePath.resolve(baseName + ".csv")
      matchingFields.writeAsText("file://" + targetPath.toAbsolutePath.toString).setParallelism(1)
      env.execute(s"Flink Query $baseName for user $user")
      targetPath
    }

    def cleanup(): Unit = {
    }
  }

}

