{
    "beaker": "2",
    "evaluators": [
        {
            "name": "HTML",
            "plugin": "HTML",
            "view": {
                "cm": {
                    "mode": "htmlmixed"
                }
            }
        },
        {
            "name": "TeX",
            "plugin": "TeX",
            "view": {
                "cm": {
                    "mode": "stex"
                }
            }
        },
        {
            "name": "IPython",
            "plugin": "IPython",
            "imports": "",
            "supplementalClassPath": "",
            "view": {
                "cm": {
                    "mode": "python"
                }
            },
            "setup": "%matplotlib inline\nimport numpy\nimport matplotlib\nfrom matplotlib import pylab, mlab, pyplot\nnp = numpy\nplt = pyplot\nfrom IPython.display import display\nfrom IPython.core.pylabtools import figsize, getfigs\nfrom pylab import *\nfrom numpy import *\n"
        },
        {
            "name": "JavaScript",
            "plugin": "JavaScript",
            "jsSetting2": "",
            "jsSetting1": "",
            "view": {
                "cm": {
                    "mode": "javascript",
                    "background": "#FFE0F0"
                }
            },
            "languageVersion": "ES2015"
        },
        {
            "name": "Python3",
            "plugin": "Python3",
            "setup": "%matplotlib inline\nimport numpy\nimport matplotlib\nfrom matplotlib import pylab, mlab, pyplot\nnp = numpy\nplt = pyplot\nfrom IPython.display import display\nfrom IPython.core.pylabtools import figsize, getfigs\nfrom pylab import *\nfrom numpy import *\n",
            "view": {
                "cm": {
                    "mode": "python"
                }
            }
        }
    ],
    "cells": [
        {
            "id": "markdownRBGLQA",
            "type": "markdown",
            "body": [
                "<p style=\"color: #20335d;;font-weight: 900; font-size: 22pt;\">  NOMAD analytics toolkit</p>",
                "<label style=\"text-align: center; color: #20335d; font-weight: 900; font-size: 18pt;\">Tutorial example on the stability of materials:</label> <label style=\"color: #20335d;font-weight: 900; font-size: 15pt;\"> Visualizing phase diagrams with dependence on chemical potentials</label>",
                " </p>",
                " <p style=\"font-size: 15px;\"> developed by Mikkel Strange and Kristian S. Thygesen. [Last update September 19 2016]</p>"
            ],
            "evaluatorReader": false
        },
        {
            "id": "codeGHSu9O",
            "type": "code",
            "evaluator": "JavaScript",
            "input": {
                "body": [
                    "beaker.process_form = function() {",
                    "  beaker.query = $('#query').val();    ",
                    "  beaker.evaluate(\"phase_diagram\"); // evaluate all cells with tag phase_diagram",
                    "}"
                ]
            },
            "output": {
                "state": {},
                "selectedType": "Text",
                "pluginName": "JavaScript",
                "height": 50,
                "hidden": true,
                "elapsedTime": 26
            },
            "evaluatorReader": true,
            "lineCount": 4,
            "initialization": true
        },
        {
            "id": "codeIaOuj8",
            "type": "code",
            "evaluator": "Python3",
            "input": {
                "body": [
                    "#import ase.db",
                    "from phasediagram2 import PhaseDiagram",
                    "from refs import refs_ceder as refs",
                    "#con = ase.db.connect('/Users/strange/Nomad/nomad-lab-base/analysis-tools/phase-diagram/cubic_perovskites.db')",
                    "#references = [(row.formula, row.energy) for row in con.select(reference='standard')]",
                    "pd = PhaseDiagram(refs, verbose=True)"
                ]
            },
            "output": {
                "state": {},
                "selectedType": "Results",
                "pluginName": "Python3",
                "shellId": "746F6E6E5EB641948A576C6CEA447F90",
                "height": 50,
                "hidden": true,
                "result": {
                    "type": "Results",
                    "outputdata": [
                        {
                            "type": "out",
                            "value": "Species: Li, P, Fe, O\nReferences: 51\n0    Li             0.000\n1    P              0.000\n2    Fe             0.000\n3    O              0.000\n4    Li2O          -6.200\n5    Li2O2         -7.040\n6    FeO           -4.095\n7    Fe2O3        -11.250\n8    Fe3O4        -15.682\n9    Fe3P          -1.114\n10   Fe2P          -0.876\n11   FeP           -0.339\n12   FeP2          -0.601\n13   FeP4          -1.265\n14   P4O18        -32.042\n15   P2O5         -17.343\n16   P4O9         -31.265\n17   P4O6O2       -27.792\n18   P4O7         -24.028\n19   P4O6         -20.173\n20   LiP7          -2.261\n21   LiP5          -1.873\n22   LiP           -1.193\n23   Li3P7         -4.619\n24   Li3P          -2.944\n25   LiFeP         -1.238\n26   LiFe5O8      -30.650\n27   Li3Fe5O8     -35.668\n28   LiFeO2        -9.156\n29   Li5FeO4      -21.883\n30   Li3PO4       -22.189\n31   Li4P2O7      -36.022\n32   LiPO3        -13.685\n33   Fe9PO4O8     -47.627\n34   Fe3PO4O3     -26.078\n35   Fe4P2O8      -38.360\n36   Fe2PO4O      -20.143\n37   Fe3P2O8      -23.187\n38   Fe7P6O24     -95.984\n39   Fe2P2O7      -29.097\n40   FePO4        -15.309\n41   Fe7P8O28    -113.022\n42   Fe4P6O21     -80.173\n43   Fe2P4O12     -47.801\n44   FeP3O9       -33.953\n45   FeP4O11      -41.533\n46   LiFePO4      -18.853\n47   Li3Fe2P3O12   -53.192\n48   LiFeP2O7     -29.376\n49   LiFeP3O9     -37.523\n50   Li9Fe3P6O21P2O8  -132.471\n"
                        }
                    ]
                },
                "elapsedTime": 351
            },
            "evaluatorReader": true,
            "lineCount": 6,
            "initialization": true
        },
        {
            "id": "codeubF2um",
            "type": "code",
            "evaluator": "HTML",
            "input": {
                "body": [
                    "<style type=\"text/css\">",
                    " .phasediagram_instructions{",
                    "    font-size: 15px;",
                    "  } ",
                    "</style>",
                    "<!-- Button trigger modal -->",
                    "<button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#phasediagram-motivation-modal\">",
                    " Introduction and motivation",
                    "</button>",
                    "",
                    "<!-- Modal -->",
                    "<div class=\"modal fade\" id=\"phasediagram-motivation-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"phasediagram-motivation-modal-label\">",
                    "  <div class=\"modal-dialog modal-lg\" role=\"document\">",
                    "    <div class=\"modal-content\">",
                    "      <div class=\"modal-header\">",
                    "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>",
                    "        <h4 class=\"modal-title\" id=\"phasediagram-motivation-modal-label\">Introduction and motivation</h4>",
                    "      </div>",
                    "      <div class=\"modal-body phasediagram_instructions\">",
                    "        <p> ",
                    "          In this tutorial we present a tool that produces compotitional phase diagrams. As an example we consider",
                    "          materials made of Li-Fe-P-O; the methology is similar to one in [1,2] and the ",
                    "          analysis can be compared directly to the on in [1].",
                    "        </p>",
                    "          ",
                    "        <p> Phase diagrams are generaly useful to determine if a given material is thermodynamic stable under certain conditions such as temperature, pressure etc. In this tutorial we consider only a compositional phasediagram at zero temperature and pressure. We visualize the phase diagram with the so-called convex hull which in general is a set of connected multidimensional \"surfaces\"; for a binary compound it is composed of connected lines and for ternary compound it is composed of connected planes. Since the phase diagram only consider the thermodynamic stability, there may still be materials which are metastable that are missed by the materials making up the convex hull. One way to explore a larger phase-space of materials is to allow for variations of the chemipotenial of some of the elements, i.e. of oxygen, which could be tuned during synthesis by varying the temperature etc.",
                    "        </p>",
                    "        <p>References</p>",
                    "        <ol>",
                    "          <li> S. P Ong, L. Wang, B. Kang, and G. Ceder, Chem. Mater. 20, 1798-1807 (2008).</li>",
                    "          <li> A. R. Akbarzadeh, V. Olzolins, and C. Wolverton, Advanced Materials 19, 3233-3239 (2007) </li>",
                    "        </ol>",
                    "      </div>",
                    "      <div class=\"modal-footer\">",
                    "        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>",
                    "<!--         <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->",
                    "      </div>",
                    "    </div>",
                    "  </div>",
                    ""
                ]
            },
            "output": {
                "state": {},
                "selectedType": "BeakerDisplay",
                "height": 72,
                "result": {
                    "type": "BeakerDisplay",
                    "innertype": "Html",
                    "object": "<script>\nvar beaker = bkHelper.getBeakerObject().beakerObj;\n</script>\n<style type=\"text/css\">\n .phasediagram_instructions{\n    font-size: 15px;\n  } \n</style>\n<!-- Button trigger modal -->\n<button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#phasediagram-motivation-modal\">\n Introduction and motivation\n</button>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"phasediagram-motivation-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"phasediagram-motivation-modal-label\" style=\"display: none;\">\n  <div class=\"modal-dialog modal-lg\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n        <h4 class=\"modal-title\" id=\"phasediagram-motivation-modal-label\">Introduction and motivation</h4>\n      </div>\n      <div class=\"modal-body phasediagram_instructions\">\n        <p> \n          In this tutorial we present a tool that produces compotitional phase diagrams. As an example we consider\n          materials made of Li-Fe-P-O; the methology is similar to one in [1,2] and the \n          analysis can be compared directly to the on in [1].\n        </p>\n          \n        <p> Phase diagrams are generaly useful to determine if a given material is thermodynamic stable under certain conditions such as temperature, pressure etc. In this tutorial we consider only a compositional phasediagram at zero temperature and pressure. We visualize the phase diagram with the so-called convex hull which in general is a set of connected multidimensional \"surfaces\"; for a binary compound it is composed of connected lines and for ternary compound it is composed of connected planes. Since the phase diagram only consider the thermodynamic stability, there may still be materials which are metastable that are missed by the materials making up the convex hull. One way to explore a larger phase-space of materials is to allow for variations of the chemipotenial of some of the elements, i.e. of oxygen, which could be tuned during synthesis by varying the temperature etc.\n        </p>\n        <p>References</p>\n        <ol>\n          <li> S. P Ong, L. Wang, B. Kang, and G. Ceder, Chem. Mater. 20, 1798-1807 (2008).</li>\n          <li> A. R. Akbarzadeh, V. Olzolins, and C. Wolverton, Advanced Materials 19, 3233-3239 (2007) </li>\n        </ol>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n<!--         <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n      </div>\n    </div>\n  </div>\n</div>"
                },
                "elapsedTime": 0
            },
            "evaluatorReader": true,
            "lineCount": 40,
            "initialization": true
        },
        {
            "id": "codeZ2DSp3",
            "type": "code",
            "evaluator": "HTML",
            "input": {
                "body": [
                    "<style type=\"text/css\">",
                    " .phasediagram_instructions{",
                    "    font-size: 15px;",
                    "  } ",
                    "</style>",
                    "<!-- Button trigger modal -->",
                    "<button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#phasediagram-instructions-modal\">",
                    " Instructions",
                    "</button>",
                    "",
                    "<!-- Modal -->",
                    "<div class=\"modal fade\" id=\"phasediagram-instructions-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"phasediagram-instructions-modal-label\">",
                    "  <div class=\"modal-dialog\" role=\"document\">",
                    "    <div class=\"modal-content\">",
                    "      <div class=\"modal-header\">",
                    "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>",
                    "        <h4 class=\"modal-title\" id=\"phasediagram-instructions-modal-label\">Instructions</h4>",
                    "      </div>",
                    "      <div class=\"modal-body phasediagram_instructions\">",
                    "<p> In this example, you can run the phase diagram code with varying chemical potentials for the elements as well as try to",
                    "  predict if a material of given composition will decompose into other phases.",
                    "  ou will work directly in Python, but need only to enter some very",
                    "  simple commands like: </p>",
                    "        <p><code> pd.decompose(...) </code> and <code> pd.plot(...) </code>.</p>",
                    "  <!--<b>principal component analysis (<a href=\"https://en.wikipedia.org/wiki/Feature_scaling\" target=\"_blank\">PCA</a>)</b> and two selected non-linear methods, <b>multidimensional scaling (<a href=\"https://en.wikipedia.org/wiki/Multidimensional_scaling\" target=\"_blank\">MDS</a>) </b>",
                    "        and <b>t-Distributed Stochastic Neighbor Embedding (<a href=\"https://en.wikipedia.org/wiki/T-distributed_stochastic_neighbor_embedding\" target=\"_blank\">t-SNE</a>) </b>. -->",
                    "      ",
                    "<p> The Li-Fe-P-O materials considered in this example are taken from [1] and the database of the energies used by",
                    "  the phase diagram module are formation energies with respect to the monomers Li(s), Fe(s), P(s) and a corrected value for O2.",
                    "  <!--in the cubic perovskite structure and is take from Castelli et al. (Energy Environ. Sci. 5, 5814 (2011)).</p>-->",
                    "<p> Lets first condider that we are interested in a \"new\" material, ",
                    "    say with the chemical formula <b>FeLiO</b> and the calculated formation energy <b>E(FeLiO)</b>>.",
                    "    To test if it is (thermodynamically) stable or not compared to other the phases of Fe-Li-O in the database we can use the command:</p>",
                    "       <p>",
                    " <code>pd.decompose('FeLiO')</code> ",
                    "        </p>",
                    " <p> This shows what the material 'FeLiO' will decompose into using the materials in the database (FeLiO -> (1/5) Li5FeO4 + (1/5) FeO - (3/5) Fe) . If <b>E(FeLiO)</b> is below the total energy of the decomposed phases, in this case about -5.2 eV, the \"new\" material is prediceted as stable, i.e. the material is below the complex hull.",
                    "   </p>",
                    "    <p> FePO4 is an experimentally identified phase which <code>pd.decompose('FePO4')</code> supports (FePO4->FePO4).",
                    "   </p>",
                    "        ",
                    "<p> To visualize the complex hull of the ternary composition Li-Fe-O enter: <br><code> pd.plot('LiFeO') </code></p>",
                    "        This can be compared with Figure 1b in [1]. To investigate the line binary \"line\" Li-O use <code>pd.plot('LiO')</code>.",
                    "        <p>The chemical potentials can be adjusted, say of oxygen, by entering<br><code>pd.plot(O=-1.3)</code>.</p>",
                    "          <p>References</p>",
                    "        <ol>",
                    "          <li> S. P Ong, L. Wang, B. Kang, and G. Ceder, Chem. Mater. 20, 1798-1807 (2008).</li>",
                    "          <!--<li> A. R. Akbarzadeh, V. Olzolins, and C. Wolverton, Advanced Materials 19, 3233-3239 (2007) </li>-->",
                    "        </ol>      ",
                    "      </div>",
                    "      <div class=\"modal-footer\">",
                    "        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>",
                    "<!--         <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->",
                    "      </div>",
                    "    </div>",
                    "  </div>",
                    ""
                ]
            },
            "output": {
                "state": {},
                "selectedType": "BeakerDisplay",
                "height": 72,
                "result": {
                    "type": "BeakerDisplay",
                    "innertype": "Html",
                    "object": "<script>\nvar beaker = bkHelper.getBeakerObject().beakerObj;\n</script>\n<style type=\"text/css\">\n .phasediagram_instructions{\n    font-size: 15px;\n  } \n</style>\n<!-- Button trigger modal -->\n<button type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#phasediagram-instructions-modal\">\n Instructions\n</button>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"phasediagram-instructions-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"phasediagram-instructions-modal-label\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n        <h4 class=\"modal-title\" id=\"phasediagram-instructions-modal-label\">Instructions</h4>\n      </div>\n      <div class=\"modal-body phasediagram_instructions\">\n<p> In this example, you can run the phase diagram code with varying chemical potentials for the elements as well as try to\n  predict if a material of given composition will decompose into other phases.\n  ou will work directly in Python, but need only to enter some very\n  simple commands like: </p>\n        <p><code> pd.decompose(...) </code> and <code> pd.plot(...) </code>.</p>\n  <!--<b>principal component analysis (<a href=\"https://en.wikipedia.org/wiki/Feature_scaling\" target=\"_blank\">PCA</a>)</b> and two selected non-linear methods, <b>multidimensional scaling (<a href=\"https://en.wikipedia.org/wiki/Multidimensional_scaling\" target=\"_blank\">MDS</a>) </b>\n        and <b>t-Distributed Stochastic Neighbor Embedding (<a href=\"https://en.wikipedia.org/wiki/T-distributed_stochastic_neighbor_embedding\" target=\"_blank\">t-SNE</a>) </b>. -->\n      \n<p> The Li-Fe-P-O materials considered in this example are taken from [1] and the database of the energies used by\n  the phase diagram module are formation energies with respect to the monomers Li(s), Fe(s), P(s) and a corrected value for O2.\n  <!--in the cubic perovskite structure and is take from Castelli et al. (Energy Environ. Sci. 5, 5814 (2011)).</p>-->\n</p><p> Lets first condider that we are interested in a \"new\" material, \n    say with the chemical formula <b>FeLiO</b> and the calculated formation energy <b>E(FeLiO)</b>&gt;.\n    To test if it is (thermodynamically) stable or not compared to other the phases of Fe-Li-O in the database we can use the command:</p>\n       <p>\n <code>pd.decompose('FeLiO')</code> \n        </p>\n <p> This shows what the material 'FeLiO' will decompose into using the materials in the database (FeLiO -&gt; (1/5) Li5FeO4 + (1/5) FeO - (3/5) Fe) . If <b>E(FeLiO)</b> is below the total energy of the decomposed phases, in this case about -5.2 eV, the \"new\" material is prediceted as stable, i.e. the material is below the complex hull.\n   </p>\n    <p> FePO4 is an experimentally identified phase which <code>pd.decompose('FePO4')</code> supports (FePO4-&gt;FePO4).\n   </p>\n        \n<p> To visualize the complex hull of the ternary composition Li-Fe-O enter: <br><code> pd.plot('LiFeO') </code></p>\n        This can be compared with Figure 1b in [1]. To investigate the line binary \"line\" Li-O use <code>pd.plot('LiO')</code>.\n        <p>The chemical potentials can be adjusted, say of oxygen, by entering<br><code>pd.plot(O=-1.3)</code>.</p>\n          <p>References</p>\n        <ol>\n          <li> S. P Ong, L. Wang, B. Kang, and G. Ceder, Chem. Mater. 20, 1798-1807 (2008).</li>\n          <!--<li> A. R. Akbarzadeh, V. Olzolins, and C. Wolverton, Advanced Materials 19, 3233-3239 (2007) </li>-->\n        </ol>      \n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n<!--         <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n      </div>\n    </div>\n  </div>\n</div>"
                },
                "elapsedTime": 0
            },
            "evaluatorReader": true,
            "lineCount": 57,
            "initialization": true
        },
        {
            "id": "codeSnfSqe",
            "type": "code",
            "evaluator": "HTML",
            "input": {
                "body": [
                    "<p>Enter a command:</p>",
                    "",
                    "<p>",
                    "  <input type='text' id='query' value='' size='79'",
                    "         onkeydown=\"if (event.keyCode == 13) beaker.process_form()\">",
                    "</p>",
                    "<p>Examples:</p>",
                    "<p>",
                    " <fontsize=4>",
                    "<code>print(pd.symbols)</code> -- components in references<br>",
                    "<code>pd.decompose('FeLiO')</code> -- what will it be decomposed into<br>",
                    "<code>pd.decompose('FePO4')</code> -- what will it be decomposed (exp. found)<br>",
                    "<code>pd.plot()</code> -- plot the convex hull<br>",
                    "<code>pd.plot('FeLiO')</code> -- plot the convex hull of the ternary Fe-Li-O<br>",
                    "<code>pd.plot('FeLiO', dims=3)</code> -- plot the convex hull of the ternary Fe-Li-O in 3D<br>",
                    "<code>pd.plot('LiP')</code> -- plot the convex hull of the binary Li-P system<br>",
                    "<code>pd.plot(O=-1.3)</code> -- use a different chemical potential for Oxygen",
                    "</fontsize>",
                    "</p>",
                    "<button onclick='beaker.process_form()'> Run </button>"
                ]
            },
            "output": {
                "state": {},
                "selectedType": "BeakerDisplay",
                "height": 299,
                "result": {
                    "type": "BeakerDisplay",
                    "innertype": "Html",
                    "object": "<script>\nvar beaker = bkHelper.getBeakerObject().beakerObj;\n</script>\n<p>Enter a command:</p>\n\n<p>\n  <input type=\"text\" id=\"query\" value=\"\" size=\"79\" onkeydown=\"if (event.keyCode == 13) beaker.process_form()\">\n</p>\n<p>Examples:</p>\n<p>\n <fontsize=4>\n<code>print(pd.symbols)</code> -- components in references<br>\n<code>pd.decompose('FeLiO')</code> -- what will it be decomposed into<br>\n<code>pd.decompose('FePO4')</code> -- what will it be decomposed (exp. found)<br>\n<code>pd.plot()</code> -- plot the convex hull<br>\n<code>pd.plot('FeLiO')</code> -- plot the convex hull of the ternary Fe-Li-O<br>\n<code>pd.plot('FeLiO', dims=3)</code> -- plot the convex hull of the ternary Fe-Li-O in 3D<br>\n<code>pd.plot('LiP')</code> -- plot the convex hull of the binary Li-P system<br>\n<code>pd.plot(O=-1.3)</code> -- use a different chemical potential for Oxygen\n\n</fontsize=4></p>\n<button onclick=\"beaker.process_form()\"> Run </button>"
                },
                "elapsedTime": 0
            },
            "evaluatorReader": true,
            "lineCount": 20,
            "initialization": true
        },
        {
            "id": "code1zERGQ",
            "type": "code",
            "evaluator": "Python3",
            "input": {
                "body": [
                    "# Here you can execute python commands",
                    "print('running the command:', beaker.query, '...')",
                    "exec(beaker.query)",
                    "print('done')"
                ]
            },
            "output": {
                "state": {},
                "selectedType": "Results",
                "pluginName": "Python3",
                "shellId": "6922F636D5D94D17B258B0903EFA87D4",
                "height": 86,
                "dataresult": [
                    -10.772113943373578,
                    [
                        55,
                        54,
                        53,
                        52,
                        51,
                        50,
                        49,
                        48,
                        47,
                        46,
                        45,
                        44,
                        43,
                        42,
                        41,
                        40,
                        39,
                        38,
                        37,
                        36,
                        35,
                        34,
                        33,
                        32,
                        31,
                        30,
                        29,
                        28,
                        27,
                        26,
                        25,
                        24,
                        23,
                        22,
                        21,
                        20,
                        19,
                        18,
                        17,
                        16,
                        15,
                        14,
                        13,
                        12,
                        11,
                        10,
                        9,
                        8,
                        7,
                        6,
                        5,
                        4,
                        3,
                        2,
                        0,
                        1
                    ],
                    [
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0.25,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        1,
                        0,
                        0,
                        0
                    ]
                ]
            },
            "evaluatorReader": true,
            "lineCount": 4,
            "tags": "phase_diagram"
        }
    ],
    "namespace": {
        "selected_feature_list": [
            "rs(A)",
            "rs(B)",
            "rp(A)",
            "rp(B)",
            "rd(A)",
            "rd(B)"
        ],
        "allowed_operations": [
            "|-|",
            "/",
            "^2",
            "exp"
        ],
        "maxDim": null,
        "max_dim": "2",
        "max_dim2": 18,
        "max_dim3": [
            11
        ],
        "runInfo": "running Lasso",
        "viewer_result": "a07a590d6030cf73",
        "embed_method": "pca",
        "standardize": "True",
        "query": "pd.plot(O=-0.5)",
        "x": 2
    },
    "locked": true
}
