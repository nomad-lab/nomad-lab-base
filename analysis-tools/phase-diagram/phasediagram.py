from __future__ import division, print_function
import fractions
import re

import numpy as np
from scipy.spatial import ConvexHull

from ase.atoms import string2symbols
from ase.utils import hill
from ase.data import chemical_symbols

import matplotlib.pyplot as plt
textwidth_points = 455.24408
textwidth = 6.299  # in
rcparams = {'font.size' : 14,
                        #'axes.labelsize' : 12,
                        'legend.fontsize': 14,
                        #'xtick.labelsize' : 12,
                        #'ytick.labelsize' : 12,
                        'font.family' : 'lmodern',
                        'text.latex.unicode': True,
                        #'lines.markersize': 11, 
                        #'backend': 'ps',
                        'text.usetex': True
                        }
plt.rcParams.update(rcparams)



#import ase.db
_solvated = []

#con = ase.db.connect('cubic_perovskites.db')


def make_references():# need formulas now
    #references = (name, energy) name=Zn2O4
    #return references
    pass


def parse_formula(formula):
    aq = formula.endswith('(aq)')
    if aq:
        formula = formula[:-4]
    charge = formula.count('+') - formula.count('-')
    if charge:
        formula = formula.rstrip('+-')
    count = {}
    for symbol in string2symbols(formula):
        count[symbol] = count.get(symbol, 0) + 1
    return count, charge, aq


def get_123(con):
    rows_mon = []
    rows_bin = []
    rows_tri = []
    for row in con.select():
        count, charge, aq = parse_formula(row.formula)
        if len(count) == 1:
            rows_mon.append((row, count))
            #print(row.formula, row.reference)
        elif len(count) == 2:
            rows_bin.append((row, count))
        elif len(count) == 3:
            rows_tri.append((row, count))
        #else:
        #    print('multiple species:', len(count), row.formula)
    return rows_mon, rows_bin, rows_tri

#m, b, t = get_123(con)


def float2str(x):
    f = fractions.Fraction(x).limit_denominator(100)
    n = f.numerator
    d = f.denominator
    if abs(n / d - f) > 1e-6:
        return '{0:.3f}'.format(f)
    if d == 0:
        return '0'
    if f.denominator == 1:
        return str(n)
    return '{0}/{1}'.format(f.numerator, f.denominator)


def bisect(A, X, Y, f):
    a = []
    for i in [0, -1]:
        for j in [0, -1]:
            if A[i, j] == -1:
                A[i, j] = f(X[i], Y[j])
            a.append(A[i, j])

    if np.ptp(a) == 0:
        A[:] = a[0]
        return
    if a[0] == a[1]:
        A[0] = a[0]
    if a[1] == a[3]:
        A[:, -1] = a[1]
    if a[3] == a[2]:
        A[-1] = a[3]
    if a[2] == a[0]:
        A[:, 0] = a[2]
    if not (A == -1).any():
        return
    i = len(X) // 2
    j = len(Y) // 2
    bisect(A[:i + 1, :j + 1], X[:i + 1], Y[:j + 1], f)
    bisect(A[:i + 1, j:], X[:i + 1], Y[j:], f)
    bisect(A[i:, :j + 1], X[i:], Y[:j + 1], f)
    bisect(A[i:, j:], X[i:], Y[j:], f)


def print_results(results):
    total_energy = 0.0
    print('reference    coefficient      energy')
    print('------------------------------------')
    for name, coef, energy in results:
        total_energy += coef * energy
        if abs(coef) < 1e-7:
            continue
        print('{0:14}{1:>10}{2:12.3f}'.format(name, float2str(coef), energy))
    print('------------------------------------')
    print('Total energy: {0:22.3f}'.format(total_energy))
    print('------------------------------------')


class PhaseDiagram:
    def __init__(self, references, filter='', verbose=True):
        """Phase-diagram.

        references: list of (name, energy) tuples
            List of references.  The energy must be the total energy and not
            energy per atom.  The names can also be dicts like
            ``{'Zn': 1, 'O': 2}`` which would be equivalent to ``'ZnO2'``.
        filter: str or list of str
            Use only those references that match the given filter.
            Example: ``filter='ZnO'`` will select those that
            contain zinc or oxygen.
        verbose: bool
            Write information.
        """

        self.input_references = references.copy()
        filter = parse_formula(filter)[0]

        self.verbose = verbose

        self.species = {}
        self.references = []
        for name, energy in references:
            if isinstance(name, str):
                count = parse_formula(name)[0]
            else:
                count = name
                name = hill(count)

            if filter and any(symbol not in filter for symbol in count):
                continue

            natoms = 0
            for symbol, n in count.items():
                natoms += n
                if symbol not in self.species:
                    self.species[symbol] = len(self.species)
            self.references.append((count, energy, name, natoms))

        self.symbols = [None] * len(self.species)
        for symbol, id in self.species.items():
            self.symbols[id] = symbol

        if verbose:
            print('Species:', ', '.join(self.symbols))
            print('References:', len(self.references))
            for i, (count, energy, name, natoms) in enumerate(self.references):
                print('{0:<5}{1:10}{2:10.3f}'.format(i, name, energy))

        self.calculate()

    def calculate(self, **kwargs):
        """Calculate convex hull
            use calculate(O=2.0) to change the chemical potential of O (oxygen)
            by 2.0 eV
        """
        self.points = np.zeros((len(self.references), len(self.species) + 1))
        for s, (count, energy, name, natoms) in enumerate(self.references):
            for symbol, n in count.items():
                hmm = len(count) > 1  # only add if it is not a monomer
                #hmm = True
                if symbol in kwargs and hmm:
                    energy -= n * kwargs[symbol] # adjust energy 
                self.points[s, self.species[symbol]] = n / natoms
            self.points[s, -1] = (energy) / natoms

        hull = ConvexHull(self.points[:, 1:])

        # Find relevant simplices:
        ok = hull.equations[:, -2] < 0
        self.simplices = hull.simplices[ok]

        # Create a mask for those points that are on the convex hull:
        self.hull = np.zeros(len(self.points), bool)
        for simplex in self.simplices:
            self.hull[simplex] = True

        if self.verbose:
            print('Simplices:', len(self.simplices))

    def decompose(self, formula=None, **kwargs):
        """Find the combination of the references with the lowest energy.

        formula: str
            Stoichiometry.  Example: ``'ZnO'``.  Can also be given as
            keyword arguments: ``decompose(Zn=1, O=1)``.

        Example::

            pd = PhaseDiagram(...)
            pd.decompose(Zn=1, O=3)

        Returns energy, indices of references and coefficients."""

        if formula:
            assert not kwargs
            kwargs = parse_formula(formula)[0]

        point = np.zeros(len(self.species))
        N = 0
        for symbol, n in kwargs.items():
            point[self.species[symbol]] = n
            N += n

        # Find coordinates within each simplex:
        X = self.points[self.simplices, 1:-1] - point[1:] / N

        # Find the simplex with positive coordinates that sum to
        # less than one:
        for i, Y in enumerate(X):
            try:
                x = np.linalg.solve((Y[1:] - Y[:1]).T, -Y[0])
            except np.linalg.linalg.LinAlgError:
                continue
            if (x >= 0).all() and x.sum() <= 1:
                break
        else:
            assert False, X

        indices = self.simplices[i]
        points = self.points[indices]

        scaledcoefs = [1 - x.sum()]
        scaledcoefs.extend(x)

        energy = N * np.dot(scaledcoefs, points[:, -1])

        coefs = []
        results = []
        for coef, s in zip(scaledcoefs, indices):
            count, e, name, natoms = self.references[s]
            coef *= N / natoms
            coefs.append(coef)
            results.append((name, coef, e))

        if self.verbose:
            print_results(results)

        return energy, indices, np.array(coefs)

    def plot(self, filter=None, ax=None, dims=None, show=True, **kwargs):
        """Make 2-d or 3-d plot of datapoints and convex hull.

        Default is 2-d for 2- and 3-component diagrams and 3-d for a
        4-component diagram.
        """
        #import matplotlib.pyplot as plt
        if filter is not None:
            self.__init__(self.input_references, filter, self.verbose) 

        if len(kwargs) > 0:
            self.calculate(**kwargs)

        N = len(self.species)

        if dims is None:
            if N <= 3:
                dims = 2
            else:
                dims = 3

        if ax is None:
            projection = None
            if dims == 3:
                projection = '3d'
                from mpl_toolkits.mplot3d import Axes3D
                Axes3D  # silence pyflakes
            ax = plt.gca(projection=projection)
        else:
            if dims == 3 and not hasattr(ax, 'set_zlim'):
                raise ValueError('Cannot make 3d plot unless axes projection '
                                 'is 3d')

        if dims == 2:
            if N == 2:
                self.plot2d2(ax)
            elif N == 3:
                self.plot2d3(ax)
            else:
                raise ValueError('Can only make 2-d plots for 2 and 3 '
                                 'component systems!')
        else:
            if N == 3:
                self.plot3d3(ax)
            elif N == 4:
                self.plot3d4(ax)
            else:
                raise ValueError('Can only make 3-d plots for 3 and 4 '
                                 'component systems!')
        if show:
            plt.show()
        if filter is not None:
            self.__init__(self.input_references, verbose=self.verbose)
        return ax

    def plot2d2(self, ax):
        x, e = self.points[:, 1:].T
        for i, j in self.simplices:
            ax.plot(x[[i, j]], e[[i, j]], '-b')
        ax.plot(x[self.hull], e[self.hull], 'og')
        ax.plot(x[~self.hull], e[~self.hull], 'sr')
        for a, b, ref in zip(x, e, self.references):
            name = re.sub('(\d+)', r'$_{\1}$', ref[2])
            ax.text(a, b, name, horizontalalignment='center',
                    verticalalignment='bottom')

        ax.set_xlabel(self.symbols[1])
        ax.set_ylabel('energy [eV/atom]')

    def plot2d3(self, ax):
        x, y = self.points[:, 1:-1].T.copy()
        x += y / 2
        y *= 3**0.5 / 2
        for i, j, k in self.simplices:
            ax.plot(x[[i, j, k, i]], y[[i, j, k, i]], '-b')
        ax.plot(x[self.hull], y[self.hull], 'og')
        ax.plot(x[~self.hull], y[~self.hull], 'sr')
        for a, b, ref in zip(x, y, self.references):
            name = re.sub('(\d+)', r'$_{\1}$', ref[2])
            ax.text(a, b, name, horizontalalignment='center',
                    verticalalignment='bottom')

    def plot3d3(self, ax):
        x, y, e = self.points[:, 1:].T

        ax.scatter(x[self.hull], y[self.hull], e[self.hull],
                   c='g', marker='o')
        ax.scatter(x[~self.hull], y[~self.hull], e[~self.hull],
                   c='r', marker='s')

        for a, b, c, ref in zip(x, y, e, self.references):
            name = re.sub('(\d+)', r'$_{\1}$', ref[2])
            ax.text(a, b, c, name, ha='center', va='bottom')

        for i, j, k in self.simplices:
            ax.plot(x[[i, j, k, i]],
                    y[[i, j, k, i]],
                    zs=e[[i, j, k, i]], c='b')

        ax.set_xlim3d(0, 1)
        ax.set_ylim3d(0, 1)
        ax.view_init(azim=115, elev=30)
        ax.set_xlabel(self.symbols[1])
        ax.set_ylabel(self.symbols[2])
        ax.set_zlabel('energy [eV/atom]')

    def plot3d4(self, ax):
        x, y, z = self.points[:, 1:-1].T
        a = x / 2 + y + z / 2
        b = 3**0.5 * (x / 2 + y / 6)
        c = (2 / 3)**0.5 * z

        ax.scatter(a[self.hull], b[self.hull], c[self.hull],
                   c='g', marker='o')
        ax.scatter(a[~self.hull], b[~self.hull], c[~self.hull],
                   c='r', marker='s')

        for x, y, z, ref in zip(a, b, c, self.references):
            name = re.sub('(\d+)', r'$_{\1}$', ref[2])
            ax.text(x, y, z, name, ha='center', va='bottom')

        for i, j, k, w in self.simplices:
            ax.plot(a[[i, j, k, i, w, k, j, w]],
                    b[[i, j, k, i, w, k, j, w]],
                    zs=c[[i, j, k, i, w, k, j, w]], c='b')

        ax.set_xlim3d(0, 1)
        ax.set_ylim3d(0, 1)
        ax.set_zlim3d(0, 1)
        ax.view_init(azim=115, elev=30)


refs2 = [('Al', 0.0),
        ('O', 0.0),
        ('AlO', -1.0),
        ('AlO2', -2.3),
        ('Al2O3',-6.5),
        ('AlO3', -5.0)]


refs3 = [('K', 0), ('Ta', 0), ('O2', 0),
         ('K3TaO8', -16.167), ('KO2', -2.288),
         ('KO3', -2.239), ('Ta2O5', -19.801),
         ('TaO3', -8.556), ('TaO', -1.967),
         ('K2O', -3.076), ('K2O2', -4.257),
         ('KTaO3', -13.439)]


refs_ceder = [('Li', 0.0),
              ('O', 0.0),
              ('Fe', 0.0),
              ('P', 0.0),
              ('Li2O', -6.200),
              ('Li2O2', -7.040),
              ('FeO', -4.095),
              ('Fe2O3', -11.250),
              ('Fe3O4', -15.682),
              ('Fe3P', -1.114),
              ('Fe2P', -0.876),
              ('FeP', -0.339),
              ('FeP2', -0.601),
              ('FeP4', -1.265),
              ('P4O18', -32.042),
              ('P2O5', -17.343),
              ('P4O9', -31.265),
              ('P4O6O2', -27.792)]

if __name__ == '__main__':
    refs = [('Al', -0.3),
            ('O', 0.0),
            ('AlO', -1.0),
            ('AlO2', -2.3),
            ('Al2O3',-6.5),
            ('AlO3', -5.0)]
    from refs import refs_ceder as refs
    pd = PhaseDiagram(refs, verbose=True)
    pd.plot(filter='LiFeO')
    pd.decompose('LiO')
#    print(parse_formula('P4O6O2'))
#    pd = PhaseDiagram(refs, verbose=False)
#    print(pd.species)
    #pd.calculate(O=0.0)
#    print(pd.species)
#    pd.decompose(Al=1, O=1)
#    for ea in range(-5, 10, 1):
#        print('E_O={0}'.format(ea))
#        pd.plot(Al=ea)
