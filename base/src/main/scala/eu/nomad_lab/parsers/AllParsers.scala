/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers
import scala.collection.breakOut

object AllParsers {
  class DuplicateParser(name: String) extends Exception(
    s"Duplicate parser with name $name"
  ) {}

  def listToMap(parserList: Seq[ParserGenerator]): Map[String, ParserGenerator] = {
    val res: Map[String, ParserGenerator] = parserList.map { (pg: ParserGenerator) =>
      pg.name -> pg
    }(breakOut)

    if (res.size != parserList.length) {
      for (pg <- parserList) {
        if (pg != res(pg.name))
          throw new DuplicateParser(pg.name)
      }
    }
    res
  }

  /**
   * All known parsers
   */
  val activeParsers: Map[String, ParserGenerator] = {
    listToMap(Seq(
      FhiAimsParser,
      CastepParser,
      Cp2kParser,
      ExcitingParser,
      ExcitingParserLithium,
      GaussianParser,
      DlPolyParser,
      LibAtomsParser,
      GpawParser,
      Dmol3Parser,
      VaspRunParser,
      QboxParser,
      LammpsParser,
      AmberParser,
      GromacsParser,
      GromosParser,
      NamdParser,
      CharmmParser,
      TinkerParser,
      CrystalParser,
      OctopusParser,
      OnetepParser,
      CpmdParser,
      SiestaParser,
      QuantumEspressoParser,
      Wien2kParser,
      ElkParser,
      AbinitParser,
      FleurParser,
      DftbPlusParser,
      MopacParser,
      NWChemParser,
      BigDFTParser,
      OrcaParser,
      TurbomoleParser,
      AsapParser,
      AtkParser,
      GamessParser,
      GulpParser,
      FploParser,
      MolcasParser,
      PhonopyParser,
      AtomicDataParser,
      QhpParser,
      ElasticParser
    ))
  }

  /**
   * All inactive parsers
   */
  val inactiveParsers: Map[String, ParserGenerator] = {
    listToMap(Seq(
      VaspParser, // disabled because not really useful
      FhiAimsControlInParser
    ))
  }

  /**
   * All known parsers
   */
  val knownParsers: Map[String, ParserGenerator] = {
    for ((k, v) <- inactiveParsers) {
      if (activeParsers.contains(k))
        throw new DuplicateParser(k)
    }
    activeParsers ++ inactiveParsers
  }

  /**
   * The default active parsers
   */
  val defaultParserCollection: ParserCollection = {
    new ParserCollection(activeParsers)
  }
}
