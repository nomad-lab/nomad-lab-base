#!/bin/bash
nomadRoot=${nomadRoot:-/nomad/nomadlab}
updateDeploy=1
target_hostname=${target_hostname:-$HOSTNAME}
chownRoot=
tls=
secretWebCerts=

while test ${#} -gt 0
do
    case "$1" in
      --tls)
          tls=--tls
          ;;
      --secret-web-certs)
          shift
          secretWebCerts=${1:-web-certs}
          ;;
      --target-hostname)
          shift
          target_hostname=$1
          ;;
      --nomad-root)
          shift
          nomadRoot=$1
          ;;
      --chown-root)
          shift
          chownRoot=$1
          ;;
      *)
          echo "usage: $0 [--tls] [--nomad-root <pathToNomadRoot>] [--chown-root <pathForPrometheusVolumes>] [--target-hostname hostname]"
          echo
          echo "Env variables: target_hostname, nomadRoot"
          exit 0
          ;;
  esac
  shift
done

chownRoot=${chownRoot:-$nomadRoot/servers/$target_hostname}

echo "# Initial setup"
echo "To make kubectl work, for example for the test kubernetes"
echo "  export KUBECONFIG=/etc/kubernetes/admin.conf"

echo "# Helm install"
if [ -n updateDeploy ]; then
    cat > helm-tiller-serviceaccount.yaml <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF
    cat > prometheus-alertmanager-volume.yaml <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: prometheus-alertmanager
spec:
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: manual-alertmanager
  hostPath:
    path: $chownRoot/prometheus/alertmanager-volume
EOF

    cat > prometheus-server-volume.yaml <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: prometheus-server
spec:
  capacity:
    storage: 16Gi
  storageClassName: manual-prometheus
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  hostPath:
    path: $chownRoot/prometheus/server-volume
EOF

    cat > prometheus-values.yaml <<EOF
alertmanager:
  persistentVolume:
    storageClass: manual-alertmanager
  service:
    type: NodePort
server:
  persistentVolume:
    storageClass: manual-prometheus
  service:
    type: NodePort
EOF
fi

echo "  kubectl create -f helm-tiller-serviceaccount.yaml"
if [ -n "$tls" ] ; then
    echo "# secure heml as described in https://docs.helm.sh/using_helm/#using-ssl-between-helm-and-tiller"
    echo "# create certificates"
    echo "mkdir helm-certs"
    echo "cd helm-certs"
    echo "openssl genrsa -out ./ca.key.pem 4096"
    echo "openssl req -key ca.key.pem -new -x509 -days 7300 -sha256 -out ca.cert.pem -extensions v3_ca"
    echo "openssl genrsa -out ./tiller.key.pem 4096"
    echo "openssl genrsa -out ./helm.key.pem 4096"
    echo "openssl req -key tiller.key.pem -new -sha256 -out tiller.csr.pem"
    echo "openssl req -key helm.key.pem -new -sha256 -out helm.csr.pem"
    echo "openssl x509 -req -CA ca.cert.pem -CAkey ca.key.pem -CAcreateserial -in tiller.csr.pem -out tiller.cert.pem -days 365"
    echo "openssl x509 -req -CA ca.cert.pem -CAkey ca.key.pem -CAcreateserial -in helm.csr.pem -out helm.cert.pem  -days 365"
    echo "cp ca.cert.pem \$(helm home)/ca.pem"
    echo "cp helm.cert.pem \$(helm home)/cert.pem"
    echo "cp helm.key.pem \$(helm home)/key.pem"
    echo "# initialize helm"
    echo "helm init --override 'spec.template.spec.containers[0].command'='{/tiller,--storage=secret}' \\"
    echo "          --tiller-tls \\"
    echo "          --tiller-tls-verify \\"
    echo "          --tiller-tls-cert=cert.pem \\"
    echo "          --tiller-tls-key=key.pem \\"
    echo "          --tls-ca-cert=ca.pem \\"
    echo "          --service-account=tiller"
else
    echo "  helm init --service-account tiller"
fi
echo "# Prometheus setup"
echo "  kubectl create -f prometheus-alertmanager-volume.yaml"
echo "  kubectl create -f prometheus-server-volume.yaml"
echo "  helm install $tls --name prometheus -f prometheus-values.yaml stable/prometheus"
