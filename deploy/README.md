# NOMAD deployments

This in information on how to deploy the archive and analytics toolkit.
Some information is a bit specific to our own production and development systems.
In general scripts create files to do the installation and print out commands.
For security reasons they do not execute the deployment themselves.
You might want to do it manually, or deploy only part of the system.

## deploy/kubernetes.txt


## deploy/base

contains info for deploying the basic infrastructure: Docker, kubernetes and on the top of kubernetes (execute the baseSetup.sh script)

## deploy/frontend

## deploy/api

## deploy/container-manager

## Machine specific hints:
