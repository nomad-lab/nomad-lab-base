apiVersion: v1
kind: ReplicationController
metadata:
  name: normalizer-rc
  namespace: ${rootnamespace}
  labels:
    user: ${user}
    component: normalizer
spec:
  replicas: 1
  selector:
    config: "${config}"
    app: normalizer-server
  template:
    metadata:
      labels:
        user: ${user}
        config: "${config}"
        app: normalizer-server
    spec:
      containers:
      - image: labdev-nomad.esc.rzg.mpg.de:5000/nomadlab/nomadtool:v${version}
        name: normalizer
        command: 
        - java
        - -jar
        - /app/nomadTool-assembly-${version}.jar
        - normalize
        - --rabbitMQ
        - --normalizers
        - PrototypesNormalizer  
        env:
        - name: NOMAD_ENV
          value: "${config}"
        imagePullPolicy: IfNotPresent
        volumeMounts:
        - mountPath: "/raw-data"
          name: raw-data-volume
          readOnly: true
        - mountPath: "/parsed"
          name: parsed-data-volume
          readOnly: false
        - mountPath: "/normalized"
          name: normalized-data-volume
          readOnly: false
        - mountPath: "/temporary-downloads"
          name: temporary-downloads-data-volume
          readOnly: false
        - mountPath: "/scripts"
          name: scripts-data-volume
          readOnly: true
      volumes:
      - name: parsed-data-volume
        hostPath:
          path: "${nomad_lab.hostPaths.parsedRoot}"
      - name: raw-data-volume
        hostPath:
          path: "${nomad_lab.hostPaths.rawDataRoot}"
      - name: normalized-data-volume
        hostPath:
          path: "${nomad_lab.hostPaths.normalizedRoot}"
      - name: temporary-downloads-data-volume
        hostPath:
          path: "${nomad_lab.hostPaths.baseSharedTmp}/webservice-${config}"
      - name: scripts-data-volume
        hostPath:
          path: "${nomad_lab.hostPaths.baseSharedTmp}/scripts-${config}"
