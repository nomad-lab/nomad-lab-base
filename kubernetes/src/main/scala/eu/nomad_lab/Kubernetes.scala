/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import com.typesafe.config.{ Config, ConfigException, ConfigFactory, ConfigValueType, ConfigValue }
import com.typesafe.scalalogging.StrictLogging
import eu.{ nomad_lab => lab }
import org.json4s.JValue
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import scala.collection.mutable
import org.{ json4s => jsn }
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files

import scala.collection.mutable.ListBuffer

object Kubernetes extends StrictLogging {

  val usage = """
Usage:
  kuberneteTool [--help]
    [--config <config subtree to use>]
    [--component <component name, eg treeparser> [--component ...]]
    [--component-filepath <filepath to the component, defaults to KubernetesTemplate/{component}.yaml> [--component-filepath ...]]
    [--verbose]
    [--all | -A <Create kubernetes config file for all components>]
Creates actual Kubernetes files from the template
                """

  def appendFlattenedVals(config: Config, resMap: mutable.Map[String, String], baseVal: String = ""): Unit = {
    val entriesIt = config.entrySet().iterator()
    while (entriesIt.hasNext()) {
      val entry = entriesIt.next()
      val path = if (baseVal.isEmpty())
        entry.getKey()
      else
        baseVal + "." + entry.getKey
      val value = entry.getValue()
      value.valueType() match {
        case ConfigValueType.OBJECT =>
          appendFlattenedVals(config.getConfig(entry.getKey()), resMap, path)
        case _ =>
          resMap += (path -> value.unwrapped().toString())
      }
    }
  }

  def main(args: Array[String]): Unit = {

    if (args.length == 0) {
      println(usage)
      return
    }

    val varToReplaceRe = """\$\{([a-zA-Z][a-zA-Z0-9]*)\}""".r

    var list: List[String] = args.toList
    var configToUse: Option[String] = None
    var templateList: ListBuffer[Path] = ListBuffer()
    var verbose: Boolean = false

    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--all" | "-A" =>
          val listFiles = getListOfFiles("../KubernetesTemplate/")
          templateList ++= listFiles
        case "--config" =>
          if (list.isEmpty) {
            println(s"Error: missing config subtree name after --config. $usage")
            return
          }
          configToUse = Some(list.head)
          list = list.tail
        case "--component" =>
          if (list.isEmpty) {
            println(s"Error: missing component name after --component. $usage")
            return
          }
          val component = list.head
          list = list.tail
          templateList += Paths.get("../KubernetesTemplate", component + ".yaml.in")
        case "--component-filepath" =>
          if (list.isEmpty) {
            println(s"Error: missing path after --component-filepath. $usage")
            return
          }
          val path = list.head
          list = list.tail
          templateList += Paths.get(path)
        case "--verbose" =>
          verbose = true
        case _ =>
          println(s"Error: unexpected argument $arg. $usage")
          return
      }
    }

    val baseConfig: Config = ConfigFactory.load()
    val config: Config = configToUse match {
      case Some(conf) =>
        try {
          baseConfig.getConfig("overrides").getConfig(conf).withFallback(
            baseConfig.withValue(
              "nomad_lab.configurationToUse",
              com.typesafe.config.ConfigValueFactory.fromAnyRef(conf)
            )
          )
        } catch {
          case ex: ConfigException =>
            logger.warn(s"could not find config $conf, using default", ex)
            baseConfig
        }
      case None =>
        baseConfig
    }
    LocalEnv.defaultConfig = config
    import scala.language.postfixOps
    import sys.process._
    val user = "whoami" !!
    val uid = "id -u" !!
    val group = "docker"
    val gid = "getent group docker" #| "cut -d: -f3" !!
    val productionRun = "/prod-030"
    val version = lab.NomadCoreVersionInfo.version
    val repl = mutable.Map[String, String]()
    appendFlattenedVals(config, repl)
    repl ++= LocalEnv.defaultSettings.replacements
    repl += ("version" -> version)
    repl += ("treeParser" -> ("eu.nomad-laboratory/nomadtreeparserworker:v" + version))
    repl += ("calculationParser" -> ("eu.nomad-laboratory/nomadcalculationparserworker:v" + version))
    repl += ("normalizer" -> ("eu.nomad-laboratory/normalizer:v" + version))
    repl += ("user" -> user.trim)
    repl += ("uid" -> uid.trim)
    repl += ("group" -> group.trim)
    repl += ("gid" -> gid.trim)
    repl += ("parsedFolder" -> productionRun.trim)
    repl += ("normalizedFolder" -> (productionRun + "n").trim)
    repl += ("config" -> (configToUse match {
      case Some(c) => c
      case None => config.getString("nomad_lab.configurationToUse")
    }))

    if (templateList.isEmpty)
      logger.error(s"nothing to do, $usage")
    println(s"\nrootnamespace: ${repl("rootnamespace")}\n")
    for (template <- templateList) {
      val source = scala.io.Source.fromFile(template.toFile())
      val lines = try source.mkString finally source.close()
      val str = LocalEnv.makeReplacements(repl, lines)
      println(str)
      val target: Path = Paths.get("../kubeGen", template.getFileName().toString().dropRight(3))
      println(s"${template.toAbsolutePath().normalize()} -> ${target.toAbsolutePath().normalize()}")
      Files.createDirectories(target.getParent)
      printToFile(target.toFile()) { p => p.println(str) }
    }
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit): Unit = {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }

  def getListOfFiles(dir: String): List[Path] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter((f: File) => f.isFile && f.getName.endsWith(".yaml.in")).toList.map {
        (f: File) => f.toPath()
      }
    } else {
      List[Path]()
    }
  }
}
