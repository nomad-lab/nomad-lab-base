'use strict';
const
  async = require('async'),
  request = require('request'),
  kubernetesMaster = process.argv[2],namespace = process.argv[3], componentName = process.argv[4];

function createNamespaceConfig(user) {
  const namespace =  {
    "kind": "Namespace",    
    "apiVersion": "v1",
    "metadata": {
     "name": user
    }
  };
  return(namespace);
};

function createServiceConfig(user) {
  const service= {
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
      "name": "Beaker",
      "namespace": user,
      "labels": {
        "component": "Beaker"
      }
    },
    "spec":{
      "type": "NodePort",
      "ports":[{
        "port": "8801",
        "name": "beaker-port",
        "targetPort": "8801",
        "protocol": "TCP"
      }],
      "selector":{
        "user": user,
        "app": "Beaker"
        }
    }
  };
  return(service);
};

function createRcControllerConfig(user) {
  const rcController = {
    "apiVersion": "v1",
    "kind": "ReplicationController",
    "metadata": {
      "name": "beaker-rc",
      "namespace": user,
      "labels": {
        "user": user,
        "app": "Beaker"
      },
    },
    "spec": {
      "replicas": 1,
      "selector":{
        "user": user,
        "app": "Beaker"
        },
      "template": {
        "metadata": {
          "labels": {
            "user":user,
            "app": "Beaker"
          }
        },
        "spec": {
          "containers": [
            {
              "image": "labdev-nomad.esc.rzg.mpg.de:5000/nomadlab/notebook:v1.3.0-17-g7da6387-dirty",
              "name": "Beaker",
              "ports": [
                {
                  "containerPort": 8801,
                  "name": "inspection-port",
                  "protocol": "TCP"
                }
              ],
              "imagePullPolicy": "IfNotPresent",
              "volumeMounts": [
                {
                  "mountPath": "/raw-data",
                  "name": "raw-data-volume",
                  "readOnly": true
                },
                {
                  "mountPath": "/parsed",
                  "name": "parsed-data-volume",
                  "readOnly": true
                },
                {
                  "mountPath": "/normalized",
                  "name": "normalized-data-volume",
                  "readOnly": true
                },
                {
                  "mountPath": "/data",
                  "name": "user-data-volume"
                },
                {
                  "mountPath": "/home/beaker/notebooks",
                  "name": "notebooks-volume"
                }
              ]
            }
          ],
          volumes: [
            {
              "name": "parsed-data-volume",
              "hostPath": { "path": "/nomad/nomadlab/parsed" }
            },
            {
              "name": "raw-data-volume",
              "hostPath": { "path": "/nomad/nomadlab/raw-data"}
            },
            {
              "name": "normalized-data-volume",
              "hostPath": { "path": "/nomad/nomadlab/normalized" }
            },
            {
              "name": "user-data-volume",
              "hostPath": { "path": "/nomad/nomadlab/kubernetes/user-data/{{user}}" }
            },
            {
              "name": "notebooks-volume",
              "hostPath": { "path": "/nomad/nomadlab/beaker-notebooks/notebooks"}
            }
          ]
        }
      }
    }
  };
  return(rcController);
}

function getConfig(component,componentName) {
  if(component === "namespace")
    return(createNamespaceConfig(componentName))
  else if (component === "service")
   return(createServiceConfig(componentName))
  else if (component === "replicationController")
   return(createRcControllerConfig(componentName))
};


async.waterfall([
  
  // get the existing namespace, if present
  function(next) {
    request.get("http://"+kubernetesMaster+"/api/v1/namespaces/" + namespace, next);
  },
  
  //create the Namespace if its not present
  function(res, body, next) {
    console.log(res.statusCode, body);      
    if (res.statusCode !== 200) {
      request.post({
        url:"http://"+kubernetesMaster+"/api/v1/namespaces",
        body: JSON.stringify(getConfig("namespace",namespace))
      }, next);
    }
    else next(null, res,body)
  },
  //Check if namespace created successfully and get service, if present
  function(res, body, next) {
    console.log(res.statusCode, body);  
    if (res.statusCode !== 200 && res.statusCode !== 201) {
      return("Namespace could not be created!");
    }
    else{
      request.get("http://"+kubernetesMaster+"/api/v1/namespaces/" + namespace + "/services/" + componentName, next)
    }  
  },
  //Create the service if its not present 
  function(res, body, next) {
    console.log(res.statusCode, body);
    if (res.statusCode !== 200) {
      request.post({
        url:"http://"+kubernetesMaster+"/api/v1/namespaces/" + namespace + "/services",
        body: JSON.stringify(getConfig("service",componentName))
      }, next);
    }
    else next(null, res,body)
  },
  // Check if service created successfully and Get rc-controller, if present
  function(res, body, next) {
    console.log(res.statusCode, body);
    if (res.statusCode !== 200 && res.statusCode !== 201) {
      return("Service could not be created!");
    }
    else{
      request.get("http://"+kubernetesMaster+"/api/v1/namespaces/" + namespace + "/replicationcontrollers/" + componentName, next)
    }  
  },
    //Create the rc-controller if its not present 
  function(res, body, next) {
    console.log(res.statusCode, body);
    if (res.statusCode !== 200) {
      request.post({
        url:"http://"+kubernetesMaster+"/api/v1/namespaces/" + namespace + "/replicationcontrollers",
        body: JSON.stringify(getConfig("replicationController",componentName))
      }, next);
    }
    else next(null, res,body)
  }
], function(err, res, body) {
  console.log(res.statusCode, body);
});


