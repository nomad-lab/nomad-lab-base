package eu.nomad_lab.fileservice

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Directives.concat
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }
import eu.nomad_lab.webservice_base.Webservice
import io.swagger.models.Scheme

import scala.concurrent.{ ExecutionContext, Future }

object FileWebservice extends App with Webservice {

  implicit val system: ActorSystem = ActorSystem("fileServingAkkaHttpServer", config)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher
  lazy val config: Config = LocalEnv.defaultConfig
  lazy val metaData: MetaInfoEnv = KnownMetaInfoEnvs.publicMeta

  override val apiDocumentationClasses: Set[Class[_]] = Set(classOf[DownloadApi])
  override val skipDefinitions = Seq("Function1", "Function1RequestContextFutureRouteResult")
  override val apiDescription = "Provides functions to access and download files in the raw data " +
    "archives of the NOMAD project. These zip-archives contain the original files that have been " +
    "uploaded to the NOMAD Repository by the users."
  override val apiVersion = "0.1"
  override val apiTitle = "NOMAD FileServing API"

  private val downloadApi = new DownloadApi(config)

  override def serviceRoute = concat(downloadApi.routes)

  val interface = config.getString("nomad_lab.archive.file_service.interface")
  val port = config.getInt("nomad_lab.archive.file_service.port")
  val rootPathString = config.getString("nomad_lab.archive.file_service.rootPath")
  val hostName = config.getString("nomad_lab.archive.file_service.hostName")
  val protocol = config.getString("nomad_lab.archive.file_service.protocol") match {
    case "" => if (hostName.startsWith("localhost:")) Scheme.HTTP else Scheme.HTTPS
    case "http://" | "http" => Scheme.HTTP
    case "https://" | "https" => Scheme.HTTPS
    case x => throw new UnsupportedOperationException(s"protocol $x is not supported!")
  }
  val fullRoute = fullRoutes(
    serviceBaseName = "files",
    rootPath = rootPathString.split("/").filter(_ != ""),
    hostName = hostName,
    includeSwagger = true,
    protocol = protocol
  )
  val serverBindingFuture: Future[ServerBinding] = Http().bindAndHandle(
    fullRoute, interface = interface, port = port
  )

  println(s"Server online at http://$interface:$port/\n")

}
