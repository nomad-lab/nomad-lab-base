package eu.nomad_lab.fileservice

import java.net.URI
import java.nio.file.Paths
import javax.ws.rs.{ GET, POST, Path, QueryParam }

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.{ Directives, Route }
import com.typesafe.config.Config
import io.swagger.annotations._
import spray.json.DefaultJsonProtocol

import scala.collection.JavaConverters._
import scala.concurrent.{ ExecutionContext, Future }

case class FileRequest(files: Seq[String], archiveName: String)

protected trait FileServiceSprayJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val fileRequestFormat = jsonFormat2(FileRequest)
}

@Api(
  value = "File Serving API for raw data",
  produces = "vnd.api+json"
)
@Path("/rawdata")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class DownloadApi(config: Config)(implicit private val exec: ExecutionContext)
    extends Directives with FileServiceSprayJsonSupport {
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult

  private val archiveHandler = new ArchiveHandler(
    config.getStringList("nomad_lab.archive.file_service.fileLocations").asScala.map { x =>
      Paths.get(new URI(x))
    }
  )

  def routes: Route = pathPrefix("rawdata") {
    concat(
      path("list") {
        parameters('directory) { dirUri =>
          get {
            listDirectory(dirUri)
          }
        }
      },
      path("get") {
        parameters('file) { fileUri =>
          get {
            getFile(fileUri)
          }
        }
      },
      path("multi_get") {
        entity(as[FileRequest]) { request =>
          getZippedFiles(request)
        }
      }
    )
  }

  @GET @Path("/list")
  @ApiOperation(
    value = "list the content of a directory in the given rawdata archive"
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def listDirectory(
    @ApiParam(value = "NOMAD URI of the directory", required = true)@QueryParam("directory") directory: String
  ): Route = onComplete {
    val (archiveId, dirPath) = directory.indexOf("/") match {
      case -1 => (directory, "")
      case x => val y = directory.splitAt(x); (y._1, y._2.substring(1))
    }
    Future {
      archiveHandler.getDirectoryListing(archiveId, dirPath)
    }
  }(resolveQueryResult(_))

  @GET @Path("/get")
  @ApiOperation(
    value = "get a single file specified by its NOMAD URI"
  //TODO: determine correct response data type header
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def getFile(
    @ApiParam(value = "NOMAD URI of the file", required = true)@QueryParam("file") directory: String
  ): Route = onComplete {
    val (archiveId, dirPath) = directory.indexOf("/") match {
      case -1 => (directory, "")
      case x => val y = directory.splitAt(x); (y._1, y._2.substring(1))
    }
    Future {
      archiveHandler.getFile(archiveId, dirPath)
    }
  }(resolveQueryResult)

  @POST @Path("/multi_get")
  @ApiOperation(
    value = "get a set of file specified by their NOMAD URI as a zip-archive",
    produces = "application/zip"
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def getZippedFiles(
    @ApiParam(value = "NOMAD URIs of the files", required = true) requestedFiles: FileRequest
  ): Route = ???

}