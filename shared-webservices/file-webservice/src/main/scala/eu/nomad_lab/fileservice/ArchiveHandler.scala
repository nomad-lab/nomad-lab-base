package eu.nomad_lab.fileservice

import java.nio.file.Path

import akka.http.scaladsl.model._
import akka.stream.scaladsl.StreamConverters
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.elasticsearch.ApiCallException
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.apache.commons.compress.archivers.zip.{ ZipArchiveEntry, ZipFile }
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.JValue

class ArchiveHandler(archiveLocations: Seq[Path]) {

  private def getArchive(archiveId: String): ZipFile = {

    //TODO: Do we have a dedicated ArchiveID validity check function?
    if (archiveId(0) != 'R' || archiveId.size != 29)
      throw ApiCallException(s"'$archiveId' doesn't seem to be a valid archive identifier")
    val prefix = s"R${archiveId.substring(1, 3)}"
    val archive = s"${archiveId.substring(1)}.zip"
    val fetchArchive: PartialFunction[Path, Path] = {
      case x if x.resolve(s"$prefix/R$archive").toFile.exists() => x.resolve(s"$prefix/R$archive")
      case x if x.resolve(s"$prefix/r$archive").toFile.exists() => x.resolve(s"$prefix/r$archive")
    }

    archiveLocations.collectFirst(fetchArchive) match {
      case Some(path) => new ZipFile(path.toFile)
      case None => throw ApiCallException(s"archive $archiveId not found on file system!")
    }
  }

  def getDirectoryListing(archiveId: String, pathInArchive: String): ResponseData[DirectoryContent] = {
    val archive = getArchive(archiveId)
    val files = collection.mutable.Map[String, ZipArchiveEntry]()
    val subFolders = collection.mutable.Map[String, (Long, Long)]()
    val iterator = archive.getEntries
    var found = false
    val subPath = s"$archiveId/data/$pathInArchive" + (if (pathInArchive.nonEmpty) "/" else "")
    //TODO: handle cases where the file size cannot be read
    while (iterator.hasMoreElements) {
      val nextEntry = iterator.nextElement()
      val name = nextEntry.getName
      found = found || (name + "/" == subPath)
      if (name.startsWith(subPath)) {
        val stripped = name.stripPrefix(subPath)
        if (stripped.indexOf("/") == -1) {
          files(stripped) = nextEntry
        } else if (stripped.indexOf("/") > 0) {
          val key = stripped.substring(0, stripped.indexOf("/"))
          val stats = subFolders.getOrElse(key, (0l, 0l))
          subFolders(key) = (stats._1 + 1, stats._2.toLong + nextEntry.getSize)
        }
      }
    }
    if (files.nonEmpty || subFolders.nonEmpty) {
      val fileInfo = files.map { x => FileInfo(x._1, x._2.getSize) }.toSeq
      val folderInfo = subFolders.map(x => FolderInfo(x._1, x._2._1, x._2._2)).toSeq
      ResponseData(Seq(DirectoryContent(archiveId, pathInArchive, fileInfo, folderInfo)), Map())

    } else if (found) {
      throw ApiCallException(s"requested folder '$pathInArchive' in archive $archiveId is a file!")
    } else {
      throw ApiCallException(s"folder '$pathInArchive' does not exist (or is empty) " +
        s"in archive $archiveId")
    }
  }

  def getFile(archiveId: String, pathInArchive: String): HttpResponse = {
    val archive = getArchive(archiveId)
    val subPath = s"$archiveId/data/$pathInArchive"
    val entry = archive.getEntry(subPath)
    if (entry != null) {
      val fileStream = StreamConverters.fromInputStream(() => archive.getInputStream(entry))
      //TODO: add an appropriate content type?
      val entity = HttpEntity(ContentTypes.NoContentType, fileStream)
      HttpResponse(status = StatusCodes.OK, entity = entity)
    } else {
      throw ApiCallException(s"could not find path '$pathInArchive' in archive '$archiveId'")
    }
  }
}

case class FileInfo(name: String, size: Long)
case class FolderInfo(name: String, totalFiles: Long, totalSize: Long)

case class DirectoryContent(archiveId: String, dirName: String, files: Seq[FileInfo],
    subFolders: Seq[FolderInfo]) extends BaseValue {

  override def typeStr: String = "RawDataArchiveDirectory"

  override def idStr: String = s"$archiveId/$dirName"

  override def attributes: Map[String, JValue] = Map(
    "archive_identifier" -> decompose(archiveId),
    "number_of_files" -> decompose(files.length),
    "total_file_size" -> decompose(files.foldRight(0l)((x, y) => x.size + y)),
    "directory_name" -> decompose(dirName),
    "files" -> decompose(files),
    "sub_folders" -> decompose(subFolders)
  )
}