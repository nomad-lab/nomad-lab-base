from __future__ import print_function
import re
"""Very simplistic test that checks if at least the expected metainfos are somehow handled by the parsers
go to the parser directory and run

    find . -path \*/target -prune -a -name .py -o -name \*.py | python ../test/parserTest.py --no-usage

to get the list of meta infos.
This does not check that they are correctly handled, and it requires also QM/DFT oriented values that do not make
sense for empirical forecfields.
"""

repoRequired = [
    "program_name",
    "atom_labels",
    "atom_positions",
    "program_version",
    "xc_functional_name",
    "electronic_structure_method",
    "energy_total",
    "simulation_cell",
    "configuration_periodic_dimensions"]

encReq = [
    'xc_functional_name',
    'section_system',
    'k_mesh_points',
    'k_mesh_weights',
    'atom_labels',
    'simulation_cell',
    'section_method',
    'section_xc_functionals',
    'configuration_periodic_dimensions',
    'atom_positions',
    'program_basis_set_type',
    'section_frame_sequence',
    'section_sampling_method',
    'single_configuration_calculation_to_method_ref',
    'single_configuration_calculation_to_system_ref',
    'atom_forces',
    'frame_sequence_to_frames_ref',
    'frame_sequence_to_sampling_method_ref',
    'eigenvalues_kpoints',
    'eigenvalues_values',
    'eigenvalues_occupation',
    'band_k_points',
    'band_energies',
    'band_segm_start_end',
    'band_segm_labels',
    'dos_energies',
    'dos_values',
    'smearing_kind',
    'smearing_width'
]

enc2 = ['smearing_kind', 'smearing_width']

def repoRequiredGrep(req):
    return "(?:\\(?P<|[\"'])(" + "|".join(req) + ")(?:[\"']|>|__)"

def missingRequired(fIn, req):
    missingReq = set(req)
    reqRe = re.compile(repoRequiredGrep(req))
    while True:
        line = fIn.readline()
        if not line: break
        for m in reqRe.finditer(line):
            g = m.group(1)
            if g in missingReq:
                missingReq.remove(g)
    return missingReq

def extractIndexes(fIn, idxs = None):
    indexRe = re.compile("\\[\\s*[\"']([a-zA-Z_0-9]+)[\"']\\s*\\]")
    pathRe = re.compile("[\"']([a-zA-Z_0-9]*/[a-zA-Z_0-9/]+)[\"']")
    if idxs is None:
        idxs = set()
    while True:
        line = fIn.readline()
        if not line: break
        for m in indexRe.finditer(line):
            g = m.group(1)
            idxs.add(g)
        for m in pathRe.finditer(line):
            g = m.group(1)
            idxs.add(g)
    return idxs

def checkParser(files):
    missingReq = repoRequired + encReq
    for f in files:
        with open(f) as fIn:
            missingReq = missingRequired(fIn, missingReq)
    return missingReq

if __name__ == "__main__":
    usage = """usage: parserTest.py [--no-usage] [--help]
  checks if the expected meta infos are use in the files given on stdin.
  Normally you can use it by going to the directory or the parser repository (parsers/<code>) and run
    find . -path \*/target -prune -a -name .py -o -name \*.py | python ../test/parserTest.py --no-usage
"""
    import sys
    printUsage = True
    for arg in sys.argv[1:]:
        if arg == "--no-usage":
            printUsage = False
        elif arg == "--help":
            print(usage)
            sys.exit(0)
        else:
            print("unexpected argument",arg)
            print(usage)
            sys.exit(1)
    if printUsage:
        print(usage)
        print("  reading stdin, Ctrl-D to stop...")
    lines = sys.stdin.read().splitlines()
    print("...done")
    missing = list(checkParser(lines))
    missing.sort()
    print("\nMissing metainfo:")
    for m in missing:
        print(" ", m)
