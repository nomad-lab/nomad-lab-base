NOMAD Lab Notebook
==================

Here we create the docker image for the notebook where one can do analyses using the [NOMAD Lab](http://nomad-lab.eu/) infrastructure.

This is based on the [beaker notebook](http://beakernotebook.com/), to create the docker image just use sbt in the main directory (one up) and execute notebook/docker.
