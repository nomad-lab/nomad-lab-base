if [[ -n "$NOMAD_USER" ]] ; then
  ln -s "/data/shared/$NOMAD_USER" /home/beaker/myshared
  ln -s "/data/private/$NOMAD_USER" /home/beaker/myprivate
fi
mkdir -p /home/beaker/.beaker/v1/web/tmp
chown -R beaker:beaker /home/beaker/.beaker/v1/web
su -m beaker -c "export PATH=$PATH:/usr/sbin && /home/beaker/src/core/beaker.command --listen-interface 0.0.0.0"
