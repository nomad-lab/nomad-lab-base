/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parquet

import java.nio.charset.StandardCharsets

import collection.JavaConverters._
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.meta.{ MetaInfoEnv, MetaInfoRecord }
import eu.nomad_lab.parsers.GenericBackend.InvalidAssignementException
import eu.nomad_lab.parsers.ParserBackendExternal
import eu.nomad_lab.parsers.ParseResult
import eu.nomad_lab.h5
import eu.nomad_lab.resolve.Archive
import org.apache.hadoop.conf.Configuration
import org.apache.parquet.hadoop.api.WriteSupport
import org.apache.parquet.hadoop.api.WriteSupport.WriteContext
import org.apache.parquet.io.api.{ Binary, RecordConsumer }
import org.apache.parquet.schema.MessageType
import org.json4s.{ JNothing, JObject, JString, JValue }
import org.json4s.JsonAST._
import ucar.ma2.{ Array => NArray, IndexIterator => NIndexIterator }

import scala.collection.mutable
import org.apache.hadoop.fs
import org.apache.parquet.hadoop.ParquetWriter
import org.apache.parquet.hadoop.ParquetFileWriter
import org.apache.parquet.column.ParquetProperties
import com.typesafe.scalalogging.StrictLogging

object ParquetArchiveWriter {
  import org.apache.hadoop.conf.Configuration
  //import org.apache.parquet.schema.MessageType
  import org.apache.parquet.hadoop.metadata.CompressionCodecName.UNCOMPRESSED
  import org.apache.parquet.hadoop.metadata.CompressionCodecName.LZO
  import org.apache.parquet.hadoop.ParquetWriter.DEFAULT_BLOCK_SIZE;
  import org.apache.parquet.hadoop.ParquetWriter.DEFAULT_PAGE_SIZE;

  def builder(file: fs.Path): ParquetArchiveWriterBuilder = {
    new ParquetArchiveWriterBuilder(file)
  }

  def dumpToParquet(archive: Archive, targetPath: String): Unit = {
    val configuration = new Configuration();

    val outP = new fs.Path(targetPath)
    val sObj = ParquetSchemaGenerator.defaultSchema
    val writeSupport: ParquetArchiveWriter = new ParquetArchiveWriter(sObj.schema, sObj.indexMap, sObj.metaInfoEnv)
    val encodingProps: ParquetProperties = ParquetProperties.builder().
      withPageSize(DEFAULT_PAGE_SIZE).
      //withDictionaryPageSize(dictionaryPageSize).
      withDictionaryEncoding(true).
      //withWriterVersion(writerVersion).
      build()
    val parquetWriter = builder(outP).
      withWriteMode(ParquetFileWriter.Mode.OVERWRITE).
      withConf(configuration).
      withCompressionCodec(UNCOMPRESSED).
      withDictionaryEncoding(true).
      enableValidation().build()
    /*
    val parquetWriter = new ParquetWriter[Archive](
      outP, // file: fs.Path
      ParquetFileWriter.Mode.OVERWRITE, // mode
      writeSupport, // write support
      UNCOMPRESSED, // compressionCodecName
      DEFAULT_BLOCK_SIZE, // blockSize
      true, // validating
      configuration, // configuration
      ParquetWriter.MAX_PADDING_SIZE_DEFAULT, // maxPadding size
      encodingProps
    )*/
    parquetWriter.write(archive)
    parquetWriter.close()
  }
}

class ParquetArchiveWriterBuilder(file: fs.Path) extends ParquetWriter.Builder[Archive, ParquetArchiveWriterBuilder](file) {
  var schemaObj: Option[ParquetSchemaGenerator] = None

  def withSchemaGenerator(sObj: ParquetSchemaGenerator): ParquetArchiveWriterBuilder = {
    schemaObj = Some(sObj)
    this
  }

  override def self(): ParquetArchiveWriterBuilder = this

  override protected def getWriteSupport(conf: Configuration): WriteSupport[Archive] = {
    val sObj = schemaObj match {
      case Some(s) => s
      case None => ParquetSchemaGenerator.defaultSchema
    }
    new ParquetArchiveWriter(sObj.schema, sObj.indexMap, sObj.metaInfoEnv)
  }
}

class ParquetArchiveWriter(
    val schema: MessageType,
    val indexMap: Map[String, Int],
    val metaInfoEnv: MetaInfoEnv
) extends WriteSupport[Archive] {
  var backend: Option[ParquetWriterBackend] = None
  val scanner = new h5.H5EagerScanner(h5.ScanType.RowScan, sortMeta = true)

  override def init(conf: Configuration): WriteContext = {
    val mV: String = eu.nomad_lab.NomadCoreVersionInfo.metaInfoVersion
    val cV: String = eu.nomad_lab.NomadCoreVersionInfo.version
    var extraMetaData = mutable.Map(
      "nomad.meta.version" -> mV,
      "nomad.core.version" -> cV
    )
    new WriteSupport.WriteContext(schema, extraMetaData.asJava)
  }

  override def prepareForWrite(rc: RecordConsumer): Unit = {
    backend = Some(new ParquetWriterBackend(schema, indexMap, metaInfoEnv, rc))
  }

  override def write(archive: Archive): Unit = {
    backend match {
      case Some(b) =>
        val visitor = new h5.EmitEventsVisitor(b)
        scanner.scanResolvedRef(archive, visitor)
      case None =>
        throw new Exception(s"write called before prepareForWrite")
    }
  }
}

class ParquetWriterBackend(
    val schema: MessageType,
    val indexMap: Map[String, Int],
    val metaInfoEnv: MetaInfoEnv,
    val recordConsumer: RecordConsumer
) extends ParserBackendExternal {
  val _openSections = mutable.Set[(String, Long)]()
  var lastOpenField: Option[(String, Int)] = None //Parquet allows only one field to be open at a time
  val openFields: mutable.Stack[(String, Int)] = mutable.Stack()

  def fieldManager(name: String, index: Int) = {
    //    println(s"field: ${name}")
    //    println(s"OpenFields: ${openFields}")
    if (openFields.isEmpty) {
      recordConsumer.startField(name, index)
      openFields.push((name, index))
    } else {
      val metaInfo = metaInfoEnv.metaInfoRecordForName(name).get
      val parentSection = if (name == "calculation_context") "archive_context" else metaInfoEnv.parentSectionName(name).getOrElse("calculation_context")
      //println(s"parentSection: $parentSection")
      var handled = false
      while (!handled && openFields.nonEmpty) {
        if (name == openFields.top._1) {
          handled = true
        } else if (parentSection == openFields.top._1) {
          openFields.push((name, index))
          recordConsumer.startField(name, index)
          handled = true
        } else {
          val top = openFields.pop()
          recordConsumer.endField(top._1, top._2)
        }
      }
      if (!handled) {
        openFields.push((name, index))
        recordConsumer.startField(name, index)
      }
    }
    //println(s"OpenFields: ${openFields}")
  }

  def endLastFields() = {
    while (openFields.nonEmpty) {
      val top = openFields.pop()
      //println(s"closing $top")
      recordConsumer.endField(top._1, top._2)
    }
  }

  /**
   * Started a parsing session
   */
  override def startedParsingSession(mainFileUri: Option[String], parserInfo: JValue, parserStatus: Option[ParseResult.Value], parserErrors: JValue): Unit = {
    //println("startedParsingSession")
    recordConsumer.startMessage()
    //    recordConsumer.startField("type", index)
    //    recordConsumer.addBinary(Binary.fromString("""nomad_info_data_parquet_1_0"""))
    //    recordConsumer.endField("type", index)
    //    mainFileUri match {
    //      case Some(uri) =>
    //        recordConsumer.startField("mainFileUri", index)
    //        recordConsumer.addBinary(Binary.fromString(uri))
    //        recordConsumer.endField("mainFileUri", index)
    //      case None => ()
    //    }
    //    parserInfo match {
    //      case JNothing => ()
    //      case _ =>
    //        recordConsumer.startField("parserInfo", index)
    //        recordConsumer.addBinary(Binary.fromConstantByteArray(JsonUtils.normalizedUtf8(parserInfo)))
    //        recordConsumer.endField("parserInfo", index)
    //    }
    //    parserStatus match {
    //      case None => ()
    //      case status =>
    //        recordConsumer.startField("parseResult", index)
    //        recordConsumer.addBinary(Binary.fromString(status.toString))
    //        recordConsumer.endField("parseResult", index)
    //    }
    //
    //    recordConsumer.startField("parserErrors", index)
    //    recordConsumer.addBinary(Binary.fromConstantByteArray(JsonUtils.normalizedUtf8(parserErrors)))
    //    recordConsumer.endField("parserErrors", index)
  }

  /**
   * finished a parsing session
   */
  override def finishedParsingSession(parserStatus: Option[ParseResult.Value], parserErrors: JValue, mainFileUri: Option[String], parserInfo: JValue, parsingStats: Map[String, Long]): Unit = {
    endLastFields()
    recordConsumer.endMessage()
  }

  def getIndex(metaInfo: String): Int = {
    indexMap(metaInfo)
  }

  /**
   * Adds a json value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addValue(metaName: String, value: JValue, gIndex: Long): Unit = {
    //println(s"addValue: $metaName")
    val metaInfo = metaInfoEnv.metaInfoRecordForName(metaName).get
    val index = getIndex(metaName)
    fieldManager(metaName, index)
    value match {
      case JBool(b) =>
        recordConsumer.addBoolean(b)
      case JInt(i) =>
        if (metaInfo.dtypeStr.contains("i") || metaInfo.dtypeStr.contains("i32"))
          recordConsumer.addInteger(i.intValue())
        else
          recordConsumer.addLong(i.longValue())
      case JDecimal(d) =>
        if (metaInfo.dtypeStr.contains("f32"))
          recordConsumer.addFloat(d.floatValue())
        else
          recordConsumer.addDouble(d.doubleValue())
      case JDouble(d) =>
        recordConsumer.addDouble(d)
      case JString(s) =>
        recordConsumer.addBinary(Binary.fromString(s))
      case JObject(o) =>
        recordConsumer.addBinary(Binary.fromConstantByteArray(JsonUtils.normalizedUtf8(value)))
      case JNothing =>
        None
      case _ =>
        throw new Exception //InvalidAssignementException(metaInfoEnv, s"invalid value $value when expecting integer")
    }
    //recordConsumer.endField(metaName, index)
  }

  /**
   * Adds a floating point value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addRealValue(metaName: String, value: Double, gIndex: Long): Unit = {
    val index = getIndex(metaName)
    fieldManager(metaName, index)
    recordConsumer.addDouble(value)
  }

  /**
   * Adds a new array of the given size corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   * The array is unitialized.
   */
  override def addArray(metaName: String, shape: Seq[Long], gIndex: Long): Unit = {
    val index = getIndex(metaName)
    fieldManager(metaName, index)
    recordConsumer.startGroup()
    val indexOfShape = getIndex("shape") // The shape is the first element in the group so its index will be 0.
    recordConsumer.startField("shape", indexOfShape)
    shape.foreach(recordConsumer.addLong)
    recordConsumer.endField("shape", indexOfShape)
    if (shape.foldLeft(1L)(_ * _) > 0) {
      val indexOfValues = getIndex("values")
      recordConsumer.startField("values", indexOfValues)
    }
  }

  override def closeArray(metaName: String, shape: Seq[Long], gIndex: Long): Unit = {
    if (shape.foldLeft(1L)(_ * _) > 0) {
      val indexOfValues = getIndex("values")
      recordConsumer.endField("values", indexOfValues)
    }
    //val index = getIndex(metaName)
    recordConsumer.endGroup()
  }
  /**
   * Adds values to the last array added
   */
  override def setArrayValues(metaName: String, values: NArray, offset: Option[Seq[Long]], gIndex: Long): Unit = {
    val metaInfo = metaInfoEnv.metaInfoRecordForName(metaName) match {
      case Some(r) => r
      case None => throw new Exception(s"could not find meta info $metaName")
    }
    val it = values.getIndexIterator()

    while (it.hasNext()) {
      metaInfo.dtypeStr match {
        case Some(d) =>
          d match {
            case "f" | "f64" =>
              recordConsumer.addDouble(it.getDoubleNext())
            case "f32" =>
              recordConsumer.addFloat(it.getFloatNext())
            case "i" | "i32" =>
              recordConsumer.addInteger(it.getIntNext())
            case "r" | "i64" =>
              recordConsumer.addLong(it.getLongNext())
            case "b" =>
              recordConsumer.addBoolean(it.getBooleanNext())
            case "C" =>
              recordConsumer.addBinary(Binary.fromString(it.getObjectNext().toString))
            case "D" =>
              recordConsumer.addBinary(Binary.fromString(JsonUtils.normalizedStr(it.getObjectNext() match {
                case o: org.json4s.JValue => o
              })))
            case c =>
              throw new Exception(s"Unknown meta info type $c")
          }
        case None =>
          throw new Exception(s"no dtypeStr in meta info $metaName")
      }
    }

  }

  /**
   * Opens and adds an array value; alternative of addArray, setArrayValues and closeArray
   */
  override def addArrayValues(metaName: String, values: NArray, gIndex: Long): Unit = {
    val metaInfo = metaInfoEnv.metaInfoRecordForName(metaName) match {
      case Some(r) => r
      case None => throw new Exception(s"could not find meta info $metaName")
    }
    val index = getIndex(metaName)
    fieldManager(metaName, index)
    recordConsumer.startGroup()
    val indexOfShape = getIndex("shape") // The shape is the first element in the group so its index will be 0.
    recordConsumer.startField("shape", indexOfShape)
    for (s <- values.getShape) {
      recordConsumer.addLong(s)
    }
    recordConsumer.endField("shape", indexOfShape)

    if (values.getSize > 0) {
      val indexOfValues = getIndex("values")
      val it = values.getIndexIterator()
      recordConsumer.startField("values", indexOfValues)
      while (it.hasNext()) {
        metaInfo.dtypeStr match {
          case Some(d) =>
            d match {
              case "f" | "f64" =>
                recordConsumer.addDouble(it.getDoubleNext())
              case "f32" =>
                recordConsumer.addFloat(it.getFloatNext())
              case "i" | "i32" =>
                recordConsumer.addInteger(it.getIntNext())
              case "r" | "i64" =>
                recordConsumer.addLong(it.getLongNext())
              case "b" =>
                recordConsumer.addBoolean(it.getBooleanNext())
              case "C" =>
                recordConsumer.addBinary(Binary.fromString(it.getObjectNext().toString))
              case "D" =>
                recordConsumer.addBinary(Binary.fromString(JsonUtils.normalizedStr(it.getObjectNext() match {
                  case o: org.json4s.JValue => o
                })))
              case c =>
                throw new Exception(s"Unknown meta info type $c")
            }
          case None =>
            throw new Exception(s"no dtypeStr in meta info $metaName")
        }
      }
      recordConsumer.endField("values", indexOfValues)
    }
    recordConsumer.endGroup()
  }

  /**
   * returns the sections that are still open
   *
   * sections are identified by metaName and their gIndex
   */
  def openSections(): Iterator[(String, Long)] = _openSections.iterator

  /**
   * returns information on an open section (for debugging purposes)
   */
  def sectionInfo(metaName: String, gIndex: Long): String = {
    if (_openSections.contains(metaName -> gIndex))
      s"section $metaName, gIndex $gIndex is open"
    else
      s"section $metaName, gIndex $gIndex is closed"
  }

  /**
   * closes a section
   *
   * after this no other value can be added to the section.
   * metaName is the name of the meta info, gIndex the index of the section
   */
  def closeSection(metaName: String, gIndex: Long): Unit = {
    //println(s"CloseSection called: $metaName")
    _openSections -= (metaName -> gIndex)
    if (openFields.head._1 != metaName) {
      val top = openFields.pop()
      recordConsumer.endField(top._1, top._2)
    }
    //val index = getIndex(metaName)
    recordConsumer.endGroup()
    //    recordConsumer.endField(metaName, index)
  }

  /**
   * Informs tha backend that a section with the given gIndex should be opened
   *
   * The index is assumed to be unused, it is an error to reopen an existing section.
   */
  def openSectionWithGIndex(metaName: String, gIndex: Long): Unit = {
    //println(s" OpenSection called: $metaName  ")
    _openSections += (metaName -> gIndex)
    val index = getIndex(metaName)
    fieldManager(metaName, index)
    recordConsumer.startGroup()
    val in = getIndex("c_index") //The index of c_index should always be 0
    recordConsumer.startField("c_index", in)
    recordConsumer.addLong(gIndex)
    recordConsumer.endField("c_index", in)
  }

  def backendInfo: JValue = {
    JObject(
      ("backendType" -> JString(getClass().getName())) :: Nil
    )
  }

  def cleanup(): Unit = {}

  /**
   * sets info values of an open section.
   *
   * references should be references to gIndex of the root sections this section refers to.
   */
  override def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = ()
}
