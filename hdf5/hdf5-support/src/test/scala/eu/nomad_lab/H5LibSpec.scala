/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification
import java.nio.file.Files
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import com.typesafe.scalalogging.StrictLogging
import org.{ json4s => jn }
import org.json4s.native.JsonMethods.{ compact, render, parse }

/**
 * Specification (fixed tests) for MetaInfo serialization
 */
class H5LibSpec extends Specification with StrictLogging {
  def writeAndReadBack(
    filePath: java.nio.file.Path,
    dtypeStr: String,
    value: NArray
  ): NArray = {
    {
      val h5File = H5Lib.fileCreate(filePath.toString())
      val group1 = H5Lib.groupCreate(h5File, "/group1")
      val group2 = H5Lib.groupCreate(group1, "group2")
      val dim1 = H5Lib.dimsCreate(value.getShape().map(_.toLong))
      val dataset1 = H5Lib.datasetCreate(
        groupId = group2,
        name = "d1",
        dtypeStr = dtypeStr,
        dimensionsId = dim1,
        dimensionNames = Seq()
      )
      H5Lib.datasetWrite(
        datasetId = dataset1,
        value = value,
        dtypeStr = dtypeStr
      )
      H5Lib.datasetClose(dataset1)
      H5Lib.dimsClose(dim1)
      H5Lib.groupClose(group1)
      H5Lib.groupClose(group2)
      H5Lib.fileClose(h5File)
    }

    val file2 = H5Lib.fileOpen(filePath.toString())
    val dataset2 = H5Lib.datasetOpen(file2, "/group1/group2/d1")
    H5Lib.datasetRead(
      datasetId = dataset2,
      dtypeStr = dtypeStr
    )
  }

  "h5 int" >> {
    val tmpFile2 = Files.createTempFile("int", ".h5")
    tmpFile2.toFile().deleteOnExit()
    val vi1 = new ma2.ArrayInt.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        vi1.set(i, j, (i + 2 * j))
    val vi2 = writeAndReadBack(tmpFile2, dtypeStr = "i", value = vi1)

    {
      val it1 = vi1.getIndexIterator()
      val it2 = vi2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getIntNext() must_== it2.getIntNext()
      }
      vi1.getShape must_== vi2.getShape()
    }
  }

  "h5 double" >> {
    val tmpFile1 = Files.createTempFile("double", ".h5")
    tmpFile1.toFile().deleteOnExit()
    val v1 = new ma2.ArrayDouble.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        v1.set(i, j, (i + j).toDouble / (j + 1).toDouble)
    val v2 = writeAndReadBack(tmpFile1, dtypeStr = "f", value = v1)

    {
      val it1 = v1.getIndexIterator()
      val it2 = v2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getDoubleNext() must_== it2.getDoubleNext()
      }
      v1.getShape must_== v2.getShape()
    }
  }

  "h5 bool" >> {
    val tmpFile1 = Files.createTempFile("bool", ".h5")
    tmpFile1.toFile().deleteOnExit()
    val v1 = new ma2.ArrayBoolean.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        v1.set(i, j, (i + j) % 3 < 2)
    val v2 = writeAndReadBack(tmpFile1, dtypeStr = "b", value = v1)

    {
      val it1 = v1.getIndexIterator()
      val it2 = v2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getBooleanNext() must_== it2.getBooleanNext()
      }
      v1.getShape must_== v2.getShape()
    }
  }

  "h5 string" >> {
    val tmpFile1 = Files.createTempFile("str", ".h5")
    tmpFile1.toFile().deleteOnExit()
    val v1 = new ma2.ArrayString.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        v1.set(i, j, i.toString() + "_" + j.toString())
    val v2 = writeAndReadBack(tmpFile1, dtypeStr = "C", value = v1)

    {
      val it1 = v1.getIndexIterator()
      val it2 = v2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getObjectNext() must_== it2.getObjectNext()
      }
      v1.getShape must_== v2.getShape()
    }
  }

  "h5 json" >> {
    val tmpFile1 = Files.createTempFile("json", ".h5")
    tmpFile1.toFile().deleteOnExit()
    val v1 = new ma2.ArrayObject.D2(classOf[org.json4s.JsonAST.JValue], 2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        v1.set(i, j, jn.JObject((i.toString() -> jn.JInt(j)) :: Nil))
    val v2 = writeAndReadBack(tmpFile1, dtypeStr = "D", value = v1)

    {
      val it1 = v1.getIndexIterator()
      val it2 = v2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        val json1 = it1.getObjectNext() match {
          case v: jn.JValue =>
            compact(render(v))
          case v =>
            throw new Exception(s"object is not json, but $v")
        }
        val json2 = it2.getObjectNext() match {
          case v: jn.JValue =>
            compact(render(v))
          case v =>
            throw new Exception(s"object2 is not json, but $v")
        }
        json1 must_== json2
      }
      v1.getShape must_== v2.getShape()
    }
  }

  "attrib" >> {
    val tmpFile = Files.createTempFile("attrib", ".h5")
    tmpFile.toFile().deleteOnExit()
    val h5File = H5Lib.fileCreate(tmpFile.toString())
    val group1 = H5Lib.groupCreate(h5File, "/group1")
    val dim1 = H5Lib.dimsCreate(Array(3: Long, 4))
    val dataset1 = H5Lib.datasetCreate(
      groupId = group1,
      name = "d1",
      dtypeStr = "i",
      dimensionsId = dim1,
      dimensionNames = Seq("pippo", "ll")
    )
    H5Lib.datasetClose(dataset1)
    H5Lib.dimsClose(dim1)
    H5Lib.groupClose(group1)
    H5Lib.fileClose(h5File)

    val file2 = H5Lib.fileOpen(tmpFile.toString())
    val dataset2 = H5Lib.datasetOpen(file2, "/group1/d1")
    val attribId = H5Lib.attributeOpen(dataset2, attributeName = "dimensionNames", attributeType = "C")
    val attribDims = H5Lib.attributeGetDimensionId(attribId)
    val attribV = H5Lib.dimsExtents(attribDims)
    val dimNames = H5Lib.attributeReadStr(attribId)
    dimNames must_== Array("pippo", "ll")
  }

  "resize" >> {
    val tmpFile2 = Files.createTempFile("resize", ".h5")
    tmpFile2.toFile().deleteOnExit()
    val vi0 = new ma2.ArrayInt.D2(1, 3)
    for (i <- 0 until 1)
      for (j <- 0 until 3)
        vi0.set(i, j, (i + 2 * j))
    val vi01 = new ma2.ArrayInt.D2(1, 3)
    for (i <- 0 until 1)
      for (j <- 0 until 3)
        vi01.set(i, j, (i + 1 + 2 * j))
    val vi1 = new ma2.ArrayInt.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        vi1.set(i, j, (i + 2 * j))

    val h5File = H5Lib.fileCreate(tmpFile2.toString())
    val group1 = H5Lib.groupCreate(h5File, "/group1")
    val dim1 = H5Lib.dimsCreate(vi0.getShape().map(_.toLong), Array(H5Lib.unlimitedDim, 3))
    val dataset1 = H5Lib.datasetCreate(
      groupId = group1,
      name = "d1",
      dtypeStr = "i",
      dimensionsId = dim1,
      dimensionNames = Seq()
    )
    H5Lib.datasetWrite(
      datasetId = dataset1,
      value = vi0,
      dtypeStr = "i"
    )
    H5Lib.dimsClose(dim1)
    H5Lib.datasetResize(dataset1, vi1.getShape.map(_.toLong).toSeq)
    H5Lib.datasetWrite(
      datasetId = dataset1,
      value = vi01,
      offset = Seq(1: Long, 0),
      dtypeStr = "i"
    )
    H5Lib.datasetClose(dataset1)
    H5Lib.groupClose(group1)
    H5Lib.fileClose(h5File)

    val file2 = H5Lib.fileOpen(tmpFile2.toString())
    val dataset2 = H5Lib.datasetOpen(file2, "/group1/d1")
    val vi2 = H5Lib.datasetRead(
      datasetId = dataset2,
      dtypeStr = "i"
    )

    {
      val it1 = vi1.getIndexIterator()
      val it2 = vi2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getIntNext() must_== it2.getIntNext()
      }
      vi1.getShape must_== vi2.getShape()
    }
  }

  "resize2" >> {
    val tmpFile2 = Files.createTempFile("resize", ".h5")
    tmpFile2.toFile().deleteOnExit()
    val vi0 = new ma2.ArrayInt.D2(1, 3)
    for (i <- 0 until 1)
      for (j <- 0 until 3)
        vi0.set(i, j, (i + 2 * j))
    val vi01 = new ma2.ArrayInt.D1(3)
    for (i <- 0 until 1)
      for (j <- 0 until 3)
        vi01.set(j, (i + 1 + 2 * j))
    val vi1 = new ma2.ArrayInt.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        vi1.set(i, j, (i + 2 * j))

    val h5File = H5Lib.fileCreate(tmpFile2.toString())
    val group1 = H5Lib.groupCreate(h5File, "/group1")
    val dim1 = H5Lib.dimsCreate(vi0.getShape().map(_.toLong), Array(H5Lib.unlimitedDim, 3))
    val dataset1 = H5Lib.datasetCreate(
      groupId = group1,
      name = "d1",
      dtypeStr = "i",
      dimensionsId = dim1,
      dimensionNames = Seq()
    )
    H5Lib.datasetWrite(
      datasetId = dataset1,
      value = vi0,
      dtypeStr = "i"
    )
    H5Lib.dimsClose(dim1)
    H5Lib.datasetResize(dataset1, vi1.getShape.map(_.toLong).toSeq)
    H5Lib.datasetWrite(
      datasetId = dataset1,
      value = vi01,
      offset = Seq(1: Long, 0),
      valueSizes = Seq(1: Long, 3),
      dtypeStr = "i"
    )
    H5Lib.datasetClose(dataset1)
    H5Lib.groupClose(group1)
    H5Lib.fileClose(h5File)

    val file2 = H5Lib.fileOpen(tmpFile2.toString())
    val dataset2 = H5Lib.datasetOpen(file2, "/group1/d1")
    val vi2 = H5Lib.datasetRead(
      datasetId = dataset2,
      dtypeStr = "i"
    )

    {
      val it1 = vi1.getIndexIterator()
      val it2 = vi2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getIntNext() must_== it2.getIntNext()
      }
      vi1.getShape must_== vi2.getShape()
    }
  }

  "partialRead" >> {
    val tmpFile2 = Files.createTempFile("partialR", ".h5")
    tmpFile2.toFile().deleteOnExit()
    val vi01 = new ma2.ArrayInt.D2(1, 2)
    for (i <- 0 until 1)
      for (j <- 0 until 2)
        vi01.set(i, j, (i + 1 + 2 * j))
    val vi1 = new ma2.ArrayInt.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        vi1.set(i, j, (i + 2 * j))

    val h5File = H5Lib.fileCreate(tmpFile2.toString())
    val group1 = H5Lib.groupCreate(h5File, "/group1")
    val dim1 = H5Lib.dimsCreate(vi1.getShape().map(_.toLong), Array(H5Lib.unlimitedDim, 3))
    val dataset1 = H5Lib.datasetCreate(
      groupId = group1,
      name = "d1",
      dtypeStr = "i",
      dimensionsId = dim1,
      dimensionNames = Seq()
    )
    H5Lib.datasetWrite(
      datasetId = dataset1,
      value = vi1,
      dtypeStr = "i"
    )
    H5Lib.dimsClose(dim1)
    H5Lib.datasetClose(dataset1)
    H5Lib.groupClose(group1)
    H5Lib.fileClose(h5File)

    val file2 = H5Lib.fileOpen(tmpFile2.toString())
    val dataset2 = H5Lib.datasetOpen(file2, "/group1/d1")
    val vi2 = H5Lib.datasetRead(
      datasetId = dataset2,
      dtypeStr = "i",
      offset = Seq(1: Long, 0),
      dimsToRead = Seq(1: Long, 2)
    )

    {
      val it1 = vi01.getIndexIterator()
      val it2 = vi2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getIntNext() must_== it2.getIntNext()
      }
      vi01.getShape must_== vi2.getShape()
    }

    val vi02 = new ma2.ArrayInt.D1(2)
    val vi3 = H5Lib.datasetRead(
      datasetId = dataset2,
      dtypeStr = "i",
      offset = Seq(1: Long, 0),
      dimsToRead = Seq(1: Long, 2),
      value = Some(vi02)
    )

    {
      val it1 = vi01.getIndexIterator()
      val it2 = vi3.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getIntNext() must_== it2.getIntNext()
      }
      vi3.getShape must_== vi02.getShape
    }
  }

  "copy" >> {
    val tmpFile1 = Files.createTempFile("copy1", ".h5")
    tmpFile1.toFile().deleteOnExit()
    val tmpFile2 = Files.createTempFile("copy2", ".h5")
    tmpFile2.toFile().deleteOnExit()
    val vi1 = new ma2.ArrayInt.D2(2, 3)
    for (i <- 0 until 2)
      for (j <- 0 until 3)
        vi1.set(i, j, (i + 2 * j))

    val h5File = H5Lib.fileCreate(tmpFile1.toString())
    val group1 = H5Lib.groupCreate(h5File, "/group1")
    val dim1 = H5Lib.dimsCreate(vi1.getShape().map(_.toLong), Array(H5Lib.unlimitedDim, 3))
    val dataset1 = H5Lib.datasetCreate(
      groupId = group1,
      name = "d1",
      dtypeStr = "i",
      dimensionsId = dim1,
      dimensionNames = Seq()
    )
    H5Lib.datasetWrite(
      datasetId = dataset1,
      value = vi1,
      dtypeStr = "i"
    )
    H5Lib.dimsClose(dim1)
    H5Lib.datasetClose(dataset1)
    H5Lib.groupClose(group1)
    val h5FileCopy = H5Lib.fileCreate(tmpFile2.toString())
    H5Lib.objectCopy(h5File, "/group1", h5FileCopy, "/group2")
    H5Lib.fileClose(h5File)
    H5Lib.fileClose(h5FileCopy)

    val file2 = H5Lib.fileOpen(tmpFile2.toString())
    val dataset2 = H5Lib.datasetOpen(file2, "/group2/d1")
    val vi2 = H5Lib.datasetRead(
      datasetId = dataset2,
      dtypeStr = "i"
    )

    {
      val it1 = vi1.getIndexIterator()
      val it2 = vi2.getIndexIterator()
      while (it1.hasNext() && it2.hasNext()) {
        it1.getIntNext() must_== it2.getIntNext()
      }
      vi1.getShape must_== vi2.getShape()
    }
  }

}
