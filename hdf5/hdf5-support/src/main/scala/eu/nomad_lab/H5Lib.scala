/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import hdf.hdf5lib.H5
import hdf.hdf5lib.HDF5Constants
import hdf.hdf5lib.structs.H5O_info_t
import com.typesafe.scalalogging.StrictLogging
import scala.util.control.NonFatal
import scala.collection
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import org.json4s.native.JsonMethods.{ compact, render, parse }

object H5Lib extends StrictLogging {
  val readSupport = (H5.H5open() >= 0)
  val writeSupport = readSupport

  val unlimitedDim: Long = HDF5Constants.H5S_UNLIMITED.toLong

  class HdfException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  protected val typeMap: Map[String, Long] = Map(
    ("C" -> HDF5Constants.H5T_C_S1),
    ("D" -> HDF5Constants.H5T_C_S1),
    ("i" -> HDF5Constants.H5T_NATIVE_INT32),
    ("i32" -> HDF5Constants.H5T_NATIVE_INT32),
    ("i64" -> HDF5Constants.H5T_NATIVE_INT64),
    ("r" -> HDF5Constants.H5T_NATIVE_INT64),
    ("f" -> HDF5Constants.H5T_NATIVE_DOUBLE),
    ("f32" -> HDF5Constants.H5T_NATIVE_FLOAT),
    ("f64" -> HDF5Constants.H5T_NATIVE_DOUBLE),
    ("b" -> HDF5Constants.H5T_NATIVE_INT8),
    ("B" -> HDF5Constants.H5T_NATIVE_INT8)
  )

  def typeCreate(typeStr: String): Long = {
    val typeId = H5.H5Tcopy(typeMap(typeStr))
    if (typeStr == "C" || typeStr == "D" || typeStr == "B")
      H5.H5Tset_size(typeId, HDF5Constants.H5T_VARIABLE)
    typeId
  }

  def typeClose(typeId: Long): Unit = {
    val err = H5.H5Tclose(typeId)
    if (err < 0)
      logger.warn(s"Error $err while closing type with id $typeId")
  }

  def fileCreate(filePath: String): Long = {
    H5.H5Fcreate(filePath, HDF5Constants.H5F_ACC_TRUNC, HDF5Constants.H5P_DEFAULT,
      HDF5Constants.H5P_DEFAULT)
  }

  def fileOpen(filePath: String, write: Boolean = false): Long = {
    val flags = if (write)
      HDF5Constants.H5F_ACC_RDWR
    else
      HDF5Constants.H5F_ACC_RDONLY
    H5.H5Fopen(filePath, flags, HDF5Constants.H5P_DEFAULT)
  }

  def fileFlush(fileId: Long): Unit = {
    val err = H5.H5Fflush(fileId, HDF5Constants.H5F_SCOPE_GLOBAL)
    if (err < 0)
      logger.warn(s"Error $err while closing file with id $fileId")
  }

  def fileClose(fileId: Long): Unit = {
    val err = H5.H5Fclose(fileId)
    if (err < 0)
      logger.warn(s"Error $err while closing file with id $fileId")
  }

  def groupCreate(locId: Long, groupPath: String): Long = {
    H5.H5Gcreate(locId, groupPath, HDF5Constants.H5P_DEFAULT,
      HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT)
  }

  def groupOpen(locId: Long, name: String): Long = {
    H5.H5Gopen(locId, name, HDF5Constants.H5P_DEFAULT)
  }

  def groupGet(locId: Long, groupPath: String, create: Boolean = true): Long = {
    try {
      groupOpen(locId, groupPath)
    } catch {
      case NonFatal(e) =>
        if (create)
          groupCreate(locId, groupPath)
        else
          -1
    }
  }

  def groupClose(group: Long): Unit = {
    val err = H5.H5Gclose(group)
    if (err < 0)
      logger.warn(s"Error $err closing type with id $group")
  }

  def groupMemberCount(group: Long): Int = {
    H5.H5Gn_members(group, ".").toInt
  }

  def groupMembers(group: Long, childNames: Array[String], childTypes: Array[Int], lTypes: Array[Int], childRefs: Array[Long]): Unit = {
    H5.H5Gget_obj_info_all(group, ".", childNames, childTypes, lTypes, childRefs, HDF5Constants.H5_INDEX_NAME)
  }
  def groupObjectType: Int = HDF5Constants.H5O_TYPE_GROUP
  //def groupInfo(group: Long, name: String, accessProprtyList: Long): H5O_info_t = {
  //  H5.H5Oget_info_by_name(group, name, HDF5Constants.H5P_DEFAULT)
  //}

  def attributeCreate(objId: Long, attributeName: String, attributeType: String, dimensionsId: Long = HDF5Constants.H5S_SCALAR.toLong): Long = {
    val typeId = typeCreate(attributeType)
    val attributeId = H5.H5Acreate(objId, attributeName, typeId, dimensionsId, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT)
    typeClose(typeId)
    attributeId
  }

  def attributeOpen(objId: Long, attributeName: String, attributeType: String): Long = {
    H5.H5Aopen(objId, attributeName, HDF5Constants.H5P_DEFAULT)
  }

  def attributeClose(attrib: Long): Unit = {
    val err = H5.H5Aclose(attrib)
    if (err < 0)
      logger.warn(s"Error $err closing attribute with id $attrib")
  }

  def attributeWriteStr(attributeId: Long, values: Array[String]): Unit = {
    val memType = typeCreate("C")
    H5.H5AwriteVL(attributeId, memType, values)
    typeClose(memType)
  }

  def attributeGetDimensionId(attributeId: Long): Long = {
    H5.H5Aget_space(attributeId)
  }

  def attributeReadStr(attributeId: Long, dimsToRead: Seq[Long] = Seq(), offset: Seq[Long] = Seq()): Array[String] = {
    val memType = typeCreate("C")
    val dims = if (dimsToRead.isEmpty) {
      val dimsId = attributeGetDimensionId(attributeId)
      val res = if (offset.isEmpty)
        dimsExtents(dimsId)
      else
        Array.fill[Long](dimsRank(dimsId))(1)
      dimsClose(dimsId)
      res
    } else {
      dimsToRead.toArray
    }
    val nToRead = dims.foldLeft(1: Long)(_ * _)
    val strBuf = Array.fill[String](nToRead.toInt)("")
    H5.H5AreadVL(attributeId, memType, strBuf)
    typeClose(memType)
    strBuf
  }

  def dimsCreate(dim: Array[Long], maxDims: Array[Long] = null): Long = {
    H5.H5Screate_simple(dim.size, dim, maxDims)
  }

  def dimsClose(dims: Long): Unit = {
    H5.H5Sclose(dims)
  }

  def dimsRank(dimsId: Long): Int = {
    H5.H5Sget_simple_extent_ndims(dimsId)
  }

  def dimsExtents(dimsId: Long): Array[Long] = {
    val rank = H5.H5Sget_simple_extent_ndims(dimsId)
    val dims = Array.fill[Long](rank)(0: Long)
    val maxDims = Array.fill[Long](rank)(0: Long)
    H5.H5Sget_simple_extent_dims(dimsId, dims, maxDims)
    dims
  }

  def dimsMaxExtents(dimsId: Long): Array[Long] = {
    val rank = H5.H5Sget_simple_extent_ndims(dimsId)
    val dims = Array.fill[Long](rank)(0: Long)
    val maxDims = Array.fill[Long](rank)(0: Long)
    H5.H5Sget_simple_extent_dims(dimsId, dims, maxDims)
    maxDims
  }

  def setDimNames(datset: Long, dimensionNames: Array[String]): Unit = {
    val dimensions = dimsCreate(Array(dimensionNames.length.toLong))
    val attrib = attributeCreate(datset, "dimensionNames", "C", dimensions)
    attributeWriteStr(attrib, dimensionNames)
    attributeClose(attrib)
    dimsClose(dimensions)
  }

  def datasetCreate(groupId: Long, name: String, dtypeStr: String, dimensionsId: Long, dimensionNames: Seq[String] = Seq(), chunkSize: Seq[Long] = Seq()): Long = {
    val typeId = typeCreate(dtypeStr)
    val maxDims = dimsMaxExtents(dimensionsId)
    val chunkingPropId = if (!chunkSize.isEmpty || maxDims.contains(unlimitedDim)) {
      val chunkS = if (!chunkSize.isEmpty) {
        chunkSize.toArray
      } else {
        val dimNow = dimsExtents(dimensionsId)
        val chunkDims = Array.fill[Long](dimNow.length)(1)
        for (idim <- 0 until chunkDims.length) {
          if (maxDims(idim) != unlimitedDim)
            chunkDims(idim) = dimNow(idim).max(1: Long)
          else if (dimNow(idim) == 0)
            dimNow(idim) = 1
        }
        while (chunkDims.foldLeft(1: Long)(_ * _) > 128 * 1024) {
          val (maxV, maxI) = chunkDims.zipWithIndex.foldLeft((-1: Long, -1: Int)) {
            case ((vMax, iMax), (v, i)) =>
              if (v > vMax)
                (v, i)
              else
                (vMax, iMax)
          }
          chunkDims(maxI) = (maxV + 1) / 2
        }
        var chunkTotSize = chunkDims.foldLeft(1: Long)(_ * _)
        val minSize = (dimNow.foldLeft(1: Long)(_ * _) / 4).min(4 * 1024)
        var idim: Int = maxDims.length
        while (chunkTotSize < minSize) {
          if (maxDims(idim) == unlimitedDim && dimNow(idim) > chunkDims(idim)) {
            val nSplits = (dimNow(idim) + chunkDims(idim) * 2 - 1) / (chunkSize(idim) * 2)
            chunkDims(idim) = (dimNow(idim) + nSplits - 1) / nSplits
          }
        }
        chunkDims
      }
      val chunkPropId = H5.H5Pcreate(HDF5Constants.H5P_DATASET_CREATE);
      if (chunkPropId >= 0)
        H5.H5Pset_chunk(chunkPropId, chunkS.length, chunkS)
      chunkPropId
    } else {
      HDF5Constants.H5P_DEFAULT
    }
    val dataset = H5.H5Dcreate(groupId, name, typeId, dimensionsId, HDF5Constants.H5P_DEFAULT, chunkingPropId, HDF5Constants.H5P_DEFAULT)
    if (chunkingPropId != HDF5Constants.H5P_DEFAULT) {
      val pErr = H5.H5Pclose(chunkingPropId)
      if (pErr < 0)
        logger.error(s"H5Lib.dataseCreate: error $pErr while closing property list")
    }
    typeClose(typeId)
    if (!dimensionNames.isEmpty) {
      try {
        setDimNames(dataset, dimensionNames.toArray)
      } catch {
        case NonFatal(e) =>
          logger.warn(s"could not store dimension names $dimensionNames", e)
      }
    }
    val sDim = dimsCreate(Array(1: Long))
    val dtypeAttrId = attributeCreate(dataset, attributeName = "dtypeStr", attributeType = "C", dimensionsId = sDim)
    attributeWriteStr(dtypeAttrId, values = Array(dtypeStr))
    dimsClose(sDim)
    attributeClose(dtypeAttrId)
    dataset
  }

  def datasetOpen(locId: Long, name: String): Long = {
    H5.H5Dopen(locId, name, HDF5Constants.H5P_DEFAULT)
  }

  def datasetGet(locId: Long, name: String, dtypeStr: String, dimensions: Seq[Long], maxDimensions: Seq[Long] = Seq(), dimensionNames: Seq[String] = Seq(), create: Boolean = true): Long = {
    try {
      datasetOpen(locId, name)
    } catch {
      case NonFatal(e) =>
        if (create) {
          val maxDims: Array[Long] = if (maxDimensions.isEmpty)
            null
          else
            maxDimensions.toArray
          val dims = dimsCreate(dimensions.toArray, maxDims)
          val datasetId = datasetCreate(locId, name, dtypeStr, dims, dimensionNames)
          dimsClose(dims)
          datasetId
        } else {
          -1
        }
    }
  }

  def datasetClose(datasetId: Long): Unit = {
    val err = H5.H5Dclose(datasetId)
    if (err < 0)
      logger.warn(s"Error $err while closing dataset with id $datasetId")
  }

  def datasetResize(datasetId: Long, dimensions: Seq[Long]): Unit = {
    H5.H5Dset_extent(datasetId, dimensions.toArray)
    // is
    //   datasetGetDimensionsId(datasetId)
    // required to refresh the internal cache?
    // It does not seem to be the case, but see the remarks in
    // https://www.hdfgroup.org/HDF5/Tutor/extend.html
  }

  def datasetObjectType: Int = HDF5Constants.H5O_TYPE_DATASET

  protected val simpleTypeForClass: Map[Class[_], Long] = Map(
    (classOf[Float] -> HDF5Constants.H5T_NATIVE_FLOAT),
    (classOf[Double] -> HDF5Constants.H5T_NATIVE_DOUBLE),
    (classOf[Short] -> HDF5Constants.H5T_NATIVE_INT16),
    (classOf[Int] -> HDF5Constants.H5T_NATIVE_INT32),
    (classOf[Long] -> HDF5Constants.H5T_NATIVE_INT64)
  )

  def datasetWrite(datasetId: Long, value: NArray, dtypeStr: String, offset: Seq[Long] = Seq(), valueSizes: Seq[Long] = Seq()): Unit = {
    //logger.info(s"value:${value.getDataType.getPrimitiveClassType} ${value.getShape.mkString("[", ",", "]")} offset: ${offset.mkString("[", ",", "]")} valueSizes: ${valueSizes}")
    val dataType = value.getDataType()
    val c = classOf[java.lang.Byte]
    val dClass = dataType.getPrimitiveClassType()
    val valueS = if (valueSizes.isEmpty)
      value.getShape.map(_.toLong)
    else
      valueSizes.toArray
    val memSpaceId = dimsCreate(valueS, null)
    val fileSpaceId = if (offset.isEmpty) {
      HDF5Constants.H5S_ALL.toLong
    } else {
      val fSpaceId = datasetGetDimensionsId(datasetId)
      val err = H5.H5Sselect_hyperslab(fSpaceId, HDF5Constants.H5S_SELECT_SET, offset.toArray, null,
        valueS, null)
      if (err < 0)
        logger.error(s"Failed to set selection for dataset $datasetId: error $err")
      fSpaceId
    }
    simpleTypeForClass.get(dClass) match {
      case Some(memTypeId) =>
        val validTypeCombination = dtypeStr match {
          case "f" | "f64" =>
            memTypeId == HDF5Constants.H5T_NATIVE_DOUBLE || memTypeId == HDF5Constants.H5T_NATIVE_FLOAT
          case "f32" =>
            memTypeId == HDF5Constants.H5T_NATIVE_FLOAT
          case "i" | "i32" =>
            memTypeId == HDF5Constants.H5T_NATIVE_INT32 || memTypeId == HDF5Constants.H5T_NATIVE_INT16
          case "i64" | "r" =>
            memTypeId == HDF5Constants.H5T_NATIVE_INT64 || memTypeId == HDF5Constants.H5T_NATIVE_INT32 || memTypeId == HDF5Constants.H5T_NATIVE_INT16
        }
        if (!validTypeCombination)
          throw new Exception(s"invalid type combination detected: $dtypeStr => $dataType ($dClass) vs $memTypeId")
        H5.H5Dwrite(
          datasetId,
          memTypeId, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, value.getStorage()
        )
      case None =>
        if (dtypeStr == "C") {
          if (dClass != classOf[String] && dClass != classOf[Object])
            throw new HdfException(s"unexpected array type for string storage, expected strings or objects")
          val memDataId = typeCreate("C")
          value.getStorage() match {
            case storage: Array[Object] =>
              H5.H5Dwrite_VLStrings(datasetId, memDataId, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, storage)
            case storage: Array[String] =>
              H5.H5Dwrite_VLStrings(datasetId, memDataId, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, storage.asInstanceOf[Array[Object]])
            case _ =>
              logger.error(s"storage of string NArray has unexpected type for array $value")
          }
          typeClose(memDataId)
        } else if (dtypeStr == "b") {
          if (dClass != classOf[Boolean])
            throw new HdfException(s"unexpected array type for boolean, expected booleans")
          val byteBuf = new Array[Byte](value.getSize.toInt)
          val it = value.getIndexIterator
          var ii: Int = 0
          while (it.hasNext()) {
            byteBuf(ii) = if (it.getBooleanNext()) {
              1: Byte
            } else {
              0: Byte
            }
            ii += 1
          }
          H5.H5Dwrite(datasetId, HDF5Constants.H5T_NATIVE_INT8, memSpaceId,
            fileSpaceId, HDF5Constants.H5P_DEFAULT, byteBuf)
        } else if (dtypeStr == "D") {
          if (!(classOf[org.json4s.JsonAST.JValue].isAssignableFrom(dataType.getClassType()) ||
            dClass == classOf[java.lang.Object]))
            throw new HdfException(s"unexpected array type for json storage, expected JValues")
          val memDataId = typeCreate("D")
          val strBuf: Array[Object] = Array.fill[Object](value.getSize().toInt)("")
          val it = value.getIndexIterator()
          var ii: Int = 0
          while (it.hasNext()) {
            it.getObjectNext() match {
              case v: org.json4s.JsonAST.JValue =>
                strBuf(ii) = compact(render(v))
                ii += 1
              case v =>
                throw new HdfException(s"uexpected value $v in JValue array")
            }
          }
          H5.H5Dwrite_VLStrings(datasetId, memDataId, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, strBuf)
          typeClose(memDataId)

        } else if (dtypeStr == "B") {
          if (dClass != classOf[ucar.ma2.StructureData] && dClass != classOf[ucar.ma2.StructureDataIterator] && dClass != classOf[java.nio.ByteBuffer])
            throw new HdfException(s"unexpected array type for binary storage")
          logger.error(s"Hdf5Support.writeDataset unimplemented for type $dClass")
        } else {
          logger.error(s"Hdf5Support.writeDataset unimplemented for type $dClass, ${dataType.getClassType}")
        }
    }
    dimsClose(memSpaceId)
    if (!offset.isEmpty)
      dimsClose(fileSpaceId)
  }

  def datasetGetDimensionsId(datasetId: Long): Long = {
    H5.H5Dget_space(datasetId)
  }

  def datasetGetTypeId(datasetId: Long): Long = {
    H5.H5Dget_type(datasetId)
  }

  def createNArray(dtypeStr: String, intShape: Array[Int]): NArray = {
    dtypeStr match {
      case "f" | "f64" => new ma2.ArrayDouble(intShape)
      case "f32" => new ma2.ArrayFloat(intShape)
      case "i" | "i32" => new ma2.ArrayInt(intShape)
      case "i64" | "r" => new ma2.ArrayLong(intShape)
      case "b" => new ma2.ArrayBoolean(intShape)
      case "B" => new ma2.ArrayString(intShape)
      case "C" => new ma2.ArrayString(intShape)
      case "D" => new ma2.ArrayObject(classOf[org.json4s.JsonAST.JValue], intShape)
      case _ =>
        throw new HdfException(s"Unknown dtypeStr $dtypeStr, known types: f,f32,f64,i,i32,i64,r,b,B,C,D")
    }
  }

  def datasetRead(datasetId: Long, dtypeStr: String, dimsToRead: Seq[Long] = Seq(), offset: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    val dims = if (dimsToRead.isEmpty) {
      val dimsId = datasetGetDimensionsId(datasetId)
      val res = if (offset.isEmpty)
        dimsExtents(dimsId)
      else
        Array.fill[Long](dimsRank(dimsId))(1)
      dimsClose(dimsId)
      res
    } else {
      dimsToRead.toArray
    }
    val intDims = dims.map(_.toInt)
    val memSpaceId = dimsCreate(dims, null)
    val fileSpaceId = if (offset.isEmpty) {
      HDF5Constants.H5S_ALL.toLong
    } else {
      val fSpaceId = datasetGetDimensionsId(datasetId)
      val err = H5.H5Sselect_hyperslab(fSpaceId, HDF5Constants.H5S_SELECT_SET, offset.toArray, null,
        dims, null)
      if (err < 0)
        logger.error(s"Failed to set selection for dataset $datasetId: error $err")
      fSpaceId
    }
    try {
      val array: NArray = value match {
        case Some(a) =>
          a
        case None =>
          createNArray(dtypeStr, intDims)
      }
      dtypeStr match {
        case "f" | "f64" | "f32" | "i" | "i32" | "i64" | "r" =>
          H5.H5Dread(datasetId, typeMap(dtypeStr), memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, array.getStorage())
        case "b" =>
          val totDim = intDims.foldLeft(1)(_ * _)
          val byteBuf = new Array[Byte](totDim)
          H5.H5Dread(datasetId, HDF5Constants.H5T_NATIVE_INT8, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, byteBuf)
          val it = array.getIndexIterator
          var ii: Int = 0
          while (it.hasNext()) {
            it.setBooleanNext(byteBuf(ii) != 0)
            ii += 1
          }
        case "C" | "D" =>
          val memTypeId = typeCreate("C")
          val arr = if (dtypeStr == "C")
            array
          else
            createNArray("C", intDims)
          arr.getStorage() match {
            case a: Array[Object] =>
              H5.H5Dread_VLStrings(datasetId, memTypeId, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, a)
            case a: Array[String] =>
              H5.H5Dread_VLStrings(datasetId, memTypeId, memSpaceId, fileSpaceId, HDF5Constants.H5P_DEFAULT, a.asInstanceOf[Array[Object]])
            case _ =>
              throw new HdfException(s"Hdf.datasetRead: found unexpected value for array storage")
          }
          if (dtypeStr == "D") {
            val i1 = arr.getIndexIterator()
            val i2 = array.getIndexIterator()
            while (i1.hasNext() && i2.hasNext()) {
              val strVal = i1.getObjectNext()
              val jVal = if (strVal == null)
                org.json4s.JNull
              else
                parse(strVal.toString())
              i2.setObjectNext(jVal)
            }
          }
        case "B" =>
          throw new HdfException(s"Byte array unimplemented")
        case _ =>
          throw new HdfException(s"Unexpected dtypeStr '$dtypeStr'")
      }
      array
    } finally {
      if (!offset.isEmpty)
        dimsClose(fileSpaceId)
      dimsClose(memSpaceId)
    }
  }

  def objectCopy(sourceId: Long, sourceName: String, targetLocId: Long, targetName: String): Unit = {
    H5.H5Ocopy(sourceId, sourceName, targetLocId, targetName, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT)
  }

  object GroupContent {
    /**
     * Represents the type of entry
     */
    object ObjectType extends Enumeration {
      val Unknown = Value(-1) // Unknown object type
      val Group = Value(0) // Object is a group
      val Dataset = Value(1) // Object is a dataset
      val NamedDatatype = Value(2) // Object is a named data type
      val Ntypes = Value(3) // Number of different object types
      type ObjectType = Value
    }

    /**
     * class representing an entry in a group
     */
    case class GroupInfo(
      oname: String, // name
      otype: ObjectType.Value,
      ltype: Int, // do not expose?
      orefs: Long // do not expose?
    )

    def apply(
      baseGroup: Long,
      groupPath: String = "."
    ): GroupContent = {
      new GroupContent(baseGroup, groupPath)
    }

  }

  /**
   * lists the contents of a group
   *
   * should switch to iterate for *huge* listings?
   */
  class GroupContent(
      baseGroup: Long,
      groupPath: String = "."
  ) extends collection.IndexedSeq[GroupContent.GroupInfo] {
    val lengthL: Long = if (baseGroup < 0)
      0
    else
      H5.H5Gn_members(baseGroup, groupPath)
    def length: Int = 0.max(lengthL.toInt)
    val aLen = 1.max(length)
    protected val oname = new Array[String](aLen)
    protected val otype = new Array[Int](aLen)
    protected val ltype = new Array[Int](aLen) // skip?
    protected val orefs = new Array[Long](aLen) // skip ?
    if (baseGroup >= 0)
      H5.H5Gget_obj_info_all(baseGroup, groupPath, oname, otype, ltype, orefs, HDF5Constants.H5_INDEX_NAME);

    override def apply(i: Int): GroupContent.GroupInfo = {
      GroupContent.GroupInfo(oname(i), GroupContent.ObjectType(otype(i)), ltype(i), orefs(i))
    }
  }
}
