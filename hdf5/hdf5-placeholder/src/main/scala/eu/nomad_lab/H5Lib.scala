/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab;

import com.typesafe.scalalogging.StrictLogging
import scala.util.control.NonFatal
import scala.collection
import ucar.ma2.{ Array => NArray }

/**
 * Library wrapping hdf5
 *
 * Placeholder if the native hdf library is not available
 */
object H5Lib extends StrictLogging {
  val readSupport = false
  val writeSupport = false
  val unlimitedDim: Long = -1

  class HdfException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  def errorMsg = "placeholder, you need to compile the binary HDF5 to have support"

  def createNArray(dtypeStr: String, dims: Array[Int]): NArray = {
    throw new HdfException(errorMsg)
  }

  def typeCreate(typeStr: String): Long = {
    throw new HdfException(errorMsg)
  }

  def typeClose(typeId: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def fileCreate(filePath: String): Long = {
    throw new HdfException(errorMsg)
  }

  def fileOpen(filePath: String, write: Boolean = false): Long = {
    throw new HdfException(errorMsg)
  }

  def fileFlush(fileId: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def fileClose(fileId: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def groupCreate(locId: Long, groupPath: String): Long = {
    throw new HdfException(errorMsg)
  }

  def groupOpen(locId: Long, name: String): Long = {
    throw new HdfException(errorMsg)
  }

  def groupGet(locId: Long, groupPath: String, create: Boolean = true): Long = {
    throw new HdfException(errorMsg)
  }

  def groupClose(group: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def groupMemberCount(group: Long): Int = {
    throw new HdfException(errorMsg)
  }

  def groupMembers(group: Long, childNames: Array[String], childTypes: Array[Int], lTypes: Array[Int], childRefs: Array[Long]): Unit = {
    throw new HdfException(errorMsg)
  }

  def groupObjectType: Int = {
    throw new HdfException(errorMsg)
  }

  def attributeCreate(objId: Long, attributeName: String, attributeType: String, dimensionsId: Long = 0): Long = {
    throw new HdfException(errorMsg)
  }

  def attributeOpen(objId: Long, attributeName: String, attributeType: String): Long = {
    throw new HdfException(errorMsg)
  }

  def attributeClose(attrib: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def attributeWriteStr(attributeId: Long, values: Array[String]): Unit = {
    throw new HdfException(errorMsg)
  }

  def attributeGetDimensionId(attributeId: Long): Long = {
    throw new HdfException(errorMsg)
  }

  def attributeReadStr(attributeId: Long, dimsToRead: Seq[Long] = Seq(), offset: Seq[Long] = Seq()): Array[String] = {
    throw new HdfException(errorMsg)
  }

  def dimsCreate(dim: Array[Long], maxDims: Array[Long] = null): Long = {
    throw new HdfException(errorMsg)
  }

  def dimsClose(dims: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def dimsRank(dimsId: Long): Int = {
    throw new HdfException(errorMsg)
  }

  def dimsExtents(dimsId: Long): Array[Long] = {
    throw new HdfException(errorMsg)
  }

  def dimsMaxExtents(dimsId: Long): Array[Long] = {
    throw new HdfException(errorMsg)
  }

  def setDimNames(datset: Long, dimensionNames: Array[String]): Unit = {
    throw new HdfException(errorMsg)
  }

  def datasetCreate(groupId: Long, name: String, dtypeStr: String, dimensionsId: Long, dimensionNames: Seq[String] = Seq(), chunkSize: Seq[Long] = Seq()): Long = {
    throw new HdfException(errorMsg)
  }

  def datasetOpen(locId: Long, name: String): Long = {
    throw new HdfException(errorMsg)
  }

  def datasetGet(locId: Long, name: String, dtypeStr: String, dimensions: Seq[Long], maxDimensions: Seq[Long] = Seq(), dimensionNames: Seq[String] = Seq(), create: Boolean = true): Long = {
    throw new HdfException(errorMsg)
  }

  def datasetClose(datasetId: Long): Unit = {
    throw new HdfException(errorMsg)
  }

  def datasetResize(datasetId: Long, dimensions: Seq[Long]): Unit = {
    throw new HdfException(errorMsg)
  }

  def datasetObjectType: Int = {
    throw new HdfException(errorMsg)
  }

  def datasetWrite(datasetId: Long, value: NArray, dtypeStr: String, offset: Seq[Long] = Seq(), valueSizes: Seq[Long] = Seq()): Unit = {
    throw new HdfException(errorMsg)
  }

  def datasetGetDimensionsId(datasetId: Long): Long = {
    throw new HdfException(errorMsg)
  }

  def datasetGetTypeId(datasetId: Long): Long = {
    throw new HdfException(errorMsg)
  }

  def datasetRead(datasetId: Long, dtypeStr: String, dimsToRead: Seq[Long] = Seq(), offset: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    throw new HdfException(errorMsg)
  }

  def objectCopy(sourceLocId: Long, sourceName: String, targetLocId: Long, targetName: String): Unit = {
    throw new HdfException(errorMsg)
  }

  object GroupContent {
    /**
     * Represents the type of entry
     */
    object ObjectType extends Enumeration {
      val Unknown = Value(-1) // Unknown object type
      val Group = Value(0) // Object is a group
      val Dataset = Value(1) // Object is a dataset
      val NamedDatatype = Value(2) // Object is a named data type
      val Ntypes = Value(3) // Number of different object types
      type ObjectType = Value
    }

    /**
     * class representing an entry in a group
     */
    case class GroupInfo(
      oname: String, // name
      otype: ObjectType.Value,
      ltype: Int, // do not expose?
      orefs: Long // do not expose?
    )

    def apply(
      baseGroup: Long,
      groupPath: String = "."
    ): GroupContent = {
      new GroupContent(baseGroup, groupPath)
    }
  }

  /**
   * lists the contents of a group
   *
   * shoulds twitch to iterate for *huge* listings?
   */
  class GroupContent(
      baseGroup: Long,
      groupPath: String = "."
  ) extends collection.IndexedSeq[GroupContent.GroupInfo] {
    def length: Int = 0

    override def apply(i: Int): GroupContent.GroupInfo = {
      throw new HdfException(errorMsg)
    }
  }

}
