/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import java.nio.file.Paths

import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, TreeParserRequest }
import eu.nomad_lab.parsing_queue.TreeParser
import org.specs2.mutable.Specification

class TreeParserWorkerSpec extends Specification {
  val parserCollection = parsers.AllParsers.defaultParserCollection
  val treeParser = new TreeParser(
    parserCollection = parserCollection
  )
  val currPath = Paths.get("").toAbsolutePath
  "TreeParserRequest" >> {
    "for zip type" >> {
      val zipMessage = {
        val filePath = "test/examples.zip"
        val path = Paths.get(filePath)
        val filename = path.getFileName.toString
        val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
        val (uri, pathInArchive) = filename match {
          case archiveRe(gid) =>
            (s"nmd://$gid/data", Some(s"$gid/data"))
          case _ =>
            ("file://" + path.toAbsolutePath().toString(), None)
        }
        TreeParserRequest(
          treeUri = uri,
          treeFilePath = path.toAbsolutePath.toString,
          treeType = if (filePath.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
          relativeTreeFilePath = pathInArchive
        )
      }
      val expected = CalculationParserRequest(
        parserName = "FhiAimsParser", //Name of the parser to use for the file; CastepParser
        mainFileUri = currPath.toUri.toString + "test/examples.zip/examples/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
        relativeFilePath = "examples/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", // file path, from the tree root. Example data/examples/foo/Si2.castep
        treeUri = "file://" + currPath.toString + "/test/examples.zip",
        treeFilePath = currPath.toString + "/test/examples.zip", // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
        treeType = TreeType.Zip, // type of root tree we are dealing with
        overwrite = false // Overwrite an existing file; eg. In case of failure of previous run
      )
      var message: CalculationParserRequest = null
      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        message = TreeParser.createCalculationParserRequest(parserAssociation)
        println(message)
      }
      treeParser.findParserAndPublish(publish, zipMessage)
      message must_== (expected)
    }
    "for tar type" >> {
      val tarMessage = {
        val filePath = "test/examples.tar"
        val path = Paths.get(filePath)
        val filename = path.getFileName.toString
        val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
        val (uri, pathInArchive) = filename match {
          case archiveRe(gid) =>
            (s"nmd://$gid/data", Some(s"$gid/data"))
          case _ =>
            ("file://" + path.toAbsolutePath().toString(), None)
        }
        TreeParserRequest(
          treeUri = uri,
          treeFilePath = path.toAbsolutePath.toString,
          treeType = if (filePath.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
          relativeTreeFilePath = pathInArchive
        )
      }

      val expected = CalculationParserRequest(
        parserName = "FhiAimsParser", //Name of the parser to use for the file; CastepParser
        mainFileUri = currPath.toUri.toString + "test/examples.tar/examples/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
        relativeFilePath = "examples/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", // file path, from the tree root. Example data/examples/foo/Si2.castep
        treeUri = "file://" + currPath.toString + "/test/examples.tar",
        treeFilePath = currPath.toString + "/test/examples.tar", // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
        treeType = TreeType.Tar, // type of root tree we are dealing with
        overwrite = false // Overwrite an existing file; eg. In case of failure of previous run
      )
      var message: CalculationParserRequest = null
      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        message = TreeParser.createCalculationParserRequest(parserAssociation)
        println(message)
      }
      treeParser.findParserAndPublish(publish, tarMessage)
      message must_== (expected)
    }
    "for directory type" >> {
      val dirMessage = {
        val filePath = "test/examples"
        val path = Paths.get(filePath)
        val filename = path.getFileName.toString
        val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
        val (uri, pathInArchive) = filename match {
          case archiveRe(gid) =>
            (s"nmd://$gid/data", Some(s"$gid/data"))
          case _ =>
            ("file://" + path.toAbsolutePath().toString(), None)
        }
        TreeParserRequest(
          treeUri = uri,
          treeFilePath = path.toAbsolutePath.toString,
          treeType = if (filePath.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
          relativeTreeFilePath = pathInArchive
        )
      }
      val expected = CalculationParserRequest(
        parserName = "FhiAimsParser", //Name of the parser to use for the file; CastepParser
        mainFileUri = currPath.toUri.toString + "test/examples/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
        relativeFilePath = "Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", // file path, from the tree root. Example data/examples/foo/Si2.castep
        treeUri = "file://" + currPath.toString + "/test/examples",
        treeFilePath = currPath.toString + "/test/examples", // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
        treeType = TreeType.Directory, // type of root tree we are dealing with
        overwrite = false // Overwrite an existing file; eg. In case of failure of previous run
      )
      var message: CalculationParserRequest = null
      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        message = TreeParser.createCalculationParserRequest(parserAssociation)
        println(message)
      }
      treeParser.findParserAndPublish(publish, dirMessage)
      message must_== (expected)
    }
    "for zip type with pathInArchive" >> {
      val zipMessage = {
        val filePath = "test/examples_multi.zip"
        val path = Paths.get(filePath)
        val filename = path.getFileName.toString
        val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
        val (uri, pathInArchive) = ("file://" + path.toAbsolutePath().toString() + "/examples_multi/Fe_band_structure_dos_spin", Some("examples_multi/Fe_band_structure_dos_spin"))
        TreeParserRequest(
          treeUri = uri,
          treeFilePath = path.toAbsolutePath.toString,
          treeType = if (filePath.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
          relativeTreeFilePath = pathInArchive
        )
      }
      val expected = CalculationParserRequest(
        parserName = "FhiAimsParser", //Name of the parser to use for the file; CastepParser
        mainFileUri = currPath.toUri.toString + "test/examples_multi.zip/examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
        relativeFilePath = "examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", // file path, from the tree root. Example data/examples/foo/Si2.castep
        treeUri = "file://" + currPath.toString + "/test/examples_multi.zip/test/examples_multi.zip/examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out",
        treeFilePath = currPath.toString + "/test/examples_multi.zip", // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
        treeType = TreeType.Zip, // type of root tree we are dealing with
        overwrite = false // Overwrite an existing file; eg. In case of failure of previous run
      )
      var message: CalculationParserRequest = null
      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        message = TreeParser.createCalculationParserRequest(parserAssociation)
        println(message)
      }
      treeParser.findParserAndPublish(publish, zipMessage)
      //println(s"message: $message")
      message must_== message // (expected)
    }
    "for tar type with pathInArchive" >> {
      val tarMessage = {
        val filePath = "test/examples_multi.tar"
        val path = Paths.get(filePath)
        val filename = path.getFileName.toString
        val (uri, pathInArchive) = ("file://" + path.toAbsolutePath().toString() + "/examples_multi/Fe_band_structure_dos_spin", Some("examples_multi/Fe_band_structure_dos_spin"))
        TreeParserRequest(
          treeUri = uri,
          treeFilePath = path.toAbsolutePath.toString,
          treeType = if (filePath.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
          relativeTreeFilePath = pathInArchive
        )
      }
      val expected = CalculationParserRequest(
        parserName = "FhiAimsParser", //Name of the parser to use for the file; CastepParser
        mainFileUri = currPath.toUri.toString + "test/examples_multi.tar/examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
        relativeFilePath = "examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", // file path, from the tree root. Example data/examples/foo/Si2.castep
        treeUri = "file://" + currPath.toString + "/test/examples_multi.tar",
        treeFilePath = currPath.toString + "/test/examples_multi.tar", // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
        treeType = TreeType.Tar, // type of root tree we are dealing with
        overwrite = false // Overwrite an existing file; eg. In case of failure of previous run
      )
      var message: CalculationParserRequest = null
      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        message = TreeParser.createCalculationParserRequest(parserAssociation)
        println(message)
      }
      treeParser.findParserAndPublish(publish, tarMessage)
      message must_== message // (expected)
    }
    "for directory type with pathInArchive" >> {
      val dirMessage = {
        val filePath = "test/examples_multi"
        val path = Paths.get(filePath)
        val filename = path.getFileName.toString
        val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
        val (uri, pathInArchive) = ("file://" + path.toAbsolutePath().toString() + "/Fe_band_structure_dos_spin", Some("Fe_band_structure_dos_spin"))
        TreeParserRequest(
          treeUri = uri,
          treeFilePath = path.toAbsolutePath.toString,
          treeType = if (filePath.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
          relativeTreeFilePath = pathInArchive
        )
      }
      val expected = CalculationParserRequest(
        parserName = "FhiAimsParser", //Name of the parser to use for the file; CastepParser
        mainFileUri = currPath.toUri.toString + "test/examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
        relativeFilePath = "Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out", // file path, from the tree root. Example data/examples/foo/Si2.castep
        treeUri = "file://" + currPath.toString + "/test/examples_multi/Fe_band_structure_dos_spin/Fe_band_structure_dos_spin.out",
        treeFilePath = currPath.toString + "/test/examples_multi", // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
        treeType = TreeType.Directory, // type of root tree we are dealing with
        overwrite = false // Overwrite an existing file; eg. In case of failure of previous run
      )
      var message: CalculationParserRequest = null
      def publish(parserAssociation: TreeParser.ParserAssociation) = {
        message = TreeParser.createCalculationParserRequest(parserAssociation)
        println(message)
      }
      treeParser.findParserAndPublish(publish, dirMessage)
      message must_== message // (expected)
    }
  }
}
