/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.meta

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.time.ZonedDateTime

import com.typesafe.scalalogging.StrictLogging
import scala.util.control.NonFatal
import scala.util.matching.Regex
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.JsonSupport.formats

object MetaInfoTool extends StrictLogging {

  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    if (list.isEmpty) {
      println(usage)
      return
    }
    val cmd = list.head
    list = list.tail
    cmd match {
      case "--help" | "-h" =>
        println(usage)
      case "reformat" => reformatCommand(list)
      case "cascade" => cascadeCommand(list)
      case command =>
        println(s"invalid command $command")
        println(usage)
    }
  }

  private val usage = """
      |Usage:
      |metaInfoTool [--help] <command>
      |
      |  Available commands:
      |
      |  reformat: Reformats the metainfo, and possibly converts between formats
      |  cascade: reformats and cascades changes exploded > new format > old format
      |  delete-backup: deletes all backup files
      |
      |  Run archiveTool <command> [--help] to view information about the
      |  options for each command.""".stripMargin

  private val usage_convert_old = """metaInfoTool convert-old
      |    [--to=<dir>] [--no-overwrite] <file1> [<file2>...]
      |
      |  Converts a meta info from the old format to the new one.""".stripMargin

  private val usage_reformat = """metaInfoTool reformat [--no-check] [--base-dir=<baseOutDir>] [--same|--exploded|--compact|--v1] [--in-dict=]<path1> [--out-dict=<outpath1>] [[--in-dict=]<path2> [--out-dict=<outpath2>]...]
      |
      |  Reformats the meta info dictionaries <path1> <path2>...
      |
      |  The input format of the given path can be:
      |  *.nomadmetainfo.json: a single document dictionary in the meta 1.0.0 format
      |  *.meta_dictionary.json: a single document dictionary in the meta 2.0.0 format
      |  <a directory>: assumes it contains an exploded dictionary in the meta 2.0.0 format
      |
      |  The output will in <outPath*> if given, otherwise if <baseOutDir> is given it will
      |  be a dictionary created inside <baseOutDir>, whereas if neither --base-dir nor
      |  --out-dict are given it will be in place (only supported for the same format).
      |
      |  The format is:
      |  --same     : the same as the input (default)
      |  --exploded : exploded dictionary in the 2.0.0 format
      |  --compact  : imploded single document meta 2.0.0 format
      |  --v1       : single document in the old meta 1.0.0 format
      |
      |  --no-checks disables some checks (that might fail when a meta_name is converted
      |  to lower case)""".stripMargin

  object DictFormat extends Enumeration {
    type DictFormat = Value
    val Compact, Exploded, Version1 = Value
  }

  def reformatCommand(args: List[String]): Unit = {
    var list: List[String] = args
    var targetFormat: Option[DictFormat.Value] = None
    var files: List[(Path, Option[Path])] = Nil
    var baseOutDir: Option[Path] = None
    var check: Boolean = true
    val baseDirRe = "^--base-dir=(.*)$".r
    val inDictRe = "^(?:--in-dict=)?(.*)$".r
    val outDirRe = "^--out-dict=(.*)$".r
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_reformat)
          return
        case "--same" =>
          targetFormat = None
        case "--no-check" =>
          check = false
        case "--exploded" =>
          targetFormat = Some(DictFormat.Exploded)
        case "--compact" =>
          targetFormat = Some(DictFormat.Compact)
        case "--v1" =>
          targetFormat = Some(DictFormat.Version1)
        case baseDirRe(d) =>
          baseOutDir = Some(Paths.get(d))
        case outDirRe(d) =>
          files = files match {
            case (p, None) :: rest =>
              (p, Some(Paths.get(d))) :: rest
            case (p, Some(x)) :: rest =>
              throw new Exception(s"Two --out-dict ($x, $d) given for a single --in-dict ($p)")
            case Nil =>
              throw new Exception(s"--out-dir=$d given before --in-dict")
          }
        case inDictRe(f) =>
          val p = Paths.get(f)
          if (!Files.exists(p)) {
            if (p.toString.startsWith("--"))
              throw new Exception(s"Unexpected non file argument $f, mistyped a command line flag?")
            else
              throw new Exception(s"Non existing file $f given as input")
          }
          files = (p -> None) :: files
      }
    }
    logger.info(s"converting $files to $targetFormat, baseDir = $baseOutDir")
    for ((p, outP) <- files) {
      var env: Option[SimpleMetaInfoEnv] = None
      val (origDict, origFormat) = {
        if (Files.isDirectory(p)) {
          (MetaDictionary.fromExplodedPath(p) -> DictFormat.Exploded)
        } else {
          val explodedDictionaryRe = "^([a-zA-Z0-9_]+)(?:\\s+([0-9]+(?:\\.[0-9]+(?:\\.[0-9]+-[-+a-zA-Z0-9_])?)?))?\\.meta_dictionary$".r
          val metaDictionaryRe = "^([a-zA-Z0-9_]+)(?:\\s+([0-9]+(?:\\.[0-9]+(?:\\.[0-9]+-[-+a-zA-Z0-9_])?)?))?\\.meta_dictionary\\.json$".r
          val v1DictionaryRe = "^.*\\.nomadmetainfo\\.json$".r
          p.getFileName.toString match {
            case metaDictionaryRe(name, version) =>
              (MetaDictionary.fromFilePath(p) -> DictFormat.Compact)
            case v1DictionaryRe() =>
              val e = SimpleMetaInfoEnv.fromFilePath(
                filePath = p.toString,
                dependencyResolver = new RelativeDependencyResolver()
              )
              env = Some(e)
              val d = e.toMetaDictionary(check = check)
              (d -> DictFormat.Version1)
            case f =>
              throw new Exception(s"Non standard extension for file '$f' ($p), ignoring it.")
          }
        }
      }
      val toFormat = targetFormat.getOrElse(origFormat)
      val outPath = outP match {
        case Some(pp) => pp
        case None =>
          baseOutDir match {
            case Some(bd) => bd.resolve(toFormat match {
              case DictFormat.Exploded => s"${origDict.metadict_name}.meta_dictionary"
              case DictFormat.Compact => s"${origDict.metadict_name}.meta_dictionary.json"
              case DictFormat.Version1 => s"${origDict.metadict_name}.nomadmetainfo.json"
            })
            case None =>
              if (toFormat != origFormat)
                throw new Exception(s"Neither baseDir (default for all) nor a specific out-dir has been given for $p, and in place reformatting is not acceptable when changing format (from $origFormat to $toFormat)")
              else
                p
          }
      }
      toFormat match {
        case DictFormat.Exploded =>
          logger.info("explode")
          if (!Files.exists(outPath))
            outPath.toFile.mkdirs()
          if (!Files.isDirectory(outPath))
            throw new Exception(s"$outPath is not a directory and format should be an exploded dictionary")
          origDict.toExplodedPath(outPath)
        case DictFormat.Compact =>
          logger.info("compact")
          if (!Files.exists(outPath.getParent))
            outPath.getParent.toFile.mkdirs()
          origDict.toFilePath(outPath)
        case DictFormat.Version1 =>
          logger.info("v1")
          val e = env match {
            case Some(ee) => ee
            case None =>
              val mInfo = MetaInfo.fromMetaDictionary(origDict)
              SimpleMetaInfoEnv.fromMetaInfo(mInfo)
          }
          e.toFilePath(outPath)
      }
    }
  }

  private val usage_cascade = """metaInfoTool cascade [--help] [--clean] [--base-dir=<baseDir>]
      |   [--[no-]v1] [--exploded-dir=<explodedDir>] [--compact-dir=<compactDir>] [--v1-dir=<v1Dir>]
      |
      |Defaults:
      |  - baseDir = .
      |  - explodedDir = <baseDir>/meta_info/meta_dictionary
      |  - compactDir  = <baseDir>/meta_info_exploded
      |  - v1Dir       = <baseDir>/meta_info/nomad_meta_info
      |  - v1          = true if --v1, false if --no-v1, otherwise true if v1Dir exists
      |
      |Reformats and updates all the connected metainfo versions.
      |In particular it
      | - reformats the exploded directory
      | - converts the dictionaries in the exploded directory to compact directory
      | - in the compact directory reformats the dictionaries not converted from the exploded directory
      | - if v1 then converts all the dictionaries in the compact directory to the v1 directory
      |
      |The end result should be that all meta info are nicely formatted and up to date.
      |All older files are preserved as *.bk<n> files. --clean avoids any reformatting,
      |and simply deletes all the backup and temporary files that are present.
      |""".stripMargin

  def cascadeCommand(args: List[String]): Unit = {
    var list: List[String] = args
    var files: List[(Path, Option[Path])] = Nil
    var v1: Option[Boolean] = None
    var clean: Boolean = false
    var baseDir: Option[Path] = None
    var explodedDir: Option[Path] = None
    var compactDir: Option[Path] = None
    var v1Dir: Option[Path] = None
    val baseDirRe = "^--base-dir=(.*)$".r
    val explodedDirRe = "^--exploded-dir=(.*)$".r
    val compactDirRe = "^--compact-dir=(.*)$".r
    val v1DirRe = "^--v1-dir=(.*)$".r
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_cascade)
          return
        case baseDirRe(d) =>
          baseDir = Some(Paths.get(d))
        case explodedDirRe(d) =>
          explodedDir = Some(Paths.get(d))
        case compactDirRe(d) =>
          compactDir = Some(Paths.get(d))
        case v1DirRe(d) =>
          v1Dir = Some(Paths.get(d))
        case "--clean" =>
          clean = true
        case "--v1" =>
          v1 = Some(true)
        case "--no-v1" =>
          v1 = Some(false)
        case a =>
          throw new Exception("")
      }
    }
    if (clean)
      logger.info(s"cleanup, no reformatting performed, removing backup files")
    val bDir: Path = baseDir.getOrElse(Paths.get("."))
    val expDir: Path = explodedDir.getOrElse(bDir.resolve("meta_info_exploded"))
    val cDir: Path = compactDir.getOrElse(bDir.resolve("meta_info/meta_dictionary"))
    val vDir: Path = v1Dir.getOrElse(bDir.resolve("meta_info/nomad_meta_info"))
    val doV1: Boolean = v1.getOrElse(Files.exists(vDir))
    val bkOrTmpRe = "^.*\\.(?:bk[0-9]*|tmp)$".r
    var doneCompact: Set[String] = Set()
    var doneV1: Set[String] = Set()
    if (Files.exists(expDir)) {
      val explodedDictRe = "^([_a-zA-Z0-9]+)(\\s+[0-9]+.*)?\\.meta_dictionary$".r
      val dirI = Files.list(expDir).iterator()
      while (dirI.hasNext) {
        val dir = dirI.next
        dir.getFileName.toString match {
          case explodedDictRe(name, v) =>
            val version = (if (null == v) "" else v)
            if (clean) {
              val fileI = Files.list(dir).iterator()
              while (fileI.hasNext) {
                val file = fileI.next
                file.getFileName.toString match {
                  case bkOrTmpRe() =>
                    Files.deleteIfExists(file)
                  case _ =>
                }
              }
            } else {
              val dict = MetaDictionary.fromExplodedPath(dir)
              dict.toExplodedPath(dir)
              dict.nameCheck(dir.resolve("_.meta_dictionary.json")) match {
                case Some(msg) =>
                  throw new Exception(s"Inconsistent directory name and dictionary content: $msg")
                case None =>
              }
              val targetPath = cDir.resolve(s"${name}${version}.meta_dictionary.json")
              doneCompact += targetPath.getFileName.toString
              dict.toFilePath(targetPath)
            }
          case f =>
            logger.debug(s"Ingnoring unexpected path $f")
        }
      }
    }
    if (Files.exists(cDir)) {
      val compactDictRe = "^([_a-zA-Z0-9]+)(\\s+[0-9]+.*)?\\.meta_dictionary\\.json$".r
      val fileI = Files.list(cDir).iterator()
      while (fileI.hasNext) {
        val file = fileI.next
        if (clean) {
          file.getFileName.toString match {
            case compactDictRe(_, _) =>
            case bkOrTmpRe() =>
              Files.delete(file)
            case _ =>
              logger.warn(s"Ignoring unexpected path $file")
          }
        } else {
          file.getFileName.toString match {
            case p @ compactDictRe(name, v) =>
              val version = (if (null == v) "" else v)
              val dict = MetaDictionary.fromFilePath(file)
              if (!doneCompact(p))
                dict.toFilePath(file)
              dict.nameCheck(file) match {
                case Some(msg) =>
                  throw new Exception(s"Inconsistent directory name and dictionary content: $msg")
                case None =>
              }
              if (doV1) {
                val targetPath = vDir.resolve(s"${name}${version}.nomadmetainfo.json")
                doneCompact += targetPath.getFileName.toString
                val mInfo = MetaInfo.fromMetaDictionary(dict)
                SimpleMetaInfoEnv.fromMetaInfo(mInfo)
                  .toFilePath(targetPath)
                doneV1 += targetPath.getFileName.toString
              }
            case bkOrTmpRe() =>
            case f =>
              logger.debug(s"Ingnoring unexpected path $file")
          }
        }
      }
    }
    if (doV1 && Files.exists(vDir)) {
      val v1DictRe = "^([_a-zA-Z0-9])+(\\s+[0-9]+.*)?\\.nomadmetainfo\\.json$".r
      val fileI = Files.list(vDir).iterator()
      while (fileI.hasNext) {
        val file = fileI.next
        if (clean) {
          file.getFileName.toString match {
            case v1DictRe(_, _) =>
            case bkOrTmpRe() =>
              Files.delete(file)
            case _ =>
              logger.warn(s"Ignoring unexpected path $file")
          }
        } else {
          file.getFileName.toString match {
            case f @ v1DictRe(name, v) =>
              val version = (if (null == v) "" else v)
              if (!doneV1(f)) {
                logger.warn(s"Orphan V1 dictionary $f (reformatting all the same)")
                val env = SimpleMetaInfoEnv.fromFilePath(
                  file.toString,
                  dependencyResolver = new RelativeDependencyResolver()
                )
                env.toFilePath(file)
              }
            case bkOrTmpRe() =>
            case _ =>
              logger.warn(s"Ignoring unexpected path $file")
          }
        }
      }
    }
  }
}
