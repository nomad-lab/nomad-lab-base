/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.webservice

import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest
import spray.http._
import spray.http._
import StatusCodes._
import org.json4s.{ JArray, JBool, JDecimal, JDouble, JField, JInt, JNothing, JNull, JObject, JString, JValue }
import java.nio.file.Paths
import eu.nomad_lab.meta.MetaInfoCollection
import eu.nomad_lab.meta.RelativeDependencyResolver
import eu.nomad_lab.meta.SimpleMetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.JsonUtils

import scala.concurrent.Future
import scala.concurrent.duration.Duration

class NomadMetaInfoServiceSpec extends Specification with Specs2RouteTest with NomadMetaInfoService {
  def actorRefFactory = system

  val metaInfoCollection: MetaInfoCollection = {
    val classLoader: ClassLoader = getClass().getClassLoader();
    val filePath = classLoader.getResource("nomad_meta_info/main.nomadmetainfo.json").getFile()
    val resolver = new RelativeDependencyResolver
    val mainEnv = SimpleMetaInfoEnv.fromFilePath(filePath, resolver)
    new SimpleMetaInfoEnv(
      name = "last",
      description = "latest version, unlike all others this one is symbolic and will change in time",
      source = JObject(JField("path", JString(Paths.get(filePath).getParent().toString()))),
      nameToGid = Map[String, String](),
      gidToName = Map[String, String](),
      metaInfosMap = Map[String, MetaInfoRecord](),
      dependencies = Seq(mainEnv),
      kind = MetaInfoEnv.Kind.Version
    )
  }

  "NomadMetaInfoService" should {

    "return meta info type of last version as nomad_meta_info_1_0" in {
      Get("/nmi/v/last/info.json") ~> myRoute ~> check {
        (JsonUtils.parseStr(responseAs[String]) \ "type").extract[String] must_== "nomad_meta_info_1_0"
      }
    }

    "leave GET requests to unknown paths unhandled" in {
      Get("/kermit") ~> myRoute ~> check {
        handled must beFalse
      }
    }

    "return a MethodNotAllowed error for PUT requests to version info.json" in {
      Put("/nmi/v/last/info.json") ~> sealRoute(myRoute) ~> check {
        status === MethodNotAllowed
        responseAs[String] === "HTTP method not allowed, supported methods: GET"
      }
    }

    "GET metaInfo graph" in {
      Get("/nmi/v/last/n/section_run/metainfograph.json") ~> sealRoute(myRoute) ~> check {
        (JsonUtils.parseStr(responseAs[String]) \ "nodes").extract[JArray].children.length >= 1 must_== true
      }
    }

    "GET metaInfo graph for incorrect metaInfo" in {
      Get("/nmi/v/common/n/someRandomStringThatSHouldNotPass/metainfograph.json") ~> sealRoute(myRoute) ~> check {
        responseAs[String] must_== "null"
      }
    }

    "GET multiple metaInfo graph" in {
      Get("/nmi/v/last/multiplemetainfograph.json?metaInfoList=atom_forces_t0,atom_forces_type,atom_label") ~> sealRoute(myRoute) ~> check {
        (JsonUtils.parseStr(responseAs[String]) \ "nodes").extract[JArray].children.length >= 3 must_== true
      }
    }

    "GET multiple metaInfo graph for incorrect metaInfo" in {
      Get("/nmi/v/last/multiplemetainfograph.json?metaInfoList=asadsad,atdsfsdfes_sade,basdsadel") ~> sealRoute(myRoute) ~> check {
        (JsonUtils.parseStr(responseAs[String]) \ "nodes").extract[JArray].children.length == 0 must_== true
      }
    }

    "GET metaInfo graph" in {
      Get("/nmi/v/last/n/section_run/metainfograph.json") ~> sealRoute(myRoute) ~> check {
        (JsonUtils.parseStr(responseAs[String]) \ "nodes").extract[JArray].children.length >= 1 must_== true
      }
    }

    "GET metaInfo graph" in {
      Get("/nmi/v/last/n/section_run/metainfograph.json") ~> sealRoute(myRoute) ~> check {
        (JsonUtils.parseStr(responseAs[String]) \ "nodes").extract[JArray].children.length >= 1 must_== true
      }
    }
    import spray.util._
    "store uncached metaInfo values" in {
      val tStamp1 = System.currentTimeMillis
      val f1 = metaInfoJson("last", "section_run").await
      val time1 = System.currentTimeMillis - tStamp1
      logger.info(s"Time for first request: $time1")
      Thread.sleep(500)
      val tStamp2 = System.currentTimeMillis
      val f2 = metaInfoJson("last", "section_run").await
      val time2 = System.currentTimeMillis - tStamp2
      logger.info(s"Time for second request: $time2")
      val res = if (time1 - time2 >= 0) true else false
      res must_== true
      f1 mustEqual f2
    }
    "store uncached version" in {
      val tStamp1 = System.currentTimeMillis
      val f1 = annotatedVersionJson("all").await
      val time1 = System.currentTimeMillis - tStamp1
      logger.info(s"Time for first request for version all: $time1")
      Thread.sleep(500)
      val tStamp2 = System.currentTimeMillis
      val f2 = annotatedVersionJson("all").await
      val time2 = System.currentTimeMillis - tStamp2
      logger.info(s"Time for second request for version all: $time2")
      val res = if (time1 - time2 >= 0) true else false
      res must_== true
      f1 mustEqual f2
    }

    "store keys for data cache" in {
      val data1 = metaInfoJson("last", "section_method").await
      val data2 = metaInfoJson("last", "section_run").await
      val data3 = metaInfoJson("last", "section_single_configuration_calculation").await
      data1.children.size mustNotEqual (0)
      data2.children.size mustNotEqual (0)
      data3.children.size mustNotEqual (0)
    }

    //    "store uncached metainfograph values" in {
    //      val tStamp1 = System.currentTimeMillis
    val data1 = metaInfoJson("last", "section_run").await
    //      val time1 = System.currentTimeMillis - tStamp1
    //      logger.info(s"Time for first request: $time1")
    //      Thread.sleep(500)
    //      val tStamp2 = System.currentTimeMillis
    //      val data2 = cachedMetaInfoGraph("last", "section_run").await
    //      val time2 = System.currentTimeMillis - tStamp2
    //      logger.info(s"Time for second request: $time2")
    //      time1 - time2 >= 0
    //      JsonUtils.normalizedStr(data1).substring(0, 50) mustEqual JsonUtils.normalizedStr(data2).substring(0, 50)
    //    }
    //
    //    "store keys for graph cache" in {
    //      val data1 = cachedMetaInfoGraph("last", "section_method").await
    //      val data2 = cachedMetaInfoGraph("last", "section_run").await
    //      JsonUtils.normalizedStr(data1).substring(0, 50) mustNotEqual JsonUtils.normalizedStr(data2).substring(0, 50)
    //    }
    //
    //    "not mix graph cache and data cache" in {
    //      val data1 = cachedMetaInfoJson("last", "section_run").await
    //      val data2 = cachedMetaInfoGraph("last", "section_run").await
    //      val data3 = cachedMetaInfoGraph("last", "section_single_configuration_calculation").await
    //      JsonUtils.normalizedStr(data1).substring(0, 50) mustNotEqual JsonUtils.normalizedStr(data2).substring(0, 50)
    //      JsonUtils.normalizedStr(data1).substring(0, 50) mustNotEqual JsonUtils.normalizedStr(data3).substring(0, 50)
    //      JsonUtils.normalizedStr(data1).substring(0, 50) mustEqual JsonUtils.normalizedStr(data1).substring(0, 50)
    //    }

  }
}
