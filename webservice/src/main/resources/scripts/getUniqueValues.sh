host=nomad-flink-01
val=$1


if [ "$val" == "uploader" -o "$val" == "program_name" -o "$val" == "atom_symbols" -o "$val" == "system_reweighted_composition" -o "$val" == "system_composition" -o "$val" == "section_repository_info.repository_spacegroup_nr" -o "$val" == "section_repository_info.repository_basis_set_type" -o "$val" == "section_repository_info.repository_system_type"  -o "$val" == "section_repository_info.repository_xc_treatment" -o "$val" == "section_repository_info.repository_crystal_system"  ]; then

curl -s -XGET "http://$host:9210/_search?pretty" -H 'Content-Type: application/json' -d"
{
  \"size\": 0,
  \"aggs\" : {
      \"counter\" : {
          \"terms\" : { \"field\" : \"$val\",  \"size\" : 4000 }
      }
   }
}
"
else
 echo "bash getAllValues.sh uploader|program_name|atom_symbols"
fi
